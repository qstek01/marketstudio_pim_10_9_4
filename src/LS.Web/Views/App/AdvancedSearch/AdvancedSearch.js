﻿
LSApp.controller("SearchController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$rootScope', '$localStorage', '$http', '$filter',
function ($scope, $window, $location, $routeParams, dataFactory, $rootScope, $localStorage, $http, $filter) {
    // --------------------------------------------------- Start Common Function --------------------------------------------------------------------------------

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    $localStorage.CategorySelectionValue = "Category Name";

    //$scope.monthSelectorOptions = {
    //    start: "year",
    //    depth: "year"
    //};
    //$scope.getType = function (x) {
    //    return typeof x;
    //};
    //$scope.isDate = function (x) {
    //    return x instanceof Date;
    //};
    $rootScope.getEnableSubProduct = function () {

        //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
        dataFactory.getEnableSubProduct().success(function (response) {

            $rootScope.EnableSubProduct = response;
        })
    };
    $rootScope.getEnableSubProduct();
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

    $scope.typeInput = 'text';
    $scope.searchValue = "Search Value";

    $scope.searchtxt = "";
    $scope.CategoryID = "";
    $scope.catalogIdAttr = 0;
    $scope.getCustomerIDs = 0;
    $scope.wildcardSearch = 0;
    if ($localStorage.getCatalogID === undefined) {
        $scope.catalogIdAttr = 0;
    } else {
        $scope.catalogIdAttr = $localStorage.getCatalogID;
        $scope.Catalog_id = $localStorage.getCatalogID;
        $scope.getCustomerIDs = $localStorage.getCustomerID;
    }
    $scope.categorydetails = false;
    $scope.itemnumberonly = false;
    $scope.subitemnumberonly = false;
    //$scope.usewildcards = false;
    $scope.categorydetails = true;
    $scope.productdetails = false;
    $scope.subproductdetails = false;
    $scope.familydetails = false;
    $scope.globalattributes = false;
    $scope.checksubpro = "subprod";
    $scope.searchEmptyResult = false;
    $scope.SearchTypeName = "Items";
    $scope.SearchType = "0";
    $scope.usewildcards = "4";
    $scope.searchResultsTitle = "";
    if ($scope.SearchTypeName != "Category") {
        //alert(1);
        $("#SearchCategoryTree").removeClass("disabledbutton");
        $("#SearchCategoryTree").addClass("enabled");
        $scope.categorydetails = false;
    }
    $scope.SearchcatalogDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {

                dataFactory.GetCatalogDetailsforSearchCustomers($scope.catalogIdAttr, $scope.getCustomerIDs).success(function (response) {
                    if ($localStorage.getCatalogID == "" || $localStorage.getCatalogID == undefined) {
                        $scope.catalogIdAttr = response[0].CATALOG_ID

                    }
                    else {
                        $scope.catalogIdAttr = $localStorage.getCatalogID;
                    }
                    var validCount = 0;
                    angular.forEach(response, function (data) {
                        if (data.CATALOG_ID.toString() == $scope.catalogIdAttr) {
                            validCount = 1;
                        }
                    });
                    if (validCount == 0) {
                        $scope.catalogIdAttr = response[0].CATALOG_ID
                        $scope.catalogIdAttrDataSource.read();
                    }
                    //dataFactory.GetCatalogDetails($scope.catalogIdAttr, $scope.getCustomerIDs).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.SearchTypeName = "Items";
    $scope.catalogIdAttrDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, sort: { field: "ATTRIBUTE_NAME" },
        transport: {
            read: function (options) {
                dataFactory.GetAllSpecifiedattributes($scope.catalogIdAttr, $scope.SearchTypeName).success(function (response) {

                    if (response.length != 0) {
                        if ($scope.SearchTypeName == 'SubItems') {
                            response[0].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            $scope.seletedAttribute = $localStorage.CatalogSubItemNumber;
                            if ($scope.subproductgrid == "True") {
                                $scope.seletedAttribute = $scope.Selectedattributetypeforsubproduct;
                                $localStorage.subproductgrid = "False";
                            }

                            if ($scope.subinvproductgrid == "True") {
                                $scope.seletedAttribute = $scope.Selectedattributetypeforinvsubproduct;
                                $localStorage.subinvproductgrid = "False";
                            }
                        }
                        if ($scope.SearchTypeName == 'Items') {
                            response[0].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            $scope.seletedAttribute = $localStorage.CatalogItemNumber;
                            if ($scope.productgrid == "True") {
                                $scope.seletedAttribute = $scope.Selectedattributetypeforproduct;
                                $localStorage.productgrid = "False";
                            }
                            if ($scope.invproductgrid == "True") {
                                $scope.seletedAttribute = $scope.Selectedattributetypeforinvproduct;
                                $localStorage.invproductgrid = "False";
                            }
                        }
                        else {
                            $scope.seletedAttribute = response[0].ATTRIBUTE_NAME;
                        }
                        //  $scope.getFilterType();
                        //$scope.GetFilters.read();
                        // $scope.SelectItem();
                    }
                    else {
                        $scope.seletedAttribute = "";
                        $scope.getFilterType()
                        $scope.GetFilters.read();
                        $scope.SelectItem();
                    }
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    //$scope.catalogIdAttrDataSource = new kendo.data.DataSource({
    //    type: "json",
    //    serverFiltering: true, sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
    //    transport: {
    //        read: function (options) {
    //            dataFactory.GetAllattributes($scope.catalogIdAttr).success(function (response) {
    //                options.success(response);
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }
    //});

    $(document).keypress(function (e) {
        if (e.which == 13) {
            // enter pressed
            $scope.btnsearchClick();
        }
    });
    $scope.selectcatalogattr = function (catalogid) {
        $scope.catalogIdAttr = catalogid;
        if (catalogid != 0 && catalogid != "") {
            $("#categorydisable").show();
            $("#categorylbldisable").show();
            $scope.catalogIdAttrDataSource.read();
            $rootScope.dropDownCategorySearchDatasource.read();
        } else {
            $rootScope.dropDownCategorySearchDatasource.read();
            //$("#categorydisable").hide();
            //$("#categorylbldisable").hide();
        }
    };
    $rootScope.Dashboards = true;
    $scope.itemnumberclick = function ($event) {
        if ($event) {
            //$scope.categorydetails = false;
            $scope.productdetails = true;
            //$scope.subproductdetails = false;
            //$scope.familydetails = false;
            //$scope.subitemnumberonly = false;
        }
    };
    //
    $scope.GetSelectedItem = function (e) {
        //alert(e);

        if (e.usewildcards == "1" || "2" || "3" || "4") {
            return true;
        }
        if ($scope.categorydetails == true || $scope.familydetails == true || $scope.productdetails == true || $scope.subproductdetails == true) {
            $scope.usewildcards = "1";
        }
        else if ($scope.subitemnumberonly == false && $scope.globalattributes == false && $scope.itemnumberonly == false) {
            $scope.categorydetails = true
        }
    };
    $scope.subitemnumberclick = function ($event) {
        if ($event) {
            //$scope.categorydetails = false;
            $scope.productdetails = false;
            //$scope.subproductdetails = false;
            //$scope.familydetails = false;
            //$scope.itemnumberonly = false;
        }
    };
    //start Sorting in checkbox
    chksortAsc.checked = false;
    $scope.sortAsc = function () {
        if (chksortAsc.checked == true) {
            $scope.catalogIdAttrDataSource = new kendo.data.DataSource({
                type: "json",
                serverFiltering: true, sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
                transport: {
                    read: function (options) {
                        dataFactory.GetAllSpecifiedattributes($scope.catalogIdAttr, $scope.SearchTypeName).success(function (response) {

                            if (response.length != 0) {
                                if ($scope.SearchTypeName == 'SubItems') {
                                    response[0].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                                    $scope.seletedAttribute = $localStorage.CatalogSubItemNumber;
                                }
                                if ($scope.SearchTypeName == 'Items') {
                                    response[0].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                                    $scope.seletedAttribute = $localStorage.CatalogItemNumber;
                                } else {
                                    $scope.seletedAttribute = response[0].ATTRIBUTE_NAME;
                                }

                            }
                            else {
                                $scope.seletedAttribute = "";
                            }
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        }
        else {
            $scope.catalogIdAttrDataSource = new kendo.data.DataSource({
                type: "json",
                serverFiltering: true, sort: { field: "ATTRIBUTE_NAME" },
                transport: {
                    read: function (options) {

                        dataFactory.GetAllSpecifiedattributes($scope.catalogIdAttr, $scope.SearchTypeName).success(function (response) {

                            if (response.length != 0) {
                                if ($scope.SearchTypeName == 'SubItems') {
                                    response[0].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                                    $scope.seletedAttribute = $localStorage.CatalogSubItemNumber;
                                }
                                if ($scope.SearchTypeName == 'Items') {
                                    response[0].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                                    $scope.seletedAttribute = $localStorage.CatalogItemNumber;
                                }
                                else {
                                    $scope.seletedAttribute = response[0].ATTRIBUTE_NAME;
                                }
                            }
                            else {
                                $scope.seletedAttribute = "";
                            }
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        }
    }


    //end
    //$scope.dropDownCategoryDatasource = new kendo.data.HierarchicalDataSource({
    //    type: "json",
    //    transport: {
    //        read: function (options) {
    //            dataFactory.GetCatalogCategorySortDetails($scope.catalogIdAttr, options.data.id).success(function (response) {
    //                options.success(response);
    //            }).error(function (response) {
    //                options.success(response);
    //            });
    //        }
    //    },
    //    schema: {
    //        model: {
    //            id: "id",
    //            hasChildren: "hasChildren"
    //        }
    //    }
    //});

    //if ($("#CategoryTree").val() !== undefined) {
    //    $scope.dropDownCategory = $("#CategoryTree").kendoExtDropDownTreeView({
    //        treeview: {
    //            dataSource: $scope.dropDownCategoryDatasource,
    //            dataTextField: "CATEGORY_NAME",
    //            dataValueField: "id",
    //            loadOnDemand: false,
    //            select: onfamilySelect
    //        }
    //    }).data("kendoExtDropDownTreeView");

    //    $scope.dropDownCategory.treeview().expand(".k-item");
    //}

    $("#searchid").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


    $scope.categorydetailsclick = function ($event) {
        //if ($event) {
        //$scope.itemnumberonly = false;
        //$scope.subitemnumberonly = false;
        //$scope.globalattributes = false;
        //// }

        $scope.productdetails = "1";
        //$scope.subproductdetails = false;
        //$scope.familydetails = false;
        if ($scope.usewildcards == "1") {
            return true;
        }
        if ($scope.categorydetails == true || $scope.familydetails == true || $scope.productdetails == true || $scope.subproductdetails == true) {
            $scope.usewildcards = "1";
        }
        else {
            $scope.usewildcards = "1";
        }
    };

    $scope.familydetailsclick = function ($event) {
        // if ($event) {
        $scope.itemnumberonly = false;
        $scope.subitemnumberonly = false;
        $scope.globalattributes = false;
        //}
        $scope.categorydetails = false;
        $scope.productdetails = false;
        $scope.subproductdetails = false;
        $scope.familydetails = "1";
        if ($scope.usewildcards == "1") {
            return true;
        }
        if ($scope.categorydetails == "1" || $scope.familydetails == "1" || $scope.productdetails == "1" || $scope.subproductdetails == "1") {
            $scope.usewildcards = "1";
        }
        else {
            $scope.usewildcards = "1";
        }
    };

    $scope.productdetailsclick = function ($event) {
        //  if ($event) {

        $scope.itemnumberonly = false;
        $scope.subitemnumberonly = false;
        $scope.globalattributes = false;
        // }
        $scope.categorydetails = false;
        $scope.productdetails = "1";
        $scope.subproductdetails = false;
        $scope.familydetails = false;
        if ($scope.usewildcards == "1") {
            return true;
        }
        if ($scope.categorydetails == "1" || $scope.familydetails == "1" || $scope.productdetails == "1" || $scope.subproductdetails == "1") {
            $scope.usewildcards = "1";
        }
        else {
            $scope.usewildcards = "1";
        }
    };
    $scope.subproductdetailsclick = function ($event) {

        $scope.itemnumberonly = false;
        $scope.subitemnumberonly = false;
        $scope.globalattributes = false;
        $scope.categorydetails = false;
        $scope.productdetails = false;
        $scope.subproductdetails = "1";
        $scope.familydetails = false;
        if ($scope.usewildcards == "1") {
            return true;
        }
        if ($scope.categorydetails == "1" || $scope.familydetails == "1" || $scope.productdetails == "1" || $scope.subproductdetails == "1") {
            $scope.usewildcards = "1";
        }
        else {
            $scope.usewildcards = "1";
        }
    };
    $scope.globalattributesclick = function ($event, val) {
        if (!$event && val) {
            $scope.itemnumberonly = "1";
            $scope.subitemnumberonly = "1";
            $scope.categorydetails = false;
            $scope.productdetails = false;
            $scope.subproductdetails = false;
            $scope.familydetails = false;
        }
        else if ($event && val) {
            $scope.itemnumberonly = "1";
            $scope.subitemnumberonly = "1";
        }
        else {
            $scope.itemnumberonly = false;
            $scope.subitemnumberonly = false;
        }
    };

    $scope.catalogChange = function (e) {
        $scope.clearText();
        var catalId = e.sender.value();
        if (catalId == '') {
            catalId = 0;
        }

        $scope.catalogIDD = catalId;
        $scope.selectcatalogattr(catalId);
        $rootScope.selecetedCatalogId = catalId;
        // $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
        $scope.GetUserRoleRightsAll();
        $scope.catalogIdAttrDataSource.read();



        // alert(e.sender.text());
    };
    $scope.attributechange = function (e) {

        $scope.clearText();
        $scope.seletedAttribute = e.sender.value();

        if ($scope.seletedAttribute == "Category id" || $scope.seletedAttribute == "Category Name" || $scope.seletedAttribute == "Short Description") {
            $localStorage.CategorySelectionValue = $scope.seletedAttribute;
        }
        $scope.getFilterType()
        $scope.GetFilters.read();
        $scope.searchtxt = "";
        document.getElementById("mainCatalogSearch").readOnly = false;
    };

    $scope.getFilterType = function () {
        dataFactory.GetType($scope.seletedAttribute).success(function (response) {
            $scope.Filterdatatype = response;
            $scope.GetFilters.read();
            //options.success(response);
        }).error(function (error) {
            options.error(error);
        });

    };


    $scope.GetFilters = new kendo.data.DataSource({

        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetAllFiltersValue($scope.seletedAttribute).success(function (response) {
                    if (response.length != 0) {
                        $scope.selectedFilters = response[0].FilterId;
                        $scope.usewildcards = response[0].FilterId;
                        if ($scope.Filterdatatype == "Date") {
                            $scope.typeInput = 'text';
                            //$scope.typeInput = false;


                            if ($scope.usewildcards == "14") {

                                $scope.hidenValue = false;
                                $scope.endDateHide = true;
                                $scope.hidenSearchText = false;
                                $scope.numericTextHiden = false;
                            }
                            else if ($scope.usewildcards != undefined) {
                                $scope.hidenValue = true;
                                $scope.endDateHide = false;
                                $scope.hidenSearchText = false;
                                $scope.numericTextHiden = false;
                            }
                            else {
                                $scope.hidenSearchText = true;
                                $scope.hidenValue = false;
                                $scope.endDateHide = false;
                                $scope.numericTextHiden = false;
                            }
                        }
                        else if ($scope.Filterdatatype == "Number") {

                            $scope.typeInput = 'number';


                            //var clean = $scope.searchtxt.replace(/[^-0-9\.]/g, '');
                            //var negativeCheck = clean.split('-');
                            //var decimalCheck = clean.split('.');
                            if ($scope.usewildcards == "14") {

                                $scope.hidenValue = false;
                                $scope.endDateHide = false;
                                $scope.hidenSearchText = false;
                                $scope.numericTextHiden = true;
                            }
                            else if ($scope.usewildcards != undefined) {
                                $scope.hidenValue = false;
                                $scope.endDateHide = false;
                                $scope.hidenSearchText = true;
                                $scope.numericTextHiden = false;
                            }
                            else {
                                $scope.hidenSearchText = true;
                                $scope.hidenValue = false;
                                $scope.endDateHide = false;
                                $scope.numericTextHiden = false;
                            }
                        }
                        else {
                            $scope.typeInput = 'text';

                            $scope.hidenSearchText = true;
                            $scope.hidenValue = false;
                            $scope.endDateHide = false;
                            $scope.numericTextHiden = false;
                        }

                    }
                    else {
                        $scope.selectedFilters = "";
                        $scope.typeInput = 'text';

                    }
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    //if ($scope.searchTextBox = 'searchTextBox') {
    //    $("#mainCatalogSearch").addEventListener("keypress", function (evt) {
    //        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
    //            evt.preventDefault();
    //        }
    //    });
    //}
    // $scope.categorydetails = true;
    // $scope.productdetails = true;
    //  $scope.familydetails = true;
    // --------------------------------------------------- End Common Function --------------------------------------------------------------------------------
    // --------------------------------------------------- Start button clicks events--------------------------------------------------------------------------------

    $scope.ModifyResults = function (data) {
        if (data.length === 0) {
            $("#dynamictable").hide();
        }
        $("#familyEditor").hide();
        $("#searchtab").show();
        $("#resultbtntab").hide();
    };

    $scope.enterKeyPressedSearch = function (keyEvent) {
        if (keyEvent.keyCode == 13) {
            $scope.btnsearchClick();
        }
    };
    /////////////////////////////

    //$scope.Mysearch=function()
    //{


    //}


    $scope.SearchBy = function () {
        if ($scope.catalogIdAttr === '1') {

            $.msgBox({
                title: "CatalogStudio",
                content: 'Please change the working catalog.',
                type: "error"
            });
            return false;
        }
        if ($scope.searchtxt == "" || $scope.searchtxt == undefined) {
            $.msgBox({
                title: "CatalogStudio",
                content: 'Please enter a value in the Search text.',
                type: "error"
            });
            return false;
        }
        $scope.productData = [];
        if ($scope.CategoryID !== "") {
            $scope.Option = '1';
        }
        else
            $scope.Option = '2';

        $("#divSubItemSearch").hide();
        if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && $scope.SearchTypeName == "Category") {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#dynamictable").hide();
            $("#divSubProductSearch").hide();
            // $scope.seletedAttribute;
            $scope.seletedAttribute = $localStorage.CategorySelectionValue;
            $scope.categorydetails = true;
            $rootScope.dropDownCategorySearchDatasource.read();
            $scope.GetcategorySearchGridresultsdataSource.read();
            $scope.GetcategorySearchGridresultsdataSource.page(1);
        }
        else if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && $scope.SearchTypeName == "Product") {

            $scope.searchid = 0;
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();
            $scope.categorydetails = false;
            $("#dynamictable").hide();

            $scope.GetFamilySearchGridresultsdataSource.read();
            $scope.GetFamilySearchGridresultsdataSource.page(1);
        }
        else if ($scope.searchtxt !== "" && ($scope.searchtxt != undefined || ($scope.searchEndtxt != undefined)) && $scope.SearchTypeName == "Items") {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;

            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();
            $("#divSubItemSearch").hide();
            $("#dynamictable").hide();
            $("#divSubProductSearch").hide();
            $scope.categorydetails = false;
            if ($scope.seletedAttribute == 'ITEM#' || $scope.seletedAttribute == $localStorage.CatalogItemNumber) {
                $("#divProductSearch").hide();
                $scope.GetItemSearchResultsdataSource.read();
                $scope.GetItemSearchResultsdataSource.page(1);
                //$scope.PageTotalSize = $("#itemSearchResult").data("kendoGrid").dataSource.pageSize();
                //$scope.PageSize = $("#itemSearchResult").data("kendoGrid").dataSource.page();
                // $scope.PageTotal = $("#itemSearchResult").data("kendoGrid").dataSource.data().length;

            }
            else {
                $("#divItemSearch").hide();
                $scope.GetProductSearchGridresultsdataSource.read();
                $scope.GetProductSearchGridresultsdataSource.page(1);
            }
        }
        else if ($scope.searchtxt !== "" && ($scope.searchtxt != undefined || ($scope.searchEndtxt != undefined)) && $scope.SearchTypeName == "SubItems") {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();
            $("#divItemSearch").hide();
            $scope.categorydetails = false;
            $("#dynamictable").hide();
            if ($scope.seletedAttribute == 'SUBITEM#' || $scope.seletedAttribute == 'ITEM#' || $scope.seletedAttribute == $localStorage.CatalogItemNumber || $scope.seletedAttribute == $localStorage.CatalogSubItemNumber) {
                $("#divSubProductSearch").hide();
                $scope.GetSubItemSearchResultsdataSource.read();
                $scope.GetSubItemSearchResultsdataSource.page(1);
            }
            else {
                $("#divItemSearch").hide();
                $scope.GetSubProductSearchGridresultsdataSource.read();
                $scope.GetSubProductSearchGridresultsdataSource.page(1);
            }
        }

        else {
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();
            $("#dynamictable").hide();
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'No results found, please try using different combinations',

                //type: "info"
            });

        }
    };

    //test
    $scope.btnsearchClick = function () {
        if ($scope.catalogIdAttr == '0' && $scope.searchtxt != "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "This search has ALL catalogs selected and may take some time to process.",
                type: "confirm",
                buttons: [{ value: "Ok" }, { value: "Cancel" }],
                success: function (result) {
                    if (result === "Ok") {
                        $scope.SearchBy();
                    }
                }

            });
            return false
        }

        else if ($scope.searchtxt == "" || $scope.searchtxt == undefined) {
            $scope.searchEmptyResult = true;
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a value in the Search text.',
                type: "error"
            });
            return false;
        }
        else if (($scope.searchEndtxt == "" || $scope.searchEndtxt == undefined) && ($scope.selectedFilters == 14)) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a value in the Search text.',
                type: "error"
            });
            return false;
        }

        else if ($scope.catalogIdAttr != '0') {
            $scope.SearchBy();
        }
    };
    //end
    $scope.btncancelclick = function () {
        $("#SearchCategoryTree").removeClass("disabledbutton");
        $scope.searchtxt = "";
        $scope.usewildcards = '4';
        $("#divItemSearch").hide();
        $("#divSubItemSearch").hide();
        $("#divCategorySearch").hide();
        $("#divFamilySearch").hide();
        $("#divProductSearch").hide();
        $("#divSubProductSearch").hide();
        $("#dynamictable").hide();
        $scope.SearchTypeName = "Items";
        $scope.seletedAttribute = "Items";
        //$scope.SearchcatalogDataSource.read();
        $scope.categorydetails = false;
        $scope.IsAttribute = false;
        $rootScope.dropDownCategorySearchDatasource.read();
        $scope.SearchcatalogDataSource.read();
        ///   $("#drp").removeClass("disabledbutton");
        $scope.SearchTypeName = "Items";
        chksortAsc.checked = false;
        $scope.catalogIdAttrDataSource.read();
        $scope.catalogIdAttr = $localStorage.getCatalogID;
        $scope.searchEmptyResult = false;

        //if (chksortAsc.checked == false) {
        //    $scope.sortAsc();
        //}
        $scope.Filterdatatype = "Text"
        $scope.usewildcards = 0
        //$scope.getFilterType()
        $scope.GetFilters.read();
        $scope.SelectItem();

        //$scope.getFilterType()
        //$scope.GetFilters.read();
        //$scope.SelectItem();
        $scope.hidenSearchText = true;
        $scope.hidenValue = false;
        $scope.endDateHide = false;
        $scope.numericTextHiden = false;
        $scope.searchtxt = "";
        //$rootScope.dropDownCategoryDatasource.read();
    };
    $scope.display = false;
    $scope.mouseleave = function (val) {
        var val = $("#searchid").val();
        if (val !== "0" && val !== "") {
        }
        else {
            $("#categorydisable").show();
            $("#categorylbldisable").show();
        }
    };
    $scope.blur = function () {
        $("#searchid").val(0);
        $("#categorydisable").show();
        $("#categorylbldisable").show();
    };

    $scope.clearText = function () {
        $scope.searchValue = "Search Value";
        $scope.searchEmptyResult = false;
        $("#divItemSearch").hide();
        $("#divItemSearch").hide();
        $("#divSubItemSearch").hide();
        $("#divCategorySearch").hide();
        $("#divFamilySearch").hide();
        $("#divProductSearch").hide();
        $("#divSubProductSearch").hide();
        $("#dynamictable").hide();
        //  $("#searchEmptyResult").hide();
        $scope.searchtxt = "";
        $scope.searchEndtxt = "";

    }

    $scope.btnRefresh = function () {

        $scope.clearText();
        clearFiter();
    }

    // --------------------------------------------------- End button clicks event --------------------------------------------------------------------------------

    // ---------------------------------------------------Start Item Search Results--------------------------------------------------------------------------------

    $scope.GetItemSearchResultsdataSource = new kendo.data.DataSource({
        pageSize: 100,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        //messages: {
        //    itemsPerPage: ''
        //},
        //sort: { field: "FAMILY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.SearchResults($scope.searchtxt, $scope.selectedFilters, $scope.catalogIdAttr, $scope.CategoryID).success(function (response) {
                    if (response.length == 0) {
                        $scope.searchEmptyResult = true;
                        // $scope.searchResultsTitle = "";
                        $("#divItemSearch").hide();
                    }
                    else {
                        $("#divItemSearch").show();
                        //$scope.searchResultsTitle = "for " + ' value "' + $scope.searchtxt + '"';
                        $scope.searchEmptyResult = false;

                        $scope.PageTotal = response.length;
                        $scope.TotalPerPage = 'Found ' + $scope.PageTotal + ' Items';
                    }
                    $scope.PageTotal = response.length;
                    $scope.TotalPerPage = 'Found ' + $scope.PageTotal + ' Items';
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    STRING_VALUE: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    PRODUCT_ID: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    OBJECT_TYPE: { editable: false },
                    OBJECT_NAME: { editable: false },
                    FAMILY_ID: { editable: false },
                    FAMILY_NAME: { editable: false },
                    SUBFAMILY_NAME: { editable: false },
                    CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    SUBCATNAME_L1: { editable: false },
                    SUBCATNAME_L2: { editable: false },
                    SUBCATNAME_L3: { editable: false },
                    CATALOG_ID: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" },
                    MODIFIED_USER: { editable: false },
                    MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '',
                    //type: "info"
                });


            }
        }

    });


    $scope.GetSubItemSearchResultsdataSource = new kendo.data.DataSource({
        pageSize: 100,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        //messages: {
        //    itemsPerPage: ''
        //},
        //sort: { field: "FAMILY_NAME", dir: "asc" },
        transport: {
            read: function (options) {

                if ($scope.catalogIdAttr == "") {
                    $scope.catalogIdAttr = 0;
                }
                dataFactory.SubSearchResults($scope.searchtxt, $scope.selectedFilters, $scope.catalogIdAttr, $scope.CategoryID).success(function (response) {
                    if (response.length == 0) {
                        $scope.searchEmptyResult = true;
                        //  $scope.searchResultsTitle = "";
                        $("#divSubItemSearch").hide();
                    }
                    else {
                        $("#divSubItemSearch").show();
                        $scope.searchEmptyResult = false;
                        //$scope.searchResultsTitle = "for " + ' value "' + $scope.searchtxt + '"';
                    }
                    $scope.PageTotal = response.length;
                    $scope.TotalPerPage = 'Found ' + $scope.PageTotal + ' Items';
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    STRING_VALUE: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    PRODUCT_ID: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    OBJECT_TYPE: { editable: false },
                    OBJECT_NAME: { editable: false },
                    FAMILY_ID: { editable: false },
                    FAMILY_NAME: { editable: false },
                    SUBFAMILY_NAME: { editable: false },
                    CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    SUBCATNAME_L1: { editable: false },
                    SUBCATNAME_L2: { editable: false },
                    SUBCATNAME_L3: { editable: false },
                    CATALOG_ID: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" },
                    MODIFIED_USER: { editable: false },
                    MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '',
                    type: "error"
                });
            }
        }
    });
    $("#itemSearchResult").kendoTooltip({
        filter: "td:nth-child(2)", //this filter selects the second column's cells
        position: "right",
        content: function (e) {
            var dataItem = $("#itemSearchResult").data("kendoGrid").dataItem(e.target.closest("tr"));
            var content = dataItem.CATALOG_NAME + "->";
            if (dataItem.SUBCATNAME_L1 != '_' && dataItem.SUBCATNAME_L1 != null && dataItem.SUBCATNAME_L1 != '-' && dataItem.SUBCATNAME_L1 != '') {
                content = content + dataItem.SUBCATNAME_L1 + "->";
            }
            //if (dataItem.SUBCATNAME_L2 != '_' && dataItem.SUBCATNAME_L2 != null && dataItem.SUBCATNAME_L2 != '-' && dataItem.SUBCATNAME_L2 != '') {
            //    content = content + dataItem.SUBCATNAME_L2 ;
            //}
            //if (dataItem.SUBCATNAME_L3 != '_' && dataItem.SUBCATNAME_L3 != null && dataItem.SUBCATNAME_L3 != '-' && dataItem.SUBCATNAME_L3 != ' ') {
            //    content = content + dataItem.SUBCATNAME_L3 + "->";
            //}
            content = content + dataItem.CATEGORY_NAME + '->' + dataItem.FAMILY_NAME + '->' + dataItem.STRING_VALUE;
            return content;
        }
    }).data("kendoTooltip");
    $scope.itemSearchGridresultsColumns = [
         { field: "STRING_VALUE", title: "Searched Value", template: "<a class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=STRING_VALUE#</a>", width: "180px" },
         { field: "STRING_VALUE", title: $localStorage.CatalogItemNumber, template: "<a class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=STRING_VALUE#</a>", width: "180px" },
          { field: "FAMILY_NAME", title: "Product Name", template: "<a title='Product Name' class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=FAMILY_NAME#</a>", width: "auto" },
           { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=CATEGORY_NAME#</a>", width: "auto" },
         { field: "CATALOG_NAME", title: "Catalog Name", width: "auto" },
         { field: "SUBCATNAME_L1", title: "Category Name", width: "180px", hidden: true },
        { field: "SUBCATNAME_L2", title: "SubCatName L2", width: "180px", hidden: true },
        { field: "SUBCATNAME_L3", title: "SubCatName L3", width: "180px", hidden: true },
         { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "auto", hidden: true },
         { field: "CREATED_USER", title: "Created User", width: "180px", hidden: true },
         { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true },
         { field: "MODIFIED_USER", title: "Modified User", width: "180px", hidden: true },
         { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true },
     //{ field: "SUBCATNAME_L1", title: "SubCatName L1", width: "180px", hidden: true },
     //         { field: "SUBCATNAME_L2", title: "SubCatName L2", width: "180px", hidden: true },
     //         { field: "SUBCATNAME_L3", title: "SubCatName L3", width: "180px", hidden: true }
    ];
    $("#subitemSearchResult").kendoTooltip({
        filter: "td:nth-child(2)", //this filter selects the second column's cells
        position: "right",
        content: function (e) {
            var dataItem = $("#subitemSearchResult").data("kendoGrid").dataItem(e.target.closest("tr"));
            var content = dataItem.CATALOG_NAME + "->";
            if (dataItem.SUBCATNAME_L1 != '_' && dataItem.SUBCATNAME_L1 != null && dataItem.SUBCATNAME_L1 != '-' && dataItem.SUBCATNAME_L1 != '') {
                content = content + dataItem.SUBCATNAME_L1 + "->";
            }
            //if (dataItem.SUBCATNAME_L2 != '_' && dataItem.SUBCATNAME_L2 != null && dataItem.SUBCATNAME_L2 != '-' && dataItem.SUBCATNAME_L2 != '') {
            //    content = content + dataItem.SUBCATNAME_L2;
            //}
            //if (dataItem.SUBCATNAME_L3 != '_' && dataItem.SUBCATNAME_L3 != null && dataItem.SUBCATNAME_L3 != '-' && dataItem.SUBCATNAME_L3 != ' ') {
            //    content = content + dataItem.SUBCATNAME_L3;
            //}
            content = content + dataItem.CATEGORY_NAME + '->' + dataItem.FAMILY_NAME + '->' + dataItem.STRING_VALUE;
            return content;
        }
    }).data("kendoTooltip");
    $scope.subitemSearchGridresultsColumns = [
        { field: "STRING_VALUE", title: "Searched Value", template: "<a class='k-link' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=STRING_VALUE#</a>", width: "180px" },
        { field: "STRING_VALUE", title: $localStorage.CatalogSubItemNumber, template: "<a class='k-link' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=STRING_VALUE#</a>", width: "180px" },
        { field: "FAMILY_NAME", title: "Product Name", template: "<a title='Product Name' class='k-link' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=FAMILY_NAME#</a>", width: "auto" },
        { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=CATEGORY_NAME#</a>", width: "auto" },
        { field: "CATALOG_NAME", title: "Catalog Name", width: "auto" },
        { field: "SUBCATNAME_L1", title: "Category Name", width: "180px", hidden: true },
        { field: "SUBCATNAME_L2", title: "SubCatName L2", width: "180px", hidden: true },
        { field: "SUBCATNAME_L3", title: "SubCatName L3", width: "180px", hidden: true },
        { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "auto", hidden: true },
        //{ field: "CATALOG_ITEM_NO", title: "ITEM#", template: "<a class='k-link' href='javascript:void(0);' >#=CATALOG_ITEM_NO#</a>", width: "auto", hidden: true },
        { field: "CREATED_USER", title: "Created User", width: "180px", hidden: true },
        { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true },
        { field: "MODIFIED_USER", title: "Modified User", width: "180px", hidden: true },
        { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true }];

    $scope.ngclkproductid = function (e) {
        if (e != undefined && e.dataItem != undefined && e.dataItem.PRODUCT_ID != undefined)
            $rootScope.ProductID = "ProductId~" + e.dataItem.PRODUCT_ID;
        $('#tabstrip-2').css('display', 'block');
        $scope.dynamictable = true;
        $scope.divProductGrid = true;
        $scope.myDIV = true;
        $scope.newProductBtn = true;
        $scope.speccontainer = false;
        $scope.familiescontainer = false;
        $scope.clonecontainer = false;
        $scope.tablescontainer = false;
        $scope.groupscontainer = false;
        $scope.selectedattributeEdit = false;
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        $("#sampleProdgrid").show();
        $("#subproducts").hide();
        $rootScope.clickSelectedItems(e.dataItem, true, false);
        var tabstripSp = $("#productgridtabstrip").data("kendoTabStrip");
        var myTabSp = tabstripSp.tabGroup.children("li").eq(0);
        tabstripSp.enable(myTabSp);
        tabstripSp.select(myTabSp);
        $rootScope.inverted = '';

        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;width: 1320px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1320px !important;");

        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");
        }

        if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1750px; !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1750px !important;width: 1750px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1590) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1210px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1210px !important;");

        }
        if (window.screen.availWidth >= 1600 && window.screen.availWidth <= 1690) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1450px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1450px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1195px !important;");

        }

        /*Sub Product*/

        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1270px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1270px !important;");
        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");
        }

        if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1750px; !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1750px !important;width: 1750px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1440px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1440px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

        }

    };
    $scope.ngclkfamilyid = function (e) {
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        e.dataItem.id = e.dataItem.FAMILY_ID;
        $rootScope.clickSelectedItems(e.dataItem, true, true);

        var tabstripSp = $("#productgridtabstrip").data("kendoTabStrip");
        var myTabSp = tabstripSp.tabGroup.children("li").eq(0);
        tabstripSp.enable(myTabSp);
        tabstripSp.select(myTabSp);
        $rootScope.inverted = '';

        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1270px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1270px !important;");

        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");
        }

        if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1750px; !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1750px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1590) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1210px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1210px !important;");

        }
        if (window.screen.availWidth >= 1600 && window.screen.availWidth <= 1690) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1430px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1430px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

        }

        /*Family Grid*/

        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#familygrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;width: 1320px !important;");
            $("#familygrid tbody").css("cssText", "margin-left: -2px;width: 1320px !important;");
            //$('#sampleProdgrid').css('max-width', '1220px', '!important');
            //$('#sampleProdgrid')[0].style.maxWidth='1220px !important';
            //$('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
            //$('#sampleProdgrid tbody')[0].style.maxWidth = '1220px !important';
        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#familygrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
            $("#familygrid tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
        }

        if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
            $("#familygrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1900px !important;");
            $("#familygrid tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
            $("#familygrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;width: 1460px !important;");
            $("#familygrid tbody").css("cssText", "margin-left: -2px;width: 1460px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#familygrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
            $("#familygrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

        }

        /*Sub Product*/

        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1270px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1270px !important;");
        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");
        }

        if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1750px; !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1750px !important;width: 1750px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1440px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1440px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

        }

        $rootScope.LoadFamilyData(e.dataItem.CATALOG_ID, e.dataItem.FAMILY_ID, e.dataItem.CATEGORY_ID);

    };
    $scope.ngclkSubProductid = function (e) {
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        $("#divsubProductGrid").show();
        $("#subproductpaging1").show();
        e.dataItem.id = e.dataItem.FAMILY_ID;
        $rootScope.clickSelectedItemsInverted(e.dataItem, true);
        var tabstripSp = $("#productgridtabstrip").data("kendoTabStrip");
        var myTabSp = tabstripSp.tabGroup.children("li").eq(1);
        tabstripSp.enable(myTabSp);
        tabstripSp.select(myTabSp);
        $rootScope.inverted = '';
        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1315px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1315px !important;");
        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1750px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1750px !important;");
        }

        if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1750px; !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1750px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1210px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1210px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#sampleProdgrid").css("cssText", "margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

        }

        /*Sub Product*/

        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1270px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1270px !important;");

        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1750px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1781px !important;width: 1781px !important;");
        }

        if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1750px; !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1780px !important;width: 1780px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1440px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1440px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

        }
    };


    $scope.ngclkCategory = function (e) {
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        $rootScope.clickSelectedItem(e.dataItem);
        $scope.newProductBtn1 = false;
    };

    $scope.ngclkCategoryid = function (e) {
        $('#familyEditor').show();
        $('#recentmodifiedtab').hide();
        $('#resultbtntab').show();
        e.dataItem.id = e.dataItem.CATEGORY_ID;
        $rootScope.clickSelectedItem(e.dataItem);
    };


    $scope.navigate = function (e) {
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        e.id = e.FAMILY_ID;
        $rootScope.clickSelectedItems(e, true, true);
        var tabstripSp = $("#productgridtabstrip").data("kendoTabStrip");
        var myTabSp = tabstripSp.tabGroup.children("li").eq(0);
        tabstripSp.enable(myTabSp);
        tabstripSp.select(myTabSp);
    };
    // --------------------------------------------------- End Item Search Results -----------------------------------------------------------------------------------

    // ---------------------------------------------------Start Category Search Results--------------------------------------------------------------------------------

    $scope.GetcategorySearchGridresultsdataSource = new kendo.data.DataSource({
        pageSize: 100,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        //messages: {
        //    itemsPerPage: ''
        //},
        //sort: { field: "CATEGORY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetCategorySearchResults($scope.searchtxt, $scope.usewildcards, $scope.catalogIdAttr, $scope.CategoryID, $scope.Option, $scope.seletedAttribute).success(function (response) {
                    if (response.length != 0) {

                        $scope.isCategorySearchResultsNull = false;
                        $scope.searchEmptyResult = false;
                        $("#divCategorySearch").show();
                        // $scope.searchResultsTitle = "for " + ' value "' + $scope.searchtxt + '"';
                    }
                    else {

                        $("#divCategorySearch").hide();
                        $scope.isCategorySearchResultsNull = true;

                        $scope.searchEmptyResult = true;
                        // $scope.searchResultsTitle = "";
                    }
                    $scope.PageTotal = response.length;
                    $scope.TotalPerPage = 'Found ' + $scope.PageTotal + ' Items';
                  

                    // To insert searched values into grid
                    for (var res = 0 ; res < response.length ; res++) {
                        response[res].Search_Result = $scope.searchtxt;
                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "CATEGORY_ID",
                fields: {
                    Search_Result: { editable: false }
                    , CATALOG_ID: { editable: false }
                    , CATALOG_NAME: { editable: false }
                    , CATEGORY_ID: { editable: false }
                    , CATEGORY_NAME: { editable: false }
                    , PARENT_CATEGORY: { editable: false }
                    , SHORT_DESC: { editable: false }
                    , IMAGE_FILE: { editable: false }
                    , IMAGE_TYPE: { editable: false }
                    , IMAGE_NAME: { editable: false }
                    , IMAGE_NAME2: { editable: false }
                    , IMAGE_FILE2: { editable: false }
                    , IMAGE_TYPE2: { editable: false }
                    , CUSTOM_NUM_FIELD1: { editable: false }
                    , CUSTOM_NUM_FIELD2: { editable: false }
                    , CUSTOM_NUM_FIELD3: { editable: false }
                    , CUSTOM_TEXT_FIELD1: { editable: false }
                    , CUSTOM_TEXT_FIELD2: { editable: false }
                    , CUSTOM_TEXT_FIELD3: { editable: false }
                    , CREATED_USER: { editable: false }
                    , CREATED_DATE: { editable: false, type: "date" }
                    , MODIFIED_USER: { editable: false }
                    , MODIFIED_DATE: { editable: false, type: "date" }
                    , PUBLISH: { editable: false }
                    , PUBLISH2PRINT: { editable: false }
                    , PUBLISH2CD: { editable: false }
                    , WORKFLOW_STATUS: { editable: false }
                    , WORKFLOW_COMMENTS: { editable: false }
                    , CLONE_LOCK: { editable: false }
                    , IS_CLONE: { editable: false }
                    , CATEGORY_SHORT: { editable: false }
                    , CUSTOMER_ID: { editable: false }
                    , CATEGORY_PARENT_SHORT: { editable: false }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '',
                    //type: "info"
                });
            }
        },
        change: function (e) {
            if ($scope.isCategorySearchResultsNull)
                $("#divCategorySearch").hide();
        }
    });



    $scope.categorySearchGridresultsColumns = [
        { field: "Search_Result", title: "Searched Value", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkCategory(this)'>#=Search_Result#</a>", width: "180px" },
        { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkCategory(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
        { field: "CATEGORY_PARENT_SHORT", title: "Parent Category", width: "180px" },
        { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
        { field: "SHORT_DESC", title: "Short description", width: "180px", hidden: true },
        { field: "IMAGE_FILE", title: "Image File", width: "180px", hidden: true },
        { field: "IMAGE_NAME", title: "Image Name", width: "180px", hidden: true },
        { field: "IMAGE_NAME2", title: "Image Name2", width: "180px", hidden: true },
        { field: "IMAGE_FILE2", title: "Image File2", width: "180px", hidden: true },
        { field: "CUSTOM_NUM_FIELD1", title: "Custom Num Field1", width: "180px", hidden: true },
        { field: "CUSTOM_NUM_FIELD2", title: "Custom Num Field2", width: "180px", hidden: true },
        { field: "CUSTOM_NUM_FIELD3", title: "Custom Num Field3", width: "180px", hidden: true },
        { field: "CUSTOM_TEXT_FIELD1", title: "Custom Text Field1", width: "180px", hidden: true },
        { field: "CUSTOM_TEXT_FIELD2", title: "Custom Text Field2", width: "180px", hidden: true },
        { field: "CUSTOM_TEXT_FIELD3", title: "Custom Text Field3", width: "180px", hidden: true },
          { field: "CREATED_USER", title: "Created User", width: "180px", hidden: true },
         { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true },
         { field: "MODIFIED_USER", title: "Modified User", width: "180px", hidden: true },
         { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true },
        { field: "PUBLISH", title: "Publish", width: "180px", hidden: true },
        { field: "PUBLISH2PRINT", title: "Publish To Print", width: "180px", hidden: true },
        { field: "PUBLISH2CD", title: "Publish To CD", width: "180px", hidden: true },
        { field: "WORKFLOW_STATUS", title: "Workflow Status", width: "180px", hidden: true },
        { field: "WORKFLOW_COMMENTS", title: "Workflow Comments", width: "180px", hidden: true },
        { field: "CLONE_LOCK", title: "Clone Lock", width: "180px", hidden: true },
        { field: "IS_CLONE", title: "IS Clone", width: "180px", hidden: true }
    ];

    // ---------------------------------------------------End Category Search Results--------------------------------------------------------------------------------

    // ---------------------------------------------------Start Family Search Results--------------------------------------------------------------------------------
    //search1





    $scope.GetFamilySearchGridresultsdataSource = new kendo.data.DataSource({
        pageSize: 100,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        //messages: {
        //    itemsPerPage: ''
        //},
        //sort: { field: "CATEGORY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                //  alert('GetFamilySearchResults');

                if ($scope.seletedAttribute == "") {
                    $scope.seletedAttribute = "All";
                }

                dataFactory.GetFamilySearchResults($scope.searchtxt, $scope.searchid, $scope.usewildcards, $scope.catalogIdAttr, $scope.seletedAttribute, $scope.CategoryID, $scope.searchEndtxt).success(function (response) {
                    if (response.length != 0) {


                        if (response[0].ATTRIBUTE_TYPE == 12) {
                            for (var i = 0 ; i < response.length ; i++) {

                                response[i].STRING_VALUE = response[i].NUMERIC_VALUE;
                            }
                        }



                        $("#divFamilySearch").show();
                        $scope.isFamilySearchResultsNull = false;
                        $scope.searchEmptyResult = false;
                        // $scope.searchResultsTitle = "for " + ' value "' + $scope.searchtxt + '"';
                    }
                    else {
                        $scope.isFamilySearchResultsNull = true;
                        $scope.searchEmptyResult = true;
                        // $scope.searchResultsTitle = "";
                    }
                    $scope.PageTotal = response.length;
                    $scope.TotalPerPage = 'Found ' + $scope.PageTotal + ' Items';
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });

            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    CATALOG_ID: { editable: false }, CATALOG_NAME: { editable: false }, CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false }, FAMILY_ID: { editable: false }, FAMILY_NAME: { editable: false },
                    STATUS: { editable: false }, ATTRIBUTE_ID: { editable: false }, ATTRIBUTE_NAME: { editable: false },
                    STRING_VALUE: { editable: false }, ATTRIBUTE_TYPE: { editable: false }, NUMERIC_VALUE: { editable: false },
                    OBJECT_NAME: { editable: false }, OBJECT_TYPE: { editable: false }, CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" }, MODIFIED_USER: { editable: false }, MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '',
                    type: "error"
                });
            }
        },
        change: function (e) {
            if ($scope.isFamilySearchResultsNull)
                $("#divFamilySearch").hide();
        }
    });
    $("#FamilySearchResult").kendoTooltip({
        filter: "td:nth-child(2)", //this filter selects the second column's cells
        position: "right",
        content: function (e) {
            var dataItem = $("#FamilySearchResult").data("kendoGrid").dataItem(e.target.closest("tr"));
            var content = dataItem.CATALOG_NAME + "->";
            if (dataItem.SUBCATNAME_L1 != '_' && dataItem.SUBCATNAME_L1 != null && dataItem.SUBCATNAME_L1 != '-' && dataItem.SUBCATNAME_L1 != '') {
                content = content + dataItem.SUBCATNAME_L1 + "->";
            }
            //if (dataItem.SUBCATNAME_L2 != '_' && dataItem.SUBCATNAME_L2 != null && dataItem.SUBCATNAME_L2 != '-' && dataItem.SUBCATNAME_L2 != '') {
            //    content = content + dataItem.SUBCATNAME_L2;
            //}
            //if (dataItem.SUBCATNAME_L3 != '_' && dataItem.SUBCATNAME_L3 != null && dataItem.SUBCATNAME_L3 != '-' && dataItem.SUBCATNAME_L3 != ' ') {
            //    content = content  + dataItem.SUBCATNAME_L3;
            //}
            content = content + dataItem.CATEGORY_NAME + '->' + dataItem.FAMILY_NAME;
            return content;
        }
    }).data("kendoTooltip");

    $scope.familySearchGridresultsColumns = [
         { field: "STRING_VALUE", title: "Searched Value", template: "<a title='Product Name' class='k-link'  href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=STRING_VALUE#</a>", width: "180px" },
           { field: "FAMILY_NAME", title: "Product Name", template: "<a title='Product Name' class='k-link'  href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=FAMILY_NAME#</a>", width: "180px" },
         { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
         { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
         { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "180px", hidden: true },
         //{ field: "STRING_VALUE", title: "Searched Value", width: "180px" },
         { field: "NUMERIC_VALUE", title: "Numeric Value", width: "180px", hidden: true },
         { field: "STATUS", title: "Status", width: "180px", hidden: true },
         { field: "CREATED_USER", title: "Created User", width: "180px", hidden: true },
         { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true },
         { field: "MODIFIED_USER", title: "Modified User", width: "180px", hidden: true },
         { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true }
    ];


    $scope.SelectItem = function (e) {

        if (e == undefined) {
            $scope.usewildcards = 0;
        }

        $scope.searchEndtxt = '';
        $scope.usewildcards = e;
        if ($scope.usewildcards == "4" || $scope.usewildcards == "12") {
            $scope.searchtxt = "Empty";
            $scope.searchValue = "Empty";
            document.getElementById("mainCatalogSearch").readOnly = true;
        }
        else if ($scope.usewildcards == "5" || $scope.usewildcards == "13") {
            $scope.searchtxt = "Exists";
            $scope.searchValue = "Exists";
            document.getElementById("mainCatalogSearch").readOnly = true;
        }
        else {
            $scope.searchValue = "Search Value";
            $scope.searchtxt = "";
            document.getElementById("mainCatalogSearch").readOnly = false;
        }

        if ($scope.Filterdatatype == "Date") {
            if ($scope.usewildcards == "14") {

                $scope.hidenValue = false;
                $scope.endDateHide = true;
                $scope.hidenSearchText = false;
                $scope.numericTextHiden = false;
            }
            else if ($scope.usewildcards != undefined) {
                $scope.hidenValue = true;
                $scope.endDateHide = false;
                $scope.hidenSearchText = false;
                $scope.numericTextHiden = false;
            }
            else {
                $scope.hidenSearchText = true;
                $scope.hidenValue = false;
                $scope.endDateHide = false;
                $scope.numericTextHiden = false;
            }
        }
        else if ($scope.Filterdatatype == "Number") {
            if ($scope.usewildcards == "14") {

                $scope.hidenValue = false;
                $scope.endDateHide = false;
                $scope.hidenSearchText = false;
                $scope.numericTextHiden = true
            }
            else if ($scope.usewildcards != undefined) {
                $scope.hidenValue = false;
                $scope.endDateHide = false;
                $scope.hidenSearchText = true;
                $scope.numericTextHiden = false;
            }
            else {
                $scope.hidenSearchText = true;
                $scope.hidenValue = false;
                $scope.endDateHide = false;
                $scope.numericTextHiden = false;
            }
        }
        else {
            $scope.hidenSearchText = true;
            $scope.hidenValue = false;
            $scope.endDateHide = false;
            $scope.numericTextHiden = false;
        }
    }
    // ---------------------------------------------------End Family Search Results--------------------------------------------------------------------------------
    // ---------------------------------------------------Start Product Search Results--------------------------------------------------------------------------------
    function clearFiter() {
        $("form.k-filter-menu button[type='reset']").trigger("click");
    }
    $scope.GetProductSearchGridresultsdataSource = new kendo.data.DataSource({
        pageSize: 100,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        //messages: {
        //    itemsPerPage: ''
        //},
        //sort: { field: "CATEGORY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                if ($scope.seletedAttribute == "") {
                    $scope.seletedAttribute = "All";
                }

                var seletedAttribute = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.seletedAttribute), key,
{ keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
                dataFactory.GetProductSearchResults($scope.searchtxt, $scope.searchid, $scope.usewildcards, $scope.catalogIdAttr, seletedAttribute, $scope.CategoryID, $scope.searchEndtxt).success(function (response) {
                    if (response.length != 0) {

                        $("#divProductSearch").show();
                        $scope.searchEmptyResult = false;
                        $scope.isProductSearchResultsNull = false;
                        //$scope.searchResultsTitle = "for " + ' value "' + $scope.searchtxt + '"';
                    }
                    else {
                        $scope.searchEmptyResult = true;
                        $scope.isProductSearchResultsNull = true;
                        //$scope.searchResultsTitle = "";
                    }
                    $scope.PageTotal = response.length;
                    $scope.TotalPerPage = 'Found ' + $scope.PageTotal + ' Items';
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    STRING_VALUE: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    PRODUCT_ID: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    OBJECT_TYPE: { editable: false },
                    OBJECT_NAME: { editable: false },
                    FAMILY_ID: { editable: false },
                    FAMILY_NAME: { editable: false },
                    SUBFAMILY_NAME: { editable: false },
                    CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    SUBCATNAME_L1: { editable: false },
                    SUBCATNAME_L2: { editable: false },
                    SUBCATNAME_L3: { editable: false },
                    CATALOG_ID: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" },
                    MODIFIED_USER: { editable: false },
                    MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '',
                    type: "error"
                });
            }

        },
        change: function (e) {
            if ($scope.isProductSearchResultsNull)
                $("#divProductSearch").hide();
        }
    });

    $scope.GetSubProductSearchGridresultsdataSource = new kendo.data.DataSource({
        pageSize: 100,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        messages: {
            itemsPerPage: ''
        },
        // sort: { field: "CATEGORY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                if ($scope.seletedAttribute == "") {
                    $scope.seletedAttribute = "All";
                }
                if ($scope.searchtxt != "" && $scope.searchtxt != null && $scope.searchtxt != undefined)
                    $rootScope.searchtxt = $scope.searchtxt;
                else if ($rootScope.searchtxt != "" && $rootScope.searchtxt != null && $rootScope.searchtxt != undefined)
                    $scope.searchtxt = $rootScope.searchtxt;
                dataFactory.GetSubProductSearchResults($scope.searchtxt, $scope.searchid, $scope.usewildcards, $scope.catalogIdAttr, $scope.seletedAttribute, $scope.CategoryID, $scope.searchEndtxt).success(function (response) {
                    if (response.length != 0) {
                        $scope.searchEmptyResult = false;
                        $scope.isSubProductSearchResultsNull = false;
                        $("#divSubProductSearch").show();
                        //$scope.searchResultsTitle = "for " + ' value "' + $scope.searchtxt + '"';
                    }
                    else {
                        $scope.searchEmptyResult = true;
                        $scope.isSubProductSearchResultsNull = true;
                        // $scope.searchResultsTitle = "";
                    }
                    $scope.PageTotal = response.length;
                    $scope.TotalPerPage = 'Found ' + $scope.PageTotal + ' Items';
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    STRING_VALUE: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    PRODUCT_ID: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    OBJECT_TYPE: { editable: false },
                    OBJECT_NAME: { editable: false },
                    FAMILY_ID: { editable: false },
                    FAMILY_NAME: { editable: false },
                    SUBFAMILY_NAME: { editable: false },
                    CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    SUBCATNAME_L1: { editable: false },
                    SUBCATNAME_L2: { editable: false },
                    SUBCATNAME_L3: { editable: false },
                    CATALOG_ID: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" },
                    MODIFIED_USER: { editable: false },
                    MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '',
                    type: "error"
                });
            }

        },
        change: function (e) {
            if ($scope.isSubProductSearchResultsNull)
                $("#divSubProductSearch").hide();
        }
    });
    $(document).ready(function () {
        $scope.title = "I 'm a tooltip!";
    });
    $("#productSearchResult").kendoTooltip({
        filter: "td:nth-child(2)", //this filter selects the second column's cells
        position: "right",
        content: function (e) {
            var dataItem = $("#productSearchResult").data("kendoGrid").dataItem(e.target.closest("tr"));
            var content = dataItem.CATALOG_NAME + "->";
            if (dataItem.SUBCATNAME_L1 != '_' && dataItem.SUBCATNAME_L1 != null && dataItem.SUBCATNAME_L1 != '-' && dataItem.SUBCATNAME_L1 != '') {
                content = content + dataItem.SUBCATNAME_L1 + "->";
            }
            //if (dataItem.SUBCATNAME_L2 != '_' && dataItem.SUBCATNAME_L2 != null && dataItem.SUBCATNAME_L2 != '-' && dataItem.SUBCATNAME_L2 != '') {
            //    content = content + "->" + dataItem.SUBCATNAME_L2;
            //}
            //if (dataItem.SUBCATNAME_L3 != '_' && dataItem.SUBCATNAME_L3 != null && dataItem.SUBCATNAME_L3 != '-' && dataItem.SUBCATNAME_L3 != ' ') {
            //    content = content + "->"  + dataItem.SUBCATNAME_L3;
            //}
            content = content + dataItem.CATEGORY_NAME + '->' + dataItem.FAMILY_NAME + '->' + dataItem.ITEM_NO;
            return content;
        }
    }).data("kendoTooltip");
    $scope.productSearchGridresultsColumns = [
        { field: "STRING_VALUE", title: "Searched Value", template: "<a class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=STRING_VALUE#</a>", width: "180px" },
          { field: "ITEM_NO", title: $localStorage.CatalogItemNumber, template: "<a title='Product Specification' class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=ITEM_NO#</a>", width: "180px", hidden: false },
        { field: "FAMILY_NAME", title: "Product Name", template: "<a title=#=FAMILY_NAME# class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=FAMILY_NAME#</a>", width: "180px" },
         { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
        { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
        { field: "SUBCATNAME_L1", title: "Category Name", width: "180px", hidden: true },
        { field: "SUBCATNAME_L2", title: "SubCatName L2", width: "180px", hidden: true },
        { field: "SUBCATNAME_L3", title: "SubCatName L3", width: "180px", hidden: true },
        { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "180px", hidden: true },
         { field: "NUMERIC_VALUE", title: "Numeric Value", width: "180px", hidden: true },
         { field: "STATUS", title: "Status", width: "180px", hidden: true },
         { field: "CREATED_USER", title: "Created User1", width: "180px", hidden: true },
         { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true },
         { field: "MODIFIED_USER", title: "Modified User", width: "180px", hidden: true },
         { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true }
    ];
    $("#SubproductSearchResult").kendoTooltip({
        filter: "td:nth-child(2)", //this filter selects the second column's cells
        position: "right",
        content: function (e) {
            var dataItem = $("#SubproductSearchResult").data("kendoGrid").dataItem(e.target.closest("tr"));
            var content = dataItem.CATALOG_NAME + "->";
            if (dataItem.SUBCATNAME_L1 != '_' && dataItem.SUBCATNAME_L1 != null && dataItem.SUBCATNAME_L1 != '-' && dataItem.SUBCATNAME_L1 != '') {
                content = content + dataItem.SUBCATNAME_L1 + "->";
            }
            //if (dataItem.SUBCATNAME_L2 != '_' && dataItem.SUBCATNAME_L2 != null && dataItem.SUBCATNAME_L2 != '-' && dataItem.SUBCATNAME_L2 != '') {
            //    content = content + "->" + dataItem.SUBCATNAME_L2;
            //}
            //if (dataItem.SUBCATNAME_L3 != '_' && dataItem.SUBCATNAME_L3 != null && dataItem.SUBCATNAME_L3 != '-' && dataItem.SUBCATNAME_L3 != ' ') {
            //    content = content + "->"  + dataItem.SUBCATNAME_L3;
            //}
            content = content + dataItem.CATEGORY_NAME + '->' + dataItem.FAMILY_NAME + '->' + dataItem.CATALOG_ITEM_NO + '->' + dataItem.ITEM_NO;
            return content;
        }
    }).data("kendoTooltip");
    $scope.subproductSearchGridresultsColumns = [
        { field: "STRING_VALUE", title: "Searched Value", template: "<a title='SubProducts Specification' class='k-link' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=STRING_VALUE#</a>", width: "180px" },
        { field: "ITEM_NO", title: "SubItem#", template: "<a title='SubProducts Specification' class='k-link' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=ITEM_NO#</a>", width: "180px", hidden: false },
       { field: "CATALOG_ITEM_NO", title: "Item#", template: "<a class='k-link' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=CATALOG_ITEM_NO#</a>", width: "180px", hidden: false },
         { field: "FAMILY_NAME", title: "Product Name", template: "<a title='Product Name' class='k-link' href='javascript:void(0);'ng-click='ngclkSubProductid(this)'>#=FAMILY_NAME#</a>", width: "180px" },
           { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);'ng-click='ngclkSubProductid(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
        { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
         { field: "SUBCATNAME_L1", title: "Category Name", width: "180px", hidden: true },
        { field: "SUBCATNAME_L2", title: "SubCatName L2", width: "180px", hidden: true },
        { field: "SUBCATNAME_L3", title: "SubCatName L3", width: "180px", hidden: true },
        { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "180px", hidden: true },

        { field: "NUMERIC_VALUE", title: "Numeric Value", width: "180px", hidden: true },
        { field: "STATUS", title: "Status", width: "180px", hidden: true },
        { field: "CREATED_USER", title: "Created User", width: "180px", hidden: true },
        { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true },
        { field: "MODIFIED_USER", title: "Modified User", width: "180px", hidden: true },
        { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }, hidden: true }
    ];

    // ---------------------------------------------------End Product Search Results--------------------------------------------------------------------------------


    $rootScope.dropDownCategorySearchDatasource = new kendo.data.HierarchicalDataSource({
        type: "json",
        sort: { field: "SORT_ORDER", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetCategoriesForSearch($scope.catalogIdAttr, options.data.id).success(function (response) {
                    if ($scope.categorydetails == true || $scope.catalogIdAttr == 0) {

                        //$scope.dataItemCat = response;
                        //for (catId = 0; CatID <= $scope.dataItemCat.length; CatID++)
                        //{
                        //    $scope.dataItemCat.splice(catId, 1);
                        //}

                        $("#SearchCategoryTree").addClass("disabledbutton");

                    }
                    else {

                        $("#SearchCategoryTree").removeClass("disabledbutton");


                    }
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        }
    });

    //if ($("#SearchCategoryTree").val() !== undefined) {
    //    $rootScope.SearchCategoryTree = $("#SearchCategoryTree").kendoExtDropDownTreeView({
    //        treeview: {
    //            dataSource: $rootScope.dropDownCategorySearchDatasource,
    //            dataTextField: "CATEGORY_NAME",
    //            dataValueField: "id",
    //            loadOnDemand: true,
    //            select: onSelect,


    //        }
    //    }).data("kendoExtDropDownTreeView");
    //    //   $rootScope.dropDownParnetCategoryTreeView.treeview().expand(".k-item");
    //}
    //function onDataBound(e) {
    //    //var tree = $("#SearchCategoryTree").data("kendoExtDropDownTreeView");
    //    //var firstNode = $("#SearchCategoryTree").find('.k-first');
    //    //tree.select(firstNode);
    //    //tree.select(firstNode);
    //    //var dataItem = this.dataItem(e.sender.root.children()[0]);
    //    //if ($scope.catalogIDD == 0)
    //    //{
    //    //    $('#SearchCategoryTree').children()[0].children[0].children[0].innerText = "";

    //    //    return null;
    //    //}
    //    var treeview = $("#SearchCategoryTree").data("kendoExtDropDownTreeView");
    //    treeview.treeview().findByUid($("#SearchCategoryTree").find('li')[0].attributes[2].value);
    //    var selectitem = treeview.treeview().findByUid($("#SearchCategoryTree").find('li')[0].attributes[2].value);
    //    treeview.treeview().select(selectitem);
    //    $('#SearchCategoryTree').children()[0].children[0].children[0].attributes[0].value = "off"
    //    $('#SearchCategoryTree').children()[0].children[0].children[0].innerText = selectitem[0].outerText;
    //    $scope.CategoryID = treeview.treeview().dataSource._data[0].id;

    //}

    if ($("#SearchCategoryTree").val() !== undefined) {

        $rootScope.SearchCategoryTree = $("#SearchCategoryTree").kendoExtDropDownTreeView({
            treeview: {
                dataSource: $rootScope.dropDownCategorySearchDatasource,
                dataTextField: "CATEGORY_NAME",
                dataValueField: "id",
                loadOnDemand: true,
                select: onSelect,
                selectable: true,
                dataBound: onDataBound
            }

        }).data("kendoExtDropDownTreeView");


        //var selected;
        //var treeView = $rootScope.SearchCategoryTree;
        ////treeView.expand($treePath);

        //// Gotta make both calls...
        //treeView.select(selected);

        //treeView.trigger('select', { node: selected });

        //   $rootScope.dropDownParnetCategoryTreeView.treeview().expand(".k-item");
    }
    //**Start DateTime Picker **//
    $(document).ready(function () {
        var startDate = angular.element(document.getElementById("start"));
        var endDate = angular.element(document.getElementById(""));



        function startChange() {
            var startDate = start.value(),
            endDate = end.value();

            if (startDate) {
                startDate = new Date(startDate);
                startDate.setDate(startDate.getDate());
                end.min(startDate);
            } else if (endDate) {
                start.max(new Date(endDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
                //$scope.searchEndtxt = '';
                //$scope.searchtxt = '';
                //startDate = null;
                //endDate = null;
            }
        }

        function endChange() {
            var endDate = end.value(),
            startDate = start.value();

            if (endDate) {
                endDate = new Date(endDate);
                endDate.setDate(endDate.getDate());
                start.max(endDate);
            } else if (startDate) {
                end.min(new Date(startDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
                //$scope.searchEndtxt = '';
                //$scope.searchtxt = '';
                //startDate = null;
                //endDate = null;
            }
        }

        var start = $("#start").kendoDatePicker({
            change: startChange,
            format: "dd/MM/yyyy"

        }).data("kendoDatePicker");

        var end = $("#end").kendoDatePicker({
            change: endChange,
            format: "dd/MM/yyyy"

        }).data("kendoDatePicker");
        document.getElementById("end").readOnly = true;
        document.getElementById("start").readOnly = true;


        //start.max(end.value());
        //end.min(start.value());
    });
    //**End With DateTimePicker **//




    $(document).ready(function () {
        var Date = $("#Date").kendoDatePicker({
            //change: DateChange

            dateInput: true,
            format: "dd/MM/yyyy"

        }).data("kendoDatePicker");
        document.getElementById("Date").readOnly = true;



    });

    function onSelect(e) {
        var dataItem = this.dataItem(e.sender._clickTarget);
        $scope.CategoryID = dataItem.id;
    };



    $scope.init = function () {

        $("#divItemSearch").hide();
        $("#divCategorySearch").hide();
        $("#divFamilySearch").hide();
        $("#divProductSearch").hide();
        $("#divSubProductSearch").hide();
        $("#familyEditor").hide();
        $("#searchtab").show();
        $("#resultbtntab").hide();
        $("#dynamictable").hide();
        $("#divSubItemSearch").hide();
        $scope.productData = [];
        $rootScope.SubProdInvert = false;
        $rootScope.btnSubProdInvert = false;
        $rootScope.invertedproductsshow = false;
        $rootScope.invertedproductsbutton = false;
        $rootScope.invertedproductsbutton1 = false;



    };
    $scope.IsAttribute = false;


    $scope.callSearchType = function (e) {
        //
        // alert(e);
        $scope.clearText();
        $scope.SearchTypeName = e;
        if ($scope.SearchTypeName != 'Category') {
            let searchText = document.getElementById("mainCatalogSearch").value;
            if (searchText == "") {
                $scope.searchEmptyResult = true;
            }
            $scope.IsAttribute = false;
            $scope.categorydetails = false;
            $("#SearchCategoryTree").removeClass("disabledbutton");
            $("#drp").removeClass("disabledbutton");
            $rootScope.dropDownCategorySearchDatasource.read();

            //$scope.Filterdatatype = "Text"
            //$scope.usewildcards = 0
            //  $scope.getFilterType()
            //$scope.GetFilters.read();
            //$scope.SelectItem();
            $scope.catalogIdAttrDataSource.read();
        }
        else {
            let searchText = document.getElementById("mainCatalogSearch").value;
            if (searchText == "")
            {
                $scope.searchEmptyResult = true;
            }
            var grid = $("#page");
            grid.find(".k-grid-pager").insertBefore(grid.children(".k-grid-header-wrap"));
            $("#page").find(".k-grid-pager").appendTo.apply(grid.children(".k-grid-content"));
            //$("#drp").addClass("disabledbutton");
            $scope.IsAttribute = false;
            $scope.seletedAttribute = "";
            $scope.categorydetails = true;
            $rootScope.dropDownCategorySearchDatasource.read();

            //$scope.Filterdatatype = "Text"
            //$scope.usewildcards = 0
            //$scope.getFilterType()
            //$scope.GetFilters.read();
            //  $scope.SelectItem();
            $scope.catalogIdAttrDataSource.read();
        }
    }
    function categoryBind() {

        $rootScope.SearchCategoryTree = $("#SearchCategoryTree").kendoExtDropDownTreeView({
            treeview: {
                dataSource: $rootScope.dropDownCategorySearchDatasource,
                dataTextField: "CATEGORY_NAME",
                dataValueField: "id",
                loadOnDemand: true,
                select: onSelect,
                selectable: true,
                dataBound: onDataBound
            }
        }).data("kendoExtDropDownTreeView");
    };
    var i = 0;
    function onDataBound(e) {
        //var tree = $("#SearchCategoryTree").data("kendoExtDropDownTreeView");
        //var firstNode = $("#SearchCategoryTree").find('.k-first');
        //tree.select(firstNode);
        //tree.select(firstNode);
        //var dataItem = this.dataItem(e.sender.root.children()[0]);

        //if ($scope.categorydetails == true) {
        //  $('#SearchCategoryTree').children()[0].children[0].children[0].innerText = "All";

        // return null;
        //  }
        var treeview = $("#SearchCategoryTree").data("kendoExtDropDownTreeView");
        treeview.treeview().findByUid($("#SearchCategoryTree").find('li')[0].attributes[2].value);
        var selectitem = treeview.treeview().findByUid($("#SearchCategoryTree").find('li')[0].attributes[2].value);
        treeview.treeview().select(selectitem);

        $('#SearchCategoryTree').children()[0].children[0].children[0].attributes[0].value = "off";
        if (selectitem[0].outerText == undefined) {
            $('#SearchCategoryTree').children()[0].children[0].children[0].innerText = "All";
            $scope.CategoryID = "";
        } else {
            $('#SearchCategoryTree').children()[0].children[0].children[0].innerText = selectitem[0].outerText;
            $scope.CategoryID = treeview.treeview().dataSource._data[0].id;
        }
    }



    $scope.onDataBoundprodgrd = function () {
        $scope.TotalPerPage = 'Found ' + $('#itemSearchResult').data("kendoGrid").dataSource._total + ' Items';
    }
    $scope.onDataBoundsubprodgrd = function () {
        $scope.TotalPerPage = 'Found ' + $('#subitemSearchResult').data("kendoGrid").dataSource._total + ' Items';
    }
    $scope.onDataBoundcatgrd = function () {
        $scope.TotalPerPage = 'Found ' + $('#page').data("kendoGrid").dataSource._total + ' Items';
    }

    $scope.onDataBoundfamgrd = function () {
        $scope.TotalPerPage = 'Found ' + $('#FamilySearchResult').data("kendoGrid").dataSource._total + ' Items';
    }
    $scope.onDataBoundProductgrd = function () {
        $scope.TotalPerPage = 'Found ' + $('#productSearchResult').data("kendoGrid").dataSource._total + ' Items';
    }

    $scope.onDataBoundSubProductgrd = function () {
        $scope.TotalPerPage = 'Found ' + $('#SubproductSearchResult').data("kendoGrid").dataSource._total + ' Items';
    }

    $scope.init();


    angular.element(document).ready(function () {
        //do it here
    });

    setTimeout(function () {


        var SelectedValue = sessionStorage.getItem("Dropdownvalue");

        $scope.productgrid = $localStorage.productgrid;
        $scope.subproductgrid = $localStorage.subproductgrid;
        $scope.invproductgrid = $localStorage.invproductgrid;
        $scope.subinvproductgrid = $localStorage.subinvproductgrid;

        if ($scope.productgrid == "True") {
            $scope.Selectedattributetypeforproduct = sessionStorage.getItem("Dropdownvalueforproductselectedattribute");
            $scope.seletedAttribute = $scope.Selectedattributetypeforproduct;
        }

        if ($scope.subproductgrid == "True") {
            $scope.Selectedattributetypeforsubproduct = sessionStorage.getItem("Dropdownvalueforsubproductselectedattribute");
            $scope.seletedAttribute = $scope.Selectedattributetypeforsubproduct;
        }

        if ($scope.invproductgrid == "True") {
            $scope.Selectedattributetypeforinvproduct = sessionStorage.getItem("Dropdownvalueforinvproductselectedattribute");
            $scope.seletedAttribute = $scope.Selectedattributetypeforinvproduct;
        }

        if ($scope.subinvproductgrid == "True") {
            $scope.Selectedattributetypeforinvsubproduct = sessionStorage.getItem("Dropdownvalueforsubinvproductselectedattribute");
            $scope.seletedAttribute = $scope.Selectedattributetypeforinvsubproduct;
        }

        $scope.SearchTypeName = SelectedValue;

        if (SelectedValue == "Category") {
            $scope.seletedAttribute = 'Category Name';
        }
        else if (SelectedValue == "Product") {
            $scope.seletedAttribute = 'Product Name';
        }
        else if (SelectedValue == "SubItems") {
            if ($scope.Selectedattributetype == $localStorage.CatalogSubItemNumber || $scope.Selectedattributetype == null && $scope.Selectedattributetypeforsubproduct == undefined && $scope.Selectedattributetypeforsubproduct == undefined) {
                $scope.seletedAttribute = 'SubItems';
            }

        }
        else {

            if ($scope.Selectedattributetype == $localStorage.CatalogItemNumber || $scope.Selectedattributetype == null && $scope.Selectedattributetypeforinvproduct == undefined && $scope.Selectedattributetypeforproduct == undefined) {
                $scope.seletedAttribute = $localStorage.CatalogItemNumber;
            }

        }
        var searchText = sessionStorage.getItem("searchText");
        $scope.callSearchType(SelectedValue);
        $scope.getFilterType()
        $scope.GetFilters.read();

        //  sessionStorage.removeItem("Dropdownvalue");


        setTimeout(function () {


            if (searchText != null && searchText != "") {
                $scope.searchtxt = searchText;

                $scope.btnsearchClick();
                sessionStorage.removeItem("searchText");
            }

        }, 3000);
    }, 3000);
    //  $scope.test();
}]);
