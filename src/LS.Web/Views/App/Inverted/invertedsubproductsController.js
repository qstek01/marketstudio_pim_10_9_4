﻿LSApp.controller('invertedsubproductsController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', '$localStorage',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope, $localStorage) {
        $scope.backsub = function () {
            $('#dynamictablesubproducts').show();
            $("#familyEditor").hide();
            $('#BackSubDiv').hide();
            $('#subassociatedgrid').hide();
        };

        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

        $scope.usewildcards = 4;
        $rootScope.Dashboards = true;
        $scope.click = function () {
            $scope.InvertedDataSubproducts();
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = false;
        };
        $scope.prodid = 0;
        $scope.subproductId = 0;
        $scope.selectedRow = [];
        function rebind() {
            $rootScope.InvertedsubprodDataBackup = [];
            $rootScope.InvertedsubprodDataBackup.push($scope.InvertedprodDataSub);
            $rootScope.InvertedsubprodDataBackup.push($scope.InvertedcolumnsSubproducts);
            $rootScope.InvertedsubprodDataBackup.push($scope.ProdCurrentPage);
            $rootScope.InvertedsubprodDataBackup.push($scope.ProdCountPerPage);
            $rootScope.InvertedsubprodDataBackup.push($scope.InvertSubProdBeforeSearch);
        }
        $scope.navigatesub = function (e) {

            $scope.prodid = e.MAINPRODUCT_ID;
            $scope.subproductId = e.SUBPRODUCT_ID;
            $scope.selectedRow = e;
            $scope.subproductassociatedfamilyGridDatasource.read();
        };
        $scope.currentPageCountSub = "";
        $scope.navigatesubclk = function (e) {
            rebind();
            $rootScope.clickSelectedItemsInverted(e.dataItem, false);
            var tabstripSP = $("#productgridtabstrip").data("kendoTabStrip");
            var myTabSP = tabstripSP.tabGroup.children("li").eq(1);
            tabstripSP.enable(myTabSP);
            tabstripSP.select(myTabSP);
            $('#dynamictablesubproducts').hide();
            $("#familyEditor").show();
            $('#BackSubDiv').show();
            $('#subassociatedgrid').hide();
            $rootScope.familyMainEditor = true;
            $rootScope.invertedproductsshow = false;
            $rootScope.invertedproductsbutton1 = false;
            $rootScope.btnSubProdInvert = true;
            $rootScope.SubProdInvert = true;
            $('#samplegridss').css('max-width', '1220px');
            $('#samplegridss tbody').css('max-width', '1220px');

            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#invertedproductssamplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1327px !important;");
                $("#invertedproductssamplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1327px !important;");
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#invertedproductssamplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
                $("#invertedproductssamplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#invertedproductssamplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#invertedproductssamplegridss tbody").css("cssText", "margin-left: -2px;width: 1480px !important;");

            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#invertedproductssamplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#invertedproductssamplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

            }

        };
        $scope.init = function () {
            $rootScope.familyMainEditor = false;
            $rootScope.invertedproductsshow = true;
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = false;
            //  $rootScope.btnSubProdInvert = false;
            //  $rootScope.SubProdInvert = false;
            $('#dynamictablesubproducts').hide();
            $('#BackSubDiv').hide();
            $('#subassociatedgrid').hide();
            $scope.gridCurrentPage = "10";
            //  $scope.InvertedData();
        };
        $scope.init();
        $scope.ProdCurrentPage = "1";
        // document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaging";
        $scope.ProdCountPerPage = "50";
        // $("#cboProdPageCount").data("kendoComboBox").value("1");

        $scope.callProductGridPagePerinverted = function (pageper) {
            if (pageper != null && pageper != "0") {
                // document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaging";
                $scope.ProdCountPerPage = pageper;
                $scope.ProdCurrentPage = "1";
                // $("#cboProdPageCount").data("kendoComboBox").value($scope.ProdCurrentPage);
                //document.getElementById("gridpaging" + pageper).className = "gridpaginghighlight";
                //$scope.Prodsubdata();
                if ($scope.InvertSubProductSearch != "" && $scope.InvertSubProductSearch != undefined) {
                    $scope.SearchSubProduct("pagingItems");
                } else {
                    $scope.Prodsubdata();
                }
            }
        };

        $scope.callProductGridPaging = function (pageno) {
            if (pageno != null && pageno <= $scope.totalProdPageCount && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {
                if (pageno == 'PREV' && $scope.ProdCurrentPage != 1) {
                    $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) - 1;
                } else if (pageno == 'PREV' && $scope.ProdCurrentPage == 1) {
                    $scope.ProdCurrentPage = "1";
                } else if (pageno == 'NEXT') {
                    $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) + 1;
                    if ($scope.ProdCurrentPage > $scope.totalProdPageCount) {
                        $scope.ProdCurrentPage = $scope.totalProdPageCount;
                    }
                } else {
                    $scope.ProdCurrentPage = pageno;
                }
                //  $("#cboProdPageCount").data("kendoComboBox").value($scope.ProdCurrentPage);
                if ($scope.InvertSubProductSearch != "" && $scope.InvertSubProductSearch != undefined) {
                    $scope.SearchSubProduct("paging");
                } else {
                    $scope.Prodsubdata();
                }
                //$scope.Prodsubdata();
            }
        };


        $scope.SelectAttribute = function (attrName) {
            $scope.selectedAttr = attrName;
        };

        //$scope.SearchSubProduct = function (searchText) {

        //    if (searchText == "") {
        //        $scope.InvertedData();
        //        $scope.InvertSubProductSearch = '';
        //        $('#txtSearchSub').removeClass();
        //        $('#txtSearchSub').addClass('clearable');
        //    } else {
        //        dataFactory.GetInvertedSearchProducts($rootScope.selecetedCatalogId, $scope.selectedAttr, searchText).success(function (response) {
        //            var obj = jQuery.parseJSON(response.Data.Data);
        //            $scope.InvertedprodData = obj;
        //            $scope.Invertedcolumns = response.Data.Columns;
        //            $scope.tblInvertedGrid.reload();
        //        }).error(function (error) {
        //            options.error(error);
        //        });
        //    }

        //};
        $scope.refreshSearch = function () {
            // $rootScope.currentPageCount = 1;
            $scope.InvertSubProductSearch = "";
            //$scope.ProdCountPerPage = "5";
            $scope.Prodsubdata();
            $scope.ResultEmpty = false;
        };
        $scope.InvertedprodData = [];
        var ProdCount = [];
        $scope.InvertSubProdBeforeSearch = [];
        $scope.unusedId = ["PRODUCT_ID",
            "CATALOG_ID",
            "CATALOG_NAME",
            "FAMILY_ID",
            "CATEGORY_ID",
            "MAINPRODUCT_ID",
            "SUBPRODUCT_ID",
            "id"
        ];
        $scope.opt = "TOTALCOUNTSUB";
        $scope.InvertedprodDataSub = [];
        $scope.Prodsubdata = function () {
            if ($rootScope.SubProdPageCount != undefined && $rootScope.SubProdPageCount != "" && $scope.currentPageCountSub === "") {
                $scope.ProdCountPerPage = $rootScope.SubProdPageCount;
                $scope.gridCurrentPage = $rootScope.SubProdPageCount;
            }

            dataFactory.GetInvertedProductsCount($rootScope.selecetedCatalogId, $scope.opt).success(function (response) {
                var obj = jQuery.parseJSON(response);
                $scope.InvertedprodData = obj.Data;
                $scope.Invertedcolumns = obj.Columns;
                var ProdEditPageloopcnt = Math.ceil(parseInt(obj) / parseInt($scope.ProdCountPerPage));
                ProdCount = [];
                for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                    ProdCount.push(i);
                }
                $scope.ProdPageCount = ProdCount;
                $scope.changeInvSubprod = 1;
                if ($scope.ProdCurrentPage == "1") {
                    $scope.changeInvSubprod = $scope.ProdCurrentPage;
                } else {
                    $scope.changeInvSubprod = $scope.ProdCurrentPage;
                }
                $scope.ProdCurrentPage = $scope.changeInvSubprod;
                $scope.totalProdPageCount = ProdEditPageloopcnt;
                if ($rootScope.SubProdPageCount != undefined && $rootScope.SubProdPageCount != "" && $scope.currentPageCountSub === "") {
                    $scope.ProdCountPerPage = $rootScope.SubProdPageCount;
                    $scope.gridCurrentPage = $rootScope.SubProdPageCount;
                }
                angular.forEach($scope.productData, function (value) {
                    value.SORT = parseFloat(value.PRODUCT_ID);
                });
                $scope.InvertedDataSubproducts();

            }).error(function (error) {
                options.error(error);
            });

        };

        function kendoDropDown() {
            $("#attributeNameSub").kendoDropDownList({
                dataTextField: "Caption",
                dataValueField: "Caption",
                dataSource: $scope.InvertSubProdBeforeSearch,
                height: 400
            });
        }
        $scope.ResultEmpty = false;
        $scope.keyPressed = function (keyEvent) {
            if (keyEvent.keyCode == 13) {
                $scope.SearchSubProduct($scope.InvertSubProductSearch);
            }
        };
        $scope.SearchSubProduct = function (searchText) {
            if (searchText == "") {
                $scope.InvertedDataSubproducts();
                $scope.InvertSubProductSearch = '';
                //$('#txtSearchSub').removeClass();
                // $('#txtSearchSub').addClass('clearable');
                $scope.ResultEmpty = false;
            } else {
                var searchAttr = "";
                if ($scope.selectedAttr.toLowerCase() == "SUBITEM#") {
                    searchAttr = "item#".toUpperCase();
                    $scope.InvertSubProdBeforeSearch.push(arrayText);

                } else {
                    searchAttr = $scope.selectedAttr;
                }
                var searchTxt;
                if (searchText == "paging" || searchText == "pagingItems") {
                    searchTxt = $scope.InvertSubProductSearch;

                } else {
                    searchTxt = searchText;
                    $scope.ProdCurrentPage = 1;
                }
                dataFactory.GetInvertedSearchSubProducts($rootScope.selecetedCatalogId, $scope.ProdCurrentPage, $scope.ProdCountPerPage, searchAttr, $scope.usewildcards, searchTxt).success(function (response) {

                    if (response.Data.length > 0) {
                        var obj = jQuery.parseJSON(response.Data[0].Data);
                        if (obj.length > 0) {

                            $scope.ResultEmpty = false;

                        } else {
                            $scope.ResultEmpty = true;
                        }
                        $scope.InvertedprodDataSub = obj;
                        $scope.InvertedcolumnsSubproducts = response.Data[0].Columns;
                        if (searchText != "paging") {
                            var objTotal = jQuery.parseJSON(response.Data[1].Data);
                            var ProdEditPageloopcnt = Math.ceil(parseInt(objTotal[0].COUNT) / parseInt($scope.ProdCountPerPage));
                            ProdCount = [];
                            for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                                ProdCount.push(i);
                            }
                            $scope.ProdPageCount = ProdCount;
                            $scope.changeInvSubprod = 1;
                            if ($scope.ProdCurrentPage == "1") {
                                $scope.changeInvSubprod = $scope.ProdCurrentPage;
                            } else {
                                $scope.changeInvSubprod = $scope.ProdCurrentPage;
                            }
                            $scope.ProdCurrentPage = $scope.changeInvSubprod;
                            $scope.totalProdPageCount = ProdEditPageloopcnt;
                            if ($rootScope.SubProdPageCount != undefined && $rootScope.SubProdPageCount != "" && $scope.currentPageCountSub === "") {
                                $scope.ProdCountPerPage = $rootScope.SubProdPageCount;
                                $scope.gridCurrentPage = $rootScope.SubProdPageCount;
                            }
                            angular.forEach($scope.productData, function (value) {
                                value.SORT = parseFloat(value.PRODUCT_ID);
                            });
                            rebind();
                        }
                    }
                    $scope.tblInvertedGridSubproducts.reload();
                }).error(function (error) {
                    options.error(error);
                });
            }

        };
        $scope.ResultEmptyFirst = false;
        $scope.InvertedDataSubproducts = function () {
            $scope.InvertSubProdBeforeSearch = [];
            $('#dynamictablesubproducts').show();
            if ($scope.ProdCurrentPage == null) {
                $scope.ProdCurrentPage = "1";
            }
            dataFactory.GetInvertedsubProducts($rootScope.selecetedCatalogId, $scope.ProdCurrentPage, $scope.ProdCountPerPage).success(function (response) {
                if (response.Data.Columns.length > 0) {
                    $scope.ResultEmptyFirst = false;
                    var obj = jQuery.parseJSON(response.Data.Data);

                    $rootScope.LoadSubProductGrid = true;
                    $scope.InvertedprodDataSub = obj;
                    $scope.InvertedcolumnsSubproducts = response.Data.Columns;
                    //angular.forEach($scope.productData, function (value) {
                    //    value.SORT = parseFloat(value.SUBPRODUCT_ID);
                    //});


                    $rootScope.SubProdPageCount = $scope.ProdCountPerPage;
                    $scope.currentPageCountSub = $scope.ProdCountPerPage;
                    $scope.tblInvertedGridSubproducts.reload();
                    for (var i = 0; i < $scope.InvertedcolumnsSubproducts.length; i++) {
                        var index = $scope.unusedId.indexOf($scope.InvertedcolumnsSubproducts[i].Caption);
                        if (index == -1) {
                            if ($scope.InvertedcolumnsSubproducts.indexOf($scope.InvertedcolumnsSubproducts[i].Caption) == -1) {
                                this.arrayText = {
                                    Caption: $localStorage.CatalogSubItemNumber,
                                    ColumnName: 'SUBITEM#',
                                };
                                if ($scope.InvertedcolumnsSubproducts[i].Caption.toLowerCase() == "item#") {
                                    $scope.InvertSubProdBeforeSearch.push(arrayText);

                                } else {
                                    $scope.InvertSubProdBeforeSearch.push($scope.InvertedcolumnsSubproducts[i]);
                                }
                            }

                        }
                    }

                    rebind();
                    $("#SubPagingTop").show();
                    $("#SubPagingBottom").show();

                    kendoDropDown();
                    if ($rootScope.btnSubProdInvert) {

                        var tabstrip = $("#tabstripInvertedProducts").data("kendoTabStrip");
                        var myTab = tabstrip.tabGroup.children("li").eq(1);
                        tabstrip.select(myTab);
                        $rootScope.btnSubProdInvert = false;
                        $rootScope.SubProdInvert = false;
                    }
                } else {
                    $scope.ResultEmptyFirst = true;
                }
            }).error(function (error) {
                options.error(error);
            });
        };
        $("#tabstripInvertedProducts").kendoTabStrip({
            read: function () {
                if ($rootScope.btnSubProdInvert) {
                    ;
                    var tabstrip = $("#tabstripInvertedProducts").data("kendoTabStrip");
                    var myTab = tabstrip.tabGroup.children("li").eq(1);
                    tabstrip.select(myTab);
                    $rootScope.btnSubProdInvert = false;
                    $rootScope.SubProdInvert = false;
                }
            }
        });
        $scope.tblInvertedGridSubproducts = new ngTableParams({ page: 1, count: 1000 },
            {
                counts: [],
                autoBind: true,
                total: function () { return $scope.InvertedprodDataSub.length; },
                $scope: { $data: {} },
                getData: function ($defer, params) {
                    var orderedData = $scope.InvertedprodDataSub;
                    // var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) :
                    // filteredData;
                    //  var orderedData = filteredData;
                    params.total(orderedData.length);
                    return $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });



        $scope.orderFields = "SORT";
        // $scope.Prodsubdata();



        $scope.subproductassociatedfamilyGridDatasource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.SubProdFamilyAssociatedCategory($scope.prodid, $scope.subproductId, $rootScope.selecetedCatalogId).success(function (response) {
                        options.success(response);
                        if ($scope.subproductassociatedfamilyGridDatasource._data.length > 0) {
                            $('#dynamictablesubproducts').hide();
                            $("#familyEditor").hide();
                            $('#BackSubDiv').show();
                            $('#subassociatedgrid').show();
                            $rootScope.familyMainEditor = false;
                            $rootScope.invertedproductsshow = true;
                            $rootScope.invertedproductsbutton = true;
                            $rootScope.invertedproductsbutton1 = false;
                        } else {
                            $rootScope.clickSelectedItemsInverted($scope.selectedRow, false);
                            var tabstripSP = $("#productgridtabstrip").data("kendoTabStrip");
                            var myTabSP = tabstripSP.tabGroup.children("li").eq(1);
                            tabstripSP.enable(myTabSP);
                            tabstripSP.select(myTabSP);
                            $('#dynamictablesubproducts').hide();
                            $("#familyEditor").show();
                            $('#BackSubDiv').show();
                            $('#subassociatedgrid').hide();
                            $rootScope.familyMainEditor = false;
                            $rootScope.invertedproductsshow = true;
                            $rootScope.invertedproductsbutton = true;
                            $rootScope.invertedproductsbutton1 = false;
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, schema: {
                model: {
                    fields: {
                        CATEGORY_ID: { editable: false },
                        CATALOG_NAME: { editable: false },
                        CATEGORY_NAME: { editable: false },
                        PRODUCT_ID: { editable: false }
                    }
                }
            }
        });



        $scope.subproductassociatedfamilyGridOptions = {
            dataSource: $scope.subproductassociatedfamilyGridDatasource,
            autoBind: false, selectable: true,
            columns: [
                { field: "SUBPRODUCTITEMNO", title: $localStorage.CatalogSubItemNumber, width: "180px", template: "<a title='SubItem No'  class='k-link' href='javascript:void(0);'  ng-click='navigatesubclk(this)'>#=SUBPRODUCTITEMNO#</a>" },
                { field: "STRING_VALUE", title: $localStorage.CatalogItemNumber, width: "180px", template: "<a title='Catalog Item Number'  class='k-link' href='javascript:void(0);'  ng-click='navigatesubclk(this)'>#=STRING_VALUE#</a>" },
                { field: "FAMILY_NAME", title: "Family Name", width: "180px", template: "<a title='Family Name'  class='k-link' href='javascript:void(0);'  ng-click='navigatesubclk(this)'>#=FAMILY_NAME#</a>" },
                { field: "CATEGORY_NAME", title: "Category Name", width: "180px", template: "<a title='Category Name'  class='k-link' href='javascript:void(0);'  ng-click='navigatesubclk(this)'>#=CATEGORY_NAME#</a>" },
                { field: "CATALOG_NAME", title: "Catalog Name", width: "180px", template: "<a title='Catalog Name'  class='k-link' href='javascript:void(0);'  ng-click='navigatesubclk(this)'>#=CATALOG_NAME#</a>" }
            ],

            editable: false
        };
        if ($rootScope.invertedBackTosubProduct == true) {
            if ($rootScope.InvertedsubprodDataBackup.length == 5) {
                $('#dynamictablesubproducts').show();
                $rootScope.invertedBackTosubProduct == false;
                $scope.InvertedprodDataSub = $rootScope.InvertedsubprodDataBackup[0];
                $scope.InvertedcolumnsSubproducts = $rootScope.InvertedsubprodDataBackup[1];
                $scope.ProdCurrentPage = $rootScope.InvertedsubprodDataBackup[2];
                $scope.ProdCountPerPage = $rootScope.InvertedsubprodDataBackup[3];
                $scope.gridCurrentPage = parseInt($scope.ProdCountPerPage);
                $scope.InvertSubProdBeforeSearch = $rootScope.InvertedsubprodDataBackup[4];
                $scope.ResultEmpty = false;
                $("#SubPagingTop").show();
                $("#SubPagingBottom").show();
                kendoDropDown();
                if ($rootScope.btnSubProdInvert) {
                    ;
                    var tabstrip = $("#tabstripInvertedProducts").data("kendoTabStrip");
                    var myTab = tabstrip.tabGroup.children("li").eq(1);
                    tabstrip.select(myTab);
                    $rootScope.btnSubProdInvert = false;
                    $rootScope.SubProdInvert = false;
                }
            }
        }
        $rootScope.GetInvertedSearchSubProductDetails = function () {
            if ($rootScope.invertedBackTosubProduct == false && $rootScope.LoadSubProductGrid != true) {

                $scope.Prodsubdata();
            }
            else {
                if ($rootScope.InvertedsubprodDataBackup.length == 5) {
                    $('#dynamictablesubproducts').show();
                    $rootScope.invertedBackTosubProduct == false;
                    $scope.InvertedprodDataSub = $rootScope.InvertedsubprodDataBackup[0];
                    $scope.InvertedcolumnsSubproducts = $rootScope.InvertedsubprodDataBackup[1];
                    $scope.ProdCurrentPage = $rootScope.InvertedsubprodDataBackup[2];
                    $scope.ProdCountPerPage = $rootScope.InvertedsubprodDataBackup[3];
                    $scope.gridCurrentPage = parseInt($scope.ProdCountPerPage);
                    $scope.InvertSubProdBeforeSearch = $rootScope.InvertedsubprodDataBackup[4];
                    $scope.ResultEmpty = false;
                    $("#SubPagingTop").show();
                    $("#SubPagingBottom").show();
                    kendoDropDown();
                    if ($rootScope.btnSubProdInvert) {
                        ;
                        var tabstrip = $("#tabstripInvertedProducts").data("kendoTabStrip");
                        var myTab = tabstrip.tabGroup.children("li").eq(1);
                        tabstrip.select(myTab);
                        $rootScope.btnSubProdInvert = false;
                        $rootScope.SubProdInvert = false;
                    }
                }
            }
        }

        $scope.GetScrollId = function (e) {
            $scope.scrollId = e;
        }

        $scope.ClearScrollId = function () {
            $scope.scrollId = "";
        }

        $scope.invsubprodsetWidth = function (event) {

            var resizeheader = document.getElementById("invsubprodresize_" + $scope.scrollId);
            var resizebody = document.getElementById("invsubprodresize1_" + $scope.scrollId);
            style = window.getComputedStyle(resizeheader);
            wdt = style.getPropertyValue('width');
            resizebody.style.width = wdt;
        }


    }]);
