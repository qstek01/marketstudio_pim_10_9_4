﻿
LSApp.controller('invertedproductsController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', '$localStorage', 'blockUI',
function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope, $localStorage, blockUI) {
    $('#importshow').hide();
    $('#samplegrid').hide();
    $scope.ProdCurrentPage = 1;
    $('#templatenamereadonly').hide();
    $('#showimport').hide();
    $("#readonlyTemplateNameTextbox").hide();
    $scope.templateList = 1;
    $scope.selectedTemplateId = 1;
    $localStorage.imgaepopupforinvert = true;
    $scope.makeTrue = false;
    $("#applybtn").removeClass("active");
    $('#dialog-confirm6').hide();
    $scope.usewildcards = 4;

    $scope.click = function () {
        $scope.InvertedData();
    };

    $scope.prodid = 0;
    $scope.checkedNodes = 0;
    $scope.selectedRow = [];

    $scope.navigate = function (e) {
        $scope.prodid = e.PRODUCT_ID;
        $scope.selectedRow = e;
        $rootScope.productassociatedfamilyGridDatasource.read();

        $('#associatedgrid').show();
        $('#dynamictable').hide();
        $('#hide').hide();
        $('#showimport').hide();
    };



    $scope.selectedAttr = $localStorage.CatalogItemNumber;


    $scope.SelectAttribute = function (attrName) {
        $scope.selectedAttr = attrName;
    };

    $scope.filterNodeValues = [];

    function rebind() {
        $rootScope.InvertedprodDataBackup = [];
        $rootScope.InvertedprodDataBackup.push($scope.InvertedprodData);
        $rootScope.InvertedprodDataBackup.push($scope.Invertedcolumns);
        $rootScope.InvertedprodDataBackup.push($scope.ProdCurrentPage);
        $rootScope.InvertedprodDataBackup.push($scope.ProdCountPerPage);
        $rootScope.InvertedprodDataBackup.push($scope.InvertProdBeforeSearch);
    }

    $scope.navigateclk = function (e) {
        rebind();
        $rootScope.LoadSubProductGrid = false;
        $rootScope.clickSelectedItems(e.dataItem, false, false);
        $('#tabstrip').show();
        $('#columnsetup').hide();
        $('#showimport').hide();
        $rootScope.invertedproductsbutton = false;
        $rootScope.invertedproductsbutton1 = true;
        $('#sampleProdgrid').css('max-width', '1220px');
        $('#sampleProdgrid tbody').css('max-width', '1220px');

    };


    $rootScope.LoadInvertedprodData = function (catalogId, id, cat_id) {
        $rootScope.selecetedCatalogId = catalogId
        $scope.catalog_id = catalogId;
        $scope.Family_id = id;
        $scope.category_id = cat_id;
        ;
        $scope.sub_id = '0';
        var ProdCount = [];
        dataFactory.getprodspecsforinvert(catalogId, id, true, cat_id, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.sub_id).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.getInvertedprodData = obj;

            $scope.Invertedcolumns = response.Data.Columns;

            if ($scope.InvertedprodData.length == 0 && $rootScope.deleteProduct == true && $scope.ProdCurrentPage != 1) {
                $scope.ProdCurrentPage = $scope.ProdCurrentPage - 1;
                $scope.getInvertedprodData(catalogId, id, cat_id);
            }

            //To Change Dynamice CatalogNo
            for (var i = 0; i < response.Data.Columns.length; i++) {
                if (response.Data.Columns[i]["Caption"].includes("ITEM#")) {
                    response.Data.Columns[i]["Caption"] = response.Data.Columns[i]["Caption"].replace("ITEM#", $localStorage.CatalogItemNumber);
                }
            }
            // To push the values into arry for split only attributes in drop down list.
            var AttributeValues = [];
            for (var i = 0 ; $scope.Invertedcolumns.length > i ; i++) {

                if (
                     $scope.Invertedcolumns[i].Caption == "CATALOG_ID" ||
                    $scope.Invertedcolumns[i].Caption == "FAMILY_ID" ||
                    $scope.Invertedcolumns[i].Caption == "PRODUCT_ID" ||
                     $scope.Invertedcolumns[i].Caption == "SORT" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2WEB" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2PRINT" ||
                     $scope.Invertedcolumns[i].Caption == "WORKFLOW STATUS" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2PDF" ||
                     $scope.Invertedcolumns[i].Caption == "PUBLISH2EXPORT" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2PORTAL" ||
                    $scope.Invertedcolumns[i].Caption == "SubProdCount"
                    ) {
                } else {
                    if ($scope.Invertedcolumns[i].Caption.includes("__OBJ")) {
                        AttributeValues.push({ "Value": i, "Name": $scope.Invertedcolumns[i].Caption.slice(0, $scope.Invertedcolumns[i].Caption.search("__OBJ")) });
                    } else {
                        AttributeValues.push({ "Value": i, "Name": $scope.Invertedcolumns[i].Caption });
                    }
                }
            }



            // To remove extra duplicate values (__)
            for (var i = 0 ; AttributeValues.length > i ; i++) {
                if (AttributeValues[i].Name.includes("__")) {
                    AttributeValues.splice(i, 1);
                }
            }

            $scope.AttributeValues = AttributeValues;

        });
    }


    $rootScope.Dashboards = true;
    $scope.init = function () {
        $('#pin').hide();
        $rootScope.familyMainEditor = false;
        $rootScope.Navigator = false;
        $rootScope.invertedproductsshow = true;
        $rootScope.invertedproductsbutton = false;
        $rootScope.invertedproductsbutton1 = false;
        $('#associatedgrid').hide();
        $('#dynamictable').show();
        $('#hide').show();
        $('#invertproductpreview').hide();
        $('#selectedattributeproducts').hide();
        $('#showimport').hide();
        $('#importshow').hide();
        $('#dynamicsortforInvcertedproducts').data = '';




    };
    $scope.init();
    $scope.opt = "TOTALCOUNT";
    $scope.ProdCurrentPage = "1";
    $scope.ProdCountPerPage = "100";

    $scope.callProductGridPagePerInvertedProducts = function (pageper) {
        if (pageper != null && pageper != "0") {
            $scope.ProdCountPerPage = pageper;
            $scope.ProdCurrentPage = "1";
            if ($scope.InvertProductSearch != "" && $scope.InvertProductSearch != undefined) {
                $scope.SearchProduct("pagingItems");
            } else {
                $scope.Proddata();
            }
        }
    };



    $('#samplegrid tbody').scroll(function (e) {
            $('#samplegrid thead').css("left", -$("#samplegrid tbody").scrollLeft()); //fix the thead relative to the body scrolling
            $('#samplegrid thead th:nth-child(-n+9)').css("left", $("#samplegrid tbody").scrollLeft()); //fix the first cell of the header
            $('#samplegrid tbody td:nth-child(-n+9)').css("left", $("#samplegrid tbody").scrollLeft()); //fix the first column of tdbody
        });
  



    $scope.callProductGridPagingMain = function (pageno) {
        if (pageno != null && pageno <= $scope.totalProdPageCount && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {
            if (pageno == 'PREV' && $scope.ProdCurrentPage != 1) {
                $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) - 1;
            } else if (pageno == 'PREV' && $scope.ProdCurrentPage == 1) {
                $scope.ProdCurrentPage = "1";
            } else if (pageno == 'NEXT') {
                $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) + 1;
                if ($scope.ProdCurrentPage > $scope.totalProdPageCount) {
                    $scope.ProdCurrentPage = $scope.totalProdPageCount;
                }
            } else {
                $scope.ProdCurrentPage = pageno;
            }
            if ($scope.InvertProductSearch != "" && $scope.InvertProductSearch != undefined) {
                $scope.SearchProduct("paging");
            } else {
                $scope.InvertedData();
            }

        }
    };

    $scope.InvertedprodData = [];
    var ProdCount = [];

    $scope.currentPageCount = "";
    $scope.refreshSearch = function () {
        $rootScope.currentPageCount = 1;
        $scope.InvertProductSearch = "";
        $scope.ProdCountPerPage = "100";
        $scope.Proddata();
    };


    $scope.Proddata = function () {

        if ($rootScope.ProdPerPageCount != undefined && $rootScope.ProdPerPageCount != "" && $scope.currentPageCount === "") {
            $scope.ProdCountPerPage = $rootScope.ProdPerPageCount;
            $scope.gridCurrentPage = $rootScope.ProdPerPageCount;
        }
        dataFactory.GetInvertedProductsCount($rootScope.selecetedCatalogId, $scope.opt).success(function (response) {
            var obj = jQuery.parseJSON(response);
            $scope.getproductCount = obj;
            $rootScope.getproductCountdetails = $scope.getproductCount;
            $scope.displayProductCount = $rootScope.getproductCountdetails;
            if ($scope.getproductCount < $scope.ProdCountPerPage) {
                $scope.TotalTableInvertCount = $scope.getproductCount;
            }
            else {
                $scope.TotalTableInvertCount = $scope.ProdCountPerPage;
            }
            //$('#samplegrid').show();
            var ProdEditPageloopcnt = Math.ceil(parseInt(obj) / parseInt($scope.ProdCountPerPage));
            ProdCount = [];
            for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                ProdCount.push(i);
            }
            $scope.ProdPageCount = ProdCount;
            $scope.totalProdPageCount = ProdEditPageloopcnt;
            $scope.ProdCurrentPage = 1;
            if ($rootScope.ProdPerPageCount != undefined && $rootScope.ProdPerPageCount != "" && $scope.currentPageCount === "") {
                $scope.ProdCountPerPage = $rootScope.ProdPerPageCount;
                $scope.gridCurrentPage = $rootScope.ProdPerPageCount;
            }
            angular.forEach($scope.productData, function (value) {
                value.SORT = parseFloat(value.PRODUCT_ID);
            });

            $scope.InvertedData();

        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.keyEnterPressed = function (keyEvent) {
        if (keyEvent.keyCode == 13) {
            $scope.SearchProduct($scope.InvertProductSearch);
            keyEvent.preventDefault();
            keyEvent.stopPropagation();
        }
    };

    $scope.ResultEmpty = false;
    $scope.SearchProduct = function (searchText) {
        if (searchText == undefined) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Kindly enter a value to search.',
                type: "info"
            })
            return;
        }
        var itemNumber = "";
        if (searchText == "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Kindly enter a value to search.',
                type: "info"
            })
            $scope.ProdCurrentPage = "1";
            $scope.InvertedData();
            $scope.InvertProductSearch = '';
            $scope.ResultEmpty = false;

        } else {
            var searchTxt;
            if (searchText == "paging" || searchText == "pagingItems") {
                searchTxt = $scope.InvertProductSearch;

            } else {
                searchTxt = searchText;
                $scope.ProdCurrentPage = 1;
            }

            if ($scope.selectedAttr == $localStorage.CatalogItemNumber) {
                itemNumber = "ITEM#";
            } else {
                itemNumber = $scope.selectedAttr;
            }

            dataFactory.GetInvertedSearchProducts($rootScope.selecetedCatalogId, $scope.ProdCurrentPage, $scope.ProdCountPerPage, itemNumber, $scope.usewildcards, searchTxt, $scope.checkedNodes, $scope.selectedTemplateId).success(function (response) {

                if (response.Data.length > 0 || response.Data.Columns.length > 0) {
                    //$("#samplegrid").show();
                    var obj = jQuery.parseJSON(response.Data[0].Data);
                    $scope.InvertedprodData = obj;
                    $scope.displayProductCount = obj.length;
                    for (var i = 0; i < response.Data[0].Columns.length; i++) {
                        if (response.Data[0].Columns[i].Caption == "ITEM#") {
                            response.Data[0].Columns[i].Caption = $localStorage.CatalogItemNumber;
                        }
                    }
                    $scope.Invertedcolumns = response.Data[0].Columns;
                    if (obj.length > 0) {
                        $scope.ResultEmpty = false;
                    } else {
                        $scope.ResultEmpty = true;
                    }
                    $rootScope.currentPageCount = 1;
                    $rootScope.ProdPerPageCount = $scope.ProdCountPerPage;
                    if (searchText != "paging") {
                        var objTotal = jQuery.parseJSON(response.Data[1].Data);
                        var prodEditPageloopcnt = Math.ceil(parseInt(objTotal[0].COUNT) / parseInt($scope.ProdCountPerPage));
                        if (prodEditPageloopcnt == 0) {
                            prodEditPageloopcnt = 1
                        }
                        ProdCount = [];
                        for (var i = 1; i <= prodEditPageloopcnt; i++) {
                            ProdCount.push(i);
                        }
                        $scope.ProdPageCount = ProdCount;
                        $scope.totalProdPageCount = prodEditPageloopcnt;
                        $scope.ProdCurrentPage = 1;
                        if ($rootScope.ProdPerPageCount != undefined && $rootScope.ProdPerPageCount != "" && $scope.currentPageCount === "") {
                            $scope.ProdCountPerPage = $rootScope.ProdPerPageCount;
                            $scope.gridCurrentPage = $rootScope.ProdPerPag
                            eCount;
                        }
                        angular.forEach($scope.productData, function (value) {
                            value.SORT = parseFloat(value.PRODUCT_ID);
                        });
                        rebind();
                    }
                    $rootScope.tblInvertedGrid.reload();

                    if( $scope.ResultEmpty === true)
                    {
                        $("#Actioncolumn").hide();
                        }
                }
                else {
                    //$("#samplegrid").hide();\
                   
                    $("#samplee100").hide();
                    $scope.ResultEmpty = true;
                }

            }).error(function (error) {
                options.error(error);
            });
        }

    };
    $scope.InvertProdBeforeSearch = [];
    $scope.unusedId = ["PRODUCT_ID",
        "CATALOG_ID",
        "CATALOG_NAME",
        "FAMILY_ID",
        "CATEGORY_ID"
    ];
    function kendoDropDown() {
        $("#attributeName").kendoDropDownList({
            dataTextField: "Caption",
            dataValueField: "Caption",
            dataSource: $scope.InvertProdBeforeSearch,
            height: 400
        });
    }
    $rootScope.inverted = 'invertedproducts';
    $scope.InvertedData = function () {
        $scope.InvertProdBeforeSearch = [];
        $('#dynamictable').show();
        $('#hide').show();
        if ($scope.ProdCurrentPage == null) {

            $scope.ProdCurrentPage = 1;
        }
        if ($scope.ProdCountPerPage == null) {

            $scope.ProdCountPerPage = 100;
        }
        if ($scope.filterNodeValues.length > 0)
            $scope.checkedNodes = $scope.filterNodeValues;
        dataFactory.GetInvertedProducts($rootScope.selecetedCatalogId, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.checkedNodes, $scope.selectedTemplateId).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.InvertedprodData = obj;
            $scope.displayProductCount = $rootScope.getproductCountdetails;
            $scope.ResultEmpty = false;
            $rootScope.ProdPerPageCount = $scope.ProdCountPerPage;
            $rootScope.currentPageCount = $scope.ProdCountPerPage;
            $scope.currentPageCount = $scope.ProdCountPerPage;
            $rootScope.tblInvertedGrid.reload();
            var columnName = [];
            for (var i = 0; i < response.Data.Columns.length; i++) {
                var index = $scope.unusedId.indexOf(response.Data.Columns[i].Caption);
                if (index == -1) {
                    if ($scope.InvertProdBeforeSearch.indexOf(response.Data.Columns[i].Caption) == -1) {
                        if (response.Data.Columns[i]["Caption"] == "ITEM#__OBJ__1__1__false__true") {
                            response.Data.Columns[i]["Caption"] = $localStorage.CatalogItemNumber + "__OBJ__1__1__false__true";
                        }

                       
                    }
                }

             
            }
            $scope.Invertedcolumns = response.Data.Columns;
            $("#samplegrid").show();
            var AttributeValues = [];

            for (var i = 0 ; $scope.Invertedcolumns.length > i ; i++) {

                if (
                     $scope.Invertedcolumns[i].Caption == "CATALOG_ID" ||
                    $scope.Invertedcolumns[i].Caption == "FAMILY_ID" ||
                    $scope.Invertedcolumns[i].Caption == "PRODUCT_ID" ||
                     $scope.Invertedcolumns[i].Caption == "SORT" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2WEB" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2PRINT" ||
                     $scope.Invertedcolumns[i].Caption == "WORKFLOW STATUS" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2PDF" ||
                     $scope.Invertedcolumns[i].Caption == "PUBLISH2EXPORT" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2PORTAL" ||
                    $scope.Invertedcolumns[i].Caption == "SubProdCount"
                    ) {

                }

                else {

                    if ($scope.Invertedcolumns[i].Caption.includes("__OBJ")) {
                        AttributeValues.push({ "Value": i, "Name": $scope.Invertedcolumns[i].Caption.slice(0, $scope.Invertedcolumns[i].Caption.search("__OBJ")) });
                        $scope.InvertProdBeforeSearch.push({ "Caption": $scope.Invertedcolumns[i].Caption.slice(0, $scope.Invertedcolumns[i].Caption.search("__OBJ")), "ColumnName": $scope.Invertedcolumns[i].Caption.slice(0, $scope.Invertedcolumns[i].Caption.search("__OBJ")) });
                    } else {
                        AttributeValues.push({ "Value": i, "Name": $scope.Invertedcolumns[i].Caption });
                    }
                }

                if ($scope.Invertedcolumns[i].Caption == "PRODUCT_ID") {
                    $scope.Invertedcolumns[i].Caption = "ITEM_ID"
                }
            }



            // To remove extra duplicate values (__)
            for (var i = 0 ; AttributeValues.length > i ; i++) {
                if (AttributeValues[i].Name.includes("__")) {
                    AttributeValues.splice(i, 1);
                }
            }

            $scope.AttributeValues = AttributeValues;

            rebind();
            $("#PagingTop").show();
            $("#PagingBottom").show();
            kendoDropDown();
            $scope.modifyPaginationGrid();
        }).error(function (error) {
            options.error(error);
        });
    };
    $scope.InvertedData1 = function () {
        $scope.InvertProdBeforeSearch = [];
        $('#dynamictable').show();
        $('#hide').show();
        if ($scope.ProdCurrentPage == null) {

            $scope.ProdCurrentPage = 1;
        }
        if ($scope.ProdCountPerPage == null) {

            $scope.ProdCountPerPage = 100;
        }
        if ($scope.filterNodeValues.length > 0)
            $scope.checkedNodes = $scope.filterNodeValues;
        dataFactory.GetInvertedProducts($rootScope.selecetedCatalogId, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.checkedNodes, $scope.selectedTemplateId).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.InvertedprodData = obj;
            $scope.ResultEmpty = false;
            $rootScope.ProdPerPageCount = $scope.ProdCountPerPage;
            $rootScope.currentPageCount = $scope.ProdCountPerPage;
            $scope.currentPageCount = $scope.ProdCountPerPage;
            $rootScope.tblInvertedGrid.reload();
            var columnName = [];
            for (var i = 0; i < response.Data.Columns.length; i++) {
                var index = $scope.unusedId.indexOf(response.Data.Columns[i].Caption);
                if (index == -1) {
                    if ($scope.InvertProdBeforeSearch.indexOf(response.Data.Columns[i].Caption) == -1) {
                        if (response.Data.Columns[i]["Caption"] == "ITEM#__OBJ__1__1__false__true") {
                            response.Data.Columns[i]["Caption"] = $localStorage.CatalogItemNumber + "__OBJ__1__1__false__true";
                        }
                    }
                }
            }
            $scope.Invertedcolumns = response.Data.Columns;
            $("#samplegrid").show();
            var AttributeValues = [];

            for (var i = 0 ; $scope.Invertedcolumns.length > i ; i++) {

                if (
                     $scope.Invertedcolumns[i].Caption == "CATALOG_ID" ||
                    $scope.Invertedcolumns[i].Caption == "FAMILY_ID" ||
                    $scope.Invertedcolumns[i].Caption == "PRODUCT_ID" ||
                     $scope.Invertedcolumns[i].Caption == "SORT" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2WEB" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2PRINT" ||
                     $scope.Invertedcolumns[i].Caption == "WORKFLOW STATUS" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2PDF" ||
                     $scope.Invertedcolumns[i].Caption == "PUBLISH2EXPORT" ||
                    $scope.Invertedcolumns[i].Caption == "PUBLISH2PORTAL" ||
                    $scope.Invertedcolumns[i].Caption == "SubProdCount"
                    ) {

                }

                else {

                    if ($scope.Invertedcolumns[i].Caption.includes("__OBJ")) {
                        AttributeValues.push({ "Value": i, "Name": $scope.Invertedcolumns[i].Caption.slice(0, $scope.Invertedcolumns[i].Caption.search("__OBJ")) });
                        $scope.InvertProdBeforeSearch.push({ "Caption": $scope.Invertedcolumns[i].Caption.slice(0, $scope.Invertedcolumns[i].Caption.search("__OBJ")), "ColumnName": $scope.Invertedcolumns[i].Caption.slice(0, $scope.Invertedcolumns[i].Caption.search("__OBJ")) });
                    } else {
                        AttributeValues.push({ "Value": i, "Name": $scope.Invertedcolumns[i].Caption });
                    }
                }
            }



            // To remove extra duplicate values (__)
            for (var i = 0 ; AttributeValues.length > i ; i++) {
                if (AttributeValues[i].Name.includes("__")) {
                    AttributeValues.splice(i, 1);
                }
            }

            $scope.AttributeValues = AttributeValues;

            rebind();
            $("#PagingTop").show();
            //$("#PagingBottom").show();
            kendoDropDown();
            $scope.modifyPaginationGrid();
        }).error(function (error) {
            options.error(error);
        });
    };

    $rootScope.tblInvertedGrid = new ngTableParams({ page: 1, count: 1000 },
    {

        counts: [],
        noPager: true,
        autoBind: true,
        total: 1,
        $scope: { $data: {} },
        getData: function ($defer, params) {
            var filteredData = $scope.InvertedprodData;
            var orderedData = filteredData;
            params.total(orderedData.length);
            return $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
    $rootScope.LoadInvertedData = function () {
        $scope.ProdCurrentPage = 1
        $scope.ProdCountPerPage = 100;
        $scope.InvertedData();
        $rootScope.tblInvertedGrid.reload();
    }
    $rootScope.LoadInvertedData1 = function () {
        $scope.ProdCurrentPage = 1
        $scope.ProdCountPerPage = 100;
        $scope.InvertedData1();
        $rootScope.tblInvertedGrid.reload();
    }

    $scope.orderFields = "SORT";

    if ($rootScope.invertedBackToProduct == false) {
        $scope.Proddata();
    }
    else {
        if ($rootScope.InvertedprodDataBackup.length == 5) {

            $rootScope.invertedBackToProduct == false;
            $scope.InvertedprodData = $rootScope.InvertedprodDataBackup[0];
            $scope.Invertedcolumns = $rootScope.InvertedprodDataBackup[1];
            $scope.ProdCurrentPage = $rootScope.InvertedprodDataBackup[2];
            $scope.ProdCountPerPage = $rootScope.InvertedprodDataBackup[3];
            $scope.gridCurrentPage = parseInt($scope.ProdCountPerPage);
            $scope.InvertProdBeforeSearch = $rootScope.InvertedprodDataBackup[4];
            $scope.ResultEmpty = false;
            $("#PagingTop").show();
            $("#PagingBottom").show();
            kendoDropDown();
        }

    }
    $rootScope.productassociatedfamilyGridDatasource = new kendo.data.DataSource({
        type: "json", autoBind: true,
        transport: {
            read: function (options) {
                dataFactory.ProdFamilyAssociatedCategory($scope.prodid, $rootScope.selecetedCatalogId).success(function (response) {
                    options.success(response);
                    if ($rootScope.productassociatedfamilyGridDatasource._data.length > 0) {
                        // $('#associatedgrid').hide();
                        $rootScope.familyMainEditor = false;
                        $rootScope.invertedproductsshow = true;
                        $rootScope.invertedproductsbutton1 = false;
                        $rootScope.invertedproductsbutton = true;
                        $('#associatedgrid').show();
                        $('#dynamictable').hide();
                        $('#hide').hide();
                    } else {
                        $rootScope.clickSelectedItems($scope.selectedRow, false, false);
                        $rootScope.familyMainEditor = false;
                        $rootScope.invertedproductsshow = true;
                        $rootScope.invertedproductsbutton1 = false;
                        $rootScope.invertedproductsbutton = true;
                        $('#associatedgrid').show();
                        $('#dynamictable').hide();
                        $('#hide').hide();
                    }
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        }, schema: {
            model: {
                fields: {
                    CATEGORY_ID: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    PRODUCT_ID: { editable: false }
                }
            }
        }
    });

    $scope.backs = function () {
        $rootScope.familyMainEditor = false;
        $rootScope.invertedproductsshow = true;
        $rootScope.invertedproductsbutton = false;
        $rootScope.invertedproductsbutton1 = false;
        $('#associatedgrid').hide();
        $('#dynamictable').show();
        $('#hide').show();
        $scope.gridCurrentPage = "5";
    };

    $rootScope.productassociatedfamilyGridOptions = {
        dataSource: $rootScope.productassociatedfamilyGridDatasource,
        autoBind: false, selectable: true,
        columns: [{ field: "STRING_VALUE", title: $localStorage.CatalogItemNumber, width: "180px", template: "<a title='Item No'  class='k-link' href='javascript:void(0);'  ng-click='navigateclk(this)'>#=STRING_VALUE#</a>" },
            { field: "FAMILY_NAME", title: "Family Name", width: "180px", template: "<a title='Family Name'  class='k-link' href='javascript:void(0);'  ng-click='navigateclk(this)'>#=FAMILY_NAME#</a>" },

             { field: "CATEGORY_NAME", title: "Category Name", width: "180px", template: "<a title='Category Name'  class='k-link' href='javascript:void(0);'  ng-click='navigateclk(this)'>#=CATEGORY_NAME#</a>" },
          { field: "CATALOG_NAME", title: "Catalog Name", width: "180px", template: "<a title='Catalog Name'  class='k-link' href='javascript:void(0);'  ng-click='navigateclk(this)'>#=CATALOG_NAME#</a>" }
        ],
        editable: false
    };

    $scope.GetScrollId = function (e) {
        $scope.scrollId = e;
    }

    $scope.ClearScrollId = function () {
        $scope.scrollId = "";
    }

    $scope.invprodsetWidth = function (event) {

       var resizeheader = document.getElementById("invprodresize_" + $scope.scrollId);
       var resizebody = document.getElementById("invprodresize1_" + $scope.scrollId);
       style = window.getComputedStyle(resizeheader);
       wdt = style.getPropertyValue('width');
       resizebody.style.width = wdt;
   }

    $scope.exportFormatType = ".XLS";

    $scope.invertedProductsExport = function () {
        
        if ($scope.getproductCount != 0) {
            $scope.exportFormatType = ".XLS";
            dataFactory.ExportInvertedProducts($rootScope.selecetedCatalogId, $scope.opt, $rootScope.DisplayIdcolumns, $scope.exportFormatType, $scope.selectedTemplateId).success(function (response) {
                window.open("DownloadInvertedProductsExport.ashx?Path=" + response);
            }).error(function (error) {
                options.error(error);
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'No Products Available For Export!',
                type: "info"
            });
        }
    }

    $scope.selectedrow = function (columnindex) {
        $scope.selectedcolumnindex = columnindex;
    }
    $scope.Productrightclick = function (e, Mainproductdata, Mainproductcolumns) {
        $(".menuProduct").show();
        e.preventDefault();
        $(".menuProduct").css("left", e.pageX);
        $(".menuProduct").css("top", e.pageY);

        $scope.Copyinvproductitemtextdropdown = false;
        $scope.searchforinvproductitemtextdropdown = false;
        $scope.Copyinvproductcolumntextdropdown = false;
        $scope.Searchforinvproductattributevaluedropdown = false;
        $scope.Copyinvproductitemtextdropdown = true;
        $scope.searchforinvproductitemtextdropdown = true;
        $scope.itemtext = $localStorage.CatalogItemNumber;
        $scope.copyinvproductitemtextvalue = $scope.itemtext;
        $scope.invproductitemtextvalue = Mainproductdata["ITEM#"];
        if ($scope.invproductitemtextvalue.includes("(")) {
            $scope.invproductitemtextvalue = $scope.invproductitemtextvalue.substr(0, $scope.invproductitemtextvalue.lastIndexOf("(")).trim();
        }

        //Copyinvproductitemtext
        document.getElementById("Copyinvproductitemtext").title = $scope.invproductitemtextvalue;
        $('[data-toggle="tooltip"]').tooltip();
        for (var i = 0; i < $scope.copyinvproductitemtextvalue.length; i++) {
            if (i > 9) {
                $scope.copyinvproductitemtextvalue = $scope.copyinvproductitemtextvalue.slice(0, 10);
                $scope.copyinvproductitemtextvalue = $scope.copyinvproductitemtextvalue + ("...".replace($scope.copyinvproductitemtextvalue));
            }
        }

        //searchforinvproductitemtext
        document.getElementById("searchforinvproductitemtext").title = $scope.invproductitemtextvalue;
        $('[data-toggle="tooltip"]').tooltip();

        $scope.bindinvproductitemtext = $scope.invproductitemtextvalue;

        for (var i = 0; i < $scope.bindinvproductitemtext.length; i++) {
            if (i > 9) {
                $scope.bindinvproductitemtext = $scope.bindinvproductitemtext.slice(0, 10);
                $scope.bindinvproductitemtext = $scope.bindinvproductitemtext + ("...".replace($scope.bindinvproductitemtext));
            }
        }
        if ($scope.selectedcolumnindex > 5) {
            $scope.Copyinvproductcolumntextdropdown = true;
            $scope.Searchforinvproductattributevaluedropdown = true;

            $scope.mainproductcolumnheader = Mainproductcolumns[$scope.selectedcolumnindex].Caption;

            $scope.bindinvproductcolumntext = $scope.mainproductcolumnheader;
            $scope.mainproductattvalue = Mainproductdata[$scope.mainproductcolumnheader];

            //Copyinvproductcolumntext
            document.getElementById("Copyinvproductcolumntext").title = $scope.mainproductattvalue;
            $('[data-toggle="tooltip"]').tooltip();

            for (var i = 0; i < $scope.bindinvproductcolumntext.length; i++) {
                if (i > 9) {
                    $scope.bindinvproductcolumntext = $scope.bindinvproductcolumntext.slice(0, 10);
                    $scope.bindinvproductcolumntext = $scope.bindinvproductcolumntext + ("...".replace($scope.bindinvproductcolumntext));
                }
            }
            //Searchforinvproductattributevalue
            if ($scope.mainproductattvalue == null) {
                $scope.Copyinvproductcolumntextdropdown = false;
                $scope.Searchforinvproductattributevaluedropdown = false;
            }
            else {
                document.getElementById("Searchforinvproductattributevalue").title = $scope.mainproductattvalue;
                $('[data-toggle="tooltip"]').tooltip();

                $scope.bindinvproductattributevalue = $scope.mainproductattvalue;

                for (var i = 0; i < $scope.bindinvproductattributevalue.length; i++) {
                    if (i > 9) {
                        $scope.bindinvproductattributevalue = $scope.bindinvproductattributevalue.slice(0, 10);
                        $scope.bindinvproductattributevalue = $scope.bindinvproductattributevalue + ("...".replace($scope.bindinvproductattributevalue));
                    }
                }
            }

        }

        else {
            $scope.Copyinvproductcolumntextdropdown = false;
            $scope.Searchforinvproductattributevaluedropdown = false;
        }
    };


    $scope.CopyinvproductItemtext = function (selectedtext) {

        //$('#hidesearchdiv').hide();
        $scope.hidemenu();
        if (selectedtext == 'itemtextinvproduct') {
            $scope.invproductitemtextvalue = $scope.invproductitemtextvalue;
            var clipboardtext = $scope.invproductitemtextvalue;
            new Clipboard('#Copyinvproductitemtext', {
                text: function (itemtextvalue) {
                    return clipboardtext;
                }
            })
        }
    }

    $scope.Searchforinvproduct = function (selectedsearchtext) {
        if (selectedsearchtext == 'Searchforinvproduct') {
            $scope.searchitemtextvalue = $scope.invproductitemtextvalue;
            document.getElementById('txt_search').value = $scope.searchitemtextvalue;
            sessionStorage.setItem("searchText", $scope.searchitemtextvalue);
            Item = "Products";
            sessionStorage.setItem("Dropdownvalue", Item);

            if ($scope.Itemtextvalue != "") {
                window.document.location = "/App/Search";
                return false;
            } else {
                window.document.location = "/App/Search";
                return false;
            }
        }
    }

    $scope.Copyinvproductcolumntext = function (selectedtext) {
        $scope.hidemenu();
        if (selectedtext == 'Columntextinvproduct') {
            $scope.invproductcolumnvalue = $scope.mainproductattvalue;
            var clipboardtext = $scope.invproductcolumnvalue;
            new Clipboard('#Copyinvproductcolumntext', {
                text: function (columntextvalue) {
                    return clipboardtext;
                }
            })
        }
    }

    $scope.Searchforinvproductattribute = function (selectedsearchtext) {
        if (selectedsearchtext == 'Searchforinvproductattributevalue') {
            $scope.searchproductattvalue = $scope.mainproductattvalue;
            document.getElementById('txt_search').value = $scope.searchproductattvalue;
            sessionStorage.setItem("searchText", $scope.searchproductattvalue);
            Item = "Products";
            $localStorage.invproductgrid = "True";
            sessionStorage.setItem("Dropdownvalue", Item);
            sessionStorage.setItem("Dropdownvalueforinvproductselectedattribute", $scope.mainproductcolumnheader);
            if ($scope.Itemtextvalue != "") {
                window.document.location = "/App/Search";
                return false;
            } else {
                window.document.location = "/App/Search";
                return false;
            }
        }
    }


    $scope.hidemenu = function () {
        $(".menuProduct").hide();
    }

    $scope.checkedNodeIdsValues = [];


    $scope.multipleTabledesignerClick = function () {

        $scope.invertedsearchfilter.center().open();
        $scope.treeData = new kendo.data.HierarchicalDataSource({
            type: "json",
            loadOnDemand: false,
            transport: {
                read: function (options) {
                    dataFactory.categorySearchFilterTree($rootScope.selecetedCatalogId, options.data.id, $rootScope.selecetedCatalogId).success(function (response) {
                        options.success(response);
                        if ($scope.selectedCheckedNodes != undefined) {
                            for (var j = 0; j < response.length; j++) {
                                if (($scope.selectedCheckedNodes).indexOf(response[j].CATEGORY_ID) !== -1) {
                                    response[j].check = true;
                                    response[j].checked = true;
                                }
                            }
                        }
                    }).error(function (response) {
                        options.success(response);
                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            },

        });
        $scope.treeDataoptions = {
            checkboxes: {
                checkChildren: true,
            }, check: onCheck,
            dataSource: $scope.treeData
        };

    }

    function checkedNodeIds(nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                if (nodes[i].CATEGORY_ID.contains("~")) {
                    checkedNodes.push(nodes[i].id + "!");
                } else if (nodes[i].CATEGORY_ID && !nodes[i].check) {
                    checkedNodes.push(nodes[i].CATEGORY_ID);
                }
                else {
                    checkedNodes.push(nodes[i].id);
                }
            }
            if (nodes[i].hasChildren) {
                checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    }

    var checkedNodes = [], message;
    function onCheck() {
        checkedNodes = [];
        checkedNodeIds($scope.treeData._data, checkedNodes);
        if (checkedNodes.length > 0) {
            message = "IDs of checked nodes: " + checkedNodes.join(",");
            $scope.checkedNodes = checkedNodes;
        } else {
            $scope.checkedNodes = checkedNodes;
            message = "No nodes checked.";
        }
    }

    $scope.convertedValues = function () {
        if ($scope.ProdCurrentPage == null || $scope.ProdCurrentPage == "undefined" || $scope.ProdCurrentPage == "") {
            $scope.ProdCurrentPage = 1;
        }
        if (angular.isNumber($rootScope.selecetedCatalogId)) {
            $rootScope.selecetedCatalogId = $rootScope.selecetedCatalogId;
        } else {
            $rootScope.selecetedCatalogId = parseInt($rootScope.selecetedCatalogId);
        }
        if (angular.isNumber($scope.ProdCurrentPage)) {
            $scope.ProdCurrentPage = $scope.ProdCurrentPage;
        } else {
            $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage);
        }
        if (angular.isNumber($scope.ProdCountPerPage)) {
            $scope.ProdCountPerPage = $scope.ProdCountPerPage;
        } else {
            $scope.ProdCountPerPage = parseInt($scope.ProdCountPerPage);
        }
    }

    $scope.applyCategoryFilter = function (InvertProductSearch) {
        if ($scope.checkedNodes.length > 0) {
            $scope.selectedCheckedNodes = [];
            $scope.InvertProdBeforeSearch = [];
            $scope.selectedCheckedNodes = $scope.checkedNodes;
            $scope.filterNodeValues = $scope.checkedNodes;
            if ($scope.InvertProductSearch != null && $scope.InvertProductSearch != "" && $scope.InvertProductSearch != "undefined") {
                $scope.SearchProduct(InvertProductSearch);
            } else {
                $scope.convertedValues();
                dataFactory.GetInvertedProducts($rootScope.selecetedCatalogId, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.checkedNodes, $scope.selectedTemplateId).success(function (response) {

                    if (response != null) {
                        var obj = jQuery.parseJSON(response.Data.Data);
                    }
                    else {
                        return;
                    }
                    if (obj.length > 0) {
                        $scope.invertedsearchfilter.close();
                        // $("#samplegrid").show();
                        $scope.ResultEmpty = false;
                        $scope.InvertedprodData = obj;
                        $scope.Invertedcolumns = response.Data.Columns;
                        $scope.ResultEmpty = false;
                        $rootScope.ProdPerPageCount = $scope.ProdCountPerPage;
                        $rootScope.currentPageCount = $scope.ProdCountPerPage;
                        $scope.currentPageCount = $scope.ProdCountPerPage;
                        $rootScope.tblInvertedGrid.reload();
                        $scope.modifyPaginationGrid();
                        var columnName = [];
                        for (var i = 0; i < $scope.Invertedcolumns.length; i++) {
                            var index = $scope.unusedId.indexOf($scope.Invertedcolumns[i].Caption);
                            if (index == -1) {
                                if ($scope.InvertProdBeforeSearch.indexOf($scope.Invertedcolumns[i].Caption) == -1) {
                                    if ($scope.Invertedcolumns[i]["Caption"] == "ITEM#") {
                                        $scope.Invertedcolumns[i]["Caption"] = $localStorage.CatalogItemNumber;
                                    }
                                    $scope.InvertProdBeforeSearch.push($scope.Invertedcolumns[i]);
                                }
                            }
                        }
                        $("#applybtn").addClass("active");
                        $scope.treeData.read();
                        rebind();
                        $("#PagingTop").show();
                        $("#PagingBottom").show();
                        kendoDropDown();
                        $scope.checkedNodes = [];
                        $scope.invprodsetWidthReset();
                    }
                    else {
                        $scope.invertedsearchfilter.close();
                        $("#applybtn").removeClass("active");
                        $("#samplegrid").hide();
                        $scope.ResultEmpty = true;
                        $scope.treeData.read();
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
        else {
            $("#applybtn").removeClass("active");
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select atleast one value.',
                type: "info"
            });
            return;
        }
    }
    $scope.closePopUp = function () {
        $scope.invertedsearchfilter.close();
    }
    $scope.clearSelectedValues = function () {
        $scope.checkedNodes = [];
        $scope.filterNodeValues = [];
        $scope.treeData.read();
        $("#applybtn").removeClass("active");
    }
    $scope.refreshInvertedProductGrid = function () {
        $scope.invertedsearchfilter.close();
        $scope.InvertProdBeforeSearch = [];
        $scope.InvertProductSearch = null;
        $scope.treeData.read();
        //$("#samplegrid").show();
        $("#applybtn").removeClass("active");
        $scope.ResultEmpty = false;
        $scope.ProdCurrentPage = 1;
        $scope.convertedValues();
        dataFactory.GetInvertedProducts($rootScope.selecetedCatalogId, $scope.ProdCurrentPage, $scope.ProdCountPerPage, "0", $scope.selectedTemplateId).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.InvertedprodData = obj;
            $scope.Invertedcolumns = response.Data.Columns;
            $scope.ResultEmpty = false;
            $rootScope.ProdPerPageCount = $scope.ProdCountPerPage;
            $rootScope.currentPageCount = $scope.ProdCountPerPage;
            $scope.currentPageCount = $scope.ProdCountPerPage;
            $rootScope.tblInvertedGrid.reload();
            var columnName = [];
            for (var i = 0; i < $scope.Invertedcolumns.length; i++) {
                var index = $scope.unusedId.indexOf($scope.Invertedcolumns[i].Caption);
                if (index == -1) {

                    if ($scope.InvertProdBeforeSearch.indexOf($scope.Invertedcolumns[i].Caption) == -1) {
                        if ($scope.Invertedcolumns[i]["Caption"] == "ITEM#") {
                            $scope.Invertedcolumns[i]["Caption"] = $localStorage.CatalogItemNumber;
                            $scope.Invertedcolumns[i]["ColumnName"] = "ITEM#";
                        }
                        $scope.InvertProdBeforeSearch.push($scope.Invertedcolumns[i]);
                    }
                }
            }
            rebind();
            $("#PagingTop").show();
            $("#PagingBottom").show();
            kendoDropDown();
            $scope.invprodsetWidthReset();
        }).error(function (error) {
            options.error(error);
        });
    }
    $scope.modifyPaginationGrid = function () {
        if ($rootScope.ProdPerPageCount != undefined && $rootScope.ProdPerPageCount != "" && $scope.currentPageCount === "") {
            $scope.ProdCountPerPage = $rootScope.ProdPerPageCount;
            $scope.gridCurrentPage = $rootScope.ProdPerPageCount;
        }
        dataFactory.GetInvertedModifiedProductsCount().success(function (response) {
            var obj = jQuery.parseJSON(response);
            var ProdEditPageloopcnt = Math.ceil(parseInt(obj) / parseInt($scope.ProdCountPerPage));
            ProdCount = [];
            for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                ProdCount.push(i);
            }
            $scope.ProdPageCount = ProdCount;
            $scope.totalProdPageCount = ProdEditPageloopcnt;
            if ($scope.ProdCurrentPage == undefined)
                $scope.ProdCurrentPage = 1;
            if ($rootScope.ProdPerPageCount != undefined && $rootScope.ProdPerPageCount != "") {
                $scope.ProdCountPerPage = $rootScope.ProdPerPageCount;
                $scope.gridCurrentPage = $rootScope.ProdPerPageCount;
            }
            angular.forEach($scope.productData, function (value) {
                value.SORT = parseFloat(value.PRODUCT_ID);
            });

        });
    }
    $scope.attributeListDataSource = {
        type: "jsonp",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getAttributeListUnderCatalog($rootScope.selecetedCatalogId).success(function (response) {
                    options.success(response.Data.Data);
                }).error(function (response) {
                    options.success(response);
                });
            }
        }
    };
    $scope.customOptions = {
        dataSource: $scope.attributeListDataSource,
        dataTextField: "attributeName",
        dataValueField: "attributeID",
    };
    $scope.importSuccessResultDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: true,
        scrollable: true,
        transport: {
            read: function (options) {
                dataFactory.getHierarchyDetailsUnderInvertedProducts($rootScope.selecetedCatalogId, $scope.hierarchyDetails.Data).success(function (response) {
                    options.success(response.m_Item1);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                fields: {
                    CATEGORY_ID: { type: "string" },
                    CATEGORY_NAME: { type: "string" }
                }
            }
        }
    });
    $scope.importSuccessResultGridOptions = {
        type: "json",
        dataSource: $scope.importSuccessResultDatasource,
        autoBind: false, resizable: true,
        sortable: true,
        pageable: false,
        serverPaging: false,
        serverSorting: false,
        scrollable: true,
        columns: [
        { field: "CATEGORY_ID", title: "CATEGORY_ID", width: "100px" },
          { field: "CATEGORY_NAME", title: "CATEGORY_NAME", width: "100px" }
        ]
    };
    $scope.invertedproductsNavigate = function () {
        $scope.prodid = $scope.storeHierarchyDetails.PRODUCT_ID;
        dataFactory.ProdFamilyAssociatedCategory($scope.prodid, $rootScope.selecetedCatalogId).success(function (response) {
            rebind();
            $rootScope.LoadSubProductGrid = false;
            $rootScope.clickSelectedItems(response[0], false, false);
            $('#tabstrip').show();
            $('#columnsetup').hide();
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = true;
            $('#sampleProdgrid').css('max-width', '1220px');
            $('#sampleProdgrid tbody').css('max-width', '1220px');
        });
    }

    $scope.availableInvertedProductAttributesOptions = new kendo.data.DataSource({
        type: "json",
        pageable: true, pageSize: 10, autoBind: false,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getAttributeListUnderCatalog($rootScope.selecetedCatalogId, $scope.selectedTemplateId).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    ATTRIBUTE_NAME: { editable: false }
                }
            }
        }
    });

    $scope.AvailableInvertedProductAttributesOptions = {
        dataSource: $scope.availableInvertedProductAttributesOptions,
        pageable: { buttonCount: 5 }, autoBind: false,
        filterable: { mode: "row" },
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateInvertedProductAttributesSelection($event, this)"></input>', filterable: false },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", filterable: true },
        ]
    };
    $scope.invertedProductsAttribute = function () {

        $scope.attributeForInvertedProducts.open();
        $("#newTemplateNameTextbox").hide();
        $("#readonlyTemplateNameTextbox").show();
        $scope.makeTrue = true;
        $scope.selectedInvertedProductAttributes.read();
        $scope.availableInvertedProductAttributesOptions.read();

    };

    $scope.selectedInvertedProductAttributes = new kendo.data.DataSource({
        type: "json",
        pageable: true, pageSize: 10, autoBind: false,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getSelectedInvertedProductAttributes($rootScope.selecetedCatalogId, $scope.selectedTemplateId).success(function (response) {
                    if (response.length == 0) {

                        $("#readonlyTemplateNameTextbox").hide();
                        $("#newTemplateNameTextbox").show();
                        $scope.invertedProductTemplateName = null;
                    }
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    ATTRIBUTE_NAME: { editable: false }
                }
            }
        }
    });

    $scope.SelectedInvertedProductAttributesOptions = {
        dataSource: $scope.selectedInvertedProductAttributes,
        pageable: { buttonCount: 5 }, autoBind: false,
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateInvertedProductAttributesRemoveSelection($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
        ]
    };


    $scope.selectedInvertedProductAttributes.read();

    $scope.SaveInvertedProductPublishAttributes = function (templateTextBoxValue) {

        var _check = false;

        for (var i = 0 ; i < $scope.availableInvertedProductAttributesOptions._data.length ; i++) {

            if ($scope.availableInvertedProductAttributesOptions._data[i]["ISAvailable"] == true) {
                _check = true;
                break;
            }

        }

        if (_check == false) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one attribute.',
                type: "info"
            });

            return;
        }




        if (templateTextBoxValue != "" && templateTextBoxValue != undefined && templateTextBoxValue != null) {
            dataFactory.saveSelectedInvertedProductAttributes($rootScope.selecetedCatalogId, $scope.selectedTemplateId, templateTextBoxValue, $scope.availableInvertedProductAttributesOptions._data).success(function (response) {
                if (response != null && response != "AlreadyExsits") {
                    $scope.onchangetemplateListDynaic(response);
                    $("#newTemplateNameTextbox").hide();
                    $("#readonlyTemplateNameTextbox").show();
                    $scope.invertedProductsTemplateList.read();
                    if ($scope.InvertProductSearch != undefined && $scope.InvertProductSearch != "") {
                        $scope.SearchProduct($scope.InvertProductSearch);
                    } else {
                        $scope.InvertedData();
                    }
                    $scope.selectedInvertedProductAttributes.read();
                    $scope.availableInvertedProductAttributesOptions.read();
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Saved successfully.',
                        type: "info"
                    });
                }
                else if (response != null && response == "AlreadyExsits") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Template Already Exists.',
                        type: "info"
                    });

                    return;
                }
                else {
                    return;
                }
            }).error(function (response) {
                options.success(response);
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter the template name.',
                type: "info"
            });
        }
    }
    $scope.invertedProductsTemplateList = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.invertedProductsTemplateList($scope.SelectedCatalogId).success(function (response) {
                    options.success(response);
                    for (var i = 0; i < response.length ; i++) {


                        if (response[i].TEMPLATE_NAME === $scope.invertedProductTemplateName) {
                            $scope.selectedTemplateId = response[i].TEMPLATE_ID;
                            $("#selectedtemplatename").data("kendoDropDownList").select(i + 1);
                        }
                        if ($scope.invertedProductTemplateName == undefined && response.length > 0) {

                            $scope.invertedProductTemplateName = response[1].TEMPLATE_NAME;
                        }
                    }



                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.updateInvertedProductAttributesSelection = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);

    };
    $scope.updateInvertedProductAttributesRemoveSelection = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };
    $scope.DeleteInvertedProductPublishAttributes = function () {

        if ($scope.selectedInvertedProductAttributes._data.length != 0) {
            var Isvailable = false;
            for (var i = 0 ; i < $scope.selectedInvertedProductAttributes._data.length ; i++) {
                if ($scope.selectedInvertedProductAttributes._data[i]["ISAvailable"] == true) {
                    Isvailable = true;
                }
            }
            if (Isvailable) {
                dataFactory.deleteInvertedProductPublishAttributes($rootScope.selecetedCatalogId, $scope.selectedTemplateId, $scope.selectedInvertedProductAttributes._data).success(function (response) {
                    if (response != null && response == "SUCCESS") {
                        if ($scope.InvertProductSearch != undefined && $scope.InvertProductSearch != "") {
                            $scope.SearchProduct($scope.InvertProductSearch);
                        } else {
                            $scope.InvertedData();
                        }
                        $scope.selectedInvertedProductAttributes.read();
                        $scope.availableInvertedProductAttributesOptions.read();
                        $scope.invertedProductsTemplateList.read();
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Attributes removed successfully.',
                            type: "info"
                        });
                    }
                    else {
                        return;
                    }

                }).error(function (response) {
                    options.success(response);
                });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one attribute..',
                    type: "info"
                });
            }

        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one attribute..',
                type: "info"
            });

        }


    }

    $scope.RefreshAttributeDetails = function () {
        $scope.selectedInvertedProductAttributes.read();
        $scope.availableInvertedProductAttributesOptions.read();
        $scope.invertedProductsTemplateList.read();
        $("#AvailableInvertedProductAttributes").data("kendoGrid").dataSource.filter({});
        $("#SelectedInvertedProductAttributes").data("kendoGrid").dataSource.filter({});
    }
    $scope.CloseAttributeDetails = function () {
        $scope.attributeForInvertedProducts.close();
    }
    $scope.SaveAttributeDetails = function (templateTextBoxValue) {
        var count = 0;
        $scope.saveAttributeValues = [];
        for (var i = 0; i < $scope.availableInvertedProductAttributesOptions._data.length; i++) {
            if ($scope.availableInvertedProductAttributesOptions._data[i].ISAvailable == true) {
                count = count + 1;
            }
        }

        if ($scope.selectedInvertedProductAttributes._data.length > 0) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Save Succcessfully.',
                type: "info"
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one attribute.',
                type: "info"
            });
        }
        $scope.invertedProductsTemplateList.read();
        $scope.InvertedData();
    }
    $scope.onchangetemplateList = function (e) {
        if (e.sender.text() == "----- New -----") {
            $scope.selectedTemplateId = 0;
            $("#readonlyTemplateNameTextbox").hide();
            $("#newTemplateNameTextbox").show();
            $scope.invertedProductTemplateName = null;
            $scope.makeTrue = false;
            // $scope.InvertedData();
        }
        else {
            $scope.convertInt = e.sender.value();
            $scope.selectedTemplateId = parseInt($scope.convertInt);
            $("#newTemplateNameTextbox").hide();
            $("#readonlyTemplateNameTextbox").show();
            $scope.invertedProductTemplateName = e.sender.text();
            $scope.makeTrue = true;
            // $scope.InvertedData();
        }
        if ($scope.InvertProductSearch != undefined && $scope.InvertProductSearch != "") {
            $scope.SearchProduct($scope.InvertProductSearch);
        } else {
            $scope.InvertedData();
        }
        $scope.selectedInvertedProductAttributes.read();
        $scope.availableInvertedProductAttributesOptions.read();

    }
    $scope.DeleteInvertedTemplateList = function () {

        var selectingTemplateID = $scope.selectedTemplateId;
        if (selectingTemplateID != null && selectingTemplateID != undefined && selectingTemplateID != 0) {
            dataFactory.deleteSelectedTemplateName($rootScope.selecetedCatalogId, selectingTemplateID).success(function (response) {
                if (response != null && response == "SUCCESS") {
                    $scope.selectedTemplateId = 0;
                    $scope.invertedProductTemplateName = null;
                    $("#readonlyTemplateNameTextbox").hide();
                    $("#newTemplateNameTextbox").show();
                    if ($scope.InvertProductSearch != undefined && $scope.InvertProductSearch != "") {
                        $scope.SearchProduct($scope.InvertProductSearch);
                    } else {
                        $scope.InvertedData();
                    }
                    $scope.selectedInvertedProductAttributes.read();
                    $scope.availableInvertedProductAttributesOptions.read();
                    $scope.invertedProductsTemplateList.read();
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Template name deleted successfully.',
                        type: "info"
                    });
                }
                else {
                    return;
                }

            }).error(function (response) {
                options.success(response);
            });
        }
    }
    $scope.invprodsetWidthReset = function (event) {

        var resizeheader = $('[id*="invprodresize_"]');
        var resizebody = $('[id*="invprodresize1_"]')

        if (resizeheader != null && resizeheader != undefined) {
            angular.forEach(resizeheader, function (rowVal) {
                rowVal.style.width = '100px';
            });
        }
        if (resizebody != null && resizebody != undefined) {
            angular.forEach(resizebody, function (rowVal) {
                rowVal.style.width = '100px';
            });
        }
    }
    $scope.onchangetemplateListDynaic = function (data) {

        $scope.convertInt = data;
        $scope.selectedTemplateId = parseInt($scope.convertInt);
        $("#newTemplateNameTextbox").hide();
        $("#readonlyTemplateNameTextbox").show();
        //  $scope.invertedProductTemplateName = e.sender.text();
        $scope.makeTrue = true;
        //$scope.InvertedData();

        if ($scope.InvertProductSearch != undefined && $scope.InvertProductSearch != "") {
            $scope.SearchProduct($scope.InvertProductSearch);
        } else {
            $scope.InvertedData();
        }


        //    $scope.selectedInvertedProductAttributes.read();
        //   $scope.availableInvertedProductAttributesOptions.read();

    }
    //$scope.ProductPreview = function (data) {
    //    var Cat_Id = $rootScope.selecetedCatalogId;

    //    dataFactory.getPdfXpressdefaultTemplate(Cat_Id).success(function (response) {
    //        if (response == "" || response == null) {
    //            $scope.btnProductPreview1(data);
    //        }
    //        else {
    //            $scope.PreviewProductPdf(data);
    //        }

    //    });

    //}
    //$scope.btnProductPreview1 = function (data) {
    //    $('#selectedattributeproducts').hide();
    //    $('#associatedgrid').hide();
    //    $('#dynamictable').show();
    //    $('#hide').show();
    //    $("#dynamictable").addClass("col-sm-6");
    //    $("#dynamictable").removeClass("col-sm-12");
    //    $("#table10").addClass("col-sm-6");
    //    $("#table10").removeClass("col-sm-12");
    //    $("#table10").addClass("grid");
    //    $('#invertproductpreview').show();
    //    $("#invertproductpreview").css("display", "block");
    //    dataFactory.ProductPreview(data.Data.CATALOG_ID, data.Data.FAMILY_ID, data.Data.PRODUCT_ID, $rootScope.ProductLevelMultipletablePreview, $scope.selecetedCategoryId).success(function (response) {
    //        $scope.invertproductpreview10Text = response.replace("<b>ITEM#</b>", "<b>" + $localStorage.CatalogItemNumber + "</b>");
    //    }).error(function (error) {
    //        options.error(error);
    //    });
    //};
    $scope.ProductPreview = function (data) {
        var Cat_Id = $rootScope.selecetedCatalogId;
        var TYPE = 'PRODUCT';
        var Id = data.Data.FAMILY_ID;

        //  dataFactory.getPdfXpressdefaultTemplate(Cat_Id).success(function (response) {
        dataFactory.getPdfXpressdefaultTemplate_Product(TYPE, Id,Cat_Id).success(function (response) {

            if (response == "" || response == null) {
                $scope.btnProductPreview1(data);
            }
            else {
                $scope.PreviewProductPdf(data);
            }

        });

    }
    $scope.saveProductSessionForPdfXpress = function (data) {
        if ($scope.currentProductId == null || $scope.currentProductId == '') {
            $scope.currentProductId = 0;
        }

        var Id = data.Data.FAMILY_ID;
        var category_Id = data.Data.CATEGORY_ID;
        var category_Name = $localStorage.CategoryID;
        var Family_Name = data.Data.FAMILY_ID;

        var countList = 0;
        var listSelecteddata = category_Name.split('~');
        var listSelected = category_Name.split('~');

        angular.forEach(listSelected, function (value) {
            angular.forEach(listSelecteddata, function (value1) {
                if (value == value1) {
                    countList = countList + 1;
                }
            });
        });
        var Catalog_ID = $localStorage.getCatalogID;
        dataFactory.setPdfXpressType("PRODUCT", listSelected[0], Family_Name, $scope.currentProductId, Catalog_ID).success(function (response) {

            window.open("../Category/PdfPreviewProduct?countList=" + countList, '_blank');

        });
    }

    //Jothipriya NOV-19-2021 Product list preview start
    $scope.PreviewProductPdf = function (data) {
        var TYPE = 'PRODUCT';
        var Id = data.Data.FAMILY_ID;;
        var Catalog_id = $localStorage.getCatalogID;

        dataFactory.getPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {

            if (response != "") {
                $scope.SelectedFileForUpload = response;
                $scope.UploadFile($scope.SelectedFileForUpload, false);
            }
        });
        $scope.currentProductId = data.Data.PRODUCT_ID;
        $scope.currentFamilyIdPdf = data.Data.FAMILY_ID;
        $scope.saveProductSessionForPdfXpress(data);
        $scope.Message = "";
        if ($rootScope.SelectedFileForUpload != undefined)
            $rootScope.UploadFile($rootScope.SelectedFileForUpload, false);
    };

    $scope.saveProductSessionForPdfXpress = function (data) {
        if ($scope.currentProductId == null || $scope.currentProductId == '') {
            $scope.currentProductId = 0;
        }
        var Id = data.Data.FAMILY_ID;
        var category_Id = data.Data.CATEGORY_ID;
        var category_Name = $localStorage.CategoryID;
        var Family_Name = data.Data.FAMILY_ID;
        var countList = 0;
        var listSelecteddata = category_Name.split('~');
        var listSelected = category_Name.split('~');

        angular.forEach(listSelected, function (value) {
            angular.forEach(listSelecteddata, function (value1) {
                if (value == value1) {
                    countList = countList + 1;
                }
            });
        });
        var Catalog_ID = $localStorage.getCatalogID;
        dataFactory.setPdfXpressType("PRODUCT", listSelected[0], Family_Name, $scope.currentProductId, Catalog_ID).success(function (response) {
            window.open("../Category/PdfPreviewProduct?countList=" + countList, '_blank');
        });
    }
    //Jothipriya NOV-19-2021 Product list end 

    $scope.btnProductPreview1 = function (data) {

        $('#selectedattributeproducts').hide();
        $('#associatedgrid').hide();
        $('#dynamictable').show();
        $('#hide').show();
        $("#dynamictable").addClass("col-sm-6");
        $("#dynamictable").removeClass("col-sm-12");
        $("#table10").addClass("col-sm-6");
        $("#table10").removeClass("col-sm-12");
        $("#table10").addClass("grid");
        $('#invertproductpreview').show();
        $("#invertproductpreview").css("display", "block");


        dataFactory.ProductPreview(data.Data.CATALOG_ID, data.Data.FAMILY_ID, data.Data.PRODUCT_ID, $rootScope.ProductLevelMultipletablePreview, $scope.selecetedCategoryId).success(function (response) {
            $scope.ProductPreviewText = response.replace("<b>ITEM#</b>", "<b>" + $localStorage.CatalogItemNumber + "</b>");
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.hideClick = function (e) {
        $('#selectedattributeproducts').hide();
        $('#associatedgrid').hide();
        $('#dynamictable').show();
        $('#hide').show();
        $("#dynamictable").removeClass("col-sm-6");
        $("#dynamictable").addClass("col-sm-12");
        $('#invertproductpreview').hide();
        $("#table10").removeClass("col-sm-6");
        $("#table10").addClass("col-sm-12");
        $("#table10").removeClass("grid");
    };
    $scope.CancelProductItem = function () {
        $('#associatedgrid').hide();
        $('#dynamictable').show();
        $('#hide').show();
        $("#dynamictable").removeClass("col-sm-6");
        $("#dynamictable").addClass("col-sm-12");
        $('#selectedattributeproducts').hide();
        $("#table10").removeClass("col-sm-5");
        $("#table10").removeClass("col-sm-6");
        $("#table10").addClass("col-sm-12");
        $("#table10").removeClass("grid");
        $("#PagingBottom").css("display", "block");
        $("#PagingBottom").show();
    }
    $scope.prodconfigselectedId = function (product_id, e) {
        if (e.currentTarget.checked === true) {
            $scope.productconfig_ids = $scope.productconfig_ids + "," + product_id;
            $scope.productconfig_ids = $scope.productconfig_ids.replace(",,", ",");
        } else {
            var val = $scope.productconfig_ids;
            var res = val.split(",");
            var Product_IDs = $scope.productconfig_ids;
            if (res.length > 0) {
                for (var i = 0; i < res.length; i++) {
                    if (res[i] === product_id) {
                        Product_IDs = $scope.productconfig_ids.replace(product_id, "");
                    }
                }
            }
            Product_IDs = Product_IDs.replace(",,", ",");
            $scope.productconfig_ids = Product_IDs;
        }

    };
    $scope.Addtoprodctconfigurator = function (e) {

        if ($scope.Family_ID != 0 && $scope.product_ids.trim() != "") {

            dataFactory.AddProductconfigurator($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.product_ids, $scope.selecteddRow).success(function (response) {
                if (response != null) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });

                }
            });

        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select the products.',
                type: "info"
            });

        }
    };
    $scope.ProductSortOrderForInvertedProducts = new kendo.data.DataSource({
        type: "json", autoBind: false,
        serverFiltering: false,
        transport: {
            read: function (options) {
                dataFactory.getProductSortorder($scope.getSortFamilyId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.refershtable = function () {
        $rootScope.tblInvertedGrid.reload();
    }

    $scope.LoadSubproductData = function () {
        if ($scope.ProdCurrentPageSP == null) {
            $scope.ProdCurrentPageSP = 1;
        }
        dataFactory.Getsubproductspecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId, $scope.ProdCurrentPageSP, $scope.ProdCountPerPageSP).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $rootScope.SubprodData = obj;
            $rootScope.columnsSubAtt = response.Data.Columns;
            angular.forEach($rootScope.SubprodData, function (value) {
                value.SORT = parseFloat(value.SORT);
            });
            if ($rootScope.invertProdId == undefined) {
                if ($rootScope.SubprodData.length == 1) {
                    $('#cboProdPageCountSPup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageSP == null ? 1 : $scope.ProdCurrentPageSP == '' ? 1 : parseInt($scope.ProdCurrentPageSP);
                    $('#cboProdPageCountSPdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageSP == null ? 1 : $scope.ProdCurrentPageSP == '' ? 1 : parseInt($scope.ProdCurrentPageSP);;
                }
                return true;
            }
            dataFactory.GetPagenumberInverted($rootScope.selecetedCatalogId, $localStorage.CategoryID, $rootScope.selecetedFamilyId, $rootScope.invertProdId, "SubProduct").success(function (response1) {

                if (response1 != null && response1 != "null" && response1 != "~") {

                    var obj = response1.split('~');
                    var ProdEditPageloopcnt1 = Math.floor(obj[0] / parseInt($scope.ProdCountPerPage));
                    if ((obj[0] % parseInt($scope.ProdCountPerPage)) > 0) {
                        ProdEditPageloopcnt1 = ProdEditPageloopcnt1 + 1;
                    }
                    if (ProdEditPageloopcnt1 == 1) {
                        var ProdEditPageloopcntMain = Math.floor(obj[2] / parseInt($scope.ProdCountPerPage));
                        if ((obj[2] % parseInt($scope.ProdCountPerPage)) > 0) {
                            ProdEditPageloopcntMain = ProdEditPageloopcntMain + 1;
                        }
                        $('#subproductdiv').show();
                        $('#ColumnSetupSubproducts').hide();
                        if (ProdEditPageloopcntMain == 1) {
                            return true;
                        }
                        $rootScope.pageCountMain = ProdEditPageloopcntMain;
                        var grid = $("#subproductsmaingrid").data("kendoGrid");
                        grid.dataSource.page($rootScope.pageCountMain);
                        return true;
                    }
                    if (ProdEditPageloopcnt1 == 0) {
                        ProdEditPageloopcnt1 = 1;
                    }
                    $rootScope.callProductGridPagingSP(ProdEditPageloopcnt1);
                    if (obj[2] != "") {
                        var ProdEditPageloopcntMain = Math.floor(obj[2] / parseInt($scope.ProdCountPerPage));
                        if ((obj[2] % parseInt($scope.ProdCountPerPage)) > 0) {
                            ProdEditPageloopcntMain = ProdEditPageloopcntMain + 1;
                        }
                        $rootScope.totalProdPageCountSP = ProdEditPageloopcntMain;
                        $rootScope.pageCountMain = ProdEditPageloopcntMain;

                        var grid = $("#subproductsmaingrid").data("kendoGrid");
                        if ($rootScope.pageCountMain != undefined && $rootScope.pageCountMain != "" && $rootScope.pageCountMain != null) {
                            grid.dataSource.page($rootScope.pageCountMain);
                            $rootScope.callProductGridPagingSP(ProdEditPageloopcnt1);
                        }
                    }
                    $('#cboProdPageCountSPup').data("kendoDropDownList").span[0].innerHTML = ProdEditPageloopcnt1;
                    $('#cboProdPageCountSPdown').data("kendoDropDownList").span[0].innerHTML = ProdEditPageloopcnt1;
                }
            });

        }).error(function (error) {
            options.error(error);
        });
    };




    $rootScope.rowselectedEvent = function (product_Id, e, family_Id, clickedData) {

        $scope.selectedRowDynamicAttributes = [];
        // $rootScope.userImageandAttachments = true;
        $('#invertproductpreview').hide();
        $('#associatedgrid').hide();
        $('#dynamictable').show();
        $('#hide').show();
        $("#dynamictable").addClass("col-sm-6");
        $("#dynamictable").removeClass("col-sm-12");
        $("#paging").addClass("col-sm-12");
        $("#table10").addClass("col-sm-5");
        $("#table10").removeClass("col-sm-12");
        $("#table10").addClass("grid");
        $('#selectedattributeproducts').show();
        $("#selectedattributeproducts").css("display", "block");
        $('#filterAttributePack').show();
        $('#invertedselectedattributenoresultfound').hide();
        $('#PagingBottom').css("display", "none");
        $('#showbutton').show();
        $('#showbutton1').show();
        $('#selectedattributenoresultfound').hide();

        $rootScope.getProduct_id = product_Id;
        $rootScope.getEventValue = e;
        $rootScope.getFamily_id = family_Id;

       

        $rootScope.getclickedData = clickedData;
        $scope.catalogid = $rootScope.getclickedData.CATALOG_ID;
        attributeWithout = [];
        // $scope.attributesOLD = Object.keys(clickedData);
        dataFactory.GetAttributeDetails(clickedData.CATALOG_ID, clickedData.FAMILY_ID).success(function (response) {
            if (response != null) {
                $scope.selectedRowAttributes = response;
                dataFactory.getNullvalueAttributesForSearch($rootScope.getFamily_id, $rootScope.getProduct_id, $scope.SelectedCatalogId).success(function (response) {
                    $scope.fulldata = response;
                    $scope.attributesOLD = Object.keys(response);

                    attributeWithout = [];
                    var calcAttributestemp = [];
                    angular.forEach($scope.attributesOLD, function (value, key) {
                        if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                            attributeWithout.push(value);
                    });
                    $scope.attributescalc = attributeWithout;
                    $scope.fromPageNo = 0;
                    $scope.toPageNo = 150;
                    if (attributeWithout.length < 150) {
                        $scope.toPageNo = attributeWithout.length;
                    }
                    var attributestemp = [];
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        attributestemp.push(attributeWithout[i]);
                    }
                    $scope.attributes = attributestemp;
                    $scope.AllAttributeValues = $scope.attributes;
                    // To bind top10 vaules in the Grid 
                    $scope.attributesValuesForEdit = [];
                    $scope.ProdEditPageCount = 10;

                    for (var i = 0 ; i < $scope.ProdEditPageCount ; i++) {
                        if ($scope.attributes[i] != undefined)
                            $scope.attributesValuesForEdit.push($scope.attributes[i])
                    }
                    $scope.attributes = $scope.attributesValuesForEdit;
                    // End
                    $scope.attrLength = $scope.attributes.length;
                    $scope.ProdSpecsCountPerPage = "150";
                    $scope.ProdSpecsCurrentPage = "1";

                    $scope.loopCount = [];
                    $scope.loopEditProdsCount = [];
                    var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                    var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                    if ((attributeWithout.length % parseInt($scope.ProdSpecsCountPerPage)) > 0) {
                        loopcnt = loopcnt + 1;
                        loopEditProdsCnt = loopEditProdsCnt + 1;
                    }
                    $scope.ProdSpecsPageCount = $scope.loopCount;
                    $scope.totalSpecsProdPageCount = loopEditProdsCnt;
                    for (var i = 0; i < loopcnt; i++) {
                        if (((i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)) < attributeWithout.length) {
                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)
                            });
                        } else {

                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: attributeWithout.length
                            });
                        }
                    }
                    var theString;
                    $scope.selectedRowEdit = {};

                    angular.copy($scope.fulldata, $scope.selectedRowEdit);
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains('__')) {
                            var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                            if (tempattrvals.length != 0) {
                                var uimask = "";
                                theString = tempattrvals[0].ATTRIBUTE_ID;

                                tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                                if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                    theString = 0;
                                }
                                var itemval = attributeWithout[i];
                                if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                                    $scope.item_ = $scope.selectedRowEdit[itemval];
                                }
                                if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                    var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                    pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                    pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                    var reg = new RegExp(pattern);
                                    uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                    tempattrvals[0].attributePattern = reg;
                                } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                    tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                    uimask = tempattrvals[0].DECIMAL;
                                } else {
                                    tempattrvals[0].attributePattern = 524288;
                                    uimask = 524288;
                                }
                                tempattrvals[0].uimask = uimask;
                                $scope.selectedRowDynamicAttributes[theString] = [];
                                $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                                
                            }
                        }
                    }
                    $scope.groups = [];
                    var picklistNAme = "";
                    var attrId = "";

                    $scope.iterationCount = 0;
                    $scope.isUIReleased = 0;
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains("OBJ")) {
                            if (attributeWithout[i].split('__')[2] !== "0") {
                                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                
                                    if (sa.USE_PICKLIST) {
                                        $scope.iterationCount++;
                                    }
                               
                            }
                        }
                    }
                    // $rootScope.selectedRowDynamic = [];

                    $scope.iterationPicklistCount = 0;
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains("OBJ")) {
                            if (attributeWithout[i].split('__')[2] !== "0") {
                                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                if (sa != undefined) {
                                    if (sa.USE_PICKLIST) {
                                        $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                                    }
                                }
                            }
                        }
                    }


                    $scope.updateproductspecs = $scope.fulldata;

                    $("#result").hide();

                    if ($scope.isUIReleased == 0) {

                        $scope.selectedRow = {};


                        angular.copy($scope.updateproductspecs, $scope.selectedRow);


                        blockUI.stop();




                        $rootScope.ajaxLoaderDivShow = false;

                        //$scope.selectedRow["SORT"] = data.$index + 1;
                        //$timeout(function () {
                        //    $scope.selectedRow["SORT"] = data.$index + 1;

                        //}, 50);

                        $timeout(function () {

                            blockUI.stop();
                        }, 100);
                        // $rootScope.ajaxLoaderDivShow = false;

                    }
                }
                )
            };
        }).error(function (error) {
            options.error(error);
        });
        $scope.hierarchy();

        document.getElementById('scrolltotop').scroll({ top: 0, behavior: 'smooth' });

    };


    $scope.GetPickListData = function (attrName, picklistdata) {
        dataFactory.getPickListData(picklistdata).success(function (response) {
            $scope.selectedRowDynamic[attrName] = response;
            $scope.iterationPicklistCount++;
            if ($scope.iterationPicklistCount == $scope.iterationCount) {
                //$rootScope.windows_Open();
                $timeout(function () {
                    $scope.selectedRow = {};
                    angular.copy($scope.updateproductspecs, $scope.selectedRow);
                    blockUI.stop();
                    $scope.isUIReleased = 1;
                }, 200);


            }
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.LoadMoreData = function () {
        if ($scope.ProdEditPageCount != 0) {

            $scope.attributesValuesForEdit = [];
            $scope.ProdEditPageCount = ($scope.ProdEditPageCount + 10);

            for (var i = 0 ; i < $scope.ProdEditPageCount ; i++) {
                if ($scope.AllAttributeValues[i] != undefined) {
                    $scope.attributesValuesForEdit.push($scope.AllAttributeValues[i])
                }
            }
            $scope.attributes = $scope.attributesValuesForEdit;
        }
    }



    $rootScope.rowfunforinverted = function (product_Id, e, family_Id, clickedData) {
        $('#invertproductpreview').hide();
        $('#associatedgrid').hide();
        $('#dynamictable').show();
        $('#hide').show();
        $("#dynamictable").addClass("col-sm-6");
        $("#dynamictable").removeClass("col-sm-12");
        $("#paging").addClass("col-sm-12");
        $("#table10").addClass("col-sm-6");
        $("#table10").removeClass("col-sm-12");
        $("#table10").addClass("grid");
        $('#selectedattributeproducts').show();
        $("#selectedattributeproducts").css("display", "block");
        $('#filterAttributePack').show();
        $('#invertedselectedattributenoresultfound').hide();
        $("#PagingBottom").css("display", "none");


        $rootScope.getProduct_id = product_Id;
        $rootScope.getEventValue = e;
        $rootScope.getFamily_id = family_Id;

        $rootScope.getclickedData = clickedData;
        $scope.catalogid = $rootScope.getclickedData.CATALOG_ID;
        attributeWithout = [];
        // $scope.attributesOLD = Object.keys(clickedData);
        dataFactory.GetAttributeDetails(clickedData.CATALOG_ID, clickedData.FAMILY_ID).success(function (response) {
            if (response != null) {
                $scope.selectedRowAttributes = response;
                dataFactory.getNullvalueAttributesForSearch($rootScope.getFamily_id, $rootScope.getProduct_id, $scope.SelectedCatalogId).success(function (response) {
                    $scope.fulldata = response;
                    $scope.attributesOLD = Object.keys(response);
                });
                attributeWithout = [];
                var calcAttributestemp = [];
                angular.forEach($scope.attributesOLD, function (value, key) {
                    if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                        attributeWithout.push(value);
                });
                $scope.attributescalc = attributeWithout;
                $scope.fromPageNo = 0;
                $scope.toPageNo = 150;
                if (attributeWithout.length < 150) {
                    $scope.toPageNo = attributeWithout.length;
                }
                var attributestemp = [];
                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                    attributestemp.push(attributeWithout[i]);
                }
                $scope.attributes = attributestemp;
                $scope.AllAttributeValues = $scope.attributes;
                // To bind top10 vaules in the Grid 
                $scope.attributesValuesForEdit = [];
                $scope.ProdEditPageCount = 10;

                for (var i = 0 ; i < $scope.ProdEditPageCount ; i++) {
                    if ($scope.attributes[i] != undefined)
                        $scope.attributesValuesForEdit.push($scope.attributes[i])
                }
                $scope.attributes = $scope.attributesValuesForEdit;
                // End
                $scope.attrLength = $scope.attributes.length;
                $scope.ProdSpecsCountPerPage = "150";
                $scope.ProdSpecsCurrentPage = "1";

                $scope.loopCount = [];
                $scope.loopEditProdsCount = [];
                var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                if ((attributeWithout.length % parseInt($scope.ProdSpecsCountPerPage)) > 0) {
                    loopcnt = loopcnt + 1;
                    loopEditProdsCnt = loopEditProdsCnt + 1;
                }
                $scope.ProdSpecsPageCount = $scope.loopCount;
                $scope.totalSpecsProdPageCount = loopEditProdsCnt;
                for (var i = 0; i < loopcnt; i++) {
                    if (((i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)) < attributeWithout.length) {
                        $scope.loopCount.push({
                            PAGE_NO: (i + 1),
                            FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                            TO_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)
                        });
                    } else {

                        $scope.loopCount.push({
                            PAGE_NO: (i + 1),
                            FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                            TO_PAGE_NO: attributeWithout.length
                        });
                    }
                }
                var theString;
                $scope.selectedRowEdit = {};
                angular.copy($scope.fulldata, $scope.selectedRowEdit);
                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                    if (attributeWithout[i].contains('__')) {
                        var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                        if (tempattrvals.length != 0) {
                            var uimask = "";
                            theString = tempattrvals[0].ATTRIBUTE_ID;
                            tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                            if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                theString = 0;
                            }
                            var itemval = attributeWithout[i];
                            if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                                $scope.item_ = $scope.selectedRowEdit[itemval];
                            }
                            if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                var reg = new RegExp(pattern);
                                uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                tempattrvals[0].attributePattern = reg;
                            } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                uimask = tempattrvals[0].DECIMAL;
                            } else {
                                tempattrvals[0].attributePattern = 524288;
                                uimask = 524288;
                            }
                            tempattrvals[0].uimask = uimask;
                            $scope.selectedRowDynamicAttributes[theString] = [];
                            $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                        }
                    }
                }
                $scope.groups = [];
                var picklistNAme = "";
                var attrId = "";
                $scope.iterationCount = 0;
                $scope.isUIReleased = 0;
                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                    if (attributeWithout[i].contains("OBJ")) {
                        if (attributeWithout[i].split('__')[2] !== "0") {
                            var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                            if (sa.USE_PICKLIST) {
                                $scope.iterationCount++;
                            }
                        }
                    }
                }
                // $rootScope.selectedRowDynamic = [];

                $scope.iterationPicklistCount = 0;
                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                    if (attributeWithout[i].contains("OBJ")) {
                        if (attributeWithout[i].split('__')[2] !== "0") {
                            var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                            if (sa.USE_PICKLIST) {
                                $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                            }
                        }
                    }
                }


                $scope.updateproductspecs = $scope.fulldata;
                $("#result").hide();

                if ($scope.isUIReleased == 0) {

                    $scope.selectedRow = {};


                    angular.copy($scope.updateproductspecs, $scope.selectedRow);
                    // $localStorage.selectedRow1 = $scope.selectedRow;
                    //$scope.ProductSortOrder.read();

                    angular.copy($scope.updateproductspecs, $scope.selectedRow);
                    //$scope.hierarchy();

                    //var ddl = $('#dynamicsort').data('kendoDropDownList');
                    //ddl.dataSource.data({}); // clears dataSource
                    //ddl.text(""); // clears visible text
                    //ddl.value("");

                    //  $rootScope.windows_Open();

                    blockUI.stop();




                    $rootScope.ajaxLoaderDivShow = false;

                    //$scope.selectedRow["SORT"] = data.$index + 1;
                    //$timeout(function () {
                    //    $scope.selectedRow["SORT"] = data.$index + 1;

                    //}, 50);

                    $timeout(function () {

                        blockUI.stop();
                    }, 100);
                    // $rootScope.ajaxLoaderDivShow = false;

                }
            }
        }).error(function (error) {
            options.error(error);
        });

    };
    $scope.hierarchy = function () {
        $scope.getfamilyname = [];
        $scope.getfamilyid = [];
        dataFactory.getHierarchyDetailsUnderInvertedProducts($rootScope.selecetedCatalogId, $rootScope.getclickedData).success(function (response) {
            $scope.getfamilyname1 = response;
            $scope.getCategoryIdForSpecUpdate = response[0].CATEGORY_ID;
            for (var i = 0 ; $scope.getfamilyname1.length > i ; i++) {
                if ($scope.getfamilyname1[i].FAMILY_NAME) {
                    $scope.getfamilyname1[i].FAMILY_ID
                    $scope.getfamilyname1[i].CATEGORY_NAME
                    $scope.getfamilyname.push({ "Text": $scope.getfamilyname1[i].CATEGORY_NAME.toString() + '->' + $scope.getfamilyname1[i].FAMILY_NAME.toString(), "Value": $scope.getfamilyname1[i].FAMILY_ID });
                    document.getElementById("attributeName1").innerHTML = $scope.getfamilyname;


                }
            }

            kendoDropDown1();
        })

    }
    $scope.invertedproducts = function (famname) {
        $rootScope.getclickedData.FAMILY_ID = $scope.getfamilyname;
        $rootScope.rowfunforinverted($rootScope.getProduct_id, $rootScope.getEventValue, $scope.getfamilyname, $rootScope.getclickedData);
    }
    $scope.ResetEditClick = function () {
        $scope.searchText = '';
        $scope.callAttributeNameSearch();
    }
    $scope.callAttributeNameSearch = function () {
        $scope.fromPageNo = 1;
        $scope.toPageNo = 1;
        $scope.currentPage = -1;
        var attributeWithout = [];
        var attributesearch = [];

        angular.forEach($scope.attributesOLD, function (value, key) {
            if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2'))
                attributeWithout.push(value);

        });
        angular.forEach(attributeWithout, function (value, key) {
            if (value.toLowerCase().indexOf($scope.searchText.toLowerCase()) > -1 || value.toUpperCase() == "PUBLISH2WEB" || value.toUpperCase() == "PUBLISH2PRINT" || value.toUpperCase() == "PUBLISH2PDF" || value.toUpperCase() == "PUBLISH2EXPORT" || value.toUpperCase() == "PUBLISH2PORTAL") {
                attributesearch.push(value);
            }
        });
        attributeWithout = attributesearch;
        if (attributeWithout == 0) {
            $("#result").show();
        }
        else {
            $("#result").hide();
        }
        $scope.toPageNo = attributeWithout.length;
        var attributestemp = [];
        for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
            attributestemp.push(attributeWithout[i]);
        }
        $scope.attributes = attributestemp;
        if ($scope.attributes.length === 5) {
            $('#selectedattributenoresultfound').show();
        }
        else {
            $('#selectedattributenoresultfound').hide();
        }
        for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
            var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
            if (tempattrvals.length > 0) {
                var theString = tempattrvals[0].ATTRIBUTE_ID;
                var uimask = "";
                tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                    theString = 0;
                }
                if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                    var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                    pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                    pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                    var reg = new RegExp(pattern);
                    uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                    tempattrvals[0].attributePattern = reg;
                } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text')) {
                    tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                    uimask = tempattrvals[0].DECIMAL;
                } else {
                    tempattrvals[0].attributePattern = 524288;
                    uimask = 524288;
                }
                tempattrvals[0].uimask = uimask;
                $scope.selectedRowDynamicAttributes[theString] = [];
                $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
            }
        }
        for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
            if (attributeWithout[i].contains("OBJ")) {
                if (attributeWithout[i].split('__')[2] !== "0") {
                    var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                    if (sa.USE_PICKLIST) {
                        var theString = sa.ATTRIBUTE_ID;
                        $scope.GetPickListData(theString, sa.PICKLIST_NAME);
                    }
                }
            }
        }
        $scope.searchText = '';
    };
    function kendoDropDown1() {
        $("#attributeName1").kendoDropDownList({
            dataSource: $scope.getfamilyname,
            dataTextField: "Text",
            dataValueField: "Value",
            height: 400
        });
    }

    $rootScope.SaveOrUpdateProductforinvertItem = function (row) {
        $scope.productGridSearchValue = "";

        $scope.oldAttributeValues = $scope.updateproductspecs;
        $scope.refershtable();
        $scope.stringValue;
        var displayAttribures = "";
        var tt = $('#textboxvalue4');
        if ($scope.val_type === "true") {
            angular.forEach(row, function (value, key) {
                if (key.contains("OBJ")) {
                    var sa = [key.split('__')[2]];
                    if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0) {
                        var id = sa;
                        if ($("#textBoxValue" + id)[0].nextElementSibling != null) {
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != null && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != undefined && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {
                                if (!$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Value is too long") && !value.includes("Value is too long")) {
                                    row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
                                }
                                else {
                                    row[key] = value;
                                }

                                if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != null && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                    row[key] = "";
                                }
                            }
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != null && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                row[key] = "";
                            }
                        }
                    }
                    else if ($("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling != null && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {
                        row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
                    }
                    if (sa != undefined) {
                        //To check if the attribute is a value required field
                        if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {

                            if (displayAttribures == '') {
                                displayAttribures = "The following attribute values are required,\n";
                            }
                            displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                        }
                    }

                }

            });

            $scope.ParentProductId = '0';
            $scope.blurCheck = false;
            
            if (displayAttribures == '') {
               $timeout(function(){
                dataFactory.SaveProductSpecs(row, $scope.getCategoryIdForSpecUpdate, $scope.ParentProductId, $scope.blurCheck, $scope.oldAttributeValues).success(function (response) {
                    if (response != null) {
                        //$rootScope.tblInvertedGrid.reload();

                        $scope.blurCheck = false;
                        if ($scope.ParentProductId !== 0) {
                        }
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"

                        });
                        //$rootScope.LoadInvertedprodData();
                        $rootScope.LoadInvertedData1();


                    }


                }).error(function (error) {
                    options.error(error);
                });
               }, 100);
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + displayAttribures + '.',
                    type: "info"
                });

            }
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter Data using a valid format.',
                type: "error"
            });
        }

        // --------------------------------Undo family and Product Tab------------------------------------------------------//
        $scope.productSpec = $rootScope.selectedProductAttributeRow;
        $rootScope.ProductDetailsValue = $rootScope.selectedProductAttributeRow;
        $rootScope.baseProductRowAttributes = $scope.selectedRowAttributes;
        // --------------------------------Undo family and Product Tab------------------------------------------------------//
        // -----------------------------Only Undo Product Tab---------------------------------------------------------//
        if ($rootScope.undoProductFlag == "0") {
            $scope.productSpec = angular.copy(row);
            Productdetails.push($scope.productSpec);
            $scope.detailslength = Productdetails.length;
            if (Productdetails.length > 10) {
                Productdetails.splice(0, 1);
            }
            $rootScope.UndoProductDetailsValueOnly = {};
            $rootScope.UndoProductDetailsValueOnly = $scope.productSpec;
            $localStorage.ProductDetailsValueOnly = Productdetails;
            $scope.$emit("SendOnlyUndoProductGridDetails", $rootScope.UndoProductDetailsValueOnly);
        }
        else {
            $rootScope.undoProductFlag = "0";
        }

        // -----------------------------Only Undo Product Tab---------------------------------------------------------//
        $('.block-ui-overlay').show();
        $('.imageloader').show();

    };
    $scope.OpenPopupWindow = function (id) {
        //
        //  $window.open("../Views/App/Partials/ImageManagementPopup.html", "popup", "width=300,height=200,left=10,top=150");
        //$scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
        //$scope.winManageDrive.center().open();
        $scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
        $scope.winManageDrive.title("Asset Management");
        $scope.winManageDrive.center().open();
        $scope.driveMahementIsVisible = true;

        if (id.Name == "" || id.Name == undefined) {
            $rootScope.paramValue = id;
            $scope.attribute = id;
        }
        else {

            $rootScope.paramValue = id.Name;
            $scope.attribute = id.Value;
        }
    };
    $scope.selectedRowSub = {};
    $rootScope.SaveNewProdFileSelection = function (fileName, path) {
        //
        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            document.getElementById($scope.attribute).value = path;
            $scope.selectedRow[$scope.attribute] = path;
            //$scope.attribute = path;
            $rootScope.productAttachPath = path;
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;
    };
    $rootScope.SaveNewSubFileSelection = function (fileName, path) {
        //
        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            document.getElementById($scope.attribute).value = path;
            $scope.selectedRow[$scope.attribute] = path;
            //$scope.attribute = path;
            $rootScope.productAttachPath = path;
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;

    };
    $rootScope.SaveProductFileSelection = function (fileName, path) {


        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            angular.forEach($scope.selectedRow, function (value, key) {
                if (key == $rootScope.paramValue) {
                    $scope.selectedRow[key] = path;
                    // $scope.$apply();
                }
            });
            angular.forEach($scope.selectedRowSub, function (value, key) {
                if (key == $rootScope.paramValue) {
                    $scope.selectedRowSub[key] = path;
                    // $scope.$apply();
                }
            });
            $rootScope.productAttachPath = path;
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;

    };

    $scope.clearImagePath = function (e) {
        //
        document.getElementById(e).value = "";
        $scope.selectedRowSub[e] = "";
        $scope.selectedRow[e] = "";
    };
    $scope.importproducts = function () {
        $('#importshow').show();
        $('#hide').hide();
        $scope.importinvert = true;
        $scope.AttrUploadDiv = true;
        $scope.userbuttondiv = true;
        $scope.Importdiv = true;
        $scope.hideImportType = true;
        $scope.importtype = "Attribute";
        $rootScope.importtypeValue = "Attribute";
        $scope.$emit("MyFunction");



    }
    $scope.UIMask = function (decimal, number) {
        var uimask = "";
        var uimask1 = "";
        for (var i = 0; i < decimal; i++) {
            uimask += "x";
        }
        for (var i1 = 0; i1 < number; i1++) {
            uimask1 += "x";
        }
        if (number != 0) {
            return uimask + "." + uimask1;
        } else {
            return uimask;
        }
    };
    $scope.invertedProductsRefresh = function () {
        $scope.InvertedData();
        document.getElementById('txtSearch').value = "";
        $scope.InvertProductSearch = "";
    }
}]);