﻿
LSApp.controller("ExportController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$http', '$localStorage', '$rootScope', '$filter', function ($scope, $window, $location, $routeParams, dataFactory, $http, $localStorage, $rootScope, $filter) {


    // --------------------------------------------------- Start Common Function --------------------------------------------------------------------------------
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    $scope.EnableExportBatch = false;
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    $scope.schedule = false;
    $scope.hideProjectName = false;
    $scope.SelectedCatalogName = "";
    $scope.SelectedcategoryId = "All";
    $scope.SelectedcategoryName = "";
    $scope.SelectedprojectId = 0;
    $scope.SelectedprojectName = "";
    $scope.SelectedCatalogId = 0;
    $scope.projectId = 0;
    $scope.SelectedcategoryIdexp = "";
    $scope.selectedAttributeData = [];
    $scope.Atributeidsexportew = "";
    $scope.allowselection = "";
    $scope.exporttypes = "1";
    $scope.chooseFile = "2";
    $scope.outputformat = "XLS";
    $scope.Delimiters = "\t";
    $scope.enabledelimiters = true;
    $scope.daterange = true;
    $scope.partialexport = false;
    $scope.fromdate = "";
    $scope.todate = "";
    $scope.exportids = "No";
    $scope.hierarchy = "No";
    $scope.nxtbtn = true;
    $scope.Expbtn = false;
    $scope.Category_ids = [];
    $scope.Family_ids = [];
    $scope.Familyids = [];




    $scope.exporttabledesigner = "No";
    $scope.exportsplitsheet = "No";
    $scope.singlesheet = "No";
    $scope.splitsheetshow = false;
    $scope.disableexpids = true;
    $scope.hierarchyshow = false;
    $scope.enableColumndelimiters = true;
    $scope.Columndelimeter = "";
    //$scope.Expbutton = true;



    $scope.displaydateonselection = false;
    $scope.exporsubproducts = "No";
    $scope.Newproject = false;
    $("#datepickerfrom").hide();
    $("#datepickerto").hide();
    $scope.Export = {
        PROJECT_NAME: "",
        COMMENTS: "",
        exportAttributes: "Column",
        applyFilter: "No",
        familyFilter: "No"
    };
    $scope.ExportTemplate = {
        PROJECT_ID: "",
        PROJECT_NAME: "",
        COMMENTS: "",
        exportAttributes: "Column",
        applyFilter: "No",
        familyFilter: "No",
        SelectedCatalogId: "",
        OutputFormat: "",
        hierarchy: "",
        exportids: "",
        subProduct: "",
        exporttypes: "",
        Delimiters: "", fromdate: "", todate: ""
    };
    if ($scope.SelectedCatalogId == '') {

        $scope.SelectedCatalogId = $localStorage.getCatalogID;
    }
    $scope.catalogDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetCatalogDetails($scope.SelectedItem, $scope.getCustomerID).success(function (response) {

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    //$scope.prodfamilyattrdataSource = new kendo.data.DataSource({
    //    type: "json",
    //    serverFiltering: true,
    //    sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
    //    transport: {
    //        read: function (options) {
    //            dataFactory.GetSelectedattributes($scope.SelectedCatalogId, $scope.SelectedprojectId).success(function (response) {
    //                options.success(response);
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }, schema: {
    //        model: {
    //            id: "ATTRIBUTE_ID",
    //            fields: {
    //                ISAvailable: { type: "boolean" },
    //                CATALOG_ID: { editable: false },
    //                FAMILY_ID: { editable: false },
    //                ATTRIBUTE_NAME: { editable: false },
    //                ATTRIBUTE_TYPE: { editable: false }
    //            }
    //        }
    //    },
    //    group: {
    //        field: "ATTRIBUTE_TYPE", aggregates: [
    //           { field: "ATTRIBUTE_TYPE", aggregate: "count" }
    //        ]
    //    }

    //});
    $scope.genericTemplateDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GettemplateDetails($localStorage.getCatalogID, "GenericTool").success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.templateDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GettemplateDetails($localStorage.getCatalogID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.btnLoad = function () {

        if ($scope.SelectedprojectId !== 0 && $scope.SelectedprojectId !== "") {
            $("#divselectcatalogattributes").show();
            $("#divselectcatalog").hide();
            $scope.GetAllCatalogattributesdataSource.read();
            $scope.prodfamilyattrdataSource.read();
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select An existing Project.',
                //type: "info"
            });
        }
    };
    $scope.btnNext = function (Form) {
        $rootScope.URL = Form.url.$modelValue;
        $rootScope.USERNAME = Form.username.$modelValue;
        $rootScope.PASSWORD = Form.password.$modelValue;
        $rootScope.ftp = Form.chooseFile.$modelValue;
        if ($rootScope.ftp == 1) {
            if (Form.description.$modelValue.length < 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Enter the template name',
                    type: "info"
                });
                return;
            }
            else {
                $rootScope.templateName = Form.description.$modelValue;
            }
        }
        if ($scope.exporttypes == "4") {
            if ($scope.fromdate == "" && $scope.todate == "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Select the date',
                    type: "info"
                });
                return;
            }
        }


        if ($rootScope.EnableSubProduct == true) {
            $scope.CheckEnableSubProduct = Form.exporsubproducts.$modelValue;
            if ($scope.CheckEnableSubProduct == "No") {
                $scope.CheckEnableSubProduct = 'false';
            } else if ($scope.CheckEnableSubProduct == "Yes") {
                $scope.CheckEnableSubProduct = 'true';
            }
        }
        	
        dataFactory.ProdCount($scope.SelectedCatalogId).success(function (response) {
            if ($scope.exporttypes == 1) {
                if (response > 500000) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select the "category export" if  product count is greater than five lakhs.',
                        type: "error"
                    });
                    stop.propogation();
                }
            }
        $scope.dataSource.read();
        $scope.exportFinal = false;
        $("#treeViewDivAttribute").hide();
        var exportDetails = JSON.stringify(Form);
        if ($scope.Export.PROJECT_NAME == $('#projectName').data('kendo-drop-down-list').text()) {
            $("#divselectcatalogs").hide();
            $("#divselectcatalogattributes").show();
            $("#divselectcatalog").hide();
            $("#treeViewDivAttribute").show();
            // Start - > update existing templete value .
            // $("#divselectcatalogs").hide();
            // $("#divselectcatalogattributes").show();
            //  $("#divselectcatalog").hide();
            $scope.PROJECT_ID = Form.projectName.$modelValue
            if ($scope.selectedFamilyId === undefined) {
                $scope.selectedFamilyId = 0;
            }
            var alldata = [];
            alldata.push($scope.Export);
            alldata.push($scope.prodfamilyattrdataSource._data);
            if ($scope.exporttypes == "3") {
                $scope.partialexport = true;
            }
            $scope.ExportTemplate.PROJECT_ID = $scope.PROJECT_ID;
            $scope.ExportTemplate.PROJECT_NAME = $scope.Export.PROJECT_NAME;
            $scope.ExportTemplate.SelectedCatalogId = $scope.SelectedCatalogId;
            $scope.ExportTemplate.OutputFormat = $scope.outputformat;
            $scope.ExportTemplate.exportAttributes = $scope.Export.exportAttributes;
            $scope.ExportTemplate.hierarchy = $scope.hierarchy;
            $scope.ExportTemplate.applyFilter = $scope.Export.applyFilter;
            $scope.ExportTemplate.exportids = $scope.exportids;

            if ($scope.EnableSubProduct == true) {
                $scope.Export.exporsubproducts = $scope.CheckEnableSubProduct;
                $scope.ExportTemplate.subProductRequired = $scope.Export.exporsubproducts;
            }
            $scope.ExportTemplate.exporttypes = $scope.exporttypes;
            $scope.ExportTemplate.Delimiters = $scope.Delimiters;
            $scope.ExportTemplate.fromdate = $scope.fromdate;
            $scope.ExportTemplate.todate = $scope.todate;
            $scope.ExportTemplate.COMMENTS = $scope.Export.COMMENTS;
            
            
            // Tochange the current date format
             if ($scope.exporttypes == "4") {
                 
                var changedateformatfromdate = $scope.fromdate;
                var arrayoffromdate = changedateformatfromdate.split('/');
                $scope.fromdate = arrayoffromdate[1] + "/" + arrayoffromdate[0] + "/" + arrayoffromdate[2];
                var changedateformattodate = $scope.todate;
                var arrayoftodate = changedateformattodate.split('/');
                $scope.todate = arrayoftodate[1] + "/" + arrayoftodate[0] + "/" + arrayoftodate[2];
            }
            // End
            dataFactory.SaveExportproject($scope.Export, $scope.SelectedCatalogId, $scope.Export.exportAttributes, $scope.Export.applyFilter, $scope.Export.familyFilter, $scope.SelectedCatalogName,
                $scope.SelectedcategoryId, $scope.selectedFamilyId, $scope.projectId, $scope.SelectedcategoryIdexp, $scope.prodfamilyattrdataSource._data, $scope.selectedAttributeData, $scope.famarray, $scope.ExportTemplate, $scope.fromdate, $scope.todate).success(function (response) {
                    $scope.genericTemplateDataSource.read();
                }).error(function (error) {
                    options.error(error);
                });
            //End
            return true;
        }
        dataFactory.Saveproject($scope.Export, $scope.SelectedCatalogId, 7).success(function (response) {

            if (response === 0) {
                $("#divselectcatalogs").hide();
                $("#divselectcatalogattributes").hide();
                $("#divselectcatalog").show();
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Template Name already exists, please select a different name.',
                    type: "error"
                });
            } else {
                $("#divselectcatalogs").hide();
                $("#divselectcatalogattributes").show();
                $("#divselectcatalog").hide();
                $scope.projectId = response;

                $scope.BatchProjectId = response;

                if ($scope.selectedFamilyId === undefined) {
                    $scope.selectedFamilyId = 0;
                }
                var alldata = [];
                alldata.push($scope.Export);
                alldata.push($scope.prodfamilyattrdataSource._data);
                if ($scope.exporttypes == "3") {
                    $scope.partialexport = true;
                }
                $scope.ExportTemplate.PROJECT_ID = $scope.PROJECT_ID;
                $scope.ExportTemplate.PROJECT_NAME = $scope.Export.PROJECT_NAME;
                $scope.ExportTemplate.SelectedCatalogId = $scope.SelectedCatalogId;
                $scope.ExportTemplate.OutputFormat = $scope.outputformat;
                $scope.ExportTemplate.exportAttributes = $scope.Export.exportAttributes;
                $scope.ExportTemplate.hierarchy = $scope.hierarchy;
                $scope.ExportTemplate.applyFilter = $scope.Export.applyFilter;
                $scope.ExportTemplate.exportids = $scope.exportids;

                if ($scope.EnableSubProduct == true) {
                    $scope.Export.exporsubproducts = $scope.CheckEnableSubProduct;
                    $scope.ExportTemplate.subProductRequired = $scope.Export.exporsubproducts;

                }
                $scope.ExportTemplate.exporttypes = $scope.exporttypes;
                $scope.ExportTemplate.Delimiters = $scope.Delimiters;
                // $scope.ExportTemplate.fromdate = $scope.fromdate;
                // $scope.ExportTemplate.todate = $scope.todate;
                $scope.ExportTemplate.COMMENTS = $scope.Export.COMMENTS;
                // Tochange the current date format

                if ($scope.exporttypes == "4") {
                    var ChangedateFormatfromdate = $scope.fromdate;
                    var arrayOfFromDate = ChangedateFormatfromdate.split('/');
                    $scope.fromdate = arrayOfFromDate[1] + "/" + arrayOfFromDate[0] + "/" + arrayOfFromDate[2];
                    var ChangedateFormatToDate = $scope.todate;
                    var arrayOfToDate = ChangedateFormatToDate.split('/');
                    $scope.todate = arrayOfToDate[1] + "/" + arrayOfToDate[0] + "/" + arrayOfToDate[2];
                }
                // End
                dataFactory.SaveExportproject($scope.Export, $scope.SelectedCatalogId, $scope.Export.exportAttributes, $scope.Export.applyFilter, $scope.Export.familyFilter, $scope.SelectedCatalogName,
                    $scope.SelectedcategoryId, $scope.selectedFamilyId, $scope.projectId, $scope.SelectedcategoryIdexp, $scope.prodfamilyattrdataSource._data, $scope.selectedAttributeData, $scope.famarray, $scope.ExportTemplate, $scope.fromdate, $scope.todate).success(function (response) {
                        $scope.genericTemplateDataSource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
            }
        }).error(function (error) {
            options.error(error);
        });
        $scope.templateDataSource.read();
        $scope.SelectedprojectId = 0;
        $scope.projectId = 0;
        $scope.prodfamilyattrdataSource.read();
        });

     };



    // Tep  - Start

    $scope.btnCancelSample = function () {
        var dropdownlist = $("#projectName").data("kendoDropDownList");
        dropdownlist.select(1);

        $('#divdate').hide();
        $("#divselectcatalogs").hide();
        $("#divselectcatalogattributes").hide();
        $("#divselectcatalog").show();
        $("#treeViewDivAttribute").hide();
    }
    // Tap - End



    $scope.btnCancel = function () {
        //  $('[name="projectName"]').data("kendoDropDownList").value(2);
        $scope.AtributesexportDisplayNames = [];
        if ($scope.Export.PROJECT_NAME == "") {
            var dropdownlist = $("#projectName").data("kendoDropDownList");
            dropdownlist.select();

        }
        else {
            var dropdownlist = $("#projectName").data("kendoDropDownList");
            dropdownlist.select(1);
        }
        // $scope.SelectedprojectId = "";



        $scope.schedule = false;
        $scope.displayselectiontree();
        $scope.CheckOrUncheckBox1 = false;
        $scope.GetAllCatalogattributesdataSource.read();
        $scope.prodfamilyattrdataSource.read();
        $scope.templateDataSource.read();
        $scope.projectDataSource.read();
        // window.location.pathname = 'App/Export';
        $('#divdate').hide();
        $("#divselectcatalogs").hide();
        $("#divselectcatalogattributes").hide();
        $("#divselectcatalog").show();
        $scope.fromdate = "";
        $scope.todate = "";
        $scope.selectallcheckbox = "";
        $scope.CheckOrUncheckBox2 = false;

    };


    $scope.backtoexp = function () {
        window.location.pathname = 'App/Export';
        $("#divselectcatalogs").hide();
        $("#AttributeDisplayGrid").hide();
        $("#divselectcatalogattributes").hide();
        $("#divselectcatalog").show();

        $scope.fromdate = "";
        $scope.todate = "";

    };
    var data = [{ type: "Export", value: "7" }
        //,{ type: "InDesign Catalog", value: "1" },
        //{ type: "PDF Catalog", value: "3" }
        //{ type: "HTML Catalog", value: "9" }
    ];
    // create DropDownList from input HTML element
    $("#projecttype").kendoDropDownList({
        dataTextField: "type",
        dataValueField: "value",
        dataSource: data,
        index: 0
    });

    $scope.catalogChange = function (e) {
        $scope.SelectedCatalogId = e.sender.value();
        $scope.SelectedCatalogName = e.sender.text();
        $scope.CheckOrUncheckBox1 = false;
        $scope.selectallcheckbox = "";
        $scope.CheckOrUncheckBox2 = false;
        if ($scope.SelectedCatalogId != "" && $scope.SelectedCatalogName != "--- Select Catalog ---") {
            $localStorage.getCatalogID = $scope.SelectedCatalogId;
            $scope.categoryDataSource.read();
            $scope.ExporttreeData.read();
            $scope.GetAllCatalogattributesdataSource.read();
            $scope.templateDataSource.read();
            $scope.projectDataSource.read();
            $scope.exportValid = false;
        }
        else {
            $scope.exportValid = true;
        }
    };
    $scope.categoryChange = function (e) {
        $scope.SelectedcategoryId = e.sender.value();
        $scope.SelectedcategoryName = e.sender.text();
        $scope.familyDataSource.read();
    };
    $scope.singlesheetchange = function (singlesheet) {

        if ($scope.singlesheet == "Yes") {
            $scope.hierarchy = "Yes";
            $scope.exportsplitsheet = "No";
            $scope.splitsheetshow = true;
            $scope.hierarchyshow = true;
        }
        else {
            $scope.hierarchy = "No";
            $scope.exportsplitsheet = "No";
            $scope.splitsheetshow = false;
            $scope.hierarchyshow = false;
        }
    }


    //------------------------Export File format selection-------------------------//
    $scope.displayselectiontree = function (e) {
        if ($scope.exporttypes == "2") {
            $scope.ExporttreeData.read();
            $scope.Categoryexport = true;
            $scope.splitoption = true;
            $scope.Expbtn = true;
            $scope.nxtbtn = false;
            $scope.daterange = false;
            $scope.partialexport = false;
            $("#datepickerfrom").hide();
            $("#datepickerto").hide();
            $scope.fromdate = "";
            $scope.todate = "";
            $scope.displaydateonselection = false;
            $("#nextBtn").attr('disabled', 'disabled');
        }
        else if ($scope.exporttypes == "3") {
            $scope.ExporttreeData.read();
            $scope.nxtbtn = true;
            $scope.Expbtn = false;
            $scope.partialexport = true;
            $scope.Categoryexport = false;
            $scope.splitoption = false;
            $scope.daterange = false;
            $("#datepickerfrom").hide();
            $("#datepickerto").hide();
            $scope.fromdate = "";
            $scope.todate = "";
            $scope.displaydateonselection = false;
            $("#nextBtn").attr('disabled', 'disabled');
            chkEnabled2.checked = false;
        }
        else if ($scope.exporttypes == "4") {
            $scope.partialexport = false;
            $scope.nxtbtn = true;
            $scope.Expbtn = false;
            $scope.Categoryexport = false;
            $scope.splitoption = false;
            $scope.daterange = true;
            $scope.displaydateonselection = true;
            $("#datepickerfrom").show();
            $("#datepickerto").show();
            $("#nextBtn").removeAttr('disabled');
        }
        else {
            $scope.SelectedcategoryIdexp = "All";
            $scope.SelectedcategoryId = "All";
            $scope.famarray = [];
            $scope.nxtbtn = true;
            $scope.Expbtn = false;
            $scope.partialexport = false;
            $scope.Categoryexport = false;
            $scope.splitoption = false;
            $scope.daterange = false;
            $("#datepickerfrom").hide();
            $("#datepickerto").hide();
            $scope.fromdate = "";
            $scope.todate = "";
            $scope.displaydateonselection = false;
            $("#nextBtn").removeAttr('disabled');
        }

    };

    //$scope.hierarchychange = function (e) {
    //    if ($scope.hierarchy == "No") {
    //        $scope.exportids = "No";
    //        $scope.disableexpids = true;
    //    }
    //    else {
    //        $scope.exportids = "Yes";
    //        $scope.fdisableexpids = false;

    //    }
    //};
    $scope.exportchangeevent = function (e) {

        if ($scope.outputformat == "Text") {
            $scope.enabledelimiters = false;
        }
        else {
            $scope.enabledelimiters = true;
        }
    };

    $scope.Columndelimeterchangeevent = function (e) {

        if ($scope.Columndelimeter == "Others") {
            $scope.enableColumndelimiters = false;
        }
        else {
            $scope.enableColumndelimiters = true;
        }
    };

    $scope.projectChange = function (e) {
        $scope.SelectedprojectId = e.sender.value();
        $scope.SelectedprojectName = e.sender.text();
    };
    $scope.DeleteTemplate = function (e) {
        var project_id = e.sender._old;
        if (project_id.trim() !== "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to delete the Template " + e.sender.dataSource._data[e.sender.selectedIndex - 1].PROJECT_NAME + "?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        bool = true;
                    }
                    if (bool === true) {
                        dataFactory.DeleteTemplate(project_id, $localStorage.getCatalogID).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            $scope.templateDataSource.read();
                            $scope.ResetPage();
                        });
                    }
                }
            });
        }
    };
    $scope.DeleteProject = function (projectID) {

        if (projectID == "") {
            projectID = $scope.getProjectId;
        }

        if (projectID == null || projectID == "" || projectID == undefined) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Template.',
                type: "info"
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to delete the template?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {

                    var bool = false;
                    if (result === "Yes") {

                        dataFactory.DeleteProject($localStorage.getCatalogID, projectID).success(function (response) {
                            if (response == "Deleted") {
                                $scope.genericTemplateDataSource.read();
                                $scope.ResetPage();
                                //$scope.Newproject = false;
                                //$scope.Export.PROJECT_NAME = "";
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Delete successful.',
                                    type: "info"
                                });
                                $scope.getProjectId = "";
                            }

                            else {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: response + '.',
                                    type: "info"
                                });
                            }
                        });
                    }
                }
            });
        }
    };
    $scope.OpenExportTemplate = function (e) {

        $scope.Newproject = true;
        var project_id = e.sender._old;
        if (project_id.trim() !== "") {
            $scope.SelectedprojectId = project_id;
            $scope.Export.PROJECT_NAME = e.sender.text();
            // $scope.Export.PROJECT_NAME = "New Template";
            dataFactory.OpenTemplate(project_id, $localStorage.getCatalogID, "GenericTool").success(function (response) {

                if (response != null) {
                    var data = response[0];
                    $scope.SelectedprojectId = data.PROJECT_ID;
                    $scope.PROJECT_ID = data.PROJECT_ID;
                    $scope.projectId = data.PROJECT_ID;
                    //  $scope.Export.PROJECT_NAME = data.PROJECT_NAME
                    //  $scope.ExportTemplate.PROJECT_NAME = $scope.Export.PROJECT_NAME;
                    $scope.SelectedCatalogId = data.CATALOG_ID;
                    $scope.outputformat = data.EXPORT_FORMAT;
                    $scope.prodfamilyattrdataSource.read();
                    if (data.EXPORT_FORMAT === "Text") {
                        $scope.enabledelimiters = false;
                    }
                    $scope.Export.exportAttributes = data.EXPORT_ATTRIBUTE;
                    $scope.hierarchy = data.HIERARCHY_REQUIRED === true ? "Yes" : "No";
                    $scope.Export.applyFilter = data.APPLY_FILTER === true ? "Yes" : "No";
                    $scope.exportids = data.EXPORT_ID_COLUMN === true ? "Yes" : "No";

                    $scope.Export.exporsubproducts = data.SUB_PRODUCTS_REQUIRED === true ? "Yes" : "No";
                    $scope.exporsubproducts = $scope.Export.exporsubproducts;
                    $scope.exporttypes = data.EXPORT_TYPE;
                    $scope.Delimiters = data.DELIMITER;


                    $scope.DateFormatPatten = function (data, option) {
                        var Year = data.substring(0, 4);
                        var Month = data.substring(5, 7)
                        var date = data.substring(8, 10)
                        if (option == 1) {
                            $scope.fromdate = Month + '/' + date + '/' + Year;
                        } else {
                            $scope.todate = Month + '/' + date + '/' + Year;
                        }
                    }



                    if (data.EXPORT_TYPE === "4") {
                        //Change Date Format
                        var FromDatetxt = data.FROM_DATE.replace('T00:00:00', '');
                        var ToDatetxt = data.TO_DATE.replace('T00:00:00', '');
                        $scope.DateFormatPatten(FromDatetxt, 1);
                        $scope.DateFormatPatten(ToDatetxt, 2);
                        $("#nextBtn").removeAttr('disabled');
                    }
                    if (data.EXPORT_TYPE === "3") {
                        $scope.partialexport = true;
                        $scope.daterange = false;
                        $("#datepickerfrom").hide();
                        $("#datepickerto").hide();
                        $scope.fromdate = "";
                        $scope.todate = "";
                        $scope.displaydateonselection = false;
                        $("#nextBtn").attr('disabled', 'disabled');
                    }
                    else if (data.EXPORT_TYPE === "4") {
                        $scope.partialexport = false;
                        $scope.daterange = true;
                        $scope.displaydateonselection = true;
                        $("#datepickerfrom").show();
                        $("#datepickerto").show();
                        $("#nextBtn").removeAttr('disabled');
                    }
                    else {
                        $scope.partialexport = false;
                        $scope.daterange = false;
                        $("#datepickerfrom").hide();
                        $("#datepickerto").hide();
                        $scope.fromdate = "";
                        $scope.todate = "";
                        $scope.displaydateonselection = false;
                        $("#nextBtn").removeAttr('disabled');
                    }
                    $scope.Export.COMMENTS = data.COMMENTS;
                    $scope.GetAllCatalogattributesdataSource.read();
                    $scope.ExporttreeData.read();
                }
            });

        } else {

            $scope.Newproject = false;
            $scope.Export.PROJECT_NAME = "";
        }
    };
    $scope.model = [];
    $scope.OpenTemplate = function (e) {

        var project_id = e.sender._old;
        if (project_id.trim() !== "") {
            dataFactory.OpenTemplate(project_id, $localStorage.getCatalogID, "").success(function (response) {

                $scope.model = response;
                if ($scope.model[0] !== "" && $scope.model[1] !== "" && $scope.model[2] !== "" && $scope.model[3] !== "" && $scope.model[4] !== "" && $scope.model[5] !== "" && $scope.model[6] !== "") {
                    $scope.SelectedprojectId = project_id;
                    $scope.projectId = project_id;
                    $scope.prodfamilyattrdataSource.read();
                    $scope.GetAllCatalogattributesdataSource.read();
                    $scope.Export.PROJECT_NAME = $scope.model[6];
                    $scope.Export.COMMENTS = $scope.model[7];
                    $scope.Export.applyFilter = $scope.model[1];
                    $scope.Export.exportAttributes = $scope.model[0];
                    $scope.Export.familyFilter = $scope.model[1];
                    $("#divselectcatalogs").show();
                    $("#divselectcatalogattributes").show();
                    $("#divselectcatalog").hide();
                    $("#lnkExporttab").addClass("active");
                    $("#exportTab").addClass("active in");
                    $("#openTab").removeClass("active in");
                    $("#deleteTab").removeClass("active in");
                    $("#lnkOpenTab").removeClass("active");
                    $("#lnkDelTab").removeClass("active");
                }
                else {

                    $scope.Export.PROJECT_NAME = "";
                    $scope.Export.COMMENTS = "";
                    $("#divselectcatalog").show();
                    $("#divselectcatalogattributes").hide();
                    $("#lnkExporttab").addClass("active");
                    $("#exportTab").addClass("active in");
                    $("#openTab").removeClass("active in");
                    $("#deleteTab").removeClass("active in");
                    $("#lnkOpenTab").removeClass("active");
                    $("#lnkDelTab").removeClass("active");
                }
            });
        }
    };


    $scope.categoryDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetCategoryDetails($scope.SelectedCatalogId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.projectDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetProjectDetails($scope.SelectedCatalogId, 7).success(function (response) {
                    $scope.getProjectId = response[0].PROJECT_ID;
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.GetAllCatalogattributesdataSource = new kendo.data.DataSource({
        type: "json",
        filterable: true,
        serverFiltering: false,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                //Check That Catalog is selected or not ??

                dataFactory.GetCatalogattributes($scope.SelectedCatalogId, $scope.SelectedprojectId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: true },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
                { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });

    $scope.ExporttreeData = new kendo.data.HierarchicalDataSource({

        type: "json",
        loadOnDemand: false, autoBind: false,
        transport: {
            read: function (options) {
                //dataFactory.GetCategoryDetails($scope.SelectedCatalogId).success(function (response) {
                //    options.success(response);
                dataFactory.CategoriesForExport($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });

            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: ""
            }
        }
    });

    //--------------For select all check box cateogires --------------------


    $scope.CheckOrUncheck1 = function () {


        if (chkEnabled1.checked == true) {
            $scope.CheckOrUncheckBox1 = true;
            $scope.SelectedcategoryIdexp = "All";
        } else {
            $scope.CheckOrUncheckBox1 = false;
            $scope.SelectedcategoryIdexp = "";
        }
        if ($scope.CheckOrUncheckBox1) {
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"]').prop('checked', true).trigger("change");
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"]').prop('disabled', true).trigger("change");
            $scope.SelectedcategoryIdexp = "All";
            $scope.CheckOrUncheckBox1 = true;
        } else {
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"]').prop('checked', false).trigger("change");
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"]').prop('disabled', false).trigger("change");
            $scope.CheckOrUncheckBox1 = false;
        }

    };




    //-----------------------------


    //--------------For select all check box Families --------------------



    $scope.CheckOrUncheck1fam = function (e) {

        $scope.CheckedStatus = e.CheckOrUncheckBox2;
        $scope.selectallcheckbox = "Checkedall";
        if (chkEnabled2.checked == true) {
            $scope.CheckOrUncheckBox2 = true;
            chkEnabled2.checked = false;
            //$scope.SelectedcategoryIdexp = "All";
        } else {
            $scope.CheckOrUncheckBox2 = false;
            //$scope.SelectedcategoryIdexp = "";
        }
        if ($scope.CheckOrUncheckBox2) {
            $('#leftNavTreeViewKendoNavigatorfam input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox2 = true;

        } else {
            $('#leftNavTreeViewKendoNavigatorfam input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox2 = false;
        }

    };

    $scope.CheckOrUncheck2fam = function (e) {

        $scope.CheckedStatus = e.CheckOrUncheckBox3;
        $scope.selectallcheckbox = "Checkedall";
        if (chkEnabled3.checked == true) {
            $scope.CheckOrUncheckBox3 = true;
            //$scope.SelectedcategoryIdexp = "All";
        } else {
            $scope.CheckOrUncheckBox3 = false;
            //$scope.SelectedcategoryIdexp = "";
        }
        if ($scope.CheckOrUncheckBox3) {
            $('#leftNavTreeViewKendoNavigatorfam input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox3 = true;

        } else {
            $('#leftNavTreeViewKendoNavigatorfam input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox3 = false;
        }

    };


    $scope.ExporttreeDatafam = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        autoBind: false,
        transport: {

            read: function (options) {

                if ($scope.checkeditems != "" && $scope.checkeditems != "undefined") {

                    dataFactory.getCategoryFamilyExport($scope.checkeditems, $scope.SelectedCatalogId).success(function (response) {
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: ""
            }
        }
    });

    $scope.ExporttreeDatafam1 = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        autoBind: false,
        transport: {

            read: function (options) {

                if ($scope.checkeditems != "" && $scope.checkeditems != "undefined") {

                    dataFactory.getCategoryFamilyExport($scope.checkeditems, $scope.SelectedCatalogId).success(function (response) {
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });
                }

            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: ""
            }
        }
    });

    $scope.ExporttreeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {

            //console.log(e);
            if (!e.node) {
                $scope.attachChangeEvent();
            }
        }
    };

    $scope.ExporttreeOptionsCatalogfam = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {

            //console.log(e);
            if (!e.node) {
                $scope.attachChangeEventFamily();
            }
        }
    };

    $scope.attachChangeEvent = function () {

        var dataSource = $scope.ExporttreeData;
        $scope.checkeditems = '';
        $scope.SelectedcategoryIdexp = "0";
        dataSource.bind("change", function () {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.checkedNodeIds(dataSource.view(), checkedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            //console.log(selectedNodes);
            $scope.checkeditems = checkedNodes.join(",");
            if ($scope.SelectedcategoryIdexp != "All") {
                $scope.SelectedcategoryIdexp = checkedNodes.join(",");
            }
            if ($scope.SelectedcategoryIdexp == "") {
                $scope.SelectedcategoryIdexp = "0";
                $scope.checkeditems = "0";
            }
            $scope.ExporttreeDatafam.read();
            $scope.ExporttreeDatafam1.read();
        });



    };

    $scope.attachChangeEventFamily = function () {

        var dataSource = $scope.ExporttreeDatafam;
        $scope.familycheckeditems = '';
        dataSource.bind("change", function () {

            var selectedNodes = 0;
            $scope.famarray = [];
            var checkedNodesfam = [];
            $scope.selectallcheckbox != "";

            $scope.checkedNodeIdsfamily(dataSource.view(), checkedNodesfam);
            for (var i = 0; i < checkedNodesfam.length; i++) {
                var nd = checkedNodesfam[i];
                $scope.famarray[i] = checkedNodesfam[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            if ($scope.selectallcheckbox != "Checkedall") {
                chkEnabled2.checked = false;
            }
            //console.log(selectedNodes);

            $scope.familycheckeditems = checkedNodesfam.join(",");
            // $scope.selectedFamilyId = checkedNodesfam.join(",");
        });



    };
    $scope.checkedNodeIdsfamily = function (nodes, checkedNodesfam) {

        var varbool = 0;
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodesfam.push(nodes[i].FAMILY_ID);
                varbool += 1;
            }
            else {
                if (nodes[i].checked == false) {
                    chkEnabled2.checked = false;

                }
                //    if (chkEnabled2.checked == true)
                //        $scope.selectallcheckbox = "";
                //    //    else if (chkEnabled2.checked == false && checkedNodesfam.length == nodes.length)
                //    //        chkEnabled2.checked = true;
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIdsfamily(nodes[i].children.view(), checkedNodesfam);
            }
        }
        if (varbool == nodes.length)
            chkEnabled2.checked = true;

    };

    $scope.checkedNodeIds = function (nodes, checkedNodes) {

        $scope.uncheckednodes = "";
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].CATEGORY_ID);

            }
            else {
                $scope.uncheckednodes = nodes[i].CATEGORY_ID;
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
            if ($scope.uncheckednodes != "") {

                $scope.SelectedcategoryIdexp = "0";

            }
        }
    };


    function groupHeaderName(e) {
        if (e.value === 1) {
            return "Item Specifications (" + e.count + " items)";
        }
        else if (e.value === 3) {
            return "Item Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 4) {
            return "Item Price (" + e.count + " items)";
        } else if (e.value === 7) {
            return "Product Description (" + e.count + " items)";
        } else if (e.value === 6) {
            return "Item Key (" + e.count + " items)";
        } else if (e.value === 9) {
            return "Product Image / Attachment (" + e.count + " items)";
        } else if (e.value === 11) {
            return "Product Specifications (" + e.count + " items)";
        }
        else if (e.value === 12) {
            return "Product Price (" + e.count + " items)";
        }
        else if (e.value === 13) {
            return "Product Key (" + e.count + " items)";
        }
        else if (e.value === 0) {
            return "Category (" + e.count + " items)";
        }
        else if (e.value === 21) {
            return "Category Specification (" + e.count + " items)";
        }
        else if (e.value === 23) {
            return " CategoryImage/Attachement (" + e.count + " items)";
        }
        else if (e.value === 25) {
            return "Category Description (" + e.count + " items)";
        }
        else if (e.value === 2) {
            return "Product (" + e.count + " items)";
        }
        else {
            return "Item Specifications (" + e.count + " items)";
        }
    }




    $scope.mainGridOptions = {
        dataSource: $scope.GetAllCatalogattributesdataSource,
        filterable: { mode: "row" },
        columns: [
            { field: "ISAvailable", width: "50px", title: "<input type='checkbox' id='chkSelectAll' ng-click='checkAll($event, this)'/>", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', filterable: false },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px", filterable: true },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                filterable: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderNameall
            }], dataBound: function (e) {
            },
    };
    $scope.group1 = false;
    $scope.group3 = false;
    $scope.group4 = false;
    $scope.group7 = false;
    $scope.group6 = false;
    $scope.group9 = false;
    $scope.group12 = false;
    $scope.group11 = false;
    $scope.group13 = false;
    $scope.group0 = false;
    $scope.group2 = false;
    $scope.group21 = false;
    $scope.group23 = false;
    $scope.group25 = false;

    $scope.checkgroupAll1 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group1 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);

        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };
    $scope.checkgroupAll3 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        //tar.dirty = e.currentTarget.checked;

        $scope.group3 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };
    $scope.checkgroupAll4 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group4 = e.currentTarget.checked;
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };
    $scope.checkgroupAll7 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group7 = e.currentTarget.checked;
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };
    $scope.checkgroupAll6 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group6 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };
    $scope.checkgroupAll9 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group9 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };
    $scope.checkgroupAll11 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group11 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };
    $scope.checkgroupAll12 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group12 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };
    $scope.checkgroupAll13 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group13 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };
    $scope.checkgroupAll0 = function (e, tar) {

        var state = e.currentTarget.checked;


        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group0 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);

    };
    $scope.checkgroupAll2 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group2 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };

    $scope.checkgroupAll21 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group21 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);

        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };


    $scope.checkgroupAll23 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group23 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);

        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };


    $scope.checkgroupAll25 = function (e, tar) {

        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {

                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount + 1;
                }
            }
        });
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {


                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
                if (this['ISAvailable'] === true) {
                    $scope.Attribcount = $scope.Attribcount - 1;
                }

            }
        });
        $scope.catselectgrid.refresh();
        $scope.group25 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);

        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group21) {
            $("#chk21").prop("checked", true);
        } if ($scope.group23) {
            $("#chk23").prop("checked", true);
        } if ($scope.group25) {
            $("#chk25").prop("checked", true);
        }
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
        else
            $("#chkSelectAll").prop("checked", false);
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
    };







    function groupHeaderNameall(e) {
        if (e.value === 1) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll1($event," + e.value + ")'/> Item Specifications (" + e.count + " items)";
        }
        else if (e.value === 3) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll3($event," + e.value + ")'/> Item Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 4) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll4($event," + e.value + ")'/> Item Price (" + e.count + " items)";
        } else if (e.value === 6) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll6($event," + e.value + ")'/> Item Key (" + e.count + " items)";
        } else if (e.value === 7) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll7($event," + e.value + ")'/> Product Description (" + e.count + " items)";
        }
        else if (e.value === 9) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll9($event," + e.value + ")'/> Product Image / Attachment (" + e.count + " items)";
        } else if (e.value === 11) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll11($event," + e.value + ")'/> Product Specifications (" + e.count + " items)";
        }
        else if (e.value === 12) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll12($event," + e.value + ")'/> Product Price (" + e.count + " items)";
        }
        else if (e.value === 13) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll13($event," + e.value + ")'/> Product Key (" + e.count + " items)";
        }
        else if (e.value === 0) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll0($event," + e.value + ")'/> Category (" + e.count + " items)";
        }
        else if (e.value === 21) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll21($event," + e.value + ")'/> Category Specification (" + e.count + " items)";
        }
        else if (e.value === 23) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll23($event," + e.value + ")'/> CategoryImage/Attachement (" + e.count + " items)";
        }
        else if (e.value === 25) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll25($event," + e.value + ")'/> Category Description (" + e.count + " items)";
        }
        else if (e.value === 2) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll2($event," + e.value + ")'/> Product (" + e.count + " items)";
        }
        else {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll($event," + e.value + ")'/> Item Specifications (" + e.count + " items)";
        }
    }
    $scope.updateSelection = function (e, id) {
        var attrType = id.dataItem["ATTRIBUTE_TYPE"];
        var checkVal = e.currentTarget.checked;

        id.dataItem.set("ISAvailable", e.currentTarget.checked);
        if (id.dataItem.ISAvailable === true) {
            $scope.Attribcount = $scope.Attribcount - 1;
        }
        else {
            $scope.Attribcount = $scope.Attribcount + 1;
        }
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
        if (!checkVal) {
            $("#chkSelectAll").prop("checked", false);
            if (attrType == "0")
                $scope.group0 = false;
            else if (attrType == "1")
                $scope.group1 = false;
            else if (attrType == "2")
                $scope.group2 = false;
            else if (attrType == "3")
                $scope.group3 = false;
            else if (attrType == "4")
                $scope.group4 = false;
            else if (attrType == "7")
                $scope.group7 = false;
            else if (attrType == "6")
                $scope.group6 = false;
            else if (attrType == "9")
                $scope.group9 = false;
            else if (attrType == "12")
                $scope.group12 = false;
            else if (attrType == "11")
                $scope.group11 = false;
            else if (attrType == "13")
                $scope.group13 = false;
            else if (attrType == "21")
                $scope.group21 = false;
            else if (attrType == "23")
                $scope.group23 = false;
            else if (attrType == "25")
                $scope.group25 = false;
        }
        else {
            var totalCount = 0;
            var selectCount = 0;
            $.each($scope.catselectgrid.dataSource._data, function () {
                if (this['ATTRIBUTE_TYPE'] === attrType) {
                    totalCount++;
                    if (this['ISAvailable'] === true) {
                        selectCount++;
                    }
                    if (totalCount == selectCount) {
                        if (attrType == "0")
                            $scope.group0 = true;
                        else if (attrType == "1")
                            $scope.group1 = true;
                        else if (attrType == "2")
                            $scope.group2 = true;
                        else if (attrType == "3")
                            $scope.group3 = true;
                        else if (attrType == "4")
                            $scope.group4 = true;
                        else if (attrType == "7")
                            $scope.group7 = true;
                        else if (attrType == "6")
                            $scope.group6 = true;
                        else if (attrType == "9")
                            $scope.group9 = true;
                        else if (attrType == "12")
                            $scope.group12 = true;
                        else if (attrType == "11")
                            $scope.group11 = true;
                        else if (attrType == "13")
                            $scope.group13 = true;
                        else if (attrType == "21")
                            $scope.group21 = true;
                        else if (attrType == "23")
                            $scope.group23 = true;
                        else if (attrType == "25")
                            $scope.group25 = true;
                    }
                    else {
                        if (attrType == "0")
                            $scope.group0 = false;
                        else if (attrType == "1")
                            $scope.group1 = false;
                        else if (attrType == "2")
                            $scope.group2 = false;
                        else if (attrType == "3")
                            $scope.group3 = false;
                        else if (attrType == "4")
                            $scope.group4 = false;
                        else if (attrType == "7")
                            $scope.group7 = false;
                        else if (attrType == "6")
                            $scope.group6 = false;
                        else if (attrType == "9")
                            $scope.group9 = false;
                        else if (attrType == "12")
                            $scope.group12 = false;
                        else if (attrType == "11")
                            $scope.group11 = false;
                        else if (attrType == "13")
                            $scope.group13 = false;
                        else if (attrType == "21")
                            $scope.group21 = false;
                        else if (attrType == "23")
                            $scope.group23 = false;
                        else if (attrType == "25")
                            $scope.group25 = false;
                    }
                }
            });
        }
        if ($scope.group0)
            $("#chk0").prop("checked", true);
        if ($scope.group1)
            $("#chk1").prop("checked", true);
        if ($scope.group2)
            $("#chk2").prop("checked", true);
        if ($scope.group3)
            $("#chk3").prop("checked", true);
        if ($scope.group4)
            $("#chk4").prop("checked", true);
        if ($scope.group7)
            $("#chk7").prop("checked", true);
        if ($scope.group6)
            $("#chk6").prop("checked", true);
        if ($scope.group9)
            $("#chk9").prop("checked", true);
        if ($scope.group12)
            $("#chk12").prop("checked", true);
        if ($scope.group11)
            $("#chk11").prop("checked", true);
        if ($scope.group13)
            $("#chk13").prop("checked", true);
        if ($scope.group21)
            $("#chk21").prop("checked", true);
        if ($scope.group23)
            $("#chk23").prop("checked", true);
        if ($scope.group25)
            $("#chk25").prop("checked", true);
        if ($scope.group1 && $scope.group2 && $scope.group3 && $scope.group4 && $scope.group6 && $scope.group7 && $scope.group9 && $scope.group11 && $scope.group12 && $scope.group13
             && $scope.group21 && $scope.group23 && $scope.group25 && $scope.group0) {
            $("#chkSelectAll").prop("checked", true);
        }
    };

    $scope.prodfamilyattrdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        sort: { field: "DISPLAY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetSelectedattributes($scope.SelectedCatalogId, $scope.SelectedprojectId).success(function (response) {

                    for (var i = 0 ; response.length > i ; i++) {


                        if (response[i].ATTRIBUTE_ID == 1) {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            response[i].DISPLAY_NAME = $localStorage.CatalogItemNumber;

                        }
                        if (response[i].ATTRIBUTE_NAME == "SUBITEM#") {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            response[i].DISPLAY_NAME = $localStorage.CatalogSubItemNumber;
                        }

                        if (response[i].ATTRIBUTE_NAME == "PRODUCT_ID") {
                            response[i].ATTRIBUTE_NAME = "PRODUCT_ID";
                            response[i].DISPLAY_NAME = "ITEM_ID";
                        }

                        if (response[i].ATTRIBUTE_NAME == "FAMILY_ID") {
                            response[i].ATTRIBUTE_NAME = "FAMILY_ID";
                            response[i].DISPLAY_NAME = "PRODUCT_ID";
                        }

                        if (response[i].ATTRIBUTE_NAME == "FAMILY_NAME") {
                            response[i].ATTRIBUTE_NAME = "FAMILY_NAME";
                            response[i].DISPLAY_NAME = "PRODUCT_NAME";
                        }

                        if (response[i].ATTRIBUTE_NAME == "SUBFAMILY_ID") {
                            response[i].ATTRIBUTE_NAME = "SUBFAMILY_ID";
                            response[i].DISPLAY_NAME = "SUBPRODUCT_ID";
                        }

                        if (response[i].ATTRIBUTE_NAME == "SUBFAMILY_NAME") {
                            response[i].ATTRIBUTE_NAME = "SUBFAMILY_NAME";
                            response[i].DISPLAY_NAME = "SUBPRODUCT_NAME";
                        }

                       


                    }

                    options.success(response);
                    $scope.Attribcount = 253 - $scope.prodfamilyattrdataSource._data.length;
                    $("#remainingattrib").empty();
                    $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
                    $scope.Atributeidsexport = [];
                    $scope.Atributeidsexportew = "";
                    for (var i = 0; i < $scope.prodfamilyattrdataSource._data.length; i++) {
                        $scope.Atributeidsexport.push($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_ID);
                    }
                    for (var i2 = 0; i2 < $scope.Atributeidsexport.length; i2++) {
                        $scope.Atributeidsexportew += $scope.Atributeidsexport[i2] + ",";
                    }
                    $scope.dataSource.read();
                }).error(function (error) {



                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
                { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });

    $scope.selectedGridOptions = {
        dataSource: $scope.prodfamilyattrdataSource,
        selectable: "multiple",
        columns: [
            { field: "DISPLAY_NAME", title: "Attribute Name", width: "250px" },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderName
            }]
    };
    $scope.updateSelection1 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };
    //$scope.SavePublishAttributes = function () {

    //   $scope.Atributeidsexport = [];
    //    var removeitems = [];
    //    for (var i = 0; i < $scope.GetAllCatalogattributesdataSource._data.length; i++) {
    //        var entity = $scope.GetAllCatalogattributesdataSource._data[i];
    //        if (entity.ISAvailable) {
    //            $scope.prodfamilyattrdataSource.add(entity);
    //            removeitems.push(entity);
    //            $scope.Atributeidsexport.push(entity.ATTRIBUTE_ID);
    //            $("#treeViewDivAttribute").show();
    //        }
    //    }
    //    for (var j = 0; j < removeitems.length; j++) {

    //        $scope.GetAllCatalogattributesdataSource.remove(removeitems[j]);
    //    }

    //    $scope.dataSource.read();


    //    for (var i = 0; i < $scope.Atributeidsexport.length; i++) {

    //        $scope.Atributeidsexportew += $scope.Atributeidsexport[i] + ",";

    //    }
    //    $scope.attributeMainGridOptions.dataSource.read();

    //    $scope.attributeMainGridOptions.read();

    //};
    $scope.SavePublishAttributes = function () {
        $scope.Atributeidsexport = [];
        $scope.AtributesexportDisplayNames = [];
        var removeitems = [];
        var count = 0;
        for (var i1 = 0; i1 < $scope.GetAllCatalogattributesdataSource._data.length; i1++) {
            var entitys = $scope.GetAllCatalogattributesdataSource._data[i1];
            if (entitys.ISAvailable) {
                count = count + 1;
            }
        };
        var availableCount = $scope.prodfamilyattrdataSource._data.length;
        var moreThenCount = ((availableCount + count) - 253);
        var totalCount = (availableCount + count);
        if (count <= 253 && ((availableCount + count) <= 253) || $scope.outputformat == 'XLSX' || $scope.outputformat == 'CSV' || $scope.outputformat == 'TXT') {
            for (var i = 0; i < $scope.GetAllCatalogattributesdataSource._data.length; i++) {
                var entity = $scope.GetAllCatalogattributesdataSource._data[i];
                if (entity.ISAvailable) {
                    $scope.prodfamilyattrdataSource.add(entity);
                    removeitems.push(entity);
                    $scope.Atributeidsexport.push(entity.ATTRIBUTE_ID);

                    $scope.AtributesexportDisplayNames.push(entity.ATTRIBUTE_ID + "," + entity.ATTRIBUTE_NAME);
                    $("#treeViewDivAttribute").show();
                }
            }
            for (var j = 0; j < removeitems.length; j++) {
                $scope.GetAllCatalogattributesdataSource.remove(removeitems[j]);
            }

            for (var i2 = 0; i2 < $scope.Atributeidsexport.length; i2++) {

                $scope.Atributeidsexportew += $scope.Atributeidsexport[i2] + ",";

            }
            //  $scope.dataSource.read();
            $scope.dataSource.read();
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Maximum Attributes allowed is 253 -selected(' + totalCount + ' )Attributes, Please remove(' + moreThenCount + ' ) Attributes and try again.',
                type: "info"
            });
        }
    };

    $scope.DeletePublishAttributes = function () {
        var g = $scope.prodselectgrid.select();

        var removeitems = [];
        var defaultvalues = ["CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_NAME", "FAMILY_ID", "FAMILY_NAME", "SUBFAMILY_ID", "SUBFAMILY_NAME", "PRODUCT_ID"];
        g.each(function (index, row) {
            var selectedItem = $scope.prodselectgrid.dataItem(row);
            if (defaultvalues.indexOf(selectedItem.ATTRIBUTE_NAME) >= 0) {

            }
            else {
                removeitems.push(selectedItem);
                $scope.GetAllCatalogattributesdataSource.add(selectedItem);
            }
        });
        for (var i = 0; i < removeitems.length; i++) {
            $scope.prodfamilyattrdataSource.remove(removeitems[i]);
        }
        //for (var i = 0; i < removeitems.length; i++) {
        //    $scope.dataSource.remove(removeitems[i]);
        //}
        angular.forEach($scope.dataSource._data, function (data) {
            for (var i = 0; i < removeitems.length; i++) {
                if (data.ATTRIBUTE_ID == removeitems[i].ATTRIBUTE_ID) {
                    $scope.dataSource.remove(data);
                }
            }
        });
        for (var i = 0; i < removeitems.length; i++) {
            $scope.Atributeidsexportew = $scope.Atributeidsexportew.replace(removeitems[i].ATTRIBUTE_ID + ',', '')
        }

    };

    $scope.checkAll = function (e) {

        var state = e.currentTarget.checked;
        //  var grid = $scope.catselectgrid;

        $.each($scope.catselectgrid.dataSource._data, function () {


            if (this['ISAvailable'] === true) {
                $scope.Attribcount = $scope.Attribcount + 1;
            }

        });
        $.each($scope.catselectgrid.dataSource._data, function () {

            if (this['ISAvailable'] != state)
                this.dirty = true;
            this['ISAvailable'] = state;
            if (this['ISAvailable'] === true) {
                $scope.Attribcount = $scope.Attribcount - 1;
            }

        });

        var ecount = $scope.GetAllCatalogattributesdataSource._data.length - $scope.prodfamilyattrdataSource._data.length;
        $("#remainingattrib").empty();
        $("#remainingattrib").append("Maximum number of attributes available for selection :" + $scope.Attribcount);
        //$(".chkbxforallattributes").each(function () {
        //    $(this).prop("checked", true);
        //});
        $scope.catselectgrid.refresh();

        if (chkSelectAll.checked == true) {
            $("#chk0").prop("checked", true);
            $("#chk1").prop("checked", true);
            $("#chk2").prop("checked", true);
            $("#chk3").prop("checked", true);
            $("#chk4").prop("checked", true);
            $("#chk7").prop("checked", true);
            $("#chk6").prop("checked", true);
            $("#chk9").prop("checked", true);
            $("#chk12").prop("checked", true);
            $("#chk11").prop("checked", true);
            $("#chk13").prop("checked", true);
            $("#chk21").prop("checked", true);
            $("#chk23").prop("checked", true);
            $("#chk25").prop("checked", true);
            $scope.group1 = true;
            $scope.group3 = true;
            $scope.group4 = true;
            $scope.group7 = true;
            $scope.group6 = true;
            $scope.group9 = true;
            $scope.group12 = true;
            $scope.group11 = true;
            $scope.group13 = true;
            $scope.group0 = true;
            $scope.group2 = true;
            $scope.group21 = true;
            $scope.group23 = true;
            $scope.group25 = true;
        }
        else {
            $scope.group1 = false;
            $scope.group3 = false;
            $scope.group4 = false;
            $scope.group7 = false;
            $scope.group6 = false;
            $scope.group9 = false;
            $scope.group12 = false;
            $scope.group11 = false;
            $scope.group13 = false;
            $scope.group0 = false;
            $scope.group2 = false;
            $scope.group21 = false;
            $scope.group23 = false;
            $scope.group25 = false;
        }
    };


    $scope.ValidExportExport = function () {

        /////////////////////////////////////////////////////////////////


        if ($scope.projectId == 0) {
            $scope.projectId = $scope.SelectedprojectId;
        }
        if ($scope.selectedFamilyId === undefined) {
            $scope.selectedFamilyId = 0;
        }
        var alldata = [];
        $scope.attributeorder = $scope.attributeDiplayDataSource.data();

        // Dynamic  Catalog_item no Change. _ START

        for (var i = 0 ; $scope.prodfamilyattrdataSource._data.length > i ; i++) {
            if ($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_ID == 1) {
                $scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME = "ITEM#";
                $scope.prodfamilyattrdataSource._data[i].DISPLAY_NAME = "ITEM#";
            }
            if ($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME == "SUBITEM#") {
                $scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                $scope.prodfamilyattrdataSource._data[i].DISPLAY_NAME = $localStorage.CatalogSubItemNumber;
            }

            if ($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME == "PRODUCT_ID") {
                $scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME = "PRODUCT_ID";
                $scope.prodfamilyattrdataSource._data[i].DISPLAY_NAME = "FAMILY_ID";
            }

            if ($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME == "PRODUCT_NAME") {
                $scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME = "FAMILY_NAME";
                $scope.prodfamilyattrdataSource._data[i].DISPLAY_NAME = "FAMILY_NAME";
            }

            if ($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME == "SUBPRODUCT_ID") {
                $scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME = "SUBFAMILY_ID";
                $scope.prodfamilyattrdataSource._data[i].DISPLAY_NAME = "SUBFAMILY_ID";
            }

            if ($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME == "SUBPRODUCT_NAME") {
                $scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME = "SUBFAMILY_NAME";
                $scope.prodfamilyattrdataSource._data[i].DISPLAY_NAME = "SUBFAMILY_NAME";
            }

            if ($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME == "ITEM_ID") {
                $scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME = "PRODUCT_ID";
                $scope.prodfamilyattrdataSource._data[i].DISPLAY_NAME = "PRODUCT_ID";
            }
        }



        for (var i = 0 ; $scope.attributeorder.length > i ; i++) {
            if ($scope.attributeorder[i].ATTRIBUTE_ID == 1) {
                $scope.attributeorder[i].ATTRIBUTE_NAME = "ITEM#";
                $scope.attributeorder[i].DISPLAY_NAME = "ITEM#";
            }
            if ($scope.attributeorder[i].ATTRIBUTE_NAME == "SUBITEM#") {
                $scope.attributeorder[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                $scope.attributeorder[i].DISPLAY_NAME = $localStorage.CatalogSubItemNumber;
            }
        }
        //End

        //Mv - TO ADD Dynamic Values 

        //$scope.schedule = true;
        //$scope.sceduleDate = "6/4/2018 20:12";


        var DateTime = new Date($('#txtDataTime').val());
        var hour = DateTime.getHours() == '0' ? '00' : DateTime.getHours().toString().length == 1 ? '0' + DateTime.getHours() : DateTime.getHours();
        var min = DateTime.getMinutes() == '0' ? '00' : DateTime.getMinutes().toString().length == 1 ? '0' + DateTime.getMinutes() : DateTime.getMinutes();
        $scope.scheduleDate = DateTime.getDate() + "/" + (DateTime.getMonth() + 1) + "/" + DateTime.getFullYear() + " " + hour + ":" + min;
        var Passwordencrpt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($rootScope.PASSWORD), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        dataFactory.Exportproject($scope.Export, $scope.SelectedCatalogId, $scope.Export.exportAttributes, $scope.Export.applyFilter, $scope.Export.familyFilter,
        $scope.SelectedCatalogName,
        $scope.SelectedcategoryId, $scope.selectedFamilyId, $scope.projectId, $scope.SelectedcategoryIdexp, $scope.prodfamilyattrdataSource._data,
        $scope.selectedAttributeData,
        $scope.outputformat, $scope.Delimiters, $scope.fromdate, $scope.todate, $scope.exportids,
        $scope.hierarchy, $scope.attributeorder, $scope.famarray, $scope.schedule, $scope.scheduleDate, $rootScope.ftp, $rootScope.URL, $rootScope.USERNAME,
        Passwordencrpt,
        $rootScope.templateName,$scope.singlesheet, $scope.exporttypes).success(function (response) {

            //$scope.GetAllCatalogattributesdataSource.read();
            // $scope.prodfamilyattrdataSource.read();
            // $scope.templateDataSource.read();
            // $scope.projectDataSource.read();

            if ($rootScope.EnableSubProduct == true) {
                if ($scope.CheckEnableSubProduct == "true") {
                    $scope.EnableSubProduct = "true"
                } else if ($scope.CheckEnableSubProduct == "false") {
                    $scope.EnableSubProduct = "false"
                }
            }



            if ($scope.exporttabledesigner == "Yes") {
                $scope.EnableTableDesigner = "true"
            } else if ($scope.exporttabledesigner == "No") {
                $scope.EnableTableDesigner = "false"
            }

            //$("#divselectcatalogs").hide();
            //$("#divselectcatalogattributes").hide();
            //$("#divselectcatalog").show();
            if (response != null) {
                var Password = $rootScope.PASSWORD;
                var Passwordencrpt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(Password), key, {
                    keySize: 128 / 8,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });
                if ($rootScope.ftp == 1) {
                    dataFactory.DownloadExportSheet(response, $scope.EnableSubProduct, $rootScope.URL, $rootScope.USERNAME, Passwordencrpt, $rootScope.templateName).success(function (response) {

                    }).error(function (error) {

                    });
                }

            }

            if ($scope.schedule != true) {

                if ($scope.outputformat == "CSV") {
                    if ($scope.exporttabledesigner == "Yes") {
                        window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "tabledesigner");
                    }

                    window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "Products");
                    window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "Families");
                    window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "categories");
                }
                else {
                    window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner);

                }
                $scope.Export.PROJECT_NAME = "";
                $scope.Export.COMMENTS = "";
                $scope.famarray = [];
                $scope.selectedAttributeData = [];
                $scope.ExporttreeData.read();
                $scope.fromdate = "";
                $scope.todate = "";

                //
                $.msgBox({
                    title: "MarketStudio",
                    content: "Export completed successfully.",
                    type: "confirm",
                    buttons: [{ value: "Ok" }],
                    success: function (result) {
                        var bool = false;
                        if (result === "Ok") {
                            bool = true;
                            chkEnabled3.checked = false;
                            chkEnabled2.checked = false;
                        }
                        if (bool === true) {

                            $scope.backtopproj = true;
                            // window.location.href = 'App/Export';
                        }
                    }
                });
            }
            else {
                $.msgBox({
                    title: "MarketStudio",
                    content: response,
                    type: "confirm",
                    buttons: [{ value: "Ok" }],
                    success: function (result) {
                        var bool = false;
                        if (result === "Ok") {
                            bool = true;
                        }
                        if (bool === true) {
                            $scope.backtopproj = true;
                            // window.location.href = 'App/Export';
                        }
                    }
                });
            }
            //window.location.pathname = 'App/Export';

        }).error(function (error) {
            options.error(error);
        });
        ////////////////////////////////////////////////////////////
    }


    $scope.btnExport = function () {
        // if ($scope.catalogIdAttr == '0' && $scope.searchtxt != "") {


        if ($scope.schedule != true) {



            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Export may take some time to complete.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                    { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {
                        $scope.ValidExportExport();

                    }
                    else {
                        return false;
                    }
                }
            });

        }
        else {
            $scope.ValidExportExport();
        }
        //  }

    };

    $scope.familyDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getCategoryFamily($scope.SelectedcategoryId, $scope.SelectedCatalogId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        cache: false
    });
    $scope.familyChange = function (e) {
        if (e.sender.value() !== "") {
            $scope.selectedFamilyId = e.sender.value();
            $scope.selectedFamilyName = e.sender.text();
        }
    };



    $scope.click = function (e) {
        $("#custom-menu1").css({ top: (e.pageY) + "px", left: (e.pageX) + "px" }).show();
    };
    $scope.leftNavCollapse = function () {
        var grid = $('#gridExportAttributes').data('kendoGrid');
        var allMasterRows = grid.tbody.find('>tr.k-master-row');

        for (var i = 0; i < allMasterRows.length; i++) {

            grid.collapseRow(allMasterRows.eq(i));

        }

    };

    $scope.leftNavExpand = function () {
        var grid = $('#gridExportAttributes').data('kendoGrid');
        var allMasterRows = grid.tbody.find('>tr.k-master-row');

        for (var i = 0; i < allMasterRows.length; i++) {

            grid.expandRow(allMasterRows.eq(i));

        }
    };

    $scope.attributeGridDatasource = new kendo.data.DataSource({
        type: "json",
        batch: true,
        pageSize: 10,
        serverFiltering: true,
        transport: {
            read: function (options) {


                if ($scope.SelectedcategoryIdexp === undefined || $scope.SelectedcategoryIdexp === '') {
                    $scope.SelectedcategoryIdexp = "All";
                }
                if ($scope.familycheckeditems === undefined) {
                    $scope.familycheckeditems = "All";
                }
                if ($scope.Atributeidsexport.length != null && $scope.Atributeidsexport.length != "")
                    $scope.Atributeidsexportew = "";

                for (var i = 0; i < $scope.Atributeidsexport.length; i++) {
                    $scope.Atributeidsexportew += $scope.Atributeidsexport[i] + ",";
                }

                if ($scope.Atributeidsexportew != null) {
                    // $scope.familycheckeditems,
                    dataFactory.GetAttributeListByCatalogId($scope.SelectedCatalogId, $scope.SelectedcategoryIdexp, $scope.Atributeidsexportew, $scope.famarray).success(function (response) {
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                }
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID"
            }
        }
        ,
        error: function (e) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + e.xhr.responseText + '.',
                type: "error"
            });
        }
    });


    $("#gridExportAttributes").kendoGrid({
        dataSource: $scope.attributeGridDatasource,
        pageable: { buttonCount: 5 },
        columns: [
            { field: "ATTRIBUTE_ID", title: "Active", width: 30, template: '<input type="checkbox" id="allow" class="chkbx5" #= ATTRIBUTE_ID ?checked="checked" : "" # ng-click="attrfunctionset($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
        ],
        detailInit: detailInitAttributeValues,
        schema: {
            model: {
                id: "ATTRIBUTE_ID"
            }
        }
        ,
        editable: {
            mode: "inline",
            update: true,
        }
    });
    $scope.attrfunctionset = function (e, id) {

        // var attribute_id = e.data.ATTRIBUTE_ID;
    };
    $scope.attrfunctionvalueset = function (e, id) {

        //   var attribute_id = e.data.ATTRIBUTE_ID;
    };

    function detailInitAttributeValues(e) {

        var attribute_id = e.data.ATTRIBUTE_ID;
        $("<div/>").appendTo(e.detailCell).kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: function (options) {
                        if ($scope.SelectedcategoryIdexp === undefined || $scope.SelectedcategoryIdexp === '') {
                            $scope.SelectedcategoryIdexp = "All";
                        }
                        if ($scope.familycheckeditems === undefined || $scope.familycheckeditems === '') {
                            $scope.familycheckeditems = "All";
                        }

                        dataFactory.GetAttributeValues($scope.SelectedCatalogId, $scope.SelectedcategoryIdexp, attribute_id, $scope.famarray).success(function (response) {
                            options.success(response);
                        }).error(function (response) {
                            options.success(response);
                        });
                    }
                },
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 10

            },
            scrollable: false,
            sortable: true,
            pageable: { buttonCount: 5 },
            columns: [
                { field: "PRODUCT_ID", title: "Active", width: 30, template: '<input type="checkbox" id="allow" class="chkbx5" #= PRODUCT_ID ?checked="checked" : "" # ng-click="attrfunctionvalueset($event, this)"></input>' },
                { field: "STRING_VALUE", width: "110px" }
            ]
        });
    }

    $scope.roleplanGroupfunctionset = function (e, id) {
        if (e.target.checked) {
            var foundAttrItem = $filter('filter')($scope.selectedAttributeData, { ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID }, true)[0];
            //get the index
            var index = $scope.selectedAttributeData.indexOf(foundAttrItem);
            if (index == -1) {
                $scope.selectedAttributeData.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    STRING_VALUE: "'" + id.dataItem.STRING_VALUE + "'",
                    ATTRIBUTE_NAME: id.dataItem.OBJECT_NAME
                });
            } else if (index != -1) {
                $scope.selectedAttributeData[index].STRING_VALUE = $scope.selectedAttributeData[index].STRING_VALUE + "," +
                    "'" + id.dataItem.STRING_VALUE + "'";
            }
            else {
                $scope.selectedAttributeData.pop({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    STRING_VALUE: "'" + id.dataItem.STRING_VALUE + "'",
                    ATTRIBUTE_NAME: id.dataItem.OBJECT_NAME
                });
            }
        }
        if (!e.target.checked) {
            $scope.selectedAttributeData.pop({
                ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                STRING_VALUE: "'" + id.dataItem.STRING_VALUE + "'",
                ATTRIBUTE_NAME: id.dataItem.OBJECT_NAME
            });
        }
    };



    $scope.dataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        serverFiltering: false,
        transport: {
            read: function (options) {
                if ($scope.SelectedcategoryIdexp === undefined || $scope.SelectedcategoryIdexp === '' || $scope.SelectedcategoryIdexp === '0') {
                    $scope.SelectedcategoryIdexp = "All";
                }
                if ($scope.familycheckeditems === undefined) {
                    $scope.familycheckeditems = "All";
                }
                // $scope.familycheckeditems,
                dataFactory.GetAttributeListByCatalogId($scope.SelectedCatalogId, $scope.SelectedcategoryIdexp, $scope.Atributeidsexportew, $scope.famarray).success(function (response) {

                    for (var i = 0 ; response.length > i ; i++) {


                        if (response[i].ATTRIBUTE_ID == 1) {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            response[i].DISPLAY_NAME = $localStorage.CatalogItemNumber;

                        }
                        if (response[i].ATTRIBUTE_NAME == "SUBITEM#") {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            response[i].DISPLAY_NAME = $localStorage.CatalogSubItemNumber;
                        }

                    }


                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ATTRIBUTE_ID: { editable: false, nullable: true },
                    ATTRIBUTE_NAME: { editable: false, validation: { required: true } }
                }
            }
        }
    });

    $scope.attributeMainGridOptions = {


        dataSource: $scope.dataSource,
        pageable: false,
        selectable: "row",
        columns: [
            //{ field: "SORT_ORDER", title: "Sort Order", template: "#=SORT_ORDER#" },
            //  { field: "FUNCTION_GROUP_ID", title: " ID" },
            { field: "ATTRIBUTE_NAME", title: "ATTRIBUTE NAME" }
            // ,
            //{ field: "DEFAULT_ACTION_ALLOW", title: "Active", width: 160, template: '<input type="checkbox" id="allow" class="chkbx5" #= DEFAULT_ACTION_ALLOW ?checked="checked" : "" # ng-click="roleplanGroupfunctionset($event, this)"> Check All</input>' }
        ],

        editable: false, detailTemplate: kendo.template($("#plantemplate").html()),
        dataBound: function () {
            //    this.expandRow(this.tbody.find("tr.k-master-row").first());
            // this.expandRow(this.tbody.find("tr.k-master-row"));
        }
    };


    $scope.attributeDetailGridOptions = function (dataItem) {

        return {
            dataSource: {
                type: "json",
                transport: {
                    read: function (options) {


                        if ($scope.SelectedcategoryIdexp === undefined || $scope.SelectedcategoryIdexp === '') {
                            $scope.SelectedcategoryIdexp = "All";
                        }
                        if ($scope.familycheckeditems === undefined || $scope.familycheckeditems === '') {
                            $scope.familycheckeditems = "All";
                        }

                        dataFactory.GetAttributeValues($scope.SelectedCatalogId, $scope.SelectedcategoryIdexp, dataItem.ATTRIBUTE_ID, $scope.famarray).success(function (response) {
                            options.success(response);
                        }).error(function (response) {
                            options.success(response);
                        });
                    }
                }, schema: {
                    model: {
                        id: "ATTRIBUTE_ID",
                        fields: {
                            ATTRIBUTE_ID: { editable: false, nullable: true },
                            STRING_VALUE: { editable: false, nullable: true },
                            OBJECT_NAME: { editable: false, nullable: true },

                        }
                    }
                },
                serverPaging: false,
                serverSorting: true,
                serverFiltering: false

            },
            scrollable: true,
            sortable: true,
            selectable: "row",
            pageable: false,
            columns: [
                { field: "STRING_VALUE", title: "Active Features" },
                { field: "PRODUCT_ID", title: "View", width: 130, template: '<input type="checkbox" id="allow" class="chkbx5" #= PRODUCT_ID ?checked="checked" : "" # ng-click="roleplanGroupfunctionset($event, this)"> </input>' }
            ],

            editable: false,
            dataBound: function () {
                if (this.dataSource.data().length === 0) {
                    var masterRow = this.element.closest("tr.k-detail-row").prev();
                    $("#grid").data("kendoGrid").collapseRow(masterRow);
                    masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                }
                else {
                    //  this.expandRow(this.tbody.find("tr.k-master-row"));
                    //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                }
            }
        };
    };



    $scope.getDisplayIdColoum = function () {
        //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
        dataFactory.getDisplayIdColoum().success(function (response) {

            $rootScope.getDisplayIdColoums = response;
            //  $scope.disableexpids = response;
            $scope.exportids = $rootScope.getDisplayIdColoums === true ? "Yes" : "No";
        })
    };




    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $scope.SelectedItem = 0;
            $scope.SelectedCatalogId = 0;
        }
        else {
            $scope.SelectedItem = $localStorage.getCatalogID;
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
        }
        if ($localStorage.getCustomerID === undefined) {
            $scope.getCustomerID = 0;

        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
        }
        if ($scope.SelectedCatalogId == '') {

            $scope.SelectedCatalogId = $localStorage.getCatalogID;
        }
        $("#divselectcatalogs").hide();
        $("#divselectcatalogattributes").hide();
        $("#divselectcatalog").show();
        $scope.getEnableSubProduct();
        $scope.getDisplayIdColoum();
        $scope.splitsheetshow = false;
        $scope.hierarchyshow = false;
    };
    $scope.Exportclick = function () {
        $scope.EnableExportBatch = false;
        $scope.GetAllCatalogattributesdataSource.read();
        $scope.prodfamilyattrdataSource.read();
        $scope.templateDataSource.read();
        $scope.projectDataSource.read();
        $("#divselectcatalogs").hide();
        $("#divselectcatalogattributes").hide();
        $("#divselectcatalog").show();

    };
    $scope.attributeDiplayDataSource = new kendo.data.DataSource({
        type: "json",
        transport: {
            read: function (options) {
                if ($scope.projectId == 0 && $scope.SelectedprojectId != 0 && $scope.SelectedprojectId != undefined) {
                    $scope.projectId = $scope.SelectedprojectId;
                }

                dataFactory.GetSelectedDisplayAttributes($scope.SelectedCatalogId, $scope.projectId, $scope.prodfamilyattrdataSource._data).success(function (response) {
                    $scope.AtributesexportDisplayNames = [];
                    if ($scope.AtributesexportDisplayNames != undefined && $scope.AtributesexportDisplayNames.length > 0) {
                        //var Testing = angular.equals(response, $scope.AtributesexportDisplayNames)
                        $scope.Testing = [];
                        for (var i = 0; i < $scope.AtributesexportDisplayNames.length; i++) {
                            var attribute = $scope.AtributesexportDisplayNames[i].split(',');
                            response.push({ ATTRIBUTE_ID: attribute[0], ATTRIBUTE_NAME: attribute[1], DISPLAY_NAME: attribute[1] })
                        }
                    }
                    $scope.AtributesexportDisplayNames = [];

                    for (var i = 0 ; response.length > i ; i++) {


                        if (response[i].ATTRIBUTE_ID == 1) {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            response[i].DISPLAY_NAME = $localStorage.CatalogItemNumber;

                        }
                        if (response[i].ATTRIBUTE_NAME == "SUBITEM#") {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            response[i].DISPLAY_NAME = $localStorage.CatalogSubItemNumber;
                        }

                    }

                    options.success(response);
                    //$scope.attributeorder = response;
                });
            },


        }
    })
    $scope.exportFinal = false;
    $scope.btnNextStep = function () {
        $scope.exportFinal = true;
        $scope.attributeDiplayDataSource.read();
        autoBind = true,
         $("#AttributeDisplayGrid").show();
        $scope.EnableExportBatch = false;
        $("#AttributeDisplayGrid").kendoGrid({
            dataSource: $scope.attributeDiplayDataSource,
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: {
                            editable: false,
                            nullable: true
                        },
                        DISPLAY_NAME: {
                            editable: true,
                            validation: {
                                required: true
                            }
                        },
                    }
                }
            },
            columns: [{
                field: "ATTRIBUTE_NAME",
                title: "Attribute Name",
                template: '<fieldset disabled>#:ATTRIBUTE_NAME#</fieldset>'
            },
            {
                field: "DISPLAY_NAME",
                title: "Display Name",
            },
            {
                field: "ATTRIBUTE_ID",
                title: "ATTRIBUTE_ID",
                hidden: true
            }],
            editable: true,
        })
    }
    $scope.ResetPage = function () {
        $scope.SelectedCatalogName = "";
        $scope.SelectedcategoryId = "All";
        $scope.SelectedcategoryName = "";
        $scope.SelectedprojectId = 0;
        $scope.SelectedprojectName = "";
        $scope.SelectedCatalogId = 0;
        $scope.projectId = 0;
        $scope.SelectedcategoryIdexp = "";
        $scope.selectedAttributeData = [];
        $scope.Atributeidsexportew = "";
        $scope.allowselection = "";
        $scope.exporttypes = "1";
        $scope.outputformat = "XLS";
        $scope.Delimiters = "\t";
        $scope.enabledelimiters = true;
        $scope.daterange = true;
        $scope.partialexport = false;
        $scope.fromdate = "";
        $scope.todate = "";
        $scope.exportids = "No";
        $scope.hierarchy = "No";

        $scope.disableexpids = false;

        if ($localStorage.DisplayIdBycoloumValue == true) {
            $scope.disableexpids = false;
        }
        else {
            $scope.disableexpids = true;
        }


        $scope.tabledesigner = "No";
        // $scope.disableexpids = false;

        $scope.displaydateonselection = false;
        $scope.exporsubproducts = "No";
        $scope.Newproject = false;


        $("#datepickerfrom").hide();


        $("#datepickerto").hide();
        $scope.Export = {
            PROJECT_NAME: "",
            COMMENTS: "",
            exportAttributes: "Column",
            applyFilter: "No",
            familyFilter: "No",
            subProduct: "",
        };
        $scope.ExportTemplate = {
            PROJECT_ID: "",
            PROJECT_NAME: "",
            COMMENTS: "",
            exportAttributes: "Column",
            applyFilter: "No",
            familyFilter: "No",
            SelectedCatalogId: "",
            OutputFormat: "",
            hierarchy: "",
            exportids: "",
            subProduct: "",
            exporttypes: "",
            Delimiters: "", fromdate: "", todate: ""
        };
    }
    //--------To get EnableSubProduct value from preference page-------
    $rootScope.getEnableSubProduct = function () {
        //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
        dataFactory.getEnableSubProduct().success(function (response) {

            $rootScope.EnableSubProduct = response;
        })
    };
    $scope.init();

    // Worked on Export Batch Mariyavijayan - Start [25-05-2018]

    var clearedBatchValues = [];
    //New_WithOut value or parameter.
    $scope.ClearLogExport = function () {
        var getselectedValues = clearedBatchValues;

        if (getselectedValues.length != 0) {

            dataFactory.ClearLogExport(getselectedValues).success(function (response) {
                if (response == "DeletedSuccessfully_TEST") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'DeletedSuccessfully.',
                        type: "info"
                    });
                }
                $scope.ExportViewLogGridOptionsDatasource.read();
                // $scope.GetBatchLogList();
                clearedBatchValues = [];
            })
        }
    }
    // To clear the selected value in import viewlog



    //  ******************************************************************************************************************

    // To clear the selected value in import viewlog

    //************** ***************************** ************************************ ********************************* ***********************************[Start]


    var DeleteValues1 = [];


    $scope.selectAll = function (ev, th) {
        var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
        var items = grid.dataItems();
        $scope.TotoalCount = items;
        //Start
        var CurrentCheck = ev.currentTarget.checked;


        if (CurrentCheck == true) {
            items.forEach(function (item) {
                item.set("ISAvailable", false);

            });
        }



        if (CurrentCheck == true) {

            DeleteValues1 = [];

            for (var i = 0; i < items.length; i++) {
                DeleteValues1.push(items[i].EXPORTID);
            }
        } else {
            DeleteValues1 = [];
        }


        if (CurrentCheck == true) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].STATUS != "Inprogress") {
                    clearedBatchValues.push(items[i].EXPORTID);
                }
            }
            // clearedBatchValues.push(batchId);
        }
        if (CurrentCheck == false) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].STATUS != "Inprogress") {
                    clearedBatchValues.pop(items[i].EXPORTID);
                }
            }
        }
        //End
        items.forEach(function (item) {
            if (item.ISAvailable == false) {

                if (item.STATUS == "Inprogress") {
                    item.set("ISAvailable", false);
                } else {
                    {
                        item.set("ISAvailable", ev.target.checked);
                    }
                }
            }
            else if (item.ISAvailable == true) {
                if (item.STATUS == "Inprogress") {
                    item.set("ISAvailable", false);
                } else {
                    item.set("ISAvailable", ev.target.checked);
                }
            }
        });
    };

    // To clear the selected value in import viewlog BATCH_ID // ***********************************************$$$$$$$$$$$$$$$$$$$$




    $scope.updateSelectionForDelete = function (e, id) {

        var grid = $(e.target).closest('[kendo-grid]').data("kendoGrid");
        var items = grid.dataItems();

        var EXPORTID = id.dataItem.EXPORTID;
        var CurrentCheck = e.currentTarget.checked;
        if (CurrentCheck == true) {
            clearedBatchValues.push(EXPORTID);

            DeleteValues1.push(EXPORTID);

            if (items.length == DeleteValues1.length) {
                $("#Updateselect").prop("checked", true);
            }
        }
        if (CurrentCheck == false) {
            for (var i = 0 ; clearedBatchValues.length > i ; i++) {
                if (EXPORTID == clearedBatchValues[i]) {
                    clearedBatchValues.pop(clearedBatchValues[i]);
                }
            }
            DeleteValues1.pop();
            $("#Updateselect").prop("checked", false);
        }

        if (DeleteValues1.length == 0) {
            $("#Updateselect").prop("checked", false);
        }
        // alert(batchId);
    }
    //************** ***************************** ************************************ ********************************* ***********************************[End]

    //  *****************************************************************************************************************


    $scope.scheduleTime = new Date();

    $('#txtDataTime').kendoDateTimePicker({
        value: $scope.scheduleTime,

        parseFormats: ['MM/dd/yyyy'],
        min: $scope.scheduleTime,

    });


    $scope.CancelProjectName = function () {
        $scope.winExportProject.center().close();
        $scope.ScheduleProjectName = null;
    }


    $scope.UpdateProjectName = function () {

        //var projectName = $('#txtScheduleProjectName').val();

        var projectName = $scope.ScheduleProjectName;

        if (projectName == undefined || projectName == "") {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Must enter the template name.',
                type: "info"
            });
        }
        else {


            dataFactory.UpdateProjectForBatch($scope.BatchProjectId, projectName).success(function (response) {



                if (response == "DeletedSuccessfully") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Template name updated successfully.',
                        type: "info"
                    });
                    $scope.Export.PROJECT_NAME = $scope.ScheduleProjectName;

                    $scope.EnableExportBatch = true;
                    $scope.schedule = true;
                    $scope.scheduleTime = new Date();
                    var currentdate = new Date();
                    $('#txtDataTime').kendoDateTimePicker({
                        value: $scope.scheduleTime,
                        parseFormats: ['MM/dd/yyyy'],
                        min: $scope.scheduleTime,
                    });

                    $scope.winExportProject.center().close();

                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Project name already exist.',
                        type: "info"
                    });
                }

            });
        }
    }

    $scope.EnableBachfunExport = function () {

        if ($scope.Export.PROJECT_NAME.trim().length == 0) {
            $scope.winExportProject.refresh({ url: "../views/app/partials/ExportProjectName.html" });
            $scope.winExportProject.center().open();

        }
        else {

            $('#divdate').show();

            $scope.EnableExportBatch = true;
            $scope.schedule = true;
            $scope.scheduleTime = new Date();
            var currentdate = new Date();
            $('#txtDataTime').kendoDateTimePicker({
                value: $scope.scheduleTime,
                parseFormats: ['MM/dd/yyyy'],
                min: $scope.scheduleTime,
            });
        }


        //if ($scope.exporttabledesigner == "Yes") {
        //    $scope.EnableTableDesigner = "true"
        //} else if ($scope.exporttabledesigner == "No") {
        //    $scope.EnableTableDesigner = "false"
        //}

        //if ($scope.Export.PROJECT_NAME.trim().length == 0 )
        //{
        //    $scope.hideProjectName = true;
        //    $.msgBox({
        //        title: $localStorage.ProdcutTitle,
        //        content: "Must Add Project Name",
        //        type: "confirm",
        //        buttons: [{ value: "Yes" }, { value: "No" }],                
        //        success: function (result) {
        //            $scope.hideProjectName = true;
        //            if (result === "Yes")
        //            {
        //                $scope.hideProjectName = true;                      
        //            }
        //            else
        //            {
        //                $scope.hideProjectName = false;
        //            }                  
        //        }
        //    });          
        //}else
        //{
        //    $scope.EnableExportBatch = true;
        //    $scope.schedule = true;
        //    $scope.scheduleTime = new Date();
        //    var currentdate = new Date();
        //    $('#txtDataTime').kendoDateTimePicker({
        //        value: $scope.scheduleTime,
        //        parseFormats: ['MM/dd/yyyy'],
        //        min: $scope.scheduleTime,
        //    });
        //}

    }
    //Open popup in log view

    $scope.ViewExportLog = function () {
        $scope.ExportViewLogGridOptionsDatasource.read();
        $scope.winExportBatchLog.refresh({ url: "../views/app/partials/ExportBatchLog.html" });
        $scope.winExportBatchLog.center().open();
    }

    $scope.RefreshExportBatchLog = function () {
        $scope.ExportViewLogGridOptionsDatasource.read();
    }


    $scope.RunExportProcess = function (data) {
        dataFactory.RunExportProcess(data.dataItem.BATCH_ID).success(function (response) {

            $scope.ExportViewLogGridOptionsDatasource.read();

            if (response == "Export is already running.Please wait for few minutes") {
                // $scope.ImportViewLogGridOptionsDatasource.read();
                //  $scope.GetBatchLogList();
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Export is already running.Please wait for few minutes.',
                    type: "info"
                });

            }
            //$scope.BatchLogList = response;

        });
    }


    $scope.ClearExportLogByid = function (data) {
        dataFactory.ClearExportLogByid(data.dataItem.EXPORTID).success(function (response) {

            $scope.ExportViewLogGridOptionsDatasource.read();

        });
    }


    $scope.ExportDownload = function (data) {

        var filename = "Job_" + data.dataItem.EXPORTID + ".xls";

        //windowlocation = windowlocation + "/Content/InDesignXML/" + e.dataItem.XML_FILE_NAME + ".xml";
        window.open("DownloadExportBatchFile.ashx?Path=" + filename + "&ExportBatchId=" + data.dataItem.EXPORTID);

    }

    $scope.ExportViewLogGridOptions =
        {
            dataSource: $scope.ExportViewLogGridOptionsDatasource,
            sortable: true, scrollable: true, editable: false, pageable: { buttonCount: 5 }, autoBind: false,
            toolbar: [
           { name: "save", text: "", template: '<button id="refreshLog" class="btn btn-primary btn-xs" title="Refresh" ng-click="RefreshExportBatchLog()">Refresh</button>' },
            { name: "cancel", text: "", template: '<button id="clearLog" class="btn btn-primary btn-xs" style="float: right;margin-right: 0px;" title="Delete Multiple Records" ng-click="ClearLogExport()">Delete</button>' }
            ],
            columns: [
            { field: "EXPORT_NAME", title: "EXPORT_NAME", width: "100px", headerTemplate: '<span title="EXPORT_NAME">EXPORT_NAME</span>' },
            { field: "PROJECT_NAME", title: "TEMPLATE_NAME", width: "100px", headerTemplate: '<span title="TEMPLATE_NAME">TEMPLATE_NAME</span>' },
            { field: "STATUS", title: "STATUS", width: "120px", headerTemplate: '<span title="STATUS">STATUS</span>' },
            { field: "SCEDULETIME", title: "SCHEDULE_TIME", width: "100px", headerTemplate: '<span title="SCEDULETIME">SCHEDULE_TIME</span>' },
            { field: "CUSTOMER_NAME", title: "CUSTOMER_NAME", width: "100px", headerTemplate: '<span title="CUSTOMER_NAME">CUSTOMER_NAME</span>' },

            {
                field: "CustomStatus",
                title: "Action",
                template: '<button #= (STATUS=="Completed") ? "" : "disabled=true" # ng-click="ExportDownload(this)" title="Download" class="btn btn-info cus"><i class="fa fa-download"></i> </button>  &nbsp <button #= STATUS=="Pending" ? "" : "disabled=true" # ng-click="RunExportProcess(this)" title="Run"  class="btn btn-info cus"><i class="icon-play"></i> </button> &nbsp <button #= STATUS=="Inprogress" ? "disabled=true" : "" # ng-click="ClearExportLogByid(this)" title="Delete"  class="btn btn-info cus"><i class="fa fa-remove"></i> </button>',
                width: "110px",
                sortable: false,
                headerTemplate: '<span title="Action">ACTION</span>',
                filterable: false
            },
           {
               field: "ISAvailable",
               title: "Select",
               sortable: false,
               // template: '<input type="checkbox"   ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>',#= ISAvailable ? "disabled" : ""
               template: '<input type="checkbox"  #= STATUS=="Inprogress" ? checked="disabled" : !ISAvailable && STATUS!="Inprogress" ? checked="" : "checked" #  ng-click="updateSelectionForDelete($event, this)"></input>',
               // template: '<input type="checkbox"   #= CustomStatus=="Inprogress"?"(#=ISAvailable ? "checked=checked " : "") ":"disabled=true " #  ng-click="updateSelection($event, this)"></input>',
               //  template: '<input type="checkbox"   #= CustomStatus=="Inprogress"? "disabled=true " : "checked=checked" #  ng-click="updateSelection($event, this)"></input>',
               headerTemplate: "<input type='checkbox' title='select all'  id='Updateselect'  ng-click='selectAll($event,this)'/>",
               width: "40px",
               filterable: false
           },

            ],
            filterable: true,
            dataBound: function (e) {
            },

        };

    $scope.ExportViewLogGridOptionsDatasource = new kendo.data.DataSource({
        pageSize: 10,
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetBatchExportLogList().success(function (response) {
                    options.success(response);

                    angular.forEach($('.k-i-filter'), function (value) {
                        value.className = "k-icon k-filter";
                    });
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        },
        schema: {
            model: {
                id: "EXPORTID",
                fields: {
                    // CatalogName: { editable: false, nullable: true },
                    ISAvailable: { editable: true, type: "boolean", sortable: false },
                    EXPORT_NAME: { editable: false, nullable: true },
                    PROJECT_NAME: { editable: false, nullable: true },
                    STATUS: { editable: false, nullable: true },
                    SCEDULETIME: { editable: false, nullable: true },
                    CUSTOMER_NAME: { editable: false, nullable: true },

                }
            }
        }
    });
    // Worked on Export Batch Mariyavijayan - End



    $("#chkEnabled1").on("click", function () {
        if ($("#chkEnabled1").prop("checked") == true) {
            $("#nextBtn").removeAttr('disabled');
        }
        else {
            $("#nextBtn").attr('disabled', 'disabled');
        }

    });

    $("#leftNavTreeViewKendoNavigator").on("change", function () {

        var treeView = $("#treeViewDiv").data("kendoTreeView");
        var len = $('#treeViewDiv').find('input:checkbox:checked').length;
        if (len > 0) {
            $("#nextBtn").removeAttr('disabled');
        } else {
            $("#nextBtn").attr('disabled', 'disabled');
        }


        //$("#treeViewDiv").find("input:checkbox:not(:checked)").each(function()
        //{
        //  $("#nextBtn").attr('disabled', 'disabled');
        //});

    });


    //FTP Export
    $("#ftppath").hide();
    $scope.isdisabled = false;
    $scope.exportpathchange = function (value) {

        if (value == "1") {

            $("#ftppath").show();
            $scope.isdisabled = true;

        }
        else if (value == "2") {
            //$scope.partialexport = false;
            $("#ftppath").hide();
            $scope.isdisabled = false;
            $scope.url = null;
            $scope.username = null;
            $scope.password = null;
        }
    }

    $scope.FtpChange = function () {
        $scope.isdisabled = true;
    };

    $scope.btnValidate = function (url, username, password) {
        var Passwordencrpt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(password), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        dataFactory.btnValidate(url, username, Passwordencrpt).success(function (response) {
            if (response != "false") {
                $scope.isdisabled = false;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Successfull',
                    type: "info"
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Check the correct FTP url and User credentials',
                    type: "info"
                });
            }
        }).error(function (error) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Check the correct FTP url and User credentials',
                type: "info"
            });
        });
    };


    $scope.clearFtpValues = function () {
        $scope.url = null;
        $scope.username = null;
        $scope.password = null;
    };

    $scope.updateChks = function (node) {
        var checkboxes = $('#leftNavTreeViewKendoNavigator input:checkbox');
        var selected = checkboxes.filter(':checked');
        checkboxes.not(selected).prop('disabled', selected.length > 0)
        //if(selected.length == 0)
        //{
        //    $scope.Expbutton = false;
        //}
        //else
        //{
        //    $scope.Expbutton = true;
        //}
        chkEnabled3.checked = false;
    }

    $scope.updateChksfam = function (nodefam) {
        var checkboxes = $('#leftNavTreeViewKendoNavigator input:checkbox');
        var selected = checkboxes.filter(':checked');
        var selected1 = checkboxes.filter(':not(:checked)');
        checkboxes.not(selected).prop('disabled', selected.length > 0);
        var dataSource = $scope.ExporttreeDatafam;
        $scope.familycheckeditems = '';
        var selectedNodes = 0;
        $scope.famarray = [];
        var checkedNodesfam = [];
        $scope.selectallcheckbox != "";
        $scope.checkedNodeIdsfamily(dataSource.view(), checkedNodesfam);
            for (var i = 0; i < checkedNodesfam.length; i++) {
                var nd = checkedNodesfam[i];
                $scope.famarray[i] = checkedNodesfam[i];
                if (nd.checked) {
                    selectedNodes++;
                }
                else
                {
                    $scope.CheckOrUncheckBox3 = false;
                }
            }
            if ($scope.selectallcheckbox != "Checkedall") {
                chkEnabled2.checked = false;
            }
            //console.log(selectedNodes);

            $scope.familycheckeditems = checkedNodesfam.join(",");
            // $scope.selectedFamilyId = checkedNodesfam.join(",");
      
    }


    $scope.ExpNext = function (checkednode) {
        // if ($scope.catalogIdAttr == '0' && $scope.searchtxt != "") {
        for (i = 0; i < checkednode.ExporttreeDatafam1._data.length; i++) {
            if (checkednode.ExporttreeDatafam1._data[i].checked) {
                $scope.Family_ids[i] = checkednode.ExporttreeDatafam1._data[i].FAMILY_ID
            }
        }

        var famcount = $scope.Family_ids.length;
        for (i = 0; i < famcount; i++) {
            if ($scope.Family_ids[i] != null)
            { $scope.Familyids[i] = $scope.Family_ids[i]; }
            else
            {
                $scope.Familyids[i] = "null";
            }

        }
        if (famcount > 0) {

            dataFactory.Categoryexportlength($scope.SelectedCatalogId, $scope.SelectedcategoryIdexp, $scope.Familyids).success(function (response) {
                if (response > 240) {
                    $scope.attrcount = 1;

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Maximum 256 Attributes are allowed.',
                        type: "error"
                    });
                }
                else {
                    dataFactory.Saveproject($scope.Export, $scope.SelectedCatalogId, 7).success(function (response) {

                        if (response === 0) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Template Name already exists, please select a different name.',
                                type: "error"
                            });
                        } else {
                            $scope.projectId = response;

                            $scope.BatchProjectId = response;

                            if ($scope.selectedFamilyId === undefined) {
                                $scope.selectedFamilyId = 0;
                            }
                            var alldata = [];
                            alldata.push($scope.Export);
                            alldata.push($scope.prodfamilyattrdataSource._data);
                            if ($scope.exporttypes == "3") {
                                $scope.partialexport = true;
                            }
                            $scope.ExportTemplate.PROJECT_ID = $scope.PROJECT_ID;
                            $scope.ExportTemplate.PROJECT_NAME = $scope.Export.PROJECT_NAME;
                            $scope.ExportTemplate.SelectedCatalogId = $scope.SelectedCatalogId;
                            $scope.ExportTemplate.OutputFormat = $scope.outputformat;
                            $scope.ExportTemplate.exportAttributes = $scope.Export.exportAttributes;
                            $scope.ExportTemplate.hierarchy = $scope.hierarchy;
                            $scope.ExportTemplate.applyFilter = $scope.Export.applyFilter;
                            $scope.ExportTemplate.exportids = $scope.exportids;

                            if ($scope.EnableSubProduct == true) {
                                $scope.Export.exporsubproducts = $scope.CheckEnableSubProduct;
                                $scope.ExportTemplate.subProductRequired = $scope.Export.exporsubproducts;

                            }
                            $scope.ExportTemplate.exporttypes = $scope.exporttypes;
                            $scope.ExportTemplate.Delimiters = $scope.Delimiters;
                            // $scope.ExportTemplate.fromdate = $scope.fromdate;
                            // $scope.ExportTemplate.todate = $scope.todate;
                            $scope.ExportTemplate.COMMENTS = $scope.Export.COMMENTS;
                            // Tochange the current date format

                            if ($scope.exporttypes == "4") {
                                var ChangedateFormatfromdate = $scope.fromdate;
                                var arrayOfFromDate = ChangedateFormatfromdate.split('/');
                                $scope.fromdate = arrayOfFromDate[1] + "/" + arrayOfFromDate[0] + "/" + arrayOfFromDate[2];
                                var ChangedateFormatToDate = $scope.todate;
                                var arrayOfToDate = ChangedateFormatToDate.split('/');
                                $scope.todate = arrayOfToDate[1] + "/" + arrayOfToDate[0] + "/" + arrayOfToDate[2];
                            }
                            // End
                            dataFactory.SaveExportproject($scope.Export, $scope.SelectedCatalogId, $scope.Export.exportAttributes, $scope.Export.applyFilter, $scope.Export.familyFilter, $scope.SelectedCatalogName,
                                $scope.SelectedcategoryId, $scope.selectedFamilyId, $scope.projectId, $scope.SelectedcategoryIdexp, $scope.prodfamilyattrdataSource._data, $scope.selectedAttributeData, $scope.famarray, $scope.ExportTemplate, $scope.fromdate, $scope.todate).success(function (response) {
                                    $scope.genericTemplateDataSource.read();
                                }).error(function (error) {
                                    options.error(error);
                                });
                        }
                    }).error(function (error) {
                        options.error(error);
                    });

                    $scope.Category_ids = checkednode.SelectedcategoryIdexp;
                    $scope.Catalog_id = checkednode.SelectedCatalogId;
                    for (i = 0; i < checkednode.ExporttreeDatafam1._data.length; i++) {
                        if (checkednode.ExporttreeDatafam1._data[i].checked) {
                            $scope.Family_ids[i] = checkednode.ExporttreeDatafam1._data[i].FAMILY_ID
                        }
                    }
                    if ($scope.Category_ids == "0") {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please Select Category',
                            type: "info"
                        });
                    }
                    else {

                        if ($scope.schedule != true) {



                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Export may take some time to complete.",
                                type: "confirm",
                                buttons: [{ value: "Ok" },
                                    { value: "Cancel" }
                                ],
                                success: function (result) {
                                    if (result === "Ok") {

                                        $scope.ValidCategoryExport();

                                    }
                                    else {
                                        return false;
                                    }
                                }
                            });

                        }
                        else {
                            $scope.ValidCategoryExport();
                        }
                    }

                }
            }).error(function (error) {
                options.error(error);
            });
        }



    };

    $scope.SplitSheetSelection = function () {
        if ($scope.exportsplitsheet == "Yes") {
            $scope.hierarchy = "Yes";
            $scope.exportids = "Yes";
            $scope.hierarchyshow = true;
            
            }
        else {
                $scope.exportids = "No";
                $scope.hierarchy = "No";
                $scope.hierarchyshow = false;
            }

    }

    $scope.ValidCategoryExport = function () {

        /////////////////////////////////////////////////////////////////

        if ($scope.projectId == 0) {
            $scope.projectId = $scope.SelectedprojectId;
        }
        if ($scope.selectedFamilyId === undefined) {
            $scope.selectedFamilyId = 0;
        }
        var alldata = [];
        $scope.attributeorder = $scope.attributeDiplayDataSource.data();

        // Dynamic  Catalog_item no Change. _ START

        for (var i = 0 ; $scope.prodfamilyattrdataSource._data.length > i ; i++) {
            if ($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_ID == 1) {
                $scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME = "ITEM#";
                $scope.prodfamilyattrdataSource._data[i].DISPLAY_NAME = "ITEM#";
            }
            if ($scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME == "SUBITEM#") {
                $scope.prodfamilyattrdataSource._data[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                $scope.prodfamilyattrdataSource._data[i].DISPLAY_NAME = $localStorage.CatalogSubItemNumber;
            }
        }



        for (var i = 0 ; $scope.attributeorder.length > i ; i++) {
            if ($scope.attributeorder[i].ATTRIBUTE_ID == 1) {
                $scope.attributeorder[i].ATTRIBUTE_NAME = "ITEM#";
                $scope.attributeorder[i].DISPLAY_NAME = "ITEM#";
            }
            if ($scope.attributeorder[i].ATTRIBUTE_NAME == "SUBITEM#") {
                $scope.attributeorder[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                $scope.attributeorder[i].DISPLAY_NAME = $localStorage.CatalogSubItemNumber;
            }
        }
        //End

        //Mv - TO ADD Dynamic Values 

        //$scope.schedule = true;
        //$scope.sceduleDate = "6/4/2018 20:12";


        var DateTime = new Date($('#txtDataTime').val());
        var hour = DateTime.getHours() == '0' ? '00' : DateTime.getHours().toString().length == 1 ? '0' + DateTime.getHours() : DateTime.getHours();
        var min = DateTime.getMinutes() == '0' ? '00' : DateTime.getMinutes().toString().length == 1 ? '0' + DateTime.getMinutes() : DateTime.getMinutes();
        $scope.scheduleDate = DateTime.getDate() + "/" + (DateTime.getMonth() + 1) + "/" + DateTime.getFullYear() + " " + hour + ":" + min;
        var Passwordencrpt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($rootScope.PASSWORD), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        dataFactory.CategoryExport($scope.Export, $scope.SelectedCatalogId, $scope.Export.exportAttributes, $scope.Export.applyFilter, $scope.Export.familyFilter,
        $scope.SelectedCatalogName,
        $scope.SelectedcategoryId, $scope.selectedFamilyId, $scope.projectId, $scope.SelectedcategoryIdexp, $scope.prodfamilyattrdataSource._data,
        $scope.selectedAttributeData,
        $scope.outputformat, $scope.Delimiters, $scope.fromdate, $scope.todate, $scope.exportids,
        $scope.hierarchy, $scope.attributeorder, $scope.famarray, $scope.schedule, $scope.scheduleDate, $rootScope.ftp, $rootScope.URL, $rootScope.USERNAME,
        Passwordencrpt,
        $rootScope.templateName, $scope.exporttypes, $scope.Family_ids, $scope.exportsplitsheet,$scope.singlesheet).success(function (response) {
            $scope.Family_ids = [];
            //$scope.GetAllCatalogattributesdataSource.read();
            // $scope.prodfamilyattrdataSource.read();
            // $scope.templateDataSource.read();
            // $scope.projectDataSource.read();
            $scope.Familyids = [];
            if ($rootScope.EnableSubProduct == true) {
                if ($scope.CheckEnableSubProduct == "true") {
                    $scope.EnableSubProduct = "true"
                } else if ($scope.CheckEnableSubProduct == "false") {
                    $scope.EnableSubProduct = "false"
                }
            }



            if ($scope.exporttabledesigner == "Yes") {
                $scope.EnableTableDesigner = "true"
            } else if ($scope.exporttabledesigner == "No") {
                $scope.EnableTableDesigner = "false"
            }

            //$("#divselectcatalogs").hide();
            //$("#divselectcatalogattributes").hide();
            //$("#divselectcatalog").show();
            if (response != null) {
                var Password = $rootScope.PASSWORD;
                var Passwordencrpt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(Password), key, {
                    keySize: 128 / 8,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });
                if ($rootScope.ftp == 1) {
                    dataFactory.DownloadExportSheet(response, $scope.EnableSubProduct, $rootScope.URL, $rootScope.USERNAME, Passwordencrpt, $rootScope.templateName).success(function (response) {

                    }).error(function (error) {

                    });
                }

            }

            if ($scope.schedule != true) {
                if ($scope.exportsplitsheet == "Yes") {
                    if ($scope.outputformat == "CSV") {
                        if ($scope.exporttabledesigner == "Yes") {
                            window.open("DownloadCategoryExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "tabledesigner");
                        }
                        for (j = 0; j < $scope.Familyids.length; j++) {
                            window.open("DownloadCategoryExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "Products_" + j);
                        }


                        window.open("DownloadCategoryExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "Families");
                        window.open("DownloadCategoryExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "categories");
                    }
                    else {
                        window.open("DownloadCategoryExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner);
                    }

                }
                else {

                    if ($scope.outputformat == "CSV") {
                        if ($scope.exporttabledesigner == "Yes") {
                            window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "tabledesigner");
                        }


                        window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "Products");
                        window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "Families");
                        window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner + "&Type=" + "categories");
                    }

                    else {
                        window.open("DownloadFullExport.ashx?Path=" + response + "&EnableSubProduct=" + $scope.EnableSubProduct + "&EnableTableDesigner=" + $scope.EnableTableDesigner);
                    }
                }

                $scope.Export.PROJECT_NAME = "";
                $scope.Export.COMMENTS = "";
                $scope.famarray = [];
                $scope.selectedAttributeData = [];
                $scope.ExporttreeData.read();
                $scope.fromdate = "";
                $scope.todate = "";

                //
                $.msgBox({
                    title: "MarketStudio",
                    content: "Export completed successfully.",
                    type: "confirm",
                    buttons: [{ value: "Ok" }],
                    success: function (result) {
                        var bool = false;
                        if (result === "Ok") {
                            bool = true;
                            chkEnabled3.checked = false;
                            chkEnabled2.checked = false;
                        }
                        if (bool === true) {

                            $scope.backtopproj = true;
                            // window.location.href = 'App/Export';
                        }
                    }
                });
            }
            else {
                $.msgBox({
                    title: "MarketStudio",
                    content: response,
                    type: "confirm",
                    buttons: [{ value: "Ok" }],
                    success: function (result) {
                        var bool = false;
                        if (result === "Ok") {
                            bool = true;
                        }
                        if (bool === true) {
                            $scope.backtopproj = true;
                            // window.location.href = 'App/Export';
                        }
                    }
                });
            }
            //window.location.pathname = 'App/Export';

        }).error(function (error) {
            options.error(error);
        });


       

        ////////////////////////////////////////////////////////////
    }



}]);
 