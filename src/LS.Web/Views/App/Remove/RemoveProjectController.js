﻿
LSApp.controller('RemoveProject', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope, $localStorage) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    $scope.SelectedprojectId = 0;
    $scope.SelectedprojectName = "";
    var datat = [{ type: "Export", value: "7" },
                   { type: "InDesign Catalog", value: "1" },
                   { type: "PDF Catalog", value: "3" }];

    $("#projecttypes").kendoDropDownList({
        dataTextField: "type",
        dataValueField: "value",
        dataSource: datat,
        index: 0,
        change: function (e) {
            $scope.projectDataSource.read();
        }
    });
    
    $scope.projectDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetProjectDetails($scope.SelectedCatalogId, $("#projecttypes").val()).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.projectChange = function (e) {
        $scope.SelectedprojectId = e.sender.value();
        $scope.SelectedprojectName = e.sender.text();
    };
    $scope.btnDelete = function (a)
    {

        if ($scope.SelectedprojectName !== "")
        {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure want to Remove the Project?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {

                    if (result === "Yes") {


                        $http.post("../App/RemoveProjectFromDb?catlogId=" + $scope.SelectedCatalogId + "&selectedprojectId=" + $scope.SelectedprojectId + "&selectedprojectName=" + $scope.SelectedprojectName)
                            .success(function (msg) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + msg + '.',
                                    type: "info"
                                });

                                $scope.projectDataSource.read();
                            })
                            .error(function () {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Please try again.',
                                    type: "error"
                                });


                            });
                    }
                    else {

                        return;
                    }


                }
            });


        } else {         
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select one project.',
                //type: "info"
            });


        }


    };
    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $scope.SelectedCatalogId = 0;
            $scope.Deletecatalogname = "----SELECT----";
        }
        else {
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
            $scope.Deletecatalogname = $localStorage.getCatalogName;
        }
       
    };
    $scope.init();
}]);

