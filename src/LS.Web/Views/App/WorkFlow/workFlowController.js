﻿LSApp.controller('WorkFlowController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$filter', '$compile', 'productService', '$rootScope', '$localStorage', 'blockUI',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $filter, $compile, productService, $rootScope, $localStorage, blockUI) {

        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

        $scope.Catalog = {
            CATALOG_NAME: '',
            CATALOG_ID: 0,
            VERSION: '1.0',
            DESCRIPTION: 'Sample Catalog',
            FAMILY_FILTERS: '',
            PRODUCT_FILTERS: '',
            familyfilter: [],
            productfilter: [],
            DEFAULT_FAMILY: ''
        };
       
        $scope.WorkFlowFilter =
             {
                 Option: 'All',
                 UserId: 'tbadmin',
                 FromDate: '',
                 ToDate: '',
                 ACTIVE_STATUS_NAME: '',
                 Selected_User: 'aa',
                 Slected_Status: 'SUBMIT',
         
             };
        $scope.workFlowActiveStaus = new kendo.data.HierarchicalDataSource({
            type: "json",
            loadOnDemand: false,
            transport: {
                read: function (options) {
                    dataFactory.getWorkFlowActiveStaus().success(function (response) {
                        
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
            }
        });
        
        $scope.WorkflowStatus = new kendo.data.HierarchicalDataSource({
            type: "json",
            loadOnDemand: false,
            transport: {
                read: function (options) {
                    dataFactory.getWorkflow().success(function (response) {
                       
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
            }
        });

        $scope.WorkflowUsers = new kendo.data.HierarchicalDataSource({
            type: "json",
            loadOnDemand: false,
            transport: {
                read: function (options) {
                    dataFactory.getUserListForWorkFlow().success(function (response) {
                        
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
            }
        });
      
        $scope.workflowtreeDataRead = function () {
            $scope.workflowtreeData.read();
        };
        $scope.workflowtreeData = new kendo.data.HierarchicalDataSource({
            type: "json",
            loadOnDemand: false,
            transport: {
                read: function (options) {
                    $scope.WorkFlowFilter.Selected_User = '';
                    var wfUser = $scope.WorkflowUsers._data;
                    wfUser.forEach(function (item) {
                        var sd = item.checkbxUserSel;
                        if (item.checkbxUserSel == 1)
                        {
                            $scope.WorkFlowFilter.Selected_User = $scope.WorkFlowFilter.Selected_User + "," + item.UserName;
                        }
                    });
                     
                    $scope.WorkFlowFilter.Slected_Status = '';
                    var wfStatus = $scope.WorkflowStatus._data;
                    wfStatus.forEach(function (item) {
                     
                        var sd = item.Vals;
                        if (item.Vals == 1) {
                            $scope.WorkFlowFilter.Slected_Status = $scope.WorkFlowFilter.Slected_Status + "," + item.STATUS_NAME;
                        }
                    });
                   
                    dataFactory.getAllCategoriesForWorkFlow($rootScope.selecetedCatalogId, options.data.id, $scope.WorkFlowFilter.FromDate, $scope.WorkFlowFilter.ToDate, $scope.WorkFlowFilter.Slected_Status, $scope.WorkFlowFilter.Selected_User).success(function (response) {
                        
                        blockUI.start();
                        options.success(response);
                        blockUI.stop();
                      
                    }).error(function (response) {
                        options.success(response);
                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            }
        });
      

        $scope.treeOptionsCatalog = {
            checkboxes: {
                checkChildren: true,
            },
            loadOnDemand: false,
            dataBound: function (e) {
                //console.log(e);
                if (!e.node) {            
                    $scope.attachChangeEvent();
                }
            }
        };

        $scope.attachChangeEvent = function () {
            
            var dataSource = $scope.workflowtreeData;
            $scope.checkeditems = '';
            dataSource.bind("change", function (e) {
                var selectedNodes = 0;
                var checkedNodes = [];
                $scope.checkedNodeIds(dataSource.view(), checkedNodes);
                for (var i = 0; i < checkedNodes.length; i++) {
                    var nd = checkedNodes[i];
                    if (nd.checked) {
                        selectedNodes++;
                    }
                }
                //console.log(selectedNodes);
                $scope.checkeditems = checkedNodes.join(",");

            });
        };
        $scope.checkedNodeIds = function (nodes, checkedNodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].checked) {
                    checkedNodes.push(nodes[i].id);
                }
                if (nodes[i].hasChildren) {
                    $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
                }
            }
        };

        $scope.clickSelectedItemWorkFlow = function (e) {
           
            $("#familyEditor").show();
            //$("#searchtab").hide();
            //$("#resultbtntab").show();
            $rootScope.clickSelectedItem(e);
        };

        $scope.changeWorkFlowStatus = function() {
            if ($scope.checkeditems == '') {               
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Select at least one Category.',
                  //  type: "info"
                });

                return;
            }

            dataFactory.UpdatechangeWorkFlowStatus($scope.WorkFlowFilter.ACTIVE_STATUS_NAME, $scope.SelectedCatalogId, $scope.checkeditems).success(function(response) {
                blockUI.start();              
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Update completed.',
                     type: "info"
                });

                //options.success(response);
                blockUI.stop();

            }).error(function(response) {
                //options.success(response);
            });
        };

        $scope.workFlowUserGridOptions = {
            dataSource: $scope.WorkflowUsers,
            autoBind: true,
            sortable: true,
            pageable: false,
            header:false,
            columns: [
                //{ field: "RESTORE", title: "RESTORE", width: 150, template: '<input type="checkbox" class="chkbx1" #= RESTORE ? "checked=unchecked" : "" # ng-click="updateSelection($event, this)"></input>' },
               { field: "checkbxUserSel", title: "Select", sortable: false, template: '<input type="checkbox" class="chkbx1" #= checkbxUserSel ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>", width: "50px" },
               { field: "UserName", title: "Users", width: "180px" }
              

            ], 
            editable: "inline"
        };
        $scope.updateSelection = function (e, id) {
            if (id.dataItem.checkbxUserSel !== 1) {
                id.dataItem.set("checkbxUserSel", e.target.checked);
            } else {
                id.dataItem.set("checkbxUserSel", true);
            }
        };

        $scope.selectAll = function (ev) {
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            items.forEach(function (item) {
                if (item.checkbxUserSel !== 1) {
                    item.set("checkbxUserSel", ev.target.checked);
                }
            });
        };

        $scope.workFlowStatusGridOptions = {
            dataSource: $scope.WorkflowStatus,
            autoBind: true,
            sortable: true,
            pageable: false,
            header: false,
            columns: [
                //{ field: "RESTORE", title: "RESTORE", width: 150, template: '<input type="checkbox" class="chkbx1" #= RESTORE ? "checked=unchecked" : "" # ng-click="updateSelection($event, this)"></input>' },
               { field: "Vals", title: "Select", sortable: false, template: '<input type="checkbox" class="chkbx1" #= Vals ? "checked=checked" : "" #  ng-click="updateSelectionStatus($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAllStatus($event)'/>", width: "50px" },
               { field: "STATUS_NAME", title: "Status", width: "180px" }


            ],
            editable: "inline"
        };
        $scope.updateSelectionStatus = function (e, id) {
            if (id.dataItem.Vals !== 1) {
                id.dataItem.set("Vals", e.target.checked);
            } else {
                id.dataItem.set("Vals", true);
            }
        };

        $scope.selectAllStatus = function (ev) {
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            items.forEach(function (item) {
                if (item.Vals !== 1) {
                    item.set("Vals", ev.target.checked);
                }
            });
        };

        $scope.init = function () {
           
            if ($localStorage.getCatalogID === undefined) {
                $rootScope.selecetedCatalogId = 0;
            } else {
                $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
            }
            $scope.workflowtreeData.read();
        };

       
    $scope.init();
}]);
