﻿
LSApp.controller('SupplierController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$filter', '$compile', 'productService', '$localStorage', 'blockUI', '$rootScope',
function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $filter, $compile, productService, $localStorage, blockUI, $rootScope) {


    //------ Multiple Table-------------------------------------------------------------------------------------------------------------------------------//

    //-------------------------------Get External Drive Path--------------------------------//    
    $scope.imageDriverPath = '..';
    $scope.extDriveFlag = '';
    $scope.serverSharedPath = '';
    $scope.getImageDrivePath = function () {        
        dataFactory.GetImageDrivePath().success(function (resDrivePath) {         
            if (resDrivePath != null) {
                $scope.imageDriverPath = resDrivePath.m_Item1;
                $scope.extDriveFlag = resDrivePath.m_Item2;
                $scope.serverSharedPath = resDrivePath.m_Item3;
            }
        });
    }
    //-------------------------------Get External Drive Path--------------------------------//

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    // $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
    if ($localStorage.getCustomerCompanyName == undefined) {

        dataFactory.GetCurretUserInfo().success(function (response) {
            $rootScope.currentUser = response;
            $localStorage.getCustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
            $localStorage.getUserName = $rootScope.currentUser.Username;
            $localStorage.getCustomerCompanyName = $rootScope.currentUser.CustomerDetails.Comments;
            $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
            $scope.getImageDrivePath();
        });
    }
    else {
        $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
        $scope.getImageDrivePath();
        //  alert($scope.currentUser.CustomerDetails.Comments);
    }    

    $scope.RefreshGorup = function () {
        $scope.GetAllSupplierdataSource.read();
    };

    $scope.EnableSubProduct = true;
    $scope.countryListDataSource = new kendo.data.DataSource({

        type: "json",
        serverFiltering: true,
        transport: {

            read: function (options) {
               
                dataFactory.getListofCountry().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });


    $scope.stateListDataSource = new kendo.data.DataSource({

        type: "json",
        serverFiltering: true,
        transport: {

            read: function (options) {

                if ($scope.Supplier.COUNTRY !== "" && $scope.Supplier.COUNTRY != undefined) {

                    dataFactory.GetListOfStatesForSupplier($scope.Supplier.COUNTRY).success(function (response) {

                        var sp = $scope.Supplier.STATE;
                        options.success(response);
                        $scope.Supplier.STATE = sp;
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    dataFactory.GetListOfStatesForSupplier().success(function (response) {
                        //var sp = $scope.Supplier.STATE;
                        options.success(response);
                        //$scope.Supplier.STATE = sp;
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        }
    });

    $scope.countryChange = function (e) {

        $scope.Supplier.COUNTRY = e.sender.value();
        $scope.Supplier.STATE = "";
        $scope.stateListDataSource.read();
    };


    $scope.rowIndex = [];
    $scope.initDropDownLists = function (e) {
        $scope.rowIndex = e.sender.dataSource.getByUid(e.model.uid);
        $scope.Supplier.SUPPLIER_NAME = e.model.SUPPLIER_NAME;
        $scope.Supplier.COUNTRY = e.model.COUNTRY;
        $scope.Supplier.STATE = e.model.STATE;
        
        if (!e.model.LOGO_IMAGE_FILE == "" || !e.model.LOGO_IMAGE_FILE == null || !e.model.LOGO_IMAGE_FILE == undefined) {      
            var imagePath = "http://" + window.location.host + "/Content/ProductImages/" + $scope.getCustomerCompanyName + e.model.LOGO_IMAGE_FILE;
            var request = new XMLHttpRequest();
            var extFileExits = '';
            if ($scope.extDriveFlag == 'true') {
                imagePath = $scope.imageDriverPath + "Content/ProductImages/" + $scope.getCustomerCompanyName + e.model.LOGO_IMAGE_FILE;
                //t = 'http://10.0.0.127:84/Content/ProductImages/DEMO/Images/FT.jpg'
            }
                check(imagePath)
                  .on("error", function (f) {
                      extFileExits = 'Failure';
                      $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW = "";
                      $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW1 = "\\images\\filenotfound1.jpg";
                  })
                  .on("load", function (f) {
                      extFileExits = 'success';
                      var path = e.model.LOGO_IMAGE_FILE;
                      if (path.toLowerCase().endsWith('.gif') || path.toLowerCase().endsWith('.jpg') || path.toLowerCase().endsWith('.jpeg') || path.toLowerCase().endsWith('.png') || path.toLowerCase().endsWith('.bmp')) {
                          $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW = path;
                      }
                      else if (path.toLowerCase().endsWith(".eps") || path.toLowerCase().endsWith(".tif") || path.toLowerCase().endsWith(".tiff") || path.toLowerCase().endsWith(".psd") ||
                            path.toLowerCase().endsWith(".tga") || path.toLowerCase().endsWith(".pcx")) {
                          dataFactory.Getimagevaluevalues(path).success(function (response) {
                              //
                              var companyName = '/' + $scope.getCustomerCompanyName;
                              var convertedPath = response.replace(companyName, '');
                              $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW = convertedPath;
                          }).error(function (error) {
                              options.error(error);
                          });
                      }
                  })
                function check(src) {
                    return $("<img>").attr('src', src);
                }                
                var countries = $("#countrydropdown").kendoDropDownList({
                    dataTextField: "COUNTRY",
                    dataValueField: "COUNTRY_CODE",
                    dataSource: $scope.countryListDataSource
                }).data("kendoDropDownList");
                var categories = $("#statesdropdown").kendoDropDownList({
                    dataTextField: "STATE",
                    dataValueField: "STATE_CODE",
                    dataSource: $scope.stateListDataSource.read()
                }).data("kendoDropDownList");
            //else {
            //    request.open('HEAD', imagePath, false);
            //    request.send();
            //}            
        }
       
    };


    $scope.GetAllSupplierdataSource = new kendo.data.DataSource({
        pageSize: 10,
        type: "json",
        serverFiltering: true, editable: true,
        serverPaging: true,
        serverSorting: true, pageable: true,
        transport: {
            read: function (options) {

                dataFactory.GetAllSupplier(options.data, $localStorage.getCustomerID).success(function (response) {
                    options.success(response);

                    $scope.SupllierEntireData = response.Data;
                }).error(function (error) {
                    options.error(error);
                });
            }
            , update: function (options) {

                for (var i = 0 ; i < $scope.SupllierEntireData.length ; i++)
                {
                    if ((options.data.SUPPLIER_NAME == $scope.SupllierEntireData[i]["SUPPLIER_NAME"]) && ($scope.SupllierEntireData[i]["SUPPLIER_ID"] != options.data["SUPPLIER_ID"]))
                    {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Supplier name already exist.',
                            type: "info"
                        });
                        return;
                    }
                }
                if (options.data.SUPPLIER_NAME != null && options.data.SUPPLIER_NAME != "")
                {
                  
                    if ($scope.Supplier.LOGO_IMAGE_FILE_PREVIEW == "" || $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW1 == "") {
                        options.data.LOGO_IMAGE_FILE = $scope.Supplier.LOGO_IMAGE_FILE;
                    }

                    if ($scope.Supplier.COUNTRY == "") {
                        options.data.STATE = "";
                        options.data.COUNTRY = "";
                    }

                    $scope.arrCountry = $.makeArray(options.data.COUNTRY);

                    var country = $scope.arrCountry[0].COUNTRY;
                    if (country == undefined) {
                        country = $scope.arrCountry[0];
                    }


                    $scope.arrState = $.makeArray(options.data.STATE);
                    if ($scope.arrState.length > 0) {
                        var state = $scope.arrState[0].STATE;
                    }


                    if (state == undefined) {
                        state = $scope.arrState[0];

                    }

                    if (($scope.Supplier.COUNTRY == country && $scope.Supplier.STATE == state) || ($scope.Supplier.COUNTRY == "" && $scope.Supplier.STATE == "")) {
                        dataFactory.UpdateSupplierDetails($scope.Supplier.SUPPLIER_NAME, options.data.COUNTRY.COUNTRY, options.data.STATE.STATE, options.data).success(function (response) {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Update Successful.',
                                type: "info"
                            });
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Please select State option.",
                            type: "info"
                        });
                    }
                }else
                {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Must fill supplier name .",
                        type: "info"
                    });


                }
                

            }, 
            
            //destroy: function (options) {

            //        dataFactory.DeleteSupplier(options.data.SUPPLIER_NAME).success(function (response) {

            //        $.msgBox({
            //            title: $localStorage.ProdcutTitle,
            //            content: '' + response + '' + options.data.SUPPLIER_NAME + ' ' + 'deleted successfully..',
            //            type: "info"
            //        });

            //        options.success(response);

            //    }).error(function (error) {
            //        options.error(error);
            //    });
                       
            //},
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
                return kendo.stringify(options);

            }
        }, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "SUPPLIER_NAME",
               // id: "SUPPLIER_ID",
                fields: {
                    SUPPLIER_NAME: { editable: true, nullable: true },
                    SUPPLIER_COMPANY_NAME: { editable: true },
                    CITY: { editable: true },
                    STATE: { editable: true, nullable: true },
                    COUNTRY_CODE: { editable: true, nullable: true },
                    COUNTRY: { editable: true, nullable: true },
                    LOGO_IMAGE_FILE: { editable: true, nullable: true },
                    ADDRESS_LINE_1: { editable: true },
                    ADDRESS_LINE_2: { editable: true },
                    ADDRESS_LINE_3: { editable: true },
                    ZIP: { editable: true, type: "number" },
                    PHONE_1: { editable: true, nullable: true, type: "number" },
                    FAX: { editable: true, type: "number" },
                    EMAIL: { editable: true },
                    URL: { editable: true },
                    SUPPLIER_ID :{ hidden: true}
                }
            }
        }
    });


    $scope.DeleteSupplierRecords = function (e,options) {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to delete the record?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {

                    dataFactory.DeleteSupplier(options.dataItem["SUPPLIER_NAME"]).success(function (response) {

                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '' + options.dataItem["SUPPLIER_NAME"] + ' ' + 'deleted successfully',
                            type: "info"
                        });
                        $scope.GetAllSupplierdataSource.read();

                       // options.success(response);
                       

                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    $scope.GetAllSupplierdataSource.read();
                }
            }
        });
    };


    function onEditorClosed(e) {
        $scope.GetAllSupplierdataSource.read();
    }

    $scope.supplierGridOptions = {
        dataSource: $scope.GetAllSupplierdataSource,
        sortable: true,
        scrollable: true, filterable: true,
        pageable: { buttonCount: 5 },
        autoBind: true,
        editable: {
            mode: "popup",
            template: kendo.template($("#popup_suppliereditor").html()), update: true,
            //confirmation: function (e) {
            //    return "Are you sure you want to delete " + e.SUPPLIER_NAME + " record?";
            //},

        }, edit: function (e) {

            e.model.dirty = true;
            e.container.data("kendoWindow").one("close", onEditorClosed);
            //$("#logoimage").val("");
            //$scope.Supplier.LOGO_IMAGE_FILE = e.model.LOGO_IMAGE_FILE;
            $scope.initDropDownLists(e);
        },

        selectable: "row", toolbar: [
      { template: kendo.template($("#createtemplate").html()) },

    // { template: '<a class="k-button k-button-icontext k-grid-excel" >Export</a>' }


        ], excel: {
            fileName: "Supplier List.xlsx",
            allPages: true,
            filterable: true

        },
        columns: [{ field: "SUPPLIER_NAME", title: "Supplier Name", width: "150px" },
            { field: "SUPPLIER_COMPANY_NAME", title: "Company Name", width: "150px" },
            { field: "CITY", title: "City", width: "100px" },
            { field: "STATE", title: "State", width: "100px" },
            { field: "COUNTRY", title: "Country", width: "100px" },
            { field: "LOGO_IMAGE_FILE", title: "Logo", width: "100px" },
            { field: "ADDRESS_LINE_1", title: "Address 1", width: "100px" },
            { field: "ADDRESS_LINE_2", title: "Address 2", width: "100px" },
            { field: "ADDRESS_LINE_3", title: "Address 3", width: "100px" },
            { field: "ZIP", title: "Zip", width: "100px", type: "number" },
            { field: "PHONE_1", title: "Phone", width: "100px" },
            { field: "FAX", title: "Fax", width: "100px" },
            { field: "EMAIL", title: "Email", width: "100px" },
            { field: "URL", title: "URL", width: "100px" },
            { command: [{ name: 'edit', text: "", width: "10px", template: "<a class=\'k-grid-edit k-item girdicons\'><div title=\'Edit\' class=\'glyphicon glyphicon-edit blue btn-xs-icon\'></div></a>" }, { name: "destroy", text: "", width: "10px", template: "<a class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\' ng-click=\'DeleteSupplierRecords($event,this)\'></div></a>" }], title: "Actions", width: "100px" }],
        dataBound: function (e) {
            if (e.sender._data.length > 0) {
                var row = this.tbody.find('tr:first');
                this.select(row);
                //$scope.Table.TABLE_ID = e.sender._data[0].TABLE_ID;
                //$scope.Table.TABLE_NAME = e.sender._data[0].TABLE_NAME;
                // $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            } else {
                //$scope.Table.TABLE_ID = 0;
                //$scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            }
        }
    };

    $scope.supplierGridOptionsWithoutEdit = {
        dataSource: $scope.GetAllSupplierdataSource,
        sortable: true,
        scrollable: true, filterable: true,
        pageable: { buttonCount: 5 },
        autoBind: true,
        editable: {
            mode: "popup",
            template: kendo.template($("#popup_suppliereditor").html()), update: true,
            //confirmation: function (e) {
            //    return "Are you sure you want to delete " + e.SUPPLIER_NAME + " record?";
            //},

        }, edit: function (e) {
            e.container.data("kendoWindow").one("close", onEditorClosed);
            $scope.initDropDownLists(e);
        },

        selectable: "row", toolbar: [
      { template: kendo.template($("#createtemplate").html()) },
       // { template: '<a class="k-button k-button-icontext k-grid-excel" ></a>' }
        ], excel: {
            fileName: "Supplier List.xlsx",
            allPages: true,
            filterable: true
        },
        columns: [{ field: "SUPPLIER_NAME", title: "Supplier Name", width: "150px" },
            { field: "SUPPLIER_COMPANY_NAME", title: "Company Name", width: "150px" },
            { field: "CITY", title: "City", width: "100px" },
            { field: "STATE", title: "State", width: "100px" },
            { field: "COUNTRY", title: "Country", width: "100px" },
            { field: "LOGO_IMAGE_FILE", title: "Logo", width: "100px" },
            { field: "ADDRESS_LINE_1", title: "Address 1", width: "100px" },
            { field: "ADDRESS_LINE_2", title: "Address 2", width: "100px" },
            { field: "ADDRESS_LINE_3", title: "Address 3", width: "100px" },
            { field: "ZIP", title: "Zip", width: "100px", type: "number" },
            { field: "PHONE_1", title: "Phone", width: "100px" },
            { field: "FAX", title: "Fax", width: "100px" },
            { field: "EMAIL", title: "Email", width: "100px" },
            { field: "URL", title: "Url", width: "100px" },
            { command: [{ name: "destroy", text: "", width: "10px", template: "<a class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" }], title: "Actions", width: "100px" }],
        dataBound: function (e) {
            if (e.sender._data.length > 0) {
                var row = this.tbody.find('tr:first');
                this.select(row);
                //$scope.Table.TABLE_ID = e.sender._data[0].TABLE_ID;
                //$scope.Table.TABLE_NAME = e.sender._data[0].TABLE_NAME;
                // $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            } else {
                //$scope.Table.TABLE_ID = 0;
                //$scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            }
        }
    };
    $scope.supplierGridOptionsWithoutDelete = {
        dataSource: $scope.GetAllSupplierdataSource,
        sortable: true,
        scrollable: true, filterable: true,
        pageable: { buttonCount: 5 },
        autoBind: true,
        editable: {
            mode: "popup",
            template: kendo.template($("#popup_suppliereditor").html()), update: true,
            //confirmation: function (e) {
            //    return "Are you sure you want to delete " + e.SUPPLIER_NAME + " record?";
            //},

        }, edit: function (e) {
            e.container.data("kendoWindow").one("close", onEditorClosed);
            $scope.initDropDownLists(e);
        },


        selectable: "row", toolbar: [
      { template: kendo.template($("#createtemplate").html()) },
     // { template: '<a class="k-button k-button-icontext k-grid-excel" >Export</a>' }
        ], excel: {
            fileName: "Supplier List.xlsx",
            allPages: true,
            filterable: true
        },
        columns: [{ field: "SUPPLIER_NAME", title: "Supplier Name", width: "150px" },
            { field: "SUPPLIER_COMPANY_NAME", title: "Company Name", width: "150px" },
            { field: "CITY", title: "City", width: "100px" },
            { field: "STATE", title: "State", width: "100px" },
            { field: "COUNTRY", title: "Country", width: "100px" },
            { field: "LOGO_IMAGE_FILE", title: "Logo", width: "100px" },
            { field: "ADDRESS_LINE_1", title: "Address 1", width: "100px" },
            { field: "ADDRESS_LINE_2", title: "Address 2", width: "100px" },
            { field: "ADDRESS_LINE_3", title: "Address 3", width: "100px" },
            { field: "ZIP", title: "Zip", width: "100px", type: "number" },
            { field: "PHONE_1", title: "Phone", width: "100px" },
            { field: "FAX", title: "Fax", width: "100px" },
            { field: "EMAIL", title: "Email", width: "100px" },
            { field: "URL", title: "Url", width: "100px" }, { command: [{ name: 'edit', text: "", width: "10px", template: "<a class=\'k-grid-edit k-item girdicons\'><div title=\'Edit\' class=\'glyphicon glyphicon-edit blue btn-xs-icon\'></div></a>" }], title: "Actions", width: "100px" }

        ],
        dataBound: function (e) {
            if (e.sender._data.length > 0) {
                var row = this.tbody.find('tr:first');
                this.select(row);
                //$scope.Table.TABLE_ID = e.sender._data[0].TABLE_ID;
                //$scope.Table.TABLE_NAME = e.sender._data[0].TABLE_NAME;
                // $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            } else {
                //$scope.Table.TABLE_ID = 0;
                //$scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            }
        }
    };
    $scope.supplierGridOptionsWithoutEditDelete = {
        dataSource: $scope.GetAllSupplierdataSource,
        sortable: true,
        scrollable: true,
        filterable: true,
        pageable: { buttonCount: 5 },
        autoBind: true,
        //editable: {
        //    mode: "popup",
        //    template: kendo.template($("#popup_suppliereditor").html()), update: true,
        //    confirmation: function (e) {
        //        return "Are you sure that you want to delete record for " + e.SUPPLIER_NAME + "?";
        //    },

        //}, edit: function (e) {
        //    $scope.initDropDownLists(e);
        //},

        selectable: "row", toolbar: [
      { template: kendo.template($("#createtemplate").html()) },
      // { template: '<a class="k-button k-button-icontext k-grid-excel" >Export</a>' }
        ], excel: {
            fileName: "Supplier List.xlsx",
            allPages: true,
            filterable: true
        },
        columns: [{ field: "SUPPLIER_NAME", title: "Supplier Name", width: "150px" },
            { field: "SUPPLIER_COMPANY_NAME", title: "Company Name", width: "150px" },
            { field: "CITY", title: "City", width: "100px" },
            { field: "STATE", title: "State", width: "100px" },
            { field: "COUNTRY", title: "Country", width: "100px" },
            { field: "LOGO_IMAGE_FILE", title: "Logo", width: "100px" },
            { field: "ADDRESS_LINE_1", title: "Address 1", width: "100px" },
            { field: "ADDRESS_LINE_2", title: "Address 2", width: "100px" },
            { field: "ADDRESS_LINE_3", title: "Address 3", width: "100px" },
            { field: "ZIP", title: "Zip", width: "100px", type: "number" },
            { field: "PHONE_1", title: "Phone", width: "100px" },
            { field: "FAX", title: "Fax", width: "100px" },
            { field: "EMAIL", title: "Email", width: "100px" },
            { field: "URL", title: "Url", width: "100px" }
        ],
        dataBound: function (e) {
            if (e.sender._data.length > 0) {
                var row = this.tbody.find('tr:first');
                this.select(row);
                //$scope.Table.TABLE_ID = e.sender._data[0].TABLE_ID;
                //$scope.Table.TABLE_NAME = e.sender._data[0].TABLE_NAME;
                // $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            } else {
                //$scope.Table.TABLE_ID = 0;
                //$scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            }
        }
    };

    $scope.handleChange = function (dataItem) {
        if (dataItem != undefined) {
            //$scope.Table.TABLE_ID = dataItem.TABLE_ID;
            //$scope.Table.TABLE_NAME = dataItem.TABLE_NAME;
            //$scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
        }
    };
    $scope.Supplier = {
        SUPPLIER_NAME: "",
        SUPPLIER_COMPANY_NAME: "",
        CITY: "",
        STATE_CODE: "",
        STATE: "",
        COUNTRY_CODE: "",
        COUNTRY: "",
        LOGO_IMAGE_FILE: "",
        ADDRESS_LINE_1: "",
        ADDRESS_LINE_2: "",
        ADDRESS_LINE_3: "",
        ZIP: 0,
        PHONE_1: "",
        FAX: "",
        EMAIL: "",
        URL: ""
    };
    $scope.AddnewsupplierClick = function () {
        $scope.Supplier = {
            SUPPLIER_NAME: "",
            SUPPLIER_COMPANY_NAME: "",
            CITY: "",
            STATE_CODE: "",
            STATE: "",
            COUNTRY_CODE: "",
            COUNTRY: "",
            LOGO_IMAGE_FILE: "",
            ADDRESS_LINE_1: "",
            ADDRESS_LINE_2: "",
            ADDRESS_LINE_3: "",
            ZIP: 0,
            PHONE_1: "",
            FAX: "",
            EMAIL: "",
            URL: ""
        };
        $("#suppliergrid").hide();
        $("#addnewsupplier").show();
    };
    $scope.ManageSupplier = function () {
        $scope.GetAllSupplierdataSource.read();
        $("#suppliergrid").show();
        $("#addnewsupplier").hide();
    };
    $scope.NewSupplierSave = function (supplier) {

        if (supplier.SUPPLIER_NAME.trim() == "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a valid Name.',
                // type: "info"
            });
        }
        else {
            if ((supplier.COUNTRY == "" && supplier.STATE == "") || (supplier.COUNTRY != "" && supplier.STATE != "")) {
                dataFactory.savenewsupplier($localStorage.getCustomerID, supplier).success(function (response) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });

                    $scope.GetAllSupplierdataSource.read();
                }).error(function (error) {
                    options.error(error);
                });
            }
            else
            {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Please select State option.",
                    type: "info"
                });
            }
        }
    };

    //var grid = $("#gridsupplierdetails").kendoGrid();
    //$(grid.tbody).on("click", "td", function (e) {
    //    var row = $(this).closest("tr");
    //    var rowIdx = $("tr", grid.tbody).index(row);
    //    var colIdx = $("td", row).index(this);
    //    var item = grid.dataItem(row);
    //    var id = item.Id;
    //    alert("row:" + rowIdx + " col:" + colIdx + " id:" + id + "\nitem:" + JSON.stringify(item, null, 4));
    //});

    $scope.onSuccess1 = function (e) {

        $scope.Supplier.LOGO_IMAGE_FILE = '';

        if (e.operation === "remove") {
            $scope.Table.ImagePreview = "";
        } else {
            var message = $.map(e.files, function (file) {
                return file.name;
            }).join(", ");
            if (message.indexOf('&') == -1 && message.indexOf('#') == -1 && message.indexOf('\'') == -1) {
                var imgext = message.substring(message.length, message.length - 4);
                $scope.$apply(function () {

                    $scope.Supplier.LOGO_IMAGE_FILE = '\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;
                    var firstItem = $scope.rowIndex;
                    firstItem.set('LOGO_IMAGE_FILE', $scope.Supplier.LOGO_IMAGE_FILE);
                });
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Image name contains invalid special characters( # & \' ).',
                    type: "error"
                });

                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            }

        }
    };

    $scope.onSuccess = function (e) {


        $scope.Supplier.LOGO_IMAGE_FILE = '';



        if (e.operation === "remove") {

            $scope.Supplier.LOGO_IMAGE_FILE = '';
            $scope.Table.ImagePreview = "";
        } else {
            var message = $.map(e.files, function (file) {
                return file.name;
            }).join(", ");
            if (message.indexOf('&') == -1 && message.indexOf('#') == -1 && message.indexOf('\'') == -1) {
                var imgext = message.substring(message.length, message.length - 4);
                $scope.$apply(function () {
                    $scope.Supplier.LOGO_IMAGE_FILE = '\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;
                });
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Image name contains invalid special characters( # & \' ).',
                    type: "error"
                });

                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            }
        }
    };
    $scope.onSelect = function (e) {
        var message = $.map(e.files, function (file) { return file.name; }).join(", ");
    };

    $scope.userRoleAddSupplier = false;
    $scope.userRoleModifySupplier = false;
    $scope.userRoleDeleteSupplier = false;
    $scope.userRoleViewSupplier = false;

    $scope.getUserRoleRightsSupplier = function () {
        var id = 40206;
        dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
            if (response !== "" && response !== null) {
                $scope.userRoleAddSupplier = response[0].ACTION_ADD;
                $scope.userRoleModifySupplier = response[0].ACTION_MODIFY;
                $scope.userRoleDeleteSupplier = response[0].ACTION_REMOVE;
                $scope.userRoleViewSupplier = response[0].ACTION_VIEW;
            }

        }).error(function (error) {
            options.error(error);
        });
    };


    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        }
        else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerID = 0;

        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
        } $("#suppliergrid").show();
        $("#addnewsupplier").hide();
        $scope.getUserRoleRightsSupplier();
    };

    //asset management implementation//
    $scope.OpenPopupWindow = function (e) {
        

        $rootScope.paramValue = e.value;
        $scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
        $scope.winManageDrive.title("Asset Management");
        $scope.winManageDrive.center().open();
        $scope.driveMahementIsVisible = true;

    }

    $rootScope.SaveSupplierFileSelection = function (fileName, path) {
 
        $scope.winManageDrive.center().close();
        $rootScope.categoryAttachPath = path;
        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            if ($rootScope.paramValue == "SupplierImageUpdate") {
                $scope.LOGO_IMAGE_FILE = path;
                $scope.Supplier.LOGO_IMAGE_FILE = path;
                $('#logoimage').val(path);
                if (path.toLowerCase().endsWith('.gif') || path.toLowerCase().endsWith('.jpg') || path.toLowerCase().endsWith('.jpeg') || path.toLowerCase().endsWith('.png') || path.toLowerCase().endsWith('.bmp')) {
                    $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW = path;
                }
                else if (path.toLowerCase().endsWith(".eps") || path.toLowerCase().endsWith(".tif") || path.toLowerCase().endsWith(".tiff") || path.toLowerCase().endsWith(".psd") ||
                      path.toLowerCase().endsWith(".tga") || path.toLowerCase().endsWith(".pcx") ) {
                    dataFactory.Getimagevaluevalues(path).success(function (response) {
                        //
                        var companyName = '/' + $scope.getCustomerCompanyName;
                        var convertedPath = response.replace(companyName, '');
                        $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW = convertedPath;
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
            if ($rootScope.paramValue == "SupplierImageSave") {
                $('#logoimage').val(path);
                if (path.toLowerCase().endsWith('.gif') || path.toLowerCase().endsWith('.jpg') || path.toLowerCase().endsWith('.jpeg') || path.toLowerCase().endsWith('.png') || path.toLowerCase().endsWith('.bmp')) {
                    //
                    $scope.Supplier.LOGO_IMAGE_FILE = path;
                    $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW = path;
                    $('#logoimage').val(path);
                }
                else if (path.toLowerCase().endsWith(".eps") || path.toLowerCase().endsWith(".tif") || path.toLowerCase().endsWith(".tiff") || path.toLowerCase().endsWith(".psd") ||
                      path.toLowerCase().endsWith(".tga") || path.toLowerCase().endsWith(".pcx") ) {
                    dataFactory.Getimagevaluevalues(path).success(function (response) {
                        //
                        var companyName = '/' + $scope.getCustomerCompanyName;
                        var convertedPath = response.replace(companyName, '');
                        $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW = convertedPath;
                        $scope.Supplier.LOGO_IMAGE_FILE = path;
                        $('#logoimage').val(path);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        }

        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }

        $("#logoimage").trigger("change");
        $('#logoimage').change(function () {
            console.log('Trigger Event');
        });
        $scope.driveMahementIsVisible = false;
    }

    $scope.EMAILVal = '^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    $scope.URL = '^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$';
    //$scope.AllowChar = function (e) {

    //    if (e.shiftKey || e.ctrlKey || e.altKey) {
    //        e.preventDefault();
    //    } else {
    //        var key = e.keyCode;
    //        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
    //            e.preventDefault();
    //        }
    //    }
    //}

    $scope.AllowChar = function (e) {

        if (e.ctrlKey || e.altKey) {
            e.preventDefault();
        } else {
            var key = e.keyCode;
            if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                e.preventDefault();
            }
        }
    }
    $rootScope.Clearimage = function (Imageclear, data) {

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to Clear Image?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {

                if (result === "Yes") {

                    $scope.Supplier.LOGO_IMAGE_FILE = "";
                    $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW = "";
                    $scope.Supplier.LOGO_IMAGE_FILE_PREVIEW1 = "";
                    $scope.Supplier.Imageclear = "";
                    
                    if (data == 'Update') {
                        $scope.Imageupdateclear = "";
                        $scope.Imageupdateclear1 = "";
                        $('#logoimage').val("").trigger('input');
                        
                    }
                    else {

                        $scope.GetAllSupplierdataSource.read();
                        //$rootScope.paramValue == "SupplierImageUpdate";

                    }
                    $scope.$apply();
                }
                else {
                    return;
                }
            }
        });




    };

    //To chech the Image Format
    $scope.isImage = function (image) {
        if (image.split('.')[image.split('.').length - 1] == "eps" || image.split('.')[image.split('.').length - 1] == "psd" || image.split('.')[image.split('.').length - 1] == "tiff" || image.split('.')[image.split('.').length - 1].toLowerCase() == "tif") {
            $scope.imageInvalid = "Preview only available in Family/Category preview";
            return true;
        } else {
            return false;
        }
    };


    $scope.init();




}]);
