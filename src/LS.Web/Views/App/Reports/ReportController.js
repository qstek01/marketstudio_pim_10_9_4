﻿
LSApp.controller('ReportController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$localStorage', '$rootScope', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $localStorage, $rootScope, $tooltip) {
    debugger
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    $scope.reportGrid = $localStorage.DisplayIdBycoloumValue;
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    $rootScope.ReportDetails = "";
    $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
    $('#duplicateimg').hide();
    $scope.selectedRowDynamicAttributes = [];
    $scope.activationDate = new Date();
    var DefaultDateZone = new Date();
    $scope.SelectedTimeZone = DefaultDateZone.toString().split("(")[1].replace(")", "");
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();

    currentDate = month + "/" + day + "/" + year;
    firstDay = month + "/" + day + "/" + year;

    $scope.pageno = "1";
    $scope.ProdCurrentPage = "1";
    $scope.ProdCountPerPage = "10";

    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    $scope.tomorrow = new Date();
    $scope.tomorrow.setDate($scope.tomorrow.getDate() - 1);

    var monthSD = $scope.tomorrow.getUTCMonth() + 1;
    var daySD = $scope.tomorrow.getUTCDate();
    var yearSD = $scope.tomorrow.getUTCFullYear();


    //firstDay = monthSD + "/" + daySD + "/" + yearSD;
    var monthName = monthNames[dateObj.getMonth()];
    var monthNameSD = monthNames[$scope.tomorrow.getMonth()];

    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);



    $scope.startDateModify = monthNameSD + ' ' + "01" + ',' + yearSD;
    $scope.endDateModify = monthNameSD + ' ' + day + ',' + yearSD;

    $scope.startDatenew = monthNameSD + ' ' + "01" + ',' + yearSD;

    $scope.endDatenew = monthNameSD + ' ' + day + ',' + yearSD;

    //discontinued products
    $scope.start_Date = monthName + ' ' + "01" + ',' + year;
    $scope.end_Date = monthName + ' ' + day + ',' + year;
    $scope.fileNames = "D:" + "/" + "SATHYA" + "/" + "CS10.6.2_HG_16Aug2019" + "/" + "src" + "/" + "LS.Web" + "/" + "Content" + "/" + "ProductImages" + "/" + "westward";
    $scope.init();
    $scope.ProdCurrentPage = "1";
    $scope.ProdCountPerPage = "10";
    $scope.dataSource = new kendo.data.DataSource({
        type: "json",
        sort: [{ field: "MissingImageName", dir: "asc" }],
        serverFiltering: true,
        serverPaging: true,
        serverSorting: true, editable: true, pageable: { buttonCount: 5 }, pageSize: 5,
        transport: {
            read: function (options) {

                dataFactory.GetAllMissingNames($scope.SelectedCatalogId, options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "MissingImageName",
                fields: {
                    MissingImageName: { editable: false },
                    ImagePath: { editable: false }
                }
            }
        }

    });

    $scope.mainGridOptions = {

        toolbar: [{ template: '<a class="k-button k-button-icontext k-grid-excel" >Export</a>' }],


        excel: {
            fileName: "Missing Image List.xlsx",
            allPages: true,
            filterable: true
        },
        dataSource: $scope.dataSource,
        sortable: true, scrollable: true, pageable: { buttonCount: 5 }, autoBind: true,
        selectable: "row", filterable: true,
        columns: [
            { field: "MissingImageName", title: "Missing Image Name", width: "250px" },
            { field: "ImagePath", title: "Image Path" }],
        editable: false
    };
    $scope.GetFilePathSavedLocation = function () {
        dataFactory.GetServerFilePath().success(function (response) {
            $scope.getPathFromWebConfig = response[0];
            $scope.isLocalOrServer = response[1];

        }).error(function (error) {
            options.error(error);
        });
    }


    $scope.init = function () {

        if ($localStorage.getCatalogID === undefined) {
            $scope.SelectedCatalogId = 0;
        }
        else {
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
        }
    };
    var startDate = $scope.startDatenew;
    var endDate = $scope.endDatenew;

    $('#duplicateimg').hide();

    //=============================ng-click functions for reports menu===================================================//
    $scope.catalogsum = function () {
        $('#duplicateimg').hide();
        $("#catsum").show();
        $("#catsum").css("display", "block");
        $("#missimage").hide();
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#Products_Modified_in_Last_7_days_Report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#DATASS").hide();
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $('#discontinuedproductdatepicker').hide();

        $("#datetimepicker_from").css("display", "none");
        $("#datetimepicker_to").css("display", "none");

        $("#discontinuedproducts").hide();
        $scope.DATASS = false;

    };
    $scope.famcreated7days = function () {
        $('#duplicateimg').hide();
        $("#catsum").hide();
        $("#missimage").hide();
        $("#DATASS").hide();
        $("#products_modified_in_last_7_days_report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $("#discontinuedproducts").hide();
        $("#Family_Created_in_Last_7_days_Report").show();
        $("#Family_Created_in_Last_7_days_Report").css("display", "block");
        $scope.selects = "Family Created in Last 7 days Report";
        $scope.ImageTypeReportsoptionsDatasource.read();
        $scope.DATASS = false;

    };
    $scope.prodmodfi7days = function () {
        $('#duplicateimg').hide();
        $("#catsum").hide();
        $("#missimage").hide();
        $("#DATASS").hide();
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $("#discontinuedproducts").hide();
        $("#Products_Modified_in_Last_7_days_Report").show();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "block");
        $scope.selects = "Products Modified in Last 7 days Report";
        $scope.ImageTypeReportsoptionsDatasource.read();
        $scope.DATASS = false;

    };
    $scope.duplicateitem = function () {
        $('#duplicateimg').hide();
        $("#catsum").hide();
        $("#missimage").hide();
        $("#DATASS").hide();
        $("#Products_Modified_in_Last_7_days_Report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#Duplicate_item_Report").show();
        $("#Duplicate_item_Report").css("display", "block");
        $scope.selects = "Duplicate Catalog Item Number Report";
        $scope.ImageTypeReportsoptionsDatasource.read();
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $("#discontinuedproducts").hide();

    };
    $scope.supplierlist = function () {
        $('#duplicateimg').hide();
        $("#catsum").hide();
        $("#missimage").hide();
        $("#DATASS").hide();
        $("#Products_Modified_in_Last_7_days_Report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#supplier_list_report").show();
        $("#supplier_list_report").css("display", "block");
        $scope.selects = "Supplier List Report";
        $scope.ImageTypeReportsoptionsDatasource.read();
        $scope.DATASS = false;
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $("#discontinuedproducts").hide();


    };
    $scope.allreports = function () {
        $('#duplicateimg').hide();
        $("#catsum").hide();
        $("#missimage").hide();
        $("#DATASS").hide();
        $("#Products_Modified_in_Last_7_days_Report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#all_report").show();
        $("#all_report").css("display", "block");
        $scope.selects = "All Attributes Report";
        $scope.ImageTypeReportsoptionsDatasource.read();
        $scope.DATASS = false;
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $("#discontinuedproducts").hide();
    };
    $scope.modifiedprods = function () {
        $scope.GetFilePathSavedLocation();
        $('#duplicateimg').hide();
        $("#catsum").hide();
        $("#missimage").hide();
        $("#DATASS").hide();
        $("#Products_Modified_in_Last_7_days_Report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#modifiedproductsreport").show();
        $("#modifiedproductsreport").css("display", "block");
        $("#table10").hide();
        $('#modifiedproductsreport').removeClass("col-sm-5");
        $scope.selects = "Report modified products";
        $scope.ImageTypeReportsoptionsDatasource.read();
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $("#discontinuedproducts").hide();
    };
    $scope.newprods = function () {
        $('#duplicateimg').hide();
        $("#catsum").hide();
        $("#missimage").hide();
        $("#DATASS").hide();
        $("#Products_Modified_in_Last_7_days_Report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").show();
        $("#newproducts").css("display", "block");
        $("#table10").hide();
        $("#newproducts").removeClass("col-sm-5");
        $scope.selects = "Report new products";
        $scope.ImageTypeReportsoptionsDatasource.read();
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $("#discontinuedproducts").hide();
    };
    $scope.datacom = function () {

        $("#catsum").hide();
        $("#catsum").css("display", "block");
        $("#missimage").hide();
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#Products_Modified_in_Last_7_days_Report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $("#discontinuedproducts").hide();
        $scope.DATASS = true;
    }
    $scope.duplicateimagelist = function () {
        $("#catsum").hide();
        $("#missimage").hide();
        $("#Products_Modified_in_Last_7_days_Report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $('#duplicateimg').show();
        $("#duplicateimg").css("display", "block");
        $("#table10").css("display", "none");
        $("#table10").hide();
        $scope.selects = "Duplicate image list";
        $scope.ImageTypeReportsoptionsDatasource.read();
        $("#datetimepicker_from").hide();
        $("#datetimepicker_to").hide();
        $("#discontinuedproducts").hide();

    };

    $scope.discontinuedProducts = function () {
        $('#duplicateimg').hide();
        $("#catsum").hide();
        $("#missimage").hide();
        $("#table10").css("display", "none");
        $("#table10").hide();
        $("#Products_Modified_in_Last_7_days_Report").hide();
        $("#Products_Modified_in_Last_7_days_Report").css("display", "none");
        $("#Family_Created_in_Last_7_days_Report").hide();
        $("#Family_Created_in_Last_7_days_Report").css("display", "none");
        $("#Duplicate_item_Report").hide();
        $("#Duplicate_item_Report").css("display", "none");
        $("#supplier_list_report").hide();
        $("#supplier_list_report").css("display", "none");
        $("#all_report").hide();
        $("#all_report").css("display", "none");
        $("#modifiedproductsreport").hide();
        $("#modifiedproductsreport").css("display", "none");
        $("#newproducts").hide();
        $("#newproducts").css("display", "none");
        $("#duplicateimg").hide();
        $("#duplicateimg").css("display", "block");
        $("#datacompleteness").hide();
        $("#datepicker_From").hide();
        $("#datepicker_To").hide();
        $('#discontinuedproductdatepicker').show();
        $("#datetimepicker_from").show();
        $("#datetimepicker_to").show();
        $("#discontinuedproducts").show();
        $scope.selects = "DisContinued Products";
        $scope.ImageTypeReportsoptionsDatasource.read();

    };

    //==================================kendo grid datasource=======================================//
    $scope.ImageTypeReportsoptionsDatasource = new kendo.data.DataSource({
        pageSize: 10,
        batch: false,
        autoBind: true,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        transport: {
            read: function (options) {

                dataFactory.GetAllReports($scope.SelectedCatalogId, $scope.startDateModify, $scope.endDateModify, $scope.startDatenew, $scope.endDatenew, $scope.selects, $scope.fileNames, $scope.start_Date, $scope.end_Date).success(function (response) {

                    if ($scope.selects == "Product Attribute Association with catalog Report"
                        || $scope.selects == "Products with multiple categories and catalog Report"
                          || $scope.selects == "Products Modified in Last 7 days Report"
                          || $scope.selects == "Total products Count Report"
                          || $scope.selects == "Unpublished product Report"
                          || $scope.selects == "Product that has Unpublished Columns and Values Report"
                          || $scope.selects == "All Attributes Report"
                          || $scope.selects == "All Products with No price Report"
                          || $scope.selects == "All Products with No Attributes Report"
                          || $scope.selects == "Catalog Attribute Association Report"
                          || $scope.selects == "Duplicate image list"
                          || $scope.selects == "Report new products"
                          || $scope.selects == "Report modified products"
                          || $scope.selects == "DisContinued Products"

                        ) {
                        $scope.datas = response;
                        for (var i = 0 ; response.length > i ; i++) {
                            if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                            }
                            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }
                        }
                    }
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        },
        schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            }
        }
    });
    //=======================================family created in last seven days============================================================//
    $scope.familycreatedoptions =
  {
      dataSource: $scope.ImageTypeReportsoptionsDatasource,
      autoBind: false,
      sortable: true,
      filterable: true,
      pageable: { buttonCount: 5 }, selectable: true,


      columns: [

{ field: "CATEGORY_ID", title: "Category ID", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" }


      ], dataBound: function (e) {
      },

  };




    //======================================product created in last seven days=======================================================//
    $scope.prodmodifiedoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,

        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [

{ field: "CATEGORY_ID", title: "Category ID", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "100px" }

        ], dataBound: function (e) {
        },

    };


    //======================================duplicate item number===================================================================//
    $scope.duplicateitemoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [

{ field: "CATEGORY_ID", title: "Category ID", width: "180px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "100px" },
{ field: "STRING_VALUE", title: "String Value", width: "100px" }
        ], dataBound: function (e) {
        },

    };



    //===============================================supplier list reports============================================================//

    $scope.supplierlistoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 },
        selectable: true,


        columns: [{ field: "CATEGORY_ID", title: "Category ID", width: "100px" },
      { field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
      { field: "FAMILY_ID", title: "Family ID", width: "100px" },
      { field: "FAMILY_NAME", title: "Family Name", width: "100px" },
      { field: "PRODUCT_ID", title: "Product ID", width: "100px" },
      { field: "SUPPLIER_NAME", title: "Supplier Name", width: "100px" }

        ], dataBound: function (e) {
            
        },

    };


 //==============================================all reports========================================================================//
    $scope.allattriboptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [
             { field: "ATTRIBUTE_ID", title: "Attribute ID", width: "130px" },
{ field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "150px" },
 { field: "ATTRIBUTE_TYPE", title: "Attribute Type", width: "150px" },
{ field: "CREATE_BY_DEFAULT", title: "Create By Default", width: "250px" },
{ field: "VALUE_REQUIRED", title: "Value Required", width: "200px" },
{ field: "STYLE_NAME", title: "Style Name", width: "150px" },
 { field: "STYLE_FORMAT", title: "Style Format", width: "150px" },
{ field: "DEFAULT_VALUE", title: "Default Value", width: "200px" },
{ field: "PUBLISH2PRINT", title: "Publish2Print", width: "150px" },
{ field: "PUBLISH2WEB", title: "Publish2Web", width: "150px" },
 { field: "PUBLISH2CDROM", title: "Publish2CDROM", width: "150px" },
{ field: "PUBLISH2ODP", title: "Publish2ODP", width: "150px" },
{ field: "ATTRIBUTE_DATATYPE", title: "Attribute Datatype", width: "250px" }
        ], dataBound: function (e) {
        },

    };
    //=================================option for modified products================================================//
    $scope.modifyoption = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: true,
        sortable: true,
        filterable: true,
        autoBind: true,
        selectable: false,

        pageable: { buttonCount: 5 },


        columns: [

            {
                command: [{ name: "EDIT", text: "", width: "10px", template: "<a class='k-button k-button-icontext ' title = \'EDIT\' ng-click='\RemoveTextList($event,this)\'><span class='k-icon k-edit' title = \'EDIT\'></span>Edit</a>" }], width: "100px", title: "Edit"
            },
{ field: "ITEM_NUMBER", title: $localStorage.CatalogItemNumber, width: "100px", template: "<a  class='k-link namelength' href='javascript:void(0);' ng-click='FamilyHir(this)'>#=ITEM_NUMBER#</a>" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "100px" },
{ field: "MODIFIED_USER", title: "Modified User", width: "100px" },
{
    field: "MODIFIED_DATE", title: "Modified date", format: "{0:MMM d,yyyy}", filterable: { ui: "datetimepicker" }, width: "100px"
}




        ], dataBound: function (e) {
        },

    };
    $scope.RemoveTextList = function (event, e) {
        $scope.datas = e.dataItem;
        clickedData = $scope.datas;
        family_Id = $scope.datas.FAMILY_ID;
        product_Id = $scope.datas.PRODUCT_ID;
        e = $scope.datas;
        $rootScope.rowselectedEvent(product_Id, e, family_Id, clickedData);


    }

    //========================================option for new products==============================================//
    $scope.newprodoption = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: true,
        sortable: true,
        filterable: true,
        selectable: "row",
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [
  {
      command: [{ name: "EDIT", text: "", width: "10px", template: "<a class='k-button k-button-icontext ' title = \'EDIT\' ng-click='\RemoveTextList($event,this)\'><span class='k-icon k-edit' title = \'EDIT\'></span>Edit</a>" }], width: "100px", title: "Edit"
  },
{ field: "ITEM_NUMBER", title: $localStorage.CatalogItemNumber, width: "100px", template: "<a  class='k-link namelength' href='javascript:void(0);' ng-click='FamilyHir(this)'>#=ITEM_NUMBER#</a>" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "100px" },
{ field: "CREATED_DATE", title: "Created Date", format: "{0:MMM d,yyyy}", width: "100px" },
{ field: "CREATED_USER", title: "Created User", width: "100px" }

        ], dataBound: function () {
        },

    };
    $scope.RemoveTextList = function (event, e) {
        $scope.datas = e.dataItem;
        clickedData = $scope.datas;
        family_Id = $scope.datas.FAMILY_ID;
        product_Id = $scope.datas.PRODUCT_ID;
        e = $scope.datas;
        $rootScope.rowselectedEvent(product_Id, e, family_Id, clickedData);


    }
    //============================data source for modified products================================================//
    //$scope.pageno = "1";
    //$scope.ProdCurrentPage = "1";
    //$scope.perpagecount = "10";
    //$scope.modifproducts = function () {

    //    $scope.selects = "Report modified products";
    //    if ($scope.droppageno != undefined) {
    //        $scope.ProdCurrentPage = $scope.droppageno;
    //    }
    //    if ($scope.searchItemValue == undefined || $scope.searchValue == undefined) {
    //        $scope.searchItemValue = '';
    //        $scope.searchValue = '';
    //    }
    //    //dataFactory.GetAllReports($scope.SelectedCatalogId, $scope.startDateModify, $scope.endDateModify, $scope.startDatenew, $scope.endDatenew, $scope.selects, $scope.ProdCurrentPage, $scope.perpagecount).success(function (response) {
    //    dataFactory.GetAllReports($scope.SelectedCatalogId, $scope.startDateModify, $scope.endDateModify, $scope.startDatenew, $scope.endDatenew, $scope.selects, $scope.pageno, $scope.perpagecount, $scope.fileNames, $scope.start_Date, $scope.end_Date, $scope.searchItemValue, $scope.searchValue).success(function (response) {
    //        $scope.modifiedproucts = response;
    //        for (var i = 0 ; response.length > i ; i++) {
    //            if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
    //                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
    //            }
    //            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
    //                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
    //            }
    //        }

    //    }
    //)
    //};

    //=====================================================count function for modified=============================================//

    //$scope.prodcountmodified = function () {

    //    $scope.selects = "Report modified products";
    //    dataFactory.prodcount($scope.SelectedCatalogId, $scope.startDateModify, $scope.endDateModify).success(function (response) {
    //        var obj = jQuery.parseJSON(response);
    //        var ProdEditPageloopcnt = Math.ceil(parseInt(obj) / parseInt($scope.ProdCountPerPage));
    //        var ProdCountmodifiedar = [];
    //        for (var i = 1; i <= ProdEditPageloopcnt; i++) {
    //            ProdCountmodifiedar.push(i);
    //        }
    //        $scope.ProdPageCount = ProdCountmodifiedar;
    //        $scope.totalProdPageCount1 = ProdEditPageloopcnt;
    //        $scope.ProdCurrentPage = 1;
    //        if ($scope.ProdCountPerPage != undefined && $scope.ProdCountPerPage != "" && $scope.currentPageCount === "") {
    //            $scope.ProdCountPerPage = $scope.ProdPerPageCount;

    //        }
    //        $scope.modifproducts();
    //    })

    //};
    //==========================================count function for new products======================================//
    //$scope.prodcountnew = function () {
    //    $scope.selects = "Report new products";
    //    dataFactory.prodcountnew($scope.SelectedCatalogId, $scope.startDatenew, $scope.endDatenew).success(function (response) {
    //        var obj = jQuery.parseJSON(response);
    //        var ProdEditPageloopcnt = Math.ceil(parseInt(obj) / parseInt($scope.ProdCountPerPage));
    //        var ProdCountmodifiedar = [];
    //        for (var i = 1; i <= ProdEditPageloopcnt; i++) {
    //            ProdCountmodifiedar.push(i);
    //        }
    //        $scope.ProdPageCount = ProdCountmodifiedar;
    //        $scope.totalProdPageCount1 = ProdEditPageloopcnt;
    //        $scope.ProdCurrentPage = 1;
    //        if ($scope.ProdCountPerPage != undefined && $scope.ProdCountPerPage != "" && $scope.currentPageCount === "") {
    //            $scope.ProdCountPerPage = $scope.ProdPerPageCount;

    //        }
    //        $scope.newreportproducts();
    //    })

    //};
    //============================================datasource for new products=========================================//


    //$scope.newreportproducts = function () {
    //    $scope.selects = "Report new products";
    //    if ($scope.searchItemValue == undefined || $scope.searchValue == undefined) {
    //        $scope.searchItemValue = '';
    //        $scope.searchValue = '';
    //    }
    //    //dataFactory.GetAllReports($scope.SelectedCatalogId, $scope.startDateModify, $scope.endDateModify, $scope.startDatenew, $scope.endDatenew, $scope.selects, $scope.ProdCurrentPage, $scope.perpagecount).success(function (response) {
    //    dataFactory.GetAllReports($scope.SelectedCatalogId, $scope.startDateModify, $scope.endDateModify, $scope.startDatenew, $scope.endDatenew, $scope.selects, $scope.pageno, $scope.perpagecount, $scope.fileNames, $scope.start_Date, $scope.end_Date, $scope.searchItemValue, $scope.searchValue).success(function (response) {
    //        $scope.newwproucts = response;
    //        product_ID = $scope.newwproucts.PRODUCT_ID;
    //        family_Id = $scope.newwproucts.FAMILY_ID;
    //        for (var i = 0 ; response.length > i ; i++) {
    //            if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
    //                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
    //            }
    //            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
    //                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
    //            }
    //        }

    //    }
    //)
    //};

    //===================pagination for  modify product==========================//

    //$scope.pagingmodified = function (pageno) {
    //    if (pageno != null && pageno <= $scope.totalProdPageCount1 && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {
    //        if (pageno == 'PREV' && $scope.ProdCurrentPage != 1) {
    //            $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) - 1;
    //        } else if (pageno == 'PREV' && $scope.ProdCurrentPage == 1) {
    //            $scope.ProdCurrentPage = "1";
    //        } else if (pageno == 'NEXT') {
    //            $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) + 1;
    //            if ($scope.ProdCurrentPage > $scope.totalProdPageCount1) {
    //                $scope.ProdCurrentPage = $scope.totalProdPageCount1;
    //            }
    //        } else {
    //            $scope.ProdCurrentPage = pageno;
    //        }
    //        $scope.pageno = $scope.ProdCurrentPage;
    //        $scope.modifproducts();


    //    }
    //};



    ////==============pagination for new products=========================//

    //$scope.pagingnew = function (pageno) {
    //    if (pageno != null && pageno <= $scope.totalProdPageCount1 && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {
    //        if (pageno == 'PREV' && $scope.ProdCurrentPage != 1) {
    //            $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) - 1;
    //        } else if (pageno == 'PREV' && $scope.ProdCurrentPage == 1) {
    //            $scope.ProdCurrentPage = "1";
    //        } else if (pageno == 'NEXT') {
    //            $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) + 1;
    //            if ($scope.ProdCurrentPage > $scope.totalProdPageCount1) {
    //                $scope.ProdCurrentPage = $scope.totalProdPageCount1;
    //            }
    //        } else {
    //            $scope.ProdCurrentPage = pageno;
    //        }
    //        $scope.pageno = $scope.ProdCurrentPage;
    //        $scope.newreportproducts();


    //    }
    //};


    //==========================================date picker for modified products=============================================//
    $scope.modifyproducts = function () {
        if ($scope.startDateModify > $scope.endDateModify) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'End date should be greatre than start date!',
                type: "info"
            });
        }
        else {
            // $scope.prodcountmodified();
            if ($scope.startDateModify.indexOf("/") >= 0) {
                $scope.startDateModify = $scope.startDateModify;
            }
            else {
                if ($scope.startDateModify != "") {
                    var splitStartDate = $scope.startDateModify.split(' ');
                    var month = monthNames.indexOf(splitStartDate[0]);
                    month = month + 1;
                    var splitAgainSD = splitStartDate[1].split(',');
                    $scope.startDateModify = month + "/" + splitAgainSD[0] + "/" + splitAgainSD[1];
                }
                else {
                    $.msgBox({

                        content: 'Please choose a date',
                        type: "error"
                    });
                    return false;
                }
            }
            if ($scope.endDateModify.indexOf("/") >= 0) {
                $scope.endDateModify = $scope.endDateModify;
            }
            else {
                if ($scope.endDateModify != "") {
                    var splitEndDate = $scope.endDateModify.split(' ');
                    var monthED = monthNames.indexOf(splitEndDate[0]);
                    monthED = monthED + 1;
                    var splitAgainED = splitEndDate[1].split(',');
                    $scope.endDateModify = monthED + "/" + splitAgainED[0] + "/" + splitAgainED[1];
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please choose a date',
                        type: "error"
                    });
                    return false;
                }
            }
            var startDate = new Date($scope.startDateModify);
            var endDate = new Date($scope.endDateModify);
            //$scope.GetAllDataItemCount();
            if (startDate & endDate) {
                $scope.Index = 8;
                $scope.ImageTypeReportsoptionsDatasource.read();
                //  $scope.modifproducts();

            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose valid date',
                    type: "error"
                });
            }
        }
    }
    $scope.refreshClickButton = function () {
        $scope.startDateModify = monthNameSD + ' ' + day + ',' + yearSD;
        $scope.endDateModify = monthName + ' ' + day + ',' + year;
        $scope.modifiedprods();
    }
    $scope.itemSearch = function (searchValue) {
        $scope.searchValue;
        $scope.modifiedprods();

    };

    //=========================date picker for new products==========================================//
    $scope.newlyproducts = function () {
        //$("#modified").reload();
        if ($scope.startDatenew > $scope.endDatenew) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'End date should be greatre than start date!',
                type: "info"
            });
        }
        else {
            //$scope.prodcountnew();
            if ($scope.startDatenew.indexOf("/") >= 0) {
                $scope.startDatenew = $scope.startDatenew;
            }
            else {
                if ($scope.startDatenew != "") {
                    var splitStartDate = $scope.startDatenew.split(' ');
                    var month = monthNames.indexOf(splitStartDate[0]);
                    month = month + 1;
                    var splitAgainSD = splitStartDate[1].split(',');
                    $scope.startDatenew = month + "/" + splitAgainSD[0] + "/" + splitAgainSD[1];
                }
                else {
                    $.msgBox({

                        content: 'Please choose a date',
                        type: "error"
                    });
                    return false;
                }
            }
            if ($scope.endDatenew.indexOf("/") >= 0) {
                $scope.endDatenew = $scope.endDatenew;
            }
            else {
                if ($scope.endDatenew != "") {
                    var splitEndDate = $scope.endDatenew.split(' ');
                    var monthED = monthNames.indexOf(splitEndDate[0]);
                    monthED = monthED + 1;
                    var splitAgainED = splitEndDate[1].split(',');
                    $scope.endDatenew = monthED + "/" + splitAgainED[0] + "/" + splitAgainED[1];
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please choose a date',
                        type: "error"
                    });
                    return false;
                }
            }
            var startDate = new Date($scope.startDatenew);
            var endDate = new Date($scope.endDatenew);
            if (startDate & endDate) {
                $scope.Index = 8;
                //$scope.newreportproducts();
                $scope.ImageTypeReportsoptionsDatasource.read();

            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose valid date',
                    type: "error"
                });
            }
        }
    }
    $scope.refreshButtonClick = function () {
        $scope.startDatenew = monthName + ' ' + day + ',' + year;
        $scope.endDatenew = monthName + ' ' + day + ',' + year;
        $scope.newprods();
    }
    $scope.itemValueSearch = function (searchItemValue) {
        $scope.searchItemValue;
        $scope.newprods();

    };

    //=========================Duplicateimage_list==========================================//
    $scope.Duplicateimage_list =
        {
            dataSource: $scope.ImageTypeReportsoptionsDatasource,
            autoBind: true,
            sortable: true,
            filterable: true,
            pageable: { buttonCount: 5 }, selectable: true,

            columns: [

                { field: "previewPath", title: "Images", template: "<img style='max-width:70%;max-height:195px;margin:5px' src ='#= previewPath #'/>", width: "100px" },
                { field: "Name", title: "Image Names", width: "130px" },
                 { field: "path", title: "Image Path", width: "250px" },
                 { field: "dimension", title: "Dimension", width: "100px", attributes: { style: "text-align:right;" } },
                 { field: "size", title: "Size", width: "100px", attributes: { style: "text-align:right;" } }
            ], dataBound: function (e) {
            },

        };

    //===========================================DISCONTINUED PRODUCTS  ==========================================================================//

    $scope.DisContinued_Products =
        {
            dataSource: $scope.ImageTypeReportsoptionsDatasource,
            autoBind: false,
            sortable: true,
            pageSize: 20,
            filterable: true,
            pageable: { buttonCount: 5 }, selectable: true,

            columns: [

              { field: "product_id", title: "Product Id", width: "200px" },
              { field: "product_name", title: $localStorage.CatalogItemNumber, width: "200px" },
              { field: "Deleted_User", title: "Deleted User", width: "200px" },
              { field: "Deleted_Date", title: "Deleted date", width: "200px" }

            ], dataBound: function (e) {
            },

        };
    //===========================================DISCONTINUED PRODUCTS DATE PICKER==========================================================================//

    $scope.deletedproduct = function () {
        if ($scope.start_Date > $scope.end_Date) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'End date should be greatre than start date!',
                type: "info"
            });
        }
        else {
            if ($scope.start_Date.indexOf("/") >= 0) {
                $scope.start_Date = $scope.start_Date;
            }
            else {
                if ($scope.start_Date != "") {
                    var splitStartDate = $scope.start_Date.split(' ');
                    var month = monthNames.indexOf(splitStartDate[0]);
                    month = month + 1;
                    var splitAgainSD = splitStartDate[1].split(',');
                    $scope.start_Date = month + "/" + splitAgainSD[0] + "/" + splitAgainSD[1];
                }
                else {
                    $.msgBox({

                        content: 'Please choose a date',
                        type: "error"
                    });
                    return false;
                }
            }
            if ($scope.end_Date.indexOf("/") >= 0) {
                $scope.end_Date = $scope.end_Date;
            }
            else {
                if ($scope.end_Date != "") {
                    var splitEndDate = $scope.end_Date.split(' ');
                    var monthED = monthNames.indexOf(splitEndDate[0]);
                    monthED = monthED + 1;
                    var splitAgainED = splitEndDate[1].split(',');
                    $scope.end_Date = monthED + "/" + splitAgainED[0] + "/" + splitAgainED[1];
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please choose a date',
                        type: "error"
                    });
                    return false;
                }
            }
            var startDate = new Date($scope.start_Date);
            var endDate = new Date($scope.end_Date);
            if (startDate & endDate) {
                $scope.Index = 8;
                $scope.discontinuedProducts();

            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose valid date',
                    type: "error"
                });
            }
        }
    }

    //====================================edit_option_products_navigation===============================================//
    $rootScope.rowselectedEvent = function (product_Id, e, family_Id, clickedData) {
        $('#dynamictable').show();
        $('#hide').show();
        $('#newproducts').addClass("col-sm-5");
        $('#modifiedproductsreport').addClass("col-sm-5");
        $("#dynamictable").addClass("col-sm-6");
        $("#dynamictable").removeClass("col-sm-12");
        $("#table10").addClass("width-48");
        $("#table10").removeClass("col-sm-12");
        $("#table10").addClass("grid");
        $('#selectedattributeproducts').show();
        $("#selectedattributeproducts").css("display", "block");
        $("#table10").show();
        $('#filterAttributePack').show();
        $('#filterbydate').addClass("col-sm-12");
        $('#filterbydate').removeClass("col-sm-7");
        $('#search-attr').addClass("col-sm-7");
        $('#search-attr').removeClass("col-sm-4");

        $('#new-filterbydate').addClass("col-sm-12");
        $('#new-filterbydate').removeClass("col-sm-7");
        $('#new-search-attr').addClass("col-sm-7");
        $('#new-search-attr').removeClass("col-sm-4");


        $rootScope.getProduct_id = product_Id;
        $rootScope.getEventValue = e;
        $rootScope.getFamily_id = family_Id;

        $rootScope.getclickedData = clickedData;
        //$scope.catalogid = $rootScope.getclickedData.CATALOG_ID;
        attributeWithout = [];
        selectedproduct = [];
        // dataFactory.GetInvertedproductAttributeDetails($scope.catalogid, $rootScope.getFamily_id).success(function (response) 
        dataFactory.getNullvalueAttributesForSearch($rootScope.getFamily_id, $rootScope.getProduct_id, $scope.SelectedCatalogId).success(function (response) {
            $scope.prodDataforinvert = response;
            if ($scope.prodDataforinvert.length > 0) {
                for (var i = 0; i < $scope.prodDataforinvert.length ; i++) {
                    if ($scope.prodDataforinvert[i].PRODUCT_ID == product_Id) {
                        data = i;
                    }
                }
            } else {
                data = 0;
            }
            attributeWithout = [];
            $scope.attributesOLD = Object.keys($scope.prodDataforinvert[data]);
        });
        dataFactory.GetAttributeDetails($scope.SelectedCatalogId, $rootScope.getFamily_id).success(function (response) {
            if (response != null) {
                $scope.selectedRowAttributes = response;
                var calcAttributestemp = [];
                angular.forEach($scope.attributesOLD, function (value, key) {
                    if (value.includes("__"))
                        attributeWithout.push(value);
                });
                $scope.attributescalc = attributeWithout;
                $scope.fromPageNo = 0;
                $scope.toPageNo = 150;
                if (attributeWithout.length < 150) {
                    $scope.toPageNo = attributeWithout.length;
                }
                var attributestemp = [];
                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                    attributestemp.push(attributeWithout[i]);
                }


                $scope.attributes = attributestemp;

                // To bind top10 vaules in the Grid 
                $scope.attributesValuesForEdit = [];
                $scope.ProdEditPageCount = 10;

                for (var i = 0 ; i < $scope.ProdEditPageCount ; i++) {
                    if ($scope.attributes[i] != undefined)
                        $scope.attributesValuesForEdit.push($scope.attributes[i])
                }
                $scope.attributes = $scope.attributesValuesForEdit;

                // End


                $scope.AllAttributeValues = attributestemp;
                $scope.attrLength = $scope.attributes.length;
                $scope.ProdSpecsCountPerPage = "150";
                $scope.ProdSpecsCurrentPage = "1";
                $scope.loopCount = [];
                $scope.loopEditProdsCount = [];
                var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                if ((attributeWithout.length % parseInt($scope.ProdSpecsCountPerPage)) > 0) {
                    loopcnt = loopcnt + 1;
                    loopEditProdsCnt = loopEditProdsCnt + 1;
                }
                $scope.ProdSpecsPageCount = $scope.loopCount;
                $scope.totalSpecsProdPageCount = loopEditProdsCnt;
                for (var i = 0; i < loopcnt; i++) {
                    if (((i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)) < attributeWithout.length) {
                        $scope.loopCount.push({
                            PAGE_NO: (i + 1),
                            FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                            TO_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)
                        });
                    } else {

                        $scope.loopCount.push({
                            PAGE_NO: (i + 1),
                            FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                            TO_PAGE_NO: attributeWithout.length
                        });
                    }
                }
                var theString;
                $scope.selectedRowEdit = {};
                angular.copy($scope.prodDataforinvert[data], $scope.selectedRowEdit);
                $rootScope.selectedProductAttributeRow = $scope.selectedRowEdit;

                //////////////getting base undo product value ///////////////////////////////////////////////////////////////////
                if ($localStorage.baseUndoOnlyProductDetails.length == 0) {
                    $scope.copyselectedRowEdit = angular.copy($scope.selectedRowEdit);
                    $localStorage.baseUndoOnlyProductDetails.push($scope.copyselectedRowEdit);
                }
                else {
                    if ($localStorage.baseUndoOnlyProductDetails.PRODUCT_ID != $scope.selectedRowEdit.PRODUCT_ID) {
                        $scope.copyselectedRowEdit = angular.copy($scope.selectedRowEdit);
                        $localStorage.baseUndoOnlyProductDetails.push($scope.copyselectedRowEdit);
                    }
                }

                //////////////getting base undo product value ///////////////////////////////////////////////////////////////////s

                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                    if (attributeWithout[i].contains('__')) {
                        var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                        if (tempattrvals.length != 0) {
                            var uimask = "";
                            theString = tempattrvals[0].ATTRIBUTE_ID;
                            tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                            if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                theString = 0;
                            }
                            var itemval = attributeWithout[i];
                            if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                                $scope.item_ = $scope.selectedRowEdit[itemval];
                            }
                            if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                var reg = new RegExp(pattern);
                               // uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                tempattrvals[0].attributePattern = reg;
                            } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                uimask = tempattrvals[0].DECIMAL;
                            } else {
                                tempattrvals[0].attributePattern = 524288;
                                uimask = 524288;
                            }
                            tempattrvals[0].uimask = uimask;
                            $scope.selectedRowDynamicAttributes[theString] = [];
                            $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                        }
                    }
                }
                $scope.updateproductspecs = $scope.prodDataforinvert[data]
                $("#result").hide();
                $scope.selectedRow = {};
                angular.copy($scope.updateproductspecs, $scope.selectedRow);
                angular.copy($scope.updateproductspecs, $scope.selectedRow);

            }
        }).error(function (error) {
            options.error(error);
        });

        $scope.selectedRowValue = product_Id;
        $rootScope.selectedRowValue = product_Id;


    };
    $scope.LoadMoreData = function () {
        if ($scope.ProdEditPageCount != 0) {

            $scope.attributesValuesForEdit = [];
            $scope.ProdEditPageCount = ($scope.ProdEditPageCount + 10);

            for (var i = 0 ; i < $scope.ProdEditPageCount ; i++) {
                if ($scope.AllAttributeValues[i] != undefined) {
                    $scope.attributesValuesForEdit.push($scope.AllAttributeValues[i])
                }
            }
            $scope.attributes = $scope.attributesValuesForEdit;
        }
    }
    $scope.ResetEditClick = function () {
        $scope.searchText = '';
        $scope.callAttributeNameSearch();
    }
    $scope.callAttributeNameSearch = function () {
        $scope.fromPageNo = 1;
        $scope.toPageNo = 1;
        $scope.currentPage = -1;
        var attributeWithout = [];
        var attributesearch = [];

        angular.forEach($scope.attributesOLD, function (value, key) {
            if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2'))
                attributeWithout.push(value);

        });
        angular.forEach(attributeWithout, function (value, key) {
            if (value.toLowerCase().indexOf($scope.searchText.toLowerCase()) > -1 || value.toUpperCase() == "PUBLISH2WEB" || value.toUpperCase() == "PUBLISH2PRINT" || value.toUpperCase() == "PUBLISH2PDF" || value.toUpperCase() == "PUBLISH2EXPORT" || value.toUpperCase() == "PUBLISH2PORTAL") {
                attributesearch.push(value);
            }
        });
        attributeWithout = attributesearch;
        if (attributeWithout == 0) {
            $("#result").show();
        }
        else {
            $("#result").hide();
        }
        $scope.toPageNo = attributeWithout.length;
        var attributestemp = [];
        for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
            attributestemp.push(attributeWithout[i]);
        }
        $scope.attributes = attributestemp;
        if ($scope.attributes.length === 5) {
            $('#selectedattributenoresultfound').show();
        }
        else {
            $('#selectedattributenoresultfound').hide();
        }
        for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
            var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
            if (tempattrvals.length > 0) {
                var theString = tempattrvals[0].ATTRIBUTE_ID;
                var uimask = "";
                tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                    theString = 0;
                }
                if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                    var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                    pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                    pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                    var reg = new RegExp(pattern);
                    uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                    tempattrvals[0].attributePattern = reg;
                } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text')) {
                    tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                    uimask = tempattrvals[0].DECIMAL;
                } else {
                    tempattrvals[0].attributePattern = 524288;
                    uimask = 524288;
                }
                tempattrvals[0].uimask = uimask;
                $scope.selectedRowDynamicAttributes[theString] = [];
                $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
            }
        }
        for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
            if (attributeWithout[i].contains("OBJ")) {
                if (attributeWithout[i].split('__')[2] !== "0") {
                    var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                    if (sa.USE_PICKLIST) {
                        var theString = sa.ATTRIBUTE_ID;
                        $scope.GetPickListData(theString, sa.PICKLIST_NAME);
                    }
                }
            }
        }
        $scope.searchText = '';
    };
    $rootScope.SaveOrUpdateProductforinvertItem = function (row) {
        $scope.productGridSearchValue = "";
        $scope.oldAttributeValues = $scope.updateproductspecs;
        var displayAttribures = "";
        var tt = $('#textboxvalue4');
        if ($scope.val_type === "true") {
            angular.forEach(row, function (value, key) {
                if (key.contains("OBJ")) {
                    var sa = [key.split('__')[2]];
                    if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0) {
                        var id = sa;
                        if ($("#textBoxValue" + id)[0].nextElementSibling != null) {
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != null && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != undefined && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {
                                if (!$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Value is too long") && !value.includes("Value is too long")) {
                                    row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
                                }
                                else {
                                    row[key] = value;
                                }

                                if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != null && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                    row[key] = "";
                                }
                            }
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != null && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                row[key] = "";
                            }
                        }
                    }
                    else if ($("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling != null && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {
                        row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
                    }
                    if (sa != undefined) {
                        //To check if the attribute is a value required field
                        if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {

                            if (displayAttribures == '') {
                                displayAttribures = "The following attribute values are required,\n";
                            }
                            displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                        }
                    }

                }

            });
            $localStorage.CategoryID = row.CATEGORY_ID;
            $scope.ParentProductId = '0';
            $scope.blurCheck = false;
            if (displayAttribures == '') {
                dataFactory.SaveProductSpecs(row, $localStorage.CategoryID, $scope.ParentProductId, $scope.blurCheck, $scope.oldAttributeValues).success(function (response) {
                    if (response != null) {
                        //$rootScope.tblInvertedGrid.reload();

                        $scope.blurCheck = false;
                        if ($scope.ParentProductId !== 0) {
                        }
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"

                        });


                    }


                }).error(function (error) {
                    options.error(error);
                });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + displayAttribures + '.',
                    type: "info"
                });

            }
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter Data using a valid format.',
                type: "error"
            });
        }

        // --------------------------------Undo family and Product Tab------------------------------------------------------//
        $scope.productSpec = $rootScope.selectedProductAttributeRow;
        $rootScope.ProductDetailsValue = $rootScope.selectedProductAttributeRow;
        $rootScope.baseProductRowAttributes = $scope.selectedRowAttributes;
        $scope.$emit("SendUndoProductGridDetails", $rootScope.ProductDetailsValue);
        // --------------------------------Undo family and Product Tab------------------------------------------------------//
        // -----------------------------Only Undo Product Tab---------------------------------------------------------//
        if ($rootScope.undoProductFlag == "0") {
            $scope.productSpec = angular.copy(row);
            Productdetails.push($scope.productSpec);
            $scope.detailslength = Productdetails.length;
            if (Productdetails.length > 10) {
                Productdetails.splice(0, 1);
            }
            $rootScope.UndoProductDetailsValueOnly = {};
            $rootScope.UndoProductDetailsValueOnly = $scope.productSpec;
            $localStorage.ProductDetailsValueOnly = Productdetails;
            $scope.$emit("SendOnlyUndoProductGridDetails", $rootScope.UndoProductDetailsValueOnly);
        }
        else {
            $rootScope.undoProductFlag = "0";
        }

        // -----------------------------Only Undo Product Tab---------------------------------------------------------//
        $('.block-ui-overlay').show();
        $('.imageloader').show();

    };

    $scope.CancelProductItem = function () {
        $("#table10").hide();
        $('#newproducts').removeClass("col-sm-5");
        $('#modifiedproductsreport').removeClass("col-sm-5");
        $('#filterbydate').removeClass("col-sm-12");
        $('#filterbydate').addClass("col-sm-7");
        $('#search-attr').removeClass("col-sm-7");
        $('#search-attr').addClass("col-sm-4");

        $('#new-filterbydate').removeClass("col-sm-12");
        $('#new-filterbydate').addClass("col-sm-7");
        $('#new-search-attr').removeClass("col-sm-7");
        $('#new-search-attr').addClass("col-sm-4");
    }
    $scope.OpenPopupWindow = function (id) {
        //
        //  $window.open("../Views/App/Partials/ImageManagementPopup.html", "popup", "width=300,height=200,left=10,top=150");
        //$scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
        //$scope.winManageDrive.center().open();
        // $localStorage.win = $scope.winManageDrive;
        $scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
        $scope.winManageDrive.title("Asset Management");
        $scope.winManageDrive.center().open();
        $scope.driveMahementIsVisible = true;
        $rootScope.winsave = $scope.winManageDrive;
        if (id.Name == "" || id.Name == undefined) {
            $rootScope.paramValue = id;
            $scope.attribute = id;
        }
        else {

            $rootScope.paramValue = id.Name;
            $scope.attribute = id.Value;
        }
    };

    $scope.selectedRowSub = {};
    $rootScope.SaveNewProdFileSelection = function (fileName, path) {
        //
        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            document.getElementById($scope.attribute).value = path;
            $scope.selectedRow[$scope.attribute] = path;
            //$scope.attribute = path;
            $rootScope.productAttachPath = path;
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;
    };
    $rootScope.SaveNewSubFileSelection = function (fileName, path) {
        //
        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            document.getElementById($scope.attribute).value = path;
            $scope.selectedRow[$scope.attribute] = path;
            //$scope.attribute = path;
            $rootScope.productAttachPath = path;
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;

    };
    $rootScope.SaveProductFileSelection = function (fileName, path) {


        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            angular.forEach($scope.selectedRow, function (value, key) {
                if (key == $rootScope.paramValue) {
                    $scope.selectedRow[key] = path;
                    // $scope.$apply();
                }
            });
            angular.forEach($scope.selectedRowSub, function (value, key) {
                if (key == $rootScope.paramValue) {
                    $scope.selectedRowSub[key] = path;
                    // $scope.$apply();
                }
            });
            $rootScope.productAttachPath = path;
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;

    };

    $scope.clearImagePath = function (e) {
        //
        document.getElementById(e).value = "";
        $scope.selectedRowSub[e] = "";
        $scope.selectedRow[e] = "";
    };
    //========================================================Tool tip hierarchy function====================================================//
    $scope.FamilyHir = function (s) {
        $scope.resources = s.dataItem;
        $scope.searchtxt = $scope.resources.ITEM_NUMBER;
        $scope.CategoryID = $scope.resources.CATEGORY_ID;
        $scope.selectedFilters = 0;
        $scope.catalogIdAttr = $rootScope.selecetedCatalogId;
        dataFactory.SearchResults($scope.searchtxt, $scope.selectedFilters, $scope.catalogIdAttr, $scope.CategoryID).success(function (response) {
            var dataitem = response[0];
            content = dataitem.CATALOG_NAME + "->";
            if (dataitem.SUBCATNAME_L1 != '_' && dataitem.SUBCATNAME_L1 != null && dataitem.SUBCATNAME_L1 != '-' && dataitem.SUBCATNAME_L1 != '') {
                content = content + dataitem.SUBCATNAME_L1 + "->";
            }
            content = content + dataitem.CATEGORY_NAME + '->' + dataitem.FAMILY_NAME + '->' + dataitem.STRING_VALUE;
            $scope.hierarchy = content;
            //return content;


        });
    }
  

    //--------------------------------------------------------------------------------PRITHVIN------------------------------------------------------------------------------------//    
}]);