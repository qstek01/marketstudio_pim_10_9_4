﻿
LSApp.controller('SummaryController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http,$rootScope, $localStorage) {

    
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    var selectedItem = "";


  //  $scope.UserId = 0;

    //$("#grid").on("mousedown", "tr[role='row']", function (e) {
    //    if (e.which === 3) {
    //        $("#custom-menu1").css({ top: (e.pageY) + "px", left: (e.pageX) + "px" }).show();
    //    }
    //});


    $scope.click = function (e)
    {
        $("#custom-menu1").css({ top: (e.pageY) + "px", left: (e.pageX) + "px" }).show();
    };


    $scope.leftNavCollapse = function ()
    {
        var grid = $('#grid').data('kendoGrid');
        var allMasterRows = grid.tbody.find('>tr.k-master-row');
       
        for (var i = 0; i < allMasterRows.length; i++)
        {     
                grid.collapseRow(allMasterRows.eq(i));   
        }
    };


    $scope.leftNavExpand = function () {
        var grid = $('#grid').data('kendoGrid');
        var allMasterRows = grid.tbody.find('>tr.k-master-row');
       
        for (var i = 0; i < allMasterRows.length; i++) {
     
                grid.expandRow(allMasterRows.eq(i));      
        }
    };
    $scope.HideMenu1 = function (e, dataItemId) {
        $("#custom-menu1").hide();
    };
    
    $scope.summaryGridDatasource = new kendo.data.DataSource({
        type: "json",
        batch: true,
        pageSize: 10,
        serverFiltering: true,
        transport: {
            read: function (options) {
                
                dataFactory.GetCatalogDetailsForCurrentCustomer("",  $localStorage.getCustomerID).success(function (response) {
                    
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });

            }
           
        },
        schema: {
        model: {
            id: "CATALOG_ID"}    }
        ,
        error: function (e) {        
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + e.xhr.responseText+'.',
                //type: "inerrofo"
            });

        }
    });

   
    $("#grid").kendoGrid({
        dataSource: $scope.summaryGridDatasource,
        pageable: { buttonCount: 5 },
        columns: [
          { field: "CATALOG_NAME", title: "Catalog Name", width: "200px" }
        ],
        detailInit: detailInit12,
        //dataBound: function () {
        //    this.expandRow(this.tbody.find("tr.k-master-row").first());
        //},
        schema: {
            model: {
                id: "CATALOG_ID"
            }
        }
        ,
        editable: {
            mode: "inline",
            update: true,
        }
    });


    function detailInit12(e) {
        
        var catalog_id = e.data.CATALOG_ID;
        $("<div/>").appendTo(e.detailCell).kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: function (options) {
                        
                        dataFactory.GetCatalogCountsForCatalogID(catalog_id,true).success(function (response) {
                            
                            if ($localStorage.EnableSubProductLocal == false)
                            {
                                for(var i = 0; i < response.length ; i++)
                                {
                                    if(response[i].CATALOG_NAME.includes("SubProducts"))
                                    {
                                        response.splice(i, 1);
                                    }
                                }
                            }

                                options.success(response);
                        }).error(function (response) {
                            options.success(response);
                        });
                    }
                },
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 10
               
            },
            scrollable: false,
            sortable: true,
            pageable: { buttonCount: 5 },
            columns: [
                { field: "CATALOG_NAME", width: "110px" }
            ]
        });
    }
     
   
}]);
