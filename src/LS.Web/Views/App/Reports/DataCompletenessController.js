﻿LSApp.controller('DataCompletenessController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$rootScope', '$localStorage',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $rootScope, $localStorage, blockUI) {


        $('#myDIV01').show();
        $(".k-grid k-widget").show();
        $("#nullvalueAttrgrid").show();
        $('#nullvalueattribute').show();
        $('#nullvalueattributetbl').show();
        $("#nullValueAttrEdit").hide();
        $("#dataReset").hide();



        $scope.userRoleAddCreateManage01 = false;
        $rootScope.SelectedNodeID = '0';
        $scope.SelectedItem = 0;


        $rootScope.treeData.read();
        $rootScope.clickSelectedItem01 = function (dataItem, event) {

            $scope.SelectedItem.Family_ID = "";
            $('#panel').hide();
            $("#selectedattributeEdit").hide();
            $scope.$broadcast("SendDown", "some data");
            $scope.rootCategotyForPdfXpress = true;
            $scope.$broadcast("ForResultEmpty");

            $('#subproductsmaingrid').hide();
            $('#subproductsgrid').hide();
            $('#columnsetup').hide();
            $('#dynamictable').hide();
            $('#Familyimport').hide();
            $('#categoryAttributes').show();
            $rootScope.getCategoryPdfExpress = '';
            $rootScope.getCategoryPdfExpressToolTip = 'No file chosen';
            $rootScope.getFamilyPdfExpress = '';
            $rootScope.getProductPdfExpress = '';

            if (dataItem.spriteCssClass == "category") {
                var TYPE = "Category";

                // To check the root category or not
                var Catalog_Id = dataItem.CATALOG_ID;
                var Category_ID = dataItem.CATEGORY_ID

                dataFactory.getRootCategoryPdfXpress(Catalog_Id, Category_ID).success(function (response) {

                    if (response == '0') {
                        $scope.rootCategotyForPdfXpress = true;
                    } else {
                        $scope.rootCategotyForPdfXpress = false;
                    }

                });

                // End


                $scope.GetDefaultPdfTemplate(dataItem, TYPE);

                //------------------------------------------------------Undo flags--------------------------------------------------------//
                $rootScope.categoryUndo = true;
                $rootScope.familyUndo = false;
                $rootScope.productUndo = false;
                //------------------------------------------------------Undo flags--------------------------------------------------------//
            }
            else if (dataItem.spriteCssClass == "family") {
                var TYPE = "Family";
                $("#filterAttributePack").show();
                $scope.GetDefaultPdfTemplate(dataItem, TYPE);
                $scope.getseelctedFamilyName = dataItem.CATEGORY_NAME.split('(');
                $rootScope.getClickedFamilyName = $scope.getseelctedFamilyName[0].trim();
                //------------------------------------------------------Undo flags--------------------------------------------------------//
                $rootScope.categoryUndo = false;
                $rootScope.familyUndo = true;
                $rootScope.productUndo = false;
                //------------------------------------------------------Undo flags--------------------------------------------------------//


                //------------------------------------------------------Undo product flags--------------------------------------------------------//
                $rootScope.categoryUndo = false;
                $rootScope.familyUndo = false;
                $rootScope.productUndo = true;
                //------------------------------------------------------Undo product flags--------------------------------------------------------//
            }


            $("#opendesignerFAM").hide();
            $('#opendesigner').hide();

            $rootScope.familyCreated = true;
            $('#user-toolbar-trash-family').removeAttr('disabled');


            //changes
            $rootScope.navigateprodProductTab = true;


            if ($rootScope.selectedProductDetails.length == 0) {
                $rootScope.selectedProductDetails = $localStorage.selectedListOnlyProductDetails;
            }

            if ($localStorage.selectedListOnlyProductDetails.length > 0) {
                $scope.getClickedItemFamilyItem = dataItem.CATEGORY_ID.split('~');
                $rootScope.showSelectedFamilyProductDetails = [];
                for (var j = 0; j < $localStorage.selectedListOnlyProductDetails.length; j++) {
                    if ($localStorage.selectedListOnlyProductDetails[j].FAMILY_ID == $scope.getClickedItemFamilyItem[1]) {
                        $rootScope.showSelectedFamilyProductDetails.push($localStorage.selectedListOnlyProductDetails[j]);
                    }
                }
            }

            var countList = 0;
            $rootScope.Productnew = false;
            $rootScope.selectedCloneDetails = dataItem.id;
            if (dataItem.spriteCssClass == "family") {

                $rootScope.currentFamilyId = dataItem.CATEGORY_ID.replace("~", "");
            } else if (dataItem.spriteCssClass == "subfamily") {
                $rootScope.currentFamilyId = dataItem.CATEGORY_ID.replace("~", "");
            } else if (dataItem.spriteCssClass == "defaultfamily") {
                $rootScope.currentFamilyId = dataItem.CATEGORY_ID.replace("~", "");
            }


            if ($rootScope.SelectedNodeID != 0) {
                var listSelected = $rootScope.SelectedNodeID.split('~');
                var listSelecteddata = dataItem.id.split('~');

                $rootScope.CloneCategory_id = listSelecteddata[0];
                angular.forEach(listSelected, function (value) {
                    angular.forEach(listSelecteddata, function (value1) {
                        if (value == value1) {
                            countList = countList + 1;
                        }
                    });
                });

                if (countList == 0) {
                    $rootScope.SelectedNodeID = dataItem.id;
                }
            }
            else {
                $rootScope.SelectedNodeID = dataItem.id;
            }
            if (dataItem.spriteCssClass == "family" && $rootScope.pasteProductNav != true) {
                $rootScope.SelectedNodeID = dataItem.id;
            }
            if ($rootScope.pasteProductNav == true) {

                if ($rootScope.SelectedNodeID.includes('~')) {
                    var splitValue = $rootScope.SelectedNodeID.split('~');
                    if ($rootScope.SelectedNodeID.split('~')[1] != "" && $rootScope.SelectedNodeID.split('~')[1] != "!" && !$rootScope.SelectedNodeID.split('~')[1].includes('CAT')) {
                        $rootScope.SelectedNodeID = $rootScope.SelectedNodeID.replace("~" + $rootScope.SelectedNodeID.split('~')[1], "");
                    }
                    $rootScope.SelectedNodeID = $rootScope.SelectedNodeID + "~" + dataItem.id;
                }
            }

            $rootScope.Dashboards = false;
            if (event === undefined) {
                $scope.getCurrentUrl = $location.$$absUrl.split("App")[1];
                if ($scope.getCurrentUrl == '/AdminDashboard') {
                    $rootScope.familyMainEditor = true;
                    $rootScope.invertedproductsshow = false;
                    $rootScope.invertedproductsbutton = false;
                    $rootScope.invertedproductsbutton1 = false;
                } else if ($scope.getCurrentUrl == '/Inverted') {
                    $rootScope.familyMainEditor = false;
                    $rootScope.invertedproductsshow = true;
                    $rootScope.invertedproductsbutton = false;
                    $rootScope.invertedproductsbutton1 = false;
                }
            } else {
                $rootScope.familyMainEditor = true;
                $rootScope.invertedproductsshow = false;
                $rootScope.invertedproductsbutton = false;
                $rootScope.invertedproductsbutton1 = false;
            }
            if (dataItem.id.match("~")) {
                $rootScope.familycategory = dataItem.id.split("~")[0];
                $rootScope.WORKINGCATALOGID = dataItem.CATALOG_ID;
                $scope.$broadcast("oldCatids");
            } else {
                $rootScope.familycategory = dataItem.id;
            }
            $scope.ProdCurrentPage = "1";


            if (event !== undefined && event !== null) {
                $scope.Coordinates.pageX = event.pageX;
                $scope.Coordinates.pageY = event.pageY;
            }
            $("#custom-menu1").hide();
            $("#custom-menuFam").hide();
            $scope.SelectedDataItem.VERSION = dataItem.VERSION;
            $scope.SelectedDataItem.DESCRIPTION = dataItem.DESCRIPTION;
            $scope.SelectedDataItem.CATALOG_NAME = dataItem.CATALOG_NAME;
            $rootScope.selecetedCatalogId = dataItem.CATALOG_ID;
            $scope.SelectedDataItem.CATALOG_ID = dataItem.CATALOG_ID;
            $scope.SelectedDataItem.CATEGORY_ID = dataItem.CATEGORY_ID;
            $scope.SelectedDataItem.ID = dataItem.CATEGORY_ID;
            $scope.SelectedDataItem.SORT_ORDER = dataItem.SORT_ORDER;
            $scope.SelectedItem = dataItem.CATEGORY_ID;

            $localStorage.CategoryID = dataItem.id;
            $("#dynamictable").show();
            $("#productpreview").hide();
            $("#subproductpreview").hide();

            $("#categoryGridView").show();
            if ($scope.SelectedItem.match("~")) {
                $rootScope.Family_ID_NEW = $scope.SelectedItem;
                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);


                console.log('0');
                $scope.SelectedCategoryId = $scope.SelectedItem;
                if (dataItem.parentNode().CATEGORY_ID.match("~")) {
                    console.log('1');
                    $scope.$broadcast("Active_Category_Id", dataItem.parentNode().parentNode().CATEGORY_ID);
                    $rootScope.selecetedCategoryId = dataItem.parentNode().parentNode().CATEGORY_ID;
                } else {
                    console.log('2');
                    $scope.$broadcast("Active_Category_Id", dataItem.parentNode().CATEGORY_ID);
                    $rootScope.selecetedCategoryId = dataItem.parentNode().CATEGORY_ID;
                }
            } else {
                console.log('3');
                $rootScope.selecetedCategoryId = $scope.SelectedItem;
                $scope.SelectedCategoryId = $scope.SelectedItem;
                $scope.$broadcast("Active_Category_Id", $scope.SelectedCategoryId);

                $rootScope.FamilyCATEGORY_ID = $scope.SelectedCategoryId;
                if ($rootScope.defaultSelection == true) {
                    console.log('33');
                    $rootScope.SelectedNodeID = "";
                }

            }

            console.log('4');
            if ($scope.SelectedCategoryId.match("~")) {
                console.log('5');

                var tabstrip = $("#tabstrip").data("kendoTabStrip");
                var myTab = tabstrip.tabGroup.children("li").eq(1);
                tabstrip.enable(myTab);


                var myTabscategory = tabstrip.tabGroup.children("li").eq(0);
                tabstrip.enable(myTabscategory);

                var myTabsproducts1 = tabstrip.tabGroup.children("li").eq(2);
                tabstrip.enable(myTabsproducts1);
                tabstrip.select(myTabsproducts1);

                $rootScope.enableTabNavigation = true;

                var tabstripProd = $("#productgridtabstrip").data("kendoTabStrip");
                var myTabProd = tabstripProd.tabGroup.children("li").eq(0);
                tabstripProd.enable(myTabProd);
                tabstripProd.select(myTabProd);

                $("#custom-menu1").hide();
                $("#custom-menuFam").hide();
                $(".slideout-menu").removeClass("open");
                $(".slideout-menu").css('left', "-280px");
                $rootScope.selecetedFamilyId = $scope.SelectedCategoryId;
                console.log('6');

                $scope.$broadcast("Active_Family_Id", $scope.SelectedCategoryId, dataItem.parentNode());
                console.log('7');
                $scope.$broadcast("Active_Category_Selection", $scope.SelectedDataItem);
                console.log('8');


            } else {

                console.log('9');

                var tabstrip = $("#tabstrip").data("kendoTabStrip");

                var CategoryTab = tabstrip.tabGroup.children("li").eq(0);
                tabstrip.select(CategoryTab);
                tabstrip.enable(CategoryTab, true);

                var FamilyTab = tabstrip.tabGroup.children("li").eq(1);
                tabstrip.enable(FamilyTab, false);

                var ProductTab = tabstrip.tabGroup.children("li").eq(2);
                tabstrip.enable(ProductTab, false);

                $rootScope.enableTabNavigation = false;

            }
            console.log('10');
            $('#user-toolbar-preview').removeAttr('disabled');
            if ($scope.userRoleDeleteFamily) {
                console.log('11');
                // $('#user-toolbar-association-removal-family').removeAttr('disabled');
                $('#user-toolbar-delete-family').removeAttr('disabled');
            }
            else {
                console.log('12');
                // $('#user-toolbar-association-removal-family').attr('disabled', 'disabled');
                $('#user-toolbar-delete-family').attr('disabled', 'disabled');
            }

        };


        // tree expand   
        $rootScope.expandNode = 0;
        $scope.expandData01 = function (e) {

            $rootScope.expandNode = 0;
            if ($rootScope.expandNode == 0) {
                var selectedNode = e.node.getElementsByTagName('a')[0].id;
                if ($rootScope.SelectedNodeID == "") {
                    $rootScope.SelectedNodeID = selectedNode;
                }
                else {
                    $rootScope.SelectedNodeID = $rootScope.SelectedNodeID + "~" + selectedNode;
                }
                $rootScope.defaultSelection = false;
            }

        }

        //tree close
        $scope.collapseData01 = function (e) {

            if ($rootScope.SelectedNodeIDPresist == 0) {
                var selectedNode = e.node.getElementsByTagName('a')[0].id;
                $rootScope.SelectedNodeID = $rootScope.SelectedNodeID.replace(selectedNode, '');
            }

        }
        $scope.treeOptionsCatalog01 = {
            loadOnDemand: true,
        };

        $scope.catalogDataSource01 = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCurretUserInfo().success(function (response) {
                        $rootScope.currentUser = response;
                        $localStorage.getCustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
                        $localStorage.getUserName = $rootScope.currentUser.Username;

                        $localStorage.getCustomerCompanyName = $rootScope.currentUser.CustomerDetails.Comments;

                        dataFactory.GetCatalogDetailsForCurrentCustomer($scope.SelectedItem, $rootScope.currentUser.CustomerDetails.CustomerId).success(function (cResponse) {
                            options.success(cResponse);
                            var catalogExists = false;
                            angular.forEach(cResponse, function (data) {
                                if (data.CATALOG_ID == $localStorage.getCatalogID)
                                    catalogExists = true;
                            });
                            if (catalogExists == false) {
                                $localStorage.getCatalogID = undefined;
                            }
                            if ($localStorage.getCatalogID === undefined) {
                                $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                                $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                                $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                                $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                                $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                            }
                            if ($scope.SelectedCatalogId == '') {
                                $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                                $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                                $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                                $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                                $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                            }
                            var catalogchk = 0;
                            angular.forEach(cResponse, function (value) {
                                if (value.CATALOG_ID.toString() === $rootScope.selecetedCatalogId.toString()) {
                                    catalogchk = 1;
                                }

                            });
                            if (catalogchk === 0) {
                                $rootScope.selecetedCatalogId = 1;
                                $localStorage.getCatalogName = "Master Catalog";
                                $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
                                $scope.SelectedCatalogId = 1;
                                $scope.catalogDataSource.read();
                                $rootScope.treeData._data = [];
                                i = 0;
                                $rootScope.treeData.read();
                            } else {
                                //alert("2");
                                $rootScope.treeData.read();
                            }
                            blockUI.stop();
                        }).error(function (error) {
                            options.error(error);
                        });
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        //dropdown catalog selection
        $scope.selectCatalog01 = function (e) {

            $scope.oldIndex = e.sender._oldIndex;
            $scope.allowChangeFlag = "0";
            if ($rootScope.selectedFamilyDetails == undefined) {
                $rootScope.selectedFamilyDetails = [];
                $rootScope.categoryUndoList = [];
            }

            if ($rootScope.categoryUndoList.length > 0 || $rootScope.selectedFamilyDetails.length > 0 || $rootScope.showSelectedFamilyProductDetails.length > 0) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Do you want to clear the Undo changes done by you?",
                    type: "info",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {

                        if (result === "Yes") {
                            $scope.oldIndex = e.sender._oldIndex;
                            $scope.allowChangeFlag = "1";
                            $scope.catalogChange01(e);
                        }

                        else {
                            var dropdownlist = $("#ddlKendocatalogDropDown01").data("kendoDropDownList");
                            dropdownlist.select($scope.oldIndex);
                            $scope.allowChangeFlag = "1";
                        }
                    }
                });
            }

            else {

                $scope.allowChangeFlag = "1";
            }
        }

        $scope.catalogChange01 = function (e) {


            if (e.sender.value() != "") {
                $scope.$broadcast("clearids");
                $rootScope.getSelectedNode = "";
                $rootScope.selecetedCatalogId = e.sender.value();
                $scope.selecetedCatalogId = e.sender.value();
                $localStorage.getCatalogName = e.sender.text();
                $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
                $rootScope.treeData._data = [];
                i = 0;
                $rootScope.treeData.read();

            } else {
                $rootScope.treeData = [];
                $rootScope.selecetedCatalogId = 0;
            }
            $scope.GetUserRoleRightsAll();

            location.reload();
        };


        $scope.pageno = "1";
        $scope.ProdCurrentPage = "1";
        $scope.ProdCountPerPage = "10";

        $scope.NullvalueAttributes = function (dataItem) {

            var categoryId = dataItem.id.split('~');
            var getCategoryId = categoryId[0];
            $scope.getcategory = getCategoryId;

            var getFamily_id = dataItem.CATEGORY_ID.split('~');
            var Family_id = getFamily_id[1];
            $scope.selectedFamilyId = Family_id;

            if ($scope.selectedFamilyId != null) {
                $scope.noValueAttribute1 = [];
                $scope.noValueAttribute = [];
                $scope.attributeColumnNames = [];
                $scope.attributeColumnValues = [];
                $scope.attributeEmptyColumn = [];

                $scope.nullValues = [];
                $scope.attributeValues = [];
                $scope.count = "";
                $scope.array = [];
                var k = 0;

                dataFactory.getNullvalueAttributes($scope.selectedFamilyId, $scope.pageno, $scope.ProdCountPerPage).success(function (response) {
                    if (response != null) {
                        $scope.attributeValues = response;
                        //  $scope.nullvalueattribute =response;
                        //$scope.count = response.Data.Columns.length;
                        //if ($scope.count != 0) {
                        //    $scope.noValueAttribute1 = jQuery.parseJSON(response.Data.Data);
                        //    $scope.Invertedcolumns = response.Data.Columns;
                        //    $scope.attributeEmptyColumn=response.Data.Data;


                        //    for (var i = 0 ; $scope.Invertedcolumns.length > i ; i++) {
                        //        if ($scope.Invertedcolumns[i].Caption.includes("__OBJ")) {
                        //            $scope.noValueAttribute.push({ "Value": i, "Name": $scope.Invertedcolumns[i].Caption.slice(0, $scope.Invertedcolumns[i].Caption.search("__OBJ")) });
                        //        }
                        //        else {
                        //            $scope.noValueAttribute.push({ "Value": i, "Name": $scope.Invertedcolumns[i].Caption });
                        //        }
                        //    }
                        //    for (var j = 0; $scope.noValueAttribute.length > j; j++) {
                        //        $scope.attributeColumnNames[j] = $scope.noValueAttribute[j].Name;
                        //    }

                        //    //angular.forEach($scope.noValueAttribute1, function (value, key) {
                        //    //    angular.forEach(value, function (value1, key1) {
                        //    //        $scope.nullValues[k] = value1;
                        //    //        k++;


                        //    //if (value1 == value.PRODUCT_ID)
                        //    //{
                        //    //    $scope.array[k] = "~" + value.PRODUCT_ID;
                        //    //    $scope.nullValues[k] = $scope.array[k];
                        //    //    k++;
                        //    //}
                        //    //else
                        //    //{
                        //    //    $scope.nullValues[k] = value1;
                        //    //    k++;
                        //    //}


                        //    //   });
                        //    //});
                        //    for (var a = 0 ; $scope.noValueAttribute1.length > a ; a++) {

                        //        //$scope.attributeValues[a].push($scope.nullValues[a]);

                        //            $scope.attributeValues[a] = $scope.noValueAttribute1[a];




                        //        //$scope.attributeValues.push($scope.noValueAttribute1[a]);
                        //    }
                    }
                    //else {
                    //    $scope.attributeEmptyColumn = "No Records Available";
                    //}




                });
                // });
                //  }
                //});

            }
            $scope.dataCount();
        }


        $scope.selectedRowValue = function (PRODUCT_ID, e, data) {

            $('#nullvalueAttrgrid').removeClass("col-sm-12");
            $('#nullvalueAttrgrid').addClass("col-sm-6");

            $(".pull-left").css('display', "none");


            $('#grid10').removeClass("col-sm-12");
            $('#table10').show();
            $('#table10').addClass("col-sm-6");

            $('#nullValueAttrEdit').show();
            $('#nullvalueAttrgrid').removeClass("col-sm-12");
            $('#nullvalueAttrgrid').addClass("col-sm-6");
            $("#dataReset").show();

            $scope.getSelectedRowProd_id = PRODUCT_ID;
            $scope.getRowEventValue = e;
            $scope.getselectedRowValue = data;
            $scope.selectedFamilyId;

            $scope.attrValuesForEdit = [];
            $scope.tableRowRecords = [];
            $scope.itemValue = [];
            $scope.itemName = [];

            $scope.itemNumbr = data["ITEM#"];

            //dataFactory.GetInvertedproductAttributeDetails($localStorage.getCatalogID, $scope.selectedFamilyId).success(function (response) {
            dataFactory.getNullvalueAttributesForSearch($scope.selectedFamilyId, $scope.pageno, $scope.ProdCountPerPage).success(function (response) {
                if (response != null) {
                    for (i = 0; i < response.length; i++) {
                        if (response[i].PRODUCT_ID == $scope.getSelectedRowProd_id) {
                            // selectedproduct.push(response[0]);
                            selectedproduct = response[i];
                            $scope.selectedRowAttributes = selectedproduct;
                            $scope.InvertedprodData = $scope.selectedRowAttributes;
                            $scope.InvertedprodData1 = $scope.InvertedprodData;
                            $scope.attributesOLD = Object.keys($scope.InvertedprodData);
                            attributeWithout = $scope.attributesOLD;

                            $scope.fromPageNo = 0;
                            $scope.toPageNo = 150;
                            if (attributeWithout.length < 150) {
                                $scope.toPageNo = attributeWithout.length;
                            }
                            var attributestemp = [];
                            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                                attributestemp.push(attributeWithout[i]);
                            }

                            $scope.searchText = '';
                            $scope.attributes = attributestemp;
                            $scope.updateproductspecs = $scope.InvertedprodData1;
                            $("#result").hide();
                            $scope.selectedRow = {};
                            angular.copy($scope.updateproductspecs, $scope.selectedRow);

                            //$scope.selectedRow = data["ITEM#"];
                        }
                    }
                }
            });


        }




        $scope.selectedRow = {};

        $scope.updateValue = function (selectedRow) {
            $scope.ParentProductId = 0;
            $scope.blurCheck = false;
            $scope.oldAttributeValues = $scope.getselectedRowValue;


            dataFactory.updateValueForAttr($scope.selectedRow, $scope.getcategory, $scope.selectedFamilyId, $scope.getSelectedRowProd_id, $scope.ParentProductId, $scope.blurCheck, $scope.oldAttributeValues).success(function (response) {
                $("#nullValueAttrEdit").hide();
                if (response == "Saved successfully.You are about to reach the maximum memory") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Update Successfully',
                        type: "info",
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Update Unsuccessfully',
                        type: "error"
                    });
                }
                dataFactory.getNullvalueAttributes($scope.selectedFamilyId, $scope.pageno, $scope.ProdCountPerPage).success(function (response) {
                    if (response != null) {
                        $scope.attributeValues = response;

                        $('#nullvalueAttrgrid').removeClass("col-sm-6");
                        $('#nullvalueAttrgrid').addClass("col-sm-12");
                        $(".pull-left").css('display', "block");
                    }
                });
            });


        }




        $scope.ResetEditClick = function () {
            $scope.searchText = '';
            $scope.attributeNameSearch();
            // $('#editvalue').val(' ');
            //         var elements = [] ;
            //elements = document.getElementsByClassName("MyTestClass");

            //for(var i=0; i<elements.length ; i++){
            //   elements[i].value = "" ;
            //}
        }

        $scope.attributeNameSearch = function () {
            $scope.fromPageNo = 1;
            $scope.toPageNo = 1;
            $scope.currentPage = -1;
            var attributeWithout = [];
            var attributesearch = [];

            angular.forEach($scope.attributesOLD, function (value, key) {
                if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2'))
                    attributeWithout.push(value);

            });
            angular.forEach(attributeWithout, function (value, key) {
                if (value.toLowerCase().indexOf($scope.searchText.toLowerCase()) > -1 || value.toUpperCase() == "PUBLISH2WEB" || value.toUpperCase() == "PUBLISH2PRINT" || value.toUpperCase() == "PUBLISH2PDF" || value.toUpperCase() == "PUBLISH2EXPORT" || value.toUpperCase() == "PUBLISH2PORTAL") {
                    attributesearch.push(value);
                }
            });
            attributeWithout = attributesearch;
            if (attributeWithout == 0) {
                $("#result").show();
            }
            else {
                $("#result").hide();
            }
            $scope.toPageNo = attributeWithout.length;
            var attributestemp = [];
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
                attributestemp.push(attributeWithout[i]);
            }
            $scope.attributes = attributestemp;
            if ($scope.attributes.length === 5) {
                $('#selectedattributenoresultfound').show();
            }
            else {
                $('#selectedattributenoresultfound').hide();
            }
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
                var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                if (tempattrvals.length > 0) {
                    var theString = tempattrvals[0].ATTRIBUTE_ID;
                    var uimask = "";
                    tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                    if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                        theString = 0;
                    }
                    if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                        pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                        pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                        var reg = new RegExp(pattern);
                        uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                        tempattrvals[0].attributePattern = reg;
                    } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text')) {
                        tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                        uimask = tempattrvals[0].DECIMAL;
                    } else {
                        tempattrvals[0].attributePattern = 524288;
                        uimask = 524288;
                    }
                    tempattrvals[0].uimask = uimask;
                    $scope.selectedRowDynamicAttributes[theString] = [];
                    $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                }
            }
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
                if (attributeWithout[i].contains("OBJ")) {
                    if (attributeWithout[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            var theString = sa.ATTRIBUTE_ID;
                            $scope.GetPickListData(theString, sa.PICKLIST_NAME);
                        }
                    }
                }
            }
            $scope.searchText = '';
        };

        //    $scope.attributeNameSearch = function () {
        //    $scope.fromPageNo = 1;
        //    $scope.toPageNo = 1;
        //    $scope.currentPage = -1;
        //    var attributeWithout = [];
        //    var attributesearch = [];
        //   var SearchedAttribute = [];
        //    $scope.attrcolumn=[];
        //    $scope.searchAttr = $scope.searchText;

        //    //dataFactory.getNullvalueAttributesForSearch($scope.selectedFamilyId, $scope.pageno, $scope.ProdCountPerPage).success(function (response) {
        //    //    var obj = jQuery.parseJSON(response.Data.Data);
        //    //    $scope.attributesOLD = obj[0];

        //    dataFactory.GetInvertedproductAttributeDetails($localStorage.getCatalogID,$scope.selectedFamilyId).success(function (response) {
        //            if (response != null) {
        //                for (i = 0; i < response.length; i++) {
        //                    if (response[i].PRODUCT_ID == $scope.getSelectedRowProd_id) {
        //                        // selectedproduct.push(response[0]);
        //                        selectedproduct = response[i];
        //                        $scope.selectedRowAttributes = selectedproduct;
        //                        $scope.InvertedprodData = $scope.selectedRowAttributes;
        //                        $scope.InvertedprodData1 = $scope.InvertedprodData;
        //                        $scope.attributesOLD = Object.keys($scope.InvertedprodData);
        //                        attributeWithout = $scope.attributesOLD;

        //                        $scope.fromPageNo = 0;
        //                        $scope.toPageNo = 150;
        //                        if (attributeWithout.length < 150) {
        //                            $scope.toPageNo = attributeWithout.length;
        //                        }
        //                        var attributestemp = [];
        //                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
        //                            attributestemp.push(attributeWithout[i]);
        //                        }

        //                        $scope.searchText = '';
        //                        $scope.attributes = attributestemp;
        //                    }
        //                }
        //            }
        //    //});

        //            angular.forEach($scope.attributesOLD, function (value,key) {
        //        if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2'))
        //            attributeWithout.push(value);

        //    });
        //         //$scope.attrcolumns=attributeWithout;

        //         //for (var i = 0 ; $scope.attrcolumns.length > i ; i++) {
        //         //    if ($scope.attrcolumns[i].includes("__OBJ")) {
        //         //        SearchedAttribute.push($scope.attrcolumns[i].slice(0, $scope.attrcolumns[i].search("__OBJ")));
        //         //    }
        //         //}


        //    angular.forEach(attributeWithout, function (value, key) {
        //        if (value.toLowerCase().indexOf($scope.searchAttr.toLowerCase()) > -1 || value.toUpperCase() == "PUBLISH2WEB" || value.toUpperCase() == "PUBLISH2PRINT" || value.toUpperCase() == "PUBLISH2PDF" || value.toUpperCase() == "PUBLISH2EXPORT" || value.toUpperCase() == "PUBLISH2PORTAL") {
        //            attributesearch.push(value);
        //        }
        //   });



        //    attributeWithout = attributesearch;
        //    if (attributeWithout == 0) {
        //         $("#result").show();
        //             //$scope.getselectedRowValue = SearchedAttribute;
        //    }
        //    else {
        //             $("#result").hide();
        //            // $scope.getselectedRowValue = attributeWithout;

        //    }
        //    $scope.toPageNo = attributeWithout.length;
        //    var attributestemp = [];
        //    for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
        //        attributestemp.push(attributeWithout[i]);
        //    }
        //    $scope.attributes = attributestemp;
        //    if ($scope.attributes.length === 5) {
        //        $('#selectedattributenoresultfound').show();
        //    }
        //    else {
        //        $('#selectedattributenoresultfound').hide();
        //    }
        //    for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
        //        var tempattrvals = '';
        //            //= $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__') });
        //        if (tempattrvals.length > 0) {
        //            var theString = tempattrvals[0].ATTRIBUTE_ID;
        //            var uimask = "";
        //            tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
        //            if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
        //                theString = 0;
        //            }
        //            if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
        //                var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
        //                pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
        //                pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
        //                var reg = new RegExp(pattern);
        //                uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
        //                tempattrvals[0].attributePattern = reg;
        //            } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text')) {
        //                tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
        //                uimask = tempattrvals[0].DECIMAL;
        //            } else {
        //                tempattrvals[0].attributePattern = 524288;
        //                uimask = 524288;
        //            }
        //            tempattrvals[0].uimask = uimask;
        //            $scope.selectedRowDynamicAttributes[theString] = [];
        //            $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
        //        }
        //    }
        //    for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
        //        //if (attributeWithout[i].contains("OBJ")) {
        //            if (SearchedAttribute !== "0") {
        //                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
        //                if (sa.USE_PICKLIST) {
        //                    var theString = sa.ATTRIBUTE_ID;
        //                    $scope.GetPickListData(theString, sa.PICKLIST_NAME);
        //                }
        //            }
        //        }
        //    //}
        //      });
        //    $scope.searchText = '';
        //};





        $scope.cancelEdit = function () {
            $('#nullValueAttrEdit').hide();
            $('#nullvalueAttrgrid').removeClass("col-sm-6");
            $('#nullvalueAttrgrid').addClass("col-sm-12");
            $(".pull-left").css('display', "block");

        }

        //for pagination===============================================================================================



        $scope.pagenew = function (pageno) {

            if (pageno != null && pageno <= $scope.totalProdPageCount1 && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {
                if (pageno == 'PREV' && $scope.ProdCurrentPage != 1) {
                    $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) - 1;
                } else if (pageno == 'PREV' && $scope.ProdCurrentPage == 1) {
                    $scope.ProdCurrentPage = "1";
                } else if (pageno == 'NEXT') {
                    $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) + 1;
                    if ($scope.ProdCurrentPage > $scope.totalProdPageCount1) {
                        $scope.ProdCurrentPage = $scope.totalProdPageCount1;
                    }
                } else {
                    $scope.ProdCurrentPage = pageno;
                }
                $scope.pageno = $scope.ProdCurrentPage;
                dataFactory.getNullvalueAttributes($scope.selectedFamilyId, $scope.pageno, $scope.ProdCountPerPage).success(function (response) {
                    if (response != null) {
                        $scope.attributeValues = response;
                    }
                });

            }
        };

        $scope.dataCount = function () {

            dataFactory.nullValueAttrCount($scope.selectedFamilyId).success(function (response) {
                var obj = jQuery.parseJSON(response);
                var ProdEditPageloopcnt = Math.ceil(parseInt(obj) / parseInt($scope.ProdCountPerPage));
                var ProdCountmodifiedar = [];
                for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                    ProdCountmodifiedar.push(i);
                }
                $scope.ProdPageCount = ProdCountmodifiedar;
                $scope.totalProdPageCount1 = ProdEditPageloopcnt;
                $scope.ProdCurrentPage = 1;
                if ($scope.ProdCountPerPage != undefined && $scope.ProdCountPerPage != "" && $scope.currentPageCount === "") {
                    $scope.ProdCountPerPage = $scope.ProdPerPageCount;
                }
            })

        };








    }]);