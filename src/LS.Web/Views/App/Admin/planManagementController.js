﻿'use strict';
LSApp.controller('planManagementController', ['$scope', '$window', '$location', 'dataFactory', '$route', '$localStorage', '$rootScope', function ($scope, $window, $location, dataFactory, $route, $localStorage,$rootScope) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    $scope.emailvalid = /^[A-Za-z0-9._-]+[A-Za-z0-9._-]+@[A-Za-z0-9._-]+\.[A-Za-z.]{2,5}$/;
    $scope.pattern = "/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/";
    $scope.pno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    $scope.regExPostalCode = /^[a-zA-Z0-9]{5,6}$/; // /^[0-9]{5,6}(?:-[0-9]{4})?$/;
    $scope.selectedPlan = {};
    $scope.IsNewPlan = false;

    $scope.planManagementDatasource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                dataFactory.getPlans(options.data).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        },
        schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "PLAN_ID",
                fields: {
                    PLAN_ID: { type: "int" },
                    PLAN_NAME: { type: "string" },
                    SKU_COUNT: { type: "int" },
                    NO_OF_USERS: { type: "int" },
                    SUBSCRIPTION_IN_MONTHS: { type: "int" },
                    IS_ACTIVE: { type: "bool" }
                }
            }
        },
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        pageSize: 5,
        error: function (e) {         
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + e.xhr.responseText + '.',
                type: "error"
            });
        }
    });

    $scope.planGridOptions = {
        autoBind: false,
        dataSource: $scope.planManagementDatasource,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 },
        columns: [{ field: "PLAN_ID", title: "PlanId", width: "100px" },
            { field: "PLAN_NAME", title: "PlanName", width: "100px" },
            { field: "SKU_COUNT", title: "SkuCount", width: "50px" },
            { field: "NO_OF_USERS", title: "NoOfUsers", width: "50px" },
            { field: "SUBSCRIPTION_IN_MONTHS", title: "Subscription In Months", width: "50px" },
            { field: "IS_ACTIVE", title: "IsActive", width: "70px", template: "<input type='checkbox' title='Active' kendo-tooltip  #= IsActive ? 'checked=checked' : ''# disabled='disabled' class='checkbox'/>" },
            { title: "", template: "<a title='Edit' kendo-tooltip  href='javascript:void(0);' ng-click='editPlan(this)' class='glyphicon glyphicon-edit'> </a> &nbsp; &nbsp;<a title='Detail' kendo-tooltip  href='javascript:void(0);' ng-click='showPlanDetail(this)' class='glyphicon glyphicon-th-list'></a>" ,  width: "60px" }
        ]
    };

    $scope.editPlan = function (row) {
        $scope.selectedPlan = angular.copy(row.dataItem);
        $scope.formSubmitted = false;
        $("#customerListView").hide();
        $("#editCustomerView").show();
        //$("#functionsAllowedForNewPlanGrid").hide();
        //$("#functionsAllowedForPlanGrid").show();
        $("#editTitle").html("Edit Customer");
        $("#editsub").html("Please update the customer information below.");
        $scope.IsNewPlan = false;
        $scope.functionsAllowedForPlanGridDatasource.read();
    };

    $scope.addNewPlan = function () {
        $("#planListView").hide();
        $("#editPlanView").show();
        $("#editPlanTitle").html("Add New Plan");
        $("#editPlansub").html("Please enter a new Plan information below.");
        $scope.IsNewPlan = true;
        $scope.functionsAllowedForPlanGridDatasource.read();
        //$("#functionsAllowedForNewPlanGrid").show();
        //$("#functionsAllowedForPlanGrid").hide();
    };


    $scope.showPlanDetail = function (row) {
        $scope.selectedPlan = angular.copy(row.dataItem);
        $("#planListView").hide();
        $("#editPlanView").hide();
        $("#detailTabsView").show();
        $("#detailContentView").show();
        $scope.navigateTo("/appUsers");
    };

    $scope.showListView = function () {
        $scope.navigateTo("/");
        $("#planListView").show();
        $("#editPlanView").hide();
        $("#detailTabsView").hide();
        $("#detailContentView").hide();
    };

    $scope.savePlan = function () {
        
        if ($scope.planForm.$valid == false) {
            $scope.formSubmitted = true;          
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter the required values.',
                type: "error"
            });
            return;
        }
        var str = $scope.planManagementDatasource;
        dataFactory.updatePlan($scope.selectedPlan)
        .success(function (data) {         
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Saved successfully.',
                type: "info"
            });

            // $scope.planManagementDatasource.read();
            $scope.initPlan();
            $("#planListView").show();
            $("#editPlanView").hide();
        })
        .error(function (error) {
            //console.log('Unable to save Plan data: ' + error.message);
        });
    };

    $scope.cancelChanges = function () {
        $scope.initPlan();
        //$scope.selectedCustomer = {};
        //$scope.selectedCustomer.CustomerId = 0;
        $("#planListView").show();
        $("#editPlanView").hide();
    };

    $scope.reset = function () {
        $scope.initPlan();
        //$scope.selectedCustomer = {};
        //$scope.selectedCustomer.CustomerId = 0;
        $scope.planForm.$setPristine(); //clean main form
        $scope.planForm.$dirty = false;
        $scope.formSubmitted = false;
    };

    $scope.searchplan = function () {
        if ($scope.selectedplan && $scope.selectedplan.length > 3) {
            $scope.planManagementDatasource.read();
        } else {
            if ($scope.selectedplan.length == 0) {
                $scope.planManagementDatasource.filter([]);
            }
        }
    };

    $scope.initPlan = function () {
        $scope.planManagementDatasource.read();

        $scope.selectedPlan = {
            PlanId: 0,
            PlanName: "",
            IsActive: false,
            SkuCount: 0,
            NoOfUsers: 0,
            SubscriptionInMonths: 0
        };
    };


    $scope.init = function () {
        $("#planListView").show();
        $("#editPlanView").hide();
        $scope.initPlan();
        $scope.formSubmitted = false;
        if ($location.$$path != "") {
            $location.$$path = "";
            $route.reload();
        }
    };
    $scope.init();

    $scope.navigateTo = function (route) {
        $location.path(route);
        $scope.currentRoute = route;
    };

    $scope.isActiveTab = function (route) {
        return (route) === $location.path();
    };

    $scope.functionsAllowedForPlanGridDatasource = new kendo.data.DataSource({
        type: "json",
        batch: true,
        pageSize: 5,
        serverFiltering: true,
        transport: {
            read: function (options) {
                if ($scope.IsNewPlan) {
                    dataFactory.getNewPlanFunctionAllowedData().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                } else {
                    dataFactory.getPlanFunctionAllowedData().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            update: function (options) {
                dataFactory.saveFunctionAllowedForPlan(options, $scope.selectedPlan).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
                return { models: kendo.stringify(options.models) };
            }
        },
        schema: {
            data: function (data) {
                return data;
            },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FUNCTION_ID",
                fields: {
                    FUNCTION_NAME: { type: "string", editable: false },
                    ACTION_VIEW: { type: "boolean" },
                    ACTION_MODIFY: { type: "boolean" },
                    ACTION_ADD: { type: "boolean" },
                    ACTION_REMOVE: { type: "boolean" }
                }
            }
        },
        error: function (e) {       
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + e.xhr.responseText + '.',
                type: "error"
            });


        }
    });

    $("#functionsAllowedForPlanGrid").kendoGrid({
        dataSource: $scope.functionsAllowedForPlanGridDatasource,
        pageable: { buttonCount: 5 },
        toolbar:
            ["save", "cancel"],
        columns: [
          { field: "FUNCTION_NAME", title: "FUNCTION_NAME", width: "200px" },
          { field: "ACTION_VIEW", title: "ACTION_VIEW", width: 130, template: '<input type="checkbox" class="chkbx5" #= ACTION_VIEW ? \'checked="checked"\' : "" #></input>' },
          { field: "ACTION_MODIFY", title: "ACTION_MODIFY", width: 130, template: '<input type="checkbox" class="chkbx6" #= ACTION_MODIFY ? \'checked="checked"\' : "" #></input>' },
          { field: "ACTION_ADD", title: "ACTION_ADD", width: 130, template: '<input type="checkbox" class="chkbx7" #= ACTION_ADD ? \'checked="checked"\' : "" #></input>' },
          { field: "ACTION_REMOVE", title: "ACTION_REMOVE", width: 130, template: '<input type="checkbox" class="chkbx8" #= ACTION_REMOVE ? \'checked="checked"\' : "" #></input>' },
        ],
        editable: {
            mode: "inline",
            update: true,
        }
    });

    $("#functionsAllowedForPlanGrid .k-grid-content").on("change", "input.chkbx5", function (e) {
        var grid = $("#functionsAllowedForPlanGrid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set("ACTION_VIEW", this.checked);
    });

    $("#functionsAllowedForPlanGrid .k-grid-content").on("change", "input.chkbx6", function (e) {
        var grid = $("#functionsAllowedForPlanGrid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set("ACTION_MODIFY", this.checked);
    });

    $("#functionsAllowedForPlanGrid .k-grid-content").on("change", "input.chkbx7", function (e) {
        var grid = $("#functionsAllowedForPlanGrid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set("ACTION_ADD", this.checked);
    });

    $("#functionsAllowedForPlanGrid .k-grid-content").on("change", "input.chkbx8", function (e) {
        var grid = $("#functionsAllowedForPlanGrid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set("ACTION_REMOVE", this.checked);
    });

}]);