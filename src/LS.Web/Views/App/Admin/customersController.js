﻿'use strict';
LSApp.controller('customersController', ['$scope', '$window', '$location', 'dataFactory', '$route', '$rootScope', '$localStorage'
, function ($scope, $window, $location, dataFactory, $route,$rootScope,$localStorage
) {
     $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    $scope.emailvalid = /^[A-Za-z0-9._-]+[A-Za-z0-9._-]+@[A-Za-z0-9._-]+\.[A-Za-z.]{2,5}$/;
    $scope.pattern = "/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/";
    $scope.pno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    $scope.regExPostalCode = /^[a-zA-Z0-9]{5,6}$/; // /^[0-9]{5,6}(?:-[0-9]{4})?$/;
    $scope.selectedCustomer = {};
    $scope.searchingCustomer = '';
    $scope.customerForm = {};
  
    $scope.customersDatasource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                dataFactory.getCustomers(options.data).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        },
        schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "CustomerId",
                fields: {
                    CustomerId: { type: "int" },
                    CustomerName: { type: "string" },
                    PlanName: { type: "string" },
                    ContactTitle: { type: "string" },
                    ContactName: { type: "string" },
                    CompanyName: { type: "string" },
                    Country: { type: "string" },
                    ActivationDate: { type: "string" },
                    IsActive: { type: "boolean", filterable: false }
                }
            }
        },
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        pageSize: 5,
        sort: {
            field: "CustomerName",
            dir: "asc"
        },
        error: function (e) {          
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + e.xhr.responseText + '.',
                type: "error"
            });
        }
    });

    $scope.customersGridOptions = {
        autoBind: false,
        dataSource: $scope.customersDatasource,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 },
        columns: [{ field: "CustomerId", title: "CustomerId", hidden: true },
            { field: "CustomerName", title: "Customer Name", width: "100px" },
            { field: "ContactTitle", title: "ContactTitle", width: "100px" },
            { field: "ContactName", title: "ContactName", width: "100px" },
            { field: "CompanyName", title: "CompanyName", width: "100px" },
            { field: "Plan.PlanName", title: "PlanName", width: "100px" },
            { field: "Country", title: "Country", width: "100px" },
            { field: "ActivationDate", title: "ActivationDate", width: "100px" },
            { field: "IsActive", title: "IsActive", width: "70px", filterable: false ,template: "<input type='checkbox' title='Active' kendo-tooltip  #= IsActive ? 'checked=checked' : ''# disabled='disabled' class='checkbox'/>" },
            { title: "", template: "<a title='Edit'  href='javascript:void(0);' ng-click='editCustomer(this)' class='glyphicon glyphicon-edit'></a> &nbsp; &nbsp;<a title='Detail'  href='javascript:void(0);' ng-click='showCustomerDetail(this)' class='glyphicon glyphicon-th-list'></a>", width: "60px" }
        ]
    };

    $scope.editCustomer = function (row) {
        $scope.countriesDataSource = null;
        $scope.salesRepDataSource = null;
        dataFactory.$q.all([dataFactory.getCountries()]).then(function (responses) {
            $scope.countriesDataSource = responses[0].data;
            $scope.HeadQuartersCountryCode = Enumerable.From($scope.countriesDataSource).Where("$.CountryCode  == 'US'").FirstOrDefault();
            $scope.BillingCountryCode = Enumerable.From($scope.countriesDataSource).Where("$.CountryCode  == 'US'").FirstOrDefault();
            dataFactory.getStatesByCountryCode("US").success(function (response) {
                $scope.statesDataSource = response;
                $scope.formSubmitted = false;
                $scope.selectedCustomer = angular.copy(row.dataItem);
                $scope.SalesRepId = angular.copy(row.dataItem.SalesRepId);
                $scope.BillingCountryCode = angular.copy(row.dataItem.BillingAddress.CountryCode);
                $scope.HeadQuartersCountryCode = angular.copy(row.dataItem.HeadQuartersAddress.CountryCode);
                $("#customerListView").hide();
                $("#editCustomerView").show();
                $("#editTitle").html("Edit Customer");
                $("#editsub").html("Please update the customer information below.");
            }).error(function (error) {
                //console.log('Unable to Get States');
            });

        });
    };

    $scope.addCustomer = function () {
        $("#customerListView").hide();
        $("#editCustomerView").show();
        $("#editTitle").html("Add New Customer");
        $("#editsub").html("Please enter a new customer information below.");
    };

    $scope.showCustomerDetail = function (row) {
        $scope.selectedCustomer = angular.copy(row.dataItem);
        $scope.formSubmitted = false;
        $("#customerListView").hide();
        $("#editCustomerView").hide();
        $("#detailTabsView").show();
        $("#detailContentView").show();
        $scope.navigateTo("/appUsers");
    };

    $scope.showListView = function () {
        $scope.navigateTo("/");
        $("#customerListView").show();
        $("#editCustomerView").hide();
        $("#detailTabsView").hide();
        $("#detailContentView").hide();
    };

    $scope.saveCustomer = function () {
        if ($scope.customerForm.$valid == false) {
            $scope.formSubmitted = true;         
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter the required values.',
                type: "error"
            });
            return;
        }
        dataFactory.updateCustomer($scope.selectedCustomer)
        .success(function (data) {      
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Saved successfully.',
                type: "info"
            });
            $scope.customersDatasource.read();
            $scope.initCustomer();
            $("#customerListView").show();
            $("#editCustomerView").hide();
        })
        .error(function (error) {
            //console.log('Unable to save customer data: ' + error.message);
        });
    };

    $scope.cancelChanges = function () {
        $scope.initCustomer();
        $("#customerListView").show();
        $("#editCustomerView").hide();
    };

    $scope.reset = function () {
        $scope.initCustomer();
        $scope.formSubmitted = false;
    };

    $scope.searchCustomers = function () {
        if ($scope.selectedCustomer && $scope.selectedCustomer.length > 3) {
            $scope.customersDatasource.read();
        } else {
            if ($scope.selectedCustomer.length == 0) {
                $scope.customersDatasource.filter([]);
            }
        }
    };

    $scope.initCustomer = function () {
        $scope.selectedCustomer = {
            CustomerId: 0,
            CustomerNumber: "",
            CustomerName: "",
            Website: "",
            InActive: false,
            ContactName: "",
            Address1: "",
            Address2: "",
            City: "",
            StateProvince: "",
            //CountryCode: "US",
            CountryCode: "",
            Coutry: "",
            Zip: "",
            EMail1: "",
            Phone: ""
        };
    };

    $scope.init = function () {
        $("#customerListView").show();
        $("#editCustomerView").hide();
        $("#detailTabsView").hide();
        $("#detailContentView").hide();
        $scope.customersDatasource.read();
        $scope.initCustomer();
        $scope.formSubmitted = false;
        if ($location.$$path != "") {
            $location.$$path = "";
            $route.reload();
        }
    };
    $scope.init();

    $scope.navigateTo = function (route) {

        $location.path(route);
        $scope.currentRoute = route;
    };

    $scope.isActiveTab = function (route) {
        return (route) === $location.path();
    };

    $scope.planListDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getListofPlans().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
}]);

