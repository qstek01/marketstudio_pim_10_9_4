﻿LSApp.controller('UserDashboardController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', 'ngDialog', '$rootScope', '$localStorage',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, ngDialog, $rootScope, $localStorage) {
        $rootScope.selecetedCatalogId = 0;
        $scope.SelectedItem = 0;
        $rootScope.currentUser = [];
        $scope.activationDate = new Date();
        var DefaultDateZone = new Date();
        $scope.SelectedTimeZone = DefaultDateZone.toString().split("(")[1].replace(")", "");
        $scope.userRoleForDashboard = false;
        //Get Month

        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        currentDate = month + "/" + day + "/" + year;
        firstDay = month + "/" + 1 + "/" + year;



        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


        $scope.tomorrow = new Date();
        $scope.tomorrow.setDate($scope.tomorrow.getDate() - 1);

        var monthSD = $scope.tomorrow.getUTCMonth() + 1;
        var daySD = $scope.tomorrow.getUTCDate();
        var yearSD = $scope.tomorrow.getUTCFullYear();


        //firstDay = monthSD + "/" + daySD + "/" + yearSD;
        var monthName = monthNames[dateObj.getMonth()];
        var monthNameSD = monthNames[$scope.tomorrow.getMonth()];
        $scope.startDateDashVal = monthNameSD + ' ' + 1 + ',' + yearSD;

        $scope.endDateDashVal = monthName + ' ' + day + ',' + year;

        //---end


        //End

        $scope.startDateDashValue = firstDay;
        $scope.endDateDashValue = currentDate;
        //End



        //   $localStorage.CatalogItemNumber = "Item#";  


        $localStorage.CatalogItemNumber = document.getElementById("ITEMId").value.replace(/["'\(\)]/g, "")
        $rootScope.CatalogItemNumber = $localStorage.CatalogItemNumber;

        $localStorage.CatalogSubItemNumber = document.getElementById("SUBITEMId").value.replace(/["'\(\)]/g, "")
        $rootScope.CatalogSubItemNumber = $localStorage.CatalogSubItemNumber;



        //Dashboard Rights
        $scope.GetRightsForDashboard = function () {
            dataFactory.getUserSuperAdmin().success(function (response) {
                if (response !== "" && response !== null) {
                    if (response == 1) {
                        //SuperAdmin
                        $scope.userRoleForDashboard = 0;

                        $scope.GetCustomerDetails();
                        $scope.GetCustomerCountByMonthlyWise();
                        $scope.GetCustomerCountByPlace();
                        $scope.GetTotalCustomerAndUserCount();

                    }
                    else if (response == 2) {
                        //Admin

                        $scope.userRoleForDashboard = 1;
                        // $scope.GetChartdata();
                        $scope.GetSkuCount();
                        $scope.AssetsCount();
                        $scope.ProductattributesCount();
                        $scope.FamilyattributesCount();
                        $scope.CategoryattributesCount();
                        //$scope.DataQuality();

                    }
                    else {
                        //Users                       
                        $scope.userRoleForDashboard = 1;
                        // $scope.GetChartdata();
                        $scope.GetSkuCount();
                        $scope.AssetsCount();
                        $scope.ProductattributesCount();
                        $scope.FamilyattributesCount();
                        $scope.CategoryattributesCount();
                       // $scope.DataQuality();

                    }
                }
            }).error(function (error) {
                options.error(error);
            });
        };

        ////TimeZone
        //$scope.timeZoneDataSource = new kendo.data.DataSource({

        //    type: "json",
        //    serverFiltering: true,
        //    transport: {

        //        read: function (options) {
        //            dataFactory.GetUserTimeZone().success(function (response) {
        //                options.success(response);
        //            }).error(function (error) {
        //                options.error(error);
        //            });
        //        }
        //    }
        //});

        //$scope.TimeZone = "";
        //$scope.DateZone = "";
        //$scope.timeZoneChange = function () {
        //    dataFactory.GetSelecedtimeZoneChange($scope.SelectedTimeZone).success(function (response) {

        //        var objTimeZone = response.split("-");
        //        var objDate = objTimeZone[0];
        //        $scope.DateZone = objDate;
        //        var objTime = objTimeZone[1];
        //        var objUTCTime = objTimeZone[2];
        //        var objresultTime = objTime.split("(");
        //        $scope.TimeZone = objresultTime[0].substring(1, 6);
        //        //var objResultUTCTime = objUTCTime.split(")");
        //        // $scope.UTCTimeZone = objResultUTCTime[0];
        //    }).error(function (error) {
        //        options.error(error);
        //    });

        //};


        // Dashboard Catalog Dropdown
        $scope.DashboardcatalogDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {

                    dataFactory.GetCurretUserInfo().success(function (response) {
                        $rootScope.currentUser = response;
                        $localStorage.getCustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
                        $localStorage.getUserName = $rootScope.currentUser.Username;

                        if (localStorage.undolistFlag == "1") {
                            //-----------------------------------Family UNDO Flags -------------------------//
                            $localStorage.saveDetails = [];
                            $localStorage.saveSelectedFamilyDetails = [];
                            $localStorage.savebaseFamilyAttributeRow = [];
                            $localStorage.undoFamilyTemplateData = [];
                            $localStorage.undoProductTemplateData = [];
                            //-----------------------------------Family UNDO Flags -------------------------//


                            //-----------------------------------Product UNDO Flags -------------------------//
                            $localStorage.savebaseProductAttributeRow = [];
                            //-----------------------------------Family UNDO Flags -------------------------//


                            //-----------------------------------Category Undo Flags------------------------//
                            $localStorage.categoryChangedListUndo = [];
                            $localStorage.baseCategoryValUndoList = [];
                            $localStorage.undoCategoryTemplateData = [];
                            //-----------------------------------Category Undo Flags------------------------//

                            //-----------------------------------Only Product UNDO Flags -------------------------//
                            $localStorage.selectedListOnlyProductDetails = [];
                            $localStorage.baseUndoOnlyProductDetails = [];
                            $localStorage.ProductDetailsValueOnly = [];
                            //-----------------------------------Only Product UNDO Flags -------------------------//

                            //  $rootScope.selectedFamilyDetails = [];
                            $rootScope.selectedFamilyDetails = [];
                            localStorage.undolistFlag = "0";
                        }


                        dataFactory.GetCatalogDetailsForCurrentCustomer($scope.SelectedItem, $rootScope.currentUser.CustomerDetails.CustomerId).success(function (cResponse) {
                            options.success(cResponse);
                            ////  $scope.GetRecentlyModifiedCategoriesDatasource.read();
                            //  $scope.GetRecentlyModifiedFamiliesDatasource.read();
                            //  $scope.GetRecentlyModifiedProductsDatasource.read();
                            // $scope.GetSoonToExpiredCustomerDatasource.read();
                            if ($localStorage.getCatalogID === undefined) {
                                $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                                $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                                $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                                $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                            }
                            if ($scope.SelectedCatalogId == '') {
                                $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                                $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                                $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                                $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                                $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                            }
                            var catalogchk = 0;
                            angular.forEach(cResponse, function (value) {
                                if (value.CATALOG_ID.toString() === $rootScope.selecetedCatalogId.toString()) {
                                    catalogchk = 1;
                                }
                            });
                            if (catalogchk === 0) {

                                angular.forEach(cResponse, function (value) {
                                    if (value.CATALOG_ID.toString() === 1) {
                                        catalogchk = 1;
                                    }
                                });
                                if (catalogchk == 1) {
                                    $rootScope.selecetedCatalogId = 1;
                                    $localStorage.getCatalogName = "Master Catalog";
                                    $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
                                    $scope.SelectedCatalogId = 1;
                                    $scope.DashboardcatalogDataSource.read();
                                    $rootScope.treeData._data = [];
                                    i = 0;
                                    $rootScope.treeData.read();
                                }
                                else {
                                    $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                                    $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                                    $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                                    $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                                    $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                                }
                            } else {
                            }
                            // $scope.modifyAll();
                            // alert('456')
                            $scope.modifyAllGO();
                            $scope.GetUserRoleRightsAll();
                            //blockUI.stop();
                        }).error(function (error) {
                            options.error(error);
                        });
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.DashboardcatalogChange = function (e) {

            if ($scope.allowChangeFlag == "1") {
                //-----------------------------------Family UNDO Flags -------------------------//
                $localStorage.saveDetails = [];
                $localStorage.saveSelectedFamilyDetails = [];
                $localStorage.savebaseFamilyAttributeRow = [];
                $localStorage.undoFamilyTemplateData = [];
                $localStorage.undoProductTemplateData = [];
                $rootScope.selectedFamilyDetails = [];
                //-----------------------------------Family UNDO Flags -------------------------//


                //-----------------------------------Product UNDO Flags -------------------------//
                $localStorage.savebaseProductAttributeRow = [];
                //-----------------------------------Family UNDO Flags -------------------------//


                //-----------------------------------Category Undo Flags------------------------//
                $localStorage.categoryChangedListUndo = [];
                $localStorage.baseCategoryValUndoList = [];
                $localStorage.undoCategoryTemplateData = [];
                $rootScope.categoryUndoList = [];
                //-----------------------------------Category Undo Flags------------------------//

                //-----------------------------------Only Product UNDO Flags -------------------------//
                $localStorage.selectedListOnlyProductDetails = [];
                $localStorage.baseUndoOnlyProductDetails = [];
                $localStorage.ProductDetailsValueOnly = [];
                $rootScope.showSelectedFamilyProductDetails
                //-----------------------------------Only Product UNDO Flags -------------------------//

                //  $rootScope.selectedFamilyDetails = [];

                $rootScope.selecetedCatalogId = e.sender.value();
                $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
                $scope.SelectedCatalogName = e.sender.text();
                $localStorage.getCatalogName = e.sender.text();
                $rootScope.BreadCrumbCatalogName = $scope.SelectedCatalogName;
                if ($scope.userRoleForDashboard == 1 || $scope.userRoleForDashboard == 2) {
                    // $scope.GetChartdata();
                    $scope.GetSkuCount();
                    //$scope.AssetsCount();
                    $scope.ProductattributesCount();
                    $scope.FamilyattributesCount();
                    $scope.CategoryattributesCount();
                    //$scope.DataQuality();
                }
                else {

                }
                // $scope.modifyAll();
                //  alert('799')
                $scope.modifyAllGO();
                $scope.GetUserRoleRightsAll();
                //$scope.init();
                $scope.GetAllDataItemCount();

            }


            //dataFactory.GetCategoryCountDetails($rootScope.selecetedCatalogId).success(function (Count) {
            //    $scope.CategoryCount = Count;

            //}).error(function (error) {
            //    options.error(error);
            //});


            //dataFactory.GetFamilyCountDetails($rootScope.selecetedCatalogId).success(function (Count) {
            //    $scope.FamilyCount = Count;

            //}).error(function (error) {
            //    options.error(error);
            //});



            //dataFactory.GetProductCountDetails($rootScope.selecetedCatalogId).success(function (Count) {
            //    $scope.ProductCount = Count;

            //}).error(function (error) {
            //    options.error(error);
            //});


            //dataFactory.GetSubProductCountDetails($rootScope.selecetedCatalogId).success(function (Count) {
            //    $scope.SubProductCount = Count;
            //    //options.success(Count);
            //}).error(function (error) {
            //    options.error(error);
            //});

            //dataFactory.GetCategoryCountDetails($rootScope.selecetedCatalogId).success(function (Count) {
            //    $scope.CategoryCount = Count;

            //}).error(function (error) {
            //    options.error(error);
            //});


            //dataFactory.GetFamilyCountDetails($rootScope.selecetedCatalogId).success(function (Count) {
            //    $scope.FamilyCount = Count;

            //}).error(function (error) {
            //    options.error(error);
            //});



            //dataFactory.GetProductCountDetails($rootScope.selecetedCatalogId).success(function (Count) {
            //    $scope.ProductCount = Count;

            //}).error(function (error) {
            //    options.error(error);
            //});


            //dataFactory.GetSubProductCountDetails($rootScope.selecetedCatalogId).success(function (Count) {
            //    $scope.SubProductCount = Count;
            //    //options.success(Count);
            //}).error(function (error) {
            //    options.error(error);
            //});

        };

        //Dashboard Date filters
        //$scope.modifytoday = function () {
        //    $scope.Index = 4;
        //    $("#modifytoday").addClass("active");
        //    $("#modifyyesterday").removeClass("active");
        //    $("#modifyAll").removeClass("active");
        //    $("#modifylastsevendays").removeClass("active");
        //    // $scope.GetRightsForDashboard();
        //    $scope.GetRecentlyModifiedCategoriesDatasource.read();
        //    $scope.GetRecentlyModifiedFamiliesDatasource.read();
        //    $scope.GetRecentlyModifiedProductsDatasource.read();
        //    $scope.GetRecentlyModifiedSubProductsDatasource.read();
        //    if ($scope.userRoleForDashboard == 0 && $scope.userRoleForDashboard != false) {
        //        $scope.GetSoonToExpiredCustomerDatasource.read();
        //    }
        //};
        //$scope.modifyyesterday = function () {
        //    $scope.Index = 3;
        //    $("#modifytoday").removeClass("active");
        //    $("#modifyyesterday").addClass("active");
        //    $("#modifyAll").removeClass("active");
        //    $("#modifylastsevendays").removeClass("active");
        //    // $scope.GetRightsForDashboard();
        //    $scope.GetRecentlyModifiedCategoriesDatasource.read();
        //    $scope.GetRecentlyModifiedFamiliesDatasource.read();
        //    $scope.GetRecentlyModifiedProductsDatasource.read();
        //    $scope.GetRecentlyModifiedSubProductsDatasource.read();
        //    if ($scope.userRoleForDashboard == 0 && $scope.userRoleForDashboard != false) {
        //        $scope.GetSoonToExpiredCustomerDatasource.read();
        //    }
        //};
        //$scope.modifyAll = function () {
        //    $scope.Index = 1;
        //    $("#modifytoday").removeClass("active");
        //    $("#modifyyesterday").removeClass("active");
        //    $("#modifyAll").addClass("active");
        //    $("#modifylastsevendays").removeClass("active");
        //    // $scope.GetRightsForDashboard();
        //    $scope.GetRecentlyModifiedCategoriesDatasource.read();
        //    $scope.GetRecentlyModifiedFamiliesDatasource.read();
        //    $scope.GetRecentlyModifiedProductsDatasource.read();
        //    $scope.GetRecentlyModifiedSubProductsDatasource.read();
        //    if ($scope.userRoleForDashboard == 0 && $scope.userRoleForDashboard != false) {
        //        $scope.GetSoonToExpiredCustomerDatasource.read();
        //    }

        //    // $scope.GetChartdata();
        //};
        //$scope.modifylastsevendays = function () {
        //    $scope.Index = 2;
        //    $("#modifytoday").removeClass("active");
        //    $("#modifyyesterday").removeClass("active");
        //    $("#modifyAll").removeClass("active");
        //    $("#modifylastsevendays").addClass("active");
        //    //$scope.GetRightsForDashboard();
        //    $scope.GetRecentlyModifiedCategoriesDatasource.read();
        //    $scope.GetRecentlyModifiedFamiliesDatasource.read();
        //    $scope.GetRecentlyModifiedProductsDatasource.read();
        //    $scope.GetRecentlyModifiedSubProductsDatasource.read();
        //    if ($scope.userRoleForDashboard == 0 && $scope.userRoleForDashboard != false) {
        //        $scope.GetSoonToExpiredCustomerDatasource.read();
        //    }


        //};

        //Start

        //Tochange the design of datepicker and dropdowm value.

        $scope.modifyAllGO = function () {
            if ($scope.startDateDashVal.indexOf("/") >= 0) {
                $scope.startDateDashValue = $scope.startDateDashVal;
            }
            else {
                if ($scope.startDateDashVal != "") {
                    var splitStartDate = $scope.startDateDashVal.split(' ');
                    var month = monthNames.indexOf(splitStartDate[0]);
                    month = month + 1;
                    var splitAgainSD = splitStartDate[1].split(',');
                    $scope.startDateDashValue = month + "/" + splitAgainSD[0] + "/" + splitAgainSD[1];
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please choose a date',
                        type: "error"
                    });
                    return false;
                }
            }
            if ($scope.endDateDashVal.indexOf("/") >= 0) {
                $scope.endDateDashValue = $scope.endDateDashVal;
            }
            else {
                if ($scope.endDateDashVal != "") {
                    var splitEndDate = $scope.endDateDashVal.split(' ');
                    var monthED = monthNames.indexOf(splitEndDate[0]);
                    monthED = monthED + 1;
                    var splitAgainED = splitEndDate[1].split(',');
                    $scope.endDateDashValue = monthED + "/" + splitAgainED[0] + "/" + splitAgainED[1];
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please choose a date',
                        type: "error"
                    });
                    return false;
                }
            }

            var startDate = new Date($scope.startDateDashValue);
            var endDate = new Date($scope.endDateDashValue);
            $scope.GetAllDataItemCount();
            if (startDate & endDate) {
                $scope.Index = 8;
                $scope.GetRecentlyModifiedCategoriesDatasource.read();
                $scope.GetRecentlyModifiedFamiliesDatasource.read();
                $scope.GetRecentlyModifiedProductsDatasource.read();
                $scope.GetRecentlyModifiedSubProductsDatasource.read();
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose valid date',
                    type: "error"
                });
            }
        }

        //End

        // #region Total count of customer and User 
        $scope.GetTotalCustomerAndUserCount = function () {
            dataFactory.GetTotalCustomerAndUserCount().success(function (response) {
                $scope.TotalCustomerCount = response[0].TOTALCOUNT;
                $scope.TotalCustomerUsersCount = response[1].TOTALCOUNT;
            }).error(function (error) {
                options.error(error);
            });
        };
        //#endregion

        //Customer Activity
        $scope.GetCustomerDetails = function () {
            $("#dashboard-bar-customer").empty();
            dataFactory.GetCustomerDetails().success(function (response) {

                var promises = [];
                angular.forEach(response, function (value, key) {
                    if (response[key].CREATED_SKU > response[key].TOTAL_SKU) {
                        promises.push({
                            y: response[key].CUSTOMER_NAME,
                            a: response[key].TOTAL_SKU,
                            b: response[key].TOTAL_SKU,
                            c: 0
                        });

                    }
                    else {
                        promises.push({
                            y: response[key].CUSTOMER_NAME,
                            a: response[key].CREATED_SKU,
                            b: response[key].TOTAL_SKU,
                            c: (response[key].TOTAL_SKU) - (response[key].CREATED_SKU)
                        });
                    }

                });

                Morris.Bar(
                    {
                        element: 'dashboard-bar-customer',
                        data: promises,
                        xkey: 'y',
                        ykeys: ['a', 'b', 'c'],
                        labels: ['CREATED_SKU', 'TOTAL_SKU', 'REMAINING_SKU'],
                        barColors: ['#1caf9a', '#1caf2d', '#33414E'],
                        gridTextSize: '10px',
                        hideHover: true,
                        resize: true,
                        gridLineColor: '#E5E5E5',
                    });
            }).error(function (error) {
                options.error(error);
            });
        };

        // No Of Customer By Monthly wise 
        $scope.GetCustomerCountByMonthlyWise = function () {

            $("#dashboard-line-1").empty();
            dataFactory.GetCustomerCountByMonthlyWise().success(function (response) {

                var objcustomerData = [];
                angular.forEach(response, function (value, key) {

                    objcustomerData.push({

                        //y: ""+response[key].ActivationYear + "-" + response[key].ActivationMonthInNumbers  +"-"+ "01"+"",
                        //a: response[key].CustomerCount
                        y: response[key].ActivationMonth,
                        a: response[key].CustomerCount
                    });
                });
                Morris.Bar({
                    element: 'dashboard-line-1',
                    data: objcustomerData,
                    xkey: 'y',
                    ykeys: ['a'],
                    labels: ['CUSTOMER'],
                    resize: true,
                    hideHover: true,
                    xLabels: 'day',
                    gridTextSize: '10px',
                    lineColors: ['#E04B4A'],
                    gridLineColor: '#E5E5E5'
                });
            }).error(function (error) {
                options.error(error);
            });
        };
        //-----------------------Recently modified categories-----------------

        $scope.GetRecentlyModifiedCategoriesDatasource = new kendo.data.DataSource({
            type: "json",

            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 5,
            transport: {
                read: function (options) {
             
                    if ($scope.startDateDashVal.indexOf("/") >= 0) {
                        $scope.startDateDashValue = $scope.startDateDashVal;
                    }
                    else {
                        var splitStartDate = $scope.startDateDashVal.split(' ');
                        var month = monthNames.indexOf(splitStartDate[0]);
                        month = month + 1;
                        var splitAgainSD = splitStartDate[1].split(',');
                        $scope.startDateDashValue = month + "/" + splitAgainSD[0] + "/" + splitAgainSD[1];
                    }
                    if ($scope.endDateDashVal.indexOf("/") >= 0) {
                        $scope.endDateDashValue = $scope.endDateDashVal;
                    }
                    else {
                        var splitEndDate = $scope.endDateDashVal.split(' ');
                        var monthED = monthNames.indexOf(splitEndDate[0]);
                        monthED = monthED + 1;
                        var splitAgainED = splitEndDate[1].split(',');
                        $scope.endDateDashValue = monthED + "/" + splitAgainED[0] + "/" + splitAgainED[1];
                    }

                    dataFactory.GetRecentlyModifiedCategories($rootScope.selecetedCatalogId, $scope.startDateDashValue, $scope.endDateDashValue, $scope.Index, options.data).success(function (response) {
                        //$scope.CategoryCount = response.Total;
                        options.success(response);
                        var grid = $("#category").data("kendoGrid");
                        //if (grid !== undefined && grid !== null && response.Total > 0) {
                        //    grid.dataSource.query({
                        //        page: 1,
                        //        pageSize: 5
                        //    });
                        //}

                    }).error(function (error) {
                        $scope.CategoryCount = 0;
                        options.error(error);
                    });


                },

                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "CATEGORY_ID",
                    fields: {
                        CATEGORY_NAME: { editable: false },
                        MODIFIED_USER: { editable: false },
                        MODIFIED_DATE: { editable: false, type: "date" }
                    }
                }
            }

        });


        $scope.GetRecentlyModifiedCategoriesOptions = {
            dataSource: $scope.GetRecentlyModifiedCategoriesDatasource,
            sortable: true, scrollable: true, editable: true, pageable: { buttonCount: 5 }, autoBind: false,

            filterable: true,
            selectable: "row",
            columns: [
              { field: "CATEGORY_NAME", title: "CATEGORY", template: "<a title='#=CATEGORY_NAME#' class='k-link namelength' href='javascript:void(0);' ng-click='ngClkCategoryId(this)'>#=CATEGORY_NAME#</a>" },
              { field: "MODIFIED_USER", title: "MODIFIED BY", type: 'string' },
       {
           field: "MODIFIED_DATE", title: "MODIFIED", format: "{0:MMM d,yyyy}", filterable: { ui: "datetimepicker" }

       }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //------------------------------------------------------------------------------------------------------------------------------------------------------------



        $scope.RecentlyModifiedCategoriesColumns = [
       { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='#=CATEGORY_NAME#' class='k-link namelength' href='javascript:void(0);' ng-click='ngClkCategoryId(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
       { field: "MODIFIED_USER", title: "Modified User", type: 'string' },
       {
           field: "MODIFIED_DATE", title: "Modified Date", format: "{0:MMM d,yyyy}", filterable: { ui: "datetimepicker" }

       }];

        //Recently modified Family
        $scope.GetRecentlyModifiedFamiliesDatasource = new kendo.data.DataSource({
            type: "json",

            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 5,
            transport: {
                read: function (options) {

                    if ($scope.startDateDashVal.indexOf("/") >= 0) {
                        $scope.startDateDashValue = $scope.startDateDashVal;
                    }
                    else {
                        var splitStartDate = $scope.startDateDashVal.split(' ');
                        var month = monthNames.indexOf(splitStartDate[0]);
                        month = month + 1;
                        var splitAgainSD = splitStartDate[1].split(',');
                        $scope.startDateDashValue = month + "/" + splitAgainSD[0] + "/" + splitAgainSD[1];
                    }
                    if ($scope.endDateDashVal.indexOf("/") >= 0) {
                        $scope.endDateDashValue = $scope.endDateDashVal;
                    }
                    else {
                        var splitEndDate = $scope.endDateDashVal.split(' ');
                        var monthED = monthNames.indexOf(splitEndDate[0]);
                        monthED = monthED + 1;
                        var splitAgainED = splitEndDate[1].split(',');
                        $scope.endDateDashValue = monthED + "/" + splitAgainED[0] + "/" + splitAgainED[1];
                    }


                    dataFactory.GetRecentlyModifiedFamily($rootScope.selecetedCatalogId, $scope.startDateDashValue, $scope.endDateDashValue, $scope.Index, options.data).success(function (response) {
                        //$scope.FamilyCount = response.Total;
                        options.success(response);
                        var grid = $("#family").data("kendoGrid");

                        //if (grid !== undefined && grid !== null && response.Total > 0) {
                        //    grid.dataSource.query({
                        //        page: 1,
                        //        pageSize: 5
                        //    });
                        //}
                        //


                    }).error(function (error) {
                        $scope.FamilyCount = 0;
                        options.error(error);
                    });

                },

                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "FAMILY_ID",
                    fields: {
                        FAMILY_NAME: { editable: false },
                        MODIFIED_USER: { editable: false },
                        MODIFIED_DATE: { editable: false, type: "date" }
                    }
                }
            }

        });


        $scope.GetRecentlyModifiedFamiliesOptions = {
            dataSource: $scope.GetRecentlyModifiedFamiliesDatasource,
            sortable: true, scrollable: true, editable: true, pageable: { buttonCount: 5 }, autoBind: false,

            filterable: true,
            selectable: "row",
            columns: [
               { field: "FAMILY_NAME", title: "PRODUCT", type: 'string', template: "<a title='#=FAMILY_NAME#' class='k-link namelength' href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=FAMILY_NAME#</a>" },
               { field: "MODIFIED_USER", title: "MODIFIED BY", type: 'string' },
          {
              field: "MODIFIED_DATE", title: "MODIFIED", format: "{0:MMM d,yyyy}", filterable: { ui: "datetimepicker" }
          }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };


        $scope.RecentlyModifiedFamiliesColumns = [
               { field: "FAMILY_NAME", title: "Family Name", type: 'string', template: "<a title='#=FAMILY_NAME#' class='k-link namelength' href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=FAMILY_NAME#</a>" },
               { field: "MODIFIED_USER", title: "Modified User", type: 'string' },
          {
              field: "MODIFIED_DATE", title: "Modified date", format: "{0:MMM d,yyyy}", filterable: { ui: "datetimepicker" }
          }
        ];

        //Recently Modfied Products

        var fullProdLoad = 0;
        $scope.GetRecentlyModifiedProductsDatasource = new kendo.data.DataSource({
            type: "json",

            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 5,
            transport: {
                read: function (options) {
                
                    if ($scope.startDateDashVal.indexOf("/") >= 0) {
                        $scope.startDateDashValue = $scope.startDateDashVal;
                    }
                    else {
                        var splitStartDate = $scope.startDateDashVal.split(' ');
                        var month = monthNames.indexOf(splitStartDate[0]);
                        month = month + 1;
                        var splitAgainSD = splitStartDate[1].split(',');
                        $scope.startDateDashValue = month + "/" + splitAgainSD[0] + "/" + splitAgainSD[1];
                    }
                    if ($scope.endDateDashVal.indexOf("/") >= 0) {
                        $scope.endDateDashValue = $scope.endDateDashVal;
                    }
                    else {
                        var splitEndDate = $scope.endDateDashVal.split(' ');
                        var monthED = monthNames.indexOf(splitEndDate[0]);
                        monthED = monthED + 1;
                        var splitAgainED = splitEndDate[1].split(',');
                        $scope.endDateDashValue = monthED + "/" + splitAgainED[0] + "/" + splitAgainED[1];
                    }
                    $scope.optionData = options;
                    dataFactory.GetRecentlyModifiedProducts($rootScope.selecetedCatalogId, $scope.startDateDashValue, $scope.endDateDashValue, $scope.Index, "first", options.data).success(function (response) {
                        // $scope.ProductCount = response.Total;

                        options.success(response);

                        var grid = $("#PRODUCT").data("kendoGrid");
                        //if (grid !== undefined && grid !== null && response.Total > 0) {
                        //    grid.dataSource.query({
                        //        page: 1,
                        //        pageSize: 5
                        //    });
                        //}
                        //$scope.ProductCount();
                    }).error(function (error) {
                        $scope.FamilyCount = 0;
                        options.error(error);
                    });
                },

                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "PRODUCT_ID",
                    fields: {
                        STRING_VALUE: { editable: false },
                        MODIFIED_USER: { editable: false },
                        MODIFIED_DATE: { editable: false, type: "date" }
                    }
                }
            },
            change: function (e) {
                if (fullProdLoad == 1) {
                    dataFactory.GetRecentlyModifiedProducts($rootScope.selecetedCatalogId, $scope.startDateDashValue, $scope.endDateDashValue, $scope.Index, "all", $scope.optionData.data).success(function (response) {
                        $scope.optionData.success(response);
                        var grid = $("#PRODUCT").data("kendoGrid");
                    }).error(function (error) {
                        $scope.FamilyCount = 0;
                        e.error(error);
                    });
                }
                fullProdLoad++;
            }

        });


        $scope.GetRecentlyModifiedProductsOptions = {

            dataSource: $scope.GetRecentlyModifiedProductsDatasource,
            sortable: true, scrollable: true, editable: true, pageable: { buttonCount: 5 }, autoBind: false,

            filterable: true,
            selectable: "row",
            columns: [

            { field: "STRING_VALUE", title: "ITEM", type: 'string', template: "<a title='#=STRING_VALUE#' class='k-link namelength' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=STRING_VALUE#</a>" },
            { field: "MODIFIED_USER", title: "MODIFIED BY", type: 'string' },
            {
                field: "MODIFIED_DATE", title: "MODIFIED", format: "{0:MMM d,yyyy}", filterable: { ui: "datetimepicker" }
            }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        $scope.RecentlyModifiedProductsColumns = [
            { field: "STRING_VALUE", title: "Item#", type: 'string', template: "<a title='#=STRING_VALUE#' class='k-link namelength' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=STRING_VALUE#</a>" },
            { field: "MODIFIED_USER", title: "Modified User", type: 'string' },
            {
                field: "MODIFIED_DATE", title: "Modified date", format: "{0:MMM d,yyyy}", filterable: { ui: "datetimepicker" }
            }
        ];


        //Recently Modfied SubProducts

        $scope.GetRecentlyModifiedSubProductsDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 5,
            transport: {
                read: function (options) {
                    if ($scope.startDateDashVal.indexOf("/") >= 0) {
                        $scope.startDateDashValue = $scope.startDateDashVal;
                    }
                    else {
                        var splitStartDate = $scope.startDateDashVal.split(' ');
                        var month = monthNames.indexOf(splitStartDate[0]);
                        month = month + 1;
                        var splitAgainSD = splitStartDate[1].split(',');
                        $scope.startDateDashValue = month + "/" + splitAgainSD[0] + "/" + splitAgainSD[1];
                    }
                    if ($scope.endDateDashVal.indexOf("/") >= 0) {
                        $scope.endDateDashValue = $scope.endDateDashVal;
                    }
                    else {
                        var splitEndDate = $scope.endDateDashVal.split(' ');
                        var monthED = monthNames.indexOf(splitEndDate[0]);
                        monthED = monthED + 1;
                        var splitAgainED = splitEndDate[1].split(',');
                        $scope.endDateDashValue = monthED + "/" + splitAgainED[0] + "/" + splitAgainED[1];
                    }

                    dataFactory.GetRecentlyModifiedSubProducts($rootScope.selecetedCatalogId, $scope.startDateDashValue, $scope.endDateDashValue, $scope.Index, options.data).success(function (response) {

                        //$scope.SubProductCount = response.Total;
                        options.success(response);

                        var grid = $("#SUBPRODUCT").data("kendoGrid");

                        //if (grid !== undefined && grid !== null && response.Total > 0) {
                        //    grid.dataSource.query({
                        //        page: 1,
                        //        pageSize: 5
                        //    });
                        //}


                    }).error(function (error) {
                        $scope.FamilyCount = 0;
                        options.error(error);
                    });
                },

                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "PRODUCT_ID",
                    fields: {
                        STRING_VALUE: { editable: false },
                        MODIFIED_USER: { editable: false },
                        MODIFIED_DATE: { editable: false, type: "date" }
                    }
                }
            }

        });


        $scope.GetRecentlyModifiedSubProductsOptions = {
            dataSource: $scope.GetRecentlyModifiedSubProductsDatasource,
            sortable: true, scrollable: true, editable: true, pageable: { buttonCount: 5 }, autoBind: false,

            filterable: true,
            selectable: "row",
            columns: [
            { field: "STRING_VALUE", title: $localStorage.CatalogSubItemNumber, type: 'string', template: "<a title='#=STRING_VALUE#' class='k-link namelength' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=STRING_VALUE#</a>" },
             { field: "MODIFIED_USER", title: "Modified User", type: 'string' },
            {
                field: "MODIFIED_DATE", title: "Modified date", format: "{0:MMM d,yyyy}", filterable: { ui: "datetimepicker" }
            }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        $scope.RecentlyModifiedSubProductsColumns = [
           { field: "STRING_VALUE", title: $localStorage.CatalogSubItemNumber, type: 'string', template: "<a title='#=STRING_VALUE#' class='k-link namelength' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=STRING_VALUE#</a>" },
           { field: "MODIFIED_USER", title: "Modified User", type: 'string' },
           {
               field: "MODIFIED_DATE", title: "Modified date", format: "{0:MMM d,yyyy}", filterable: { ui: "datetimepicker" }
           }
        ];

        //Total Customer By Place in Maps
        $scope.GetCustomerCountByPlace = function () {

            dataFactory.GetCustomerCountByPlace().success(function (response) {
                //option.success(response);

                var objCustomerCountByPlace = [];
                angular.forEach(response, function (value, key) {
                    objCustomerCountByPlace.push({
                        latLng: [value.LATITUDE, value.LONGTITUDE], name: value.COUNTRY
                    });
                });
                var jvm_wm = new jvm.WorldMap(

         {
             container: $('#dashboard-map-seles'),
             map: 'world_mill_en',
             backgroundColor: '#FFFFFF',
             regionsSelectable: true,
             regionStyle: {
                 selected: { fill: '#B64645' },
                 initial: { fill: '#33414E' }
             },
             markerStyle: {
                 initial: {
                     fill: '#1caf9a',
                     stroke: '#1caf9a'
                 }
             },
             markers: objCustomerCountByPlace
         });
                /* END Vector Map */
                $(".x-navigation-minimize").on("click", function () {
                    setTimeout(function () {
                        rdc_resize();
                    }, 200);
                });
            }).error(function (error) {
                options.error(error);
            });
        };

        //Soon To Expire customers 
        $scope.GetSoonToExpiredCustomerDatasource = new kendo.data.DataSource({
            pageSize: 5,
            batch: false,
            serverPaging: false,
            serverSorting: false,
            serverFiltering: false,
            transport: {
                read: function (options) {
                    dataFactory.GetSoonToExpiredCustomer($scope.Index).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                data: function (data) { return data; },
                total: function (data) {
                    return data.length;
                },
                model: {
                    id: "CUSTOMER_ID",
                    fields: {
                        CUSTOMER_NAME: { editable: false },
                        PLAN_NAME: { editable: false },
                        SUBSCRIPTION_IN_MONTHS: { editable: false },
                        ACTIVATION_DATE: { editable: false },
                        ENDING_DATE: { editable: false }
                    }
                }, error: function (e) {
                    alert(e.xhr.responseText);
                }
            }
        });

        $scope.SoonToExpiredCustomerColumns = [
           { filed: "CUSTOMER_NAME", title: "Customer Name", type: 'string', template: "<a title='CUSTOMER_NAME' class='k-link' href='javascript:void(0);'>#=CUSTOMER_NAME#</a>" },
           { filed: "PLAN_NAME", title: "Plan Name", template: "<a title='PLAN_NAME' class='k-link' href='javascript:void(0);'>#=PLAN_NAME#</a>" },
           { filed: "SUBSCRIPTION_IN_MONTHS", title: "Subscription In Months", template: "<a title='SUBSCRIPTION_IN_MONTHS' class='k-link' href='javascript:void(0);'>#=SUBSCRIPTION_IN_MONTHS#</a>" },
           { filed: "ACTIVATION_DATE", title: "Activation Date", template: "<a title='ACTIVATION_DATE' class='k-link' href='javascript:void(0);'>#=ACTIVATION_DATE#</a>" },
           { filed: "ENDING_DATE", title: "Expired Date", template: "<a title='ENDING_DATE' class='k-link' href='javascript:void(0);'>#=ENDING_DATE#</a>" }

        ];
        // Catalog Activity 
        $scope.GetChartdata = function () {
            $("#dashboard-bar-1").empty();
            dataFactory.GetRecentlyModifiedCountDetails($rootScope.selecetedCatalogId).success(function (response) {
                Morris.Bar({
                    element: 'dashboard-bar-1',
                    data: [
                        { y: response[0].MODIFIED_DATE, a: response[0].CATEGORY, b: response[0].FAMILY, c: response[0].PRODUCT },
                        { y: response[1].MODIFIED_DATE, a: response[1].CATEGORY, b: response[1].FAMILY, c: response[1].PRODUCT },
                        { y: response[2].MODIFIED_DATE, a: response[2].CATEGORY, b: response[2].FAMILY, c: response[2].PRODUCT },
                        { y: response[3].MODIFIED_DATE, a: response[3].CATEGORY, b: response[3].FAMILY, c: response[3].PRODUCT },
                        { y: response[4].MODIFIED_DATE, a: response[4].CATEGORY, b: response[4].FAMILY, c: response[4].PRODUCT },
                        { y: response[5].MODIFIED_DATE, a: response[5].CATEGORY, b: response[5].FAMILY, c: response[5].PRODUCT },
                        { y: response[6].MODIFIED_DATE, a: response[6].CATEGORY, b: response[6].FAMILY, c: response[6].PRODUCT }
                    ],
                    xkey: 'y',
                    ykeys: ['a', 'b', 'c'],
                    labels: ['Category', 'Family', 'Product'],
                    barColors: ['#33414E', '#1caf9a', '#1caf2d'],
                    gridTextSize: '10px',
                    hideHover: true,
                    resize: true,
                    gridLineColor: '#E5E5E5',
                    //attr:("width", (x.range()[1] - x.range()[0]) / data.length - 2),
                });
                //options.success(response);
            }).error(function (error) {
                options.error(error);
            });

        };

        //SKU COUNT
       

        $scope.GetSkuCount = function () {
       
            $("#dashboard-donut-1").empty();
            dataFactory.GetSkuCount().success(function (response) {
                $scope.getPlanName = response.m_Item4;
                $scope.TotalSKU = response.m_Item2;
                if ($scope.TotalSKU >= 1000000000) {
                    $scope.TotalSKU = ($scope.TotalSKU / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                }
                if ($scope.TotalSKU >= 1000000) {
                    $scope.TotalSKU = ($scope.TotalSKU / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                }
                if ($scope.TotalSKU >= 1000) {
                    $scope.TotalSKU = ($scope.TotalSKU / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                }
              
                //Morris.Donut({
                //    element: 'dashboard-donut-1',
                //    data: [
                //            { label: "Created", value: response.m_Item1 },
                //            //{ label: "Total SKU", value: response.m_Item2 },
                //            { label: "Available", value: response.m_Item3 }
                //    ],
                //    //colors: ['#33414E', '#1caf9a', '#FEA223'],
                //    colors: ['#FEA223', '#1caf9a'],
                //    resize: true
                //});
               
                google.charts.load("current", { packages: ["corechart"] });
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                      ['Created', 'Available'],
                      ['Created', response.m_Item1],
                      ['Available', response.m_Item3]


                    ]);


                    var options = {
                      //  title:   $scope.TotalSKU + ' ' +'SKUs',
                        pieHole: 0.4,
                        is3D: true,

                        titleTextStyle: {
                            fontSize: 14,
                            fontWeight:'bold',
                            color:'#337ab7'
                
                            
                            }
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('Skudonutchart'));
                    chart.draw(data, options);
                }

            }).error(function (error) {
                options.error(error);
            });
        };
        
        //Assets
        $scope.AssetsCount = function () {
            $("#dashboard-donut-2").empty();
            dataFactory.AssetsCount().success(function (response) {
                $scope.AssetsCount = response.m_Item2;
                $scope.FileCount = response.m_Item1;
                $scope.PDFCount = response.m_Item3;
                $scope.MRTCount = response.m_Item4;
                $scope.IMGCount = response.m_Item5;
                $scope.othersCount = response.m_Item6;

                //Morris.Donut({
                //    element: 'dashboard-donut-2',
                //    data: [
                //            { label: "FileCount", value: response.m_Item1 },
                //            { label: "AssetCount", value: response.m_Item2 }
                //    ],
                //    //colors: ['#33414E', '#1caf9a', '#FEA223'],
                //    colors: ['#FEA223', '#1caf9a'],
                //    resize: true
                //});
                google.charts.load("current", { packages: ["corechart"] });
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                      ['File Type', 'Asset Count'],
                      ['PDFs', $scope.PDFCount],
                      ['MRT files', $scope.MRTCount],
                      ['Images', $scope.IMGCount],
                      ['Others', $scope.othersCount]
                    ]);

                    var options = {
                        //title: 'ASSETS',
                        pieHole: 0.4,
                        is3D: true,
                        x: 0,
                        y: 28.4,
                        titleTextStyle: {
                            fontSize: 14,
                            fontWeight: 'bold',
                            color: '#337ab7'
                        }
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('assetdonutchart'));
                    chart.draw(data, options);
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        //$scope.DataQuality = function () {

        //    $("#dashboard-donut-3").empty();


        //    dataFactory.DataQuality($rootScope.selecetedCatalogId).success(function (response) {

        //        $scope.TodalAttributes = 100;
        //        $scope.TotalProductSpecification = 100;
        //        $scope.TotalProductImages = 100;
        //        $scope.TotalProductPrice = 100;
        //        $scope.TotalProductKey = 100;
        //        $scope.TotalFamilyDescription = 100;
        //        $scope.TotalFamilyImages = 100;
        //        $scope.TotalFamilyspecifications = 100;
        //        $scope.TotalFamilyPrice = 100;
        //        $scope.TotalFamilyKey = 100;
        //        $scope.TotalCategoryspecifications = 100;
        //        $scope.TotalCategoryImgaes = 100;
        //        $scope.TotalCategoryDescription = 100;

        //        $scope.IncompleteAttributes = response[0].ItemArray[0];
        //        $scope.IncompleteProductSpecification = response[0].ItemArray[1];
        //        $scope.IncompleteProductImages = response[0].ItemArray[2];
        //        $scope.IncompleteProductPrice = response[0].ItemArray[3];
        //        $scope.IncompleteProductKey = response[0].ItemArray[4];
        //        $scope.IncompleteFamilyDescription = response[0].ItemArray[5];
        //        $scope.IncompleteFamilyImages = response[0].ItemArray[6];
        //        $scope.IncompleteFamilyspecifications = response[0].ItemArray[7];
        //        $scope.IncompleteFamilyPrice = response[0].ItemArray[8];
        //        $scope.IncompleteFamilyKey = response[0].ItemArray[9];
        //        $scope.IncompleteCategoryspecifications = response[0].ItemArray[10];
        //        $scope.IncompleteCategoryImgaes = response[0].ItemArray[11];
        //        $scope.IncompleteCategoryDescription = response[0].ItemArray[12];
        //        //google.charts.load('current', { 'packages': ['corechart', 'bar'] });
        //        //google.charts.setOnLoadCallback(drawStuff);

        //        google.charts.load('current', { packages: ['corechart', 'bar'] });
        //        google.charts.setOnLoadCallback(drawAxisTickColors);

        //        function drawAxisTickColors() {
        //            var data = google.visualization.arrayToDataTable([
        //              ['Attributes', 'Total', 'In Completed'],
        //              ['Product Specification', $scope.TotalProductSpecification, $scope.IncompleteProductSpecification],
        //              ['Product Price', $scope.TotalProductPrice, $scope.IncompleteProductPrice],
        //              ['Product Images', $scope.TotalProductImages, $scope.IncompleteProductImages],
        //              ['Product Key', $scope.TotalProductKey, $scope.IncompleteProductKey],
        //              ['Family Description', $scope.TotalFamilyDescription, $scope.IncompleteFamilyDescription],
        //              ['Family Images', $scope.TotalFamilyImages, $scope.TotalFamilyImages],
        //              ['Family Price', $scope.TotalFamilyPrice, $scope.IncompleteFamilyPrice],
        //              ['Family Key', $scope.TotalFamilyKey, $scope.IncompleteFamilyKey],
        //              ['Family Specification', $scope.TotalFamilyspecifications, $scope.TotalFamilyspecifications],
        //              ['Category specifications', $scope.TotalCategoryspecifications, $scope.IncompleteCategoryspecifications],
        //              ['Category Images', $scope.TotalCategoryImgaes, $scope.IncompleteCategoryImgaes],
        //              ['Category Description', $scope.TotalCategoryDescription, $scope.IncompleteCategoryDescription]
        //            ]);



        //            var options = {
        //                title: 'DataQuality',
        //                focusTarget: 'category',
        //                VAxis: {
        //                    title: 'Attributes',


        //                    textStyle: {
        //                        fontSize: 14,
        //                        color: '#053061',
        //                        bold: true,
        //                        italic: false
        //                    },
        //                    titleTextStyle: {
        //                        fontSize: 18,
        //                        color: '#053061',
        //                        bold: true,
        //                        italic: false
        //                    }
        //                },

        //            };

        //            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        //            chart.draw(data, options);
        //        }

        //        //function drawStuff() {                  
        //        //    var chartDiv = document.getElementById('chart_div');
        //        //    var data = google.visualization.arrayToDataTable([
        //        //      ['Attributes', 'Total', 'In Completed'],
        //        //      ['Product Specification', $scope.TotalProductSpecification, $scope.IncompleteProductSpecification],
        //        //      ['Product Price', $scope.TotalProductPrice, $scope.IncompleteProductPrice],
        //        //      ['Product Images', $scope.TotalProductImages, $scope.IncompleteProductImages],

        //        //      ['Product Key', $scope.TotalProductKey, $scope.IncompleteProductKey],
        //        //      ['Family Description', $scope.TotalFamilyDescription, $scope.IncompleteFamilyDescription],
        //        //      ['Family Images', $scope.TotalFamilyImages, $scope.TotalFamilyImages],
        //        //      ['Family Price', $scope.TotalFamilyPrice, $scope.IncompleteFamilyPrice],
        //        //      ['Family Key', $scope.TotalFamilyKey, $scope.IncompleteFamilyKey],
        //        //      ['Family Specification', $scope.TotalFamilyspecifications, $scope.TotalFamilyspecifications],
        //        //      ['Category specifications', $scope.TotalCategoryspecifications, $scope.IncompleteCategoryspecifications],
        //        //      ['Category Images', $scope.TotalCategoryImgaes, $scope.IncompleteCategoryImgaes],
        //        //      ['Category Description', $scope.TotalCategoryDescription, $scope.IncompleteCategoryDescription],

        //        //    ]);

        //        //    var materialOptions = {
        //        //        //width: 900,
        //        //        chart: {
        //        //            title: 'Data Quality',

        //        //        },

        //        //        //},
        //        //        //axes: {
        //        //        //    y: {
        //        //        //        distance: { label: 'parsecs' }, // Left y-axis.
        //        //        //        brightness: { side: 'right', label: 'apparent magnitude' } // Right y-axis.
        //        //        //    }
        //        //        //}
        //        //    };




        //        //        var materialChart = new google.charts.Bar(chartDiv);
        //        //        materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));





        //        //};




        //        //Morris.Donut({
        //        //    element: 'dashboard-donut-3',
        //        //    data: [

        //        //              { label: "Incompleted ", value: response[2].ItemArray[0] },
        //        //               { label: "completed ", value: $scope.Completedpercentage }
        //        //    ],

        //        //    colors: ['#FEA223', '#1caf9a'],
        //        //    resize: true
        //        //});

        //    }).error(function (error) {
        //        options.error(error);
        //    });

        //};

        $scope.ProductattributesCount = function () {
            $("#dashboard-donut-2").empty();
            dataFactory.DataQuality($rootScope.selecetedCatalogId).success(function (response) {
                //$scope.TotalAttributes = response.Table[0].Totalproductattrs;
                //$scope.TotalProductSpecification = response.Table[0].TotalProductSpecification;
                //$scope.TotalProductImages = response.Table[0].TotalProductImages;
                //$scope.TotalProductPrice = response.Table[0].TotalProductPrice;
                //$scope.TotalProductKey = response.Table[0].TotalProductKey;

                //$scope.Totalfamilyattr = response.Table1[0].Totalfamilyattr;
                //$scope.TotalFamilyDescription = response.Table1[0].TotalFamilyDescription;
                //$scope.TotalFamilyImages = response.Table1[0].TotalFamilyImages;
                //$scope.TotalFamilyspecifications = response.Table1[0].TotalFamilyspecifications;
                //$scope.TotalFamilyPrice = response.Table1[0].TotalFamilyPrice;
                //$scope.TotalFamilyKey = response.Table1[0].TotalFamilyKey;

                //$scope.Totalcategoryattrs = response.Table2[0].Totalcategoryattrs;
                //$scope.TotalCategoryspecifications = response.Table2[0].TotalCategoryspecifications;
                //$scope.TotalCategoryImages = response.Table2[0].TotalCategoryImages;
                //$scope.TotalCategoryDescription = response.Table2[0].TotalCategoryDescription;
                


                $scope.TotalAttributes = response.Table[0].TOTALATTRIBUTES;
                $scope.IncompletedProduct = response.Table1[0].IncompletedProductAttributes;
                $scope.IncompletedFamily = response.Table2[0].IncompletedFamilyAttributes;
                $scope.IncompletedCategory = response.Table3[0].IncompletedCategoryAttributes;



                //google.charts.load("current", { packages: ["corechart"] });
                //google.charts.setOnLoadCallback(drawChart);
                //function drawChart() {
                //    var data = google.visualization.arrayToDataTable([
                //      ['Attribute Type', 'Attribute Count'],
                //      ['ProductSpecification', $scope.TotalProductSpecification],
                //      ['ProductImages', $scope.TotalProductImages],
                //       ['ProductPrice', $scope.TotalProductPrice],
                //      ['ProductKey', $scope.TotalProductKey],
                     
                //    ]);

                //    var options = {
                //        title: 'PRODUCT ATTRIBUTES',
                //        pieHole: 0.4,
                //        is3D: true,
                //        titleTextStyle: {
                //            fontSize: 25,
                //            bold: true,
                //            italic: false
                //        }
                //    };

                //    var chart = new google.visualization.PieChart(document.getElementById('productattrchart'));
                //    chart.draw(data, options);
                //}
                google.charts.load('current', { 'packages': ['gauge'] });
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                    var data = google.visualization.arrayToDataTable([
                      ['Label', 'Value'],
                      ['Item', $scope.IncompletedProduct],
                      ['Product', $scope.IncompletedFamily],
                      ['Category', $scope.IncompletedCategory]
                    ]);

                    var options = {
                        width: 400, height: 120,
                        redFrom: 0, redTo: 50,
                        greenFrom: 75, greenTo: 100,
                        yellowFrom: 50, yellowTo: 75,
                    };

                    var chart = new google.visualization.Gauge(document.getElementById('productattrchart'));
                   

                    chart.draw(data, options);

                }
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.FamilyattributesCount = function () {
            $("#dashboard-donut-2").empty();
            dataFactory.AssetsCount().success(function (response) {
              
                google.charts.load("current", { packages: ["corechart"] });
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                      ['Attribute Type', 'Attrbute Count'],
                     ['FamilySpecification', $scope.TotalFamilyspecifications],
                     ['FamilyDescription', $scope.TotalFamilyDescription],
                      ['FamilyImages', $scope.TotalFamilyImages],
                       ['FamilyPrice', $scope.TotalFamilyPrice],
                      ['FamilyKey', $scope.TotalFamilyKey]
                    ]);

                    var options = {
                        title: 'FAMILY ATTRIBUTES',
                        pieHole: 0.4,
                        is3D: true,
                        titleTextStyle: {
                            fontSize: 25,
                            bold: true,
                            italic: false
                        }
                    };

                    //var chart = new google.visualization.PieChart(document.getElementById('familyattrchart'));
                    //chart.draw(data, options);
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.CategoryattributesCount = function () {
            $("#dashboard-donut-2").empty();
            dataFactory.AssetsCount().success(function (response) {
                google.charts.load("current", { packages: ["corechart"] });
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                      ['Attribute Type', 'Attribute Count'],
                       ['CategorySpecification', $scope.TotalCategorySpecification],
                     ['CategoryDescription', $scope.TotalCategoryDescription],
                      ['CategoryImages', $scope.TotalCategoryImages],
                    ]);

                    var options = {
                        title: 'CATEGORY ATTRIBUTES',
                        pieHole: 0.4,
                        is3D: true,
                        titleTextStyle: {
                            fontSize: 25,
                            bold: true,
                            italic: false
                        }
                    };

                    //var chart = new google.visualization.PieChart(document.getElementById('categoryattrchart'));
                    //chart.draw(data, options);
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        //$scope.GetSkuCount = function () {
        //    $("#dashboard-donut-1").empty();
        //    dataFactory.GetSkuCount().success(function (response) {
        //        $scope.getPlanName = response.m_Item4;
        //        $scope.TotalSKU = response.m_Item2;
        //        Morris.Donut({
        //            element: 'dashboard-donut-1',
        //            data: [
        //                    { label: "Created", value: response.m_Item1 },
        //                    //{ label: "Total SKU", value: response.m_Item2 },
        //                    { label: "Available", value: response.m_Item3 }
        //            ],
        //            //colors: ['#33414E', '#1caf9a', '#FEA223'],
        //            colors: ['#FEA223', '#1caf9a'],
        //            resize: true
        //        });

        //    }).error(function (error) {
        //        options.error(error);
        //    });
        //};
        // Grid On Row Clicks
        //$scope.ngclkcatyid = function (e) {
        //    $("#familyEditor").show();
        //    $("#recentmodifiedtab").hide();
        //    $("#resultbtntab").show();
        //    $rootScope.clickSelectedItems(e.dataItem);
        //};
        $scope.LoadFamilySpecsData = function () {
            if ($scope.SelectedDataItem.ID !== "") {
                $scope.renderfamilyspecsgrid = true;
                $scope.renderfamilydescriptions = false;
                $scope.renderfamilyimages = false;
                $scope.renderrelatedfamily = false;
                $scope.renderclonedcategory = false;
                $scope.rendermultipletable = false;
                $rootScope.Family_Data = "Data";
                dataFactory.GetDynamicFamilySpecs($scope.Family.FAMILY_ID, $localStorage.CategoryID, $rootScope.selecetedCatalogId).success(function (response) {
                    if (response != null) {
                        var obj = jQuery.parseJSON(response.Data.Data);
                        if (obj.Columns.length > 0) {
                            $scope.familySpecsData = obj.Data;
                            $scope.columnsForFamilSpecs = obj.Columns;
                            $scope.actiondisable = true;
                        } else {
                            $scope.familySpecsData = [];
                            $scope.columnsForFamilSpecs = [];
                            $scope.actiondisable = false;
                        }
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
                $scope.renderfamilyspecsgrid = false;
            }
        };
        $scope.ngClkCategoryId = function (e) {

            $('#familyEditor').show();
            $('#recentmodifiedtab').hide();
            $('#resultbtntab').show();
            $('#tabstrip-2').css("display","none");
            e.dataItem.id = e.dataItem.CATEGORY_ID;
            $rootScope.clickSelectedItem(e.dataItem);

            // $("#theme").removeAttr("href");
            // $(".k-block, .k-button, .k-draghandle, .k-grid-header, .k-grouping-header, .k-header, .k-pager-wrap, .k-toolbar, .k-treemap-tile, html .km-pane-wrapper .k-header").css("background-color", " #d9ecf5 !important");

        };
        $scope.ngclkfamilyid = function (e) {

            $rootScope.Dashboards = true;
            $rootScope.val = false;
            $rootScope.inverted = '';
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;width: 1320px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1320px !important;");

            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1900px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;width: 1490px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1490px !important;");

            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

            }


            /*Family Grid*/

            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#familygrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;width: 1320px !important;");
                $("#familygrid tbody").css("cssText", "margin-left: -2px;width: 1320px !important;");
                //$('#sampleProdgrid').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid')[0].style.maxWidth='1220px !important';
                //$('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid tbody')[0].style.maxWidth = '1220px !important';
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#familygrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
                $("#familygrid tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#familygrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1900px !important;");
                $("#familygrid tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#familygrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;width: 1490px !important;");
                $("#familygrid tbody").css("cssText", "margin-left: -2px;width: 1490px !important;");

            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#familygrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#familygrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

            }

            /*Sub Product Grid*/

            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");
                //$('#sampleProdgrid').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid')[0].style.maxWidth='1220px !important';
                //$('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid tbody')[0].style.maxWidth = '1220px !important';
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

            }

            $("#familyEditor").show();
            $("#recentmodifiedtab").hide();
            $("#resultbtntab").show();
            $('#divProductGrid').show();
            $('#filterAttributePack').show();
            $('#filterAttributePack').css("display", "block");
           
            $('#dynamictable').show();
            $('#dynamictable').css("display", "block");
            $scope.dynamictable = true;
            $scope.Itemcountid = true;
            $('#Productcountid').removeClass('ng-hide');
            $scope.DIV = true;
            $('#filterAttributePackLeft').show();
            $('#filterAttributePackLeft').css("display", "block");
            $('#ItemGrid').removeClass('ng-hide');
            $('#newProductBtn1').show();
            $('#newProductBtn1').css("display", "block");
            $('#divProductGrid').css("display", "block");
            $rootScope.clickSelectedItems(e.dataItem, true, true);
            $("#ProductPackageNames").css({ "right": "-360px", "position": "relative" });
            $(".view-by").css({ "top": "-50px", "right": "-662px" });
            $('#divProductGrid').show();
            $('#divProductGrid').css("display", "block");
            /// $("#theme").removeAttr("href");
            //  $(".k-block, .k-button, .k-draghandle, .k-grid-header, .k-grouping-header, .k-header, .k-pager-wrap, .k-toolbar, .k-treemap-tile, html .km-pane-wrapper .k-header").css("background-color", " #d9ecf5 !important;");
            if ($scope.userRoleForDashboard == 1 || $scope.userRoleForDashboard == 2) {
                $("#dashboard-bar-1").empty();
                $("#dashboard-donut-1").empty();
                //$scope.GetChartdata();
                //  $scope.GetSkuCount();
                $rootScope.LoadFamilyData(e.dataItem.CATALOG_ID, e.dataItem.FAMILY_ID, e.dataItem.CATEGORY_ID);
            }
            else {

                $("#dashboard-bar-customer").empty();
                $("#dashboard-line-1").empty();

            }

            $scope.LoadFamilySpecsData();
            $scope.GetDefaultPdfTemplate();
        };


        $scope.GetDefaultPdfTemplate = function () {

            var TYPE = 'FAMILY';
            var Id = $scope.Family.FAMILY_ID;
            var Catalog_id = $rootScope.selecetedCatalogId;

            dataFactory.getPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {
                if (response != null && response != '0') {

                    $scope.SelectedFileForFamily = response;
                    $scope.SelectedFileForFamilyTooltip = response;
                    $localStorage.FamilyPdfXpressName = response;
                    $scope.$broadcast("MyEvent", response);
                }
                else {
                    $scope.SelectedFileForFamily = '';
                    $scope.SelectedFileForFamilyTooltip = 'No file chosen';
                }
            });
            dataFactory.getPdfXpressdefaultType('Product', Id, Catalog_id).success(function (response) {
                if (response != null && response != '0') {
                    $scope.SelectedFileForProduct = response;
                    $scope.SelectedFileForProductTooltip = response;
                }
                else {
                    $scope.SelectedFileForProduct = '';
                    $scope.SelectedFileForProductTooltip = 'No file chosen';
                }
            });
        }


        $scope.ngclkproductid = function (e) {
            
            $rootScope.Dashboards = true;
            $rootScope.inverted = '';
            if (e != undefined && e.dataItem != undefined && e.dataItem.PRODUCT_ID != undefined)
                $rootScope.ProductID = "ProductId~" + e.dataItem.PRODUCT_ID;
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;width: 1327px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1327px !important;");
                //$('#sampleProdgrid').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid')[0].style.maxWidth='1220px !important';
                //$('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid tbody')[0].style.maxWidth = '1220px !important';
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1460px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1500px !important;");


            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

            }

            /*Sub Product Grid*/

            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1327px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1327px !important;");
                //$('#sampleProdgrid').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid')[0].style.maxWidth='1220px !important';
                //$('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid tbody')[0].style.maxWidth = '1220px !important';
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: auto !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

            }


            //$('.ProductGrid').css('max-width', '1400px', 'important');
            //$('.ProductGrid tbody').css('max-width', '1220px', 'important');
            $("#familyEditor").show();
            $("#recentmodifiedtab").hide();
            $("#resultbtntab").show();
            $("").removeAttr("style");
            $("#subproductsgrid").hide();
            $("#productpaging1").hide();
            $('#tabstrip-2').show();
            $('#tabstrip-2').css("display", "block");
             $('#divProductGrid').show();
             $('#divProductGrid').css("display", "block");
            $rootScope.invertedproductsbutton1 = false;
            dataFactory.Getfirstfamilydetails($rootScope.selecetedCatalogId, e.dataItem.PRODUCT_ID).success(function (response) {
                if (response.length != 0)//------------------------------------------------------------------------------------------------------------------------
                {
                    response[0].PRODUCT_ID = e.dataItem.PRODUCT_ID;
                    $rootScope.clickSelectedItems(response[0], true, false);
                    $("#productpaging1").hide();
                }
                $('#divProductGrid').show();
                $('#divProductGrid').css("display", "block");
                $('#tabstrip-2').show();
                $('#tabstrip-2').css("display", "block");
            }).error(function (response) {
                options.success(response);
            });
            // $("#theme").removeAttr("href");
            // $(".k-block, .k-button, .k-draghandle, .k-grid-header, .k-grouping-header, .k-header, .k-pager-wrap, .k-toolbar, .k-treemap-tile, html .km-pane-wrapper .k-header").css("background-color", " #d9ecf5 !important");
        };

        //$scope.ngclkSubProductid = function (e) {

        //    $rootScope.Dashboards = true;
        //    $rootScope.inverted = '';
        //    $rootScope.SubProdInvert = false;
        //    $rootScope.invertedproductsbutton1 = false;
        //  //  e.dataItem.id = e.dataItem.FAMILY_ID;



        //    if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
        //        $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 1px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
        //        $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");
        //        //$('#sampleProdgrid').css('max-width', '1220px', '!important');
        //        //$('#sampleProdgrid')[0].style.maxWidth='1220px !important';
        //        //$('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
        //        //$('#sampleProdgrid tbody')[0].style.maxWidth = '1220px !important';
        //    }
        //    if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
        //        $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 1px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
        //        $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
        //    }

        //    if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
        //        $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 1px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
        //        $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");


        //    }
        //    if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
        //        $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 1px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
        //        $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

        //    }

        //    /*Sub Product Grid*/

        //    if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
        //        $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 1px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
        //        $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");
        //        //$('#sampleProdgrid').css('max-width', '1220px', '!important');
        //        //$('#sampleProdgrid')[0].style.maxWidth='1220px !important';
        //        //$('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
        //        //$('#sampleProdgrid tbody')[0].style.maxWidth = '1220px !important';
        //    }
        //    if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
        //        $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 1px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
        //        $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
        //    }

        //    if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
        //        $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 1px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
        //        $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

        //    }
        //    if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
        //        $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 1px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
        //        $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");

        //    }


        //    //$('.ProductGrid').css('max-width', '1400px', 'important');
        //    //$('.ProductGrid tbody').css('max-width', '1220px', 'important');
        //    $("#familyEditor").show();
        //    $("#recentmodifiedtab").hide();
        //    $("#resultbtntab").show();
        //    $rootScope.invertedproductsbutton1 = false;
        //    dataFactory.Getfirstfamilydetails($rootScope.selecetedCatalogId, e.dataItem.PRODUCT_ID).success(function (response) {

        //        response[0].PRODUCT_ID = e.dataItem.PRODUCT_ID;
        //        $rootScope.clickSelectedItems(response[0], true, false);
        //    }).error(function (response) {
        //        options.success(response);
        //    });
        //    // $("#theme").removeAttr("href");
        //    // $(".k-block, .k-button, .k-draghandle, .k-grid-header, .k-grouping-header, .k-header, .k-pager-wrap, .k-toolbar, .k-treemap-tile, html .km-pane-wrapper .k-header").css("background-color", " #d9ecf5 !important");
        //};

        $scope.ngclkSubProductid = function (e) {

            $scope.clicksubProductTab = true;
            $rootScope.Dashboards = true;
            $rootScope.inverted = '';
            $rootScope.SubProdInvert = false;
            $rootScope.invertedproductsbutton1 = false;
            //  e.dataItem.id = e.dataItem.FAMILY_ID;

            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1265px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1335px !important;");
            }

            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;width: 1310px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1310px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1465px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;width: 1485px !important;");
            }

            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;width: 1790px;");
            }

            /*Sub Product Grid*/

            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1265px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1265px !important;");
            }

            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1265px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1333px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1465px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;width: 1490px !important;");
            }

            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#samplegridss").css("cssText", "margin-top: 20px; outline: rgb(221, 221, 221) solid 0px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1790px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1790px !important;");
            }

            dataFactory.Getfirstsubfamilydetails($rootScope.selecetedCatalogId, e.dataItem.SUBPRODUCT_ID).success(function (response) {

                $("#familyEditor").show();
                $("#recentmodifiedtab").hide();
                $("#resultbtntab").show();
                response[0].SUBPRODUCT_ID = e.dataItem.SUBPRODUCT_ID;
                e.dataItem.id = response[0].FAMILY_ID;
                e.dataItem.CATEGORY_ID = response[0].CATEGORY_ID;
                e.dataItem.FAMILY_ID = response[0].FAMILY_ID;
                e.dataItem.PRODUCT_ID = response[0].PRODUCT_ID;
                $rootScope.clickSelectedItemsInverted(e.dataItem, false);
                $rootScope.SubProdInvert = false
                //var tabstripSp = $("#productgridtabstrip").data("kendoTabStrip");
                //var myTabSp = tabstripSp.tabGroup.children("li").eq(1);
                //
                //tabstripSp.enable(myTabSp);
                //tabstripSp.select(myTabSp);
                //$rootScope.inverted = '';
            }).error(function (response) {
                options.success(response);
            });
        };


        // Back to Results 
        $scope.ModifyResults = function (data) {
            $("#familyEditor").hide();
            $("#recentmodifiedtab").show();
            $("#resultbtntab").hide();
            //   $("#theme").attr("href", "css/theme-default.css");
            //  $(".k-block, .k-button, .k-draghandle, .k-grid-header, .k-grouping-header, .k-header, .k-pager-wrap, .k-toolbar, .k-treemap-tile, html .km-pane-wrapper .k-header").css("background-color", " #1caf9a !important");
            // $(".titleblue .k-toolbar .k-widget .k-toolbar-resizable").css("background-color", "#fafafa !important");
            //  $(".titleblue").css("border-color", "#efeded !important");

            if ($scope.userRoleForDashboard == 1 || $scope.userRoleForDashboard == 2) {

                $("#dashboard-bar-1").empty();
                $("#dashboard-donut-1").empty();
                $("#dashboard-map-seles").empty();
                //$scope.GetChartdata();
                $scope.GetSkuCount();
                $scope.AssetsCount();
                $scope.ProductattributesCount();
                $scope.FamilyattributesCount();
                $scope.CategoryattributesCount();
               // $scope.DataQuality();
                // $scope.GetCustomerCountByPlace();
            }
            else {
                $("#dashboard-bar-customer").empty();
                $("#dashboard-line-1").empty();

                //$scope.GetCustomerDetails();
                //$scope.GetCustomerCountByMonthlyWise();
                $scope.GetCustomerDetails();
                $scope.GetCustomerCountByMonthlyWise();
                //$scope.GetCustomerCountByPlace();
            }
            //alert('132')
            $scope.GetRecentlyModifiedCategoriesDatasource.read();
            $scope.GetRecentlyModifiedFamiliesDatasource.read();
            $scope.GetRecentlyModifiedProductsDatasource.read();

            if ($scope.userRoleForDashboard == 0 && $scope.userRoleForDashboard != false) {
                $scope.GetSoonToExpiredCustomerDatasource.read();
            }
        };


        //  $scope.Index = 4;
        //--------To get EnableSubProduct value from preference page-------
        $rootScope.getEnableSubProduct = function () {
            //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
            dataFactory.getEnableSubProduct().success(function (response) {

                $rootScope.EnableSubProduct = response;
                $localStorage.EnableSubProductLocal = response;
                if ($rootScope.EnableSubProduct == true) {


                    $("#Categories").addClass('col-md-4');
                    $("#Family").addClass('col-md-4');
                    $("#Product").addClass('col-md-4');
                    $("#Subprod").addClass('col-md-4');
                    $("#Categoriescount").addClass('col-md-2');
                    $("#Familycount").addClass('col-md-2');
                    $("#Productcount").addClass('col-md-2');
                    $("#Subproductcount").addClass('col-md-2');
                    //$("#Categories").css("cssText", "width: 25% !important;max-width: 25% !important;");  $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
                    //$("#Family").css("cssText", "width: 25% !important;max-width: 25% !important;");
                    //$("#Product").css("cssText", "width: 25% !important;max-width: 25% !important;");
                    // $("Subprod").css("cssText", "width: 25% !important;max-width: 25% !important;");
                    //$("#Categories")
                }
                else {
                    $("#Categories").addClass('col-md-4');
                    $("#Family").addClass('col-md-4');
                    $("#Product").addClass('col-md-4');
                    //$("#Categoriescount").addClass('col-md-3');
                    //$("#Familycount").addClass('col-md-3');
                    //$("#Productcount").addClass('col-md-3');
                    $("#Categoriescount").addClass('dashboard-sec-5');
                    $("#Attributecount").addClass('dashboard-sec-5');
                    $("#Productcount").addClass('dashboard-sec-5');
                    $("#Assetcount").addClass('dashboard-sec-5');

                    //$("#Categories").css("cssText", "width: 33.3% !important;max-width: 33.3% !important;");
                    //$("#Family").css("cssText", "width: 33.3% !important;max-width: 33.3% !important;");
                    //$("#Product").css("cssText", "width: 33.3% !important;max-width: 33.3% !important;");

                }
            })
        };
        //Custome Menu to fetch headers
        $scope.getHeader = function () {

            dataFactory.getHeader().success(function (response) {

                $rootScope.header = response;
            }).error(function (error) {
                options.error(error);
            });
        };

        //-------------------------------------------------------------------------


        $scope.GetAllDataItemCount = function () {

            dataFactory.GetDashboardCountDetails($rootScope.selecetedCatalogId).success(function (response) {
                $scope.CategoryCount = response.Table[0]["CATEGORYCOUNT"];
                $scope.FamilyCount = response.Table1[0]["FAMILYCOUNT"];
                $scope.ProductCount = response.Table2[0]["PRODUCTCOUNT"];
                $scope.SubProductCount = response.Table3[0]["SUBPRODUCTCOUNT"];


            }).error(function (error) {
                options.error(error);
            });
        }

        //In this Function Returns the Current producttitle Value
        $scope.PTitle = function () {
            dataFactory.getProductTitleValue().success(function (response) {
                //Assign the value into LocalStorage.
                $localStorage.ProdcutTitle = response;
                $rootScope.ProdcutTitle = $localStorage.ProdcutTitle;
                $scope.GetAllDataItemCount();
            }).error(function (error) {
                options.error(error);
            });
        };


        $scope.init = function () {
        
            if ($localStorage.getCatalogID === undefined) {
                $rootScope.selecetedCatalogId = 0;
            }
            else {
                $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
            }
            if ($rootScope.EnableSubProduct == undefined) {

                $rootScope.getEnableSubProduct();
            }

            $rootScope.BreadCrumbCatalogName = $localStorage.getCatalogName;
            $("#modifytoday").addClass("active");
            $("#modifyyesterday").removeClass("active");
            $("#modifyAll").removeClass("active");
            $("#modifylastsevendays").removeClass("active");


            //   $scope.modifyAllGO();
            $scope.GetRightsForDashboard();
            // $scope.modifyAll();
            // $scope.modifylastsevendays();
            //  $scope.GetTotalCustomerAndUserCount();
            $("#familyEditor").hide();
            $("#recentmodifiedtab").show();
            $("#resultbtntab").hide();
            $("#SUBPRODUCT").show();

            $rootScope.SubProdInvert = false;
            $rootScope.btnSubProdInvert = false;
            $rootScope.invertedproductsshow = false;
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = false;
            $scope.getHeader();
            $scope.PTitle();
            dataFactory.getAnnouncementLastDateRecords().success(function (response) {
                $scope.GetAnnouncement = response;
            }).error(function (error) {
                options.error(error);
            });

        };


        $scope.init();

        // Undo dropdown change start - Aswin kumar
        $scope.selectCatalog = function (e) {

            $scope.oldIndex = e.sender._oldIndex;
            $scope.allowChangeFlag = "0";
            if ($rootScope.selectedFamilyDetails == undefined) {
                $rootScope.selectedFamilyDetails = [];
            }

            if ($rootScope.categoryUndoList.length > 0 || $rootScope.selectedFamilyDetails.length > 0 || $rootScope.showSelectedFamilyProductDetails.length > 0) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Do you want to clear the Undo changes done by you?",
                    type: "info",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {

                        if (result === "Yes") {
                            $scope.oldIndex = e.sender._oldIndex;
                            $scope.allowChangeFlag = "1";
                            $scope.DashboardcatalogChange(e);

                        }

                        else {
                            var dropdownlist = $("#Dashboard_CatalogDD2").data("kendoDropDownList");
                            dropdownlist.select($scope.oldIndex);
                            $scope.allowChangeFlag = "1";
                        }
                    }
                });
            }

            else {

                $scope.allowChangeFlag = "1";
            }



        }

        // Undo dropdown change end - Aswin kumar

        //Call The ProductTitle Function




        //For Quick PageLoad ,Resolving F is not function issue
        //var callAtTimeout = function () {
        //};
        //$timeout(callAtTimeout, 3000);

    }]);

