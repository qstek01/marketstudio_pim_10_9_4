﻿LSApp.controller('MaindashboardController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', 'ngDialog', '$rootScope', '$localStorage',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, ngDialog, $rootScope, $localStorage) {

        $rootScope.selecetedCatalogId = 0;
        $scope.SelectedItem = 0;
        $rootScope.currentUser = [];
        $scope.activationDate = new Date();
        var DefaultDateZone = new Date();
        $scope.SelectedTimeZone = DefaultDateZone.toString().split("(")[1].replace(")", "");
        $scope.userRoleForDashboard = false;




        //Dashboard Rights
        $scope.GetRightsForDashboard = function () {
            dataFactory.getUserSuperAdmin().success(function (response) {
                if (response !== "" && response !== null) {
                    if (response == 1) {
                        //SuperAdmin
                        $scope.userRoleForDashboard = 0;

                        $scope.GetCustomerDetails();
                        $scope.GetCustomerCountByMonthlyWise();
                        $scope.GetCustomerCountByPlace();

                    }
                    else if (response == 2) {
                        //Admin

                        $scope.userRoleForDashboard = 1;
                        //$scope.GetChartdata();
                        $scope.GetSkuCount();

                    }
                    else {
                        //Users                       
                        $scope.userRoleForDashboard = 1;
                       // $scope.GetChartdata();
                        $scope.GetSkuCount();

                    }
                }
            }).error(function (error) {
                options.error(error);
            });
        };

        //TimeZone
        $scope.timeZoneDataSource = new kendo.data.DataSource({

            type: "json",
            serverFiltering: true,
            transport: {

                read: function (options) {
                    dataFactory.GetUserTimeZone().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.TimeZone = "";
        $scope.DateZone = "";
        $scope.timeZoneChange = function () {
            dataFactory.GetSelecedtimeZoneChange($scope.SelectedTimeZone).success(function (response) {

                var objTimeZone = response.split("-");
                var objDate = objTimeZone[0];
                $scope.DateZone = objDate;
                var objTime = objTimeZone[1];
                var objUTCTime = objTimeZone[2];
                var objresultTime = objTime.split("(");
                $scope.TimeZone = objresultTime[0].substring(1, 6);
            }).error(function (error) {
                options.error(error);
            });

        };


        // Dashboard Catalog Dropdown
        $scope.DashboardcatalogDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCurretUserInfo().success(function (response) {

                        $rootScope.currentUser = response;
                        $localStorage.getCustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
                        $localStorage.getUserName = $rootScope.currentUser.Username;
                        dataFactory.GetCatalogDetailsForCurrentCustomer($scope.SelectedItem, $rootScope.currentUser.CustomerDetails.CustomerId).success(function (cResponse) {
                            options.success(cResponse);

                            if ($localStorage.getCatalogID === undefined) {
                                $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                                $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                                $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                                $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                                $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                            }
                            if ($scope.SelectedCatalogId == '') {

                                $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                                $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                                $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                                $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                                $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                            }
                            var catalogchk = 0;
                            angular.forEach(cResponse, function (value) {

                                if (value.CATALOG_ID.toString() === $rootScope.selecetedCatalogId.toString()) {
                                    catalogchk = 1;
                                }
                            });
                            if (catalogchk === 0) {

                                $rootScope.selecetedCatalogId = 1;
                                $localStorage.getCatalogName = "Master Catalog";
                                $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
                                $scope.SelectedCatalogId = 1;
                                $scope.DashboardcatalogDataSource.read();
                                $rootScope.treeData._data = [];
                                i = 0;
                                $rootScope.treeData.read();
                            } else {
                            }
                            $scope.modifyAll();
                            $scope.GetUserRoleRightsAll();
                            //blockUI.stop();
                        }).error(function (error) {
                            options.error(error);
                        });
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.DashboardcatalogChange = function (e) {

            $rootScope.selecetedCatalogId = e.sender.value();
            $scope.SelectedCatalogName = e.sender.text();
            $rootScope.BreadCrumbCatalogName = $scope.SelectedCatalogName;
            if ($scope.userRoleForDashboard == 1 || $scope.userRoleForDashboard == 2) {
                //$scope.GetChartdata();
                $scope.GetSkuCount();
            }
            $scope.modifyAll();
            $scope.GetUserRoleRightsAll();
        };

        //Dashboard Date filters
        $scope.modifytoday = function () {
            $scope.Index = 4;
            $("#modifytoday").addClass("active");
            $("#modifyyesterday").removeClass("active");
            $("#modifyAll").removeClass("active");
            $("#modifylastsevendays").removeClass("active");
            // $scope.GetRightsForDashboard();
            $scope.GetRecentlyModifiedCategoriesDatasource.read();
            $scope.GetRecentlyModifiedFamiliesDatasource.read();
            $scope.GetRecentlyModifiedProductsDatasource.read();
            $scope.GetSoonToExpiredCustomerDatasource.read();
        };
        $scope.modifyyesterday = function () {
            $scope.Index = 3;
            $("#modifytoday").removeClass("active");
            $("#modifyyesterday").addClass("active");
            $("#modifyAll").removeClass("active");
            $("#modifylastsevendays").removeClass("active");
            // $scope.GetRightsForDashboard();
            $scope.GetRecentlyModifiedCategoriesDatasource.read();
            $scope.GetRecentlyModifiedFamiliesDatasource.read();
            $scope.GetRecentlyModifiedProductsDatasource.read();
            $scope.GetSoonToExpiredCustomerDatasource.read();
        };
        $scope.modifyAll = function () {
            $scope.Index = 1;
            $("#modifytoday").removeClass("active");
            $("#modifyyesterday").removeClass("active");
            $("#modifyAll").addClass("active");
            $("#modifylastsevendays").removeClass("active");
            // $scope.GetRightsForDashboard();
            $scope.GetRecentlyModifiedCategoriesDatasource.read();
            $scope.GetRecentlyModifiedFamiliesDatasource.read();
            $scope.GetRecentlyModifiedProductsDatasource.read();
            $scope.GetSoonToExpiredCustomerDatasource.read();

            // $scope.GetChartdata();
        };
        $scope.modifylastsevendays = function () {
            $scope.Index = 2;
            $("#modifytoday").removeClass("active");
            $("#modifyyesterday").removeClass("active");
            $("#modifyAll").removeClass("active");
            $("#modifylastsevendays").addClass("active");
            //$scope.GetRightsForDashboard();
            $scope.GetRecentlyModifiedCategoriesDatasource.read();
            $scope.GetRecentlyModifiedFamiliesDatasource.read();
            $scope.GetRecentlyModifiedProductsDatasource.read();
            $scope.GetSoonToExpiredCustomerDatasource.read();
        };

        // #region Total count of customer and User 
        $scope.GetTotalCustomerAndUserCount = function () {
            dataFactory.GetTotalCustomerAndUserCount().success(function (response) {
                $scope.TotalCustomerCount = response[0].TOTALCOUNT;
                $scope.TotalCustomerUsersCount = response[1].TOTALCOUNT;
            }).error(function (error) {
                options.error(error);
            });
        };
        //#endregion

        //Customer Activity
        $scope.GetCustomerDetails = function () {
            $("#dashboard-bar-customer").empty();
            dataFactory.GetCustomerDetails().success(function (response) {

                var promises = [];
                angular.forEach(response, function (value, key) {
                    if (response[key].CREATED_SKU > response[key].TOTAL_SKU) {
                        promises.push({
                            y: response[key].CUSTOMER_NAME,
                            a: response[key].TOTAL_SKU,
                            b: response[key].TOTAL_SKU,
                            c: 0
                        });

                    }
                    else {
                        promises.push({
                            y: response[key].CUSTOMER_NAME,
                            a: response[key].CREATED_SKU,
                            b: response[key].TOTAL_SKU,
                            c: (response[key].TOTAL_SKU) - (response[key].CREATED_SKU)
                        });
                    }

                });

                Morris.Bar(
                    {
                        element: 'dashboard-bar-customer',
                        data: promises,
                        xkey: 'y',
                        ykeys: ['a', 'b', 'c'],
                        labels: ['CREATED_SKU', 'TOTAL_SKU', 'REMAINING_SKU'],
                        barColors: ['#1caf9a', '#1caf2d', '#33414E'],
                        gridTextSize: '10px',
                        hideHover: true,
                        resize: true,
                        gridLineColor: '#E5E5E5',
                    });
            }).error(function (error) {
                options.error(error);
            });
        };

        // No Of Customer By Monthly wise 
        $scope.GetCustomerCountByMonthlyWise = function () {

            $("#dashboard-line-1").empty();
            dataFactory.GetCustomerCountByMonthlyWise().success(function (response) {

                var objcustomerData = [];
                angular.forEach(response, function (value, key) {
                    objcustomerData.push({
                        //y: ""+response[key].ActivationYear + "-" + response[key].ActivationMonthInNumbers  +"-"+ "01"+"",
                        //a: response[key].CustomerCount
                        y: response[key].ActivationMonth,
                        a: response[key].CustomerCount
                    });
                });
                Morris.Bar({
                    element: 'dashboard-line-1',
                    data: objcustomerData,
                    xkey: 'y',
                    ykeys: ['a'],
                    labels: ['CUSTOMER'],
                    resize: true,
                    hideHover: true,
                    xLabels: 'day',
                    gridTextSize: '10px',
                    lineColors: ['#E04B4A'],
                    gridLineColor: '#E5E5E5'
                });
            }).error(function (error) {
                options.error(error);
            });
        };

        //Recently Modified Category
        $scope.GetRecentlyModifiedCategoriesDatasource = new kendo.data.DataSource({
            
            pageSize: 5,
            batch: false,
            serverPaging: false,
            serverSorting: false,
            serverFiltering: false,
            transport: {
                read: function (options) {
                    debugger;
                    dataFactory.GetRecentlyModifiedCategories($rootScope.selecetedCatalogId, $scope.Index).success(function (response) {
                        $scope.CategoryCount = response.length;
                        options.success(response);
                    }).error(function (error) {
                        $scope.CategoryCount = 0;
                        options.error(error);
                    });
                }
            }, schema: {
                data: function (data) { return data; },
                total: function (data) {
                    return data.length;
                },
                model: {
                    id: "CATEGORY_ID",
                    fields: {
                        CATEGORY_NAME: { editable: false },
                        PARENT_CATEGORY: { editable: false }
                    }
                }, error: function (e) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + e.xhr.responseText + '.',
                        type: "error"
                    });

                }
            }
        });


        $scope.RecentlyModifiedCategoriesColumns = [
       { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='categoryid' class='k-link' href='javascript:void(0);' ng-click='ngClkCategoryId(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
       { field: "PARENT_CATEGORY", title: "Parent Category", width: "180px" }];

        //Recently modified Family
        $scope.GetRecentlyModifiedFamiliesDatasource = new kendo.data.DataSource({
            pageSize: 5,
            batch: false,
            serverPaging: false,
            serverSorting: false,
            serverFiltering: false,
            transport: {
                read: function (options) {
                     
                    dataFactory.GetRecentlyModifiedFamily($rootScope.selecetedCatalogId, $scope.Index).success(function (response) {
                        $scope.FamilyCount = response.length;
                        options.success(response);
                    }).error(function (error) {
                        $scope.FamilyCount = 0;
                        options.error(error);
                    });
                }
            }, schema: {
                data: function (data) { return data; },
                total: function (data) {
                    return data.length;
                },
                model: {
                    id: "FAMILY_ID",
                    fields: {
                        FAMILY_NAME: { editable: false },
                        MODIFIED_DATE: { type: "date" }
                    }
                }, error: function (e) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + e.xhr.responseText + '.',
                        type: "error"
                    });
                }
            }
        });

        $scope.RecentlyModifiedFamiliesColumns = [
               { field: "FAMILY_NAME", title: "Family Name", type: 'string', template: "<a title='FamilyName' class='k-link' href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=FAMILY_NAME#</a>" },
          {
              field: "MODIFIED_DATE", title: "Modified date", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }
          }
        ];

        //Recently Modfied Products
        $scope.GetRecentlyModifiedProductsDatasource = new kendo.data.DataSource({
            pageSize: 5, autobind: false,
            batch: false,
            serverPaging: false,
            serverSorting: false,
            serverFiltering: false,
            transport: {
                read: function (options) {
                    //dataFactory.GetRecentlyModifiedProducts($rootScope.selecetedCatalogId, $scope.Index).success(function (response) {
                    //    $scope.ProductCount = response.length;
                    //    options.success(response);
                    //}).error(function (error) {
                    //    $scope.ProductCount = 0;
                    //    options.error(error);
                    //});
                }
            }, schema: {
                data: function (data) { return data; },
                total: function (data) {
                    return data.length;
                },
                model: {
                    id: "PRODUCT_ID",
                    fields: {
                        STRING_VALUE: { editable: false },
                        MODIFIED_DATE: { type: "date" }
                    }
                }, error: function (e) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + e.xhr.responseText + '.',
                        type: "error"
                    });

                }
            }
        });

        $scope.RecentlyModifiedProductsColumns = [
            { field: "STRING_VALUE", title: "ITEM#", type: 'string', template: "<a title='catalogitemnumber' class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=STRING_VALUE#</a>" },
            {
                field: "MODIFIED_DATE", title: "Modified date", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }
        ];



        //Total Customer By Place in Maps
        $scope.GetCustomerCountByPlace = function () {

            dataFactory.GetCustomerCountByPlace().success(function (response) {
                //option.success(response);

                var objCustomerCountByPlace = [];
                angular.forEach(response, function (value, key) {
                    objCustomerCountByPlace.push({
                        latLng: [value.LATITUDE, value.LONGTITUDE], name: value.COUNTRY
                    });
                });
                var jvm_wm = new jvm.WorldMap(

         {
             container: $('#dashboard-map-seles'),
             map: 'world_mill_en',
             backgroundColor: '#FFFFFF',
             regionsSelectable: true,
             regionStyle: {
                 selected: { fill: '#B64645' },
                 initial: { fill: '#33414E' }
             },
             markerStyle: {
                 initial: {
                     fill: '#1caf9a',
                     stroke: '#1caf9a'
                 }
             },
             markers: objCustomerCountByPlace
         });
                /* END Vector Map */
                $(".x-navigation-minimize").on("click", function () {
                    setTimeout(function () {
                        rdc_resize();
                    }, 200);
                });
            }).error(function (error) {
                options.error(error);
            });
        };

        //Soon To Expire customers 
        $scope.GetSoonToExpiredCustomerDatasource = new kendo.data.DataSource({
            pageSize: 5,
            batch: false,
            serverPaging: false,
            serverSorting: false,
            serverFiltering: false,
            transport: {
                read: function (options) {
                    dataFactory.GetSoonToExpiredCustomer($scope.Index).success(function (response) {

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                data: function (data) { return data; },
                total: function (data) {
                    return data.length;
                },
                model: {
                    id: "CUSTOMER_ID",
                    fields: {
                        CUSTOMER_NAME: { editable: false },
                        PLAN_NAME: { editable: false },
                        SUBSCRIPTION_IN_MONTHS: { editable: false },
                        ACTIVATION_DATE: { editable: false },
                        ENDING_DATE: { editable: false }
                    }
                }, error: function (e) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + e.xhr.responseText + '.',
                        type: "error"
                    });
                }
            }
        });

        $scope.SoonToExpiredCustomerColumns = [
           { filed: "CUSTOMER_NAME", title: "Customer Name", type: 'string', template: "<a title='CUSTOMER_NAME' class='k-link' href='javascript:void(0);'>#=CUSTOMER_NAME#</a>" },
           { filed: "PLAN_NAME", title: "Plan Name", template: "<a title='PLAN_NAME' class='k-link' href='javascript:void(0);'>#=PLAN_NAME#</a>" },
           { filed: "SUBSCRIPTION_IN_MONTHS", title: "Subscription In Months", template: "<a title='SUBSCRIPTION_IN_MONTHS' class='k-link' href='javascript:void(0);'>#=SUBSCRIPTION_IN_MONTHS#</a>" },
           { filed: "ACTIVATION_DATE", title: "Activation Date", template: "<a title='ACTIVATION_DATE' class='k-link' href='javascript:void(0);'>#=ACTIVATION_DATE#</a>" },
           { filed: "ENDING_DATE", title: "Expired Date", template: "<a title='ENDING_DATE' class='k-link' href='javascript:void(0);'>#=ENDING_DATE#</a>" }

        ];

        // Catalog Activity 

        $scope.GetChartdata = function () {
            $("#dashboard-bar-1").empty();
            dataFactory.GetRecentlyModifiedCountDetails($rootScope.selecetedCatalogId).success(function (response) {
                Morris.Bar({
                    element: 'dashboard-bar-1',
                    data: [
                        { y: response[0].MODIFIED_DATE, a: response[0].CATEGORY, b: response[0].FAMILY, c: response[0].PRODUCT },
                        { y: response[1].MODIFIED_DATE, a: response[1].CATEGORY, b: response[1].FAMILY, c: response[1].PRODUCT },
                        { y: response[2].MODIFIED_DATE, a: response[2].CATEGORY, b: response[2].FAMILY, c: response[2].PRODUCT },
                        { y: response[3].MODIFIED_DATE, a: response[3].CATEGORY, b: response[3].FAMILY, c: response[3].PRODUCT },
                        { y: response[4].MODIFIED_DATE, a: response[4].CATEGORY, b: response[4].FAMILY, c: response[4].PRODUCT },
                        { y: response[5].MODIFIED_DATE, a: response[5].CATEGORY, b: response[5].FAMILY, c: response[5].PRODUCT },
                        { y: response[6].MODIFIED_DATE, a: response[6].CATEGORY, b: response[6].FAMILY, c: response[6].PRODUCT }
                    ],
                    xkey: 'y',
                    ykeys: ['a', 'b', 'c'],
                    labels: ['Category', 'Family', 'Product'],
                    barColors: ['#33414E', '#1caf9a', '#1caf2d'],
                    gridTextSize: '10px',
                    hideHover: true,
                    resize: true,
                    gridLineColor: '#E5E5E5',
                    //attr:("width", (x.range()[1] - x.range()[0]) / data.length - 2),
                });
                //options.success(response);
            }).error(function (error) {
                options.error(error);
            });

        };

        //SKU COUNT
        $scope.GetSkuCount = function () {
            $("#dashboard-donut-1").empty();
            dataFactory.GetSkuCount().success(function (response) {
                $scope.getPlanName = response.m_Item4;
                $scope.TotalSKU = response.m_Item2;
                Morris.Donut({
                    element: 'dashboard-donut-1',
                    data: [
                            { label: "Created", value: response.m_Item1 },
                            //{ label: "Total SKU", value: response.m_Item2 },
                            { label: "Available", value: response.m_Item3 }
                    ],
                    //colors: ['#33414E', '#1caf9a', '#FEA223'],
                    colors: ['#FEA223', '#1caf9a'],
                    resize: true
                });

            }).error(function (error) {
                options.error(error);
            });
        };


        $scope.ngClkCategoryId = function (e) {
            
            $('#familyEditor').show();
            $('#recentmodifiedtab').hide();
            $('#resultbtntab').show();
            e.dataItem.id = e.dataItem.CATEGORY_ID;
            $rootScope.clickSelectedItem(e.dataItem);
        };

        $scope.ngclkfamilyid = function (e) {
            $("#familyEditor").show();
            $("#recentmodifiedtab").hide();
            $("#resultbtntab").show();
            $rootScope.clickSelectedItems(e.dataItem, true, true);
            if ($scope.userRoleForDashboard == 1 || $scope.userRoleForDashboard == 2) {
                $("#dashboard-bar-1").empty();
                $("#dashboard-donut-1").empty();
                //$scope.GetChartdata();
                $scope.GetSkuCount();
            }
            else {

                $("#dashboard-bar-customer").empty();
                $("#dashboard-line-1").empty();

            }
        };

        $scope.ngclkproductid = function (e) {

            $rootScope.inverted = '';
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1030px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1030px !important;");

            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1770px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1770px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1590) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1210px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1210px !important;");

            }

            if (window.screen.availWidth >= 1600 && window.screen.availWidth <= 1800) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1420px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1420px !important;");

            }

            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

            }


            /*Sub Product*/

            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#invertedproductlist").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1030px !important;");
                $("#invertedproductlist tbody").css("cssText", "margin-left: -2px;max-width: 1030px !important;");

            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#invertedproductlist").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
                $("#invertedproductlist tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#invertedproductlist").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1780px !important;");
                $("#invertedproductlist tbody").css("cssText", "margin-left: -2px;max-width: 1780px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#invertedproductlist").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
                $("#invertedproductlist tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");

            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#invertedproductlist").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
                $("#invertedproductlist tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

            }
            $("#familyEditor").show();
            $("#recentmodifiedtab").hide();
            $("#resultbtntab").show();
            dataFactory.Getfirstfamilydetails($rootScope.selecetedCatalogId, e.dataItem.PRODUCT_ID).success(function (response) {
                $rootScope.clickSelectedItems(response[0], true, false);
            }).error(function (response) {
                options.success(response);
            });
        };

        // Back to Results 
        $scope.ModifyResults = function (data) {
            
            $scope.GetRecentlyModifiedCategoriesDatasource.read();
            $scope.GetRecentlyModifiedFamiliesDatasource.read();
            $scope.GetRecentlyModifiedProductsDatasource.read();
            $scope.GetSoonToExpiredCustomerDatasource.read();
            $("#familyEditor").hide();
            $("#recentmodifiedtab").show();
            $("#resultbtntab").hide();
            if ($scope.userRoleForDashboard == 1 || $scope.userRoleForDashboard == 2) {

                $("#dashboard-bar-1").empty();
                $("#dashboard-donut-1").empty();
                $("#dashboard-map-seles").empty();
                //$scope.GetChartdata();
                $scope.GetSkuCount();
                // $scope.GetCustomerCountByPlace();
            }
            else {
                $("#dashboard-bar-customer").empty();
                $("#dashboard-line-1").empty();
                $scope.GetCustomerDetails();
                $scope.GetCustomerCountByMonthlyWise();
            }
           

        };

        $scope.init = function () {
            if ($localStorage.getCatalogID === undefined) {
                $rootScope.selecetedCatalogId = 0;
            }
            else {
                $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
            }
            $rootScope.BreadCrumbCatalogName = $localStorage.getCatalogName;
            $("#modifytoday").addClass("active");
            $("#modifyyesterday").removeClass("active");
            $("#modifyAll").removeClass("active");
            $("#modifylastsevendays").removeClass("active");
            $scope.GetRightsForDashboard();
            //$scope.modifyAll();
            $scope.modifylastsevendays();
            $scope.GetTotalCustomerAndUserCount();
            $("#familyEditor").hide();
            $("#recentmodifiedtab").show();
            $("#resultbtntab").hide();

            $rootScope.SubProdInvert = false;
            $rootScope.btnSubProdInvert = false;
            $rootScope.invertedproductsshow = false;
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = false;

        };
        $scope.init();

        //For Quick PageLoad ,Resolving F is not function issue
        var callAtTimeout = function () {
        };
        $timeout(callAtTimeout, 3000);

    }]);

