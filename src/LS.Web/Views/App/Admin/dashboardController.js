﻿LSApp.controller('dashboardController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', 'ngDialog', '$rootScope', 'productService', '$localStorage', 'blockUI',
function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, ngDialog, $rootScope, productService, $localStorage, blockUI) {
    //----------------------------------------------------------start user rights ------------------------------------------------------------------------------------------------
    $scope.selectedattributeEdit = true;
    $rootScope.attributesetup = true;
    $rootScope.attributepack = true;
    
    var name = '';
    //$scope.FamilyDisplayFlag = "";
    $localStorage.imgaepopupforinvert = false;
    var SearchLength = $location.absUrl();

    if (SearchLength.length > 10) {
        var searchnameUserAdmin = SearchLength.substr(SearchLength.length - 9);
    }
    if (SearchLength.length > 11) {
        var searchnameUserImportData = SearchLength.substr(SearchLength.length - 10);
    }
    if (SearchLength.length > 16) {
        var searchnameUserImagemanagement = SearchLength.substr(SearchLength.length - 15);
    }
    if (SearchLength.length > 8) {
        var searchnameUserWizard = SearchLength.substr(SearchLength.length - 7);

    }

    // To avoid category gird, at the time of loading inverted products page 
    if (SearchLength.length > 8) {
        var searchnameinverted = SearchLength.substr(SearchLength.length - 8);
    }
    if (searchnameinverted == "Inverted") {
        $("#gridcategory").hide();
        $("#tabstrip").hide();

    } else {
        $("#gridcategory").show();
        $("#tabstrip").show();

    }
    //END
    // To avoid category gird, at the time of loading inverted products page    

    $rootScope.getCategoryPdfExpress = '';
    $rootScope.getCategoryPdfExpressToolTip = 'No file chosen';
    $rootScope.getFamilyPdfExpress = '';
    $rootScope.getProductPdfExpress = '';

    $rootScope.showSelectedFamilyProductDetails = [];
    //new
    $rootScope.userRoleViewSystemid = '';


    $scope.getUserDefaultCatalog = function () {
        dataFactory.getUserDefaultCatalog().success(function (response) {

            if (response !== "" && response !== null) {

                if ($rootScope.selecetedCatalogId == 0) { //|| $rootScope.selecetedCatalogId == 1
                    var items = response.split('~');
                    blockUI.start();
                    $scope.SelectedCatalogId = items[0];
                    $rootScope.selecetedCatalogId = items[0];
                    $localStorage.getCatalogName = items[1];
                    $rootScope.BreadCrumbCatalogName = $localStorage.getCatalogName;

                    $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
                    $rootScope.treeData._data = [];
                    i = 0;
                    $rootScope.treeData.read();
                    //  $rootScope.dropDownTreeViewParnetCategoryDatasource.read();
                    //  $rootScope.dropDownTreeViewDatasource.read();
                    blockUI.stop();
                }


            }

        }).error(function (error) {
            options.error(error);
        });
    };



    if (searchnameUserAdmin == "UserAdmin" || searchnameUserImagemanagement == "ImageManagement" || searchnameUserWizard == "Reports" || searchnameUserImportData == "ImportData") {

        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    }



    $scope.familyPackage = [];

    $timeout(function () {
        dataFactory.FamilyPackages($scope.SelectedCatalogId).success(function (response) {
            if (response != null || response != "undefined") {
                $scope.familyPackage = response;
            }
        });
    }, 2000);

    $scope.productPackage = [];

    $timeout(function () {
        dataFactory.ProductPackages($scope.SelectedCatalogId).success(function (response) {
            if (response != null || response != "undefined") {
                $scope.productPackage = response;
            }
        });
    }, 2000);



    $scope.associateFamilyAttribute = function (e) {

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure you want to associate '" + e.GROUP_NAME + "' Group ?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    dataFactory.AssociateAttributePackaging(e.GROUP_ID, $scope.RightClickedCategoryId, $scope.SelectedCatalogId).success(function (response) {
                        if (response !== "") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);
                            $scope.$broadcast("LoadFamilyGroupAssociate");
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                };
            }
        });

    };

    $scope.associateProductAttribute = function (e) {

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure you want to associate '" + e.GROUP_NAME + "' Group ?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    dataFactory.AssociateAttributePackaging(e.GROUP_ID, $scope.SelectedCategoryId, $scope.SelectedCatalogId).success(function (response) {
                        if (response !== "") {
                            $scope.$broadcast("LoadProductGroupAssociate");
                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                };
            }
        });
    };


    //$scope.associateFamilyAttribute = function (e) {

    //    dataFactory.AssociateAttributePackaging(e.GROUP_ID, $scope.RightClickedCategoryId, $scope.SelectedCatalogId).success(function (response) {
    //        if (response !== "") {
    //            $.msgBox({
    //                title: $localStorage.ProdcutTitle,
    //                content: '' + response + '.',
    //                type: "info"
    //            });
    //        }
    //    }).error(function (error) {
    //        options.error(error);
    //    });
    //};

    //$scope.associateProductAttribute = function (e) {

    //    dataFactory.AssociateAttributePackaging(e.GROUP_ID, $scope.SelectedCategoryId, $scope.SelectedCatalogId).success(function (response) {
    //        if (response !== "") {
    //            $.msgBox({
    //                title: $localStorage.ProdcutTitle,
    //                content: '' + response + '.',
    //                type: "info"
    //            });
    //        }
    //    }).error(function (error) {
    //        options.error(error);
    //    });
    //};




    $scope.userRoleAddCategory = false;
    $scope.userRoleModifyCategory = false;
    $scope.userRoleDeleteCategory = false;
    $scope.userRoleViewCategory = false;
    $rootScope.Mainprodcount = 0;
    $scope.userRoleAddFamily = false;
    $scope.userRoleModifyFamily = false;
    $scope.userRoleDeleteFamily = false;
    $scope.userRoleViewFamily = false;
    $rootScope.Productnew = false;
    $rootScope.ajaxLoaderDivShow = false;
    $rootScope.ajaxLoaderDivShowSub = false;
    $rootScope.userRoleAddExportIMPORT = false;
    $rootScope.invertedBackToProduct = false;
    $rootScope.invertedBackTosubProduct = false;
    $rootScope.InvertedprodDataBackup = [];
    $rootScope.InvertedsubprodDataBackup = [];
    // $rootScope.userRoleAddImport = false;
    $rootScope.userRoleAddWizard = false;
    $rootScope.userRoleAddRecycleBin = false;
    $rootScope.userRoleAddReferenceTable = false;
    $rootScope.userRoleAddInDesign = false;

    $scope.userRoleAddProductTable = false;
    $scope.userRoleModifyProductTable = false;
    $scope.userRoleDeleteProductTable = false;
    $scope.userRoleViewProductTable = false;

    $scope.userRoleAddCreateManage = false;
    $scope.userRoleModifyCreateManage = false;
    $scope.userRoleDeleteCreateManage = false;
    $scope.userRoleViewCreateManage = false;

    //------------------------------------------Search Enable -----------------------------------------//
    $rootScope.userSearchEnable = false;
    //------------------------------------------ImageManage Enable -----------------------------------------//
    $rootScope.userImageEnable = false;
    $rootScope.userRoleAssetView = false;
    $rootScope.userRoleAssetAdd = false;
    $rootScope.userRoleAssetModify = false;
    $rootScope.userRoleAssetRemove = false;

    //----------------------------------Create and Manage Features enable-----------------------------//
    $rootScope.userSelectallattributescheckbox = false;
    $rootScope.userCopypastedel = false;
    $rootScope.userClone = false;
    $rootScope.userStyle = false;
    $rootScope.userHTMLCatalog = false;
    //----------------------------------PDF Catalogs Features enable-----------------------------//
    $rootScope.userPdfCreate = false;
    $rootScope.userpdfdestemp = false;
    $rootScope.userpdfrun = false;
    $rootScope.userpdfopencat = false;
    //----------------------------------Summary -----------------------------//
    $rootScope.userSummary = false;
    //----------------------------------Missing Image -----------------------------//
    $rootScope.userMissingImage = false;
    //----------------------------------Report -----------------------------//
    $rootScope.userReportRun = false;
    //----------------------------------Idesign PDF -----------------------------//
    $rootScope.userIndesign = false;
    $rootScope.userIndesigncpy = false;
    $rootScope.userIndesignmove = false;
    //----------------------------------Global Attributes -----------------------------//
    $rootScope.userGlobalattrassociation = false;
    $rootScope.userGlobalcolumnsort = false;
    $rootScope.userGlobalcalcattr = false;
    $rootScope.userGlobalattrval = false;
    //----------------------------------Multiple tables -----------------------------//
    $rootScope.userMultiplenable = false;
    $rootScope.userMultiplenableAdd = false;
    $rootScope.userMultiplenableModify = false;
    $rootScope.userMultiplenableView = false;
    $rootScope.userMultiplenableRemove = false;
    //----------------------------------invertedprod -----------------------------//
    $rootScope.userinvp = false;
    //----------------------------------invertedsubprod -----------------------------//
    $rootScope.userinvpsub = false;
    //----------------------------------Proudcts -----------------------------//
    $rootScope.usercommon = false;
    $rootScope.userprodtable = false;
    $rootScope.userprodcongig = false;
    $rootScope.userprodcon = false;
    //----------------------------------print -----------------------------//
    $rootScope.userPrint = false;

    //----------------------prefernece------------
    $rootScope.userpref = false;
    // ---------------------- Supplier 
    $rootScope.userSupplier = false;
    // ---------------------- Supplier 
    $rootScope.userImageandAttachments = false;
    $rootScope.familyMainEditor = true;
    $rootScope.SubProdInvert = false;
    $rootScope.invertedproductsshow = false;
    $rootScope.invertedproductsbutton = false;
    $rootScope.invertedproductsbutton1 = false;
    $rootScope.btnSubProdInvert = false;
    $rootScope.displayCloneFamilies = false;

    // ------------------------Pdf Xpress

    $scope.rootCategotyForPdfXpress = false;

    //----------------------------Pdf Xpress

    //------------------------------------------------------Undo flags--------------------------------------------------------//
    $rootScope.categoryUndo = true;
    $rootScope.familyUndo = false;
    //------------------------------------------------------Undo flags--------------------------------------------------------//


    //----------------------------------------------------------End user rights ------------------------------------------------------------------------------------------------
    //----------------------------------------------------------Edit Mode --------------------------------------------------------------------------------------------------------
    $rootScope.EditMode = true;
    $rootScope.SelectedNodeID = '0';
    $rootScope.SelectedNodeIDPresist = 0;
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    $scope.hideFamilyEditor = function () {
        $('#familyMainEditor').hide();
    };
    $rootScope.treeviewheight = '';
    $rootScope.inverted = '';
    $scope.sample = 1;
    $scope.SelectedItem = 0;
    $rootScope.selecetedCatalogId = 0;
    $rootScope.selecetedCategoryId = "0";
    $rootScope.selecetedParentCategoryId = "0";
    $rootScope.selecetedFamilyId = "0";
    $rootScope.FamilyCATEGORY_ID = "";
    $scope.columnsForAtt = [];
    $scope.item = "ITEM#__OBJ__1__1__false__true";
    $scope.prodData = [];
    $scope.SelectedDataItem = { VERSION: '1', DESCRIPTION: '', CATALOG_NAME: '', CATALOG_ID: 0, CATEGORY_ID: 0, ID: 0, SORT_ORDER: 0 };
    $scope.IdsForAnotherCatalog = '';
    $scope.PasteFlag = '0';
    $scope.RemoveFlag = '';
    $scope.FlagforDelete = '';
    $scope.Showmasterids = '';
    $scope.Showmasteruncheckids = '';
    $scope.Coordinates = { pageX: 0, pageY: 0 };
    $rootScope.ProductSKUCount = '0';
    $rootScope.ProductCount = 0;
    $rootScope.currentUser = [];
    $scope.FamilyNameForClone = '';
    $("#newProductBtn").hide();
    $("#newProductBtn1").hide();
    $("#newProductBtn").css("display", "none");
    $("#newProductBtn1").css("display", "none");
    $("#renamearea").hide();
    $("#searchLeftNavi").hide();
    $("#custom-menu1").hide();
    $("#custom-menuFam").hide();
    $rootScope.searchcss = 'hide';
    $rootScope.getSelectedNode = "";
    $scope.refreshTree = function () {

        i = 0;
        $scope.RefreshLeftNavTree();
    }
    $scope.RefreshLeftNavTree = function () {

        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $rootScope.treeData.read();
        $('#PasteCategoryFamily').hide();
        $('#PasteCategory').hide();
        $('#PasteRootCategory').hide();
        $('#PasteFamily').hide();
        $scope.FamilyNameForClone = '';
        $('#CloneFamily').hide();
        $('#CloneCategory').hide();
        $('#CloneasRooCategory').hide();
        // $('#PasteCategory').show();
        $('#copycategory').show();
        // $('#CloneFamily').show();
        //$('#PasteRootCategory').show();
        //$('#CloneCategory').show();
        //$('#CloneasRooCategory').show();
        $scope.RightClickedCategoryId = '';
        $rootScope.SelectedNodeID = '';
    };
    $scope.collapseData = function (e) {

        if ($rootScope.SelectedNodeIDPresist == 0) {
            var selectedNode = e.node.getElementsByTagName('a')[0].id;
            $rootScope.SelectedNodeID = $rootScope.SelectedNodeID.replace(selectedNode, '');
        }

    }

    $rootScope.expandNode = 0;
    $scope.expandData = function (e) {

        if ($rootScope.expandNode == 0) {
            var selectedNode = e.node.getElementsByTagName('a')[0].id;
            if ($rootScope.SelectedNodeID == "") {
                $rootScope.SelectedNodeID = selectedNode;
            }
            else {
                $rootScope.SelectedNodeID = $rootScope.SelectedNodeID + "~" + selectedNode;
            }
            $rootScope.defaultSelection = false;
        }

    }
    $rootScope.familyDeleteNav = 0;

    $rootScope.familyDeleteNavStatus = false;
    var i = 0;
    $scope.ChkNav = 0; $scope.categorycnt = true;
    $rootScope.rootValue = true;
    $rootScope.treeData = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        autoBind: false,
        transport: {
            read: function (options) {
                if ($location.$$absUrl.contains("App") && $location.$$absUrl.split("App")[1] == '/Inverted') {
                    return true;
                }
                else if ($location.$$absUrl.contains("App") && $location.$$absUrl.split("App")[1] == '/Search') {
                    return true;
                }
                if ($rootScope.selecetedCatalogId != undefined && $location.$$absUrl.contains("WebSync")) {
                    $scope.SelectedCatalogId = $rootScope.selecetedCatalogId;
                }
                if ($location.$$absUrl.contains("DataCompleteness")) {
                    dataFactory.prodCountForTree($scope.SelectedCatalogId, options.data.id, $localStorage.getCatalogID).success(function (response) {
                        $rootScope.expandNode = 1;
                        blockUI.start();
                        options.success(response);
                        if ($location.$$absUrl.contains("WebSync")) {
                            blockUI.stop();
                            return true;
                        }

                        if ($rootScope.familyDeleteNavStatus == true && ($rootScope.SelectedNodeID == "" || $rootScope.SelectedNodeID == 0)) {
                            i = 0;
                        }
                        if (i == 0) {

                            if ($rootScope.treeData._data.length > 0) {
                                $rootScope.defaultSelection = true;

                                // $rootScope.clickSelectedItem($rootScope.treeData._data[0]);
                                $rootScope.getSelectedNode = $rootScope.treeData._data[0].CATEGORY_ID;
                                i = 1;
                                $scope.ChkNav = 1;
                                $scope.categorycnt = true;
                            } else {
                                var tabstrips = $("#tabstrip").data("kendoTabStrip");
                                var myTabs = tabstrips.tabGroup.children("li").eq(0);
                                tabstrips.select(myTabs);
                                var myTabsfamily = tabstrips.tabGroup.children("li").eq(1);
                                tabstrips.disable(myTabsfamily);
                                var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
                                tabstrips.disable(myTabsproducts);
                                $rootScope.clickSelectedemptyItem();
                                $scope.ChkNav = 0;
                            }
                        }
                        else {
                            if ($rootScope.rootValue == true && options.data.id != undefined) {

                                $rootScope.SelectedNodeID = options.data.id;
                                $rootScope.rootValue = false;
                            }
                        }
                        if ($rootScope.displayCloneFamilies == true) {
                            $rootScope.ngclkfamilyname("", $rootScope.cloneOptions);

                        }
                        else if ($rootScope.SelectedNodeID != 0 && $rootScope.SelectedNodeIDPresist != 0 && $rootScope.SelectedNodeID != "") {

                            var treeView = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView');
                            var idList = [];
                            var listCat = $rootScope.SelectedNodeID.split('~!')[1];
                            var listFam = $rootScope.SelectedNodeID.split('~!')[0];
                            var selectedId = listCat + "~" + listFam;
                            if (listCat == undefined || listCat == "undefined") {
                                selectedId = listFam;
                            }

                            if ($rootScope.SelectedNodeID.split('~!').length == 3) {
                                selectedId = $rootScope.SelectedNodeID.split('~!')[2] + "~" + $rootScope.SelectedNodeID.split('~!')[0] + $rootScope.SelectedNodeID.split('~!')[1];
                            }
                            angular.forEach(selectedId.split('~'), function (value) {
                                + '~' + $rootScope.SelectedNodeID.split('!~')[0].trim('~')
                                if (value != "" && value != null) {
                                    idList.push(value)
                                }
                            });
                            if ($rootScope.treeForCategory == true) {
                                idList.push("");
                            }
                            var lastNode;
                            angular.forEach($rootScope.treeData._data, function (nodes) {

                                for (var i = 0; i < idList.length; i++) {
                                    if (treeView != null && nodes != null && nodes.id == idList[i]) {
                                        var node = treeView.findByUid(nodes.uid);
                                        treeView.expand(node);
                                        lastNode = nodes;
                                    }
                                }
                                angular.forEach(nodes.children._data, function (firstChildnodes) {

                                    for (var i = 0; i < idList.length; i++) {
                                        if (treeView != null && firstChildnodes != null && firstChildnodes.id == idList[i]) {
                                            var node = treeView.findByUid(firstChildnodes.uid);
                                            treeView.expand(node);
                                            lastNode = firstChildnodes;
                                        }
                                        else if (treeView != null && firstChildnodes != null && firstChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                            var node = treeView.findByUid(firstChildnodes.uid);
                                            treeView.expand(node);
                                            lastNode = firstChildnodes;
                                        }
                                    }
                                    if (treeView != null && firstChildnodes != null && firstChildnodes.id == $rootScope.selectedFAmilyList) {
                                        var firstChildnode = treeView.findByUid(firstChildnodes.uid);
                                        firstChildnode.children().children()[firstChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                        lastNode = "";
                                        $rootScope.SelectedNodeIDPresist = 0;
                                        $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                    }

                                    angular.forEach(firstChildnodes.children._data, function (secondChildnodes) {
                                        for (var i = 0; i < idList.length; i++) {
                                            if (treeView != null && secondChildnodes != null && secondChildnodes.id == idList[i]) {
                                                var node = treeView.findByUid(secondChildnodes.uid);
                                                treeView.expand(node);
                                                lastNode = secondChildnodes;
                                            }
                                            else if (treeView != null && secondChildnodes != null && secondChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                                var node = treeView.findByUid(secondChildnodes.uid);
                                                treeView.expand(node);
                                                lastNode = secondChildnodes;
                                            }
                                        }
                                        if (treeView != null && secondChildnodes != null && secondChildnodes.id == $rootScope.selectedFAmilyList) {
                                            var secondChildnode = treeView.findByUid(secondChildnodes.uid);
                                            secondChildnode.children().children()[secondChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                            lastNode = "";
                                            $rootScope.SelectedNodeIDPresist = 0;
                                            $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                            $rootScope.pasteProductNav = false;
                                        }

                                        angular.forEach(secondChildnodes.children._data, function (thirdChildnodes) {
                                            for (var i = 0; i < idList.length; i++) {
                                                if (treeView != null && thirdChildnodes != null && thirdChildnodes.id == idList[i]) {
                                                    var node = treeView.findByUid(thirdChildnodes.uid);
                                                    treeView.expand(node);
                                                    lastNode = thirdChildnodes;
                                                }
                                                else if (treeView != null && thirdChildnodes != null && thirdChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                                    var node = treeView.findByUid(thirdChildnodes.uid);
                                                    treeView.expand(node);
                                                    lastNode = thirdChildnodes;
                                                }
                                            }
                                            if (treeView != null && thirdChildnodes != null && thirdChildnodes.id == $rootScope.selectedFAmilyList) {
                                                var thirdChildnode = treeView.findByUid(thirdChildnodes.uid);
                                                thirdChildnode.children().children()[thirdChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                lastNode = "";
                                                $rootScope.SelectedNodeIDPresist = 0;
                                                $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                                $rootScope.pasteProductNav = false;
                                            }

                                            angular.forEach(secondChildnodes.children._data, function (forthChildnodes) {
                                                for (var i = 0; i < idList.length; i++) {
                                                    if (treeView != null && forthChildnodes != null && forthChildnodes.id == idList[i]) {
                                                        var node = treeView.findByUid(forthChildnodes.uid);
                                                        treeView.expand(node);
                                                        lastNode = forthChildnodes;
                                                    }
                                                    else if (treeView != null && forthChildnodes != null && forthChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                                        var node = treeView.findByUid(forthChildnodes.uid);
                                                        treeView.expand(node);
                                                        lastNode = forthChildnodes;
                                                    }
                                                }
                                                if (treeView != null && forthChildnodes != null && forthChildnodes.id == $rootScope.selectedFAmilyList) {
                                                    var forthChildnode = treeView.findByUid(forthChildnodes.uid);
                                                    forthChildnode.children().children()[forthChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                    lastNode = "";
                                                    $rootScope.SelectedNodeIDPresist = 0;
                                                    $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                                    $rootScope.pasteProductNav = false;
                                                }

                                                angular.forEach(forthChildnodes.children._data, function (famChildnodes) {
                                                    for (var i = 0; i < idList.length; i++) {
                                                        if (treeView != null && famChildnodes != null && famChildnodes.id == idList[i]) {
                                                            var node = treeView.findByUid(famChildnodes.uid);
                                                            treeView.expand(node);
                                                            lastNode = famChildnodes;
                                                        }
                                                        else if (treeView != null && famChildnodes != null && famChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                                            var node = treeView.findByUid(famChildnodes.uid);
                                                            treeView.expand(node);
                                                            lastNode = famChildnodes;
                                                        }
                                                    }
                                                    if (treeView != null && famChildnodes != null && famChildnodes.id == $rootScope.selectedFAmilyList) {
                                                        var famChildnode = treeView.findByUid(famChildnodes.uid);
                                                        famChildnode.children().children()[famChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                        lastNode = "";
                                                        $rootScope.SelectedNodeIDPresist = 0;
                                                        $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                                        $rootScope.pasteProductNav = false;
                                                    }

                                                    angular.forEach(famChildnodes.children._data, function (subfamChildnodes) {
                                                        for (var i = 0; i < idList.length; i++) {
                                                            if (treeView != null && subfamChildnodes != null && subfamChildnodes.id == idList[i]) {
                                                                var node = treeView.findByUid(subfamChildnodes.uid);
                                                                treeView.expand(node);
                                                                lastNode = subfamChildnodes;
                                                            }
                                                            else if (treeView != null && subfamChildnodes != null && subfamChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                                                var node = treeView.findByUid(subfamChildnodes.uid);
                                                                treeView.expand(node);
                                                                lastNode = subfamChildnodes;
                                                            }
                                                        }
                                                        if (treeView != null && subfamChildnodes != null && subfamChildnodes.id == $rootScope.selectedFAmilyList) {
                                                            var subfamChildnode = treeView.findByUid(subfamChildnodes.uid);
                                                            subfamChildnode.children().children()[subfamChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                            lastNode = "";
                                                            $rootScope.SelectedNodeIDPresist = 0;
                                                            $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                                            $rootScope.pasteProductNav = false;
                                                        }

                                                    });
                                                });

                                            });
                                        });
                                    });
                                });

                            });

                            if (lastNode != "" && lastNode != undefined && $rootScope.familyDeleteNavStatus == true) {

                                if ($rootScope.familyDeleteNav == lastNode.id) {
                                    $rootScope.clickSelectedItem(lastNode);
                                    $rootScope.getSelectedNode = lastNode.CATEGORY_ID;
                                    $scope.ChkNav = 1;
                                    $scope.categorycnt = true;
                                    var node = treeView.findByUid(lastNode.uid);
                                    //node.children().children()[2].className = "k-in k-state-selected";
                                    $rootScope.SelectedNodeIDPresist = 0;
                                }
                            }
                            $rootScope.expandNode = 0;


                        } else {


                            if (options.data.id != undefined) {
                                if ($rootScope.SelectedNodeID != "" && !$rootScope.SelectedNodeID.includes(options.data.id) && !options.data.id.includes("~!") && !$rootScope.SelectedNodeID.includes("~!")) {
                                    $rootScope.SelectedNodeID = $rootScope.SelectedNodeID + "~" + options.data.id;
                                }
                            }

                        }
                        blockUI.stop();
                        $rootScope.expandNode = 0;
                    }).error(function (response) {
                        options.success(response);
                    });
                }
                dataFactory.getAllCategories($scope.SelectedCatalogId, options.data.id, $localStorage.getCatalogID).success(function (response) {
                    $rootScope.expandNode = 1;
                    blockUI.start();
                    options.success(response);
                    if ($location.$$absUrl.contains("WebSync")) {
                        blockUI.stop();
                        return true;
                    }

                    if ($rootScope.familyDeleteNavStatus == true && ($rootScope.SelectedNodeID == "" || $rootScope.SelectedNodeID == 0)) {
                        i = 0;
                    }
                    if (i == 0) {

                        if ($rootScope.treeData._data.length > 0) {
                            $rootScope.defaultSelection = true;
                            //Removed the // from blow line
                            $rootScope.clickSelectedItem($rootScope.treeData._data[0]);
                            $rootScope.getSelectedNode = $rootScope.treeData._data[0].CATEGORY_ID;
                            i = 1;
                            $scope.ChkNav = 1;
                            $scope.categorycnt = true;
                        } else {
                            var tabstrips = $("#tabstrip").data("kendoTabStrip");
                            var myTabs = tabstrips.tabGroup.children("li").eq(0);
                            tabstrips.select(myTabs);
                            var myTabsfamily = tabstrips.tabGroup.children("li").eq(1);
                            tabstrips.disable(myTabsfamily);
                            var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
                            tabstrips.disable(myTabsproducts);
                            $rootScope.clickSelectedemptyItem();
                            $scope.ChkNav = 0;
                        }
                    }
                    else {
                        if ($rootScope.rootValue == true && options.data.id != undefined) {

                            $rootScope.SelectedNodeID = options.data.id;
                            $rootScope.rootValue = false;
                        }
                    }
                    if ($rootScope.displayCloneFamilies == true) {
                        $rootScope.ngclkfamilyname("", $rootScope.cloneOptions);

                    }
                    else if ($rootScope.SelectedNodeID != 0 && $rootScope.SelectedNodeIDPresist != 0 && $rootScope.SelectedNodeID != "") {

                        var treeView = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView');
                        var idList = [];
                        var listCat = $rootScope.SelectedNodeID.split('~!')[1];
                        var listFam = $rootScope.SelectedNodeID.split('~!')[0];
                        var selectedId = listCat + "~" + listFam;
                        if (listCat == undefined || listCat == "undefined") {
                            selectedId = listFam;
                        }

                        if ($rootScope.SelectedNodeID.split('~!').length == 3) {
                            selectedId = $rootScope.SelectedNodeID.split('~!')[2] + "~" + $rootScope.SelectedNodeID.split('~!')[0] + $rootScope.SelectedNodeID.split('~!')[1];
                        }
                        angular.forEach(selectedId.split('~'), function (value) {
                            + '~' + $rootScope.SelectedNodeID.split('!~')[0].trim('~')
                            if (value != "" && value != null) {
                                idList.push(value)
                            }
                        });
                        if ($rootScope.treeForCategory == true) {
                            idList.push("");
                        }
                        var lastNode;
                        angular.forEach($rootScope.treeData._data, function (nodes) {

                            for (var i = 0; i < idList.length; i++) {
                                if (treeView != null && nodes != null && nodes.id == idList[i]) {
                                    var node = treeView.findByUid(nodes.uid);
                                    treeView.expand(node);
                                    lastNode = nodes;
                                }
                            }
                            angular.forEach(nodes.children._data, function (firstChildnodes) {

                                for (var i = 0; i < idList.length; i++) {
                                    if (treeView != null && firstChildnodes != null && firstChildnodes.id == idList[i]) {
                                        var node = treeView.findByUid(firstChildnodes.uid);
                                        treeView.expand(node);
                                        lastNode = firstChildnodes;
                                    }
                                    else if (treeView != null && firstChildnodes != null && firstChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                        var node = treeView.findByUid(firstChildnodes.uid);
                                        treeView.expand(node);
                                        lastNode = firstChildnodes;
                                    }
                                }
                                if (treeView != null && firstChildnodes != null && firstChildnodes.id == $rootScope.selectedFAmilyList) {
                                    var firstChildnode = treeView.findByUid(firstChildnodes.uid);
                                    firstChildnode.children().children()[firstChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                    lastNode = "";
                                    $rootScope.SelectedNodeIDPresist = 0;
                                    $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                }

                                angular.forEach(firstChildnodes.children._data, function (secondChildnodes) {
                                    for (var i = 0; i < idList.length; i++) {
                                        if (treeView != null && secondChildnodes != null && secondChildnodes.id == idList[i]) {
                                            var node = treeView.findByUid(secondChildnodes.uid);
                                            treeView.expand(node);
                                            lastNode = secondChildnodes;
                                        }
                                        else if (treeView != null && secondChildnodes != null && secondChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                            var node = treeView.findByUid(secondChildnodes.uid);
                                            treeView.expand(node);
                                            lastNode = secondChildnodes;
                                        }
                                    }
                                    if (treeView != null && secondChildnodes != null && secondChildnodes.id == $rootScope.selectedFAmilyList) {
                                        var secondChildnode = treeView.findByUid(secondChildnodes.uid);
                                        secondChildnode.children().children()[secondChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                        lastNode = "";
                                        $rootScope.SelectedNodeIDPresist = 0;
                                        $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                        $rootScope.pasteProductNav = false;
                                    }

                                    angular.forEach(secondChildnodes.children._data, function (thirdChildnodes) {
                                        for (var i = 0; i < idList.length; i++) {
                                            if (treeView != null && thirdChildnodes != null && thirdChildnodes.id == idList[i]) {
                                                var node = treeView.findByUid(thirdChildnodes.uid);
                                                treeView.expand(node);
                                                lastNode = thirdChildnodes;
                                            }
                                            else if (treeView != null && thirdChildnodes != null && thirdChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                                var node = treeView.findByUid(thirdChildnodes.uid);
                                                treeView.expand(node);
                                                lastNode = thirdChildnodes;
                                            }
                                        }
                                        if (treeView != null && thirdChildnodes != null && thirdChildnodes.id == $rootScope.selectedFAmilyList) {
                                            var thirdChildnode = treeView.findByUid(thirdChildnodes.uid);
                                            thirdChildnode.children().children()[thirdChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                            lastNode = "";
                                            $rootScope.SelectedNodeIDPresist = 0;
                                            $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                            $rootScope.pasteProductNav = false;
                                        }

                                        angular.forEach(secondChildnodes.children._data, function (forthChildnodes) {
                                            for (var i = 0; i < idList.length; i++) {
                                                if (treeView != null && forthChildnodes != null && forthChildnodes.id == idList[i]) {
                                                    var node = treeView.findByUid(forthChildnodes.uid);
                                                    treeView.expand(node);
                                                    lastNode = forthChildnodes;
                                                }
                                                else if (treeView != null && forthChildnodes != null && forthChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                                    var node = treeView.findByUid(forthChildnodes.uid);
                                                    treeView.expand(node);
                                                    lastNode = forthChildnodes;
                                                }
                                            }
                                            if (treeView != null && forthChildnodes != null && forthChildnodes.id == $rootScope.selectedFAmilyList) {
                                                var forthChildnode = treeView.findByUid(forthChildnodes.uid);
                                                forthChildnode.children().children()[forthChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                lastNode = "";
                                                $rootScope.SelectedNodeIDPresist = 0;
                                                $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                                $rootScope.pasteProductNav = false;
                                            }

                                            angular.forEach(forthChildnodes.children._data, function (famChildnodes) {
                                                for (var i = 0; i < idList.length; i++) {
                                                    if (treeView != null && famChildnodes != null && famChildnodes.id == idList[i]) {
                                                        var node = treeView.findByUid(famChildnodes.uid);
                                                        treeView.expand(node);
                                                        lastNode = famChildnodes;
                                                    }
                                                    else if (treeView != null && famChildnodes != null && famChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                                        var node = treeView.findByUid(famChildnodes.uid);
                                                        treeView.expand(node);
                                                        lastNode = famChildnodes;
                                                    }
                                                }
                                                if (treeView != null && famChildnodes != null && famChildnodes.id == $rootScope.selectedFAmilyList) {
                                                    var famChildnode = treeView.findByUid(famChildnodes.uid);
                                                    famChildnode.children().children()[famChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                    lastNode = "";
                                                    $rootScope.SelectedNodeIDPresist = 0;
                                                    $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                                    $rootScope.pasteProductNav = false;
                                                }

                                                angular.forEach(famChildnodes.children._data, function (subfamChildnodes) {
                                                    for (var i = 0; i < idList.length; i++) {
                                                        if (treeView != null && subfamChildnodes != null && subfamChildnodes.id == idList[i]) {
                                                            var node = treeView.findByUid(subfamChildnodes.uid);
                                                            treeView.expand(node);
                                                            lastNode = subfamChildnodes;
                                                        }
                                                        else if (treeView != null && subfamChildnodes != null && subfamChildnodes.CATEGORY_ID.replace('~', '') == idList[i]) {
                                                            var node = treeView.findByUid(subfamChildnodes.uid);
                                                            treeView.expand(node);
                                                            lastNode = subfamChildnodes;
                                                        }
                                                    }
                                                    if (treeView != null && subfamChildnodes != null && subfamChildnodes.id == $rootScope.selectedFAmilyList) {
                                                        var subfamChildnode = treeView.findByUid(subfamChildnodes.uid);
                                                        subfamChildnode.children().children()[subfamChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                        lastNode = "";
                                                        $rootScope.SelectedNodeIDPresist = 0;
                                                        $rootScope.SelectedNodeID = $rootScope.selectedFAmilyList;
                                                        $rootScope.pasteProductNav = false;
                                                    }

                                                });
                                            });

                                        });
                                    });
                                });
                            });

                        });

                        if (lastNode != "" && lastNode != undefined && $rootScope.familyDeleteNavStatus == true) {

                            if ($rootScope.familyDeleteNav == lastNode.id) {
                                $rootScope.clickSelectedItem(lastNode);
                                $rootScope.getSelectedNode = lastNode.CATEGORY_ID;
                                $scope.ChkNav = 1;
                                $scope.categorycnt = true;
                                var node = treeView.findByUid(lastNode.uid);
                                //node.children().children()[2].className = "k-in k-state-selected";
                                $rootScope.SelectedNodeIDPresist = 0;
                            }
                        }
                        $rootScope.expandNode = 0;


                    } else {


                        if (options.data.id != undefined) {
                            if ($rootScope.SelectedNodeID != "" && !$rootScope.SelectedNodeID.includes(options.data.id) && !options.data.id.includes("~!") && !$rootScope.SelectedNodeID.includes("~!")) {
                                $rootScope.SelectedNodeID = $rootScope.SelectedNodeID + "~" + options.data.id;
                            }
                        }

                    }
                    blockUI.stop();
                    $rootScope.expandNode = 0;
                }).error(function (response) {
                    options.success(response);
                });
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        requestEnd: function (e) {
            // var response = e.response;
            // var type = e.type;
            // alert(type); // displays "read"
            // alert(response.length); // displays "77"
            // $rootScope.previoustate();
        }
    });

    $("#dialog-confirm").hide();

    //$("#dialog-confirm1").hide();
    $("#dialog-confirm2").hide();
    $("#dialog-confirm6").hide();
    $("#dialog-confirm7").hide();
    //$("#dialog-confirm3").hide();
    $("#dialog-confirm4").hide();
    $("#dialog-confirm10").hide();
    $("#dialog-confirm11").hide();
    $scope.init = function () {

        $("#dialog-confirm").hide();
        $("#dialog-confirm10").hide();
        $("#dialog-confirm11").hide();
        // $("#dialog-confirm1").hide();
        $("#dialog-confirm2").hide();
        $("#dialog-confirm6").hide();
        //  $("#dialog-confirm3").hide();
        $("#dialog-confirm4").hide();
        $("#dialog-confirm8").hide();
        $scope.names = dataFactory.GetdropdownGroupName($scope.SelectedCatalogId, "Family").success(function (response) {
            options.success(response);
        });


    };

    $scope.catalogDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetCurretUserInfo().success(function (response) {
                    $rootScope.currentUser = response;
                    $localStorage.getCustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
                    $localStorage.getUserName = $rootScope.currentUser.Username;

                    $localStorage.getCustomerCompanyName = $rootScope.currentUser.CustomerDetails.Comments;
                    dataFactory.GetCatalogDetailsForCurrentCustomer($scope.SelectedItem, $rootScope.currentUser.CustomerDetails.CustomerId).success(function (cResponse) {
                        options.success(cResponse);
                        var catalogExists = false;
                        angular.forEach(cResponse, function (data) {
                            if (data.CATALOG_ID == $localStorage.getCatalogID)
                                catalogExists = true;
                        });
                        if (catalogExists == false) {
                            $localStorage.getCatalogID = undefined;
                        }
                        if ($localStorage.getCatalogID === undefined) {
                            $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                            $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                            $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                            $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                            $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                        }
                        if ($scope.SelectedCatalogId == '') {
                            $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                            $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                            $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                            $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                            $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                        }
                        var catalogchk = 0;
                        angular.forEach(cResponse, function (value) {
                            if (value.CATALOG_ID.toString() === $rootScope.selecetedCatalogId.toString()) {
                                catalogchk = 1;
                            }

                        });
                        if (catalogchk === 0) {
                            $rootScope.selecetedCatalogId = 1;
                            $localStorage.getCatalogName = "Master Catalog";
                            $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
                            $scope.SelectedCatalogId = 1;
                            $scope.catalogDataSource.read();
                            $rootScope.treeData._data = [];
                            i = 0;
                            $rootScope.treeData.read();
                        } else {
                            //alert("2");
                            // $rootScope.treeData.read();
                        }
                        blockUI.stop();
                    }).error(function (error) {
                        options.error(error);
                    });
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    if ($localStorage.getCatalogID === undefined) {

        $localStorage.getCatalogID = 0;

    }

    $rootScope.selectedRowDynamicPickList = [];
    dataFactory.GetAllPickListData($localStorage.getCatalogID).success(function (response) {

        if (response != null) {
            var GetAllPickListData = response;
            for (var i = 0; i < GetAllPickListData.length; i++) {
                $scope.GetPickListData(GetAllPickListData[i].ATTRIBUTE_ID, GetAllPickListData[i].PICKLIST_NAME)
            }

        }
    });
    $scope.GetPickListData = function (attrName, picklistdata) {
        dataFactory.getPickListData(picklistdata).success(function (response) {
            $rootScope.selectedRowDynamicPickList[attrName] = response;
            //$scope.iterationPicklistCount++;
            //if ($scope.iterationPicklistCount == $scope.iterationCount) {
            //    //$rootScope.windows_Open();
            //    $timeout(function () {

            //        $scope.selectedRow = {};
            //        angular.copy($scope.updateproductspecs, $scope.selectedRow);
            //        blockUI.stop();
            //        $scope.isUIReleased = 1;
            //    }, 200);


            //}
        }).error(function (error) {
            options.error(error);
        });
    };
    $scope.leftNavExpand = function () {

        $("#leftNavTreeViewKendoNavigator").data("kendoTreeView").expand(".k-item");

        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
    };


    $(function () {
        setNavigation();
    });

    function setNavigation() {

        var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
        if (pgurl == "Catalog" || pgurl == "AdminDashboard" || pgurl == "Attribute" || pgurl == "Picklist" || pgurl == "Reference" || pgurl == "WorkFlow" || pgurl == "Inverted" || pgurl == "InvertedSub" || pgurl == "Supplier") {
            $('#cmli').addClass("active");

            if (pgurl == "Catalog")
                $('#catli').addClass("active");
            if (pgurl == "AdminDashboard")
                $('#adli').addClass("active");
            if (pgurl == "Attribute")
                $('#atli').addClass("active");
            if (pgurl == "Picklist")
                $('#picli').addClass("active");
            if (pgurl == "Reference")
                $('#refli').addClass("active");
            if (pgurl == "WorkFlow")
                $('#workli').addClass("active");
            if (pgurl == "Inverted")
                $('#invli').addClass("active");
            if (pgurl == "InvertedSub")
                $('#invlisub').addClass("active");
            if (pgurl == "Supplier")
                $('#supli').addClass("active");
        }
        else if (pgurl == "" || pgurl == "Dashboard") {
            $('#dbli').addClass("active");
        }
        else if (pgurl == "ImportType" || pgurl == "Export" || pgurl == "XpressCatalog" || pgurl == "Indesign" || pgurl == "RemoveProject") {
            $('#publi').addClass("active");
            if (pgurl == "ImportType")
                $('#imli').addClass("active");
            if (pgurl == "Export")
                $('#exli').addClass("active");
            if (pgurl == "XpressCatalog")
                $('#xpli').addClass("active");
            if (pgurl == "Indesign")
                $('#inli').addClass("active");
            if (pgurl == "RemoveProject")
                $('#remli').addClass("active");
        }
        else if (pgurl == "GlobalAttributemanager" || pgurl == "ProductTableManager") {
            $('#wizli').addClass("active");
            if (pgurl == "GlobalAttributemanager")
                $('#gamli').addClass("active");
            if (pgurl == "ProductTableManager")
                $('#ptmli').addClass("active");
        }
        else if (pgurl == "Reports" || pgurl == "ReportLog" || pgurl == "Summary") {
            $('#repli').addClass("active");
            if (pgurl == "Reports")
                $('#reportsli').addClass("active");
            if (pgurl == "ReportLog")
                $('#reportslogli').addClass("active");
            if (pgurl == "Summary")
                $('#summaryli').addClass("active");
        }
        else if (pgurl == "Search") {
            $('#searchli').addClass("active");
            $('#searchli').addClass("textdisplay");

        }
        else if (pgurl == "ImageManagement/Manage") {
            $('#imagemanageli').addClass("active");
            $('#imagemanageli').addClass("textdisplay");

        }
        else if (pgurl == "UserAdmin" || pgurl == "AdminConfig" || pgurl == "Preference") {
            $('#caretli').addClass("active");

            if (pgurl == "UserAdmin")
                $('#useradli').addClass("active");
            if (pgurl == "AdminConfig")
                $('#acli').addClass("active");
            if (pgurl == "Preference")
                $('#prefli').addClass("active");

        }
        else if (pgurl == "RecycleBin") {
            $('#recyclebinmenu').addClass("active");
        }
    }

    //Category RemovFrom Navigator
    $scope.RemoveFromNavigator = function () {

        $scope.associationRemove = "Association";
        $scope.FlagforDelete = 'category';
        $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
        $scope.RemoveFlag = '1';
        //////$("#dialog-confirm3").show();
        //////$("#dialog-confirm3").dialog({
        //////    resizable: false,
        //////    modal: true,
        //////    buttons: {
        //////        "Yes": function () {

        //////            if ($scope.RemoveFlag !== '') {
        //////                if ($scope.FlagforDelete === "category" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId !== "" && !$scope.RightClickedCategoryId.contains('~')) {
        //////                    $scope.Cancel();
        //////                    dataFactory.CategoryremoveFromNavigator($scope.RightClickedCategoryId, $scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag).success(function (response) {
        //////                        $.msgBox({
        //////                            title: $localStorage.ProdcutTitle,
        //////                            content: '' + response + '',
        //////                            type: "info"
        //////                        });


        //////                        $scope.RefreshLeftNavTree();
        //////                        $scope.RemoveFlag = '0';
        //////                        $scope.RightClickedCategoryId = '';
        //////                        $scope.associationRemove = "Association";
        //////                    }).error(function (error) {
        //////                        options.error(error);
        //////                    });
        //////                }
        //////            }
        //////            $(this).dialog("close");
        //////        },
        //////        "No": function () {

        //////            $(this).dialog("close");
        //////        }
        //////    }

        //////});


        // $("#dialog-confirm3").hide();
        // $scope.Remove.center().open();
    };
    //$scope.Delete = function () {


    //    if ($scope.RemoveFlag !== '') {
    //        if ($scope.FlagforDelete === "category" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId !== "" && !$scope.RightClickedCategoryId.contains('~')) {
    //            $scope.Cancel();
    //            dataFactory.CategoryremoveFromNavigator($scope.RightClickedCategoryId, $scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag).success(function (response) {

    //                alert(response);
    //                $scope.RefreshLeftNavTree();
    //                $scope.RemoveFlag = '0';
    //                $scope.RightClickedCategoryId = '';
    //               $scope.associationRemove = "Association";
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //        else if ($scope.FlagforDelete === "family" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId.contains('~')) {

    //            $scope.Cancel();
    //            dataFactory.FamilyremoveFromNavigator($scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag, $scope.RightClickedCategoryId).success(function (response) {

    //                alert(response);
    //                $scope.RefreshLeftNavTree();
    //                $scope.RemoveFlag = '0';
    //                $scope.RightClickedCategoryId = '';
    //                $scope.associationRemove = "Association";
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //        else {

    //            $scope.RemoveFlag = '0';
    //            $scope.associationRemove = "Association";
    //            $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
    //            $scope.Remove.center().close();
    //        }
    //    }
    //    else {
    //        $scope.RemoveFlag = '0';
    //        $scope.associationRemove = "Association";
    //        alert("Please Choose the Option.");
    //    }
    //};

    $scope.DeleteFromClone = function () {
        $scope.CloneAlert.refresh({ url: "../views/app/partials/CloneCheckOption.html" });
        $scope.CloneAlert.center().close();
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "This action will remove the original product along with the all clones, do you want to continue?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    $scope.FlagforDelete = 'family';
                    $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
                    $scope.RemoveFlag = '0';
                    $scope.Remove.center().open();
                }
            }
        });

    };
    $scope.AssignMaster = function () {
        $scope.originalCategoryDatasourceShowMaster.read();
        $scope.clonedCategoryDatasourceShowMaster.read();
        $scope.ShowMaster.refresh({ url: "../views/app/partials/SwitchMaster.html" });
        $scope.ShowMaster.center().open();
        $scope.CloneAlert.refresh({ url: "../views/app/partials/CloneCheckOption.html" });
        $scope.CloneAlert.center().close();
    };

    $scope.originalCategoryDatasourceShowMaster = new kendo.data.DataSource({
        type: "json",
        transport: {
            read: function (options) {
                dataFactory.GetOriginalCategory($scope.RightClickedCategoryId, $scope.SelectedDataItem.CATALOG_ID, 'ORIGINAL', options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        }, schema: {
            model: {
                fields: {
                    CATEGORY_SHORT: { editable: false }, CATALOG_NAME: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    CATEGORY_LEVEL: { editable: false }

                }
            }
        }
    });



    $scope.orginalCategoryGridOptionsShowMaster = {
        dataSource: $scope.originalCategoryDatasourceShowMaster,
        autoBind: false, selectable: true,
        columns: [//{ field: "FAMILY_NAME", title: " ", width: "80px", template: "<a title='FamilyName'  class='k-link' href='javascript:void(0);'   ng-click='ngclkfamilyname(this)'>Show</a>" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
            { field: "CATEGORY_SHORT", title: "Category ID", width: "180px" },
            { field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
            { field: "CATEGORY_LEVEL", title: "Category Level", width: "180px" }],
        editable: false
    };


    $scope.clonedCategoryDatasourceShowMaster = new kendo.data.DataSource({
        type: "json",
        transport: {
            read: function (options) {
                dataFactory.GetOriginalCategory($scope.RightClickedCategoryId, $scope.SelectedDataItem.CATALOG_ID, 'CLONE', options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        }, schema: {
            model: {
                fields: {
                    CATEGORY_SHORT: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    CATEGORY_LEVEL: { editable: false }

                }
            }
        }
    });



    $scope.clonedCategoryGridOptionsShowMaster = {
        dataSource: $scope.clonedCategoryDatasourceShowMaster,
        autoBind: false, selectable: true,
        columns: [//{ field: "FAMILY_NAME", title: " ", width: "80px", template: "<a title='FamilyName'  class='k-link' href='javascript:void(0);'   ng-click='ngclkfamilyname(this)'>Show</a>" },
            //{ field: "FAMILY_NAME", title: "Action", width: "80px",template: "<a title='show'  class='k-link' href='javascript:void(0);'   ng-click='ngclkfamilyname(this)'>Show</a>" },
            { field: "FAMILY_NAME", title: "Switch Master", width: "180px", template: "<button class='btn btn-primary' ng-disabled=#=ISSUBFAMILY#  ng-click='ngclkswitchmaster(this)'>Master</button>" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
            { field: "CATEGORY_SHORT", title: "Category ID", width: "180px" },
            { field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
            { field: "CATEGORY_LEVEL", title: "Category Level", width: "180px" }
        ],
        editable: false
    };
    $scope.associationRemove = "Association";
    $scope.RemoveOption = function (e) {

        if (e.currentTarget.value === "Association") {
            $scope.associationRemove = "Association";
            $scope.RemoveFlag = '0';
        }
        else if (e.currentTarget.value === "Delete") {
            $scope.associationRemove = "Delete";
            $scope.RemoveFlag = '1';
        }
        else if (e.currentTarget.value === "Deletepermanent") {
            $scope.associationRemove = "Deletepermanent";
            $scope.RemoveFlag = '2';
        }
        else {
            $scope.associationRemove = "Association";
            $scope.RemoveFlag = '0';
        }
    };
    $scope.ngclkswitchmaster = function (e) {
        $scope.ShowMaster.refresh({ url: "../views/app/partials/SwitchMaster.html" });
        $scope.ShowMaster.center().close();
        var familyid = e.dataItem.FAMILY_ID;
        var catalogid = e.dataItem.CATALOG_ID;
        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }
        $rootScope.familyDeleteNavStatus = true;
        $rootScope.SelectedNodeIDPresist = 1;
        dataFactory.CheckmasterFamily(familyid, $rootScope.selecetedCatalogId, e.dataItem.CATEGORY_ID).success(function (response) {
            if (response !== "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: response,
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel" }],
                    success: function (result) {
                        if (result == "Yes") {
                            dataFactory.SwitchMasterFamily(familyid, catalogid, e.dataItem.CATEGORY_ID).success(function (response1) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response1 + '.',
                                    type: "info"
                                });

                                $rootScope.treeData.read();
                                var tabstrips = $("#tabstrip").data("kendoTabStrip");
                                var myTabs = tabstrips.tabGroup.children("li").eq(0);
                                tabstrips.select(myTabs);

                                var myTabsfamily = tabstrips.tabGroup.children("li").eq(1);
                                tabstrips.disable(myTabsfamily);

                                var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
                                tabstrips.disable(myTabsproducts);
                            }).error(function (error) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + error + '.',
                                    type: "Error"
                                });
                            });
                        }
                    }
                });
            }
        }).error(function (error) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + error + '.',
                type: "error"
            });
        });

    };
    $scope.Cancel = function () {
        $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
        $scope.Remove.center().close();
        $scope.CloneAlert.refresh({ url: "../views/app/partials/CloneCheckOption.html" });
        $scope.CloneAlert.center().close();
    };
    $('#DisplayUserName').ready(function () {
        //if ($('#DisplayUserName')[0].innerHTML.split(',')[1].replace('&nbsp;', '').trim() != "") {
        //    $localStorage.displayUserName = $('#DisplayUserName')[0].innerHTML;
        //}
        //else if ($('#DisplayUserName')[0].innerHTML.split(',')[1].replace('&nbsp;', '').trim() == "") {
        //    $('#DisplayUserName')[0].innerHTML = $localStorage.displayUserName;
        //}
        // new
        if ($('#DisplayUserName')[0].innerHTML.replace('&nbsp;', '').trim() != "") {
            $localStorage.displayUserName = $('#DisplayUserName')[0].innerHTML;
        }
            //else if ($('#DisplayUserName')[0].innerHTML.split(',')[1].replace('&nbsp;', '').trim() == "") {	
        else if ($('#DisplayUserName')[0].innerHTML.replace('&nbsp;', '').trim() == "") {
            $('#DisplayUserName')[0].innerHTML = $localStorage.displayUserName;
        }
        else {
            $('#DisplayUserName')[0].innerHTML = $localStorage.displayUserName;
        }


    });
    //end
    //$scope.RemoveFamily = function () {

    //    $scope.Flag = '0';
    //    $scope.EnableShowMaster = true;
    //    var id = $scope.selectedItem.id.split('~');
    //    var category_id = "0";
    //    if (id.length > 0) {
    //        category_id = id[0].toString();
    //    }
    //    $http.get('../Category/GetClonedFamilyDet?Catalog_Id=' + $rootScope.selecetedCatalogId + '&Family_Id=' + $scope.RightClickedCategoryId + '&Category_Id=' + category_id).then(function (e) {
    //        $scope.Flag = e.data;
    //        if ($scope.Flag !== "1" && $scope.Flag !== "2") {
    //            $scope.FlagforDelete = 'family';
    //            $scope.RemoveFlag = '0';
    //            $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
    //            $scope.Remove.center().open();
    //        }
    //        else {
    //            if ($scope.Flag === "1") {
    //                $scope.EnableShowMaster = true;
    //            }
    //            else {
    //                $scope.EnableShowMaster = false;
    //            }
    //            $scope.CloneAlert.refresh({ url: "../views/app/partials/CloneCheckOption.html" });
    //            $scope.CloneAlert.center().open();

    //        }
    //    });

    //};

    $scope.RemoveFamily = function () {

        $scope.RemoveFlag = '1';
        $scope.Flag = '0';
        $scope.EnableShowMaster = true;
        //  var id = $scope.SelectedItem.id.split('~');
        var id = $scope.SelectedItem.split('~');
        var category_id = "0";
        if (id.length > 0) {
            category_id = id[0].toString();
        }
        //$http.get('../Category/GetClonedFamilyDet?Catalog_Id=' + $rootScope.selecetedCatalogId + '&Family_Id=' + $scope.RightClickedCategoryId + '&Category_Id=' + category_id).then(function (e) {
        //    $scope.Flag = e.data;
        //    if ($scope.Flag !== "1" && $scope.Flag !== "2") {
        //        $scope.FlagforDelete = 'family';
        //        $scope.RemoveFlag = '0';
        //        $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
        //        $scope.Remove.center().open();
        //    }
        //    else {
        //        if ($scope.Flag === "1") {
        //            $scope.EnableShowMaster = true;
        //        }
        //        else {
        //            $scope.EnableShowMaster = false;
        //        }
        //        $scope.CloneAlert.refresh({ url: "../views/app/partials/CloneCheckOption.html" });
        //        $scope.CloneAlert.center().open();

        //    }
        //});


        // $("#dialog-confirm1").show();
        //////$("#dialog-confirm1").dialog({
        //////    resizable: false,
        //////    modal: true,
        //////    buttons: {
        //////        "Yes": function () {

        //////            if ($scope.RemoveFlag !== '') {
        //////                if ($scope.FlagforDelete === "category" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId !== "" && !$scope.RightClickedCategoryId.contains('~')) {
        //////                    $scope.Cancel();
        //////                    dataFactory.CategoryremoveFromNavigator($scope.RightClickedCategoryId, $scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag).success(function (response) {
        //////                        $.msgBox({
        //////                            title: $localStorage.ProdcutTitle,
        //////                            content: '' + response + '',
        //////                            type: "info"
        //////                        });
        //////                        $scope.RefreshLeftNavTree();
        //////                        $scope.RemoveFlag = '0';
        //////                        $scope.RightClickedCategoryId = '';
        //////                        $scope.associationRemove = "Association";
        //////                    }).error(function (error) {
        //////                        options.error(error);
        //////                    });
        //////                }
        //////                else if ($scope.FlagforDelete === "family" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId.contains('~')) {

        //////                    $scope.Cancel();
        //////                    dataFactory.FamilyremoveFromNavigator($scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag, $scope.RightClickedCategoryId).success(function (response) {
        //////                        $.msgBox({
        //////                            title: $localStorage.ProdcutTitle,
        //////                            content: '' + response + '',
        //////                            type: "info"
        //////                        });
        //////                        $scope.RefreshLeftNavTree();
        //////                        $scope.RemoveFlag = '0';
        //////                        $scope.RightClickedCategoryId = '';
        //////                        $scope.associationRemove = "Association";
        //////                    }).error(function (error) {
        //////                        options.error(error);
        //////                    });
        //////                }
        //////                else {

        //////                    $scope.RemoveFlag = '0';
        //////                    $scope.associationRemove = "Association";
        //////                    $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
        //////                    $scope.Remove.center().close();
        //////                }
        //////            }
        //////            else {
        //////                $scope.RemoveFlag = '0';
        //////                $scope.associationRemove = "Association";
        //////                $.msgBox({
        //////                    title: $localStorage.ProdcutTitle,
        //////                    content: 'Please choose an Option',
        //////                    type: "info"
        //////                });
        //////            }
        //////            $(this).dialog("close");
        //////        },
        //////        "No": function () {

        //////            $(this).dialog("close");
        //////        }

        //////    }

        //////});
        //  $("#dialog-confirm1").hide();
    };
    //Family RemovFrom Navigator
    // Family Trash From Navigator

    $scope.TrashFamily = function () {

        //----------------------------Family deletion-------------------------------------//
        dataFactory.checkproducts($rootScope.selecetedCatalogId, $scope.RightClickedCategoryId, $scope.deleteFamilyList).success(function (response) {

            if (response == "Associated") {
                $scope.ProdCheckAssociate = 1;
                $scope.FamilyDeletion();
            }
            else if (response == "Defualt_Family") {
                $scope.ProdCheckAssociate = 1;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Do you want to Permanent delete the Items under this Default Product?",
                    type: "confirm",
                    buttons: [{ value: "Yes" },
                        { value: "No" }
                    ],
                    success: function (result) {
                        if (result === "Yes") {
                            $scope.FamilyDeletion();
                        }
                    }
                });

            }
            else if (response == "Not Associated") {
                $scope.ProdCheckAssociate = 0;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Do you want to move Default Product?",
                    type: "confirm",
                    buttons: [{ value: "Ok" },
                        { value: "Cancel" }
                    ],
                    success: function (result) {
                        if (result === "Ok") {
                            $scope.FamilyDeletion();
                        }
                    }
                });
            }
        });
    }

    $scope.FamilyDeletion = function () {

        //----------------------------Family deletion-------------------------------------//




        $rootScope.familyAction = "Delete";
        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }
        else {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID;
        }
        if ($rootScope.familyDeleteNav.includes("~")) {
            $rootScope.familyDeleteNav = $rootScope.familyDeleteNav.split("~")[0];
        }
        $rootScope.familyDeleteNavStatus = true;
        $rootScope.SelectedNodeIDPresist = 1;
        if ($rootScope.familyDeleteNav == undefined)
            $rootScope.selecetedCategoryIdClone = "0";
        else
            $rootScope.selecetedCategoryIdClone = $rootScope.familyDeleteNav;
        $scope.RemoveFlag = 1;
        $scope.Flag = '0';
        $scope.EnableShowMaster = true;
        //  var id = $scope.SelectedItem.id.split('~');
        var id = $scope.SelectedItem.split('~');
        var category_id = "0";
        if (id.length > 0) {
            category_id = id[0].toString();
        }
        var family;
        $rootScope.displayCloneFamilies = false;
        if ($rootScope.SelectedRightnavigationStatus == "family") {
            family = "#dialog-confirm2";
            $scope.familystatus = "Product deleted successfully"
        }

        if ($rootScope.SelectedRightnavigationStatus == "defaultfamily") {
            family = "#dialog-confirm8";
            $scope.familystatus = "Product deleted successfully"
        }
        if ($rootScope.SelectedRightnavigationStatus == "defaultsubfamily") {
            family = "#dialog-confirm9";
            $scope.familystatus = "Product deleted successfully"
        }
        if ($rootScope.SelectedRightnavigationStatus == "subfamily") {
            family = "#dialog-confirm6";
            $scope.familystatus = "Subfamily deleted successfully"
        }
        if ($rootScope.SelectedRightnavigationStatus == "familyClone") {
            family = "#dialog-confirm7";
            $scope.familystatus = "Clone Product deleted successfully"
        }
        dataFactory.TrashCloneFamily($scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag, $scope.RightClickedCategoryId, $scope.deleteFamilyList).success(function (response) {
            if (response != null && response.length > 0) {
                $(family).val('The Product you try to delete has one or more clone.Do you want to continue?');
                $(family).text('The Product you try to delete has one or more clone.Do you want to continue?');
                $(family).show();
                $(family).dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            if ($scope.RemoveFlag !== '') {
                                if ($scope.FlagforDelete === "category" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId !== "" && !$scope.RightClickedCategoryId.contains('~')) {
                                    $scope.Cancel();
                                    dataFactory.CategoryremoveFromNavigator($scope.RightClickedCategoryId, $scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag).success(function (response) {

                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response + '.',
                                            type: "info"
                                        });
                                        $scope.RefreshLeftNavTree();
                                        $scope.RemoveFlag = '0';
                                        $scope.RightClickedCategoryId = '';
                                        $scope.associationRemove = "Association";
                                    }).error(function (error) {
                                        options.error(error);
                                    });
                                }
                                else if ($scope.FlagforDelete === "family" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId.contains('~')) {

                                    $rootScope.familyAction = "Delete";
                                    //dataFactory.TrashCloneFamily($scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag, $scope.RightClickedCategoryId, $scope.deleteFamilyList).success(function (response) {
                                    //    if (response != null && response.length > 0) {
                                    //        $rootScope.GetClonefamilyDatasource.read();
                                    //        $scope.winGetClone.refresh({ url: "../views/app/Admin/clonefamily.html" });
                                    //        $scope.winGetClone.center().open();
                                    //    }
                                    //    else {

                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: "This action will remove the original Product along with all clone. Do you want to continue",
                                        type: "confirm",
                                        buttons: [{ value: "Yes" }, { value: "No" }],
                                        success: function (result) {
                                            if (result == "Yes") {
                                                dataFactory.FamilytrashFromNavigator($scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag, $scope.RightClickedCategoryId, $scope.deleteFamilyList, $scope.ProdCheckAssociate).success(function (response) {
                                                    if (response == "Product deleted successfully") {
                                                        response = $scope.familystatus;
                                                    }
                                                    $.msgBox({
                                                        title: $localStorage.ProdcutTitle,
                                                        content: '' + response + '.',
                                                        type: "info"
                                                    });
                                                    $scope.RefreshLeftNavTree();
                                                    $scope.RemoveFlag = '0';
                                                    $scope.RightClickedCategoryId = '';
                                                    $scope.associationRemove = "Association";
                                                }).error(function (error) {
                                                    options.error(error);
                                                });
                                                // }
                                                // });
                                            }
                                        }
                                    });
                                }
                                else {
                                    $scope.RemoveFlag = '0';
                                    $scope.associationRemove = "Association";
                                    $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
                                    $scope.Remove.center().close();
                                }

                            }
                            else {
                                $scope.RemoveFlag = '0';
                                $scope.associationRemove = "Association";
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Please choose an Option.',
                                    type: "info"
                                });
                            }
                            $(this).dialog("close");
                        },
                        "Assign Master": function () {
                            $rootScope.GetClonefamilyDatasource.read();
                            $scope.winGetClone.refresh({ url: "../views/app/Admin/clonefamily.html" });
                            $(this).dialog("close");
                            $scope.winGetClone.center().open();

                        },
                        "No": function () {
                            $(this).dialog("close");
                        }

                    }
                });
            }
            else {
                $(family).show();
                $(family).dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            if ($scope.RemoveFlag !== '') {
                                if ($scope.FlagforDelete === "category" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId !== "" && !$scope.RightClickedCategoryId.contains('~')) {
                                    $scope.Cancel();
                                    dataFactory.CategoryremoveFromNavigator($scope.RightClickedCategoryId, $scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag).success(function (response) {

                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response + '.',
                                            type: "info"
                                        });
                                        $scope.RefreshLeftNavTree();
                                        $scope.RemoveFlag = '0';
                                        $scope.RightClickedCategoryId = '';
                                        $scope.associationRemove = "Association";
                                    }).error(function (error) {
                                        options.error(error);
                                    });

                                }
                                else if ($scope.FlagforDelete === "family" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId.contains('~')) {

                                    $rootScope.familyAction = "Delete";
                                    //dataFactory.TrashCloneFamily($scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag, $scope.RightClickedCategoryId, $scope.deleteFamilyList).success(function (response) {
                                    //    if (response != null && response.length > 0) {
                                    //        $rootScope.GetClonefamilyDatasource.read();
                                    //        $scope.winGetClone.refresh({ url: "../views/app/Admin/clonefamily.html" });
                                    //        $scope.winGetClone.center().open();
                                    //    }
                                    //    else {

                                    dataFactory.FamilytrashFromNavigator($scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag, $scope.RightClickedCategoryId, $scope.deleteFamilyList, $scope.ProdCheckAssociate).success(function (response) {
                                        if (response == "Product deleted successfully") {
                                            response = $scope.familystatus;
                                        }
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response + '.',
                                            type: "info"
                                        });
                                        $scope.RefreshLeftNavTree();
                                        $scope.RemoveFlag = '0';
                                        $scope.RightClickedCategoryId = '';
                                        $scope.associationRemove = "Association";
                                    }).error(function (error) {
                                        options.error(error);
                                    });
                                    // }
                                    // });
                                }
                                else {

                                    $scope.RemoveFlag = '0';
                                    $scope.associationRemove = "Association";
                                    $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
                                    $scope.Remove.center().close();
                                }
                            }
                            else {
                                $scope.RemoveFlag = '0';
                                $scope.associationRemove = "Association";
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Please choose an Option.',
                                    type: "info"
                                });
                            }
                            $(this).dialog("close");
                        },

                        "No": function () {

                            $(this).dialog("close");
                        }

                    }

                });

            }
        });
        //  $("#dialog-confirm1").hide();
    };
    // Family Trash From Navigator

    $scope.leftNavCollapse = function () {
        $("#leftNavTreeViewKendoNavigator").data("kendoTreeView").collapse(".k-item");
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();

    };

    //Left Nav - Tree Expand/Collapse-End
    $scope.catalogChange = function (e) {

        if ($scope.allowChangeFlag == "1") {

            //-------------To Hide Family Accordian And Show Category Accordion------------//

            //$("#tabstrip-2").hide();
            //$('#tabstrip-1').show();

            //-------------To Hide Family Accordian And Show Category Accordion------------//

            //-----------------------------------Family UNDO Flags -------------------------//
            $localStorage.saveDetails = [];
            $localStorage.saveSelectedFamilyDetails = [];
            $localStorage.savebaseFamilyAttributeRow = [];
            $localStorage.undoFamilyTemplateData = [];
            $localStorage.undoProductTemplateData = [];
            $rootScope.selectedFamilyDetails = [];
            //-----------------------------------Family UNDO Flags -------------------------//


            //-----------------------------------Product UNDO Flags -------------------------//
            $localStorage.savebaseProductAttributeRow = [];
            //-----------------------------------Family UNDO Flags -------------------------//


            //-----------------------------------Category Undo Flags------------------------//
            $localStorage.categoryChangedListUndo = [];
            $localStorage.baseCategoryValUndoList = [];
            $localStorage.undoCategoryTemplateData = [];
            $rootScope.categoryUndoList = [];
            //-----------------------------------Category Undo Flags------------------------//

            //-----------------------------------Only Product UNDO Flags -------------------------//
            $localStorage.selectedListOnlyProductDetails = [];
            $localStorage.baseUndoOnlyProductDetails = [];
            $localStorage.ProductDetailsValueOnly = [];
            $rootScope.showSelectedFamilyProductDetails
            $localStorage.getCatalogID = e.sender.value();
            //-----------------------------------Only Product UNDO Flags -------------------------//
            dataFactory.getdefaultfamilyexists($localStorage.getCatalogID).success(function (response) {
                if (response == 0) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Kindly select the default Product.',
                        type: "info"
                    });
                }

            });
            //  $rootScope.selectedFamilyDetails = [];
            if (e.sender.value() != "") {
                blockUI.start();
                $scope.$broadcast("clearids");
                $rootScope.getSelectedNode = "";
                $rootScope.CutCategoryId = "";
                $rootScope.selecetedCatalogId = e.sender.value();
                $localStorage.getCatalogName = e.sender.text();
                $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
                $rootScope.treeData._data = [];
                //$rootScope.expandNode = 0;
                i = 0;
                $rootScope.treeData.read();

                $timeout(function () {
                    dataFactory.FamilyPackages($scope.SelectedCatalogId).success(function (response) {
                        if (response != null || response != "undefined") {
                            $scope.familyPackage = response;
                        }
                    });
                }, 2000);

                $timeout(function () {
                    dataFactory.ProductPackages($scope.SelectedCatalogId).success(function (response) {
                        if (response != null || response != "undefined") {
                            $scope.productPackage = response;
                        }
                    });
                }, 2000);

                $timeout(function () {
                    $rootScope.ProductGroupNameDataSource.read();
                }, 2000);

                $timeout(function () {
                    $rootScope.FamilyGroupNameDataSource.read();
                }, 2000);

                //  $rootScope.dropDownTreeViewParnetCategoryDatasource.read();
                //   $rootScope.dropDownTreeViewDatasource.read();
                blockUI.stop();
                // productService.addProduct($rootScope.selecetedCatalogId);
            } else {
                $rootScope.treeData = [];
                $rootScope.selecetedCatalogId = 0;
            }
            $rootScope.expandNode = 0;
            $scope.GetUserRoleRightsAll();
        }
    };

    $rootScope.clickSelectedemptyItem = function () {
        $scope.categorycnt = false;
        $scope.$broadcast("NewCategoryIds");
        $rootScope.selecetedParentCategoryId = "0";
        //  $("#dropDownParnetCategoryTreeView .k-input").text("Root");
        $("#txt_ParentCategoryID").val("Root");


    };

    //pdf

    $scope.GetDefaultPdfTemplate = function (dataItem, TYPE) {
        $scope.$broadcast("RefreshFileuploadValues");
        var Id;
        var Catalog_id = $localStorage.getCatalogID;
        var CatalogID = $localStorage.getCatalogID;
        if (TYPE == 'Category') {
            Id = dataItem.CATEGORY_ID;
        }
        else {

            if (dataItem.CATEGORY_ID.charAt(0) === '~') {
                Id = dataItem.CATEGORY_ID.substr(1);
            } else {
                Id = dataItem.CATEGORY_ID;
            }
        }

        dataFactory.getPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {
            $rootScope.FamilyMrtFile = "";
            $rootScope.ProductMrtFile = "";
            if (TYPE == 'Category') {
                if (response != null && response != '0') {
                    $scope.getCategoryPdfExpress = response;
                    $scope.getCategoryPdfExpressToolTip = response;
                    $rootScope.getCategoryPdfExpress = $scope.getCategoryPdfExpress;
                    $rootScope.getCategoryPdfExpressToolTip = $scope.getCategoryPdfExpress;
                    $scope.$broadcast("ToAddTheData", $rootScope.getCategoryPdfExpress);
                }
                else {
                    $scope.getCategoryPdfExpress = '';
                    $scope.getCategoryPdfExpressToolTip = 'No file chosen';
                    $scope.$broadcast("ToClearTheData", "some data");
                }
            }

            else {
                if (response != null && response != '0') {
                    $scope.SelectedFileForFamily = response;
                    $rootScope.FamilyMrtFile = $scope.SelectedFileForFamily;

                    dataFactory.getPdfXpressdefaultTemplate_Product('Product', Id, CatalogID).success(function (response) {
                        if (response != null && response != '0') {
                            $scope.SelectedFileForProduct = response;
                            $rootScope.ProductMrtFile = $scope.SelectedFileForProduct;
                        }
                    });
                    $scope.SelectedFileForFamilyTooltip = response
                }
                else {
                    $scope.SelectedFileForFamily = '';
                    $scope.SelectedFileForFamilyTooltip = 'No file chosen';
                }
            }
        });

        dataFactory.getPdfXpressdefaultTemplate_Product('Product', Id, CatalogID).success(function (response) {
            if (response != null && response != '0') {
                $scope.SelectedFileForProduct = response;
                $rootScope.ProductMrtFile = $scope.SelectedFileForProduct;
            }
        });
    }

    //pdf
    $rootScope.clickSelectedItemSubProduct = function (dataItem, event) {

        $rootScope.invertedproductsbutton1 = false;
        if (dataItem.id.match("~")) {
            $rootScope.familycategory = dataItem.id.split("~")[0];
        } else {
            $rootScope.familycategory = dataItem.id;
        }

        if (event !== undefined && event !== null) {
            $scope.Coordinates.pageX = event.pageX;
            $scope.Coordinates.pageY = event.pageY;
        }
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $scope.SelectedDataItem.VERSION = dataItem.VERSION;
        $scope.SelectedDataItem.DESCRIPTION = dataItem.DESCRIPTION;
        $scope.SelectedDataItem.CATALOG_NAME = dataItem.CATALOG_NAME;
        $rootScope.selecetedCatalogId = dataItem.CATALOG_ID;
        $scope.SelectedDataItem.CATALOG_ID = dataItem.CATALOG_ID;
        $scope.SelectedDataItem.CATEGORY_ID = dataItem.CATEGORY_ID;
        $scope.SelectedDataItem.ID = dataItem.CATEGORY_ID;
        $scope.SelectedDataItem.SORT_ORDER = dataItem.SORT_ORDER;
        $scope.SelectedItem = dataItem.CATEGORY_ID;
        $localStorage.CategoryID = dataItem.id;
        $rootScope.getSelectedNode = dataItem.CATEGORY_ID;
        $("#dynamictable").show();
        $("#productpreview").hide();
        $("#subproductpreview").hide();
        // $("#dynamictableFmly").show();
        $("#categoryGridView").show();
        if ($scope.SelectedItem.match("~")) {

            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);
            $scope.SelectedCategoryId = $scope.SelectedItem;
            if (dataItem.parentNode().CATEGORY_ID.match("~")) {
                $scope.$broadcast("Active_Category_Id", dataItem.parentNode().parentNode().CATEGORY_ID);
                $rootScope.selecetedCategoryId = dataItem.parentNode().parentNode().CATEGORY_ID;
            } else {
                $scope.$broadcast("Active_Category_Id", dataItem.parentNode().CATEGORY_ID);
                $rootScope.selecetedCategoryId = dataItem.parentNode().CATEGORY_ID;
            }
        } else {
            $rootScope.selecetedCategoryId = $scope.SelectedItem;
            $scope.SelectedCategoryId = $scope.SelectedItem;
            $scope.$broadcast("Active_Category_Id", $scope.SelectedCategoryId);
            // var selectedparentcategory1 = $rootScope.dropDownTreeView._treeview.dataSource.get($scope.SelectedCategoryId);
            //  if (selectedparentcategory1 !== undefined) {
            //  var node = $rootScope.dropDownTreeView._treeview.findByUid(selectedparentcategory1.uid);
            // if (node.length > 0) {
            //$rootScope.dropDownTreeView._treeview.select(node[0]);
            // $("#dropDownTreeView .k-input").text(selectedparentcategory1.CATEGORY_NAME);
            $rootScope.FamilyCATEGORY_ID = $scope.SelectedCategoryId;
            // }
            // }
        }
        //  $rootScope.dropDownTreeViewParnetCategoryDatasource.read();
        // $rootScope.dropDownTreeViewDatasource.read();
        if ($scope.SelectedCategoryId.match("~")) {
            //$("#CategoryTab").removeClass("in active");
            //$("#FamilyTab").addClass("in active");
            //$("#ProductsTab").removeClass("in active");



            var tabstrip = $("#tabstrip").data("kendoTabStrip");
            var myTab = tabstrip.tabGroup.children("li").eq(2);
            tabstrip.enable(myTab);
            tabstrip.select(myTab);

            var myTabscategory = tabstrip.tabGroup.children("li").eq(0);
            tabstrip.enable(myTabscategory);
            var myTabsfamily = tabstrip.tabGroup.children("li").eq(1);
            tabstrip.enable(myTabsfamily);

            var myTabsproducts1 = tabstrip.tabGroup.children("li").eq(2);
            tabstrip.enable(myTabsproducts1);

            $("#custom-menu1").hide();
            $("#custom-menuFam").hide();
            $(".slideout-menu").removeClass("open");
            $(".slideout-menu").css('left', "-280px");
            $scope.$broadcast("Active_Family_Id", $scope.SelectedCategoryId, dataItem.parentNode());
            $scope.$broadcast("Active_Category_Selection", $scope.SelectedDataItem);
            // $scope.$broadcast("GetFamily", $scope.SelectedCategoryId, $rootScope.familycategory);

            $rootScope.selecetedFamilyId = $scope.SelectedCategoryId;
            $scope.$broadcast("Active_SubProduct", $scope.SelectedCategoryId, $rootScope.familycategory, 0);
            var tabstripSP = $("#productgridtabstrip").data("kendoTabStrip");
            var myTabSP = tabstripSP.tabGroup.children("li").eq(1);
            tabstripSP.enable(myTabSP);
            tabstripSP.select(myTabSP);
        } else {
            //$("#CategoryTab").addClass("in active");
            //$("#FamilyTab").removeClass("in active");
            //$("#ProductsTab").removeClass("in active");
            var tabstrips = $("#tabstrip").data("kendoTabStrip");
            var myTabs = tabstrips.tabGroup.children("li").eq(0);
            tabstrips.select(myTabs);

            var myTabsfamily = tabstrips.tabGroup.children("li").eq(1);
            tabstrips.disable(myTabsfamily);

            var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
            tabstrips.disable(myTabsproducts);

            $("#custom-menu1").hide();
            $("#custom-menuFam").hide();
            $(".slideout-menu").removeClass("open");
            $(".slideout-menu").css('left', "-280px");
        }

        $('#user-toolbar-preview').removeAttr('disabled');
        if ($scope.userRoleDeleteFamily) {
            //  $('#user-toolbar-association-removal-family').removeAttr('disabled');
            $('#user-toolbar-delete-family').removeAttr('disabled');
        }
        else {
            //  $('#user-toolbar-association-removal-family').attr('disabled', 'disabled');
            $('#user-toolbar-delete-family').attr('disabled', 'disabled');
        }

    };

    $scope.selectedRowValue = 0;
    $scope.setClickedRow = function (index) {

        $scope.selectedRowValue = index["PRODUCT_ID"];
        $rootScope.selectedRowValue = index["PRODUCT_ID"];
        $rootScope.getProductGridDetails(index);


    };
    $scope.Family = {
        FAMILY_ID: '',
        FAMILY_NAME: '',
        CATEGORY_ID: '',
        STATUS: 'CREATED',
        PUBLISH: true,
        PUBLISH2PRINT: true,
        PARENT_FAMILY_ID: 0,
        FAMILYIMG_NAME: '',
        FAMILYIMG_ATTRIBUTE: '',
        ATTRIBUTE_ID: '', OBJECT_NAME: ''
    };
    $rootScope.clickSelectedItems = function (dataItem, search, family) {
        //debugger;
        if (!search) {
            $rootScope.familyMainEditor = true;
            $rootScope.invertedproductsshow = false;
            $rootScope.invertedproductsbutton1 = true;
        }
        $scope.ProdCurrentPage = "1";
        $rootScope.selectedCloneDetails = dataItem.id;
        $rootScope.currentFamilyId = dataItem.FAMILY_ID;
        //  $rootScope.SelectedDataItem.CATALOG_ID
        //$scope.ProdCountPerPage = "5";
        //$scope.gridCurrentPage = "5";
        //document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaging";
        //$scope.ProdCountPerPage = "5";
        //$scope.gridCurrentPage = "5";
        $rootScope.familyCreated = true;
        $scope.selectedRowValue = dataItem.PRODUCT_ID;
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $scope.Family.FAMILY_ID = dataItem.FAMILY_ID;
        $scope.SelectedDataItem.VERSION = dataItem.VERSION;
        $scope.SelectedDataItem.DESCRIPTION = dataItem.DESCRIPTION;
        $scope.SelectedDataItem.CATALOG_NAME = dataItem.CATALOG_NAME;
        $rootScope.selecetedCatalogId = dataItem.CATALOG_ID;
        $scope.SelectedDataItem.CATALOG_ID = dataItem.CATALOG_ID;
        //  $rootScope.SelectedDataItem.CATALOG_ID= dataItem.CATALOG_ID;
        $scope.SelectedDataItem.CATEGORY_ID = dataItem.CATEGORY_ID;

        //   $rootScope.SelectedDataItem.ID = dataItem.id;
        // $scope.SelectedDataItem.ID = dataItem.id;

        $scope.SelectedDataItem.SORT_ORDER = dataItem.SORT_ORDER;
        $scope.SelectedItem = dataItem.id;
        $rootScope.getSelectedNode = dataItem.CATEGORY_ID;
        $localStorage.CategoryID = dataItem.CATEGORY_ID;
        $("#dynamictable").show();
        $("#productpreview").hide();
        $("#subproductpreview").hide();
        $("#categoryGridView").show();
        $rootScope.LoadProdDataPage($rootScope.selecetedCatalogId, $scope.SelectedItem, dataItem.CATEGORY_ID, $scope.selectedRowValue, "Product");
        $scope.$broadcast("Active_Category_Id", dataItem.CATEGORY_ID);
        $rootScope.selecetedCategoryId = dataItem.CATEGORY_ID;
        $scope.SelectedCategoryId = $scope.SelectedItem;
        // $rootScope.dropDownTreeViewParnetCategoryDatasource.read();
        // $rootScope.dropDownTreeViewDatasource.read();
        var tabstrip = $("#tabstrip").data("kendoTabStrip");
        var myTab = tabstrip.tabGroup.children("li").eq(1);
        tabstrip.enable(myTab);

        var myTabscategory = tabstrip.tabGroup.children("li").eq(0);
        tabstrip.enable(myTabscategory);
        var myTabsproducts = tabstrip.tabGroup.children("li").eq(2);
        tabstrip.enable(myTabsproducts);
        if (family) {
            tabstrip.select(myTab);
        } else {
            tabstrip.select(myTabsproducts);
        }
        var tabstripProd = $(".productgridtabstripclass").data("kendoTabStrip");
        var myTabProd = tabstripProd.tabGroup.children("li").eq(0);
        tabstripProd.enable(myTabProd);
        tabstripProd.select(myTabProd);
        $(".k-tabstrip-wrapper").removeAttr("style");
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $(".slideout-menu").removeClass("open");
        $(".slideout-menu").css('left', "-280px");
        $scope.$broadcast("Active_Family_Ids", $scope.SelectedCategoryId, dataItem);
        $scope.$broadcast("Active_Category_Selection", $scope.SelectedDataItem);
        // $scope.$broadcast("GetFamily", $scope.SelectedCategoryId, dataItem.CATEGORY_ID);

        $rootScope.selecetedFamilyId = $scope.SelectedCategoryId;
        $("#divProductGrid").css('display', 'block');
        if (dataItem.PRODUCT_ID != undefined) {
            $rootScope.ParentProductId = dataItem.PRODUCT_ID;
            $scope.ParentProductId = dataItem.PRODUCT_ID;
        }
        //if ($scope.ParentProductId !== undefined) {
        //    $scope.$broadcast("Active_SubProduct", $scope.SelectedCategoryId, $rootScope.familycategory, $scope.ParentProductId);
        //    var tabstripProd = $("#productgridtabstrip").data("kendoTabStrip");
        //    var myTabProd = tabstripProd.tabGroup.children("li").eq(0);
        //    tabstripProd.enable(myTabProd);
        //    tabstripProd.select(myTabProd);
        //}
        //JOTHIPRIYA	

        $scope.ProdPageCount = "1";
        if ($scope.ProdCurrentPage == "1") {
            $scope.changeProdPage = 1;
        } else {
            $scope.changeProdPage = $scope.ProdCurrentPage;
        }
    };


    $rootScope.LoadProdDataPage = function (catalogId, id, cat_id, product_id, option) {
        $rootScope.invertPro = "SubProduct";
        $scope.sub_id = '0';
        $rootScope.sub_cat_id = cat_id;
        $scope.changeProdPage = "1";//JOTHIPRIYA
        $rootScope.sub_fam_id = id;
        $scope.invertOption = option;
        $scope.invertProdId = product_id;
        $rootScope.invertProdId = $scope.invertProdId;
        //dataFactory.getprodspecs(catalogId, id, $scope.displayID).success(function (response) {
        //document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaginghighlight";
        var ProdCount = [];
        dataFactory.getprodspecsWithoutAttributes(catalogId, id, true, cat_id).success(function (response) {
            if (response.Data.Columns.length) {
                var obj = jQuery.parseJSON(response.Data.Data);
                var ProdEditPageloopcnt = Math.floor(obj.length / parseInt($scope.ProdCountPerPage));
                if ((obj.length % parseInt($scope.ProdCountPerPage)) > 0) {
                    ProdEditPageloopcnt = ProdEditPageloopcnt + 1;
                }
                $scope.changeProdPage = "1";

                for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                    ProdCount.push(i);
                }
                $scope.ProdPageCount = ProdCount;
                if ($scope.ProdCurrentPage == "1") {
                    $scope.changeProdPage = 1;
                } else {
                    $scope.changeProdPage = $scope.ProdCurrentPage;
                }
                $scope.ProdCurrentPage = $scope.changeProdPage;
                $scope.totalProdPageCount = ProdEditPageloopcnt;
            }

            dataFactory.getprodspecs(catalogId, id, true, cat_id, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.sub_id).success(function (response) {
                //var obj = jQuery.parseJSON(response.Data.Data);
                var productDetails = jQuery.parseJSON(response.Data.Data);
                var obj = productDetails.Table;
                $scope.prodData = obj;
                // Newly Added
                if (productDetails.Table1 != undefined) {
                    $scope.TotalTableCount = productDetails.Table1[0].Column1;
                }

                if ($scope.prodData.length == 0 && $rootScope.deleteProduct == true && $scope.ProdCurrentPage != 1) {
                    $scope.ProdCurrentPage = $scope.ProdCurrentPage - 1;
                    $scope.LoadProdData(catalogId, id, cat_id);
                }
                //To Change Dynamice CatalogNo
                for (var i = 0; i < response.Data.Columns.length; i++) {
                    if (response.Data.Columns[i]["Caption"].includes("ITEM#")) {
                        response.Data.Columns[i]["Caption"] = response.Data.Columns[i]["Caption"].replace("ITEM#", $localStorage.CatalogItemNumber);
                    }
                }




                // To push the values into arry for split only attributes in drop down list.
                var AttributeValues = [];
                for (var i = 0 ; $scope.columnsForAtt.length > i ; i++) {

                    if (
                        $scope.columnsForAtt[i].Caption == "CATALOG_ID" ||
                        $scope.columnsForAtt[i].Caption == "FAMILY_ID" ||
                        $scope.columnsForAtt[i].Caption == "PRODUCT_ID" ||
                        $scope.columnsForAtt[i].Caption == "SORT" ||
                        $scope.columnsForAtt[i].Caption == "PUBLISH2WEB" ||
                        $scope.columnsForAtt[i].Caption == "PUBLISH2PRINT" ||
                        $scope.columnsForAtt[i].Caption == "WORKFLOW STATUS" ||
                        $scope.columnsForAtt[i].Caption == "PUBLISH2PDF" ||
                        $scope.columnsForAtt[i].Caption == "PUBLISH2EXPORT" ||
                        $scope.columnsForAtt[i].Caption == "PUBLISH2PORTAL" ||
                        $scope.columnsForAtt[i].Caption == "SubProdCount"
                        ) {
                    } else {
                        if ($scope.columnsForAtt[i].Caption.includes("__OBJ")) {
                            AttributeValues.push({ "Value": i, "Name": $scope.columnsForAtt[i].Caption.slice(0, $scope.columnsForAtt[i].Caption.search("__OBJ")) });
                        } else {
                            AttributeValues.push({ "Value": i, "Name": $scope.columnsForAtt[i].Caption });
                        }
                    }
                }



                // To remove extra duplicate values (__)
                for (var i = 0 ; AttributeValues.length > i ; i++) {
                    if (AttributeValues[i].Name.includes("__")) {
                        AttributeValues.splice(i, 1);
                    }
                }

                $scope.AttributeValues = AttributeValues;



                // To fix the attribute drop dowwnload  - End




                $scope.columnsForAtt = response.Data.Columns;
                angular.forEach($scope.prodData, function (value) {
                    value.SORT = parseFloat(value.SORT);
                    //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE	
                    // $scope.changeProdPage = $scope.ProdCurrentPage;	
                    $scope.ProdCurrentPage == "1";
                    if ($scope.ProdCurrentPage == "1") {
                        $scope.changeProdPage = "1";//JOTHIPRIYA	
                    } else {
                        $scope.changeProdPage = $scope.ProdCurrentPage;
                    }

                });

                if ($scope.Family.FAMILY_ID == "") {
                    $scope.Family.FAMILY_ID = 0;
                }

                if ($scope.Family.FAMILY_ID == undefined) {
                    $scope.Family.FAMILY_ID = 0;
                }
                if ($scope.prodData.length === 0) {
                    $("#selectedattributenoresultfound").css("display", "block");
                }
                if ($rootScope.invertProdId == undefined) {
                    return true;
                }

                dataFactory.GetPagenumberInverted($scope.SelectedDataItem.CATALOG_ID, $scope.SelectedDataItem.CATEGORY_ID, $scope.Family.FAMILY_ID, $scope.invertProdId, $scope.invertOption).success(function (response1) {
                    var obj = response1.split('~');

                    var ProdEditPageloopcnt1 = Math.floor(obj[0] / parseInt($scope.ProdCountPerPage));
                    if ((obj[0] % parseInt($scope.ProdCountPerPage)) > 0) {
                        ProdEditPageloopcnt1 = ProdEditPageloopcnt1 + 1;
                    }

                    var ProdEditPageloopcntMain = Math.floor(obj[obj.length - 1] / 10);
                    if ((obj[obj.length - 1] % 10) > 0) {
                        ProdEditPageloopcntMain = ProdEditPageloopcntMain + 1;
                    }

                    $rootScope.pageCountMain = ProdEditPageloopcntMain;
                    //if ($scope.invertOption == "SubProduct") {
                    //   // $rootScope.callProductGridPagingSP(ProdEditPageloopcnt1);
                    //    if (obj[2] != "") {
                    //        var ProdEditPageloopcntMain = Math.floor(obj[2] / parseInt($scope.ProdCountPerPage));
                    //        if ((obj[2] % parseInt($scope.ProdCountPerPage)) > 0) {
                    //            ProdEditPageloopcntMain = ProdEditPageloopcntMain + 1;
                    //        }

                    //        $rootScope.pageCountMain = ProdEditPageloopcntMain;

                    //        var grid = $("#subproductsmaingrid").data("kendoGrid");
                    //        if ($rootScope.pageCountMain != undefined && $rootScope.pageCountMain != "" && $rootScope.pageCountMain != null && $rootScope.pageCountMain != 1) {

                    //            grid.dataSource.page($rootScope.pageCountMain);
                    //            $rootScope.callProductGridPagingSP(ProdEditPageloopcnt1);

                    //        }
                    //        else {
                    //            if ($rootScope.pageCountMain != undefined && $rootScope.pageCountMain != "" && $rootScope.pageCountMain != null) {
                    //                $rootScope.callProductGridPagingSP(ProdEditPageloopcnt1);
                    //            }
                    //        }
                    //        $('#cboProdPageCountSPup').data("kendoDropDownList").span[0].innerHTML = ProdEditPageloopcnt1;
                    //        $('#cboProdPageCountSPdown').data("kendoDropDownList").span[0].innerHTML = ProdEditPageloopcnt1;
                    //    }

                    //}
                    //else {
                    var grid = $("#subproductsmaingrid").data("kendoGrid");
                    if ($rootScope.pageCountMain != undefined && $rootScope.pageCountMain != "" && $rootScope.pageCountMain != null) {
                        grid.dataSource.page($rootScope.pageCountMain);
                    }
                    $rootScope.Family_ID_NEW = $scope.Family.FAMILY_ID;
                    $scope.callProductGridPaging(ProdEditPageloopcnt1);
                    //JOTHIPRIYA	
                    $scope.ProdCurrentPage = ProdEditPageloopcnt1;
                    // }
                });
                // $rootScope.visibleProjects = $scope.prodData;
                // $rootScope.visibleProjects1 = $scope.prodData;
                // $data = $scope.prodData;
                $rootScope.Mainprodcount = $scope.prodData.length;
                if ($scope.prodData.length === 0) {
                    $("#newProductBtn").show();
                    $("#newProductBtn1").hide();
                    $("#newProductBtn").css("display", "block");
                    $("#newProductBtn1").css("display", "none");
                }
                else {
                    $("#newProductBtn").hide();
                    $("#newProductBtn1").show();
                    $("#newProductBtn").css("display", "none");
                    $("#newProductBtn1").css("display", "block");
                }
                //$scope.tblDashBoard.reload();
                // $scope.tblFamilyGrid.reload();
                // $scope.tblFamilyGrid.$params.page = 1;
                if ($scope.userRoleDeleteFamily) {
                    // $('#user-toolbar-association-removal-family').removeAttr('disabled');
                    $('#user-toolbar-delete-family').removeAttr('disabled');
                }
                else {
                    // $('#user-toolbar-association-removal-family').attr('disabled', 'disabled');
                    $('#user-toolbar-delete-family').attr('disabled', 'disabled');
                }

                if ($scope.userRoleAddFamily) {
                    $('#user-toolbar-create-new-family').removeAttr('disabled');
                }
                else {
                    $('#user-toolbar-create-new-family').attr('disabled', 'disabled');
                }
                if ($scope.userRoleModifyFamily) {
                    $('#user-toolbar-save-family').removeAttr('disabled');
                    $('#user-toolbar-family-attribute-setup').removeAttr('disabled');
                }
                else {
                    $('#user-toolbar-save-family').attr('disabled', 'disabled');
                    $('#user-toolbar-family-attribute-setup').attr('disabled', 'disabled');
                }
                //  var toolbarProduct = $("#product-toolbar").data("kendoToolBar");
                if ($scope.userRoleAddProductTable) {
                    $('#newbtn').removeAttr('disabled');
                }
                else {
                    $('#newbtn').attr('disabled', 'disabled');
                }
                if ($rootScope.ProductCount < $rootScope.ProductSKUCount) {
                }
                else {
                    $scope.userRoleAddProductTable = false;
                }

                if ($scope.userRoleModifyProductTable) {
                    $('#columnsetubbtn').removeAttr('disabled');
                    $('#user-toolbar-default-layout').removeAttr('disabled');
                }
                else {
                    $('#columnsetubbtn').attr('disabled', 'disabled');
                    $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                }
                //if ($scope.userRoleDeleteFamily) {
                //    toolbar.enable("#user-toolbar-association-removal-family", true);
                //    toolbar.enable("#user-toolbar-delete-family", true);
                //}
                //else {
                //    toolbar.enable("#user-toolbar-association-removal-family", false);
                //    toolbar.enable("#user-toolbar-delete-family", false);
                //}

                if ($scope.prodData.length > 0) {
                    // $('#productpaging').show();
                    $('#productpaging1').hide();
                    $('#user-toolbar-export').removeAttr('disabled');
                    $('#user-toolbar-table-designer').removeAttr('disabled');
                    $('#user-toolbar-set-table-layout').removeAttr('disabled');
                    $('#user-toolbar-delete-layout').removeAttr('disabled');
                    $('#user-toolbar-create-new-layout').removeAttr('disabled');
                    if ($scope.userRoleModifyProductTable) {
                        $('#user-toolbar-default-layout').removeAttr('disabled');
                    }
                    else {
                        $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                    }
                    if (!$rootScope.EditMode) {
                        $('#user-toolbar-export').attr('disabled', 'disabled');
                        $('#user-toolbar-table-designer').attr('disabled', 'disabled');
                    }
                }
                else {
                    $('#productpaging').hide();
                    $('#productpaging1').hide();
                    $('#user-toolbar-export').attr('disabled', 'disabled');
                    $('#user-toolbar-table-designer').attr('disabled', 'disabled');
                    $('#user-toolbar-set-table-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-delete-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-create-new-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                }

                if ($rootScope.userprodcongig === true) {
                    $('#addtoprdconfig').removeAttr('disabled');
                } else {
                    $('#addtoprdconfig').attr('disabled', 'disabled');
                }
                if ($rootScope.userprodcongig === true) {
                    $('#prodconfig').removeAttr('disabled');
                } else {
                    $('#prodconfig').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#cutbtn').removeAttr('disabled');
                } else {
                    $('#cutbtn').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#copybtn').removeAttr('disabled');
                } else {
                    $('#copybtn').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#pastebtn').removeAttr('disabled');
                } else {
                    $('#pastebtn').attr('disabled', 'disabled');
                }
            }).error(function (error) {

                options.error(error);
            });
        });
    };

    //$scope.backSub = function () {
    //    $rootScope.familyMainEditor = false;
    //    $rootScope.invertedproductsshow = true;
    //    $rootScope.invertedproductsbutton = false;
    //    $rootScope.invertedproductsbutton1 = false;

    //    $('#dynamictablesubproducts').show();
    //    $("#familyEditor").hide();
    //    $('#BackDiv2').hide();
    //    $('#subassociatedgrid').hide();
    //};

    $rootScope.clickSelectedItemsInverted = function (dataItem, search) {

        if (!search) {
            $rootScope.invertedproductsbutton1 = false;
            $rootScope.SubProdInvert = true;
            $rootScope.btnSubProdInvert = true;
        }

        $localStorage.CategoryID = dataItem.CATEGORY_ID;
        $scope.selectedRowValuesubproduct = dataItem.SUBPRODUCT_ID;
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $scope.ProdCurrentPage = "1";
        //document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaging";
        $scope.ProdCountPerPage = "10";
        $scope.gridCurrentPage = "10";
        $scope.Family.FAMILY_ID = dataItem.FAMILY_ID;
        $scope.SelectedDataItem.VERSION = dataItem.VERSION;
        $scope.SelectedDataItem.DESCRIPTION = dataItem.DESCRIPTION;
        $scope.SelectedDataItem.CATALOG_NAME = dataItem.CATALOG_NAME;
        $rootScope.selecetedCatalogId = dataItem.CATALOG_ID;
        $scope.SelectedDataItem.CATALOG_ID = dataItem.CATALOG_ID;
        $scope.SelectedDataItem.CATEGORY_ID = dataItem.CATEGORY_ID;
        $scope.SelectedDataItem.ID = dataItem.id;
        $scope.SelectedDataItem.SORT_ORDER = dataItem.SORT_ORDER;
        $scope.SelectedItem = dataItem.id;
        $rootScope.getSelectedNode = dataItem.CATEGORY_ID;
        $("#dynamictable").show();
        $("#productpreview").hide();
        $("#subproductpreview").hide();
        $("#categoryGridView").show();

        $rootScope.LoadProdDataPage($rootScope.selecetedCatalogId, $scope.SelectedItem, dataItem.CATEGORY_ID, $scope.selectedRowValuesubproduct, "SubProduct");
        $scope.$broadcast("Active_Category_Id", dataItem.CATEGORY_ID);
        $rootScope.selecetedCategoryId = dataItem.CATEGORY_ID;
        $scope.SelectedCategoryId = $scope.SelectedItem;
        //  $rootScope.dropDownTreeViewParnetCategoryDatasource.read();
        ///  $rootScope.dropDownTreeViewDatasource.read();
        var tabstrip = $("#tabstrip").data("kendoTabStrip");
        var myTab = tabstrip.tabGroup.children("li").eq(2);
        tabstrip.enable(myTab);
        tabstrip.select(myTab);
        var myTabscategory = tabstrip.tabGroup.children("li").eq(0);
        tabstrip.enable(myTabscategory);
        var myTabsproducts = tabstrip.tabGroup.children("li").eq(1);
        tabstrip.enable(myTabsproducts);
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $(".slideout-menu").removeClass("open");
        $(".slideout-menu").css('left', "-280px");
        $scope.$broadcast("Active_Family_Ids", $scope.SelectedCategoryId, dataItem);
        $scope.$broadcast("Active_Category_Selection", $scope.SelectedDataItem);
        // $scope.$broadcast("GetFamily", $scope.SelectedCategoryId, dataItem.CATEGORY_ID);

        $rootScope.selecetedFamilyId = $scope.SelectedCategoryId;

        if (dataItem.MAINPRODUCT_ID != undefined) {
            $rootScope.ParentProductId = dataItem.MAINPRODUCT_ID;
            $scope.ParentProductId = dataItem.MAINPRODUCT_ID;
        }
        else {
            $rootScope.ParentProductId = dataItem.PRODUCT_ID;
            $scope.ParentProductId = dataItem.PRODUCT_ID;
        }
        //if (dataItem.MAINPRODUCT_ID !== undefined) {
        //    $scope.$broadcast("Active_SubProduct", $scope.SelectedCategoryId, $rootScope.familycategory, dataItem.MAINPRODUCT_ID);
        //    var tabstripSP = $("#productgridtabstrip").data("kendoTabStrip");
        //    var myTabSP = tabstripSP.tabGroup.children("li").eq(1);
        //    tabstripSP.enable(myTabSP);
        //    tabstripSP.select(myTabSP);
        //}
        if ($scope.ParentProductId !== undefined) {
            $scope.$broadcast("Active_SubProduct", $scope.SelectedCategoryId, $rootScope.familycategory, $scope.ParentProductId);
            var tabstripSP = $(".productgridtabstripclass").data("kendoTabStrip");
            var myTabSP = tabstripSP.tabGroup.children("li").eq(1);
            tabstripSP.enable(myTabSP);
            tabstripSP.select(myTabSP);
        }



        $rootScope.LoadMainProductssub();

    };

    $scope.onLongPress = function (d, e) {
        if ($window.innerWidth < 1242) {
            if (e !== undefined && e !== null) {
                $scope.ContextMenu(e, d);
            }
            else {
                $scope.ContextMenu($scope.Coordinates, d);
            }
        }
    };

    $scope.onTouchEnd = function () {
    };

    $scope.RightClickedCategoryName = "";
    $scope.RightClickeddataItem = [];
    /* Region for Category */
    $rootScope.familycategory = "0";
    $scope.ContextMenu = function (e, dataItemId) {
        $rootScope.SelectedRightnavigationStatus = dataItemId.spriteCssClass;
        $scope.deleteFamilyList = dataItemId.id;

        $scope.menuexportcatfamid = dataItemId.id;
        $scope.menuexportcatfamidsplit = $scope.menuexportcatfamid.split('~');
        $scope.menuexportcatid = $scope.menuexportcatfamidsplit[0];
        $scope.menuexportfamid = $scope.menuexportcatfamidsplit[1];

        $scope.checkvalue = dataItemId["CATEGORY_ID"];
        var checkvalueforcategory = dataItemId["CATEGORY_ID"];
        var bool = checkvalueforcategory.includes("CAT");
        if (bool == true) {
            $scope.copycategoryvalue = dataItemId["CATEGORY_NAME"];
            $scope.copycategoryvalueforclipboard = $scope.copycategoryvalue;

            document.getElementById("Copycategory").title = $scope.copycategoryvalueforclipboard;
            $('[data-toggle="tooltip"]').tooltip();
            for (var i = 0; i < $scope.copycategoryvalue.length; i++) {
                if (i > 9) {
                    $scope.copycategoryvalue = $scope.copycategoryvalue.slice(0, 10);
                    $scope.copycategoryvalue = $scope.copycategoryvalue + ("...".replace($scope.copycategoryvalue));
                }
            }


            $scope.searchcategoryvalue = $scope.copycategoryvalueforclipboard;
            $scope.searchcategoryvalueclk = $scope.copycategoryvalueforclipboard;

            document.getElementById("Searchcategory").title = $scope.searchcategoryvalue;
            $('[data-toggle="tooltip"]').tooltip();

            for (var i = 0; i < $scope.searchcategoryvalue.length; i++) {
                if (i > 9) {
                    $scope.searchcategoryvalue = $scope.searchcategoryvalue.slice(0, 10);
                    $scope.searchcategoryvalue = $scope.searchcategoryvalue + ("...".replace($scope.searchcategoryvalue));
                }
            }
        }

        else {
            var checkvalueforfamily = dataItemId["CATEGORY_NAME"];
            checkvalueforfamily = checkvalueforfamily.substr(0, checkvalueforfamily.lastIndexOf("(")).trim();
            $scope.copyfamilyvalue = checkvalueforfamily;
            $scope.copyfamilyvalueforclipboard = $scope.copyfamilyvalue;

            document.getElementById("Copyfamily").title = $scope.copyfamilyvalueforclipboard;
            $('[data-toggle="tooltip"]').tooltip();

            for (var i = 0; i < $scope.copyfamilyvalue.length; i++) {
                if (i > 9) {
                    $scope.copyfamilyvalue = $scope.copyfamilyvalue.slice(0, 10);
                    $scope.copyfamilyvalue = $scope.copyfamilyvalue + ("...".replace($scope.copyfamilyvalue));
                }
            }

            $scope.searchfamilyvalue = $scope.copyfamilyvalueforclipboard;
            $scope.searchfamilyvalueclk = $scope.copyfamilyvalueforclipboard;

            document.getElementById("Searchfamily").title = $scope.searchfamilyvalue;
            $('[data-toggle="tooltip"]').tooltip();

            for (var i = 0; i < $scope.searchfamilyvalue.length; i++) {
                if (i > 9) {
                    $scope.searchfamilyvalue = $scope.searchfamilyvalue.slice(0, 10);
                    $scope.searchfamilyvalue = $scope.searchfamilyvalue + ("...".replace($scope.searchfamilyvalue));
                }
            }
        }

        var countList = 0
        var listSelected = $rootScope.SelectedNodeID.split('~');
        var listSelecteddata = dataItemId.id.split('~');

        angular.forEach(listSelecteddata, function (value) {
            angular.forEach(listSelecteddata, function (value1) {
                if (value == value1) {
                    countList = countList + 1;
                }
            });
        })

        if (countList == 0) {
            $rootScope.SelectedNodeID = dataItemId.id;
        }
        $scope.SelectedItem = dataItemId.CATEGORY_ID;
        $scope.ShowHide();
        if (dataItemId.CATEGORY_ID.contains('~')) {
            $scope.RightClickedCategoryId = dataItemId.CATEGORY_ID;
            $scope.subproductcheck = dataItemId.CATEGORY_NAME;
            if ($scope.checkeditems.trim() === '') {

                if ($scope.FamilyNameForClone === '') {
                    $scope.FamilyNameForClone = dataItemId.CATEGORY_NAME;
                } else if ($scope.CopyCategoryName != $scope.FamilyNameForClone) {
                    $scope.FamilyNameForClone = $scope.CopyCategoryName;
                }
            }
            else {
                $http.get('../Category/GetCategoryCount?MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&Option=' + "Family").then(function (e) {
                    var count = 0;
                    count = e.data;
                    if (count !== "1") {
                        $scope.FamilyNameForClone = "[" + count + " Families]";
                    }
                    else {
                        $scope.FamilyNameForClone = dataItemId.CATEGORY_NAME;
                    }

                });
            }
            if (dataItemId.spriteCssClass === "familyClone" || dataItemId.spriteCssClass === "subfamilyClone") {
                $('#cutFamily').hide();

            }
            else {
                $('#cutFamily').show();
            }

        }
        else {
            $scope.RightClickedCategoryId = dataItemId.CATEGORY_ID;
            if ($scope.checkeditems.trim() === '') {
                if ($scope.FamilyNameForClone === '') {
                    $scope.FamilyNameForClone = dataItemId.CATEGORY_NAME;
                } else if ($scope.CopyCategoryName != $scope.FamilyNameForClone) {
                    $scope.FamilyNameForClone = $scope.CopyCategoryName;
                }
            }
            else {
                $scope.FamilyNameForClone = "";
                // if ($scope.FamilyNameForClone.trim() === "") {
                var items = $scope.checkeditems.split(',');
                if (items[0].contains('~')) {
                    $scope.FamilyNameForClone = "[" + items.length + " Families]";
                }
                else {
                    //var items = $scope.checkeditems.split(',');
                    $http.get('../Category/GetCategoryCount?MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&Option=' + "Category").then(function (e) {
                        var count = 0;
                        count = e.data;
                        if (count !== "1") {
                            $scope.FamilyNameForClone = "[" + count + " Categories]";
                        }
                        else {
                            $scope.FamilyNameForClone = dataItemId.CATEGORY_NAME;
                        }

                    });
                }
                // }
            }
        }
        if ($scope.RightClickedCategoryId.match("~")) {
            $("#custom-menuFam").css({ top: (e.pageY) + "px", left: (e.pageX) + "px" }).show();
            $("#custom-menu1").hide();
            $scope.pagexaxis = e.pageX;
            $scope.pageyaxis = e.pageY;
        } else {
            $("#custom-menu1").css({ top: (e.pageY) + "px", left: (e.pageX) + "px" }).show();
            $scope.pagexaxis = e.pageX;
            $scope.pageyaxis = e.pageY;
        }
        $scope.RightClickedCategoryName = dataItemId.CATEGORY_NAME;
        $scope.RightClickedCatalogID = dataItemId.CATALOG_ID;
        $scope.RightClickeddataItem = dataItemId;
        $scope.Showmasterids = $scope.checkeditems;
        $scope.Showmasteruncheckids = $scope.uncheckeditems;
        if (dataItemId.spriteCssClass === "familyClone") {
            $('#CloneRemoveFamily').show();
            $('#associationfamily').hide();
            $('#TrashFamily').hide();
            $('#SwitchMaster').show();
            $('#RemoveFamily').hide();
            
        }
        else if (dataItemId.spriteCssClass === "category") {
            // $('#Trash').show();
            //$('#CloneRemoveCategory').hide();
            $scope.clonedCategory = true;
            $('#CloneRemoveFamily').hide();
            $('#SwitchMaster').hide();
            $('#RemoveFamily').hide();
           
        }
        else if (dataItemId.spriteCssClass === "CategoryClone") {
            $('#Trash').hide();
            $('#CloneRemoveCategory').show();
            $('#ShowMaster').show();
            $('#association').css('display', 'none');
            $scope.clonedCategory = false;


            

        }
        else if (dataItemId.spriteCssClass === "subfamilyClone") {
            $('#CloneRemoveFamily').hide();
            $('#associationfamily').hide();
            $('#TrashFamily').hide();
            $('#SwitchMaster').hide();
            $('#RemoveFamily').hide();
           
        }
        else {
            $('#CloneRemoveFamily').hide();
            $('#RemoveFamily').show();
            $('#TrashFamily').show();
            $('#SwitchMaster').hide();
        }
        if ($rootScope.selecetedCatalogId.toString() !== '1' && $scope.SelectedCatalogId.toString() !== '1' && dataItemId.spriteCssClass !== "familyClone" && dataItemId.spriteCssClass !== "CategoryClone" && dataItemId.spriteCssClass !== "subfamilyClone") {
            $('#ShowMaster').show();
            $('#association').show();
            $('#AssociationRemoveFromNavigator').show();
            $('#associationfamily').show();
        }
        else {
            //$('#ShowMaster').hide();
            $('#association').hide();
            $('#AssociationRemoveFromNavigator').hide();
            $('#associationfamily').hide();
        }
        //$scope.$broadcast("Context_Category_Id", $scope.RightClickedCategoryId);
        if ($scope.RightClickedCategoryId.match("~")) {
            $rootScope.familycategory = $scope.RightClickeddataItem.id.split("~")[0];
            $scope.Showmasterids = "~" + $scope.RightClickeddataItem.id.split("~")[1];
        } else {
            $rootScope.familycategory = $scope.RightClickeddataItem.id;
            $scope.Showmasterids = $scope.RightClickeddataItem.id;
        }

    };

    $scope.Copycategoryclk = function (selectedrow) {

        $scope.HideMenu1();
        if (selectedrow == 'copycat') {
            $scope.copycategory = $scope.copycategoryvalueforclipboard;
            var cellText = $scope.copycategory;
            new Clipboard('#Copycategory', {
                text: function (itemtest) {
                    return cellText;
                }
            });

        }
    }

    $scope.Searchcategoryclk = function (selectedrow) {
        if (selectedrow == 'searchcat') {
            $scope.categoryvaluesearch = $scope.searchcategoryvalueclk;
            document.getElementById('txt_search').value = $scope.categoryvaluesearch;
            sessionStorage.setItem("searchText", $scope.categoryvaluesearch);
            Item = "Category";
            sessionStorage.setItem("Dropdownvalue", Item);

            if ($scope.categoryvaluesearch != "") {
                window.document.location = "/App/Search";
                return false;
            } else {
                window.document.location = "/App/Search";
                return false;
            }
        }

    }


    $scope.Copyfamilyclk = function (selectedrow) {

        $scope.HideMenuFam();
        if (selectedrow == 'copyfam') {
            $scope.copyfamily = $scope.copyfamilyvalueforclipboard;
            var cellText = $scope.copyfamily;
            new Clipboard('#Copyfamily', {
                text: function (itemtest) {
                    return cellText;
                }
            });

        }
    }

    $scope.Searchfamilyclk = function (selectedrow) {

        if (selectedrow == 'searchfam') {
            $scope.familyvaluesearch = $scope.searchfamilyvalueclk;
            document.getElementById('txt_search').value = $scope.familyvaluesearch;
            sessionStorage.setItem("searchText", $scope.familyvaluesearch);
            Item = "Family";
            sessionStorage.setItem("Dropdownvalue", Item);

            if ($scope.familyvaluesearch != "") {
                window.document.location = "/App/Search";
                return false;
            } else {
                window.document.location = "/App/Search";
                return false;
            }
        }

    }



    $scope.ShowHide = function () {
        $('#cutcategory').show();
        $('#copycategory').show();
        $('#remove').show();
        $('#association').show();
        $('#checkall').show();
        $('#uncheckall').show();
        $('#newfamily').show();
        $('#expandall').show();
        $('#collapseall').show();
        $('#referencetable').show();
        $('#rename').show();
        $('#find').show();
        $('#preview').show();
    };


    $scope.ContextMenuEmpty = function (e) {
        $('#EditFunction').show();
        $('#SelectFunction').show();
        if ($scope.RightClickedCategoryId !== undefined && $scope.RightClickedCategoryId.trim() !== '') {
            if ($rootScope.userCopypastedel === true) {
                $('#copycategory').show();

            } else {
                $('#copycategory').hide();

            }
            if ($rootScope.userCopypastedel === true) {
                $('#cutcategory').show();

            } else {
                $('#cutcategory').hide();

            }
            if ($scope.Clone === 1) {
                $('#CloneCategory').show();
            } else {
                $('#CloneCategory').hide();
                //$('#CloneasRooCategory').hide();
                //$('#PasteCategory').hide();
                //$('#PasteRootCategory').hide();
            }
            if ($scope.Clone === 2) {
                $('#PasteFamily').show();
            } else {
                $('#PasteFamily').hide();
            }
            if ($scope.CutFlag == '1') {
                $('#PasteFamily').show();
                $('#CloneFamily').hide();

            } else {
                $('#PasteFamily').hide();
                // $('#CloneFamily').show();
            }
            if ($scope.CopyFlag == '1') {
                //  $('#CloneFamily').show();
                $('#PasteFamily').show();
            }

            return;
        }
        else if (!$scope.categorycnt) {
            $("#custom-menu1").css({ top: (e.pageY) + "px", left: (e.pageX) + "px" }).show();
            $scope.pagexaxis = e.pageX;
            $scope.pageyaxis = e.pageY;
            $('#cutcategory').hide();
            $('#PasteCategory').hide();
            $('#copycategory').hide();
            $('#CloneFamily').hide();
            $('#PasteRootCategory').hide();
            $('#CloneCategory').hide();
            $('#CloneasRooCategory').hide();
            $('#remove').hide();
            $('#association').hide();
            $('#checkall').hide();
            $('#uncheckall').hide();
            $('#newfamily').hide();
            $('#expandall').hide();
            $('#collapseall').hide();
            $('#referencetable').hide();
            $('#rename').hide();
            $('#find').hide();
            $('#preview').hide();
        }

    };

    $scope.EmptyNavigatorRightClick = function (e) {

        if (!$scope.categorycnt) {
            $scope.Showmasterids = $scope.checkeditems;
            $scope.Showmasteruncheckids = $scope.uncheckeditems;
            $("#custom-menu1").css({ top: (e.pageY) + "px", left: (e.pageX) + "px" }).show();
            $("#custom-menu").hide();
            $scope.pagexaxis = e.pageX;
            $scope.pageyaxis = e.pageY;
            $('#PasteCategoryFamily').hide();
            $('#PasteFamily').hide();
            $('#cutcategory').hide();
            $('#PasteCategory').hide();
            $('#copycategory').hide();
            $('#CloneFamily').hide();
            $('#EditFunction').hide();
            $('#PasteRootCategory').hide();
            $('#CloneCategory').hide();
            $('#CloneasRooCategory').hide();
            $('#remove').hide();
            $('#association').hide();
            $('#checkall').hide();
            $('#uncheckall').hide();
            $('#newfamily').hide();
            $('#expandall').hide();
            $('#collapseall').hide();
            $('#referencetable').hide();
            $('#rename').hide();
            $('#find').hide();
            $('#preview').hide();
            $('#SelectFunction').hide();

        }
    };

    $scope.HideMenuFam = function (e, dataItemId) {
        $("#custom-menuFam").hide();
    };

    $scope.HideMenu1 = function (e, dataItemId) {
        $("#custom-menu1").hide();
    };
    $scope.SwitchMaster = function () {
        $scope.AssignMaster();
    };

    $scope.selectedRenameText = "";
    $scope.RenameContent = function () {
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        //$("#renamearea").css({ top: ($scope.pageyaxis) + "px", left: ($scope.pagexaxis) + "px" }).show();
        $("#renamearea").show();
        var splitIds = "";
        $scope.selectedRenameText = $scope.RightClickedCategoryName.split("(")[0].trim();
        if ($scope.RightClickedCategoryId.includes('CAT') && $rootScope.SelectedNodeID.includes('~')) {

            $rootScope.familyDeleteNavStatus = true;
            angular.forEach($rootScope.SelectedNodeID.split('~'), function (value) {
                if (!value.includes('CAT') && !value.includes('!') && value != " " && value != "") {
                    splitIds = splitIds + value;
                }

            });

            //  $rootScope.SelectedNodeID = $rootScope.SelectedNodeID.replace(splitIds, "");
        }


    };
    $scope.CloseRename = function () {
        $("#renamearea").hide();
        $rootScope.treeData.read();
    };
    $scope.CloseFindText = function () {
        $("#searchLeftNavi").hide();
        $rootScope.treeData.read();
    };
    $scope.renameUpdateBtn = function () {

        $("#renamearea").hide();

        if ($scope.selectedRenameText.trim() != "") {
            $http.get('../Category/RenameFromContextMenu?currenttext=' + $scope.selectedRenameText + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogID=' + $scope.RightClickedCatalogID).then(function () {

                $rootScope.selectedFAmilyList = $rootScope.SelectedNodeID;
                if ($rootScope.SelectedNodeID.includes('~!')) {

                    if ($rootScope.SelectedNodeID.includes('~')) {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
                    }
                    else {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0].replace('~', '');
                    }
                }
                else if ($rootScope.SelectedNodeID.includes('~')) {
                    $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
                }

                //$rootScope.treeForCategory = true;
                //$rootScope.familyDeleteNavStatus = true;
                $rootScope.SelectedNodeIDPresist = 1;
                $rootScope.treeData.read();
            });
        }
    };
    $scope.FindFromLeftNav = function () {
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $("#searchLeftNavi").show();
        $("#leftNavTreeViewKendoNavigator").data("kendoTreeView").expand(".k-item");

    };
    $rootScope.CutCategoryId = '';
    $scope.CutFlag = '0';
    $scope.CopyFlag = '0';
    $scope.CutCategory = function () {

        $scope.Clone = 0;
        $scope.CutFlag = '1';
        $rootScope.CutCategoryId = $scope.RightClickedCategoryId;
        $scope.PreviousCatalogID = $scope.RightClickedCatalogID;
        $scope.IdsForAnotherCatalog = $scope.checkeditems;
        $("#" + $scope.RightClickedCategoryId).hide();
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $('#PasteCategory').show();
        $('#PasteRootCategory').show();
        $('#CloneCategory').hide();
        $('#CloneasRooCategory').hide();
    };
    $scope.CopyCategoryId = '';
    $scope.Clone = 0;
    $scope.CopyCategory = function () {
        $scope.Clone = 1;
        $scope.CopyFlag = '1';
        $scope.CopyCategoryId = $scope.RightClickedCategoryId;
        $scope.PreviousCatalogID = $scope.RightClickedCatalogID;
        $scope.IdsForAnotherCatalog = $scope.checkeditems;
        $scope.CopyCategoryName = $scope.RightClickedCategoryName;
        //$("#" + $scope.RightClickedCategoryId).hide();
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $scope.PasteFlag = '0';

        //if ($rootScope.userCopypastedel === true) {
        //    $('#PasteCategory').show();

        //} else {
        //    $('#PasteCategory').hide();
        $('#PasteCategory').show();
        $('#PasteRootCategory').show();
        $('#CloneCategory').show();
        $('#CloneasRooCategory').show();
        // $('#PasteFamily').hide();
        $('#CloneFamily').hide();

        if ($rootScope.ProductCount >= ($rootScope.ProductSKUCount * ($rootScope.SKUAlertPercentage / 100))) {

            if ($rootScope.ProductCount > 0 && $rootScope.ProductSKUCount > 0) {
                $rootScope.SKUAlertPercentageCalculated = Math.round(($rootScope.ProductCount / $rootScope.ProductSKUCount) * 100);
                $scope.skuAlertOptions.refresh({ url: "../views/app/partials/skuAlert.html" });
                $scope.skuAlertOptions.center().open();
            }
        }
    };
    $scope.CopyFromContextMenu = function () {

        $scope.Clone = 2;
        $scope.CopyFlag = '1';
        $scope.CopiedId = $scope.RightClickedCategoryId;
        $scope.PreviousCatalogID = $scope.RightClickedCatalogID;
        $scope.IdsForAnotherCatalog = $scope.checkeditems;
        $scope.CopyCategoryName = $scope.RightClickedCategoryName;
        //$("#" + $scope.RightClickedCategoryId).hide();
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        $('#PasteFamily').show();
        //if ($rootScope.userClone === true) {
        //    $('#CloneFamily').show();

        //    } else {
        //    $('#CloneFamily').hide();
        $('#CloneFamily').show();
        $('#PasteCategoryFamily').show();
        $('#PasteCategory').hide();
        $('#PasteRootCategory').hide();
        $('#CloneasRooCategory').hide();
        if ($rootScope.ProductCount >= ($rootScope.ProductSKUCount * ($rootScope.SKUAlertPercentage / 100))) {

            if ($rootScope.ProductCount > 0 && $rootScope.ProductSKUCount > 0) {
                $rootScope.SKUAlertPercentageCalculated = Math.round(($rootScope.ProductCount / $rootScope.ProductSKUCount) * 100);
                if ($rootScope.SKUAlertPercentage != 100) {
                    $scope.skuAlertOptions.refresh({ url: "../views/app/partials/skuAlert.html" });
                    $scope.skuAlertOptions.center().open();
                }
            }
        }
    };
    $scope.CutFromContextMenu = function () {

        $scope.CutFlag = '1';
        $scope.CopiedId = $scope.RightClickedCategoryId;
        $scope.PreviousCatalogID = $scope.RightClickedCatalogID;
        $scope.IdsForAnotherCatalog = $scope.checkeditems;
        $('#PasteFamily').show();
        $('#CloneFamily').show();
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();


        $('#PasteCategoryFamily').show();
    };
    $scope.PasteCategoryId = '';
    $scope.PreviousCatalogID = '';
    $scope.PasteCategory = function () {

        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }
        $rootScope.treeForCategory = true;
        $rootScope.familyDeleteNavStatus = true;
        $rootScope.SelectedNodeIDPresist = 1;
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        if ($scope.CutFlag == '1') {
            $scope.PasteCategoryId = $scope.RightClickedCategoryId;
            if ($rootScope.CutCategoryId != "" && $rootScope.CutCategoryId != null) {
                $http.get('../Category/CutPasteCategory?CutCatId=' + $rootScope.CutCategoryId + '&&PasteCatId=' + $scope.PasteCategoryId + '&&catalogId=' + $rootScope.selecetedCatalogId + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog)
                    .then(function (response) {
                        if (response.data === "True") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Action Completed.',
                                type: "info"
                            });
                            $scope.RefreshLeftNavTree();
                            $scope.IdsForAnotherCatalog = '';
                        }
                        else if (response.data === "False") {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Action Completed.',
                                type: "info"
                            });
                            $scope.RefreshLeftNavTree();
                            $scope.IdsForAnotherCatalog = '';
                        }
                    });
            }
            //}
        } else if ($scope.CopyFlag == '1') {
            $scope.PasteOptions.refresh({ url: "../views/app/partials/Pasteoptions.html" });
            $scope.PasteOptions.center().open();
            $scope.Rootflag = 'Sub';

            //$scope.PasteCategoryId = $scope.RightClickedCategoryId;

            //$http.get('../Category/CopyPasteCategory?copyCatId=' + $scope.CopyCategoryId + '&&PasteCatId=' + $scope.PasteCategoryId + '&&catalogId=' + $rootScope.selecetedCatalogId + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&PasteFlag=' + $scope.PasteFlag)
            //    .then(function () {
            //        $scope.RefreshLeftNavTree();
            //    });
            //}
        }
        //$rootScope.treeData.read();
        $scope.init();
        $scope.CutFlag = '0';
        $scope.CopyFlag = '0';
        $scope.PasteFlag = '0';
        $scope.Clone = 0;
    };
    $scope.PasteCategoryFamily = function () {

        //$rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0].replace($scope.SelectedItem, '');
        //$scope.SelectedItem = $scope.SelectedItem + $scope.CopiedId;
        //$scope.SelectedNodeID = $scope.SelectedItem;
        //$rootScope.familyDeleteNav = $rootScope.SelectedNodeID;
        //$rootScope.familyDeleteNavStatus = true;
        //$rootScope.SelectedNodeIDPresist = 1;
        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }

        $rootScope.familyDeleteNavStatus = true;
        $rootScope.SelectedNodeIDPresist = 1;
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        if ($scope.CutFlag == '1') {
            $scope.PasteCategoryId = $scope.RightClickedCategoryId;
            if ($scope.CopiedId.match("~")) {
                $http.get('../Family/CutPasteFamily?CopiedId=' + $scope.CopiedId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogID=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&CatIdPartsKey=' + $localStorage.CategoryID)
                    .then(function (response) {

                        if (response.data === "True") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Action Completed.',
                                type: "info"
                            });
                        }
                        else if (response.data === "False") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Action not completed, please try again.',
                                type: "info"
                            });
                        }
                        $scope.checkeditems = '';
                        $scope.IdsForAnotherCatalog = '';

                        $scope.RefreshLeftNavTree();
                    });
            }
            //else
            //{
            //$http.get('../Category/CutPasteCategory?CutCatId=' + $scope.CutCategoryId + '&&PasteCatId=' + $scope.PasteCategoryId)
            //    .then(function () {
            //    });
            //}
        } else if ($scope.CopyFlag == '1') {
            blockUI.stop();
            $http.get('../Family/SubproductCheck?family_id=' + $scope.CopiedId + '&multipleFamilyids=' + $scope.checkeditems + '&multipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&catalog_id=' + $scope.PreviousCatalogID + '&option=' + "Family")
                .then(function (response) {

                    if (response.data != "False" && ($scope.RightClickedCatalogID !== $scope.PreviousCatalogID || $rootScope.AllowDuplicateItem_PartNum)) {

                        $.msgBox({
                            title: "CatalogStudio",
                            content: "Do you want to paste the selected option?",
                            type: "confirm",
                            buttons: [{ value: "Yes" }, { value: "No" }],
                            success: function (result) {
                                var bool = false;
                                if (result === "Yes") {
                                    bool = true;
                                }

                                $scope.PasteCategoryId = $scope.RightClickedCategoryId;
                                if ($scope.CopiedId.match("~")) {
                                    $http.get('../Family/CopyPasteFamily?CopiedId=' + $scope.CopiedId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogID=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&SubProductCFM=' + bool)
                                        .then(function (response) {
                                            //if (response.data === 'false') {
                                            //    $.msgBox({
                                            //        title: $localStorage.ProdcutTitle,
                                            //        content: '' + response.data + '.',
                                            //        type: "error"
                                            //    });
                                            //}
                                            //else {
                                            //    
                                            //    $.msgBox({
                                            //        title: $localStorage.ProdcutTitle,
                                            //        content: '' + response.data + '.',
                                            //        type: "info"
                                            //    });
                                            //}
                                            $scope.getUserProductCount();
                                            // $scope.getUserRoleRightsProductTable();
                                            $scope.checkeditems = '';
                                            $scope.IdsForAnotherCatalog = '';
                                            $scope.RefreshLeftNavTree();
                                        });
                                }
                            }

                        });
                    }
                    else {

                        $scope.PasteCategoryId = $scope.RightClickedCategoryId;
                        if ($scope.CopiedId.match("~")) {

                            $http.get('../Family/CopyPasteFamily?CopiedId=' + $scope.CopiedId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogID=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&SubProductCFM=' + false)
                                .then(function (response) {
                                    if (response.data === "Pasted Failed") {
                                        //$.msgBox({
                                        //    title: $localStorage.ProdcutTitle,
                                        //    content: '' + response.data + '.',
                                        //    type: "info"
                                        //});
                                    }
                                    else {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response.data + '.',
                                            type: "error"
                                        });
                                    }
                                    $scope.getUserProductCount();
                                    // $scope.getUserRoleRightsProductTable();
                                    $scope.checkeditems = '';
                                    $scope.IdsForAnotherCatalog = '';
                                    $scope.RefreshLeftNavTree();
                                });
                        }
                    }
                });
        }
        $scope.init();
        $scope.CutFlag = '0';
        $scope.CopyFlag = '0';
        $scope.Clone = 0;
    };
    $scope.familyCheck = '';
    $scope.PasteFamily = function () {

        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }

        $rootScope.familyDeleteNavStatus = true;
        $rootScope.SelectedNodeIDPresist = 1;
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        if ($scope.CutFlag == '1') {
            $scope.PasteCategoryId = $scope.RightClickedCategoryId;
            if ($scope.CopiedId.match("~")) {
                $http.get('../Family/CutPasteFamily?CopiedId=' + $scope.CopiedId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogID=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&CatIdPartsKey=' + $localStorage.CategoryID)
                    .then(function () {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Action Completed.',
                            type: "info"
                        });
                        $scope.RefreshLeftNavTree();
                        $scope.IdsForAnotherCatalog = '';
                    });
            }
        }
        else if ($scope.CopyFlag == '1') {
            $http.get('../Family/SubproductCheck?family_id=' + $scope.CopiedId + '&multipleFamilyids=' + $scope.checkeditems + '&multipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&catalog_id=' + $scope.PreviousCatalogID + '&option=' + "Family")
                   .then(function (response) {

                       if (response.data != "False" && $scope.RightClickedCatalogID !== $scope.PreviousCatalogID || $rootScope.AllowDuplicateItem_PartNum) {

                           $.msgBox({
                               title: " CatalogStudio",
                               content: "Do you want to paste the selected option?",
                               type: "confirm",
                               buttons: [{ value: "Yes" }, { value: "No" }],
                               success: function (result) {
                                   var bool = false;
                                   if (result === "Yes") {
                                       bool = true;
                                   }
                                   $scope.PasteCategoryId = $scope.RightClickedCategoryId;
                                   if ($scope.CopiedId.match("~")) {
                                       $http.get('../Family/CopyPasteFamily?CopiedId=' + $scope.CopiedId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogID=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&SubProductCFM=' + bool)
                                           .then(function (response) {

                                               $scope.getUserProductCount();
                                               //  $scope.getUserRoleRightsProductTable();
                                               $scope.familyCheck = response.data;
                                               //if (response.data === "false") {
                                               //    $.msgBox({
                                               //        title: $localStorage.ProdcutTitle,
                                               //        content: '' + response.data + '.',
                                               //        type: "info"
                                               //    });
                                               //}
                                               //else {
                                               //    $.msgBox({
                                               //        title: $localStorage.ProdcutTitle,
                                               //        content: '' + response.data + '.',
                                               //        type: "error"
                                               //    });
                                               //}
                                               $scope.IdsForAnotherCatalog = '';
                                               $scope.RefreshLeftNavTree();
                                           });
                                   }
                               }

                           });
                       }
                       else {

                           $scope.PasteCategoryId = $scope.RightClickedCategoryId;
                           if ($scope.CopiedId.match("~")) {
                               $http.get('../Family/CopyPasteFamily?CopiedId=' + $scope.CopiedId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogID=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&SubProductCFM=' + false)
                                   .then(function (response) {
                                       $scope.getUserProductCount();
                                       //  $scope.getUserRoleRightsProductTable();
                                       $scope.familyCheck = response.data;
                                       //if (response.data === "Pasted Failed") {
                                       //    //$.msgBox({
                                       //    //    title: $localStorage.ProdcutTitle,
                                       //    //    content: '' + response.data + '.',
                                       //    //    type: "info"
                                       //    //});

                                       //}
                                       //else {
                                       //    $.msgBox({
                                       //        title: $localStorage.ProdcutTitle,
                                       //        content: '' + response.data + '.',
                                       //        type: "error"
                                       //    });

                                       //}

                                       $scope.IdsForAnotherCatalog = '';
                                       $scope.RefreshLeftNavTree();
                                   });
                           }
                       }
                   });
        }
        $scope.init();
        $scope.CutFlag = '0';
        $scope.CopyFlag = '0';
        //  $rootScope.dropDownTreeViewParnetCategoryDatasource.read();

    };


    $scope.PasteRootCategory = function () {
        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();
        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }
        $rootScope.treeForCategory = true;
        $rootScope.familyDeleteNavStatus = true;
        $rootScope.SelectedNodeIDPresist = 1;
        if ($scope.CutFlag == '1') {
            $scope.PasteCategoryId = $scope.RightClickedCategoryId;
            if ($rootScope.CutCategoryId.match("~") || $rootScope.CutCategoryId === "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Selected function is not available for this family.',
                    type: "info"
                });

            } else {

                $http.get('../Category/CutPasteCategory?CutCatId=' + $rootScope.CutCategoryId + '&&PasteCatId=0&&catalogId=' + $rootScope.selecetedCatalogId + '&&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog)
                    .then(function (e) {
                        if (e.data === "True") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Action Completed.',
                                type: "info"
                            });

                        }
                        else if (e.data === "False") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Action not completed, please try again.',
                                type: "error"
                            });
                        }
                        $scope.RefreshLeftNavTree();
                        $scope.IdsForAnotherCatalog = '';
                    });
            }
        } else if ($scope.CopyFlag == '1') {
            $scope.PasteOptions.refresh({ url: "../views/app/partials/Pasteoptions.html" });
            $scope.PasteOptions.center().open();
            $scope.Rootflag = 'Root';
        }

        $scope.init();
        $scope.CutFlag = '0';
        $scope.CopyFlag = '0';
    };

    $rootScope.tblDashBoardss = new ngTableParams({ page: 1, count: 10 },
       {

           counts: [], //5, 10, 25, 50, 100
           sorting: {
               SORT: 'asc'     // initial sorting
           },
           total: function () { return $rootScope.SubprodData.length; },
           $scope: { $data: {} },
           getData: function ($defer, params) {

               var filteredData = $rootScope.SubprodData;
               var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) :
               filteredData;
               return $defer.resolve(orderedData);
           }
       });

    //$scope.tblDashBoard = new ngTableParams(
    //    { page: 1, count: 1000 },
    //    {
    //        counts: [], //5, 10, 25, 50, 100
    //        sorting: {
    //            SORT: 'asc'     // initial sorting
    //        },
    //        total: function () { return $scope.prodData.length; },
    //        $scope: { $data: {} },
    //        getData: function ($defer, params) {
    //            var filteredData = $scope.prodData;
    //            var orderedData = filteredData;
    //            params.total(orderedData.length);
    //            return $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    //        }
    //    });
    function onChange(e) {
        $scope.callProductGridPagePer(e.sender.text().trim());
    };
    $(".MainitemSelect").kendoDropDownList({
        change: onChange,
        dataSource: [
            { Value: "5" },
            { Value: "10" },
            { Value: "25" },
            { Value: "50" },
            { Value: "100" }
        ],
        dataTextField: "Value",
        dataValueField: "Value",

    });
    $scope.displayID = "true";
    // $scope.ProdCurrentPage = 1;
    $scope.ProdCountPerPage = "10";
    $scope.gridCurrentPage = "10";
    $scope.callProductGridPagePer = function (pageper) {
        if (pageper != null && pageper != "0") {

            $scope.ProdCountPerPage = pageper;
            $scope.ProdCurrentPage = "1";
            $scope.productGridSearchValue = "";

            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedCategoryId, $localStorage.CategoryID);

        }


    };

    $scope.callProductGridPagePerItem = function (pageper) {
        if (pageper != null && pageper != "0") {
            dataFactory.getCategoryFamilyId($rootScope.selecetedCatalogId, $scope.SelectedCategoryId).success(function (response) {

                if (response == '0') {
                    $scope.Category_IDs = response.m_Item1;
                    $scope.Family_IDs = response.m_Item2;
                } else {
                    $scope.Category_IDs = response.m_Item1;
                    if (response.m_Item2 == "") {
                        $scope.Family_IDs = 0;
                    }
                    else {
                        $scope.Family_IDs = response.m_Item2;
                    }

                }

                $scope.ProdCountPerPage = pageper;
                $scope.ProdCurrentPage = "1";
                $scope.productGridSearchValue = "";

                $rootScope.LoadProdItemData($rootScope.selecetedCatalogId, $scope.Family_IDs, $localStorage.CategoryID);

            });

        }


    };
    $scope.callProductGridPaging = function (pageno) {
        if (pageno != null && pageno <= $scope.totalProdPageCount && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {

            if (pageno == 'PREV' && $scope.ProdCurrentPage != 1) {
                $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) - 1;
            } else if (pageno == 'PREV' && $scope.ProdCurrentPage == 1) {
                $scope.ProdCurrentPage = "1";
                //new
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                return;
            } else if (pageno == 'NEXT' && $scope.totalProdPageCount != 1) {
                $("#cboProdPageCountup").data("kendoDropDownList").text("");
                $("#cboProdPageCountdown").data("kendoDropDownList").text("");
                $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) + 1;
                if ($scope.ProdCurrentPage > $scope.totalProdPageCount) {
                    $scope.ProdCurrentPage = $scope.totalProdPageCount;
                }
            }
                //new
            else if (pageno == 'NEXT' && $scope.totalProdPageCount == 1) {
                $scope.ProdCurrentPage = "1";
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                return;
            }

            else if ($scope.totalProdPageCount == 1) {
                $scope.ProdCurrentPage = "1";
                $scope.ProdCurrentPage = "1";
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                return;
            }
            else {

                $("#cboProdPageCountup").data("kendoDropDownList").text("");
                $("#cboProdPageCountdown").data("kendoDropDownList").text("");
                //$scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) + 1;
                $timeout(($scope.ProdCurrentPage = pageno), 500);


                $scope.ProdCurrentPage = "1";
                $scope.ProdCurrentPage = pageno;
                $rootScope.ProdPage = pageno;
            }

            $rootScope.GetAllLoadDataBasedonViews();
        }
    };

    //Pagination For ProductItem Grid in Category
    $scope.callProductGridPagingItem = function (pageno) {
        if (pageno != null && pageno <= $scope.totalProdPageCount && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {

            if (pageno == 'PREV' && $scope.ProdCurrentPage != 1) {
                $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) - 1;
            } else if (pageno == 'PREV' && $scope.ProdCurrentPage == 1) {
                $scope.ProdCurrentPage = "1";
                //new
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                return;
            } else if (pageno == 'NEXT' && $scope.totalProdPageCount != 1) {
                $("#cboProdPageCountup").data("kendoDropDownList").text("");
                $("#cboProdPageCountdown").data("kendoDropDownList").text("");
                $scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) + 1;
                if ($scope.ProdCurrentPage > $scope.totalProdPageCount) {
                    $scope.ProdCurrentPage = $scope.totalProdPageCount;
                }
            }
            else if (pageno == 'NEXT' && $scope.totalProdPageCount == 1) {
                $scope.ProdCurrentPage = "1";
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                return;
            }

            else if ($scope.totalProdPageCount == 1) {
                $scope.ProdCurrentPage = "1";
                $scope.ProdCurrentPage = "1";
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                return;
            }
            else {

                $("#cboProdPageCountup").data("kendoDropDownList").text("");
                $("#cboProdPageCountdown").data("kendoDropDownList").text("");
                $timeout(($scope.ProdCurrentPage = pageno), 500);


                $scope.ProdCurrentPage = "1";
                $scope.ProdCurrentPage = pageno;
            }

            $scope.GetAllLoadDataBasedonViewsItem();
        }
    };
    //Pagination For ProductItem Grid in Category

    $scope.GetAllLoadDataBasedonViewsItem = function () {
        $rootScope.LoadProdItemData($rootScope.selecetedCatalogId, $scope.Family_IDs, $scope.Category_IDs);
    }
    $rootScope.LoadProdData = function (catalogId, id, cat_id, pack_Id, Type, Attributes, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues) {

        var clickLoadFamilyID = []; // mariyaviji
        $scope.changeProdPage = "1";//JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
        if (id.toString() == "") {
            id = $scope.Family.FAMILY_ID;
        }
        if (id == 0) {
            id = $scope.Family.FAMILY_ID;
        }
        $rootScope.val = false;

        if (id.toString().includes("~")) {
            if (id.startsWith("~")) {
                clickLoadFamilyID = id.split("~");
            }
            else {
                clickLoadFamilyID = id;
            }
        } else {
            clickLoadFamilyID = id;
        }
        if (clickLoadFamilyID.length == 2) {
            $rootScope.ClickFamilyIDValue = clickLoadFamilyID[1];
        }
        else {
            $rootScope.ClickFamilyIDValue = null;
        }

        $scope.sub_id = '0';
        $rootScope.sub_cat_id = cat_id;
        $rootScope.sub_fam_id = id;
        //dataFactory.getprodspecs(catalogId, id, $scope.displayID).success(function (response) {
        //document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaginghighlight";
        var ProdCount = [];
        dataFactory.getprodspecsWithoutAttributes(catalogId, id, true, cat_id).success(function (response) {
            if (response.Data.Columns.length) {
                var obj = jQuery.parseJSON(response.Data.Data);
                var ProdEditPageloopcnt = Math.floor(obj.length / parseInt($scope.ProdCountPerPage));
                if ((obj.length % parseInt($scope.ProdCountPerPage)) > 0) {
                    ProdEditPageloopcnt = ProdEditPageloopcnt + 1;
                }
                $scope.changeProdPage = "1";
                for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                    ProdCount.push(i);
                }
                $scope.ProdPageCount = ProdCount;
                if ($scope.ProdCurrentPage == "1") {
                    $scope.changeProdPage = "1";//JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                } else {
                    $scope.changeProdPage = $scope.ProdCurrentPage;
                }
                //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                // $scope.ProdCurrentPage = $scope.changeProdPage;
                $scope.totalProdPageCount = ProdEditPageloopcnt;
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
            }
            if (Attributes != undefined) {
                if (Attributes.includes('#')) {
                    Attributes = Attributes.replace('#', 'hashValue')  // have to change # values 
                }
            }
            if ($rootScope.val == undefined) {
                $rootScope.val = false;
            }
            dataFactory.getprodspecs(catalogId, id, true, cat_id, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.sub_id, pack_Id, Type, Attributes, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues, $rootScope.val).success(function (response) {
                var productDetails = jQuery.parseJSON(response.Data.Data);


                var obj = productDetails.Table;
                $scope.prodData = obj;
                $scope.TotalTableCountitem = productDetails.Table.length;
                if (productDetails.Table1 != undefined) {
                    $scope.TotalTableCount = productDetails.Table1[0].Column1;
                    if ($rootScope.TypeOfProduct = "searchProduct") {
                        var ProdCount = [];
                        var ProdEditPageloopcnt = Math.floor($scope.TotalTableCount / parseInt($scope.ProdCountPerPage));
                        if (($scope.TotalTableCount % parseInt($scope.ProdCountPerPage)) > 0) {
                            ProdEditPageloopcnt = ProdEditPageloopcnt + 1;
                        }
                        $scope.changeProdPage = "1";
                        for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                            ProdCount.push(i);
                        }
                        $scope.ProdPageCount = ProdCount;
                        if ($scope.ProdCurrentPage == "1") {
                            $scope.changeProdPage = "1";//JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                        } else {
                            $scope.changeProdPage = $scope.ProdCurrentPage;
                        }
                        //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                        // $scope.ProdCurrentPage = $scope.changeProdPage;
                        $scope.totalProdPageCount = ProdEditPageloopcnt;
                        $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                        $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;

                    }


                }

                // To Hide All the product Grid Hide Function
                if ($scope.prodData.length == 0) {
                    $scope.$broadcast("ToHideAllTheProductGridFunctions", "some data");
                }
                if ($scope.prodData.length == 0 && $rootScope.deleteProduct == true && $scope.ProdCurrentPage != 1) {
                    $scope.ProdCurrentPage = $scope.ProdCurrentPage - 1;
                    $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                    $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                    $scope.LoadProdData(catalogId, id, cat_id);
                }
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                //For Product Edit

                //To Change Dynamice CatalogNo
                for (var i = 0; i < response.Data.Columns.length; i++) {
                    if (response.Data.Columns[i]["Caption"].includes("ITEM#")) {
                        response.Data.Columns[i]["Caption"] = response.Data.Columns[i]["Caption"].replace("ITEM#", $localStorage.CatalogItemNumber);
                    }
                }
                $scope.columnsForAtt = response.Data.Columns;
                $scope.TotalTableCount = productDetails.Table.length;
                // To push the values into arry for split only attributes in drop down list.
                if (Type != 'product') {
                    var AttributeValues = [];
                    for (var i = 0 ; $scope.columnsForAtt.length > i ; i++) {
                        if (
                            $scope.columnsForAtt[i].Caption == "CATALOG_ID" ||
                            $scope.columnsForAtt[i].Caption == "FAMILY_ID" ||
                            $scope.columnsForAtt[i].Caption == "PRODUCT_ID" ||
                            $scope.columnsForAtt[i].Caption == "SORT" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2WEB" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2PRINT" ||
                            $scope.columnsForAtt[i].Caption == "WORKFLOW STATUS" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2PDF" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2EXPORT" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2PORTAL" ||
                            $scope.columnsForAtt[i].Caption == "SubProdCount"
                            ) {
                        } else {
                            if ($scope.columnsForAtt[i].Caption.includes("__OBJ")) {
                                AttributeValues.push({ "Value": i, "Name": $scope.columnsForAtt[i].Caption.slice(0, $scope.columnsForAtt[i].Caption.search("__OBJ")) });
                            } else {
                                AttributeValues.push({ "Value": i, "Name": $scope.columnsForAtt[i].Caption });
                            }
                        }
                    }
                    // To remove extra duplicate values (__)
                    for (var i = 0 ; AttributeValues.length > i ; i++) {
                        if (AttributeValues[i].Name.includes("__")) {
                            AttributeValues.splice(i, 1);
                        }
                    }
                    $scope.AttributeValues = AttributeValues;
                    $localStorage.GetAllAttributesasSelected = AttributeValues;
                    $scope.GetAttribute($scope.AttributeValues);
                }
                $("#divProductGrid").show();

                angular.forEach($scope.prodData, function (value) {
                    value.SORT = parseFloat(value.SORT);
                });
                //  $("#selectedattribute").show();
                //$("#divProductGrid").hide();
                //angular.forEach($scope.prodData, function (value) {
                //    value.SORT = parseFloat(value.SORT);
                //   //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                //    // $scope.changeProdPage = $scope.ProdCurrentPage;
                //    //start
                //    $scope.ProdCurrentPage == 1;
                //    if ($scope.ProdCurrentPage == "1") {
                //        $scope.changeProdPage = "1";//JOTHIPRIYA
                //    } else {
                //        $scope.changeProdPage = $scope.ProdCurrentPage;

                //    }
                //    //$rootScope.selectedfamily_id = $scope.Family.FAMILY_ID;
                //    //end
                //    //$scope.callProductGridPaging($scope.ProdCurrentPage);

                //});

                // $rootScope.visibleProjects = $scope.prodData;  $localStorage.CatalogItemNumber;
                // $rootScope.visibleProjects1 = $scope.prodData;
                // $data = $scope.prodData;
                $rootScope.Mainprodcount = $scope.prodData.length;
                if ($scope.prodData.length === 0) {
                    $scope.newProductBtnZero = true;
                    $scope.newProductBtn = false;
                    $scope.Itemcountid = false;
                    $scope.productpaging = false;
                    $scope.noresultfound = true;
                    $("#divProductGrid").hide();
                    $("#selectedattribute").hide();
                    $("#newProductBtn").css("display", "block");
                    $("#newProductBtn1").css("display", "none");
                }
                else {
                    $scope.noresultfound = false;
                    $scope.productpaging = true;
                    $scope.Itemcountid = true;
                    $scope.newProductBtnZero = false;
                    $scope.newProductBtn = true;
                    $("#newProductBtn").css("display", "none");
                    $("#newProductBtn1").css("display", "block");
                }
                //$scope.tblDashBoard.reload();
                // $scope.tblFamilyGrid.reload();
                // $scope.tblFamilyGrid.$params.page = 1;
                if ($scope.userRoleDeleteFamily) {
                    // $('#user-toolbar-association-removal-family').removeAttr('disabled');
                    $('#user-toolbar-delete-family').removeAttr('disabled');
                }
                else {
                    // $('#user-toolbar-association-removal-family').attr('disabled', 'disabled');
                    $('#user-toolbar-delete-family').attr('disabled', 'disabled');
                }

                if ($scope.userRoleAddFamily) {
                    $('#user-toolbar-create-new-family').removeAttr('disabled');
                }
                else {
                    $('#user-toolbar-create-new-family').attr('disabled', 'disabled');
                }
                if ($scope.userRoleModifyFamily) {
                    $('#user-toolbar-save-family').removeAttr('disabled');
                    $('#user-toolbar-family-attribute-setup').removeAttr('disabled');
                }
                else {
                    $('#user-toolbar-save-family').attr('disabled', 'disabled');
                    $('#user-toolbar-family-attribute-setup').attr('disabled', 'disabled');
                }
                //  var toolbarProduct = $("#product-toolbar").data("kendoToolBar");
                if ($scope.userRoleAddProductTable) {
                    $('#newbtn').removeAttr('disabled');
                }
                else {
                    $('#newbtn').attr('disabled', 'disabled');
                }

                if ($rootScope.ProductCount < $rootScope.ProductSKUCount) {
                }
                else {
                    $scope.userRoleAddProductTable = false;
                }

                if ($rootScope.Productnew == true) {
                    $('#newbtn').attr('disabled', 'disabled');
                }
                else {
                    $('#newbtn').removeAttr('disabled');
                }
                if ($scope.userRoleModifyProductTable) {
                    $('#columnsetubbtn').removeAttr('disabled');
                    $('#user-toolbar-default-layout').removeAttr('disabled');
                }
                else {
                    $('#columnsetubbtn').attr('disabled', 'disabled');
                    $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                }
                //if ($scope.userRoleDeleteFamily) {
                //    toolbar.enable("#user-toolbar-association-removal-family", true);
                //    toolbar.enable("#user-toolbar-delete-family", true);
                //}
                //else {
                //    toolbar.enable("#user-toolbar-association-removal-family", false);
                //    toolbar.enable("#user-toolbar-delete-family", false);
                //}

                if ($scope.prodData.length > 0) {
                    // $('#productpaging').show();
                    // $('#productpaging1').show();
                    $('#user-toolbar-export').removeAttr('disabled');
                    $('#user-toolbar-table-designer').removeAttr('disabled');
                    $('#user-toolbar-set-table-layout').removeAttr('disabled');
                    $('#user-toolbar-delete-layout').removeAttr('disabled');
                    $('#user-toolbar-create-new-layout').removeAttr('disabled');
                    if ($scope.userRoleModifyProductTable) {
                        $('#user-toolbar-default-layout').removeAttr('disabled');
                    }
                    else {
                        $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                    }
                    if (!$rootScope.EditMode) {
                        $('#user-toolbar-export').attr('disabled', 'disabled');
                        $('#user-toolbar-table-designer').attr('disabled', 'disabled');
                    }
                }
                else {
                    $('#productpaging').hide();
                    $('#productpaging1').hide();
                    $('#user-toolbar-export').attr('disabled', 'disabled');
                    $('#user-toolbar-table-designer').attr('disabled', 'disabled');
                    $('#user-toolbar-set-table-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-delete-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-create-new-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                }
                if (document.getElementById('myButton1').innerText = "Show Less") {
                    document.getElementById('myButton1').innerText = "Show More"
                }
                if ($rootScope.userprodcongig === true) {
                    $('#addtoprdconfig').removeAttr('disabled');
                } else {
                    $('#addtoprdconfig').attr('disabled', 'disabled');
                }
                if ($rootScope.userprodcongig === true) {
                    $('#prodconfig').removeAttr('disabled');
                } else {
                    $('#prodconfig').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#cutbtn').removeAttr('disabled');
                } else {
                    $('#cutbtn').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#copybtn').removeAttr('disabled');
                } else {
                    $('#copybtn').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#pastebtn').removeAttr('disabled');
                } else {
                    $('#pastebtn').attr('disabled', 'disabled');
                }
                if ($rootScope.DisplayIdcolumns === true) {
                    $scope.DisplayIdcolumns == true;
                    $scope.userRoleViewSystemid = true;
                }

                blockUI.stop();
                $("#accordions").accordion({ header: "> div > h3", collapsible: true, active: false });
                $('.ui-accordion-header').removeClass('ui-corner-all').addClass('ui-accordion-header-active ui-state-active ui-corner-top').attr({ 'aria-selected': 'true', 'tabindex': '0' });
                $('.ui-accordion-header .ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
                $('.ui-accordion-content').addClass('ui-accordion-content-active').attr({ 'aria-expanded': 'true', 'aria-hidden': 'false' }).show();


            }).error(function (error) {
                options.error(error);
            });
        });

        blockUI.stop();

    };
    $rootScope.LoadData = function (catalogId, id, cat_id, pack_Id, Type, Attributes, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues) {

        var clickLoadFamilyID = []; // mariyaviji
        $scope.changeProdPage = "1";//JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
        if (id.toString() == "") {
            id = $scope.Family.FAMILY_ID;
        }
        if (id == 0) {
            id = $scope.Family.FAMILY_ID;
        }


        if (id.toString().includes("~")) {
            if (id.startsWith("~")) {
                clickLoadFamilyID = id.split("~");
            }
            else {
                clickLoadFamilyID = id;
            }
        } else {
            clickLoadFamilyID = id;
        }
        if (clickLoadFamilyID.length == 2) {
            $rootScope.ClickFamilyIDValue = clickLoadFamilyID[1];
        }
        else {
            $rootScope.ClickFamilyIDValue = null;
        }
        $scope.sub_id = '0';
        $rootScope.sub_cat_id = cat_id;
        $rootScope.sub_fam_id = id;
        //dataFactory.getprodspecs(catalogId, id, $scope.displayID).success(function (response) {
        //document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaginghighlight";
        var ProdCount = [];
        dataFactory.getprodspecsWithoutAttributes(catalogId, id, true, cat_id).success(function (response) {
            if (response.Data.Columns.length) {
                var obj = jQuery.parseJSON(response.Data.Data);
                var ProdEditPageloopcnt = Math.floor(obj.length / parseInt($scope.ProdCountPerPage));
                if ((obj.length % parseInt($scope.ProdCountPerPage)) > 0) {
                    ProdEditPageloopcnt = ProdEditPageloopcnt + 1;
                }
                $scope.changeProdPage = "1";
                for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                    ProdCount.push(i);
                }
                $scope.ProdPageCount = ProdCount;
                if ($scope.ProdCurrentPage == "1") {
                    $scope.changeProdPage = "1";//JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                } else {
                    $scope.changeProdPage = $scope.ProdCurrentPage;
                }
                //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                // $scope.ProdCurrentPage = $scope.changeProdPage;
                $scope.totalProdPageCount = ProdEditPageloopcnt;
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
            }
            if (Attributes != undefined) {
                if (Attributes.includes('#')) {
                    Attributes = Attributes.replace('#', 'hashValue')  // have to change # values 
                }
            }
            if ($rootScope.val == undefined) {
                $rootScope.val = false;
            }



            dataFactory.getprodspecsattributes(catalogId, id, true, cat_id, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.sub_id, pack_Id, Type, Attributes, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues, $rootScope.val).success(function (response) {
                var productDetails = jQuery.parseJSON(response.Data.Data);


                var obj = productDetails.Table;
                $scope.prodData = obj;

                if (productDetails.Table1 != undefined) {
                    $scope.TotalTableCount = productDetails.Table1[0].Column1;
                    if ($rootScope.TypeOfProduct = "searchProduct") {
                        var ProdCount = [];
                        var ProdEditPageloopcnt = Math.floor($scope.TotalTableCount / parseInt($scope.ProdCountPerPage));
                        if (($scope.TotalTableCount % parseInt($scope.ProdCountPerPage)) > 0) {
                            ProdEditPageloopcnt = ProdEditPageloopcnt + 1;
                        }
                        $scope.changeProdPage = "1";
                        for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                            ProdCount.push(i);
                        }
                        $scope.ProdPageCount = ProdCount;
                        if ($scope.ProdCurrentPage == "1") {
                            $scope.changeProdPage = "1";//JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                        } else {
                            $scope.changeProdPage = $scope.ProdCurrentPage;
                        }
                        //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                        // $scope.ProdCurrentPage = $scope.changeProdPage;
                        $scope.totalProdPageCount = ProdEditPageloopcnt;
                        $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                        $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;

                    }


                }
                // To Hide All the product Grid Hide Function
                if ($scope.prodData.length == 0) {
                    $scope.$broadcast("ToHideAllTheProductGridFunctions", "some data");
                }
                if ($scope.prodData.length == 0 && $rootScope.deleteProduct == true && $scope.ProdCurrentPage != 1) {
                    $scope.ProdCurrentPage = $scope.ProdCurrentPage - 1;
                    $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                    $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                    $scope.LoadProdData(catalogId, id, cat_id);
                }
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                $('#cboProdPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPage;
                //For Product Edit

                //To Change Dynamice CatalogNo
                for (var i = 0; i < response.Data.Columns.length; i++) {
                    if (response.Data.Columns[i]["Caption"].includes("ITEM#")) {
                        response.Data.Columns[i]["Caption"] = response.Data.Columns[i]["Caption"].replace("ITEM#", $localStorage.CatalogItemNumber);
                    }
                }
                $scope.columnsForAtt = response.Data.Columns;
                // To push the values into arry for split only attributes in drop down list.
                if (Type != 'product') {
                    var AttributeValues = [];
                    for (var i = 0 ; $scope.columnsForAtt.length > i ; i++) {
                        if (
                            $scope.columnsForAtt[i].Caption == "CATALOG_ID" ||
                            $scope.columnsForAtt[i].Caption == "FAMILY_ID" ||
                            $scope.columnsForAtt[i].Caption == "PRODUCT_ID" ||
                            $scope.columnsForAtt[i].Caption == "SORT" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2WEB" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2PRINT" ||
                            $scope.columnsForAtt[i].Caption == "WORKFLOW STATUS" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2PDF" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2EXPORT" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2PORTAL" ||
                            $scope.columnsForAtt[i].Caption == "SubProdCount"
                            ) {
                        } else {
                            if ($scope.columnsForAtt[i].Caption.includes("__OBJ")) {
                                AttributeValues.push({ "Value": i, "Name": $scope.columnsForAtt[i].Caption.slice(0, $scope.columnsForAtt[i].Caption.search("__OBJ")) });
                            } else {
                                AttributeValues.push({ "Value": i, "Name": $scope.columnsForAtt[i].Caption });
                            }
                        }
                    }
                    // To remove extra duplicate values (__)
                    for (var i = 0 ; AttributeValues.length > i ; i++) {
                        if (AttributeValues[i].Name.includes("__")) {
                            AttributeValues.splice(i, 1);
                        }
                    }
                    $scope.AttributeValues = AttributeValues;
                    $localStorage.GetAllAttributesasSelected = AttributeValues;
                    $scope.GetAttribute($scope.AttributeValues);
                }
                $("#divProductGrid").show();

                angular.forEach($scope.prodData, function (value) {
                    value.SORT = parseFloat(value.SORT);
                });
                //  $("#selectedattribute").show();
                //$("#divProductGrid").hide();
                //angular.forEach($scope.prodData, function (value) {
                //    value.SORT = parseFloat(value.SORT);
                //   //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                //    // $scope.changeProdPage = $scope.ProdCurrentPage;
                //    //start
                //    $scope.ProdCurrentPage == 1;
                //    if ($scope.ProdCurrentPage == "1") {
                //        $scope.changeProdPage = "1";//JOTHIPRIYA
                //    } else {
                //        $scope.changeProdPage = $scope.ProdCurrentPage;

                //    }
                //    //$rootScope.selectedfamily_id = $scope.Family.FAMILY_ID;
                //    //end
                //    //$scope.callProductGridPaging($scope.ProdCurrentPage);

                //});

                // $rootScope.visibleProjects = $scope.prodData;  $localStorage.CatalogItemNumber;
                // $rootScope.visibleProjects1 = $scope.prodData;
                // $data = $scope.prodData;
                $rootScope.Mainprodcount = $scope.prodData.length;
                if ($scope.prodData.length === 0) {
                    $("#newProductBtn").show();
                    $("#newProductBtn1").hide();
                    $("#divProductGrid").hide();
                    $("#selectedattribute").hide();
                    $("#newProductBtn").css("display", "block");
                    $("#newProductBtn1").css("display", "none");
                }
                else {
                    $("#newProductBtn").hide();
                    $("#newProductBtn1").show();
                    $("#newProductBtn").css("display", "none");
                    $("#newProductBtn1").css("display", "block");
                }
                //$scope.tblDashBoard.reload();
                // $scope.tblFamilyGrid.reload();
                // $scope.tblFamilyGrid.$params.page = 1;
                if ($scope.userRoleDeleteFamily) {
                    // $('#user-toolbar-association-removal-family').removeAttr('disabled');
                    $('#user-toolbar-delete-family').removeAttr('disabled');
                }
                else {
                    // $('#user-toolbar-association-removal-family').attr('disabled', 'disabled');
                    $('#user-toolbar-delete-family').attr('disabled', 'disabled');
                }

                if ($scope.userRoleAddFamily) {
                    $('#user-toolbar-create-new-family').removeAttr('disabled');
                }
                else {
                    $('#user-toolbar-create-new-family').attr('disabled', 'disabled');
                }
                if ($scope.userRoleModifyFamily) {
                    $('#user-toolbar-save-family').removeAttr('disabled');
                    $('#user-toolbar-family-attribute-setup').removeAttr('disabled');
                }
                else {
                    $('#user-toolbar-save-family').attr('disabled', 'disabled');
                    $('#user-toolbar-family-attribute-setup').attr('disabled', 'disabled');
                }
                //  var toolbarProduct = $("#product-toolbar").data("kendoToolBar");
                if ($scope.userRoleAddProductTable) {
                    $('#newbtn').removeAttr('disabled');
                }
                else {
                    $('#newbtn').attr('disabled', 'disabled');
                }

                if ($rootScope.ProductCount < $rootScope.ProductSKUCount) {
                }
                else {
                    $scope.userRoleAddProductTable = false;
                }

                if ($rootScope.Productnew == true) {
                    $('#newbtn').attr('disabled', 'disabled');
                }
                else {
                    $('#newbtn').removeAttr('disabled');
                }
                if ($scope.userRoleModifyProductTable) {
                    $('#columnsetubbtn').removeAttr('disabled');
                    $('#user-toolbar-default-layout').removeAttr('disabled');
                }
                else {
                    $('#columnsetubbtn').attr('disabled', 'disabled');
                    $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                }
                //if ($scope.userRoleDeleteFamily) {
                //    toolbar.enable("#user-toolbar-association-removal-family", true);
                //    toolbar.enable("#user-toolbar-delete-family", true);
                //}
                //else {
                //    toolbar.enable("#user-toolbar-association-removal-family", false);
                //    toolbar.enable("#user-toolbar-delete-family", false);
                //}

                if ($scope.prodData.length > 0) {
                    // $('#productpaging').show();
                    // $('#productpaging1').show();
                    $('#user-toolbar-export').removeAttr('disabled');
                    $('#user-toolbar-table-designer').removeAttr('disabled');
                    $('#user-toolbar-set-table-layout').removeAttr('disabled');
                    $('#user-toolbar-delete-layout').removeAttr('disabled');
                    $('#user-toolbar-create-new-layout').removeAttr('disabled');
                    if ($scope.userRoleModifyProductTable) {
                        $('#user-toolbar-default-layout').removeAttr('disabled');
                    }
                    else {
                        $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                    }
                    if (!$rootScope.EditMode) {
                        $('#user-toolbar-export').attr('disabled', 'disabled');
                        $('#user-toolbar-table-designer').attr('disabled', 'disabled');
                    }
                }
                else {
                    $('#productpaging').hide();
                    $('#productpaging1').hide();
                    $('#user-toolbar-export').attr('disabled', 'disabled');
                    $('#user-toolbar-table-designer').attr('disabled', 'disabled');
                    $('#user-toolbar-set-table-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-delete-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-create-new-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                }

                if ($rootScope.userprodcongig === true) {
                    $('#addtoprdconfig').removeAttr('disabled');
                } else {
                    $('#addtoprdconfig').attr('disabled', 'disabled');
                }
                if ($rootScope.userprodcongig === true) {
                    $('#prodconfig').removeAttr('disabled');
                } else {
                    $('#prodconfig').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#cutbtn').removeAttr('disabled');
                } else {
                    $('#cutbtn').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#copybtn').removeAttr('disabled');
                } else {
                    $('#copybtn').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#pastebtn').removeAttr('disabled');
                } else {
                    $('#pastebtn').attr('disabled', 'disabled');
                }
                document.getElementById('myButton1').innerText = "Show Less";
                blockUI.stop();



            }).error(function (error) {
                options.error(error);
            });
        });

        blockUI.stop();

    };

    $scope.columnSort = { sortColumn: 'PRODUCT_ID', reverse: false };
    $scope.picklistDropdown = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetPickListDetails().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                }); 3599
            }
        }
    });
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //......................................................................................................................................................................

    //$rootScope.dropDownTreeViewDatasource = new kendo.data.HierarchicalDataSource({
    //    type: "json",
    //    transport: {
    //        read: function (options) {
    //            dataFactory.GetCatalogCategorySortDetails($rootScope.selecetedCatalogId, options.data.id).success(function (response) {
    //                options.success(response);
    //            }).error(function (response) {
    //                options.success(response);
    //            });
    //        }
    //    },
    //    schema: {
    //        model: {
    //            id: "id",
    //            hasChildren: "hasChildren"
    //        }
    //    }
    //});
    //if ($("#dropDownTreeView").val() !== undefined) {
    //    $rootScope.dropDownTreeView = $("#dropDownTreeView").kendoExtDropDownTreeView({
    //        treeview: {
    //            dataSource: $rootScope.dropDownTreeViewDatasource,
    //            dataTextField: "CATEGORY_NAME",
    //            dataValueField: "id",
    //            loadOnDemand: false,
    //            select: onfamilySelect
    //        }
    //    }).data("kendoExtDropDownTreeView");

    //    $rootScope.dropDownTreeView.treeview().expand(".k-item");
    //}
    //function onfamilySelect(e) {
    //    var dataItem = this.dataItem(e.sender._clickTarget);
    //    $rootScope.FamilyCATEGORY_ID = dataItem.id;
    //};
    //--------------------------------------------------------------------------------------------------------------------------------------------------------
    //$rootScope.dropDownTreeViewParnetCategoryDatasource = new kendo.data.HierarchicalDataSource({
    //    type: "json",
    //    transport: {
    //        read: function (options) {
    //            dataFactory.GetCatalogCategoryDetails($rootScope.selecetedCatalogId, options.data.id, $rootScope.selecetedCategoryId).success(function (response) {
    //                options.success(response);
    //            }).error(function (response) {
    //                options.success(response);
    //            });
    //        }
    //    },
    //    schema: {
    //        model: {
    //            id: "id",
    //            hasChildren: "hasChildren"
    //        }
    //    },
    //    filter: [
    //      // leave data items which are "Beverage" and not "Coffee"
    //      { field: "CATEGORY_ID", operator: "neq", value: $rootScope.selecetedCategoryId }

    //    ]
    //});
    //if ($("#dropDownParnetCategoryTreeView").val() !== undefined) {
    //    $rootScope.dropDownParnetCategoryTreeView = $("#dropDownParnetCategoryTreeView").kendoExtDropDownTreeView({
    //        treeview: {
    //            dataSource: $rootScope.dropDownTreeViewParnetCategoryDatasource,
    //            dataTextField: "CATEGORY_NAME",
    //            dataValueField: "id",
    //            loadOnDemand: false,
    //            select: onSelect
    //        }
    //    }).data("kendoExtDropDownTreeView");


    //    $rootScope.dropDownParnetCategoryTreeView.treeview().expand(".k-item");
    //}

    //function onSelect(e) {
    //    var dataItem = this.dataItem(e.sender._clickTarget);
    //    $rootScope.selecetedParentCategoryId = dataItem.id;
    //};
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------
    $rootScope.IsPreview = false;
    $scope.CreateNewCategory = function () {
        //debugger;
        // to hide Family & product Accordion
        $("#tabstrip-2").hide();
        $("#columnsetupcategory").hide();
        $("#gridcategory").show();

        $rootScope.newCat = true;
        $rootScope.IsPreview = true;
        $scope.getCategoryPdfExpress = '';
        $scope.getCategoryPdfExpressToolTip = 'No file chosen';
        if ($scope.categorycnt) {
            $("#dialog-confirm").show();
            $("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Yes": function () {
                        $scope.$broadcast("New_Category_Id");
                        $rootScope.selecetedParentCategoryId = "0";
                        //   $("#dropDownParnetCategoryTreeView .k-input").text("Root");
                        $("#txt_ParentCategoryID").val("Root");
                        $(this).dialog("close");
                        $rootScope.SelectedNodeID = '0';
                        $scope.$broadcast("ToClearTheData", "some data");
                    },
                    "No": function () {

                        $scope.$broadcast("New_Category_Id");
                        $rootScope.CutCategoryId = $scope.RightClickedCategoryId;
                        $rootScope.selecetedParentCategoryId = $rootScope.CutCategoryId;
                        $("#txt_ParentCategoryID").val($scope.RightClickedCategoryName);
                        $("#familycategoryname").val($scope.RightClickedCategoryName);
                        //  var selectedparentcategory = $rootScope.dropDownParnetCategoryTreeView._treeview.findByText($scope.RightClickedCategoryName);
                        //  $rootScope.dropDownParnetCategoryTreeView._treeview.select(selectedparentcategory);
                        //  $("#dropDownParnetCategoryTreeView .k-input").text($scope.RightClickedCategoryName);
                        //   var selectedparentcategory1 = $rootScope.dropDownTreeView._treeview.findByText($scope.RightClickedCategoryName);
                        //    $rootScope.dropDownTreeView._treeview.select(selectedparentcategory1);
                        //   if (selectedparentcategory1.length > 0) {
                        //   $("#dropDownTreeView .k-input").text($scope.RightClickedCategoryName);
                        // $rootScope.FamilyCATEGORY_ID = $scope.RightClickedCategoryId;
                        //  }
                        $(this).dialog("close");
                        if ($rootScope.SelectedNodeID == 0 || $rootScope.SelectedNodeID == "~" || $rootScope.SelectedNodeID == undefined) {
                            $rootScope.SelectedNodeID = $rootScope.selecetedParentCategoryId;
                        }
                        if ($rootScope.SelectedNodeID.includes('~!')) {
                            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
                        }
                        else if ($rootScope.SelectedNodeID.includes('~')) {
                            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
                        }
                        $rootScope.familyDeleteNavStatus = true;
                        $rootScope.treeForCategory = true;
                        $rootScope.SelectedNodeIDPresist = 1;
                        $scope.$broadcast("ToClearTheData", "some data");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }

            });

        } else {
            $(this).dialog("close");
            $scope.$broadcast("New_Category_Id");
            $rootScope.selecetedParentCategoryId = "0";
            //  $("#dropDownParnetCategoryTreeView .k-input").text("Root");
            $("#txt_ParentCategoryID").val("Root");
        }
        var tabstrips = $("#tabstrip").data("kendoTabStrip");
        var myTabs = tabstrips.tabGroup.children("li").eq(0);
        tabstrips.select(myTabs);

        var myTabsfamily = tabstrips.tabGroup.children("li").eq(1);
        tabstrips.disable(myTabsfamily);

        var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
        tabstrips.disable(myTabsproducts);
        $("#custom-menu1").hide();
        $scope.workflowDataSourceCategorylatest.read();
    };

    $scope.CreateNewFamily = function (dataItem) {
        //

        $('#tabstrip-2').show();
        $('#tabstrip-1').hide();

        $rootScope.currentFamilyId = undefined;
        $scope.$broadcast("RefreshFileuploadValues");



        $rootScope.familyCreated = false;
        if ($rootScope.SelectedNodeID == 0 || $rootScope.SelectedNodeID == "~" || $rootScope.SelectedNodeID == undefined) {
            $rootScope.SelectedNodeID = $scope.RightClickedCategoryId;
        }
        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }

        $rootScope.familyDeleteNavStatus = true;
        $scope.$broadcast("ToClearProdFamData", "some data");
        $rootScope.SelectedNodeIDPresist = 1;
        var tabstrip = $("#tabstrip").data("kendoTabStrip");
        var myTab = tabstrip.tabGroup.children("li").eq(1);
        tabstrip.enable(myTab);
        tabstrip.select(myTab);

        var myTabscategory = tabstrip.tabGroup.children("li").eq(0);
        tabstrip.enable(myTabscategory);

        var myTabsproducts = tabstrip.tabGroup.children("li").eq(2);
        tabstrip.enable(myTabsproducts);

        $("#custom-menu1").hide();
        $("#custom-menuFam").hide();

        $("#columnsetupfamily").hide();
        $("#gridfamily").show();
        $scope.$broadcast("NewFamilyCreation", $scope.RightClickedCategoryId, $scope.RightClickeddataItem);
        $scope.Family.FAMILY_ID = 0;
        $rootScope.workflowDataSource.read();
        //new
        $("#divFmlyTab").show();
        $("#familyPreview").hide();
        $("#tabledesigner").hide();
        $("#EditMultipletable").hide();
        //$("#FamilySpecsTab").hide();	
        $('#FamilySpecsTab').css("display", "none");
    };
    $scope.CreateNewProduct = function () {
        $('#tabstrip-2').show();
        $('#tabstrip-1').hide();

        //New UI Changes
        $("#mainProductsMenu").hide();
        //$("#productgridtabstrip").hide();
        $("#filterAttributePack").hide();
        $("#filterAttributePackLeft").hide();
        $("#productpaging1").hide();
        $("#Productcountid").hide();
        $("#sampleProdgrid").hide();
        $("#productpaging").hide();

        $rootScope.SelectedNodeIDPresist = 1;
        var tabstrip = $("#tabstrip").data("kendoTabStrip");
        var myTab = tabstrip.tabGroup.children("li").eq(2);
        tabstrip.enable(myTab);
        tabstrip.select(myTab);

        var myTabscategory = tabstrip.tabGroup.children("li").eq(0);
        tabstrip.enable(myTabscategory);

        var myTabsproducts = tabstrip.tabGroup.children("li").eq(1);
        tabstrip.enable(myTabsproducts);
        $('#tabstrip-2').show();
        $('#noresultfound').hide();

        $("#custom-menu1").hide(); $("#custom-menuFam").hide();
        $("#newProductBtn").show();
        $("#newProductBtn1").hide();
        $scope.$broadcast("NewProductCreation", $scope.RightClickedCategoryId, $rootScope.familycategory);
        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.RightClickedCategoryId, $localStorage.CategoryID);

    };
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    $scope.categoryId = '';
    $scope.categoryPublishPrint = '';
    $scope.CategoryPreview = {
        CATEGORY_ID: '',
        PUBLISH2PRINT: true,
    };
    $scope.PreviewAll = function () {

        //$rootScope.clickSelectedItem($scope.RightClickeddataItem);
        $scope.ProdCurrentPage = 1;
        $scope.gridCurrentPage = "10";
        if ($scope.RightClickedCategoryId.match("~")) {
            var tabstrip = $("#tabstrip").data("kendoTabStrip");
            var myTab = tabstrip.tabGroup.children("li").eq(1);
            tabstrip.enable(myTab);
            tabstrip.select(myTab);

            var myTabscategory = tabstrip.tabGroup.children("li").eq(0);
            tabstrip.enable(myTabscategory);

            var myTabsproducts = tabstrip.tabGroup.children("li").eq(2);
            tabstrip.enable(myTabsproducts);

            $("#custom-menu1").hide(); $("#custom-menuFam").hide();
            $scope.$broadcast("FamilyPreview", $scope.RightClickedCategoryId, $rootScope.familycategory);

            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.RightClickedCategoryId, $localStorage.CategoryID);


        } else {
            $("#custom-menu1").hide(); $("#custom-menuFam").hide();
            $http.get("../Category/GetCategory?CategoryId=" + $scope.RightClickedCategoryId).
          then(function (categoryDetails) {

              $scope.CategoryPreview = categoryDetails.data;
              if ($scope.CategoryPreview.Family_ID != 0) {
                  if ($scope.CategoryPreview.PUBLISH2PRINT == true) {
                      var encryptcatalogId = $rootScope.selecetedCatalogId;
                      var encryptcategoryid = $scope.CategoryPreview.CATEGORY_ID;
                      var encryptedcategoryid = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(encryptcategoryid), key,
                      { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
                      var encryptedcatlogid = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(encryptcatalogId), key,
                        { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
                      window.open("../Category/Preview?catid=" + encryptedcategoryid + "&catalogid=" + encryptedcatlogid + "&userid=" + $scope.getCustomerIDs, '_blank');
                  }
                  else {
                      $.msgBox({
                          title: $localStorage.ProdcutTitle,
                          content: 'Category is not published to print, please publish the category to preview it.',
                          type: "info"
                      });

                  }

              }
              else {
                  $.msgBox({
                      title: $localStorage.ProdcutTitle,
                      content: 'No Product and Item in this category, please added Product And Item then category to preview it.',
                      type: "info"
                  });
              }


          });

        }
    };
    $('#search-term').keypress(function (event) {

        []
        var result = document.getElementsByClassName("highlight");

        angular.element(result).removeClass("highlight");

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $scope.start = 0;
            var treeView = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView");
            // ignore if no search term
            if ($.trim($(this).val()) == '') {
                return;
            } else {
                var term = this.value.toUpperCase();
                var tlen = term.length;
                $('#leftNavTreeViewKendoNavigator span.k-in').each(function (index) {
                    var treeView = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView");
                    $(this).context.style.backgroundColor = "white";
                    var text = $(this).text();
                    var ddd = $(this).get();
                    var classname = treeView.findByText(text);
                    var dee = '';
                    var CssClass = '';
                    if (classname.length === 1) {
                        CssClass = classname.prevObject[0].firstElementChild.className;
                        if (CssClass === undefined || CssClass === "")
                            CssClass = ddd[0].firstChild.nextElementSibling.className;
                        dee = classname.prevObject[0].innerHtml;

                        var html = '<span></span>';
                        html = html + '<span class="' + CssClass + '" style="position: inherit;"></span>';

                        var q = 0;
                        while ((p = text.toUpperCase().indexOf(term, q)) >= 0) {
                            if (text.toUpperCase().includes(term.toUpperCase())) {
                                html += text.substring(q, p) + '<span class="highlight" >' + text.substr(p, tlen) + '</span>';
                                q = p + tlen;
                            }
                        }
                        if (q > 0) {
                            html += text.substring(q);
                            $(this).context.style.backgroundColor = "yellow";
                            //$(this).html(html);
                            $(this).parentsUntil('.k-treeview').filter('.k-item').each(
                                function (index, element) {
                                    //$('#leftNavTreeViewKendo').data('kendoTreeView').expand($(this));
                                    $(this).data('search-term', term);
                                }
                            );
                        }

                    } else {
                        //for (var i = 0; i < classname.length; i++) {
                        //    CssClass = classname[i].getElementsByClassName("k-sprite");
                        //    CssClass = CssClass[0].className;
                        //}
                        var html = '<span></span>';


                        var q = 0;
                        while ((p = text.toUpperCase().indexOf(term, q)) >= 0) {
                            if (text.toUpperCase().includes(term.toUpperCase())) {
                                CssClass = classname[$scope.start].getElementsByClassName("k-sprite");
                                CssClass = CssClass[0].className;
                                html = html + '<span class="' + CssClass + '" style="position: inherit;"></span>';
                                html += text.substring(q, p) + '<span class="highlight">' + text.substr(p, tlen) + '</span>';
                                q = p + tlen;
                                $scope.start = 1;
                            }
                        }
                        if (q > 0) {
                            html += text.substring(q);
                            $(this).context.style.backgroundColor = "yellow";
                            // $(this).html(html);
                            $(this).parentsUntil('.k-treeview').filter('.k-item').each(
                                function (index, element) {
                                    //$('#leftNavTreeViewKendo').data('kendoTreeView').expand($(this));
                                    $(this).data('search-term', term);
                                }
                            );
                        }


                    }

                });
            }
            $('#leftNavTreeViewKendoNavigator .k-item').each(function () {
                if ($(this).data('search-term') != term) {
                    //$('#leftNavTreeViewKendo').data('kendoTreeView').collapse($(this));
                }
            });
        }
    });

    //$('#search-term').on('keyup', function () {
    //    //$('span.k-in > span.highlight').each(function () {
    //    //    $(this).parent().text($(this).parent().text());
    //    //});
    //    $scope.start = 0;
    //    var treeView = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView");
    //    // ignore if no search term
    //    if ($.trim($(this).val()) == '') {
    //        return;
    //    }
    //    else {
    //        var term = this.value.toUpperCase();
    //        var tlen = term.length;
    //        $('#leftNavTreeViewKendoNavigator span.k-in').each(function (index) {
    //            var treeView = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView");
    //            var text = $(this).text();
    //            var ddd = $(this).get();
    //            var classname = treeView.findByText(text);
    //            var dee = '';
    //            var CssClass = '';
    //            if (classname.length === 1) {
    //                CssClass = classname.prevObject[0].firstElementChild.className;
    //                if (CssClass === undefined || CssClass === "")
    //                    CssClass = ddd[0].firstChild.nextElementSibling.className;
    //                dee = classname.prevObject[0].innerHtml;

    //                var html = '<span></span>';
    //                html = html + '<span class="' + CssClass + '" style="position: inherit;"></span>';

    //                var q = 0;
    //                while ((p = text.toUpperCase().indexOf(term, q)) >= 0) {

    //                    html += text.substring(q, p) + '<span class="highlight">' + text.substr(p, tlen) + '</span>';
    //                    q = p + tlen;
    //                }
    //                if (q > 0) {
    //                    html += text.substring(q);
    //                    $(this).html(html);
    //                    $(this).parentsUntil('.k-treeview').filter('.k-item').each(
    //                        function (index, element) {
    //                            //$('#leftNavTreeViewKendo').data('kendoTreeView').expand($(this));
    //                            $(this).data('search-term', term);
    //                        }
    //                    );
    //                }

    //            }
    //            else {
    //                //for (var i = 0; i < classname.length; i++) {
    //                //    CssClass = classname[i].getElementsByClassName("k-sprite");
    //                //    CssClass = CssClass[0].className;
    //                //}
    //                var html = '<span></span>';


    //                var q = 0;
    //                while ((p = text.toUpperCase().indexOf(term, q)) >= 0) {
    //                    CssClass = classname[$scope.start].getElementsByClassName("k-sprite");
    //                    CssClass = CssClass[0].className;
    //                    html = html + '<span class="' + CssClass + '" style="position: inherit;"></span>';
    //                    html += text.substring(q, p) + '<span class="highlight">' + text.substr(p, tlen) + '</span>';
    //                    q = p + tlen;
    //                    $scope.start = 1;
    //                }
    //                if (q > 0) {
    //                    html += text.substring(q);
    //                    $(this).html(html);
    //                    $(this).parentsUntil('.k-treeview').filter('.k-item').each(
    //                        function (index, element) {
    //                            //$('#leftNavTreeViewKendo').data('kendoTreeView').expand($(this));
    //                            $(this).data('search-term', term);
    //                        }
    //                    );
    //                }


    //            }

    //        });
    //    }
    //    $('#leftNavTreeViewKendoNavigator .k-item').each(function () {
    //        if ($(this).data('search-term') != term) {
    //            //$('#leftNavTreeViewKendo').data('kendoTreeView').collapse($(this));
    //        }
    //    });
    //});

    var columns = $scope.columnsForAtt;
    //$.each(DispCaption, function (i) {
    //    columns.push({ fieldname: DispFieldName[i], caption: DispCaption[i], width: DispWidth[i] });
    //});

    $scope.mycolumns = columns;
    $scope.getcolumnname = function (cell) {
        return cell.ColumnName;
    };

    $scope.getcolumnwidth = function (cell) {
        return cell.width;
    };

    $scope.getcolumnShow = function (cell) {
        var a = cell.width > 0 ? true : false;
        return a;
    };

    //$scope.fnRowClick = function (record) {
    //};
    $scope.orderField = "SORT";
    $scope.reverse = false;
    $scope.fnSort = function (cell) {
        $scope.orderField = cell.ColumnName.replace(/[^a-zA-Z0-9_]/g, "_");
        $scope.reverse = !$scope.reverse;
    };

    $scope.treeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },

        loadOnDemand: false,
        dataBound: function (e) {
            //debugger;
            if (e.sender.dataSource._data.length > 0) {

                if ($rootScope.getSelectedNode == "") {
                    var treeview = e.sender;
                    var root = $('.k-item:first');
                    treeview.select(root);
                    treeview.trigger('select', { node: root });
                } else {

                }
                // $rootScope.previoustate();
            }
            if (!e.node) {
                $scope.attachChangeEvent();
            }
        }
    };

    $scope.expandTree = function (e) {

    };
    $rootScope.previoustate = function () {
        var selectedparentcategory1 = "CAT87";
        if (selectedparentcategory1 !== undefined) {
            var node = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView").findByUid(selectedparentcategory1.uid);
            if (node.length > 0) {
                var treeView = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView");
                treeView.expandPath("CAT87");
            }
        }
    };

    $scope.onExpand = function () {

    };
    $scope.selectionoftree = function () {
        var selectedparentcategory1 = "CAT35";
        if (selectedparentcategory1 !== undefined) {
            var node = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView").findByUid(selectedparentcategory1.uid);
            if (node.length > 0) {
                var TreeView = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView");
                var getitem = TreeView.dataSource.get(selectedparentcategory1);
                var selectitem = TreeView.findByUid(getitem.uid);
                TreeView.select(selectitem);
            }
        }
    };
    $scope.attachChangeEvent = function () {
        var dataSource = $rootScope.treeData;
        $scope.checkeditems = '';
        $scope.uncheckeditems = '';
        dataSource.bind("change", function (e) {
            var selectedNodes = 0;
            var UnselectedNodes = 0;
            var checkedNodes = [];
            var UncheckedNodes = [];
            $scope.checkedNodeIds(dataSource.view(), checkedNodes);
            $scope.UncheckedNodeIds(dataSource.view(), UncheckedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            $scope.checkeditems = checkedNodes.join(",");

            for (var i = 0; i < UncheckedNodes.length; i++) {
                var nd = UncheckedNodes[i];
                if (!nd.checked) {
                    UnselectedNodes++;
                }
            }
            $scope.uncheckeditems = UncheckedNodes.join(",");
        });

    };
    $scope.checkedNodeIds = function (nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].CATEGORY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    };


    $scope.UncheckedNodeIds = function (nodes, UncheckedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (!nodes[i].checked) {
                UncheckedNodes.push(nodes[i].CATEGORY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.UncheckedNodeIds(nodes[i].children.view(), UncheckedNodes);
            }
        }
    };

    $scope.PasteOptionClick = function (value) {
        if (value.currentTarget.value === "all") {
            $scope.PasteFlag = '0';
        }
        else if (value.currentTarget.value == "catandfam") {
            $scope.PasteFlag = '1';
        }
        else if (value.currentTarget.value == "cat") {
            $scope.PasteFlag = '2';
        }
        else {
            $scope.PasteFlag = '2';
        }

    };


    $scope.OkPopup = function () {

        $scope.PasteOptions.refresh({ url: "../views/app/partials/Pasteoptions.html" });
        $scope.PasteOptions.center().close();
        $http.get('../Family/SubproductCheck?family_id=' + $scope.CopyCategoryId + '&multipleFamilyids=' + $scope.checkeditems + '&multipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&catalog_id=' + $scope.PreviousCatalogID + '&option=' + "Category")
                   .then(function (response) {

                       if ($scope.PasteFlag === "0" && response.data != "False" && $scope.RightClickedCatalogID !== $scope.PreviousCatalogID || $rootScope.AllowDuplicateItem_PartNum) {

                           $.msgBox({
                               title: " CatalogStudio",
                               content: "Do you want to paste the selected option?",
                               type: "confirm",
                               buttons: [{ value: "Yes" }, { value: "No" }],
                               success: function (result) {
                                   var bool = false;
                                   if (result === "Yes") {
                                       bool = true;
                                   }
                                   
                                   if ($scope.Rootflag === "Root") {
                                       $scope.PasteCategoryId = $scope.RightClickedCategoryId;
                                       if ($scope.CopyCategoryId.match("~") || $scope.CopyCategoryId === "") {
                                           $.msgBox({
                                               title: $localStorage.ProdcutTitle,
                                               content: 'Selected function is not available for this Product.',
                                               type: "info"
                                           });
                                       } else {
                                           if (result === "Yes") {
                                               $http.get('../Category/CopyPasteCategory?copyCatId=' + $scope.CopyCategoryId + '&&PasteCatId=0&&catalogId=' + $rootScope.selecetedCatalogId + '&&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&PasteFlag=' + $scope.PasteFlag + '&SubProductCFM=' + bool)
                                                   .then(function () {
                                                       $.msgBox({
                                                           title: $localStorage.ProdcutTitle,
                                                           content: 'Pasted successfully.',
                                                           type: "info"
                                                       });

                                                       $scope.RefreshLeftNavTree();
                                                       $scope.IdsForAnotherCatalog = '';
                                                       $scope.PasteFlag = '0';
                                                   });
                                           }
                                           }
                                           $scope.PasteOptions.refresh({ url: "../views/app/partials/Pasteoptions.html" });
                                           $scope.PasteOptions.center().close();
                                       }
                                   
                                   else if ($scope.Rootflag === "Sub") {
                                       if (result === "Yes") {
                                           $scope.PasteCategoryId = $scope.RightClickedCategoryId;
                                           $http.get('../Category/CopyPasteCategory?copyCatId=' + $scope.CopyCategoryId + '&&PasteCatId=' + $scope.PasteCategoryId + '&&catalogId=' + $rootScope.selecetedCatalogId + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&PasteFlag=' + $scope.PasteFlag + '&SubProductCFM=' + bool)
                                               .then(function (response) {

                                                   if (response.data === "Paste successful") {
                                                       $.msgBox({
                                                           title: $localStorage.ProdcutTitle,
                                                           content: '' + response.data + '.',
                                                           type: "info"
                                                       });
                                                   }
                                                   else {
                                                       $.msgBox({
                                                           title: $localStorage.ProdcutTitle,
                                                           content: '' + response.data + '.',
                                                           type: "error"
                                                       });
                                                   }
                                                   $scope.RefreshLeftNavTree();
                                                   $scope.IdsForAnotherCatalog = '';
                                                   $scope.PasteFlag = '0';
                                               });
                                       }
                                       $scope.PasteOptions.refresh({ url: "../views/app/partials/Pasteoptions.html" });
                                       $scope.PasteOptions.center().close();
                                   }
                                   else {
                                       $.msgBox({
                                           title: $localStorage.ProdcutTitle,
                                           content: 'Action not completed, please try again.',
                                           type: "error"
                                       });
                                       $scope.IdsForAnotherCatalog = '';
                                       $scope.PasteFlag = '0';
                                   }

                               }

                           });
                       }
                       else {
                           $.msgBox({
                               title: " CatalogStudio",
                               content: "Do you want to paste the selected option?",
                               type: "confirm",
                               buttons: [{ value: "Yes" }, { value: "No" }],
                               success: function (result) {
                                   var bool = false;
                                   if (result === "Yes") {
                                       bool = true;
                                   }
                                   else {
                                       return;
                                   }
                                   if ($scope.Rootflag === "Root") {
                                       var bool = false;
                                       $scope.PasteCategoryId = $scope.RightClickedCategoryId;
                                       if ($scope.CopyCategoryId.match("~") || $scope.CopyCategoryId === "") {
                                           $.msgBox({
                                               title: $localStorage.ProdcutTitle,
                                               content: 'Selected function is not available for this Product.',
                                               type: "info"
                                           });
                                       } else {
                                           if (result === "Yes") {
                                               $http.get('../Category/CopyPasteCategory?copyCatId=' + $scope.CopyCategoryId + '&&PasteCatId=0&&catalogId=' + $rootScope.selecetedCatalogId + '&&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&PasteFlag=' + $scope.PasteFlag + '&SubProductCFM=' + bool)
                                                   .then(function () {
                                                       $scope.RefreshLeftNavTree();
                                                       $scope.IdsForAnotherCatalog = '';
                                                       $scope.PasteFlag = '0';
                                                   });
                                           }
                                       }
                                       $scope.PasteOptions.refresh({ url: "../views/app/partials/Pasteoptions.html" });
                                       $scope.PasteOptions.center().close();
                                   }
                                   else if ($scope.Rootflag === "Sub") {
                                       if (result === "Yes") {
                                           var varbool = false;
                                           $scope.PasteCategoryId = $scope.RightClickedCategoryId;
                                           $http.get('../Category/CopyPasteCategory?copyCatId=' + $scope.CopyCategoryId + '&&PasteCatId=' + $scope.PasteCategoryId + '&&catalogId=' + $rootScope.selecetedCatalogId + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&PasteFlag=' + $scope.PasteFlag + '&SubProductCFM=' + varbool)
                                               .then(function (response) {
                                                   if (response.data === "Paste successful") {
                                                       $.msgBox({
                                                           title: $localStorage.ProdcutTitle,
                                                           content: '' + response.data + '.',
                                                           type: "info"
                                                       });
                                                   }

                                                   $scope.RefreshLeftNavTree();
                                                   $scope.IdsForAnotherCatalog = '';
                                                   $scope.PasteFlag = '0';
                                               });
                                       }

                                       $scope.PasteOptions.refresh({ url: "../views/app/partials/Pasteoptions.html" });
                                       $scope.PasteOptions.center().close();
                                   }
                                   else {
                                       $.msgBox({
                                           title: $localStorage.ProdcutTitle,
                                           content: 'Action not completed, please try again.',
                                           type: "error"
                                       });
                                       $scope.IdsForAnotherCatalog = '';
                                       $scope.PasteFlag = '0';
                                   }
                               }
                           });
                       }

                   });
    };

    $scope.ReferenceAssociation = function () {
        if ($scope.RightClickedCategoryId.match("~")) {
            $scope.winReferencetableAssociationSetup.refresh({ url: "../views/app/partials/ReferencetableAssociation.html" });
            $scope.winReferencetableAssociationSetup.center().open();
        }
    };
    $scope.ReferenceAssociationCategory = function () {
        $scope.winReferencetableAssociationSetup.refresh({ url: "../views/app/partials/ReferencetableAssociationCategory.html" });
        $scope.winReferencetableAssociationSetup.center().open();
    };
    //----------------------Show MasterCatalog--------------------------//
    $scope.ShowMasterCatalog = function () {
        //debugger;
        //var result = $('#leftNavTreeViewKendoNavigator');
        //if (result.length>0) {
        //    result[0].attributes[3].value = true;
        //}
        $("#searchLeftNavi").hide();
        $rootScope.treeData.read();
        $scope.SelectedCatalogId = 1;
        $rootScope.treeData.read();
        $scope.SelectedCatalogname = $localStorage.getCatalogName;
        $('#ShowMaster').hide();
        $('#SelectedCatalog').show();
        $('#ShowMasterf').hide();
        $('#SelectedCatalogf').show();
        $('#ddlcatalog').hide();
        $('#lblmastercatalog').show();
    };

    $scope.ShowMasterCurrentCatalog = function () {
        //debugger;
        $("#searchLeftNavi").hide();
        $rootScope.treeData.read();
        //new
        var checkedNodes = [];
        $scope.checkedNodeIds($rootScope.treeData._data, checkedNodes);
        var ids = $scope.Showmasterids;
        var unchkids = $scope.Showmasteruncheckids;
        let text = checkedNodes.toString();
        dataFactory.Navigatorassociation($localStorage.getCatalogID, text, $scope.RightClickedCategoryId, unchkids, 'Association').success(function (response) {
            //debugger;
            $scope.Showmasterids = '';
            $scope.Showmasteruncheckids = '';
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
            $scope.RightClickedCategoryId = '';
            $scope.RefreshLeftNavTree();

            // options.success(response);
        }).error(function (response) {
            //options.success(response);
        });

        $('#ShowMaster').show();
        $('#SelectedCatalog').hide();
        $('#ShowMasterf').show();
        $('#SelectedCatalogf').hide();
        $('#ddlcatalog').show();
        $('#lblmastercatalog').hide();


    };


    $scope.CheckUncheckAll = function (val) {
        if (val) {
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"][class="testing"]').prop('checked', true).trigger("change");
        } else {
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"][class="testing"]').prop('checked', false).trigger("change");
        }
        $("#custom-menu1").hide();
    };

    $scope.AssociationRemoveFromNavigator = function () {
        //debugger;

        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }
        $rootScope.treeForCategory = true;
        $rootScope.familyDeleteNavStatus = true;
        $rootScope.SelectedNodeIDPresist = 1;

        var ids = "";
        if ($scope.selecetedCatalogId.toString() === "1")
            ids = $scope.Showmasteruncheckids;
        else
            ids = $scope.Showmasterids;
        var categoryid = "";
        category_id = $rootScope.SelectedNodeID;
        if ($scope.checkeditems == null || $scope.checkeditems == '' || $scope.checkeditems == "") {
            $scope.checkeditems = $scope.RightClickedCategoryId;
        } else {
            $scope.checkeditems;
        }
        if ($scope.checkedIds == null || $scope.checkedIds == '' || $scope.checkedIds == "") {
            $scope.checkedIds = category_id;
        }
        if (ids.contains("~")) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to remove the Product association?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        //-----------------------DETACH ROOT FAMILY SHOWING THE CLONE FAMILY---------------------------------------------------------------//  

                        $rootScope.GetClonefamilyDatasource.read();
                        category_id = $rootScope.CloneCategory_id;
                        dataFactory.Navigatorassociationremove($localStorage.getCatalogID, category_id, $scope.checkeditems).success(function (response) {
                            if (response == "Update completed") {

                                var Res = "Product association removed successfully"

                                if ($rootScope.familyAction == "Delete") {
                                    Res = "Product removed successfully"
                                }
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + Res + '.',
                                    type: "info"
                                });
                                $scope.Showmasteruncheckids = '';
                                $scope.SelectedCatalogId = $localStorage.getCatalogID;
                                $scope.RefreshLeftNavTree();
                                $scope.$broadcast("DetachCateAttributes");
                            }
                        }).error(function (response) {
                        });
                        //$scope.winGetClone.refresh({ url: "../views/app/Admin/clonefamily.html" });
                        //$scope.winGetClone.center().open();
                        //bool = true;
                        //-----------------------DETACH ROOT FAMILY SHOWING THE CLONE FAMILY---------------------------------------------------------------//
                        //bool = true;
                    }

                    //if (bool === true) {
                    //    dataFactory.Navigatorassociationremove($localStorage.getCatalogID, ids).success(function (response) {

                    //        if (response == "Update completed")
                    //        {
                    //            var Res = "Family association removed successfully"
                    //        }

                    //        $.msgBox({
                    //            title: $localStorage.ProdcutTitle,
                    //            content: '' + Res + '.',
                    //            type: "info"
                    //        });


                    //        $scope.Showmasteruncheckids = '';
                    //        $scope.SelectedCatalogId = $localStorage.getCatalogID;
                    //        $scope.RefreshLeftNavTree();
                    //    }).error(function (response) {
                    //    });
                    //}
                }
            });
        } else {
            dataFactory.ChkCategoryexists($rootScope.selecetedParentCategoryId, $scope.Showmasterids, $rootScope.selecetedCatalogId).success(function (responses) {
                if (responses == "true") {
                    dataFactory.Navigatorassociationremove($localStorage.getCatalogID, ids).success(function (response) {
                        $scope.Showmasteruncheckids = '';
                        $scope.SelectedCatalogId = $localStorage.getCatalogID;
                        $scope.RefreshLeftNavTree();
                        $scope.$broadcast("DetachCateAttributes");
                    }).error(function (response) {
                    });
                } else if (responses == "false") {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "The selected category is available only in the working catalog, are you sure want to remove the category association ?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {
                            var bool = false;
                            if (result === "Yes") {
                                bool = true;
                            }
                            if (bool === true) {
                                dataFactory.CategoryremoveFromNavigator($scope.RightClickedCategoryId, $scope.checkeditems, $rootScope.selecetedCatalogId, "0").success(function (response) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response + '.',
                                        type: "info"
                                    });
                                    $scope.RefreshLeftNavTree();
                                    $scope.$broadcast("DetachCateAttributes");
                                    $scope.RemoveFlag = '';
                                    $scope.RightClickedCategoryId = '';
                                }).error(function (error) {
                                    options.error(error);
                                });
                            } // ifff
                        }
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "The selected category is available in another catalog, on removing association it will not appear in the Master Catalog, do you want to continue?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {
                            var bool = false;
                            if (result === "Yes") {
                                bool = true;
                            }

                            if (bool === true) {
                                dataFactory.Navigatorassociationremove($localStorage.getCatalogID, ids).success(function (response) {
                                    $scope.Showmasteruncheckids = '';
                                    $scope.SelectedCatalogId = $localStorage.getCatalogID;
                                    $scope.RefreshLeftNavTree();
                                    $scope.$broadcast("DetachCateAttributes");
                                }).error(function (response) {
                                });
                            }
                        }
                    });


                }


            });
        }
    };

    $scope.getCustomerIDs = "";
    $scope.getCustomerCompanyName = "studiosoft";
    // $rootScope.DisplayIdcolumns=true;
    $rootScope.AllowDuplicateItem_PartNum = true;
    $rootScope.EnableWorkflow = false;
    $rootScope.FamilyLevelMultipletablePreview = true;
    $rootScope.ProductLevelMultipletablePreview = true;
    $rootScope.DisplaySkuProductCountInAlert = true;
    $rootScope.SKUAlertPercentage = 100;
    $rootScope.SKUAlertPercentageCalculated = 99;

    $scope.getUserProductSKUCount = function () {
        dataFactory.getUserProductSKUCount().success(function (response) {
            if (response !== "" && response !== null) {

                $rootScope.ProductSKUCount = response;
            }
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.getUserProductCount = function () {
        dataFactory.getUserProductCount('PRODUCT', '').success(function (response) {
            if (response !== "" && response !== null) {
                $rootScope.ProductCount = response;
                // var toolbarProduct1 = $("#product-toolbar").data("kendoToolBar");
                if ($rootScope.ProductCount < $rootScope.ProductSKUCount) {
                    // if (toolbarProduct1 !== null && toolbarProduct1 !== undefined) {
                    //  toolbarProduct1.enable("#product-toolbar-new-product", true);
                    //  }
                    $('#newbtn').removeAttr('disabled');
                }
                else {
                    // if (toolbarProduct1 !== null && toolbarProduct1 !== undefined) {
                    // toolbarProduct1.enable("#product-toolbar-new-product", false);
                    //  }
                    $('#newbtn').attr('disabled', 'disabled');
                    $scope.userRoleAddProductTable = false;
                }

                if ($rootScope.ProductCount >= ($rootScope.ProductSKUCount * ($rootScope.SKUAlertPercentage / 100))) {

                    if ($rootScope.ProductCount > 0 && $rootScope.ProductSKUCount > 0) {
                        $rootScope.SKUAlertPercentageCalculated = Math.round(($rootScope.ProductCount / $rootScope.ProductSKUCount) * 100);

                        if ($rootScope.SKUAlertPercentage != 100 && $localStorage.skuAlertDisplayed == 1) {
                            if ($scope.skuAlertOptions !== null && $scope.skuAlertOptions !== undefined) {

                                $scope.skuAlertOptions.refresh({ url: "../views/app/partials/skuAlert.html" });
                                $scope.skuAlertOptions.center().open();
                            }
                        }
                    }
                }


            }
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.displaySKUAlert = function () {
        //

        //if ($rootScope.ProductCount >= ($rootScope.ProductSKUCount * ($rootScope.SKUAlertPercentage / 100))) {

        //    alert("You have reached the " + $rootScope.SKUAlertPercentage + "% of your SKU limit.");
        //}
    };
    $rootScope.userSuperAdmin = false;
    $rootScope.userAdmin = false;
    $rootScope.userAdminUsers = false;
    $rootScope.usercss = false;
    $scope.getUserSuperAdmin = function () {

        dataFactory.getUserSuperAdmin().success(function (response) {
            if (response !== "" && response !== null) {
                if (response == "1") {
                    $rootScope.userSuperAdmin = true;
                    $rootScope.userAdmin = false;
                    $rootScope.userAdminUsers = false;
                    $rootScope.usercss = 'fa fa-user-secret fa-lg';
                }
                else if (response == "2") {
                    $rootScope.userSuperAdmin = false;
                    $rootScope.userAdmin = true;
                    $rootScope.userAdminUsers = false;
                    $rootScope.usercss = 'fa fa-user fa-lg';
                }
                else {
                    $rootScope.userSuperAdmin = false;
                    $rootScope.userAdmin = false;
                    $rootScope.userAdminUsers = true;
                    $rootScope.usercss = 'fa fa-users fa-lg';
                }

            }

        }).error(function (error) {
            options.error(error);
        });
    };



    $scope.CloneFamily = function () {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Do you want to clone this Product?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    if ($rootScope.SelectedNodeID.includes('~!')) {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
                    }
                    else if ($rootScope.SelectedNodeID.includes('~')) {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
                    }
                    $rootScope.familyDeleteNavStatus = true;
                    $rootScope.SelectedNodeIDPresist = 1;
                    $http.get('../Family/CloneFamily?CopiedId=' + $scope.CopiedId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogID=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog)
                        .then(function (response) {
                            if (response.data.trim() !== "") {
                                if (response.data === "Product Cloned successfully") {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response.data + '.',
                                        type: "info"
                                    });
                                } else {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response.data + '.',
                                        type: "error"
                                    });

                                }
                            }
                            $scope.checkeditems = '';
                            $scope.IdsForAnotherCatalog = '';
                            $scope.RefreshLeftNavTree();
                            $scope.RightClickedCategoryId = '';
                        });
                } else {
                    $scope.checkeditems = '';
                    $scope.IdsForAnotherCatalog = '';
                    $scope.RefreshLeftNavTree();
                }
            }
        })
        ;
    };

    $scope.CloneRemoveFamily = function () {

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to remove this Product clone?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {

                    if ($rootScope.SelectedNodeID.includes('~!')) {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
                    }
                    else if ($rootScope.SelectedNodeID.includes('~')) {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
                    }
                    else {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID;
                    }
                    if ($rootScope.familyDeleteNav.includes("~")) {
                        $rootScope.familyDeleteNav = $rootScope.familyDeleteNav.split("~")[0];
                    }
                    $rootScope.familyDeleteNavStatus = true;
                    $rootScope.SelectedNodeIDPresist = 1;
                    $http.get('../Family/CloneRemoveFamily?CopiedId=' + $scope.CopiedId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogID=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&MultipleFamilyids=' + $scope.checkeditems)
                        .then(function (response) {
                            $scope.checkeditems = '';
                            $scope.RefreshLeftNavTree();
                            $scope.RightClickedCategoryId = '';
                        });
                } else {
                    $scope.RefreshLeftNavTree();
                }
            }
        });
    };

    $scope.CloneasRootCategory = function () {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Do you want to clone this Category?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                $scope.option = "root";
                if (bool === true) {
                    if ($rootScope.SelectedNodeID.includes('~!')) {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
                    }
                    else if ($rootScope.SelectedNodeID.includes('~')) {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
                    }
                    $rootScope.treeForCategory = true;
                    $rootScope.familyDeleteNavStatus = true;
                    $rootScope.SelectedNodeIDPresist = 1;
                    $http.get('../Category/CloneasRootCategory?Catid=' + $scope.CopyCategoryId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogId=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&option=' + $scope.option + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog)
                        .then(function (e) {
                            if (e.data === "True") {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Category Cloned.',
                                    type: "info"
                                });
                            }
                            else if (e.data === "False") {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Action not Sucessful.',
                                    type: "info"
                                });
                            }
                            else if (e.data.contains("Name already exists")) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + e.data + '.',
                                    type: "info"
                                });
                            }
                            $scope.checkeditems = '';
                            $scope.IdsForAnotherCatalog = '';
                            $scope.RefreshLeftNavTree();
                            $scope.RightClickedCategoryId = '';
                        });
                } else {
                    $scope.RefreshLeftNavTree();
                }
                $('#CloneasRooCategory').hide();
                $('#CloneCategory').hide();
            }
        });
    };

    $scope.CloneCategory = function () {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Do you want to clone this Category?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                $scope.option = "sub";
                if (bool === true) {
                    if ($rootScope.SelectedNodeID.includes('~!')) {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
                    }
                    else if ($rootScope.SelectedNodeID.includes('~')) {
                        $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
                    }
                    $rootScope.treeForCategory = true;
                    $rootScope.familyDeleteNavStatus = true;
                    $rootScope.SelectedNodeIDPresist = 1;
                    $http.get('../Category/CloneasRootCategory?Catid=' + $scope.CopyCategoryId + '&RightClickedCategoryId=' + $scope.RightClickedCategoryId + '&catalogId=' + $scope.RightClickedCatalogID + '&previousCatalogID=' + $scope.PreviousCatalogID + '&option=' + $scope.option + '&MultipleFamilyids=' + $scope.checkeditems + '&MultipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog)
                        .then(function (e) {
                            if (e.data === "True") {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Category Cloned.',
                                    type: "info"
                                });
                            }
                            else if (e.data === "False") {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Action not Sucessful.',
                                    type: "info"
                                });
                            }
                            else if (e.data.contains("Name already exists")) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + e.data + '.',
                                    type: "info"
                                });
                            }
                            $scope.checkeditems = '';
                            $scope.IdsForAnotherCatalog = '';
                            $scope.RefreshLeftNavTree();
                        });
                } else {
                    $scope.RefreshLeftNavTree();
                }
                $('#CloneasRooCategory').hide();
                $('#CloneCategory').hide();
            }
        });
    };
    $scope.TrashFromNavigator = function () {
        $('#SelectFunction').show();
        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }
        $rootScope.treeForCategory = true;
        $rootScope.familyDeleteNavStatus = true;
        $rootScope.SelectedNodeIDPresist = 1;
        $scope.associationRemove = "Association";
        $scope.FlagforDelete = 'category';
        $scope.Remove.refresh({ url: "../views/app/partials/CategoryRemove.html" });
        $scope.RemoveFlag = '1';
        if ($rootScope.SelectedRightnavigationStatus == "CategoryClone") {
            category = "#dialog-confirm10";
        }
        else if ($rootScope.SelectedRightnavigationStatus == "category") {

            category = "#dialog-confirm4";
        }
        $(category).show();
        $(category).dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Yes": function () {

                    if ($scope.RemoveFlag !== '') {
                        if ($scope.FlagforDelete === "category" && $scope.checkeditems !== "" || $scope.RightClickedCategoryId !== "" && !$scope.RightClickedCategoryId.contains('~')) {
                            $scope.Cancel();
                            dataFactory.CategorytrashFromNavigator($scope.RightClickedCategoryId, $scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag).success(function (response) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });

                                $scope.$broadcast("DetachCateAttributes");
                                $scope.RefreshLeftNavTree();
                                $scope.RemoveFlag = '0';
                                $scope.RightClickedCategoryId = '';
                                $scope.associationRemove = "Association";
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }
                    $(this).dialog("close");
                },
                "No": function () {

                    $(this).dialog("close");
                }
            }

        });


        // $("#dialog-confirm3").hide();
        // $scope.Remove.center().open();
    };

    $scope.DeleteFromCloneFamily = function () {
        if ($rootScope.SelectedNodeID.includes('~!')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        }
        else if ($rootScope.SelectedNodeID.includes('~')) {
            $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        }

        $rootScope.familyDeleteNavStatus = true;
        $rootScope.SelectedNodeIDPresist = 1;
        $scope.FamilyClone.refresh({ url: "../views/app/partials/FamilyCloneCheckOption.html" });
        $scope.FamilyClone.center().close();
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "This action will remove the original Product along with the all clones, do you want to continue?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    dataFactory.DeleteFamilyPermanentFromMaster($rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId, $rootScope.selecetedCatalogId).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });
                        $rootScope.treeData.read();
                        // $scope.treeData.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
    };
    $scope.AssignMasterFamily = function () {
        var id = $scope.RightClickedCategoryId;
        $scope.RightClickedCategoryId = $rootScope.selecetedFamilyId;
        $scope.originalCategoryDatasourceShowMaster.read();
        $scope.clonedCategoryDatasourceShowMaster.read();
        $scope.RightClickedCategoryId = id;
        $scope.ShowMaster.refresh({ url: "../views/app/partials/SwitchMaster.html" });
        $scope.ShowMaster.center().open();
        $scope.FamilyClone.refresh({ url: "../views/app/partials/FamilyCloneCheckOption.html" });
        $scope.FamilyClone.center().close();
    };
    $scope.CancelFromFamily = function () {
        $scope.FamilyClone.refresh({ url: "../views/app/partials/FamilyCloneCheckOption.html" });
        $scope.FamilyClone.center().close();
    };


    $scope.val_type = "true";
    $scope.TextBoxChanged = function (e, id, attribute) {

        var url = e.currentTarget.value;
        var ids = e.currentTarget.id;
        var pattern = /((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)/;
        if (pattern.test(url)) {
            $scope.val_type = "true";
        } else {
            $scope.selectedRow[attribute] = "";
            //$("#" + ids).focus();
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a valid URL.',
                type: "error"
            });
        }
    };
    //----------------------------------------------------------start user rights ------------------------------------------------------------------------------------------------
    $scope.$broadcast("oldCatalog");
    $scope.GetUserRoleRightsAll = function () {
        dataFactory.GetUserRoleRightsAll($localStorage.getCatalogID).success(function (response) {
            if (response.length > 0) {
                $rootScope.EditMode = true;
                angular.forEach(response, function (value) {
                    if (value.FUNCTION_ID === 40202) {

                        $scope.userRoleAddCategory = value.ACTION_ADD;
                        $scope.userRoleModifyCategory = value.ACTION_MODIFY;
                        $scope.userRoleDeleteCategory = value.ACTION_REMOVE;
                        $scope.userRoleViewCategory = value.ACTION_VIEW;
                        $scope.userRoleDetachCategory = value.ACTION_DETACH;
                    }
                    else if (value.FUNCTION_ID === 5000085) {
                        //$rootscope.userRoleAddSystemid = value.ACTION_ADD;	
                        $rootScope.userRoleViewSystemid = value.ACTION_VIEW;
                    }//new
                    else if (value.FUNCTION_ID === 40203) {

                        $scope.userRoleAddFamily = value.ACTION_ADD;
                        $scope.userRoleModifyFamily = value.ACTION_MODIFY;
                        $scope.userRoleDeleteFamily = value.ACTION_REMOVE;
                        $rootScope.userRoleDetachFamily = value.ACTION_DETACH;
                        $scope.userRoleViewFamily = value.ACTION_VIEW;

                    }
                    else if (value.FUNCTION_ID === 5000033) {

                        $rootScope.userprodtable = value.ACTION_ADD; // Product Table Manager  -- add only 
                    }
                    else if (value.FUNCTION_ID === 40303) {
                        $rootScope.userRoleAddExportIMPORT = value.ACTION_ADD;
                        $rootScope.userRoleViewExportIMPORT = value.ACTION_VIEW;
                    } else if (value.FUNCTION_ID === 40304) {
                        $rootScope.userRoleAddImport = value.ACTION_ADD;
                    } else if (value.FUNCTION_ID === 40208) {
                        $rootScope.userRoleAddWizard = value.ACTION_ADD;
                    } else if (value.FUNCTION_ID === 4020402) {
                        $rootScope.userRoleAddRecycleBin = value.ACTION_ADD;
                    } else if (value.FUNCTION_ID === 4020403) {

                        $rootScope.userRoleAddReferenceTable = value.ACTION_ADD;
                        $rootScope.userRoleViewReferenceTable = value.ACTION_VIEW;
                        $rootScope.userRoleRemoveReferenceTable = value.ACTION_REMOVE;
                        $rootScope.userRoleModifyReferenceTable = value.ACTION_MODIFY;
                        $rootScope.userRoleDetachReferenceTable = value.ACTION_DETACH;
                    } else if (value.FUNCTION_ID === 40301) {
                        $rootScope.userRoleAddInDesign = value.ACTION_ADD;
                    } else if (value.FUNCTION_ID === 4020301) {
                        $scope.userRoleAddProductTable = value.ACTION_ADD;
                        $scope.userRoleModifyProductTable = value.ACTION_MODIFY;
                        $scope.userRoleDeleteProductTable = value.ACTION_REMOVE;
                        $scope.userRoleViewProductTable = value.ACTION_VIEW;
                    } else if (value.FUNCTION_ID === 5000033) {
                        $scope.userRoleAddProductTableManage = value.ACTION_ADD;
                        $scope.userRoleModifyProductTableManage = value.ACTION_MODIFY;
                        $scope.userRoleDeleteProductTableManage = value.ACTION_REMOVE;
                        $scope.userRoleDetachProductTableManage = value.ACTION_DETACH;
                        $scope.userRoleViewProductTableManage = value.ACTION_VIEW;
                        $rootScope.userprodtable = value.ACTION_VIEW;;
                        //$rootScope.userprodtable = value.ACTION_ADD;
                    } else if (value.FUNCTION_ID === 402) {
                        $scope.userRoleAddCreateManage = value.ACTION_ADD;
                        $scope.userRoleModifyCreateManage = value.ACTION_MODIFY;
                        $scope.userRoleDeleteCreateManage = value.ACTION_REMOVE;
                        $scope.userRoleViewCreateManage = value.ACTION_VIEW;
                    } else if (value.FUNCTION_ID === 5000083) {
                        $rootScope.userSearchEnable = value.ACTION_ADD; // Search add only
                        $('#searchli').show();
                    }
                    else if (value.FUNCTION_ID === 5000084) {
                        $rootScope.userRoleAssetView = value.ACTION_VIEW;
                        $rootScope.userRoleAssetAdd = value.ACTION_ADD;
                        $rootScope.userRoleAssetModify = value.ACTION_MODIFY;
                        $rootScope.userRoleAssetRemove = value.ACTION_REMOVE;
                        $rootScope.userImageEnable = value.ACTION_ADD; // Search add only
                        $('#imagemanageli').show();
                    }
                    else if (value.FUNCTION_ID === 5000001) {
                        $rootScope.userSelectallattributescheckbox = value.ACTION_ADD; // Provide Check box to select all attributes in table designer row -- Add only
                    }
                    else if (value.FUNCTION_ID === 5000002) {
                        $rootScope.userCopypastedel = value.ACTION_ADD; // Copy / Paste / Delete -- Add only
                    }
                    else if (value.FUNCTION_ID === 5000003) {
                        $rootScope.userClone = value.ACTION_ADD; // Clone feature -- Add only
                    }
                    else if (value.FUNCTION_ID === 5000004) {
                        $rootScope.userStyle = value.ACTION_ADD; // style  -- Add only
                    }
                    else if (value.FUNCTION_ID === 5000005) {
                        $rootScope.userHTMLCatalog = value.ACTION_ADD; //  html catalog  -- Add only
                    }
                    else if (value.FUNCTION_ID === 5000010) {
                        $rootScope.userPdfCreate = value.ACTION_ADD; // Create & Manage PDF -- Add only
                    }
                    else if (value.FUNCTION_ID === 5000012) {
                        $rootScope.userpdfdestemp = value.ACTION_ADD; // Design Template - add only
                    }
                    else if (value.FUNCTION_ID === 5000013) {
                        $rootScope.userpdfrun = value.ACTION_ADD; // Run  - add only 
                    }
                    else if (value.FUNCTION_ID === 5000014) {
                        $rootScope.userpdfopencat = value.ACTION_ADD; // Open Catalog -- add only
                    }
                    else if (value.FUNCTION_ID === 5000082) {
                        $rootScope.userSummary = value.ACTION_ADD; // Summary - add only 
                    }
                    else if (value.FUNCTION_ID === 5000081) {
                        $rootScope.userMissingImage = value.ACTION_ADD; // Missing Images list -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000080) {
                        $rootScope.userReportRun = value.ACTION_ADD;  // Run Report add only
                    }
                    else if (value.FUNCTION_ID === 5000070) {
                        $rootScope.userIndesign = value.ACTION_ADD; // Create & Manage  -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000071) {
                        $rootScope.userIndesigncpy = value.ACTION_ADD; // copy XML  -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000072) {
                        $rootScope.userIndesignmove = value.ACTION_ADD; // Move XML -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000061) {
                        $rootScope.userGlobalattrassociation = value.ACTION_ADD; // Attribute Association -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000062) {
                        $rootScope.userGlobalcolumnsort = value.ACTION_ADD; // Column Sorter
                    }
                    else if (value.FUNCTION_ID === 5000063) {
                        $rootScope.userGlobalcalcattr = value.ACTION_ADD; // Calculated Attributes
                    }
                    else if (value.FUNCTION_ID === 5000064) {
                        $rootScope.userGlobalattrval = value.ACTION_ADD; // Attibute value update
                    }
                    else if (value.FUNCTION_ID === 5000051) {
                        if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY || value.ACTION_MODIFY) {
                            $rootScope.userMultiplenable = true; // Multiple Table
                        }
                        $rootScope.userMultiplenableAdd = value.ACTION_ADD;
                        $rootScope.userMultiplenableModify = value.ACTION_MODIFY;
                        $rootScope.userMultiplenableView = value.ACTION_VIEW;
                        $rootScope.userMultiplenableRemove = value.ACTION_REMOVE;

                    }
                    else if (value.FUNCTION_ID === 5000030) {
                        $rootScope.userinvp = value.ACTION_ADD;
                        $rootScope.userinvpView = value.ACTION_VIEW;// Inverted Product List  -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000031) {
                        $rootScope.userinvpsub = value.ACTION_ADD; // Inverted sub Product List  -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000032) {
                        $rootScope.usercommon = value.ACTION_ADD; // Common Value update -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000033) {

                        $rootScope.userprodtable = value.ACTION_ADD; // Product Table Manager  -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000022) {
                        $rootScope.userprodcongig = value.ACTION_ADD; // Product Configurator  -- add only 
                    }
                    else if (value.FUNCTION_ID === 5000020) {
                        $rootScope.userPrint = value.ACTION_ADD; // Print option in family preview - add only 
                    }
                    else if (value.FUNCTION_ID === 5000021) {
                        $rootScope.userpref = value.ACTION_ADD; // Preference Settings - add only 
                    }
                    else if (value.FUNCTION_ID === 40206) {
                        if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY) {
                            $rootScope.userSupplier = true; // Validated in supplier controller.
                        }
                        $rootScope.userRoleModifySupplier = value.ACTION_MODIFY;
                        $rootScope.userRoleDeleteSupplier = value.ACTION_REMOVE;
                        $rootScope.userRoleAddSupplier = value.ACTION_ADD;
                        $rootScope.userRoleViewSupplier = value.ACTION_VIEW;
                    }
                    else if (value.FUNCTION_ID === 40201) {
                        if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY) {
                            $rootScope.userCatalog = true; // validated in catalog controller.
                        }
                    }
                    else if (value.FUNCTION_ID === 40205) {

                        if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY) {
                            $rootScope.userPickList = true; // Validated in picklist controller.
                        }
                        $rootScope.userRoleAddPickListManagement = value.ACTION_ADD;
                        $rootScope.userRoleModifyPickListManagement = value.ACTION_MODIFY;
                        $rootScope.userRoleDeletePickListManagement = value.ACTION_REMOVE;
                    }
                    else if (value.FUNCTION_ID === 40204) {

                        if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY) {
                            $rootScope.userAttribute = true; // validated in attribute controller.
                            $scope.userRoleAddAttributeManagement = value.ACTION_ADD;
                        }
                        $rootScope.userRoleAddAttributeManagement = value.ACTION_ADD;
                        $rootScope.userRoleModifyAttributeManagement = value.ACTION_MODIFY;
                        $rootScope.userRoleDetachAttributeManagement = value.ACTION_DETACH;
                        $rootScope.userRoleDeleteAttributeManagement = value.ACTION_REMOVE;
                    }
                    else if (value.FUNCTION_ID === 40207) {
                        $rootScope.userImageandAttachments = value.ACTION_ADD; // image and attach

                    }
                    else if (value.FUNCTION_ID === 40305) {

                        $rootScope.webSyncEnabled = value.ACTION_ADD;
                        $rootScope.webSyncModifiedEnabled = value.ACTION_MODIFY;// Product Table Manager  -- add only 
                    }

                    else if (value.FUNCTION_ID === 4020407) {//For custom app review and remove
                        if (value.ACTION_VIEW)
                            $rootScope.customApp = true;
                        else if (value.ACTION_REMOVE)
                            $rootScope.customApp = false;
                    }

                    //else if(value.FUNCTION_ID === 0)
                    //{
                    //    $scope.ResetControlls();
                    //    $scope.userRoleAddCreateManage = true;
                    //    $rootScope.userinvp = true;
                    //    $rootScope.userAttribute = false;
                    //    $rootScope.userPickList = false;
                    //    $rootScope.userCatalog = false;
                    //    $rootScope.userSupplier = false;
                    //    $rootScope.userRoleAddReferenceTable = false;
                    //    $rootScope.EnableWorkflow = false;
                    //    $rootScope.userRoleAddExportIMPORT = true;


                    //    $rootScope.EditMode = false;

                    //}

                });
                dataFactory.GetUserWiseCatalog($localStorage.getCatalogID).success(function (response) {
                    if (response !== null) {
                        if (response.length > 0) {
                            $rootScope.EditMode = response[0].IsRead == true ? false : true;
                            $localStorage.EditMode = $rootScope.EditMode;
                            if ($rootScope.EditMode == false) {
                                $scope.userRoleAddCreateManage = true;
                                $rootScope.userpdfrun = true;
                                $('#openTemp').hide();
                                $rootScope.userIndesign = true;
                                $rootScope.userGlobalattrassociation = true;
                                $rootScope.userprodtable = true;
                                $rootScope.userMissingImage = true;
                                $rootScope.userReportRun = true;
                                $rootScope.userRoleAddWizard = true;
                                $rootScope.userSummary = true;
                                $rootScope.userSearchEnable = true;
                                $rootScope.userRoleAssetView = true;
                                $rootScope.userRoleModifyPickList = true;
                                $rootScope.userRoleAddRecycleBin = true;
                            }
                        }
                    }
                });
            }

        }).error(function (error) {
            options.error(error);
        });
    };
    //$rootScope.onloaddynamic = function () {
    //     
    //};
    //----------------------------------------------------------start user rights ------------------------------------------------------------------------------------------------
    $scope.customerWiseCatalog = function (catalogId) {
        dataFactory.GetUserRoleRightsAll(catalogId).success(function (response) {
            if (response.length > 0) {
                angular.forEach(response, function (value) {

                    $scope.userRoleAddCategory = value.ACTION_ADD;
                    $scope.userRoleModifyCategory = value.ACTION_MODIFY;
                    $scope.userRoleDeleteCategory = value.ACTION_REMOVE;
                    $scope.userRoleViewCategory = value.ACTION_VIEW;

                    $scope.userRoleAddFamily = value.ACTION_ADD;
                    $scope.userRoleModifyFamily = value.ACTION_MODIFY;
                    $scope.userRoleDeleteFamily = value.ACTION_REMOVE;
                    $scope.userRoleViewFamily = value.ACTION_VIEW;


                    $rootScope.userRoleAddExportIMPORT = value.ACTION_ADD;

                    $rootScope.userRoleAddImport = value.ACTION_ADD;

                    $rootScope.userRoleAddWizard = value.ACTION_ADD;

                    $rootScope.userRoleAddRecycleBin = value.ACTION_ADD;

                    $rootScope.userRoleAddReferenceTable = value.ACTION_ADD;

                    $rootScope.userRoleAddInDesign = value.ACTION_ADD;

                    $scope.userRoleAddProductTable = value.ACTION_ADD;
                    $scope.userRoleModifyProductTable = value.ACTION_MODIFY;
                    $scope.userRoleDeleteProductTable = value.ACTION_REMOVE;
                    $scope.userRoleViewProductTable = value.ACTION_VIEW;

                    $scope.userRoleAddCreateManage = true;
                    $scope.userRoleModifyCreateManage = value.ACTION_MODIFY;
                    $scope.userRoleDeleteCreateManage = value.ACTION_REMOVE;
                    $scope.userRoleViewCreateManage = value.ACTION_VIEW;

                    $rootScope.userSearchEnable = value.ACTION_ADD; // Search add only
                    $rootScope.userRoleAssetView = value.ACTION_VIEW;
                    $rootScope.userRoleAssetAdd = value.ACTION_ADD;
                    $rootScope.userRoleAssetModify = value.ACTION_MODIFY;
                    $rootScope.userRoleAssetRemove = value.ACTION_REMOVE; // Search add only


                    $rootScope.userSelectallattributescheckbox = value.ACTION_ADD; // Provide Check box to select all attributes in table designer row -- Add only

                    $rootScope.userCopypastedel = value.ACTION_ADD; // Copy / Paste / Delete -- Add only

                    $rootScope.userClone = value.ACTION_ADD; // Clone feature -- Add only

                    $rootScope.userStyle = value.ACTION_ADD; // style  -- Add only

                    $rootScope.userHTMLCatalog = value.ACTION_ADD; //  html catalog  -- Add only

                    $rootScope.userPdfCreate = value.ACTION_ADD; // Create & Manage PDF -- Add only

                    $rootScope.userpdfdestemp = value.ACTION_ADD; // Design Template - add only

                    $rootScope.userpdfrun = value.ACTION_ADD; // Run  - add only 

                    $rootScope.userpdfopencat = value.ACTION_ADD; // Open Catalog -- add only

                    $rootScope.userSummary = value.ACTION_ADD; // Summary - add only 

                    $rootScope.userMissingImage = value.ACTION_ADD; // Missing Images list -- add only 

                    $rootScope.userReportRun = value.ACTION_ADD;  // Run Report add only

                    $rootScope.userIndesign = value.ACTION_ADD; // Create & Manage  -- add only 

                    $rootScope.userIndesigncpy = value.ACTION_ADD; // copy XML  -- add only 

                    $rootScope.userIndesignmove = value.ACTION_ADD; // Move XML -- add only 


                    $rootScope.userGlobalattrassociation = value.ACTION_ADD; // Attribute Association -- add only 

                    $rootScope.userGlobalcolumnsort = value.ACTION_ADD; // Column Sorter

                    $rootScope.userGlobalcalcattr = value.ACTION_ADD; // Calculated Attributes

                    $rootScope.userGlobalattrval = value.ACTION_ADD; // Attibute value update

                    if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY || value.ACTION_MODIFY) {
                        $rootScope.userMultiplenable = true; // Multiple Table
                    }
                    $rootScope.userMultiplenableAdd = value.ACTION_ADD;
                    $rootScope.userMultiplenableModify = value.ACTION_MODIFY;
                    $rootScope.userMultiplenableView = value.ACTION_VIEW;
                    $rootScope.userMultiplenableRemove = value.ACTION_REMOVE;


                    $rootScope.userinvp = value.ACTION_ADD;  // Inverted Product List  -- add only 

                    $rootScope.userinvpsub = value.ACTION_ADD; // Inverted sub Product List  -- add only 

                    $rootScope.usercommon = value.ACTION_ADD; // Common Value update -- add only 

                    $rootScope.userprodtable = value.ACTION_ADD; // Product Table Manager  -- add only 

                    $rootScope.userprodcongig = value.ACTION_ADD; // Product Configurator  -- add only 

                    $rootScope.userPrint = value.ACTION_ADD; // Print option in family preview - add only 

                    $rootScope.userpref = value.ACTION_ADD; // Preference Settings - add only 


                    if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY) {
                        $rootScope.userSupplier = true; // Validated in supplier controller.
                    }

                    if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY) {
                        $rootScope.userCatalog = true; // validated in catalog controller.
                    }

                    if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY) {
                        $rootScope.userPickList = true; // Validated in picklist controller.
                    }

                    if (value.ACTION_ADD || value.ACTION_REMOVE || value.ACTION_VIEW || value.ACTION_MODIFY) {
                        $rootScope.userAttribute = true; // validated in attribute controller.
                    }

                    $rootScope.userImageandAttachments = value.ACTION_ADD; // image and attach

                });
            }

        }).error(function (error) {
            options.error(error);
        });
    };


    $scope.navigatesubprodata = function (e) {
        var tbs = $("#productgridtabstrip").kendoTabStrip({

            change: function (e) {
                //some code
            }
        }).data("kendoTabStrip");
        tbs.select(0);
        $scope.ProdCurrentPage = "1";
        $scope.ProdCountPerPage = "10";
        $scope.sub_id = '0';
        //dataFactory.getprodspecs(catalogId, id, $scope.displayID).success(function (response) {
        //document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaginghighlight";
        var ProdCount = [];
        dataFactory.getprodspecsWithoutAttributes(e.CATALOG_ID, $rootScope.sub_fam_id, true, $rootScope.sub_cat_id).success(function (response) {

            var obj = jQuery.parseJSON(response.Data.Data);

            var ProdEditPageloopcnt = Math.floor(obj.length / parseInt($scope.ProdCountPerPage));

            if ((obj.length % parseInt($scope.ProdCountPerPage)) > 0) {
                ProdEditPageloopcnt = ProdEditPageloopcnt + 1;
            }


            for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                ProdCount.push(i);
            }

            $scope.ProdPageCount = ProdCount;
            $scope.ProdCurrentPage = 1;
            $scope.totalProdPageCount = ProdEditPageloopcnt;

        });

        dataFactory.getprodspecs(e.CATALOG_ID, $rootScope.sub_fam_id, true, $rootScope.sub_cat_id, $scope.ProdCurrentPage, $scope.ProdCountPerPage, e.SUBPRODUCT_ID).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.prodData = obj;


            //To Change Dynamice CatalogNo
            for (var i = 0; i < response.Data.Columns.length; i++) {
                if (response.Data.Columns[i]["Caption"].includes("ITEM#")) {
                    response.Data.Columns[i]["Caption"] = response.Data.Columns[i]["Caption"].replace("ITEM#", $localStorage.CatalogItemNumber);
                }
            }


            $scope.columnsForAtt = response.Data.Columns;
            angular.forEach($scope.prodData, function (value) {
                value.SORT = parseFloat(value.SORT);
            });


            // $rootScope.visibleProjects = $scope.prodData;
            // $rootScope.visibleProjects1 = $scope.prodData;
            // $data = $scope.prodData;
            if ($scope.prodData.length === 0) {
                $("#newProductBtn").show();
                $("#newProductBtn1").hide();
                $("#newProductBtn").css("display", "block");
                $("#newProductBtn1").css("display", "none");
            }
            else {
                $("#newProductBtn").hide();
                $("#newProductBtn1").show();
                $("#newProductBtn").css("display", "none");
                $("#newProductBtn1").css("display", "block");
            }
            //$scope.tblDashBoard.reload();
            // $scope.tblFamilyGrid.reload();
            // $scope.tblFamilyGrid.$params.page = 1;

            if ($scope.userRoleDeleteFamily) {
                // $('#user-toolbar-association-removal-family').removeAttr('disabled');
                $('#user-toolbar-delete-family').removeAttr('disabled');
            }
            else {
                //$('#user-toolbar-association-removal-family').attr('disabled', 'disabled');
                $('#user-toolbar-delete-family').attr('disabled', 'disabled');
            }

            if ($scope.userRoleAddFamily) {
                $('#user-toolbar-create-new-family').removeAttr('disabled');
            }
            else {
                $('#user-toolbar-create-new-family').attr('disabled', 'disabled');
            }
            if ($scope.userRoleModifyFamily) {
                $('#user-toolbar-save-family').removeAttr('disabled');
                $('#user-toolbar-family-attribute-setup').removeAttr('disabled');
            }
            else {
                $('#user-toolbar-save-family').attr('disabled', 'disabled');
                $('#user-toolbar-family-attribute-setup').attr('disabled', 'disabled');
            }
            //  var toolbarProduct = $("#product-toolbar").data("kendoToolBar");

            if ($scope.userRoleAddProductTable) {
                $('#newbtn').removeAttr('disabled');
            }
            else {
                $('#newbtn').attr('disabled', 'disabled');
            }
            if ($rootScope.ProductCount < $rootScope.ProductSKUCount) {
            }
            else {
                $scope.userRoleAddProductTable = false;
            }

            if ($scope.userRoleModifyProductTable) {
                $('#columnsetubbtn').removeAttr('disabled');
                $('#user-toolbar-default-layout').removeAttr('disabled');
            }
            else {
                $('#columnsetubbtn').attr('disabled', 'disabled');
                $('#user-toolbar-default-layout').attr('disabled', 'disabled');
            }
            //if ($scope.userRoleDeleteFamily) {
            //    toolbar.enable("#user-toolbar-association-removal-family", true);
            //    toolbar.enable("#user-toolbar-delete-family", true);
            //}
            //else {
            //    toolbar.enable("#user-toolbar-association-removal-family", false);
            //    toolbar.enable("#user-toolbar-delete-family", false);
            //}
            if ($scope.prodData.length > 0) {
                $('#user-toolbar-export').removeAttr('disabled');
                $('#user-toolbar-table-designer').removeAttr('disabled');
                $('#user-toolbar-set-table-layout').removeAttr('disabled');
                $('#user-toolbar-delete-layout').removeAttr('disabled');
                $('#user-toolbar-create-new-layout').removeAttr('disabled');
                if ($scope.userRoleModifyProductTable) {
                    $('#user-toolbar-default-layout').removeAttr('disabled');
                }
                else {
                    $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                }
            }
            else {
                $('#user-toolbar-export').attr('disabled', 'disabled');
                $('#user-toolbar-table-designer').attr('disabled', 'disabled');
                $('#user-toolbar-set-table-layout').attr('disabled', 'disabled');
                $('#user-toolbar-delete-layout').attr('disabled', 'disabled');
                $('#user-toolbar-create-new-layout').attr('disabled', 'disabled');
                $('#user-toolbar-default-layout').attr('disabled', 'disabled');
            }

            if ($rootScope.userprodcongig === true) {
                $('#addtoprdconfig').removeAttr('disabled');
            } else {
                $('#addtoprdconfig').attr('disabled', 'disabled');
            }
            if ($rootScope.userprodcongig === true) {
                $('#prodconfig').removeAttr('disabled');
            } else {
                $('#prodconfig').attr('disabled', 'disabled');
            }
            if ($rootScope.userCopypastedel === true) {
                $('#cutbtn').removeAttr('disabled');
            } else {
                $('#cutbtn').attr('disabled', 'disabled');
            }
            if ($rootScope.userCopypastedel === true) {
                $('#copybtn').removeAttr('disabled');
            } else {
                $('#copybtn').attr('disabled', 'disabled');
            }
            if ($rootScope.userCopypastedel === true) {
                $('#pastebtn').removeAttr('disabled');
            } else {
                $('#pastebtn').attr('disabled', 'disabled');
            }
        }).error(function (error) {
            options.error(error);
        });

    }
    //----------Custom Menu--> To show custom menu in custom dropdown------------------
    $rootScope.getHeader = function () {

        dataFactory.getHeader().success(function (response) {

            $rootScope.header = response;
            $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

            dataFactory.getUserRoleId().success(function (response) {

                if (response != null) {

                    //$rootScope.header[0].URL = $rootScope.header[0].URL + '&?UID=' + response[0] + '&?RID=' + response[1];
                    $rootScope.header[0].URL = $rootScope.header[0].URL + '?UID=' + response[0] + '&RID=' + response[1];
                }
            }).error(function (error) {
                options.error(error);
            });

        }).error(function (error) {
            options.error(error);
        });
    }
    //----------------End-----------------

    //----------------------------------------------------------end user rights ------------------------------------------------------------------------------------------------
    $scope.init = function () {
        $scope.getUserDefaultCatalog();
        $scope.getUserDefaultCatalogItemNumber();
        if ($localStorage.getCatalogID === undefined) {
            $localStorage.getCatalogID = 0;
            $scope.SelectedCatalogId = 0;
            $rootScope.selecetedCatalogId = 0;
        } else {
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
            //  $rootScope.dropDownTreeViewDatasource.read();
            //  $rootScope.dropDownTreeViewParnetCategoryDatasource.read();
        }
        $('#SelectedCatalog').hide();
        $('#SelectedCatalogf').hide();
        $('#PasteCategoryFamily').hide();
        $('#PasteCategory').hide();
        $('#PasteRootCategory').hide();
        $('#PasteFamily').hide();
        $('#CloneFamily').hide();
        $('#lblmastercatalog').hide();
        $('#CloneRemoveFamily').hide();
        $('#CloneCategory').hide();
        $('#NavigationPin').hide();


        $('#CloneasRooCategory').hide();
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerIDs = 0;
            $scope.getCustomerCompanyName = "Studiosoft";
        }
        else {
            $scope.getCustomerIDs = $localStorage.getCustomerID;
            if ($scope.getCustomerIDs !== "") {
                dataFactory.FamilyLevelMultipletablePreview($scope.getCustomerIDs).success(function (response) {
                    if (response !== "" && response !== null) {

                        var val = response[0];
                        $rootScope.DisplayIdcolumns = val.DisplayIDColumns;
                        $rootScope.EnableWorkflow = val.EnableWorkFlow;
                        $rootScope.FamilyLevelMultipletablePreview = val.FamilyLevelMultitablePreview;
                        $rootScope.ProductLevelMultipletablePreview = val.ProductLevelMultitablePreview;
                        $rootScope.AllowDuplicateItem_PartNum = val.AllowDuplicateItem_PartNum;
                        $rootScope.DisplaySkuProductCountInAlert = val.DisplaySkuProductCountInAlert;
                        $rootScope.SKUAlertPercentage = val.SKUAlertPercentage;
                    }
                });
            }
        }

        if ($rootScope.DisplayIdcolumns == undefined) {
            dataFactory.GetPreferencesDetails($scope.getCustomerIDs).success(function (response) {

                $rootScope.DisplayIdcolumns = response[0].DisplayIDColumns;
            });
        }
        //  dataFactory.GetUserRoleRightsAll()
        $scope.GetUserRoleRightsAll();

        //$scope.getUserRoleRightsCategory();
        //$scope.getUserRoleRightsFamily();
        //$scope.getUserRoleRightsExport();
        //$scope.getUserRoleRightsImport();
        //$scope.getUserRoleRightsWizard();
        //$scope.getUserRoleRightsRecycleBin();
        //$scope.getUserRoleRightsReferenceTable();
        //$scope.getUserRoleRightsInDesign();
        //$scope.getUserRoleRightsProductTable();
        //$scope.getUserRoleRightsCreateManage();

        $scope.getUserProductSKUCount();
        $scope.getUserProductCount();
        $scope.getUserSuperAdmin();

        if ($localStorage.skuAlertDisplayed == null || $localStorage.skuAlertDisplayed == undefined) {
            $scope.displaySKUAlert();
            $localStorage.skuAlertDisplayed = 0;
            $localStorage.skuAlertDisplayed = $localStorage.skuAlertDisplayed + 1;
        }
        else {
            $localStorage.skuAlertDisplayed = $localStorage.skuAlertDisplayed + 1;
        }


        $rootScope.getHeader();//----Calling custom menu dropdown----
        dataFactory.getAnnouncementLastDateRecords().success(function (response) {
            $scope.GetAnnouncement = response;
        }).error(function (error) {
            options.error(error);
        });
    };
    //------------------------------DateTime Validation---------------------------------\
    //--Main/ Sub Product Edit , New Product / Sub product , Family Specs Edit, Common update product / subproduct------------------------------------------
    $scope.onDateSelected = function (e, attr) {
        var dateReg = /^\d{2}[./-]\d{2}[./-]\d{4}\s\d{2}:\d{2}\s[AP]M$/;
        if (!e.match(dateReg)) {

            if (attr.ATTRIBUTE_ID == null) {
                $('#datetime' + attr.split("__")[2]).val("");
                $('#datetimeNewProduct' + attr.split("__")[2]).val("");
                $('#datetimeNewSubproduct' + attr.split("__")[2]).val("");
                $('#datetimeEditProduct' + attr.split("__")[2]).val("");
                $('#datetimeEditSubproduct' + attr.split("__")[2]).val("");
            } else {
                $('#datetimeCommonUpdate' + attr.ATTRIBUTE_ID).val("");
            }

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter valid format.',
                type: "error"
            });
        }
    };

    //-----------------------------------------------------------------------------------\
    $scope.ResetControlls = function () {
        $scope.userRoleAddCategory = false;
        $scope.userRoleModifyCategory = false;
        $scope.userRoleDeleteCategory = false;
        $scope.userRoleViewCategory = false;
        $rootScope.Mainprodcount = 0;
        $scope.userRoleAddFamily = false;
        $scope.userRoleModifyFamily = false;
        $scope.userRoleDeleteFamily = false;
        $scope.userRoleViewFamily = false;

        $rootScope.userRoleAddExportIMPORT = false;
        // $rootScope.userRoleAddImport = false;
        $rootScope.userRoleAddWizard = false;
        $rootScope.userRoleAddRecycleBin = false;
        $rootScope.userRoleAddReferenceTable = false;
        $rootScope.userRoleAddInDesign = false;

        $scope.userRoleAddProductTable = false;
        $scope.userRoleModifyProductTable = false;
        $scope.userRoleDeleteProductTable = false;
        $scope.userRoleViewProductTable = false;

        $scope.userRoleAddCreateManage = false;
        $scope.userRoleModifyCreateManage = false;
        $scope.userRoleDeleteCreateManage = false;
        $scope.userRoleViewCreateManage = false;

        //------------------------------------------Search Enable -----------------------------------------//
        $rootScope.userSearchEnable = false;
        //------------------------------------------ImageManage Enable -----------------------------------------//
        $rootScope.userImageEnable = false;
        $rootScope.userRoleAssetView = false;
        $rootScope.userRoleAssetAdd = false;
        $rootScope.userRoleAssetModify = false;
        $rootScope.userRoleAssetRemove = false;

        //----------------------------------Create and Manage Features enable-----------------------------//
        $rootScope.userSelectallattributescheckbox = false;
        $rootScope.userCopypastedel = false;
        $rootScope.userClone = false;
        $rootScope.userStyle = false;
        $rootScope.userHTMLCatalog = false;
        //----------------------------------PDF Catalogs Features enable-----------------------------//
        $rootScope.userPdfCreate = false;
        $rootScope.userpdfdestemp = false;
        $rootScope.userpdfrun = false;
        $rootScope.userpdfopencat = false;
        //----------------------------------Summary -----------------------------//
        $rootScope.userSummary = false;

        //----------------------------------Missing Image -----------------------------//
        $rootScope.userMissingImage = false;
        //----------------------------------Report -----------------------------//
        $rootScope.userReportRun = false;
        //----------------------------------Idesign PDF -----------------------------//
        $rootScope.userIndesign = false;
        $rootScope.userIndesigncpy = false;
        $rootScope.userIndesignmove = false;
        //----------------------------------Global Attributes -----------------------------//
        $rootScope.userGlobalattrassociation = false;
        $rootScope.userGlobalcolumnsort = false;
        $rootScope.userGlobalcalcattr = false;
        $rootScope.userGlobalattrval = false;
        //----------------------------------Multiple tables -----------------------------//
        $rootScope.userMultiplenable = false;
        $rootScope.userMultiplenableAdd = false;
        $rootScope.userMultiplenableModify = false;
        $rootScope.userMultiplenableView = false;
        $rootScope.userMultiplenableRemove = false;



        //----------------------------------invertedprod -----------------------------//
        $rootScope.userinvp = false;
        //----------------------------------invertedsubprod -----------------------------//
        $rootScope.userinvpsub = false;
        //----------------------------------Proudcts -----------------------------//
        $rootScope.usercommon = false;
        $rootScope.userprodtable = false;
        $rootScope.userprodcongig = false;
        $rootScope.userprodcon = false;
        //----------------------------------print -----------------------------//
        $rootScope.userPrint = false;

        //----------------------prefernece------------
        $rootScope.userpref = false;
        // ---------------------- Supplier 
        $rootScope.userSupplier = false;
        // ---------------------- Supplier 
        $rootScope.userImageandAttachments = false;
        $rootScope.familyMainEditor = true;
        $rootScope.SubProdInvert = false;
        $rootScope.invertedproductsshow = false;
        $rootScope.invertedproductsbutton = false;
        $rootScope.invertedproductsbutton1 = false;
        $rootScope.btnSubProdInvert = false;
        //----------------------------------------------------------End user rights ------------------------------------------------------------------------------------------------
        //----------------------------------------------------------Edit Mode --------------------------------------------------------------------------------------------------------
        $rootScope.EditMode = true;
    }

    $scope.$watch('selected', function (fac) {
        $scope.$broadcast("rowSelected", fac);
    });
    $rootScope.gridresolution = function () {
        /*Tabstrip width*/

        var getClassName = $('#wrapper').attr('class');
        if (getClassName == "container-fluid") {
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#tabstrip").css("cssText", "max-width: 1255px !important;");
            }

            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#tabstrip").css("cssText", "max-width: 1255px !important;");

            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#tabstrip").css("cssText", "max-width: 100% !important;");

            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#tabstrip").css("cssText", "max-width: 1255px !important;");

            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#tabstrip").css("cssText", "max-width: 1255px !important;");


            }

        }

        else if (getClassName == "container-fluid toggled-2") {
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#tabstrip").css("cssText", "max-width: 95% !important;");

            }

            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $('#tabstrip').css('max-width', '95%', '!important');

            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $('#tabstrip').css("cssText", "max-width: 92% !important;margin-left: 30px;");

            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $('#tabstrip').css('max-width', '95%', '!important');

            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#tabstrip").css("cssText", "max-width: 95% !important;");


            }

        }

        var getClassName = $('#wrapper').attr('class');
        if (getClassName == "container-fluid") {
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1030px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1030px !important;");

            }

            if (window.screen.availWidth >= 1600 && window.screen.availWidth <= 1800) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1180px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1180px !important;");

            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $('#sampleProdgrid').css('max-width', '1220px', '!important');
                $('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1510px !important;");
                $('#sampleProdgrid tbody').css('max-width', '1510px', '!important');
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1590) {
                $('#sampleProdgrid').css('max-width', '1200px', '!important');
                $('#sampleProdgrid tbody').css('max-width', '1200px', '!important');
            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 950px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 950px !important;");

            }


        } else if (getClassName == "container-fluid toggled-2") {
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1240px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1240px !important;");
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $('#sampleProdgrid').css('max-width', '1220px', '!important');
                $('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1030px !important;");
                $('#sampleProdgrid tbody').css("cssText", "max-width: 1775px !important");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1440px !important;");
                $('#sampleProdgrid tbody').css("cssText", "max-width: 1440px !important");
            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1160px !important;");

            }

        }

        //var getClassName = $('#wrapper').attr('class');
        //if (getClassName == "container-fluid") {
        //    if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
        //        $("#sampleProdgrid5").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1030px !important;");
        //        $("#sampleProdgrid5 tbody").css("cssText", "margin-left: -2px;max-width: 1030px !important;");

        //    }

        //    if (window.screen.availWidth >= 1600 && window.screen.availWidth <= 1800) {
        //        $("#sampleProdgrid5").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1180px !important;");
        //        $("#sampleProdgrid5 tbody").css("cssText", "margin-left: -2px;max-width: 1180px !important;");

        //    }
        //    if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
        //        $('#sampleProdgrid5').css('max-width', '1220px', '!important');
        //        $('#sampleProdgrid5 tbody').css('max-width', '1220px', '!important');
        //    }

        //    if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
        //        $("#sampleProdgrid5").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1510px !important;");
        //        $('#sampleProdgrid5 tbody').css('max-width', '1510px', '!important');
        //    }

        //    if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1590) {
        //        $('#sampleProdgrid5').css('max-width', '1200px', '!important');
        //        $('#sampleProdgrid5 tbody').css('max-width', '1200px', '!important');
        //    }
        //    if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
        //        $("#sampleProdgrid5").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 950px !important;");
        //        $("#sampleProdgrid5 tbody").css("cssText", "margin-left: -2px;max-width: 950px !important;");

        //    }


        //} else if (getClassName == "container-fluid toggled-2") {
        //    if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
        //        $("#sampleProdgrid5").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1240px !important;");
        //        $("#sampleProdgrid5 tbody").css("cssText", "margin-left: -2px;max-width: 1240px !important;");
        //    }
        //    if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
        //        $('#sampleProdgrid5').css('max-width', '1220px', '!important');
        //        $('#sampleProdgrid5 tbody').css('max-width', '1220px', '!important');
        //    }

        //    if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
        //        $("#sampleProdgrid5").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1030px !important;");
        //        $('#sampleProdgrid5 tbody').css("cssText", "max-width: 1775px !important");
        //    }

        //    if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
        //        $("#sampleProdgrid5").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1440px !important;");
        //        $('#sampleProdgrid5 tbody').css("cssText", "max-width: 1440px !important");
        //    }
        //    if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
        //        $("#sampleProdgrid5").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
        //        $("#sampleProdgrid5 tbody").css("cssText", "margin-left: -2px;max-width: 1160px !important;");

        //    }

        //}

        /*Sub Product*/

        var getClassName = $('#wrapper').attr('class');
        if (getClassName == "container-fluid") {
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#samplegridss").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1020px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1020px !important;");
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $('#samplegridss').css('max-width', '1220px', '!important');
                $('#samplegridss tbody').css('max-width', '1220px', '!important');
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $('#samplegridss').css('max-width', '1300px', '!important');
                $('#samplegridss tbody').css('max-width', '1300px', '!important');
            }

            if (window.screen.availWidth >= 1600 && window.screen.availWidth <= 1800) {
                $("#samplegridss").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1180px !important;");
                $('#samplegridss tbody').css("cssText", "max-width: 1180px !important");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $('#samplegridss').css('max-width', '1220px', '!important');
                $('#samplegridss tbody').css('max-width', '1220px', '!important');
            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#samplegridss").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 950px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 950px !important;");
            }

        } else if (getClassName == "container-fluid toggled-2") {
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#samplegridss").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1240px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1240px !important;");
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $('#samplegridss').css('max-width', '1220px', '!important');
                $('#samplegridss tbody').css('max-width', '1220px', '!important');
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#samplegridss").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1758px !important;");
                $('#samplegridss tbody').css("cssText", "max-width: 1758px !important");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1590) {
                $('#samplegridss').css('max-width', '1435px', '!important');
                $('#samplegridss tbody').css('max-width', '1435px', '!important');
            }

            if (window.screen.availWidth >= 1600 && window.screen.availWidth <= 1800) {
                $("#samplegridss").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1435px !important;");
                $('#samplegridss tbody').css("cssText", "max-width: 1435px !important");
            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#samplegridss").css("cssText", "margin-top: 20px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1160px !important;");

            }
        }

    };
    $scope.isPinShow = false;
    $scope.PinTreeLoading = function (e) {
        var treedisplay = document.getElementById("sidebar-wrapper2");

        if (e == 'show') {
            treedisplay.style.display = "block";
            $rootScope.gridresolution();

        }

    }
    $scope.TreeLoading = function (e) {
        var treedisplay = document.getElementById("sidebar-wrapper2");
        if (e == 'show') {
            $('#page-content-wrapper1').css("cssText", "width: 99%;position: absolute;");
            $('#sidebar-wrapper2').css("cssText", "margin-top: -28px;margin-left: 0px;width: 19%;z-index: 9999;border-right: 1px solid #000;max-height: 600px;display: block;height: 536px;border-bottom: 1px solid #000;");
            $('#menu').css("cssText", "margin-left: 0px;list-style: none;")
            treedisplay.style.display = "block";
            $rootScope.gridresolution();

        }

        else {
            $('#page-content-wrapper1').css("cssText", "width: 99%;position: absolute;");
            treedisplay.style.display = "none";
            $rootScope.gridresolution();

        }

    }


    $scope.BodyClickHideUnpin = function () {


        var treedisplay = document.getElementById("sidebar-wrapper2");

        if (localStorage.Pin == 'yes') {
            $('#page-content-wrapper1').css("cssText", "width: 99%;position: absolute;");
            treedisplay.style.display = "none";
            $rootScope.gridresolution();

        }

    }

    //To get the Default Item# and SubItem# values:

    $scope.getUserDefaultCatalogItemNumber = function () {
        dataFactory.GetDefaultCatalogItemNumber().success(function (response) {
            if (response != null) {
                $localStorage.CatalogItemNumber = response[0];
                $localStorage.CatalogSubItemNumber = response[1];

                $rootScope.CatalogItemNumber = response[0];
                $rootScope.CatalogSubItemNumber = response[1];
            }
        }).error(function (error) {
            options.error(error);
        });
    };
    //--------------Set Default Family in dashboard------------
    $scope.setDefaultFamilyFromNavigator = function (family) {
        $scope.DefaultfamilyId = family.SelectedItem;
        $scope.dFamilyId = $scope.DefaultfamilyId.replace('~', "");
        $scope.selectedDefaultCatalogId = family.SelectedDataItem.CATALOG_ID;
        dataFactory.setDefaultFamilyFromNavigator($scope.selectedDefaultCatalogId, $scope.dFamilyId).success(function (response) {

            if (response == '1') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Default Product Set Successfully",
                    type: "info"
                });
                $rootScope.treeData.read();
            }

        })
    }
    $rootScope.GetClonefamilyDatasource = new kendo.data.DataSource({
        //type: "json",
        autoBind: true,
        transport: {
            read: function (options) {
                var ids = "";
                if ($scope.selecetedCatalogId.toString() === "1")
                    ids = $scope.Showmasteruncheckids;
                else
                    ids = $scope.Showmasterids;

                dataFactory.GetCloneMasterFamily($localStorage.getCatalogID, ids).success(function (response) {
                    if (response.length != 0) {
                        $scope.winGetClone.refresh({ url: "../views/app/Admin/clonefamily.html" });
                        $scope.winGetClone.center().open();
                    }
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
    });

    $scope.Addclonefamily = function (e, id) {

        var bool = false;
        var ids = "";
        if ($scope.selecetedCatalogId.toString() === "1")
            ids = $scope.Showmasteruncheckids;
        else
            ids = $scope.Showmasterids;

        var category_id;

        //if ($rootScope.selecetedParentCategoryId != "0" && $rootScope.selecetedParentCategoryId != undefined && $rootScope.selecetedParentCategoryId != null && $rootScope.selecetedCategoryIdClone!=undefined)
        //{
        //    category_id = $rootScope.selecetedParentCategoryId;
        //}
        //else
        //{
        //    category_id = $rootScope.selecetedCategoryIdClone;
        //}
        category_id = $rootScope.CloneCategory_id;

        dataFactory.Addclonefamily(ids, id.dataItem.CATALOG_ID, id.dataItem.CATEGORY_ID).success(function (response) {
            $scope.winGetClone.center().close();
            $scope.ProdCheckAssociate = "1";
            if ($rootScope.familyAction == "Delete") {
                dataFactory.FamilytrashFromNavigator($scope.checkeditems, $rootScope.selecetedCatalogId, 3, $scope.RightClickedCategoryId, $scope.deleteFamilyList, $scope.ProdCheckAssociate).success(function (response) {
                    if (response == "Product deleted successfully") {
                        response = $scope.familystatus;
                    }
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                    $scope.RefreshLeftNavTree();
                    $scope.RemoveFlag = '0';
                    $scope.RightClickedCategoryId = '';
                    $scope.associationRemove = "Association";
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
                dataFactory.Navigatorassociationremove($localStorage.getCatalogID, category_id, $scope.RightClickedCategoryId).success(function (response) {
                    if (response == "Update completed") {

                        var Res = "Product association removed successfully"

                        if ($rootScope.familyAction == "Delete") {
                            Res = "Product removed successfully"
                        }
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + Res + '.',
                            type: "info"
                        });
                        $scope.Showmasteruncheckids = '';
                        $scope.SelectedCatalogId = $localStorage.getCatalogID;
                        $scope.RefreshLeftNavTree();
                    }
                }).error(function (response) {
                });
            }
        }).error(function (error) {
            options.error(error);
        });
    }
    $rootScope.GetClonefamilyGridOptions = {
        dataSource: $rootScope.GetClonefamilyDatasource,
        autoBind: true, selectable: true,
        //type: "json", 
        columns: [{ field: "CATALOG_ID", title: "CATALOG_ID", width: "180px" },
            { field: "CATEGORY_SHORT", title: "CATEGORY_ID", width: "180px" },
            { field: "CATEGORY_NAME", title: "CATEGORY_NAME", width: "180px" },
             {
                 command: [{ name: 'edit', text: "", width: "10px", template: "<a class=\'k-grid-edit k-item girdicons\' style=\'padding-right:10px\'><div title=\'Edit\' ng-click=\'Addclonefamily($event,this)\' class=\'btn btn-primary btn-xs\'>Assign</div></a>" },
                 ],
                 title: "Actions", headerTemplate: '<span title="Actions">ACTIONS</span>',
                 width: "80px",
             },
        ],
        editable: false
    };
    //----------------------TRASH CLONE FAMILY -----------------------//

    $rootScope.TrashClonefamilyDatasource = new kendo.data.DataSource({
        autoBind: true,
        transport: {
            read: function (option) {

                dataFactory.TrashCloneFamily($scope.checkeditems, $rootScope.selecetedCatalogId, $scope.RemoveFlag, $scope.RightClickedCategoryId).success(function (response) {

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
    });
    $rootScope.TrashClonefamilyGridoption = {
        datasource: $rootScope.GetClonefamilyDatasource,
        autoBind: true, Selectable: true,
        columns: [{ field: "CATALOG_ID", title: "CATALOG_ID", width: "180px" },
              { field: "CATEGORY_ID", title: "CATEGORY_ID", width: "180px" },
              { field: "CATEGORY_NAME", title: "CATEGORY_NAME", width: "180px" },
               {
                   command: [{ name: 'edit', text: "", width: "10px", template: "<a class=\'k-grid-edit k-item girdicons\' style=\'padding-right:10px\'><div title=\'Edit\' ng-click=\'AddTrashclonefamily($event,this)\' class=\'btn btn-primary btn-xs\'>Add</div></a>" },
                   ],
                   title: "Actions", headerTemplate: '<span title="Actions">ACTIONS</span>',
                   width: "80px",
               },
        ],
        editable: false
    };
    $scope.AddTrashclonefamily = function (e, id) {

        var bool = false;
        var ids = "";
        if ($scope.selecetedCatalogId.toString() === "1")
            ids = $scope.Showmasteruncheckids;
        else
            ids = $scope.Showmasterids;

        dataFactory.AddTrashclonefamily(ids, id.dataItem.CATALOG_ID, id.dataItem.CATEGORY_ID).success(function (response) {

        }).error(function (error) {
            options.error(error);
        });
    }
    $scope.init();

    $scope.indexValue = 0;
    //$scope.MultipleAttributeName = [];
    //$scope.MultipleAttributeNameDataSource = new kendo.data.HierarchicalDataSource({
    //    type: "json",
    //    loadOnDemand: false,
    //    transport: {
    //        read: function (options) {
    //            options.success($scope.MultipleAttributeName);
    //        }
    //    },
    //    schema: {
    //        model: {
    //            id: "id",
    //          hasChildren: "hasChildren"
    //        }
    //    },
    //});  

    $scope.GetAttribute = function (AttributeValues) {
        // get attributes using datafactory call.
        if (AttributeValues != null) {
            $scope.MultipleAttributeName = AttributeValues;
            //$localStorage.GetAllAttributesasSelected = AttributeValues;
            //$timeout(function () {
            //    $scope.MultipleAttributeNameDataSource.read();
            //}, 200);
        }

    };

    $scope.hidemenu = function () {
        $(".menuProduct").hide();
    }

    $rootScope.logOutClick = function (e) {
        //-----------------------------------Category Undo Flags------------------------//

        $localStorage.categoryChangedListUndo = [];
        $localStorage.baseCategoryValUndoList = [];

        var cookies = document.cookie.split(";");

        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
        //-----------------------------------Category Undo Flags------------------------//
    };


    //--------------------Undo catalog dropdown changes by Aswin kumar.M ------------------------------------///

    $scope.selectCatalog = function (e) {

        $scope.oldIndex = e.sender._oldIndex;
        $scope.allowChangeFlag = "0";
        if ($rootScope.selectedFamilyDetails == undefined) {
            $rootScope.selectedFamilyDetails = [];
        }

        if ($rootScope.categoryUndoList.length > 0 || $rootScope.selectedFamilyDetails.length > 0 || $rootScope.showSelectedFamilyProductDetails.length > 0) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Do you want to clear the Undo changes done by you?",
                type: "info",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {

                    if (result === "Yes") {
                        $scope.oldIndex = e.sender._oldIndex;
                        $scope.allowChangeFlag = "1";
                        $scope.catalogChange(e);
                    }

                    else {
                        var dropdownlist = $("#ddlKendocatalogDropDown").data("kendoDropDownList");
                        dropdownlist.select($scope.oldIndex);
                        $scope.allowChangeFlag = "1";
                    }
                }
            });
        }

        else {

            $scope.allowChangeFlag = "1";
        }


    }

    $scope.checkedNodeIds = function (nodes, checkedNodes) {
        $scope.uncheckednodes = "";
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].CATEGORY_ID);
            }
            else {
                $scope.uncheckednodes = nodes[i].CATEGORY_ID;
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
            if ($scope.uncheckednodes != "") {
                $scope.SelectedcategoryIdexp = "0";
            }
        }
    };
    //--------------------Undo catalog dropdown changes by Aswin kumar.M ------------------------------------///

    $rootScope.LoadProdItemData = function (catalogId, id, cat_id, pack_Id, Type, Attributes, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues) {

        var clickLoadFamilyID = []; // mariyaviji
        $scope.changeProdPage = "1";//JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE

        if (id.toString() == "") {
            id = $scope.Family.FAMILY_ID;
        }
        if (id.toString() == "") {
            id = $rootScope.FAMILY_ID;
        }

        if (id == 0) {
            id = $scope.Family.FAMILY_ID;
        }
        $rootScope.val = false;

        if (id == "") {
            id = 0;
        }
        if (id.toString().includes("~")) {
            if (id.startsWith("~")) {
                clickLoadFamilyID = id.split("~");
            }
            else {
                clickLoadFamilyID = id;
            }
        } else {
            clickLoadFamilyID = id;
        }
        if (clickLoadFamilyID.length == 2) {
            $rootScope.ClickFamilyIDValue = clickLoadFamilyID[1];
        }
        else {
            $rootScope.ClickFamilyIDValue = null;
        }
        $scope.sub_id = '0';
        $rootScope.sub_cat_id = cat_id;
        $rootScope.sub_fam_id = id;
        //dataFactory.getprodspecs(catalogId, id, $scope.displayID).success(function (response) {
        //document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaginghighlight";
        var ProdCount = [];
        dataFactory.getprodspecsWithoutAttributes(catalogId, id, true, cat_id).success(function (response) {
            if (response == null) {
                $scope.myDIV = false;
                $rootScope.noresultfound1 = true;
                $scope.noresultfound = true;
                $scope.newProductBtn2 = true;
                $scope.newProductBtn1 = false;
                $('#noresultfound1').show();
                $('#noresultfound1').css('display', 'block');
            }
            else 
            {
                if (response.Data.Columns.length) 
                {
                    $scope.myDIV = true;
                    $scope.newProductBtn1 = true;
                    $rootScope.noresultfound1 = false;
                    $scope.newProductBtn2 = false;
                   
                    var obj = jQuery.parseJSON(response.Data.Data);
                    var ProdEditPageloopcnt = Math.floor(obj.length / parseInt($scope.ProdCountPerPage));
                    if ((obj.length % parseInt($scope.ProdCountPerPage)) > 0) {
                        ProdEditPageloopcnt = ProdEditPageloopcnt + 1;
                    }
                    $scope.changeProdPage = "1";
                    for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                        ProdCount.push(i);
                    }
                    $scope.ProdPageCountitem = ProdCount;
                    if ($scope.ProdCurrentPageitem == "1") {
                        $scope.changeProdPage = "1";//JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                    } else {
                        $scope.changeProdPage = $scope.ProdCurrentPageitem;
                    }
                    //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                    // $scope.ProdCurrentPage = $scope.changeProdPage;
                    $scope.totalProdPageCount = ProdEditPageloopcnt;
                    $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageitem;
                    $('#cboProdPageCountdownitem').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageitem;
                }
                if (response.Data.Columns.length == 0)
                {
                    $('#noresultfound1').css('display', 'block');
                }
                else
                {
                    $('#noresultfound1').css('display', 'none');

                }
                if (Attributes != undefined) {
                    if (Attributes.includes('#')) {
                        Attributes = Attributes.replace('#', 'hashValue')  // have to change # values 
                    }
                }
                if ($rootScope.val == undefined) {
                    $rootScope.val = false;
                }
       
           
        dataFactory.getprodspecscategoryitem(catalogId, id, true, cat_id, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.sub_id, pack_Id, Type, Attributes, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues, $rootScope.val).success(function (response) {

            if (response == null) {
                $scope.myDIV = false;
                $rootScope.noresultfound1 = true;
                $scope.newProductBtn2 = true;
                $scope.newProductBtn1 = false;
                $('#noresultfound1').show();
                $('#noresultfound1').css('display', 'block');

            }
            else {


                var productDetails = jQuery.parseJSON(response.Data.Data);

                $scope.FamilyId = id;
                var obj = productDetails.Table;
                $scope.prodData = obj;
                if (productDetails.Table1 != undefined) {
                    $scope.TotalTableCount = productDetails.Table1[0].Column1;
                    if ($rootScope.TypeOfProduct = "searchProduct") {
                        var ProdCount = [];
                        var ProdEditPageloopcnt = Math.floor($scope.TotalTableCount / parseInt($scope.ProdCountPerPage));
                        if (($scope.TotalTableCountitem % parseInt($scope.ProdCountPerPage)) > 0) {
                            ProdEditPageloopcnt = ProdEditPageloopcnt + 1;
                        }
                        $scope.changeProdPage = "1";
                        for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                            ProdCount.push(i);
                        }

                        $scope.ProdPageCountitem = ProdCount;
                        if ($scope.ProdCurrentPageitem == "1") {
                            $scope.changeProdPage = "1";//JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                        } else {
                            $scope.changeProdPage = $scope.ProdCurrentPageitem;
                        }
                        if ($scope.ProdCurrentPage == "1") {
                            $scope.ProdCurrentPageitem = "1"
                        }
                        else {
                            $scope.ProdCurrentPageitem = $scope.ProdCurrentPage;
                        }
                        //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                        // $scope.ProdCurrentPage = $scope.changeProdPage;
                        $scope.totalProdPageCount = ProdEditPageloopcnt;
                        $scope.ProdCurrentPage = "1";
                        $scope.ProdCurrentPage1 = "1";
                        $scope.changeProdPage = "1";
                        $("#cboProdPageCountdownitem").data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageitem;

                    }


                }
                // To Hide All the product Grid Hide Function
                if ($scope.prodData.length == 0) {
                    $scope.$broadcast("ToHideAllTheProductGridFunctions", "some data");
                }
                if ($scope.prodData.length == 0 && $rootScope.deleteProduct == true && $scope.ProdCurrentPage != 1) {
                    $scope.ProdCurrentPage = $scope.ProdCurrentPage - 1;
                    $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageitem;
                    $('#cboProdPageCountdownitem').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageitem;
                    $scope.LoadProdData(catalogId, id, cat_id);
                }
                $('#cboProdPageCountup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageitem;
                $('#cboProdPageCountdownitem').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageitem;
                //For Product Edit

                //To Change Dynamice CatalogNo
                for (var i = 0; i < response.Data.Columns.length; i++) {
                    if (response.Data.Columns[i]["Caption"].includes("ITEM#")) {
                        response.Data.Columns[i]["Caption"] = response.Data.Columns[i]["Caption"].replace("ITEM#", $localStorage.CatalogItemNumber);
                    }
                }
                $scope.columnsForAtt = response.Data.Columns;
                if (productGridSearchValue == undefined) {
                    $scope.TotalTableCountitem = $scope.TotalTableCount;
                }
                else {
                    $scope.TotalTableCountitem = productDetails.Table.length;
                }
                // To push the values into arry for split only attributes in drop down list.
                if (Type != 'product') {
                    var AttributeValues = [];
                    for (var i = 0 ; $scope.columnsForAtt.length > i ; i++) {
                        if (
                            $scope.columnsForAtt[i].Caption == "CATALOG_ID" ||
                            $scope.columnsForAtt[i].Caption == "FAMILY_ID" ||
                            $scope.columnsForAtt[i].Caption == "PRODUCT_ID" ||
                            $scope.columnsForAtt[i].Caption == "SORT" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2WEB" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2PRINT" ||
                            $scope.columnsForAtt[i].Caption == "WORKFLOW STATUS" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2PDF" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2EXPORT" ||
                            $scope.columnsForAtt[i].Caption == "PUBLISH2PORTAL" ||
                            $scope.columnsForAtt[i].Caption == "SubProdCount"
                            ) {
                        } else {
                            if ($scope.columnsForAtt[i].Caption.includes("__OBJ")) {
                                AttributeValues.push({ "Value": i, "Name": $scope.columnsForAtt[i].Caption.slice(0, $scope.columnsForAtt[i].Caption.search("__OBJ")) });
                            } else {
                                AttributeValues.push({ "Value": i, "Name": $scope.columnsForAtt[i].Caption });
                            }
                        }
                    }
                    // To remove extra duplicate values (__)
                    for (var i = 0 ; AttributeValues.length > i ; i++) {
                        if (AttributeValues[i].Name.includes("__")) {
                            AttributeValues.splice(i, 1);
                        }
                    }
                    $scope.AttributeValues = AttributeValues;
                    $localStorage.GetAllAttributesasSelected = AttributeValues;
                    $scope.GetAttribute($scope.AttributeValues);
                }
                $("#divProductGrid").show();

                angular.forEach($scope.prodData, function (value) {
                    value.SORT = parseFloat(value.SORT);
                });
                //  $("#selectedattribute").show();
                //$("#divProductGrid").hide();
                //angular.forEach($scope.prodData, function (value) {
                //    value.SORT = parseFloat(value.SORT);
                //   //JOTHIPRIYA OCT 21 2021 PAGINATION ISSUE
                //    // $scope.changeProdPage = $scope.ProdCurrentPage;
                //    //start
                //    $scope.ProdCurrentPage == 1;
                //    if ($scope.ProdCurrentPage == "1") {
                //        $scope.changeProdPage = "1";//JOTHIPRIYA
                //    } else {
                //        $scope.changeProdPage = $scope.ProdCurrentPage;

                //    }
                //    //$rootScope.selectedfamily_id = $scope.Family.FAMILY_ID;
                //    //end
                //    //$scope.callProductGridPaging($scope.ProdCurrentPage);

                //});

                // $rootScope.visibleProjects = $scope.prodData;  $localStorage.CatalogItemNumber;
                // $rootScope.visibleProjects1 = $scope.prodData;
                // $data = $scope.prodData;
                $rootScope.Mainprodcount = $scope.prodData.length;
                if ($scope.prodData.length === 0) {
                    $scope.newProductBtnZero = true;
                    $scope.newProductBtn = false;
                    $scope.Itemcountid = false;
                    $scope.productpaging = false;
                    $scope.noresultfound = true;
                    $scope.dynamictable = true;
                    $scope.NoProduct = true;
                    $("#divProductGrid").hide();
                    $("#selectedattribute").hide();
                    $("#newProductBtn").css("display", "block");
                    $("#newProductBtn1").css("display", "none");
                }
                else {
                    $scope.noresultfound = false;
                    $scope.productpaging = true;
                    $scope.dynamictable = true;
                    $scope.Itemcountid = true;
                    $scope.newProductBtn1 = false;
                    $scope.newProductBtnZero = false;
                    $scope.newProductBtn = false;
                    $("#newProductBtn").css("display", "none");
                    $("#newProductBtn1").css("display", "block");
                }
                //$scope.tblDashBoard.reload();
                // $scope.tblFamilyGrid.reload();
                // $scope.tblFamilyGrid.$params.page = 1;
                if ($scope.userRoleDeleteFamily) {
                    // $('#user-toolbar-association-removal-family').removeAttr('disabled');
                    $('#user-toolbar-delete-family').removeAttr('disabled');
                }
                else {
                    // $('#user-toolbar-association-removal-family').attr('disabled', 'disabled');
                    $('#user-toolbar-delete-family').attr('disabled', 'disabled');
                }

                if ($scope.userRoleAddFamily) {
                    $('#user-toolbar-create-new-family').removeAttr('disabled');
                }
                else {
                    $('#user-toolbar-create-new-family').attr('disabled', 'disabled');
                }
                if ($scope.userRoleModifyFamily) {
                    $('#user-toolbar-save-family').removeAttr('disabled');
                    $('#user-toolbar-family-attribute-setup').removeAttr('disabled');
                }
                else {
                    $('#user-toolbar-save-family').attr('disabled', 'disabled');
                    $('#user-toolbar-family-attribute-setup').attr('disabled', 'disabled');
                }
                //  var toolbarProduct = $("#product-toolbar").data("kendoToolBar");
                if ($scope.userRoleAddProductTable) {
                    $('#newbtn').removeAttr('disabled');
                }
                else {
                    $('#newbtn').attr('disabled', 'disabled');
                }

                if ($rootScope.ProductCount < $rootScope.ProductSKUCount) {
                }
                else {
                    $scope.userRoleAddProductTable = false;
                }

                if ($rootScope.Productnew == true) {
                    $('#newbtn').attr('disabled', 'disabled');
                }
                else {
                    $('#newbtn').removeAttr('disabled');
                }
                if ($scope.userRoleModifyProductTable) {
                    $('#columnsetubbtn').removeAttr('disabled');
                    $('#user-toolbar-default-layout').removeAttr('disabled');
                }
                else {
                    $('#columnsetubbtn').attr('disabled', 'disabled');
                    $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                }
                //if ($scope.userRoleDeleteFamily) {
                //    toolbar.enable("#user-toolbar-association-removal-family", true);
                //    toolbar.enable("#user-toolbar-delete-family", true);
                //}
                //else {
                //    toolbar.enable("#user-toolbar-association-removal-family", false);
                //    toolbar.enable("#user-toolbar-delete-family", false);
                //}

                if ($scope.prodData.length > 0) {
                    // $('#productpaging').show();
                    // $('#productpaging1').show();
                    $('#user-toolbar-export').removeAttr('disabled');
                    $('#user-toolbar-table-designer').removeAttr('disabled');
                    $('#user-toolbar-set-table-layout').removeAttr('disabled');
                    $('#user-toolbar-delete-layout').removeAttr('disabled');
                    $('#user-toolbar-create-new-layout').removeAttr('disabled');
                    if ($scope.userRoleModifyProductTable) {
                        $('#user-toolbar-default-layout').removeAttr('disabled');
                    }
                    else {
                        $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                    }
                    if (!$rootScope.EditMode) {
                        $('#user-toolbar-export').attr('disabled', 'disabled');
                        $('#user-toolbar-table-designer').attr('disabled', 'disabled');
                    }
                }
                else {
                    $('#productpaging').hide();
                    $('#productpaging1').hide();
                    $('#user-toolbar-export').attr('disabled', 'disabled');
                    $('#user-toolbar-table-designer').attr('disabled', 'disabled');
                    $('#user-toolbar-set-table-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-delete-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-create-new-layout').attr('disabled', 'disabled');
                    $('#user-toolbar-default-layout').attr('disabled', 'disabled');
                }

                if ($rootScope.userprodcongig === true) {
                    $('#addtoprdconfig').removeAttr('disabled');
                } else {
                    $('#addtoprdconfig').attr('disabled', 'disabled');
                }
                if ($rootScope.userprodcongig === true) {
                    $('#prodconfig').removeAttr('disabled');
                } else {
                    $('#prodconfig').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#cutbtn').removeAttr('disabled');
                } else {
                    $('#cutbtn').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#copybtn').removeAttr('disabled');
                } else {
                    $('#copybtn').attr('disabled', 'disabled');
                }
                if ($rootScope.userCopypastedel === true) {
                    $('#pastebtn').removeAttr('disabled');
                } else {
                    $('#pastebtn').attr('disabled', 'disabled');
                }
                if ($rootScope.DisplayIdcolumns === true) {
                    $scope.DisplayIdcolumns == true;
                    $scope.userRoleViewSystemid = true;
                }
            }
                blockUI.stop();
                $scope.ViewByHideProducts();
                $timeout(function () {
                    $('#cboProdPageCountdownitem').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageitem;
                }, 1000);
                $("#accordions").accordion({ header: "> div > h3", collapsible: true, active: false });
                $('.ui-accordion-header').removeClass('ui-corner-all').addClass('ui-accordion-header-active ui-state-active ui-corner-top').attr({ 'aria-selected': 'true', 'tabindex': '0' });
                $('.ui-accordion-header .ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
                $('.ui-accordion-content').addClass('ui-accordion-content-active').attr({ 'aria-expanded': 'true', 'aria-hidden': 'false' }).show();

            }).error(function (error) {
                options.error(error);
            });
            

            blockUI.stop();
            }
        });

    };



    $rootScope.clickSelectedItem = function (dataItem, event) {
        $scope.ClickFamily = false;
        $scope.ShowLess = false;
        $scope.ShowMore = true;
        $scope.Family_IDs = "";
        $scope.Category_IDs = "";
        $rootScope.ProdPage = "1";
        $rootScope.FAMILY_ID = dataItem.CATEGORY_ID;
        $scope.ProdCurrentPageitem = "1";
        $rootScope.cat_id = dataItem.id;
        $scope.SelectedItem.Family_ID = "";
        $rootScope.selectedProductDetails = [];
        $localStorage.selectedListOnlyProductDetails = [];
        $rootScope.attributesetup = false;
        $rootScope.attributepack = false;
        $('#panel').hide();
        $('#panel1').hide();
        $(".panel-heading1").hide();
                    if (dataItem.spriteCssClass == "category") {
                        $scope.isCloneFamilyDelete = false;
                        $scope.isCloneFamilyDetach = false;
                        $scope.isCloneCategoryDetach = false;
                        
                        $('#tabstrip-1').show();
                    }
                    if (dataItem.spriteCssClass == "defaultfamily") {
                        $scope.isCloneFamilyDetach = false;
                        $scope.isCloneFamilyDelete = false;
                        $scope.isCloneCategoryDetach = false;
                       
                        $('#tabstrip-2').show();
                        $('#tabstrip-1').hide();

                    }
                    if (dataItem.spriteCssClass == "subfamilyClone") {
                        $scope.isCloneCategoryDetach = false;
                       

                        $scope.isCloneFamilyDetach = true;
                        $scope.isCloneFamilyDelete = true;
                        $('#tabstrip-1').hide();

                    }
                    if (dataItem.spriteCssClass == "familyClone") {
                        $scope.ID1 = dataItem.id;
                        $scope.Temp_ID1 = $scope.ID1.split("~");
                        $rootScope.Clone_Category_ID = $scope.Temp_ID1[0];
                        $scope.isCloneFamilyDetach = true;
                        $scope.isCloneFamilyDelete = true;
                        $scope.isCloneCategoryDetach = false;
                       
                        $('#tabstrip-2').show();
                        $('#tabstrip-1').hide();

                    }

                    if (dataItem.spriteCssClass == "CategoryClone") {
                        $scope.isCloneFamilyDetach = false;
                        $scope.isCloneFamilyDelete = false;
                        $scope.isCloneCategoryDetach = true;
                       
                        $('#tabstrip-1').show();
                        $('#tabstrip-2').hide();

                    }
                    if (dataItem.spriteCssClass == "category") {
                        $scope.isCloneFamilyDetach = false;
                        $scope.isCloneFamilyDelete = false;
                        $scope.isCloneCategoryDetach = false;
                        
                        $('#categoryAttributes').show();
                    }

                    $("#selectedattributeEdit").hide();
                    $scope.$broadcast("SendDown", "some data");
                    $scope.rootCategotyForPdfXpress = false;
                    $scope.$broadcast("ForResultEmpty");
                    $('#hideonfamily').hide();
                    $('#subproductsmaingrid').hide();
                    $('#subproductsgrid').hide();
                    $('#columnsetup').hide();
                    $('#dynamictable').hide();
                    $('#Familyimport').hide();

                    $rootScope.getCategoryPdfExpress = '';
                    $rootScope.getCategoryPdfExpressToolTip = 'No file chosen';

                    if (dataItem.spriteCssClass == "category") {
                       
                        $('#tabstrip-2').css('display', 'none');
                        $('#tabstrip-1').css('display', 'block');
                        var TYPE = "Category";
                        $scope.dynamictable = true;
                        $scope.selectedattributeEdit = false;
                        $("#selectedattributeEdit").addClass('ng-hide');
                        $scope.myDIV = true;
                        $scope.productpaging = true;
                        $('#productpaging').css('display', 'block');
                        $('#ProductsTab').css('display', 'none');
                        $('#mainProductsMenu').css('display', 'none');
                        $scope.isCloneFamilyDetach = false;
                        $scope.isCloneFamilyDelete = false;
                        // To check the root category or not
                        var Catalog_Id = dataItem.CATALOG_ID;
                        var Category_ID = dataItem.CATEGORY_ID
                        $rootScope.selectedcategory_id = dataItem.CATEGORY_ID;

                        dataFactory.getRootCategoryPdfXpress(Catalog_Id, Category_ID).success(function (response) {

                            if (response == '0') {
                                $scope.rootCategotyForPdfXpress = true;
                            } else {
                                $scope.rootCategotyForPdfXpress = false;
                            }

                        });

                        // End


                        $scope.GetDefaultPdfTemplate(dataItem, TYPE);

                        //------------------------------------------------------Undo flags--------------------------------------------------------//
                        $rootScope.categoryUndo = true;
                        $rootScope.familyUndo = false;
                        $rootScope.productUndo = false;
                        //------------------------------------------------------Undo flags--------------------------------------------------------//
                    }
                    else if (dataItem.spriteCssClass == "family") {
                        $scope.ID = dataItem.id;
                        $scope.Temp_ID = $scope.ID.split("~");
                        $rootScope.Category_ID = $scope.Temp_ID[0];
                        $('#tabstrip-2').css('display', 'block');
                        $('#selectedattributeitem').css('display', 'none');
                        if ($(".flyout").hasClass("expand")) {
                            $(".flyout-btn").toggleClass("btn-rotate");
                            $(".flyout").removeClass("expand");
                            $(".flyout").addClass("fade flyout-init");
                        }
                        $scope.isCloneFamilyDetach = false;
                        $scope.isCloneFamilyDelete = false;

                        $scope.newProductBtn = true;
                        $scope.dynamictable = true;
                        $scope.divProductGrid = true;
                        $scope.myDIV = true;
                        $scope.speccontainer = false;
                        $scope.familiescontainer = false;
                        $scope.clonecontainer = false;
                        $scope.tablescontainer = false;
                        $scope.groupscontainer = false;
                        
                        if ($scope.selectedattributeEdit == true)
                        {
                            $scope.selectedattributeEdit = false;
                            $("#selectedattributeEdit").addClass('ng-hide');

                        }
                        else
                        {
                            $scope.selectedattributeEdit = true;
                            $("#selectedattributeEdit").removeClass('ng-hide');
                         
                        }
                        
                        $('#ProductsTab').css('display', 'block');
                        $("#columnsetup").hide();
                        $(".column_setup").hide();
                        var TYPE = "Family";

                        $("#filterAttributePack").show();
                        $scope.GetDefaultPdfTemplate(dataItem, TYPE);
                        $scope.getseelctedFamilyName = dataItem.CATEGORY_NAME.split('(');
                        $rootScope.getClickedFamilyName = $scope.getseelctedFamilyName[0].trim();
                        //------------------------------------------------------Undo flags--------------------------------------------------------//
                        $rootScope.categoryUndo = false;
                        $rootScope.familyUndo = true;
                        $rootScope.productUndo = false;
                        //------------------------------------------------------Undo flags--------------------------------------------------------//


                        //------------------------------------------------------Undo product flags--------------------------------------------------------//
                        $rootScope.categoryUndo = false;
                        $rootScope.familyUndo = false;
                        $rootScope.productUndo = true;
                        //------------------------------------------------------Undo product flags--------------------------------------------------------//
                    }


                    $("#opendesignerFAM").hide();
                    $('#opendesigner').hide();

                    $rootScope.familyCreated = true;
                    $('#user-toolbar-trash-family').removeAttr('disabled');


                    //changes
                    $rootScope.navigateprodProductTab = true;


                    if ($rootScope.selectedProductDetails.length > 0) {
                        $rootScope.selectedProductDetails = $localStorage.selectedListOnlyProductDetails;
                    }

                    if ($localStorage.selectedListOnlyProductDetails.length > 0) {
                        $scope.getClickedItemFamilyItem = dataItem.CATEGORY_ID.split('~');
                        $rootScope.showSelectedFamilyProductDetails = [];
                        for (var j = 0; j < $localStorage.selectedListOnlyProductDetails.length; j++) {
                            if ($localStorage.selectedListOnlyProductDetails[j].FAMILY_ID == $scope.getClickedItemFamilyItem[1]) {
                                $rootScope.showSelectedFamilyProductDetails.push($localStorage.selectedListOnlyProductDetails[j]);
                            }
                        }
                    }

                    var countList = 0;
                    $rootScope.Productnew = false;
                    $rootScope.selectedCloneDetails = dataItem.id;
                    if (dataItem.spriteCssClass == "family") {

                        $rootScope.currentFamilyId = dataItem.CATEGORY_ID.replace("~", "");
                    } else if (dataItem.spriteCssClass == "subfamily") {
                        $rootScope.currentFamilyId = dataItem.CATEGORY_ID.replace("~", "");
                    } else if (dataItem.spriteCssClass == "defaultfamily") {
                        $rootScope.currentFamilyId = dataItem.CATEGORY_ID.replace("~", "");
                    }


                    if ($rootScope.SelectedNodeID != 0) {
                        var listSelected = $rootScope.SelectedNodeID.split('~');
                        var listSelecteddata = dataItem.id.split('~');

                        $rootScope.CloneCategory_id = listSelecteddata[0];
                        angular.forEach(listSelected, function (value) {
                            angular.forEach(listSelecteddata, function (value1) {
                                if (value == value1) {
                                    countList = countList + 1;
                                }
                            });
                        });

                        if (countList == 0) {
                            $rootScope.SelectedNodeID = dataItem.id;
                        }
                    }
                    else {
                        $rootScope.SelectedNodeID = dataItem.id;
                    }
                    if (dataItem.spriteCssClass == "family" && $rootScope.pasteProductNav != true) {
                        $rootScope.SelectedNodeID = dataItem.id;
                    }
                    if ($rootScope.pasteProductNav == true) {

                        if ($rootScope.SelectedNodeID.includes('~')) {
                            var splitValue = $rootScope.SelectedNodeID.split('~');
                            if ($rootScope.SelectedNodeID.split('~')[1] != "" && $rootScope.SelectedNodeID.split('~')[1] != "!" && !$rootScope.SelectedNodeID.split('~')[1].includes('CAT')) {
                                $rootScope.SelectedNodeID = $rootScope.SelectedNodeID.replace("~" + $rootScope.SelectedNodeID.split('~')[1], "");
                            }
                            $rootScope.SelectedNodeID = $rootScope.SelectedNodeID + "~" + dataItem.id;
                        }
                    }

                    $rootScope.Dashboards = false;
                    if (event === undefined) {
                        $scope.getCurrentUrl = $location.$$absUrl.split("App")[1];
                        if ($scope.getCurrentUrl == '/AdminDashboard') {
                            $rootScope.familyMainEditor = true;
                            $rootScope.invertedproductsshow = false;
                            $rootScope.invertedproductsbutton = false;
                            $rootScope.invertedproductsbutton1 = false;
                        } else if ($scope.getCurrentUrl == '/Inverted') {
                            $rootScope.familyMainEditor = false;
                            $rootScope.invertedproductsshow = true;
                            $rootScope.invertedproductsbutton = false;
                            $rootScope.invertedproductsbutton1 = false;
                        }
                    } else {
                        $rootScope.familyMainEditor = true;
                        $rootScope.invertedproductsshow = false;
                        $rootScope.invertedproductsbutton = false;
                        $rootScope.invertedproductsbutton1 = false;
                    }
                    if (dataItem.id.match("~")) {
                        $rootScope.familycategory = dataItem.id.split("~")[0];
                        $rootScope.WORKINGCATALOGID = dataItem.CATALOG_ID;
                        $scope.$broadcast("oldCatids");
                    } else {
                        $rootScope.familycategory = dataItem.id;
                    }
                    $scope.ProdCurrentPage = "1";

                    if (event !== undefined && event !== null) {
                        $scope.Coordinates.pageX = event.pageX;
                        $scope.Coordinates.pageY = event.pageY;
                    }
                    $("#custom-menu1").hide();
                    $("#custom-menuFam").hide();
                    $scope.SelectedDataItem.VERSION = dataItem.VERSION;
                    $scope.SelectedDataItem.DESCRIPTION = dataItem.DESCRIPTION;
                    $scope.SelectedDataItem.CATALOG_NAME = dataItem.CATALOG_NAME;
                    $rootScope.selecetedCatalogId = dataItem.CATALOG_ID;
                    $scope.SelectedDataItem.CATALOG_ID = dataItem.CATALOG_ID;
                    $scope.SelectedDataItem.CATEGORY_ID = dataItem.CATEGORY_ID;
                    $scope.SelectedDataItem.ID = dataItem.CATEGORY_ID;
                    $scope.SelectedDataItem.SORT_ORDER = dataItem.SORT_ORDER;
                    $scope.SelectedItem = dataItem.CATEGORY_ID;

                    $localStorage.CategoryID = dataItem.id;
                    $("#dynamictable").show();
                    $("#productpreview").hide();
                    $("#subproductpreview").hide();
                    $("#categoryGridView").show();

                    if ($scope.SelectedItem.match("~")) {

                        $rootScope.Family_ID_NEW = $scope.SelectedItem;
                        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);


                        console.log('0');
                        $scope.SelectedCategoryId = $scope.SelectedItem;
                        if (dataItem.parentNode().CATEGORY_ID.match("~")) {
                            console.log('1');
                            $scope.$broadcast("Active_Category_Id", dataItem.parentNode().parentNode().CATEGORY_ID);
                            $rootScope.selecetedCategoryId = dataItem.parentNode().parentNode().CATEGORY_ID;
                        } else {
                            console.log('2');
                            $scope.$broadcast("Active_Category_Id", dataItem.parentNode().CATEGORY_ID);
                            $rootScope.selecetedCategoryId = dataItem.parentNode().CATEGORY_ID;
                        }
                    } else {
                        console.log('3');
                        $rootScope.selecetedCategoryId = $scope.SelectedItem;
                        $scope.SelectedCategoryId = $scope.SelectedItem;
                        $scope.$broadcast("Active_Category_Id", $scope.SelectedCategoryId);
                        $rootScope.FamilyCATEGORY_ID = $scope.SelectedCategoryId;
                        if ($rootScope.defaultSelection == true) {
                            console.log('33');
                            $rootScope.SelectedNodeID = "";
                        }
                    }


                    console.log('4');
                    if ($scope.SelectedCategoryId.match("~")) {
                        console.log('5');

                        var tabstrip = $("#tabstrip").data("kendoTabStrip");
                        var myTab = tabstrip.tabGroup.children("li").eq(1);
                        tabstrip.enable(myTab);


                        var myTabscategory = tabstrip.tabGroup.children("li").eq(0);
                        tabstrip.enable(myTabscategory);

                        var myTabsproducts1 = tabstrip.tabGroup.children("li").eq(2);
                        tabstrip.enable(myTabsproducts1);
                        tabstrip.select(myTabsproducts1);
                        if (dataItem.spriteCssClass == "family") {
                            $scope.isCloneFamilyDelete = false;
                            $scope.isCloneFamilyDetach = false;
                            $("#tabstrip-1").hide();
                            $("#tabstrip-2").show();
                        }
                        if (dataItem.spriteCssClass == "subfamily") {
                            $scope.isCloneFamilyDelete = false;
                            $scope.isCloneFamilyDetach = false;
                            $("#tabstrip-1").hide();
                            $("#tabstrip-2").show();
                        }
                        $rootScope.enableTabNavigation = true;

                        var tabstripProd = $("#productgridtabstrip").data("kendoTabStrip");
                        var myTabProd = tabstripProd.tabGroup.children("li").eq(0);
                        tabstripProd.enable(myTabProd);
                        tabstripProd.select(myTabProd);

                        $("#custom-menu1").hide();
                        $("#custom-menuFam").hide();
                        $(".slideout-menu").removeClass("open");
                        $(".slideout-menu").css('left', "-280px");
                        $rootScope.selecetedFamilyId = $scope.SelectedCategoryId;
                        console.log('6');

                        $scope.$broadcast("Active_Family_Id", $scope.SelectedCategoryId, dataItem.parentNode());
                        console.log('7');
                        $scope.$broadcast("Active_Category_Selection", $scope.SelectedDataItem);
                        console.log('8');

                    } else {
                        var tabstrip = $("#tabstrip").data("kendoTabStrip");

                        var CategoryTab = tabstrip.tabGroup.children("li").eq(0);
                        tabstrip.select(CategoryTab);
                        tabstrip.enable(CategoryTab, true);

                        var FamilyTab = tabstrip.tabGroup.children("li").eq(1);
                        tabstrip.enable(FamilyTab, false);

                        var ProductTab = tabstrip.tabGroup.children("li").eq(2);
                        tabstrip.enable(ProductTab, false);

                        $rootScope.enableTabNavigation = false;
                    }
                    console.log('10');
                    $('#user-toolbar-preview').removeAttr('disabled');
                    if ($scope.userRoleDeleteFamily) {
                        console.log('11');
                        $('#user-toolbar-delete-family').removeAttr('disabled');
                    }
                    else {
                        console.log('12');
                        $('#user-toolbar-delete-family').attr('disabled', 'disabled');
                    }
                    $("#ItemGrid").removeClass('ng-hide');
                    
        blockUI.stop();

    };
    $scope.ViewByHideProducts = function () {


        var familyId = 0;

        if ($rootScope.selecetedFamilyId == 0)
            familyId = $rootScope.currentFamilyId;
        else
            familyId = $rootScope.selecetedFamilyId



        dataFactory.viewByHideProducts($rootScope.selecetedCatalogId, familyId, $localStorage.CategoryID).success(function (response) {

            var viewByChangesButton = response.split("~");
            if (viewByChangesButton[0] == "true") {
                if (viewByChangesButton[1] == 0) {

                    $scope.SpecificationButton = true;
                    $scope.SpecificationButtonItem = true;

                    //mm22
                    var classVal = $("#tagBtn").attr("class");
                    $("#tagBtn").removeClass("active");
                    var classVal = $("#tagBtnItem").attr("class");
                    $("#tagBtnItem").removeClass("active");
                    ////mm22
                }
                else {
                    $("#tagBtnItem").addClass("active");
                    $("#tagBtn").addClass("active");
                    $scope.SpecificationButton = false;
                    $scope.SpecificationButtonItem = false;
                }

                if (viewByChangesButton[2] == 0) {

                    $scope.ImageButton = true;
                    $scope.ImageButtonItem = true;
                    //mm22
                    var classVal = $("#imageBtn").attr("class");
                    $("#imageBtn").removeClass("active");
                    var classVal = $("#imageBtnItem").attr("class");
                    $("#imageBtnItem").removeClass("active");
                    ////mm22
                }
                else {
                    $("#imageBtnItem").addClass("active");
                    $("#imageBtn").addClass("active");
                    $scope.ImageButton = false;
                    $scope.ImageButtonItem = false;

                }
                if (viewByChangesButton[3] == 0) {

                    $scope.PriceButton = true;
                    $scope.PriceButtonItem = true;
                    //mm22
                    var classVal = $("#moneyBtn").attr("class");
                    $("#moneyBtn").removeClass("active");
                    var classVal = $("#moneyBtnItem").attr("class");
                    $("#moneyBtnItem").removeClass("active");
                    ////mm22
                }
                else {
                    $("#moneyBtn").addClass("active");
                    $("#moneyBtnItem").addClass("active");
                    $scope.PriceButton = false;
                    $scope.PriceButtonItem = false;

                }
                if (viewByChangesButton[4] == 0) {

                    $scope.KeyButton = true;
                    $scope.KeyButtonItem = true;
                    //mm22
                    var classVal = $("#keyBtn").attr("class");
                    $("#keyBtn").removeClass("active");
                    var classVal = $("#keyBtnItem").attr("class");
                    $("#keyBtnItem").removeClass("active");
                    ////mm22
                }
                else {
                    $("#keyBtn").addClass("active");
                    $("#keyBtnItem").addClass("active");
                    $scope.KeyButton = false;
                    $scope.KeyButtonItem = false;

                }
            }

        });
    };

    $scope.openspec = function () {
        debugger;
        $scope.speccontainer = true;
        $scope.familiescontainer = false;
        $scope.clonecontainer = false;
        $scope.tablescontainer = false;
        $scope.groupscontainer = false;
    }

    $scope.openproducts = function () {
        debugger;
        $scope.speccontainer = false;
        $scope.familiescontainer = true;
        $scope.clonecontainer = false;
        $scope.tablescontainer = false;
        $scope.groupscontainer = false;
    }

    $scope.openclone = function () {
        debugger;
        $scope.speccontainer = false;
        $scope.familiescontainer = false;
        $scope.clonecontainer = true;
        $scope.tablescontainer = false;
        $scope.groupscontainer = false;
    }
    $scope.opentable = function () {
        debugger;
        $scope.speccontainer = false;
        $scope.familiescontainer = false;
        $scope.clonecontainer = false;
        $scope.tablescontainer = true;
        $scope.groupscontainer = false;
    }
    $scope.opengroups = function () {
        debugger;
        $scope.speccontainer = false;
        $scope.familiescontainer = false;
        $scope.clonecontainer = false;
        $scope.tablescontainer = false;
        $scope.groupscontainer = true;
    }
    $scope.CloseQuickBall = function () {
        debugger;
        $scope.speccontainer = false;
        $scope.familiescontainer = false;
        $scope.clonecontainer = false;
        $scope.tablescontainer = false;
        $scope.groupscontainer = false;
    }

}]);


