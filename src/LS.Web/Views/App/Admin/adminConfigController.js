﻿LSApp.controller('adminConfigController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$localStorage', '$rootScope', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $localStorage, $rootScope) {

    

    

    $scope.emailvalid = /^[A-Za-z0-9._-]+[A-Za-z0-9._-]+@[A-Za-z0-9._-]+\.[A-Za-z.]{2,5}$/;
    $scope.pattern = "/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/";
    $scope.pno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    $scope.regExPostalCode = /^[a-zA-Z0-9]{5,6}$/; // /^[0-9]{5,6}(?:-[0-9]{4})?$/;
    $scope.selectedCustomer = {};
    $scope.searchingCustomer = '';
    $scope.selectedPlan = {};
    $scope.IsNewPlan = false;
    $scope.selectedPlanDataList = {
        FUNCTION_GROUP_ID: 0,
        FUNCTION_ID: 0,
        FUNCTION_NAME: '',
        DEFAULT_ACTION_ALLOW: true
    };
    $scope.selectedPlanData = [];
    
    $scope.GeneralSettings = {
        
        DisplaySkuProductCountInAlert: true,
        AssetLimitPercentage: 90,
        SKUAlertPercentage: 90,
    };

    $scope.PasteValidation = function (e) {

    };

    $scope.UsersSpecialValidation = function (e) {
        var regex = new RegExp("^[a-zA-Z0-9-]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (e.charCode == 32)
            return true;
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    };

    $scope.mindate = new Date();

    $scope.customersDatasource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                dataFactory.getCustomers(options.data).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        },
        schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "CustomerId",
                fields: {
                    CustomerId: { type: "int" },
                    CustomerName: { type: "string" },
                    PlanName: { type: "string" },
                    ContactTitle: { type: "string" },
                    ContactName: { type: "string" },
                    CompanyName: { type: "string" },
                    Country: { type: "string" },
                    ActivationDate: { type: "date" },
                    IsActive: { type: "boolean" },
                    //    IsApproved: { type: "bool" }
                }
            }
        },
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        pageSize: 10,
        sort: {
            field: "CustomerName",
            dir: "asc"
        },
        error: function (e) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + e.xhr.responseText + '.',
                type: "error"
            });
        }
    });

    $scope.customersGridOptions = {
        autoBind: false,
        dataSource: $scope.customersDatasource,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 },

        columns: [{ field: "CustomerId", title: "Customer Id", hidden: true },
        { field: "CustomerName", title: "Customer Name", width: "100px" },
        //{ field: "ContactTitle", title: "Contact Title", width: "100px" },
        { field: "ContactName", title: "Contact Name", width: "100px" },
        { field: "CompanyName", title: "Company Name", width: "100px" },
        { field: "Plan.PLAN_NAME", title: "Plan Name", width: "100px" },
        { field: "Country", title: "Country", width: "100px" },
        { field: "ActivationDate", title: "Activation Date", width: "100px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
        { field: "IsActive", title: "IsActive", width: "70px", template: "<input type='checkbox' title='Active' kendo-tooltip  #= IsActive ? 'checked=checked' : ''# disabled='disabled' class='checkbox'/>" },
        { title: "Edit", template: "<a title='Edit' href='javascript:void(0);' ng-click='editCustomer(this)' class='glyphicon glyphicon-edit'></a>", width: "60px" },
        { title: "Delete", template: "<a title='Delete' href='javascript:void(0);' ng-click='deleteCustomer(this)' class='glyphicon glyphicon-remove'></a>", width: "60px" }]
    };

    $scope.planManagementDatasource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                dataFactory.getPlans(options.data).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        },
        schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "PLAN_ID",
                fields: {
                    PLAN_ID: { type: "number" },
                    PLAN_NAME: { type: "string" },
                    SKU_COUNT: { type: "number" },
                    NO_OF_USERS: { type: "number" },
                    SUBSCRIPTION_IN_MONTHS: { type: "number" },
                    IS_ACTIVE: { type: "boolean" },
                    Price_In_Dollars: { type: "number" },
                    StorageGB: { type: "number" },
                    CustomPDFCatalogTemplates: { type: "number" }
                }
            }
        },
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        pageSize: 5,
        error: function (e) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + e.xhr.responseText + '.',
                type: "error"
            });
        }
    });

    $scope.planGridOptions = {
        autoBind: false,
        dataSource: $scope.planManagementDatasource,
        sortable: true,
        filterable: true,
        scrollable: true,
        pageable: { buttonCount: 5 },
        columns: [{ field: "PLAN_ID", title: "Plan Id", width: "100px" },
        { field: "PLAN_NAME", title: "Plan Name", width: "130px" },
        { field: "SKU_COUNT", title: "Sku Count", width: "120px" },
        { field: "NO_OF_USERS", title: "No Of Users", width: "150px" },
        { field: "SUBSCRIPTION_IN_MONTHS", title: "Subscription In Months", width: "250px" },
        { field: "Price_In_Dollars", title: "Price In Dollars", width: "250px" },
        { field: "StorageGB", title: "Storage", width: "100px" },
        { field: "CustomPDFCatalogTemplates", title: "Custom PDF Catalog Templates", width: "300px" },
        { field: "IS_ACTIVE", title: "IsActive", width: "100px", template: "<input type='checkbox' title='Active' kendo-tooltip  #= IS_ACTIVE ? 'checked=checked' : ''# disabled='disabled' class='checkbox'/>" },
        { title: "Edit", template: "<a title='Edit' href='javascript:void(0);' ng-click='editPlan(this)' class='glyphicon glyphicon-edit'></a>", width: "100px" },
        { title: "Delete", template: "<a title='Delete' href='javascript:void(0);' ng-click='deletePlan(this)' confirm='Are you sure?' class='glyphicon glyphicon-remove'></a>", width: "100px" }
            //{ title: "", template: "<a title='Edit' kendo-tooltip  href='javascript:void(0);' ng-click='editPlan(this)' class='glyphicon glyphicon-edit'></a> &nbsp; &nbsp;<a title='Detail' kendo-tooltip  href='javascript:void(0);' ng-click='showPlanDetail(this)' class='glyphicon glyphicon-th-list'></a>", width: "60px" }
        ]
    };

    $scope.editCustomer = function (row) {
        $scope.formSubmitted = false;
        $scope.selectedCustomer.ActivationDate = '';
        $scope.selectedCustomer = angular.copy(row.dataItem);
        $scope.ActivationDateObject = '';
        //$scope.countryCode = "~" + $scope.selectedCustomer.Country;
        $scope.countryCode = $scope.selectedCustomer.COUNTRY_CODE;
        $scope.customercatalogGridDatasource.read();
        $scope.selectedCustomer.STATE_CODE = $scope.selectedCustomer.STATE_CODE;
        $scope.EditPlanId = $scope.selectedCustomer.PlanId;
        $scope.planMode = 1;
        $scope.planListDataSource.read();
        $scope.stateListDataSource.read();
        $("#customerListView").hide();
        $("#editCustomerView").show();
        $("#editTitle").html("Edit Customer");
        $("#editsub").html("Please update the customer information below.");
    };

    $scope.deleteCustomer = function (row) {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to delete the customer?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    $scope.selectedCustomer = angular.copy(row.dataItem);
                    dataFactory.deletecustomer($scope.selectedCustomer.CustomerId)
                        .success(function (data) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + data + '.',
                                type: "info"
                            });
                            $scope.customersDatasource.read();
                        })
                        .error(function (error) {
                            //console.log('Unable to delete the customer data: ' + error.message);
                        });
                };
            }
        });
    };

    $scope.deletePlan = function (row) {
        $scope.selectedPlan = angular.copy(row.dataItem);
        dataFactory.deletePlan($scope.selectedPlan.PLAN_ID)
            .success(function (data) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + data + '.',
                    type: "info"
                });
                $scope.planManagementDatasource.read();
            })
            .error(function (error) {
                //console.log('Unable to delete the Plan : ' + error.message);
            });
    };

    $scope.editPlan = function (row) {
        $scope.selectedPlan = angular.copy(row.dataItem);
        $scope.formSubmitted = false;
        $("#planListView").hide();
        $("#editPlanView").show();
        $("#formDataView").hide();
        $("#EditformDataView").show();
        $("#editPlanTitle").html("Edit Plan");
        $("#editPlansub").html("Please update the Plan information below.");
        $scope.IsNewPlan = false;
        $scope.dataSource.read();
        $scope.selectedPlanData = [];
    };

    $scope.addCustomer = function () {
        $scope.selectedCustomer.ActivationDate = '';
        $("#customerListView").hide();
        $("#editCustomerView").show();
        $("#editTitle").html("Add New Customer");
        $("#editsub").html("Please enter a new customer information below.");
        $scope.customercatalogGridDatasource.read();
    };

    $scope.addNewPlan = function () {
        $scope.selectedPlan.PLAN_ID = 0;
        $("#planListView").hide();
        $("#editPlanView").show();
        $("#EditformDataView").hide();
        $("#formDataView").show();
        $("#editPlanTitle").html("Add New Plan");
        $("#editPlansub").html("Please enter a new Plan information below.");
        $scope.IsNewPlan = true;
        $scope.functionsAllowedForPlanGridDatasource.read();
        $scope.selectedPlanData = [];
    };

    $scope.saveCustomer = function () {

        if ($scope.selectedCustomer.PlanId == "" || $scope.selectedCustomer.PlanId == null) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select a Plan.',
                // type: "success"
            });
            return;
        }
        if ($scope.selectedCustomer.COUNTRY_CODE == "" || $scope.selectedCustomer.COUNTRY_CODE == null) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select Country.',
                // type: "info"
            });
            return;
        }
        if ($scope.selectedCustomer.STATE_CODE == "" || $scope.selectedCustomer.STATE_CODE == null) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select State.',
            });
            return;
        }
        if ($scope.selectedCustomer.CustomerId == 0 && (($scope.selectedCustomer.Comments == "") || ($scope.selectedCustomer.Comments == undefined) || ($scope.selectedCustomer.Comments == null))) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter Image Folder name.',
                type: "info"
            });
            return;
        }
        else if ($scope.selectedCustomer.CustomerId == 0 && (($scope.selectedCustomer.Comments != "") || ($scope.selectedCustomer.Comments != undefined) || ($scope.selectedCustomer.Comments != null))) {
            dataFactory.customerValidation($scope.selectedCustomer.Comments, $scope.selectedCustomer.CustomerId).success(function (response) {
                if (response > 0) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Image folder name already exists, please select a new name.',
                        type: "error"
                    });
                    return;
                }
                else {
                    // Save Customer
                    if ($scope.selectedCustomer.PlanId == 48) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Saved successfully.',
                            type: "info"
                        });
                    }
                    
                    dataFactory.updateCustomer($scope.selectedCustomer).success(function (data) {
                        
                        $scope.selectedCustomer.CustomerId = data.CustomerId;
                        var userupdateRole = $scope.customercatalogGridoptions.dataSource.data();
                        $("customercatalogGridoptions").kendoGrid({
                            autoBind: false,
                            dataSource: $scope.customercatalogGridDatasource
                        });
                        $scope.customercatalogGridDatasource.read(); // "read()" will fire the "change" event of the dataSource and the widget will be bound
                        data = $scope.customercatalogGridoptions.dataSource.data();
                        dataFactory.updateCatalog($scope.selectedCustomer.CustomerId, $scope.customercatalogGridDatasource._data).success(function (datas) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Saved successfully.',
                                type: "info"
                            });
                            $scope.customersDatasource.read();
                            $scope.initCustomer();
                            $("#customerListView").show();
                            $("#editCustomerView").hide();
                        })
                            .error(function (error) {
                                // console.log('Unable to Save Customer Information: ' + error.message);
                            });
                    })
                        .error(function (error) {
                            //console.log('Unable to Save Customer Information: ' + error.message);
                        });
                }
            }).error(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter Image Folder name.',
                    type: "error"
                });
            });
            return;
        }

        else if ($scope.selectedCustomer.CustomerId > 0 && (($scope.selectedCustomer.Comments == "") || ($scope.selectedCustomer.Comments == undefined) || ($scope.selectedCustomer.Comments == null))) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter Image Folder name.',
                type: "error"
            });
            return;
        }
        else if ($scope.selectedCustomer.CustomerId > 0 && (($scope.selectedCustomer.Comments != "") || ($scope.selectedCustomer.Comments != undefined) || ($scope.selectedCustomer.Comments != null))) {

            dataFactory.customerValidation($scope.selectedCustomer.Comments, $scope.selectedCustomer.CustomerId).success(function (response) {
                if (response == 0) {
                    dataFactory.GetCutomerPlan($scope.selectedCustomer.CustomerId, $scope.selectedCustomer.PlanId).success(function (response) {
                        if (response == "Disabled") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Selected Plan is not available, please choose a different Plan.',
                                type: "error"
                            });
                            return;
                        }
                        else {
                            dataFactory.updateCustomer($scope.selectedCustomer).success(function (data) {

                                var userupdateRole = $scope.customercatalogGridoptions.dataSource.data();
                                $("customercatalogGridoptions").kendoGrid({
                                    autoBind: false,
                                    dataSource: $scope.customercatalogGridDatasource
                                });
                                $scope.customercatalogGridDatasource.read(); // "read()" will fire the "change" event of the dataSource and the widget will be bound
                                data = $scope.customercatalogGridoptions.dataSource.data();
                                dataFactory.updateCatalog($scope.selectedCustomer.CustomerId, $scope.customercatalogGridDatasource._data).success(function (datas) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'Selected Plan is not available, please choose a different Plan.',
                                        type: "error"
                                    });

                                    $scope.customersDatasource.read();
                                    $scope.initCustomer();
                                    $("#customerListView").show();
                                    $("#editCustomerView").hide();
                                })
                                    .error(function (error) {
                                        //console.log('Unable to Save Customer Information: ' + error.message);
                                    });
                            })
                                .error(function (error) {
                                    //   console.log('Unable to Save Customer Information: ' + error.message);
                                });
                        }
                    }).error(function (error) {
                        option.error(error);
                    });
                }
                else if (response == 1) {
                    dataFactory.GetCutomerPlan($scope.selectedCustomer.CustomerId, $scope.selectedCustomer.PlanId).success(function (response) {
                        if (response == "Disabled") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Selected Plan is not available, please choose a different Plan.',
                                type: "error"
                            });
                            return;
                        }
                        else {
                            
                            dataFactory.updateCustomer($scope.selectedCustomer).success(function (data) {
                                
                                var userupdateRole = $scope.customercatalogGridoptions.dataSource.data();
                                $("customercatalogGridoptions").kendoGrid({
                                    autoBind: false,
                                    dataSource: $scope.customercatalogGridDatasource
                                });
                                $scope.customercatalogGridDatasource.read(); // "read()" will fire the "change" event of the dataSource and the widget will be bound
                                data = $scope.customercatalogGridoptions.dataSource.data();
                                dataFactory.updateCatalog($scope.selectedCustomer.CustomerId, $scope.customercatalogGridDatasource._data).success(function (datas) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'Saved successfully.',
                                        type: "info"
                                    });
                                    $scope.customersDatasource.read();
                                    $scope.initCustomer();
                                    $("#customerListView").show();
                                    $("#editCustomerView").hide();
                                })
                                    .error(function (error) {
                                        console.log('Unable to save customer data: ' + error.message + '.');
                                    });
                            }).error(function (error) {
                                console.log('Unable to save customer data: ' + error.message + '.');
                            });
                        }
                    }).error(function (error) {
                        option.error(error);
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Image Folder Name already exists, please enter the different Image Folder Name.',
                        type: "error"
                    });
                    return;
                }

            }).error(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter Image Folder name.',
                    //type: "error"
                });

            });
            return;
        }
    };
    $scope.savePlan = function () {
        if ($scope.planForm.$valid == false) {
            $scope.formSubmitted = true;
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter the required values.',
                type: "info"
            });
            return;
        }

        if ($scope.selectedPlan.NO_OF_USERS <= 0 || $scope.selectedPlan.SUBSCRIPTION_IN_MONTHS <= 0 || $scope.selectedPlan.SKU_COUNT <= 0) {
            $scope.formSubmitted = true;
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Value must be greater than zero.',
                //type: "info"
            });
            return;
        }

        if ($scope.selectedPlan.NO_OF_USERS <= 1) {
            $scope.formSubmitted = true;
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'No. of users should be greater than one.',
                type: "error"
            });
            return;
        }

        $scope.selectedPlan.FunctionGroupModel = $("#grid").data("kendoGrid")._data;
        $scope.selectedPlan.FunctionModels = $scope.selectedPlanData;//  
        $scope.selectedPlanData = [];
        dataFactory.updatePlan($scope.selectedPlan)
            .success(function (data) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + data + '.',
                    type: "info"
                });
                $scope.planListDataSource.read();
                if (data !== "Plan already exists, please create a new Plan") {
                    $scope.planManagementDatasource.read();
                    $scope.initPlan();
                    $("#planListView").show();
                    $("#editPlanView").hide();
                }
            })
            .error(function (error) {
                //console.log('Unable to save Plan data: ' + error.message);
            });
    };

    $scope.cancelChanges = function () {
        $scope.initCustomer();
        $scope.selectedCustomer = {};
        $("#customerListView").show();
        $("#editCustomerView").hide();
    };

    $scope.cancelPlanChanges = function () {
        $scope.initPlan();
        $scope.selectedPlan = {};
        $("#planListView").show();
        $("#editPlanView").hide();
    };

    $scope.reset = function () {
        $scope.initCustomer();
        $scope.selectedCustomer = {};
        $scope.formSubmitted = false;
    };

    $scope.resetPlan = function () {
        $scope.initPlan();
        $scope.selectedPlan = {};
        $scope.formSubmitted = false;
    };

    $scope.searchCustomers = function () {
        if ($scope.searchingCustomer.length > 0) {
            $scope.customersDatasource.read();
        } else {
            if ($scope.searchingCustomer.length == 0) {
                $scope.customersDatasource.filter([]);
            }
        }
    };

    $scope.searchplan = function () {
        if ($scope.searchingPlan.length > 0) {
            $scope.planManagementDatasource.read();
        } else {
            if ($scope.searchingPlan.length == 0) {
                $scope.planManagementDatasource.filter([]);
            }
        }
    };
    //$scope.countryCode = "";
    $scope.initCustomer = function () {
        $scope.selectedCustomer = {
            CustomerId: 0,
            CustomerNumber: "",
            CustomerName: "",
            InActive: false,
            ContactName: "",
            Address1: "",
            Address2: "",
            City: "",
            StateProvince: "",
            //Country: "US",
            Country: "",
            Zip: "",
            EMail1: "",
            Phone: "",
            countryCode: "",
        };
    };

    $scope.initPlan = function () {
        $scope.planManagementDatasource.read();
        $scope.selectedPlan = {
            PLAN_ID: 0,
            PLAN_NAME: "",
            IS_ACTIVE: false,
            SKU_COUNT: 0,
            NO_OF_USERS: 0,
            Price_In_Dollars: 0,
            SUBSCRIPTION_IN_MONTHS: 0,
            StorageGB: 0,
            CustomPDFCatalogTemplates: 0
        };
    };

    $scope.savegeneralsettings = function () {
        
        dataFactory.saveGeneralSettings($scope.getCustomerIDs, $scope.GeneralSettings.DisplaySkuProductCountInAlert, $scope.GeneralSettings.SKUAlertPercentage, $scope.GeneralSettings.AssetLimitPercentage).success(function (response) {
            
            if (response === 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Saved successfully.',
                    type: "info"
                });
            }
        }).error(function (error) {
            options.error(error);
        });
    };


    $scope.init = function () {
        $("#planListView").show();
        $("#editPlanView").hide();
        $("#planListView").show();
        $("#editPlanView").hide();
        $scope.initPlan();
        $("#customerListView").show();
        $("#editCustomerView").hide();
        $scope.customersDatasource.read();
        $scope.initCustomer();
        $scope.formSubmitted = false;
        if ($location.$$path != "") {
            $location.$$path = "";
            $route.reload();
        }
        if ($localStorage.getCatalogID === undefined) {
            $scope.selecetedCatalogId = 0;
        } else {
            $scope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerIDs = 0;
        }
        else {
            $scope.getCustomerIDs = $localStorage.getCustomerID;
            dataFactory.GetPreferencesDetails($scope.getCustomerIDs).success(function (response) {
                
                $scope.GeneralSettings = response[0];
            });
        }
    };

    $scope.init();

    $scope.navigateTo = function (route) {
        $location.path(route);
        $scope.currentRoute = route;
    };

    $scope.isActiveTab = function (route) {
        return (route) === $location.path();
    };

    $scope.planMode = 0;
    $scope.planListDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getListofPlans($scope.planMode).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.countryListDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {

            read: function (options) {
                dataFactory.getListofCountry().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });


    $scope.stateListDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                if ($scope.countryCode !== "" && $scope.countryCode != undefined) {
                    dataFactory.getListofStates($scope.countryCode).success(function (response) {
                        var sp = $scope.selectedCustomer.STATE_CODE;
                        options.success(response);
                        $scope.selectedCustomer.STATE_CODE = sp;
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else if ($scope.selectedCustomer.Country != "" && $scope.selectedCustomer.Country != undefined) {
                    dataFactory.getListofStates($scope.selectedCustomer.Country).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    dataFactory.getListofStates().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        }
    });

    $scope.selectedState = function (e) {
        $scope.selectedCustomer.STATE_CODE = e.sender.value();
        $scope.selectedCustomer.StateProvince = e.sender.value();
    }

    $scope.countryChange = function (e) {
        $scope.countryCode = e.sender.value();
        $scope.stateListDataSource.read();
    };

    $scope.planChange = function (e) {
        $scope.PlanName = e.sender.text();
    };
    
    $scope.functionsAllowedForPlanGridDatasource = new kendo.data.DataSource({
        type: "json",
        batch: true,
        pageSize: 5,
        serverFiltering: true,
        transport: {
            read: function (options) {
                if ($scope.IsNewPlan) {
                    dataFactory.getNewPlanFunctionAllowedData().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                } else {
                    dataFactory.getPlanFunctionAllowedData($scope.selectedPlan.PLAN_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            update: function (options) {
                dataFactory.saveFunctionAllowedForPlan(options, $scope.selectedPlan).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
                return { models: kendo.stringify(options.models) };
            }
        },
        schema: {
            data: function (data) {
                return data;
            },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FUNCTION_ID",
                fields: {
                    FUNCTION_NAME: { type: "string", editable: false },
                    ACTION_VIEW: { type: "boolean" },
                    ACTION_MODIFY: { type: "boolean" },
                    ACTION_ADD: { type: "boolean" },
                    ACTION_REMOVE: { type: "boolean" }
                }
            }
        },
        error: function (e) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + e.xhr.responseText + '.',
                type: "info"
            });
        }
    });
    

    $("#functionsAllowedForPlanGrid").kendoGrid({
        dataSource: $scope.functionsAllowedForPlanGridDatasource,
        pageable: { buttonCount: 5 },
        autoBind: true,
        sortable: false,
        columns: [
            { field: "FUNCTION_NAME", title: "Active Features", width: "200px" },
            { field: "ACTION_VIEW", title: "ACTION VIEW", width: 130, template: '<input type="checkbox" id="view" class="chkbx5" #= ACTION_VIEW ?checked="checked" : "" # ng-click="updateRoleOnUI($event, this)"></input>' },
            { field: "ACTION_MODIFY", title: "ACTION MODIFY", width: 130, template: '<input type="checkbox" id="modify" class="chkbx6" #= ACTION_MODIFY ?checked="checked" : "" # ng-click="updateRoleOnUI($event, this)"></input>' },
            { field: "ACTION_ADD", title: "ACTION ADD", width: 130, template: '<input type="checkbox" id="add" class="chkbx7" #= ACTION_ADD ?checked="checked" : "" # ng-click="updateRoleOnUI($event, this)"></input>' },
            { field: "ACTION_REMOVE", title: "ACTION REMOVE", width: 130, template: '<input type="checkbox" id="remove" class="chkbx8" #= ACTION_REMOVE ?checked="checked" : "" # ng-click="updateRoleOnUI($event, this)"></input>' }
        ],
        editable: {
            mode: "inline",
            update: true
        }
    });

    $("#EditfunctionsAllowedForPlanGrid").kendoGrid({
        dataSource: $scope.functionsAllowedForPlanGridDatasource,
        pageable: { buttonCount: 5 },
        autoBind: true,
        sortable: false,
        columns: [
            { field: "FUNCTION_NAME", title: "Active Features", width: "200px" },
            { field: "ACTION_VIEW", title: "ACTION VIEW", width: 130, template: '<input type="checkbox" id="view" class="chkbx5" #= ACTION_VIEW ?checked="checked" : "" # ng-click="updateRoleOnUI($event, this)"></input>' },
            { field: "ACTION_MODIFY", title: "ACTION MODIFY", width: 130, template: '<input type="checkbox" id="modify" class="chkbx6" #= ACTION_MODIFY ?checked="checked" : "" # ng-click="updateRoleOnUI($event, this)"></input>' },
            { field: "ACTION_ADD", title: "ACTION ADD", width: 130, template: '<input type="checkbox" id="add" class="chkbx7" #= ACTION_ADD ?checked="checked" : "" # ng-click="updateRoleOnUI($event, this)"></input>' },
            { field: "ACTION_REMOVE", title: "ACTION REMOVE", width: 130, template: '<input type="checkbox" id="remove" class="chkbx8" #= ACTION_REMOVE ?checked="checked" : "" # ng-click="updateRoleOnUI($event, this)"></input>' }
        ],
        editable: {
            mode: "inline",
            update: true
        }
    });

    $scope.updateRoleOnUI = function (e, id) {
        
        if (e.toElement.id === "view") {
            id.dataItem.set("ACTION_VIEW", e.target.checked);
            if (e.target.checked === false) {
                id.dataItem.set("ACTION_MODIFY", e.target.checked);
                id.dataItem.set("ACTION_ADD", e.target.checked);
                id.dataItem.set("ACTION_REMOVE", e.target.checked);
            }
        }
        else if (e.toElement.id === "modify") {
            id.dataItem.set("ACTION_MODIFY", e.target.checked);
            if (e.target.checked === false) {
                id.dataItem.set("ACTION_ADD", e.target.checked);
            }
            else {
                id.dataItem.set("ACTION_VIEW", e.target.checked);
            }

        }
        else if (e.toElement.id === "add") {
            id.dataItem.set("ACTION_ADD", e.target.checked);
            if (e.target.checked === true) {
                id.dataItem.set("ACTION_MODIFY", e.target.checked);
                id.dataItem.set("ACTION_VIEW", e.target.checked);
            }
        }
        else {
            
            id.dataItem.set("ACTION_REMOVE", e.target.checked);
            if (e.target.checked === true) {
                id.dataItem.set("ACTION_VIEW", e.target.checked);
            }
        }
    };

    $("#functionsAllowedForPlanGrid .k-grid-content").on("change", "input.chkbx5", function (e) {
        var grid = $("#functionsAllowedForPlanGrid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set("ACTION_VIEW", this.checked);
        if (this.checked === false) {
            dataItem.set("ACTION_MODIFY", false);
            dataItem.set("ACTION_ADD", false);
            dataItem.set("ACTION_REMOVE", false);
        }
    });

    $("#functionsAllowedForPlanGrid .k-grid-content").on("change", "input.chkbx6", function (e) {
        var grid = $("#functionsAllowedForPlanGrid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set("ACTION_MODIFY", this.checked);
        if (this.checked === false) {
            dataItem.set("ACTION_ADD", false);
        }
        else {
            dataItem.set("ACTION_VIEW", true);
        }
    });

    $("#functionsAllowedForPlanGrid .k-grid-content").on("change", "input.chkbx7", function (e) {
        var grid = $("#functionsAllowedForPlanGrid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set("ACTION_ADD", this.checked);
        if (this.checked === true) {
            dataItem.set("ACTION_VIEW", true);
            dataItem.set("ACTION_MODIFY", true);
        }
    });

    $("#functionsAllowedForPlanGrid .k-grid-content").on("change", "input.chkbx8", function (e) {

        var grid = $("#functionsAllowedForPlanGrid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set("ACTION_REMOVE", this.checked);
        if (this.checked === true) {
            dataItem.set("ACTION_VIEW", true);
        }
    });

    $scope.customercatalogGridDatasource = new kendo.data.DataSource({
        type: "json",
        batch: true, editable: false,
        // pageSize: 5,
        serverFiltering: false,
        transport: {
            read: function (options) {
                if ($scope.selectedCustomer.CustomerId == undefined) {
                    $scope.selectedCustomer.CustomerId = 0;
                }
                dataFactory.getAllCatalogs($scope.selectedCustomer.CustomerId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
                return { models: kendo.stringify(options.models) };
            }
        },
        schema: {
            data: function (data) {
                return data;
            },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "CATALOG_ID",
                fields: {
                    CATALOG_ID: { editable: false },
                    CATALOG_NAME: { type: "string", editable: false },
                    ACTIVE: { type: "boolean" }
                }
            }
        },
        error: function (e) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + e.xhr.responseText + '.',
                type: "info"
            });
        }
    });

    $scope.customercatalogGridoptions = {
        autoBind: false,
        dataSource: $scope.customercatalogGridDatasource,
        batch: true,
        sortable: false, scrollable: true, editable: true, pageable: false, filterable: false,
        selectable: "row",
        columns: [
            { field: "CATALOG_ID", title: "CATALOG ID", width: "200px" },
            { field: "CATALOG_NAME", title: "CATALOG NAME", width: "200px" },
            { field: "ACTIVE", title: "Active", width: 130, template: '<input type="checkbox" class="chkbx6" #= ACTIVE ? \'checked="checked"\' : "" # ng-click="updateCatalogSelection($event, this)"></input>' }
        ]
    };

    $scope.updateCatalogSelection = function (e, id) {
        if (id.dataItem.ACTIVE != true) {
            id.dataItem.set("ACTIVE", e.target.checked);
        } else {
            id.dataItem.set("ACTIVE", false);
        }
    };

    $scope.dataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getPlanFunctionGroup($scope.selectedPlan.PLAN_ID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "FUNCTION_GROUP_ID",
                fields: {
                    FUNCTION_GROUP_ID: { editable: false, nullable: true },
                    FUNCTION_GROUP_NAME: { editable: false, validation: { required: true } },
                    SORT_ORDER: { type: "number", validation: { required: true, min: 1 } },
                    DEFAULT_ACTION_ALLOW: { editable: false, nullable: true }
                }
            }
        }
    });

    $scope.planMainGridOptions = {
        dataSource: $scope.dataSource,
        pageable: false,
        selectable: "row",
        columns: [
            { field: "FUNCTION_GROUP_NAME", title: "Group Name" },
            { field: "DEFAULT_ACTION_ALLOW", title: "Active", width: 130, template: '<input type="checkbox" id="allow" class="chkbx5" #= DEFAULT_ACTION_ALLOW ?checked="checked" : "" # ng-click="planGroupfunctionset($event, this)"></input>' }],

        editable: false, detailTemplate: kendo.template($("#plantemplate").html()),
        dataBound: function () {
            //    this.expandRow(this.tbody.find("tr.k-master-row").first());
            //  this.expandRow(this.tbody.find("tr.k-master-row"));
        }
    };
    $scope.EditplanMainGridOptions = {
        dataSource: $scope.dataSource,
        pageable: false,
        selectable: "row",
        columns: [
            { field: "FUNCTION_GROUP_NAME", title: "Group Name" },
            { field: "DEFAULT_ACTION_ALLOW", title: "Active", width: 130, template: '<input type="checkbox" id="allow" class="chkbx5" #= DEFAULT_ACTION_ALLOW ?checked="checked disabled" : "" # ng-click="planGroupfunctionset($event, this)"></input>' }],

        editable: false, detailTemplate: kendo.template($("#Editplantemplate").html()),
        dataBound: function () {
            //    this.expandRow(this.tbody.find("tr.k-master-row").first());
            //  this.expandRow(this.tbody.find("tr.k-master-row"));
        }
    };

    $scope.planDetailGridOptions = function (dataItem) {
        return {
            dataSource: {
                type: "json",
                transport: {
                    read: function (options) {
                        dataFactory.getPlanFunctionsByGroupId($scope.selectedPlan.PLAN_ID, dataItem.FUNCTION_GROUP_ID).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }, schema: {
                    model: {
                        id: "FUNCTION_GROUP_ID",
                        fields: {
                            FUNCTION_GROUP_ID: { editable: false, nullable: true },
                            FUNC_ID: { editable: false, nullable: true },
                            FUNCTION_NAME: { editable: false, nullable: true },
                        }
                    }
                },
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true
            },
            scrollable: false,
            sortable: true,
            selectable: "row",
            pageable: false,
            columns: [
                { field: "FUNCTION_NAME", title: "Active Features" },
                { field: "DEFAULT_ACTION_ALLOW", title: "Active", width: 120, template: '<input type="checkbox" id="allow" class="chkbx5" #= DEFAULT_ACTION_ALLOW ?checked="checked" : "" # ng-click="planfunctionset($event, this)"></input>' }],
            editable: false,
            dataBound: function () {
                if (this.dataSource.data().length === 0) {
                    var masterRow = this.element.closest("tr.k-detail-row").prev();
                    $("#grid").data("kendoGrid").collapseRow(masterRow);
                    masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                }
            }
        };
    };

    $scope.EditplanDetailGridOptions = function (dataItem) {
        return {
            dataSource: {
                type: "json",
                transport: {
                    read: function (options) {
                        dataFactory.getPlanFunctionsByGroupId($scope.selectedPlan.PLAN_ID, dataItem.FUNCTION_GROUP_ID).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }, schema: {
                    model: {
                        id: "FUNCTION_GROUP_ID",
                        fields: {
                            FUNCTION_GROUP_ID: { editable: false, nullable: true },
                            FUNC_ID: { editable: false, nullable: true },
                            FUNCTION_NAME: { editable: false, nullable: true },
                        }
                    }
                },
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true
            },
            scrollable: false,
            sortable: true,
            selectable: "row",
            pageable: false,
            columns: [
                { field: "FUNCTION_NAME", title: "Active Features" },
                { field: "DEFAULT_ACTION_ALLOW", title: "Active", width: 120, template: '<input type="checkbox" id="allow" class="chkbx5" #= DEFAULT_ACTION_ALLOW ?checked="checked disabled" : "" # ng-click="planfunctionset($event, this)"></input>' }],
            editable: false,
            dataBound: function () {
                if (this.dataSource.data().length === 0) {
                    var masterRow = this.element.closest("tr.k-detail-row").prev();
                    $("#grid").data("kendoGrid").collapseRow(masterRow);
                    masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                }
            }
        };
    };

    $scope.planfunctionset = function (e, id) {
        id.dataItem.set("DEFAULT_ACTION_ALLOW", e.target.checked);
        $scope.selectedPlanData.push({
            FUNCTION_GROUP_ID: id.dataItem.FUNCTION_GROUP_ID,
            FUNCTION_ID: id.dataItem.FUNC_ID,
            FUNCTION_NAME: id.dataItem.FUNCTION_NAME,
            DEFAULT_ACTION_ALLOW: e.target.checked
        });
    };
    $scope.planGroupfunctionset = function (e, id) {

        if ($scope.planForm.$valid == false) {
            $scope.formSubmitted = true;
            if (e.target.checked) {
                id.dataItem.set("DEFAULT_ACTION_ALLOW", false);
                e.target.checked = false;
            } else {
                id.dataItem.set("DEFAULT_ACTION_ALLOW", true);
                e.target.checked = true;
            }
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please fill the plan details in the correct format.',
                //type: "info"
            });
            return;
        }
        if ($scope.selectedPlan.NO_OF_USERS <= 0 || $scope.selectedPlan.SUBSCRIPTION_IN_MONTHS <= 0 || $scope.selectedPlan.SKU_COUNT <= 0) {
            $scope.formSubmitted = true;
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Value must be greater than zero.',
                type: "error"
            });
            return;
        }

        id.dataItem.set("DEFAULT_ACTION_ALLOW", e.target.checked);
        $scope.selectedPlanData.push({
            FUNCTION_GROUP_ID: id.dataItem.FUNCTION_GROUP_ID,
            FUNCTION_ID: 0,
            FUNCTION_NAME: id.dataItem.FUNCTION_NAME,
            DEFAULT_ACTION_ALLOW: e.target.checked
        });

        $scope.selectedPlan.FunctionGroupModel = $("#grid").data("kendoGrid")._data; // 
        $scope.selectedPlan.FunctionModels = $scope.selectedPlanData;//  
        $scope.selectedPlanData = [];
        dataFactory.updatePlan($scope.selectedPlan)
            .success(function (data) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + data + '.',
                    type: "info"
                });
                $scope.planMode = 0;
                $scope.planListDataSource.read();
                if (data !== "Plan already exists, please create a new Plan") {
                    $scope.dataSource.read();
                }
            })
            .error(function (error) {
                //console.log('Unable to save Plan data: ' + error.message);
            });
        $scope.selectedPlanData = [];

    };
}]);