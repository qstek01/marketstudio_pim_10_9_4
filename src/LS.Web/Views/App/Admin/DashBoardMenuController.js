﻿LSApp.controller('ModifiedRecentMenuController', ['$scope', function ($scope) {
    
    function modifytoday() {
        $scope.modifytoday();
        $("#modifytoday").addClass("btn btn-primary");
        $("#modifyyesterday").removeClass("btn btn-primary");
        $("#modifyAll").removeClass("btn btn-primary");
        $("#modifylastsevendays").removeClass("btn btn-primary");
    }

    function modifyyesterday() {
        $scope.modifyyesterday();
        $("#modifyyesterday").addClass("btn btn-primary");
        $("#modifytoday").removeClass("btn btn-primary");
        $("#modifyAll").removeClass("btn btn-primary");
        $("#modifylastsevendays").removeClass("btn btn-primary");
    }

    function modifyAll() {
        $scope.modifyAll();
        $("#modifyAll").addClass("btn btn-primary");
        $("#modifytoday").removeClass("btn btn-primary");
        $("#modifyyesterday").removeClass("btn btn-primary");
        $("#modifylastsevendays").removeClass("btn btn-primary");
    }

    function modifylastsevendays() {
        $scope.modifylastsevendays();
        $("#modifylastsevendays").addClass("btn btn-primary");
        $("#modifytoday").removeClass("btn btn-primary");
        $("#modifyAll").removeClass("btn btn-primary");
        $("#modifyyesterday").removeClass("btn btn-primary");
    }

    $scope.toolbarOptionsforrecent = {
        items: [
            {
                type: "button", id: "modifytoday", text: "Today", attributes: { "class": "red" }, click: modifytoday
            },
            {
                type: "button", id: "modifyyesterday", text: "Yesterday", attributes: { "class": "red" }, click: modifyyesterday
            },
             
              {
                  type: "button", id: "modifylastsevendays", text: "Last 7 days", attributes: { "class": "red" }, click: modifylastsevendays
              },{
        type: "button", id: "modifyAll", text: "All", attributes: { "class": "red" }, click: modifyAll
}
        ]
    };
}]);