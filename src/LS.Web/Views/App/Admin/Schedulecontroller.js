﻿LSApp.controller('Schedulecontroller', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', 'ngDialog', '$rootScope', '$localStorage',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, ngDialog, $rootScope, $localStorage) {
       

        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

        var events = new kendo.data.ObservableArray([]);
        
        var dateTime = new Date();
        var date = dateTime.toLocaleDateString();
        if ($location.$$absUrl.split('?')[1].split('&')[0].split('=')[1] != undefined)
       {
            date = $location.$$absUrl.split('?')[1].split('&')[0].split('=')[1]
       }
        $scope.ds = new kendo.data.SchedulerDataSource({
            data: events,
            schema: {
                model: {
                    id: "taskId",
                    fields: {
                        taskId: { from: "id", type: "number" },
                        title: { from: "title", defaultValue: "No title", validation: { required: true } },
                        start: { type: "date", from: "start" },
                        end: { type: "date", from: "end" },
                        description: { from: "description" },
                        recurrenceId: { from: "recurrenceID" },
                        recurrenceRule: { from: "recurrenceRule" },
                        recurrenceException: { from: "recurrenceException" },
                        isAllDay: { type: "boolean", from: "isAllDay" },
                    }
                }
            }
        });

        $scope.schedulerOptions = {
            date: new Date(date),
            views: [
              { type: "day",  workWeekStart: 1, workWeekEnd: 7 },
              { type: "week", selected: true,workWeekStart: 1, workWeekEnd: 7 }
            ],
            timezone: "Etc/UTC",
            eventTemplate: "<span class='custom-event'> -- </span>",

            allDayEventTemplate: "<div class='custom-all-day-event'> *</div>",
            dataSource: $scope.ds
        };

        function fill() {
            var response = [];

            var event;
            var timezone = $scope.schedulerOptions.timezone;

            for (var i = 0; i < response.length; i++) {
                event = response[i];
                event.start = convertDate(event.start, timezone);
                event.end = convertDate(event.end, timezone);

                events.push(new kendo.data.SchedulerEvent(event));
            }

        };

        function convertDate(date, timezone) {
            return kendo.timezone.convert(date, date.getTimezoneOffset(), timezone);
        }

        setTimeout(function () {
            fill();
        }, 1000);
   
    }]);

