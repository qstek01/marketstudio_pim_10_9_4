﻿LSApp.controller('ProductLayoutController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $rootScope, $localStorage) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    $scope.StructureType = {
        dataSource: {
            data: [
                { Text: "Horizontal", value: "Horizontal" },
                { Text: "Vertical", value: "Vertical" },
                { Text: "Super Table", value: "SuperTable" }
            ]
        },
        dataTextField: "Text",
        dataValueField: "value"
    };
    $scope.Structure = {
        structureName: "Sample",
        defaultlayout: true,
        tabletype: "Horizontal"

    };
    $scope.StructureTypeChange = function (e) {
        $scope.Structure.tabletype = e.sender.value();
    };
    $scope.CreateStructure = function () {
        $scope.winCreationnewlayout.close();
        dataFactory.CreateStructure($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.Structure.defaultlayout, $scope.Structure.structureName, $scope.Structure.tabletype).success(function (response) {
            if (response != null) {
                $scope.winTableDesigner.refresh({ url: "../Views/App/Partials/tableDesignerPopup.html" });
                $scope.winTableDesigner.center().open();
            }
        }).error(function (error) {
            options.error(error);
        });
    };
    $scope.CancelStructure = function () {
        $scope.winCreationnewlayout.close();
    };

    $scope.StructureNameDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                debugger
                if ($scope.Family.FAMILY_ID.contains('~')) {
                    var Family_ID = $scope.Family.FAMILY_ID.split('~');
                    $scope.Family.FAMILY_ID = Family_ID[1];
                }
                dataFactory.GetStructureNameDataSourceDetails($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.StructureName = "";
    $scope.StructureNameChange = function (e) {
        $scope.StructureName = e.sender.value();
    };
    $scope.DeleteStructure = function () {
        $scope.winRemovelayout.close();
        if ($scope.StructureName !== "") {
            dataFactory.DeleteStructure($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, $scope.StructureName).success(function (response) {
                if (response != null) {                 
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                }
            }).error(function (error) {
                options.error(error);
            });
        }
    };
    $scope.DelCancelStructure = function () {
        $scope.winRemovelayout.close();
    };
    
    $scope.OpenStructure = function () {
        $scope.winopenlayout.close();
        if ($scope.StructureName !== "") {
            $scope.winTableDesigner.refresh({ url: "../Views/App/Partials/tableDesignerPopup.html" });
            $scope.winTableDesigner.center().open();
        }
    };
    $scope.openCancelStructure = function () {
        $scope.winopenlayout.close();
    };
    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        } else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
    };
    $scope.init();
}]);