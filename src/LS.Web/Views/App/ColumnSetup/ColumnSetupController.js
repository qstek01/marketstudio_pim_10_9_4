﻿
LSApp.controller("ColumnSetupController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $rootScope, $localStorage) {

    $scope.myHTML = "<div><code>HTML</code>string with <a href=\"#\">links!</a> and other <em>stuff</em><br/> hi</div>";
    //alert($rootScope.selecetedCatalogId);
    //alert($rootScope.selecetedFamilyId);
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;


    function groupHeaderName(e) {

        if (e.value === 1) {
            return "Product Specifications (" + e.count + " items)";
        }
        else if (e.value === 3) {
            return "Product Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 4) {
            return "Product Price (" + e.count + " items)";
        }
        else if (e.value === 6) {
            return "Product Key (" + e.count + " items)";
        } else if (e.value === 7) {
            return "Family Description (" + e.count + " items)";
        }  else if (e.value === 9) {
            return "Family Image / Attachment (" + e.count + " items)";
        } else if (e.value === 11) {
            return "Family Specifications (" + e.count + " items)";
        }
        else if (e.value === 12) {
            return "Family Price (" + e.count + " items)";
        }
        else if (e.value === 13) {
            return "Family Key (" + e.count + " items)";
        } else {
            return "Product Specifications (" + e.count + " items)";
        }
    }


   
    $scope.GetAllCatalogattributesdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, pageable: true, pageSize: 12,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetAllCatalogattributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });

    $scope.mainGridOptions = {
        dataSource: $scope.GetAllCatalogattributesdataSource,
        pageable: { buttonCount: 5 },
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderName
            }]
    };

    $scope.updateSelection = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };
    $scope.prodfamilyattrdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, sort: { field: "SORT_ORDER", dir: "asc" },
        pageable: true,
        pageSize: 12,
        transport: {
            read: function (options) {
                
                dataFactory.GetprodfamilyAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId).success(function (response) {
                    
                    
                    for (var i = 0 ; response.length > i ; i++) {
                        if (response[i].ATTRIBUTE_NAME == "ITEM#") {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                        }
                        if (response[i].ATTRIBUTE_NAME == "SUBITEM#") {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                        }


                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: {
                        type: "boolean"
                    },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    SORT_ORDER: { editable: false }
                }
            }
        }

    });

    $scope.selectedGridOptions = {

        dataSource: $scope.prodfamilyattrdataSource,
        pageable: { buttonCount: 5 },
        selectable: "multiple",
        dataBound: onDataBound,
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }

        ]
    };

    var updownIndex = null;

    function onDataBound(e) {
        if (updownIndex != null) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {

                if (updownIndex == i) {
                    var isChecked = this.tbody.find("tr[data-uid='" + view[i].uid + "']").find(".chkbxforprodfamily").attr("checked");
                    if (isChecked) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .addClass("k-state-selected");
                    }

                }
            }

            updownIndex = null;
        }
    }

    $scope.updateSelection1 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };
    $scope.productNewAttributeSetupClick = function () {
        $scope.winProductNewAttributeSetup.refresh({ url: "../Views/App/Partials/productNewAttributeSetup.html" });
        $scope.winProductNewAttributeSetup.center().open();
        // $scope.tableGroupHeaderValue = true;
    };

    $scope.UnPublishAttributes = function () {
        dataFactory.UnPublishAttributes($scope.prodfamilyattrdataSource._data).success(function (response) {
            $scope.GetAllCatalogattributesdataSource.read();
            $scope.prodfamilyattrdataSource.read();
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId,$localStorage.CategoryID);
            $("#columnsetupproduct").hide();
            $("#dynamictable").show();
        }).error(function (error) {
            options.error(error);
        });
        $scope.winColumnSetuplayout.close();
    };

    $scope.RefreshProductAttributes = function () {
        debugger
        $scope.GetAllCatalogattributesdataSource.read();
        $scope.prodfamilyattrdataSource.read();
        $scope.ResetPage();
    };
    //$scope.PublishAttributes = function () {
    //    dataFactory.PublishAttributes($scope.prodfamilyattrdataSource._data).success(function (response) {
    //        $scope.GetAllCatalogattributesdataSource.read();
    //        $scope.prodfamilyattrdataSource.read();
    //    }).error(function (error) {
    //        options.error(error);
    //    });
    //};
    $scope.SavePublishAttributes = function () {
        dataFactory.SavePublishAttributes($localStorage.CategoryID, $scope.GetAllCatalogattributesdataSource._data).success(function (response) {
            $scope.GetAllCatalogattributesdataSource.read();
            $scope.prodfamilyattrdataSource.read();
        }).error(function (error) {
            options.error(error);
        });
    };
    $scope.DeletePublishAttributes = function () {
        var g = $scope.prodselectgrid.select();
        var data = [];
        for (var i = 0; i < g.length; i++) {
            var selectedItem = $scope.prodselectgrid.dataItem(g[i]);
            data.push(selectedItem);
        }
        dataFactory.DeletePublishAttributes(data).success(function (response) {
            $scope.GetAllCatalogattributesdataSource.read();
            $scope.prodfamilyattrdataSource.read();
        }).error(function (error) {
            options.error(error);
        });
    };

    //$scope.BtnMoveUpClick = function () {

    //    var g = $scope.prodselectgrid.select();
    //    if (g.length === 1) {
    //        g.each(function (index, row) {
    //            var selectedItem = $scope.prodselectgrid.dataItem(row);
    //            dataFactory.BtnMoveUpClick(selectedItem).success(function (response) {
    //                $scope.GetAllCatalogattributesdataSource.read();
    //                $scope.prodfamilyattrdataSource.read();
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        });
    //    } else {
    //        alert("Please Select Only one Attribute..");
    //    }
    //};


    //$scope.BtnMoveDownClick = function () {

    //    var g = $scope.prodselectgrid.select();
    //    if (g.length === 1) {
    //        g.each(function (index, row) {
    //            var selectedItem = $scope.prodselectgrid.dataItem(row);
    //            dataFactory.BtnMoveDownClick(selectedItem).success(function (response) {
    //                $scope.GetAllCatalogattributesdataSource.read();
    //                $scope.prodfamilyattrdataSource.read();
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        });
    //    } else {
    //        alert("Please Select Only one Attribute..");
    //    }
    //};

    $scope.BtnMoveUpClick = function () {

        var g = $scope.prodselectgrid.select();

        if (g.length === 1) {

            g.each(function (index, row) {
                var selectedItem = $scope.prodselectgrid.dataItem(row);
                var data = $scope.prodselectgrid.dataSource.data();
                var dataRows = $scope.prodselectgrid.items();
                var selectedRowIndex = dataRows.index(g);
                var sortOrder = data[selectedRowIndex - 1].SORT_ORDER;
                dataFactory.BtnMoveUpClick(sortOrder, selectedItem).success(function (response) {
                    $scope.GetAllCatalogattributesdataSource.read();
                    $scope.prodfamilyattrdataSource.read();
                    updownIndex = selectedRowIndex - 1;
                }).error(function (error) {
                    options.error(error);
                });
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: ' Please select at least one Attribute.',
                //type: "info"
            });
        }
    };


    $scope.BtnMoveDownClick = function () {

        var g = $scope.prodselectgrid.select();
        if (g.length === 1) {
            g.each(function (index, row) {
                var selectedItem = $scope.prodselectgrid.dataItem(row);
                var data = $scope.prodselectgrid.dataSource.data();
                var dataRows = $scope.prodselectgrid.items();
                var selectedRowIndex = dataRows.index(g);
                var sortOrder = data[selectedRowIndex + 1].SORT_ORDER;
                dataFactory.BtnMoveDownClick(sortOrder, selectedItem).success(function (response) {
                    $scope.GetAllCatalogattributesdataSource.read();
                    $scope.prodfamilyattrdataSource.read();
                    updownIndex = selectedRowIndex + 1;
                }).error(function (error) {
                    options.error(error);
                });
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                //type: "info"
            });
        }
    };

    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        } else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        //RefreshProductAttributes();
    };
    $scope.init();
}]);
