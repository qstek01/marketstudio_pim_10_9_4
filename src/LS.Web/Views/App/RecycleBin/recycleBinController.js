﻿LSApp.controller('recycleBinController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$rootScope', '$localStorage', '$http', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route,$rootScope, $localStorage, $http) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;


    $scope.Filter =
       {
           Option: 'All',
           UserId: $localStorage.getUserName,
           FromDate: kendo.toString(kendo.parseDate(new Date()), 'MM/dd/yyyy'),
           ToDate: kendo.toString(kendo.parseDate(new Date()), 'MM/dd/yyyy')
       };
    
    $scope.RecycleBin =
      {
          XML_ID: ''
      };

    $scope.format = 'dd-MMMM-yyyy';
    
    $scope.userList = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                //dataFactory.getUserList($scope.User.USER_ROLE).success(function (response) {
                $scope.filter=1;
                dataFactory.getUserList($scope.getCustomerID, $scope.filter).success(function (response) {
                    options.success(response);
                    $scope.Filter.UserId = $localStorage.getUserName;
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
   
    $scope.recycleBinDataSource = new kendo.data.DataSource({
        type: "json",
        transport: {
            read: function (options) {
                dataFactory.getRecycleBinDataSource($scope.Filter.Option, $scope.Filter.UserId, $scope.Filter.FromDate, $scope.Filter.ToDate).success(function (response) {
                    debugger
                    options.success(response);                    
                    //jQuery.parseJSON(response.Data.Data).Data
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    
     

    $scope.Attritems = new kendo.data.DataSource({
        data: [{ Text: "All", Value: "All" },
               { Text: "Attributes", Value: "Attributes" },
               { Text: "Products", Value: "Families" },
               { Text: "Items", Value: "Products" },
               { Text: "Categories", Value: "Categories" }
        ]
    });



    $scope.restoreSelectedItems = function () {

        var recycleBinDataSources = $scope.recycleBinDataSource._data;
        dataFactory.RestoreSelectedItemsdetails(recycleBinDataSources).success(function (response) {
            if (response == "0") {             
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'No Items selected, please select Items and continue.',
                    //type: "info"
                });
            }
            if (response == "SKU Exceed") {           
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
                    type: "error"
                });
            }
            if (response == "1") {            
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Items restore successful.',
                    type: "info"
                });

            }
            $scope.recycleBinDataSource.read();
        }).error(function (error) {
            options.error(error);
        });
    };
    $scope.restoreSelectedTrashItems = function () {

        var recycleBinDataSources = $scope.recycleBinDataSource._data;
        dataFactory.RestoreSelectedTrashItemsdetails(recycleBinDataSources).success(function (response) {
            if (response == "0") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'No Items selected, please select Items and continue.',
                    //type: "info"
                });
            }
            if (response == "SKU Exceed") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
                    type: "error"
                });
            }
            if (response == "1") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Items restore successful.',
                    type: "info"
                });

            }
            $scope.recycleBinDataSource.read();
        }).error(function (error) {
            options.error(error);
        });
    };
    $scope.permanentlyRemoveSelected = function (recycleBinDataSource) {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "This action will remove the selected files permanently from the recycle bin, do you want to continue?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                if (result === "Yes") {
                    recycleBinDataSource = $scope.recycleGridOptions.dataSource.data();
                    dataFactory.PermanentlyRemoveSelecteddetails(recycleBinDataSource).success(function (response) {
                        if (response == "0" || response == undefined || response == "" || response == null) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'No Items selected, please select Items and continue.',
                                //type: "info"
                            });
                        }
                        $scope.recycleBinDataSource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
    };
    $scope.emptyRecycleBin = function (recycleBinDataSource) {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure you want to permanently delete these items?", // move to Recycle Bin
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result1) {
                bool = false;
                if (result1 === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    recycleBinDataSource = $scope.recycleGridOptions.dataSource.data();
                    dataFactory.emptyRecycleBindetails(recycleBinDataSource).success(function (response) {
                        $scope.recycleBinDataSource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        
    };

    $scope.refreshRecycleBin = function () {
        $scope.recycleBinDataSource.read();
    };

    $scope.itemChange = function (e) {
        //$scope.selects(e.sender.value());
        
        $scope.Filter.Option = e.sender.value();
        if ($scope.Filter.Option == "--- Select Option ---") {
            $scope.Filter.Option = "All";
        }
    };
    $scope.userChange = function (e) {
        //$scope.selects(e.sender.value());
        $scope.Filter.UserId = e.sender.value();
        if ($scope.Filter.UserId == "--- Select User ---") {
            $scope.Filter.UserId = "All";
        }
    };

    

     
    
    $("#recycleBinGrid .k-grid-content").on("change", "input.chkbx1", function (e) {
        var grid = $("#recycleBinGrid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set("RESTORE", this.checked);
    });

    $scope.recycleGridOptions = {
        dataSource: $scope.recycleBinDataSource,
        autoBind: true,
        sortable: true,
        pageable: false,
        columns: [
            //{ field: "RESTORE", title: "RESTORE", width: 150, template: '<input type="checkbox" class="chkbx1" #= RESTORE ? "checked=unchecked" : "" # ng-click="updateSelection($event, this)"></input>' },
            { field: "RESTORE", title: "Select", sortable: false, template: '<input type="checkbox" class="chkbx1" #= RESTORE ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>", width: "50px"  },
         //  { field: "XML_ID", title: "XML_ID", width: "180px" },
          //  { field: "ROOT_CATEGORY_ID", title: "ROOT_CATEGORY_ID", width: "180px" },
            { field: "DELETED_USER", title: "Deleted User", width: "150px" },
           { field: "ACTION_DELETED_DATEADD", title: "Deleted Date", width: "150px" },
           //{ field: "CATEGORY_SHORT", title: "Category ID", width: "150px" },
           { field: "REMOVED_CATEGORY_NAME", title: "Category Name", width: "150px" },
           { field: "REMOVED_FAMILY", title: "Product Name", width: "150px" },
           { field: "REMOVED_SUB_FAMILY", title: "Sub Product Name", width: "150px" },
           { field: "REMOVED_PRODUCT", title: $localStorage.CatalogItemNumber, width: "150px" },
           { field: "ATTRIBUTE_ID", title: "Attribute ID", width: "150px" },
           { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "150px" }
           
      
        ],
        dataBound: function () {
            dataView = this.dataSource.view();
           
            for (var i = 0; i < dataView.length; i++) {
                if (dataView[i].uid != null) {
                    if (dataView[i].ATTRIBUTE_NAME != null && dataView[i].ATTRIBUTE_NAME != "") {
                        var uid = dataView[i].uid;
                        $("#recycleBinGrid tbody").find("tr[data-uid=" + uid + "]").css("background-color", "#ccff33 !important");//.addClass("rbtable");  //alarm's in my style and we call uid for each row
                    } else if (dataView[i].REMOVED_PRODUCT != null && dataView[i].REMOVED_PRODUCT != "") {
                        var uid = dataView[i].uid;
                        $("#recycleBinGrid tbody").find("tr[data-uid=" + uid + "]").css("background-color", "#fffff0 !important");//.addClass("rbtable");  //alarm's in my style and we call uid for each row
                    }
                    else if (dataView[i].REMOVED_SUB_FAMILY != null && dataView[i].REMOVED_SUB_FAMILY != "") {
                        var uid = dataView[i].uid;
                        $("#recycleBinGrid tbody").find("tr[data-uid=" + uid + "]").css("background-color", "#0099cc !important");//.addClass("rbtable");  //alarm's in my style and we call uid for each row
                    }
                    else if (dataView[i].REMOVED_FAMILY != null && dataView[i].REMOVED_FAMILY != "") {
                        var uid = dataView[i].uid;
                        $("#recycleBinGrid tbody").find("tr[data-uid=" + uid + "]").css("background-color", "#1975ff !important").css("color", "#fffff0");;//.addClass("rbtable");  //alarm's in my style and we call uid for each row
                    }
                    else if (dataView[i].REMOVED_CATEGORY_NAME != null && dataView[i].REMOVED_CATEGORY_NAME != "") {
                        var uid = dataView[i].uid;
                        $("#recycleBinGrid tbody").find("tr[data-uid=" + uid + "]").css("background-color", "#bd5c5c !important").css("color", "#fffff0");//.addClass("rbtable");  //alarm's in my style and we call uid for each row
                    }
                    else  {
                        var uid = dataView[i].uid;
                        $("#recycleBinGrid tbody").find("tr[data-uid=" + uid + "]").css("background-color", "#bd5c5c !important").css("color", "#FF9494");//.addClass("rbtable");  //alarm's in my style and we call uid for each row
                    }


                }
            }
        },
        editable: "inline"
    };

    $scope.updateSelection = function (e, id) {
        if (id.dataItem.RESTORE !== 1) {
            id.dataItem.set("RESTORE", e.target.checked);
        } else {
            id.dataItem.set("RESTORE", true);
        }
    };

    $scope.selectAll = function (ev) {
        var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
        var items = grid.dataItems();
        items.forEach(function (item) {
            if (item.RESTORE !== 1) {
                item.set("RESTORE", ev.target.checked);
            }
        });
    };

    $scope.init = function () {
      
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerID = 0;

        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
        }
        
    };
    $scope.init();
}]);