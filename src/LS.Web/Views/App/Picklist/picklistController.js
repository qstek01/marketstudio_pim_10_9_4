﻿
LSApp.controller("picklistController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$rootScope', '$http', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $rootScope, $http, $localStorage) {


    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;



    $scope.PickListDataType = {
        PICKLIST_DATA_TYPE: ''
    };

    $scope.NewPickListName = {
        PICKLIST_NAME: ''
    };

    $scope.PickListEntries = new kendo.data.ObservableArray([]);


    $scope.PickListGroup = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getpicklistall('').success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    //$http.get("../PickList/LoadPickList").
    //    then(function (details) {
    //        $scope.PickListGroup = details.data;
    //    });

    $scope.addPickListValue = function () {
        $scope.PickListEntries.push({ Value: $scope.PicklistValue });
        $scope.PicklistValue = '';
    };

    $scope.gridColumns = [
    { field: "Value", title: "Value" }
    //{ field: "track", title: "Track" }
    ];



    //$scope.saveChanges = function (datas) {
    //    alert("save" + $scope.PickListEntries[1].Value);

    //    $http.get("../PickList/GetValue?val=" + JSON.stringify($scope.PickListEntries, null, " ")).success(function (data) {

    //    });
    //};

    //$scope.handleChange = function (data, dataItem, columns) {
    //    alert("data : " + data);
    //    alert("dataItem : " + dataItem);
    //    alert("col : " + columns);
    //};

    //$scope.savePicklist = function () {
    //    $http.get("../PickList/SavePickList?name=" + $scope.SelectedPickList + "&&data=" + JSON.stringify($scope.PickListEntries, null, " "));
    //};

    $scope.addNewPicklist = function () {
        $scope.SelectedPickListDataType = $('#drpDownDataType').data("kendoDropDownList").value();
        if ($scope.NewPickListName.PICKLIST_NAME.trim() == "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a valid Name.',
                type: "error"
            });
        }
        else {
            if ($scope.SelectedPickListDataType !== "") {
                $http.get("../PickList/SaveNewPickList?Name=" + $scope.NewPickListName.PICKLIST_NAME.trim() + "&&Datatype=" + $scope.SelectedPickListDataType).
                    then(function (request) {
                        $scope.PickListGroup.read();
                        if (request.data != "") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Saved successfully.',
                                type: "info"
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Name already exists, please enter a different name for the Picklist.',
                                type: "error"
                            });
                        }

                        $scope.NewPickListName.PICKLIST_NAME = "";
                    });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select datatype.',
                    type: "error"
                });

            }
        }

    };
    $scope.PickListDefaultDatatype = [{
        id: 1,
        DATA_TYPE: "Text"
    },
    {
        id: 2,
        DATA_TYPE: "Numeric"
    },
    {
        id: 3,
        DATA_TYPE: "Hyperlink"
    },
    {
        id: 3,
        DATA_TYPE: "Date and Time"
    }];

    $scope.SelectedPickListDataType = "Text";
    $scope.SelectedPickList = "";

    $scope.DataTypeChanged = function (pkDatatype) {
        if (pkDatatype.sender._old.toLowerCase() === "date and time") {
            $scope.SelectedPickListDataType = "DATETIME";
        } else {
            $scope.SelectedPickListDataType = pkDatatype.sender._old;
        }
    };

    $scope.PicklistData = {
        PICKLIST_DATA: ''
    };

    $scope.userRoleModifyPickList = false;
    $scope.userRoleAddPickList = true;
    $scope.newPickList = false;
    $("#PickListNumberValues").hide();
    $("#PickListValues").hide();
    $("#PickListDateValues").hide();
    $scope.refreshPicklist = function () {
        $scope.userRoleModifyPickList = true;
        $scope.userRoleAddPickList = false;
        $scope.newPickList = true;
        $("#drpPickListName").data("kendoDropDownList").value('--- Select PickList ---');
    };
    $scope.picklistChange = function (pkName) {
        
      ///  $scope.GetPickListValuesDatasource._data = null;
        
        $scope.SelectedPickList = pkName.sender._old;
//        if( $scope.SelectedPickList=="")
//        {
//            $scope.SelectedPickList = null;
        //}
         $("#showImportdiv").hide();
        if ($scope.SelectedPickList !== "") {
            $scope.userRoleModifyPickList = true;
            $scope.userRoleAddPickList = false;
            $scope.newPickList = true;

            $http.get("../PickList/LoadSelectedPickList?name=" + $scope.SelectedPickList).
                then(function (details) {
                 //   for (var i = 0; i <= details.data.length; i++) {

                        if (details.data.PICKLIST_DATA_TYPE.toLowerCase() === "date and time") {
                            $("#drpDownDataType").data("kendoDropDownList").value("Date and Time");
                        } else {
                            $("#drpDownDataType").data("kendoDropDownList").value(details.data.PICKLIST_DATA_TYPE);
                        }

                        $scope.PickListDefaultDatatype.DATA_TYPE = details.data.PICKLIST_DATA_TYPE;
                        $scope.PickListDataType.PICKLIST_DATA_TYPE = details.data.PICKLIST_DATA_TYPE;
                        $scope.PicklistData.PICKLIST_DATA = details.data.PICKLIST_DATA;
                        $scope.selecetedPLXMLData = details.data.PICKLIST_DATA;
                        if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === "date and time") {

                            var grid = $("#PickListDateValuesgrid").data("kendoGrid");
                            grid.dataSource.page(1);
                            $scope.GetPickListDateValuesDatasource.read();
                            $("#PickListValues").hide();
                            $("#PickListDateValues").show();
                            $("#PickListNumberValues").hide();
                            //$scope.GetPickListDateValuesDatasource.read();
                        } else if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'numeric') {
                            var grid2 = $("#PickListNumberValuesgrid").data("kendoGrid");
                            grid2.dataSource.page(1);
                            $("#PickListValues").hide();
                            $("#PickListDateValues").hide();
                            $("#PickListNumberValues").show();
                            $scope.GetPickListNumericValuesDatasource.read();
                        } else {
                            var grid3 = $("#PickListValuesgrid").data("kendoGrid");
                            grid3.dataSource.page(1);
                            $("#PickListValues").show();
                            $("#PickListDateValues").hide();
                            $("#PickListNumberValues").hide();
                            $timeout(function () {
                                $scope.GetPickListValuesDatasource.read();
                            }, 1000);
                            



                        }
                        if ($localStorage.EditMode == false) {
                            $('.k-grid-toolbar').hide();
                        }
                   // }
                    });

        } else {
            $("#PickListValues").hide();
            $("#PickListDateValues").hide();
            $("#PickListNumberValues").hide();
            $("#drpDownDataType").data("kendoDropDownList").value("Text");
            $scope.userRoleAddPickList = true;
            $scope.newPickList = false;

        }
    };

    $scope.selecetedPLXMLData = "";
    $scope.GetPickListValuesDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, editable: true,
        serverPaging: true,
        serverSorting: true, pageable: false, sort: { field: "ListItem", dir: "asc" },
        transport: {
            read: function (options) {
   
                    dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            },
            update: function (options) {

                //$.msgBox({
                //    title: "Select an Action",
                //    content: "Are you sure want to Update the PickListValue?",
                //    type: "confirm",
                //    buttons: [{ value: "Yes" }, { value: "No" }],
                //    success: function (result) {
                //        var bool = false;
                //        if (result === "Yes") {
                //            bool = true;
                //        }
                //        if (bool === true) {
            
                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data[$scope.getRowIndex].id, $scope.getRowIndex, "Update", $scope.GetPickListValuesDatasource._data).success(function (response) {
                  
                    options.success(response);

                    if (response == false) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "error"
                        });
                        
                    }
                    else {

                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update successful.',
                            type: "error"
                        });
                    }
                    $scope.GetPickListValuesDatasource.read();
                }).error(function (error) {
                    options.error(error);
                });
                //        };
                //    }
                //});
            },
            create: function (options) {
               
                if ($scope.GetPickListValuesDatasource._data[0].id == undefined)
                {
                    $scope.GetPickListValuesDatasource._data[0].id = "";
                }
                else
                {
                    $scope.GetPickListValuesDatasource._data[0].id = "";
                }
                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data[0].id, "0", "Insert", $scope.GetPickListValuesDatasource._data).success(function (response) {
                    options.success(response);
                    if (response == false) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "error"
                        });
                        
                    }
                    else {

                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update successful.',
                            type: "error"
                        });
                    }
                    $scope.GetPickListValuesDatasource.read();
                }).error(function (error) {
                    options.error(error);
                });
            },
            
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
            }
        }, batch: true, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "ListItem",
                fields: {
                    ListItem: { editable: true }
                }
            }
        }

    });
    $scope.GetPickListDateValuesDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        editable: true,
        serverPaging: false,
        serverSorting: true,
        pageable: false, sort: { field: "ListItem", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            },
            update: function (options) {
            
                var date = options.data.models[0].ListItem;
                var isValid = (new Date(date)).getTime() > 0;
                if (isValid) {
                    var month, day, year, hours, minutes, seconds;
                    var date = new Date($scope.GetPickListDateValuesDatasource._data[$scope.getRowIndex].id),
                        month = ("0" + (date.getMonth() + 1)).slice(-2),
                        day = ("0" + date.getDate()).slice(-2);
                    hours = ("0" + date.getHours()).slice(-2);
                    minutes = ("0" + date.getMinutes()).slice(-2);
                    seconds = ("0" + date.getSeconds()).slice(-2);

                    var getDate = [month,day, date.getFullYear()].join("/");
                    var getTime = [hours, minutes, seconds].join(":");
                    var getUpdatedID = [getDate, getTime].join(" ");
                   // var str = DATEFORMAT.ToString(@"yyyy/MM/dd hh:mm:ss tt", new CultureInfo("en-US"));
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, getUpdatedID, $scope.getRowIndex, "Update", $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                    options.success(response);
                    
                    if (response == false) {

                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "error"
                        });

                      
                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update successful.',
                            type: "error"
                        });

                    }
                    $scope.GetPickListDateValuesDatasource.read();
                }).error(function (error) {
                    options.error(error);
                });
}
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Incorrect datetime format.',
                        type: "error"
                    });
                    //$scope.GetPickListDateValuesDatasource.read();
                }
            },
            create: function (options) {
                
                if ($scope.GetPickListDateValuesDatasource._data[0].id == undefined) {
                    $scope.GetPickListDateValuesDatasource._data[0].id = "";
                }
                else {
                    $scope.GetPickListDateValuesDatasource._data[0].id = "";
                }
                var date = options.data.models[0].ListItem;
                var isValid = (new Date(date)).getTime() > 0;
                if (isValid) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE,$scope.GetPickListDateValuesDatasource._data[0].id,"0", "Insert", $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                        options.success(response);
                       
                        if (response == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Duplicate Picklist values are not allowed.',
                                type: "error"
                            });

                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Update successful.',
                                type: "error"
                            });
                        }
                        $scope.GetPickListDateValuesDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Incorrect datetime format.',
                        type: "error"
                    });
                    //$scope.GetPickListDateValuesDatasource.read();
                }
            },
           
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
            }
        }, batch: true, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "ListItem",
                fields: {
                    ListItem: { editable: true, type: "date" }
                }
            }
        }

    });
    $scope.GetPickListNumericValuesDatasource = new kendo.data.DataSource({
    
        type: "json",
        serverFiltering: true,
        editable: true,
        serverPaging: false,
        serverSorting: true, 
        pageable: false, sort: { field: "ListItem", dir: "asc"},
        transport: {
            read: function (options) {
                dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            },
            update: function (options) {
                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data[$scope.getRowIndex].id, $scope.getRowIndex, "Update", $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                    options.success(response);
                    if (response == false) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "error"
                        });
                        
                    }
                      else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update successful.',
                            type: "error"
                        });
                    }
                    $scope.GetPickListNumericValuesDatasource.read();
                }).error(function (error) {
                    options.error(error);
                });

            },
            create: function (options) {
               
                if ($scope.GetPickListNumericValuesDatasource._data[0].id == undefined) {
                    $scope.GetPickListNumericValuesDatasource._data[0].id = "";
                }
                else {
                    $scope.GetPickListNumericValuesDatasource._data[0].id = "";
                }
                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data[0].id, "0", "Insert", $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                    options.success(response);
                    if (response == false) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "error"
                        });

                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update Successful.',
                            type: "error"
                        });
                    }
                    $scope.GetPickListNumericValuesDatasource.read();
                }).error(function (error) {
                    options.error(error);
                });

            },// destroy: function (options) {
               // options.success(true);
              //  return
                //$.msgBox({
                //    title: "Select an Action",
                //    content: "Are you sure want to delete the PickList Value?",
                //    type: "confirm",
                //    buttons: [{ value: "Yes" }, { value: "No" }],
                //    success: function (result) {

                //        if (result === "Yes") {
                //            var withoutSelectedVal = $scope.GetPickListNumericValuesDatasource._data;
                //            dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                //                options.success(response);
                //                if (response == false) {
                //                    $.msgBox({
                //                        title: $localStorage.ProdcutTitle,
                //                        content: 'The Picklist value is used in another place, Are you want to delete the PickList Value Everywhere?',
                //                        //type: "error"
                //                        type: "confirm",
                //                        buttons: [{ value: "Yes" }, { value: "No" }],
                //                        success: function (result) {

                //                            if (result === "Yes") {
                                                
                //                                dataFactory.removeFromAll($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, withoutSelectedVal).success(function (response) {
                //                                    options.success(response);
                //                                    if (response == false) {
                                                       
                //                                        $.msgBox({
                //                                            title: $localStorage.ProdcutTitle,
                //                                            content: 'The Picklist value is deleted successfully.',

                //                            if (result === "Yes") {

                //                                dataFactory.removeFromAll($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, withoutSelectedVal).success(function (response) {
                //                                    options.success(response);
                //                                    if (response == false) {

                //                                        $.msgBox({
                //                                            title: $localStorage.ProdcutTitle,
                //                                            content: 'The Picklist value is deleted successfully.',
                //                                            //type: "error"
                //                                        });
                ////                                        $scope.GetPickListNumericValuesDatasource.read();

                //                                  }
                //                                });
                //                            }
                //                        }
                //                        //type: "error"
                //                    });

                //                }
                //                else {
                //                    $.msgBox({
                //                        title: $localStorage.ProdcutTitle,
                //                        content: 'The Picklist value is deleted successfully.',
                //                        //type: "error"
                //                    });
                //                }
                //                $scope.GetPickListNumericValuesDatasource.read();

                //            }).error(function (error) {
                //                options.error(error);
                //            });
                //        }

                //        else {
                //            $scope.GetPickListNumericValuesDatasource.read();
                //        }
                //    }
                //});
          //  },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
            }
        }, batch: true, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "ListItem",
                fields: {
                    ListItem: { editable: true, type: "number" }
                }
            }
        }

    });
    $scope.GetPickListNumberValues = {
        dataSource: $scope.GetPickListNumericValuesDatasource,
        sortable: true,
        scrollable: true, filterable: true, autoBind: false,
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Count : {2}"
            }
        }, editable: {
            mode: "inline",
            createAt: "bottom"
        },
        cancel: function (e) {
            setTimeout(function () {
                var grid = $("#PickListNumberValuesgrid").data("kendoGrid");
                var gridData = grid.dataSource.view();

                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($rootScope.userRoleModifyPickListManagement == false) {
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var editButton = $(currenRow).find(".k-grid-edit");
                        editButton.hide();
                    }
                    if ($rootScope.userRoleDeletePickListManagement == false) {

                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var deleteButton = $(currenRow).find(".k-grid-delete");
                        deleteButton.hide();
                    }

                }
            });
        },
        toolbar: [{ name: "create", text: "", template: "<button class=\'k-button k-grid-add k-item \' title=\'Add\' ng-show=\'userRoleAddPickListManagement\'>Add</button>" },
             //{ name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'>Save</button>" },
             //{ name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'>Cancel</button>" }
        ],
        //selectable: "row",
        columns: [{
            field: "ListItem", title: "Value", editor: function (container, options) {
                $scope.name = '';
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'numeric') {
                    var input = $('<input name="' + options.field + '" required="required"/>');
                    input.appendTo(container);
                    input.kendoNumericTextBox();
                    var grid = $("#PickListNumberValuesgrid").data("kendoGrid");
                    var gridData = grid.dataSource.view();

                    for (var i = 0; i < gridData.length; i++) {
                        var currentUid = gridData[i].uid;
                        if ($rootScope.userRoleModifyPickListManagement == false) {
                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var editButton = $(currenRow).find(".k-grid-edit");
                            editButton.hide();
                        }
                        if ($rootScope.userRoleDeletePickListManagement == false) {

                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var deleteButton = $(currenRow).find(".k-grid-delete");
                            deleteButton.hide();
                        }

                    }
                }
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'text') {
                    var input = $('<input name="' + options.field + '" type="text" required="required"/>');
                    input.appendTo(container);
                }
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'hyperlink') {
                    var input = $('<input name="' + options.field + '" type="text"  onchange="TextBoxChanged(this)" id="link" required="required"/>');
                    input.appendTo(container);
                }
                //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                tooltipElement.appendTo(container);
            }
        }, {
            //   command: [
            //    { name: "save", text: "", width: "10px", template: "<a ng-click='updateNumeric(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
            //{ name: "cancel", text: "", width: "10px", template: "<a ng-click='cancelNumeric(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
            //{ name: "destroy", text: "", width: "10px", template: "<a  class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove  blue btn-xs-icon\'></div></a>" }], title: "Actions"
            command: ["edit", { name: "destroy", text: "", width: "10px", template: "<a class='k-button k-button-icontext ' ng-show='userRoleDeletePickListManagement' title = \'Delete\' ng-click='\RemoveNumericList($event,this)\'><span class='k-icon k-delete' title = \'Delete\'></span>Delete</a>" }], width: "250px"
                  ,
            title: "Actions"
        }],
        dataBound: function (e) {
            var editeBtn = $(".k-grid-edit");
            var deleteBtn = $(".k-grid-delete");

            editeBtn.attr("title", "Edit");
            deleteBtn.attr("title", "Delete");
            if (e.sender._data.length > 0) {
                var sort = e.sender._data[0].Sort;
                if (sort != undefined) {
                    if (sort != "asc") {
                        if (e.sender.dataSource.sort()[0].dir != "desc") {
                            e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                        }
                    }
                }
            }
            var grid = $("#PickListNumberValuesgrid").data("kendoGrid");
            var gridData = grid.dataSource.view();

            for (var i = 0; i < gridData.length; i++) {
                var currentUid = gridData[i].uid;
                if ($rootScope.userRoleModifyPickListManagement == false) {
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var editButton = $(currenRow).find(".k-grid-edit");
                    editButton.hide();
                }
                if ($rootScope.userRoleDeletePickListManagement == false) {

                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var deleteButton = $(currenRow).find(".k-grid-delete");
                    deleteButton.hide();
                }

            }
            var grid = $("#PickListNumberValuesgrid").data("kendoGrid");
            $(grid.tbody).on("click", "td", function (e) {
                var row = $(this).closest("tr");
                var rowIdx = $("tr", grid.tbody).index(row);
                var colIdx = $("td", row).index(this);
                $scope.getRowIndex = rowIdx;

            });
        },
        edit: function (e) {
            var updateBtn = $(".k-grid-update");
            var cancelBtn = $(".k-grid-cancel");

            updateBtn.attr("title", "Update");
            cancelBtn.attr("title", "Cancel");

        }

    };

    //This is for remove the numeric values for picklist

    //This is for remove the numeric values for picklist

    $scope.RemoveNumericList = function (event, e) {

        // $scope.GetPickListNumericValuesDatasource._data.remove(e.dataItem);
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to delete the Picklist Value?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {

                if (result === "Yes") {
                    if ($scope.getRowIndex == undefined) {
                        $scope.getRowIndex = 0;
                    }
                    var withoutSelectedVal = $scope.GetPickListNumericValuesDatasource._data;
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, e.dataItem.id, $scope.getRowIndex, "DELETED", $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                        // options.success(response);

                        if (response == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'The Picklist value is referred in few product specifications.',
                                //type: "error"
                                type: "confirm",
                                buttons: [{ value: "OK" }],
                                success: function (result) {

                                    if (result === "OK") {

                                        //dataFactory.removeFromAll($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, withoutSelectedVal).success(function (response) {
                                        //    options.success(response);
                                        //    if (response == false) {

                                        //        $.msgBox({
                                        //            title: $localStorage.ProdcutTitle,
                                        //            content: 'The Picklist value is deleted successfully.',

                                        //            //type: "error"
                                        //        });
                                        $scope.GetPickListNumericValuesDatasource.read();

                                        //    }
                                        //});
                                    }
                                }
                                //type: "error"
                            });

                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'The Picklist value is deleted successfully.',
                                //type: "error"
                            });
                        }
                        $scope.GetPickListNumericValuesDatasource.read();

                    })
                }

                else {
                    $scope.GetPickListNumericValuesDatasource.read();
                }
            }
        });

    }

    //This is for remove the Text  values for picklist

    $scope.RemoveTextList = function (event, e) {

        // $scope.GetPickListValuesDatasource._data.remove(e.dataItem);

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to delete the Picklist Value?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {

                if (result === "Yes") {




                    if ($scope.getRowIndex == undefined) {
                        $scope.getRowIndex = 0;
                    }
                    var withoutSelectedVal = $scope.GetPickListValuesDatasource._data;
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, e.dataItem.id, $scope.getRowIndex, "DELETED", $scope.GetPickListValuesDatasource._data).success(function (response) {
                        //options.success(response);
                        if (response == false) {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'The Picklist value is referred in few product specifications.',
                                //type: "error"
                                type: "confirm",
                                buttons: [{ value: "OK" }],
                                success: function (result) {

                                    if (result === "OK") {

                                        //dataFactory.removeFromAll($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, withoutSelectedVal).success(function (response) {
                                        //    options.success(response);
                                        //    if (response == false) {

                                        //        $.msgBox({
                                        //            title: $localStorage.ProdcutTitle,
                                        //            content: 'The Picklist value is deleted successfully.',

                                        //            //type: "error"
                                        //        });
                                        $scope.GetPickListValuesDatasource.read();

                                        //    }
                                        //});
                                    }
                                }
                            });

                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'The Picklist value is deleted successfully.',
                                //type: "error"
                            });
                        }
                        $scope.GetPickListValuesDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {

                    $scope.GetPickListValuesDatasource.read();
                }


            }
        });
    }
    //This is for remove the Date values for picklist
    $scope.RemoveDateList = function (event, e) {

        // $scope.GetPickListDateValuesDatasource._data.remove(e.dataItem);

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to delete the Picklist Value?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                if (result === "Yes") {
                    if ($scope.getRowIndex == undefined) {
                        $scope.getRowIndex = 0;
                    }
                    var withoutSelectedVal = $scope.GetPickListDateValuesDatasource._data;
                    var month, day, year, hours, minutes, seconds;
                    var date = new Date(e.dataItem.id),
                        month = ("0" + (date.getMonth() + 1)).slice(-2),
                        day = ("0" + date.getDate()).slice(-2);
                    hours = ("0" + date.getHours()).slice(-2);
                    minutes = ("0" + date.getMinutes()).slice(-2);
                    seconds = ("0" + date.getSeconds()).slice(-2);

                    var getDate = [month, day, date.getFullYear()].join("/");
                    var getTime = [hours, minutes, seconds].join(":");
                    var getUpdatedID = [getDate, getTime].join(" ");

                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, getUpdatedID, $scope.getRowIndex, "DELETED", $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                        //options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'The Picklist value is referred in few product specifications.',
                                //type: "error"
                                type: "confirm",
                                buttons: [{ value: "OK" }],
                                success: function (result) {

                                    if (result === "OK") {

                                        //dataFactory.removeFromAll($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, withoutSelectedVal).success(function (response) {
                                        //    options.success(response);
                                        //    if (response == false) {

                                        //        $.msgBox({
                                        //            title: $localStorage.ProdcutTitle,
                                        //            content: 'The Picklist value is deleted successfully.',

                                        //            //type: "error"
                                        //        });
                                        $scope.GetPickListDateValuesDatasource.read();

                                        //    }
                                        //});
                                    }
                                }
                                //type: "error"
                            });

                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'The Picklist value is deleted successfully.',
                                //type: "error"
                            });
                        }
                        $scope.GetPickListDateValuesDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {

                    $scope.GetPickListDateValuesDatasource.read();
                }


            }
        });

    }

    $scope.GetPickListDateValues = {
        dataSource: $scope.GetPickListDateValuesDatasource,
        sortable: true,
        scrollable: true, filterable: true, autoBind: false,
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Count : {2}"
            }
        }, editable: {
            mode: "inline",
            createAt: "bottom"
        },
        cancel: function (e) {
            setTimeout(function () {
                var grid = $("#PickListDateValuesgrid").data("kendoGrid");
                var gridData = grid.dataSource.view();

                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($rootScope.userRoleModifyPickListManagement == false) {
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var editButton = $(currenRow).find(".k-grid-edit");
                        editButton.hide();
                    }
                    if ($rootScope.userRoleDeletePickListManagement == false) {

                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var deleteButton = $(currenRow).find(".k-grid-delete");
                        deleteButton.hide();
                    }

                }
            });
        },
        toolbar: [{ name: "create", text: "", template: "<button class=\'k-button k-grid-add k-item \' title=\'Add\' ng-show=\'userRoleAddPickListManagement\'>Add</button>" }
        ],
        // selectable: "row",
        columns: [{
            field: "ListItem", title: "Value", format: "{0:MM/dd/yyyy hh:mm tt}", editor: function (container, options) {
                //create input element and add the validation attribute
                $scope.name = '';
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'date and time') {
                    var input = $('<input kendo-date-time-picker name="' + options.field + '" k-interval=10 required="required" maxlength = "18" />');
                    input.appendTo(container);
                }
                //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                tooltipElement.appendTo(container);
            }, filterable: {
                ui: function (element) {
                    element.kendoDatePicker({
                        format: "{0:MM/dd/yyyy}"
                    });
                }
            }
        }, {
            command: ["edit", { name: "destroy", text: "", width: "10px", template: "<a class='k-button k-button-icontext ' title = \'Delete\' ng-click='\RemoveDateList($event,this)\'><span class='k-icon k-delete' title = \'Delete\'></span>Delete</a>" }], width: "250px"
                  ,
            title: "Actions"
            //   command: [
            //    { name: "save", text: "", width: "10px", template: "<a ng-click='updateDate(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
            //{ name: "cancel", text: "", width: "10px", template: "<a ng-click='cancelDate(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
            //{ name: "destroy", text: "", width: "10px", template: "<a  class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove  blue btn-xs-icon\'></div></a>" }], title: "Actions"
        }],
        dataBound: function (e) {
            var editeBtn = $(".k-grid-edit");
            var deleteBtn = $(".k-grid-delete");

            editeBtn.attr("title", "Edit");
            deleteBtn.attr("title", "Delete");


            if (e.sender._data.length > 0) {
                var sort = e.sender._data[0].Sort;
                if (sort != undefined) {
                    if (sort != "asc") {
                        if (e.sender.dataSource.sort()[0].dir != "desc") {
                            e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                        }
                    }
                }
            }
            var grid = $("#PickListDateValuesgrid").data("kendoGrid");
            var gridData = grid.dataSource.view();

            for (var i = 0; i < gridData.length; i++)
            {
                var currentUid = gridData[i].uid;
                if ($rootScope.userRoleModifyPickListManagement == false) {
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var editButton = $(currenRow).find(".k-grid-edit");
                    editButton.hide();
                }
                if ($rootScope.userRoleDeletePickListManagement == false) {

                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var deleteButton = $(currenRow).find(".k-grid-delete");
                    deleteButton.hide();
                }

            }
            var grid = $("#PickListDateValuesgrid").data("kendoGrid");
            $(grid.tbody).on("click", "td", function (e) {
                var row = $(this).closest("tr");
                var rowIdx = $("tr", grid.tbody).index(row);
                var colIdx = $("td", row).index(this);
                $scope.getRowIndex = rowIdx;

            });
        },

        edit: function (e)
        {         
            var updateBtn = $(".k-grid-update");
            var cancelBtn = $(".k-grid-cancel");
            updateBtn.attr("title", "Update");
            cancelBtn.attr("title", "Cancel");
        }

    };
    $scope.DeletePickList = function () {


        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to delete the Picklist Name?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                if (result === "Yes") {

                    dataFactory.DeletePickList($scope.SelectedPickList).success(function (response) {

                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });
                        $scope.selecetedPLXMLData = "";
                        $scope.SelectedPickList = "";

                        $scope.PickListDataType.PICKLIST_DATA_TYPE = "";
                        $("#PickListValues").hide();
                        $("#PickListDateValues").hide();
                        $("#PickListNumberValues").hide();
                        $scope.PickListGroup.read();
                        $("#drpDownDataType").data("kendoDropDownList").value("Text");
                        $scope.userRoleAddPickList = true;
                        $scope.newPickList = false;

                    }).error(function (error) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "error"
                        });
                    });

                }
                else {

                      $scope.GetPickListDateValuesDatasource.read();
                }


            }
        });

    };


    $scope.GetPickListValues = {

        dataSource: $scope.GetPickListValuesDatasource,
        sortable: true,
        scrollable: true, filterable: true, autoBind: false,
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Count : {2}"
            }
        }, editable: {
            mode: "inline",
            createAt: "bottom",

        },
        cancel: function (e) {
            setTimeout(function () {
                var grid = $("#PickListValuesgrid").data("kendoGrid");
                var gridData = grid.dataSource.view();

                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($rootScope.userRoleModifyPickListManagement == false) {
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var editButton = $(currenRow).find(".k-grid-edit");
                        editButton.hide();
                    }
                    if ($rootScope.userRoleDeletePickListManagement == false) {

                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var deleteButton = $(currenRow).find(".k-grid-delete");
                        deleteButton.hide();
                    }

                }
            });
        },
        toolbar: [{ name: "create", text: "", template: "<button title=\'Add\' class=\'k-button k-grid-add k-item \' ng-show=\'userRoleAddPickListManagement\'>Add</button>" },
          //{ name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'>Save</button>" },
          //{ name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'>Cancel</button>" }
        ],
        selectable: "row",
        columns: [{
            field: "ListItem", title: "Value", editor: function (container, options) {
                //create input element and add the validation attribute
                $scope.name = '';
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'numeric') {
                    var input = $('<input name="' + options.field + '"  data-required-msg="Picklist value is mandatory." required="required"/>');
                    input.appendTo(container);
                    input.kendoNumericTextBox();
                }
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'text') {

                    var input = $('<input name="' + options.field + '" type="text"  data-required-msg="Picklist value is mandatory." required="required"/>');
                    input.appendTo(container);
                    var grid = $("#PickListValuesgrid").data("kendoGrid");
                    var gridData = grid.dataSource.view();

                    for (var i = 0; i < gridData.length; i++) {
                        var currentUid = gridData[i].uid;
                        if ($rootScope.userRoleModifyPickListManagement == false) {
                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var editButton = $(currenRow).find(".k-grid-edit");
                            editButton.hide();
                        }
                        if ($rootScope.userRoleDeletePickListManagement == false) {

                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var deleteButton = $(currenRow).find(".k-grid-delete");
                            deleteButton.hide();
                        }

                    }
                }
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'hyperlink') {
                    var input = $('<input name="' + options.field + '" type="text"   data-required-msg="Picklist value is mandatory."  onchange="TextBoxChanged(this)" id="link" required="required"/>');
                    input.appendTo(container);
                }
                //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                tooltipElement.appendTo(container);
            }
        }, {
            //command: [
            //     { name: "save", text: "", width: "10px", template: "<a ng-click='update(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
            // { name: "cancel", text: "", width: "10px", template: "<a ng-click='cancel(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
            //    { name: "destroy", text: "", width: "10px", template: "<a class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" }

            //], title: "Actions", width: "100px"
            command: ["edit", { name: "destroy", text: "", width: "10px", template: "<a class='k-button k-button-icontext ' title = \'Delete\' ng-click='\RemoveTextList($event,this)\'><span class='k-icon k-delete' title = \'Delete\'></span>Delete</a>" }], width: "250px"
                  ,
            title: "Actions"
        }],
        dataBound: function (e) {
            var editeBtn = $(".k-grid-edit");
            var deleteBtn = $(".k-grid-delete");

            editeBtn.attr("title", "Edit");
            deleteBtn.attr("title", "Delete");


            if (e.sender._data.length > 0) {
                var sort = e.sender._data[0].Sort;
                if (sort != undefined) {
                    if (sort != "asc") {
                        if (e.sender.dataSource.sort()[0].dir != "desc") {
                            e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                        }
                    }
                }
            }


            var grid = $("#PickListValuesgrid").data("kendoGrid");
            var gridData = grid.dataSource.view();

            for (var i = 0; i < gridData.length; i++) {
                var currentUid = gridData[i].uid;
                if ($rootScope.userRoleModifyPickListManagement == false) {
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var editButton = $(currenRow).find(".k-grid-edit");
                    editButton.hide();

                }
                if ($rootScope.userRoleDeletePickListManagement == false) {

                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var deleteButton = $(currenRow).find(".k-grid-delete");
                    deleteButton.hide();

                }

            }
            var grid = $("#PickListValuesgrid").data("kendoGrid");
            $(grid.tbody).on("click", "td", function (e) {
                var row = $(this).closest("tr");
                var rowIdx = $("tr", grid.tbody).index(row);
                var colIdx = $("td", row).index(this);
                $scope.getRowIndex = rowIdx;
              
            });
        },
        edit: function (e) {
         
            var updateBtn = $(".k-grid-update");
            var cancelBtn = $(".k-grid-cancel");

            updateBtn.attr("title", "Update");
            cancelBtn.attr("title", "Cancel");

        }
    };
    $scope.update = function (e) {

        dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
            // options.success(response);
            if (response == false) {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Duplicate Picklist values are not allowed.',
                    type: "error"
                });

                $scope.GetPickListValuesDatasource.read();
            }
        }).error(function (error) {
            // options.error(error);
        });
    }
    $scope.updateNumeric = function (e) {

        dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
            // options.success(response);
            if (response == false) {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Duplicate Picklist values are not allowed.',
                    type: "error"
                });

                $scope.GetPickListNumericValuesDatasource.read();
            }
        }).error(function (error) {
            // options.error(error);
        });
    }

    $scope.updateDate = function (e) {

        dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data).success(function (response) {
            // options.success(response);
            if (response == false) {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Duplicate Picklist values are not allowed.',
                    type: "error"
                });

                $scope.GetPickListDateValuesDatasource.read();
            }
        }).error(function (error) {
            // options.error(error);
        });
    }

    $scope.cancel = function (e) {

        $scope.GetPickListValuesDatasource.read();
    }
    $scope.cancelNumeric = function (e) {

        $scope.GetPickListNumericValuesDatasource.read();
    }
    $scope.cancelDate = function (e) {
        $scope.GetPickListDateValuesDatasource.read();
    }
    $scope.userRoleAddPickList = false;
    $scope.userRoleModifyPickList = false;
    $scope.userRoleDeletePickList = false;
    $scope.userRoleViewPickList = false;
    $scope.oldcatalogid;
    $scope.$on("oldCatalog", function (event) {

        $scope.oldcatalogid = $rootScope.WORKINGCATALOGID;
        //  alert(passcategoryid);
    });
    $scope.getUserRoleRightsPickList = function () {
        var id = 40205;


        dataFactory.getUserRoleRights(id, parseInt($localStorage.getCatalogID)).success(function (response) {

            if (response !== "" && response !== null) {
                $scope.userRoleAddPickList = response[0].ACTION_ADD;
                $scope.userRoleModifyPickList = response[0].ACTION_MODIFY;
                $scope.userRoleDeletePickList = response[0].ACTION_REMOVE;
                $scope.userRoleViewPickList = response[0].ACTION_VIEW;
            }

        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.init = function () {
        $scope.getUserRoleRightsPickList();
    };

    $scope.init();

    $scope.ImportBTN = function () {
        $("#PickListValues").hide();
        $("#PickListDateValues").hide();
        $("#PickListNumberValues").hide();

        $scope.UploadDiv = true;
        //  $scope.RoleUploadDiv = false;
        // $scope.userbuttondiv = true;
        $("#Userbuttonhide").show();
        $scope.rolebuttondiv = false;
        $("#Rolebuttonhide").hide();
       
        $scope.RoleImportdiv = false;
        $scope.showImportdiv = true;
        $scope.hideImportType = false;
        $scope.Importdiv = true;
        $scope.UserImportdiv = true;
        $scope.hideImportType = true;
        $scope.importtype = "PickList";
        $rootScope.importtypeValue = "PickList";
        localStorage.setItem("saveImportType", $rootScope.importtypeValue);

        $("#showImportdiv").show();
        $scope.$emit("MyFunction");
        //$rootScope.userRoleAddExportIMPORT = true;
        //$rootScope.userRoleViewExportIMPORT = true;
    }


    // Picklist  - Start
    $scope.ExprotBTN = function () {
        $scope.SelectedPickListDataType = $('#drpPickListName').data("kendoDropDownList").value();



        if ($scope.SelectedPickListDataType != "") {

            $scope.SelectedPickListDataType = $scope.SelectedPickListDataType;
        }


        dataFactory.getPicklistExportData($scope.SelectedPickListDataType, $rootScope.selecetedCatalogId, $rootScope.DisplayIdcolumns).success(function (response) {

            if (response !== "" && response !== null) {

                window.open("DownloadPickListExport.ashx?");
                $.msgBox({
                    title: "CatalogStudio",
                    content: "Export completed successfully.",
                    type: "info"

                });
            }

        }).error(function (error) {
            options.error(error);
        });


    };




    $scope.displayFileName = '';

    $scope.selectFileforUpload = function (file) {
        this.value = null;
        $scope.SelectedFileForUpload = file[0];

        $scope.SelectedFileForUploadnamemainPicklstImport = file[0].name;
        $rootScope.SelectedFileForUploadImport = file[0].name;
        if ($rootScope.SelectedFileForUploadImport.length <= 40) {
            $scope.displayFileName = $rootScope.SelectedFileForUploadImport;
        }
        else {
            $scope.displayFileName = $rootScope.SelectedFileForUploadImport.substring(0, 40) + "." + $rootScope.SelectedFileForUploadImport.split('.')[1];
        }
    };

    $scope.ResetBTN = function () {
        $scope.SelectedFileForUpload = null;
        $scope.TemplateNameImport = "";
        $scope.backbutton = false;
        var $el = $('#importfile1');
        if ($el.val() != "") {
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
        }
        $rootScope.SelectedFileForUploadImport = "";
        $scope.displayFileName = "";

    }


    $scope.picklistImport=function ()
    {
        $("#PicklistGrid").hide();
        $
    }

    // Picklist - End





}]);