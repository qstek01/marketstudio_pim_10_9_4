﻿LSApp.controller('xpressCatalogController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $rootScope, $localStorage) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;


    $scope.partialPDF = false;
    //$scope.IsExistingProject = false;
    $scope.hierarchy = "No";
    $rootScope.hierarchy_Values = "No";
    $scope.SelectedcategoryIdexp = "";
    $('#Catalog_Details').hide();
    $scope.SelectedCatalogId = 0;
    $scope.catalogDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetCatalogDetails($scope.SelectedCatalogId, $scope.getCustomerID).success(function (response) {

                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.projectChange = function (e) {
        $("#simplePDFTemplateDesinger").hide();
        $("#hierarchyPDFTemplateDesinger").hide();

        //$("#treeViewDiv").hide();
        //$("#treeViewDivFam").hide();
        $scope.NewProject.projectName = "";
        $scope.SelectedprojectId = e.sender.value();
        $scope.SelectedprojectName = e.sender.text();
        $scope.IsExistingProject = true;
        $scope.NewProject.projectName = $scope.SelectedprojectName;

        if ($scope.NewProject.projectName == "--- New ---") {
            $scope.NewProject.projectName = "";
            $scope.Projectnamedisable = false;
            $scope.IsExistingProject = false;
        }
        else {
            $scope.Projectnamedisable = true;
        }

    };
    //----------Project name dropdown---
    $scope.projectDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {

                $scope.catalogDataSource.read();
                dataFactory.GetProjectDetails($scope.SelectedCatalogId, $("#projecttype").val()).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }
        }
    });

    //--------------------------------------------
    //-------Delete selected PDF
    $scope.DeletePDFProject = function (SelectedprojectId) {

        if ($scope.SelectedprojectId !== "") {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure want to remove the project?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {

                    if (result === "Yes") {


                        $http.post("../App/RemoveProjectFromDb?catlogId=" + $scope.SelectedCatalogId + "&selectedprojectId=" + $scope.SelectedprojectId + "&selectedprojectName=" + $scope.SelectedprojectName)
                            .success(function (msg) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + msg + '.',
                                    type: "info"
                                });
                                $scope.projectDataSource.read();
                                $scope.NewProject.projectName = "";
                                $scope.Projectnamedisable = false;
                            })
                            .error(function () {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Please try again.',
                                    type: "error"
                                });


                            });
                    }
                    else {

                        return;
                    }
                }
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select one project.',
                //type: "info"
            });


        }


    };
    //-----------------------------------
    $scope.ModifiyExportType = function (e) {

        $scope.exportFileFormat = e.exportType;
    };
    $scope.export = function () {
        if ($scope.exportFileFormat != undefined) {
            $http.post("../XpressCatalog/ExportResult?format=" + $scope.exportFileFormat).then(function (response) {
                var windowlocation = window.location.origin;
                var url = response.data;
                $.ajax({
                    url: windowlocation,
                    success: function () {
                        window.open(url);
                    },
                    error: function () {
                        alert('does not exist in server location');
                    },
                });
            });
        }
    }
    $scope.catalogChange = function (e) {
        $scope.SelectedCatalogId = e.sender.value();
        $scope.SelectedCatalogName = e.sender.text();
        $localStorage.getCatalogID = $scope.SelectedCatalogId;
        $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
        $scope.projectDataSource.read();
        $scope.PDFExpressTreeData.read();
        $http.get("../XpressCatalog/getCatalogID?catalogID=" + $scope.SelectedCatalogId).then(function () { });
        $http.get("../xpressSimpleCatalog/getCurrentCatalogID?catalogID=" + $scope.SelectedCatalogId).then(function () { });
    };

    $scope.pdfCatalogType = function () {

        if ($scope.SelectedCatalogId == '' || $scope.SelectedCatalogId == 0) {
            $scope.SelectedCatalogId = $rootScope.selecetedCatalogId;
        }
        if ($scope.checkeditems != "") {
            $http.get("../XpressCatalog/getCategoryId?categoryId=" + $scope.checkeditems).then(function () { });
            //$http.get("../XpressCatalogApi/getCategoryId?categoryId=" + $scope.checkeditems).then(function () { });
            $http.get("../xpressSimpleCatalog/getCategoryId?categoryId=" + $scope.checkeditems).then(function () { });
            dataFactory.getCategoryId($scope.checkeditems).success(function (responseCatId) { });
        
        //else
        //{
        //    $http.get("../XpressCatalog/getCategoryId?categoryId=ALL").then(function () { });
        //    //$http.get("../XpressCatalogApi/getCategoryId?categoryId=" + $scope.checkeditems).then(function () { });
        //    $http.get("../xpressSimpleCatalog/getCategoryId?categoryId=ALL").then(function () { });
        //    dataFactory.getCategoryId("ALL").success(function (responseCatId) { });
        //}
        $http.get("../XpressCatalog/SaveProject?catalogID=" + $scope.SelectedCatalogId + "&&projectName=" + $scope.NewProject.projectName + "&&projectType=" + $scope.NewProject.projectType + "&&comments=" + $scope.NewProject.comments).
            then(function (data) {
                if (data.data == 'False') {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Project Name already exists, please enter a different name for the Project.',
                        type: "error"
                    });

                }
                else {
                    $scope.winPDFCatalog.refresh({ url: "../views/app/partials/pdfCatalogType.html" });
                    $scope.winPDFCatalog.center().open();
                    $("#pdfcatalogtyp").show();
                    $("#pdfPageSetup").hide();
                    $("#pdfAdvanceOptions").hide();
                }

            });
        }
        else
        {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Kindly select category(s) to proceed. (Affects only Hierarchical template)',
                type: "error"
            });
        }
    };
    $scope.closeOpentemplate = function () {
        $("#opendesigner").hide();
    };

    $scope.closeRuntemplate = function () {
        $("#runtemplate").hide();
    };
    $scope.closeOpenCatalog = function () {
        $("#catalogtemplate").hide();
    };
    $scope.closeSimpletemplate = function () {
        $("#simplePDFTemplateDesinger").hide();
    };
    $scope.getDownloadFile = {
        filename: '',
        sourcePath: '',
        userName: ''
    };
    $scope.downloadSimpletemplate = function () {

        var filename = $scope.NewProject.projectName + '.mrt';
        var sourcePath = 'Simple';
        var userName = $scope.currentUserName;
        window.open("DownloadPDFTemplate.ashx?FileName=" + filename + "&Path=" + sourcePath + "&UserName=" + userName);
    };
    $scope.downloadHierarchytemplate = function () {

        var filename = $scope.NewProject.projectName + '.mrt';
        var sourcePath = 'Hierarchical';
        var userName = $scope.currentUserName;
        window.open("DownloadPDFTemplate.ashx?FileName=" + filename + "&Path=" + sourcePath + "&UserName=" + userName);
    };
    $scope.closeHierarchicaltemplate = function () {
        $("#hierarchyPDFTemplateDesinger").hide();
    };
    $scope.downloadDesignTemplate = function () {

        var filename = 'Report.mrt';
        var sourcePath = 'Design';
        var userName = $scope.currentUserName;
        window.open("DownloadPDFTemplate.ashx?FileName=" + filename + "&Path=" + sourcePath + "&UserName=" + userName);
    };
    // create DropDownList from input HTML element
    var data = [{ type: "PDF Catalog", value: "3" }
    //,{ type: "Export", value: "7" }
    //        ,{ type: "InDesign Catalog", value: "1" }
        //, { type: "HTML Catalog", value: "9" }
    ];

    $("#projecttype").kendoDropDownList({
        dataTextField: "type",
        dataValueField: "value",
        dataSource: data,
        index: 0
    });

    $scope.NewProject = {
        catalogID: $scope.SelectedCatalogId,
        projectName: '',
        projectType: $("#projecttype").val(),
        comments: ''
    };
    //Upload mrt files for Open Template
    $scope.ChechFileValid = function (file) {
        var isValid = false;
        if ($scope.SelectedFileForUpload != null) {
            if (file.name.contains(".mrt")) {
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type .mrt allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "MRT Files required!";
        }
        $scope.IsFileValid = isValid;
    };

    $scope.selectFileforUpload = function (file) {

        $scope.SelectedFileForUpload = file[0];
        $scope.$apply(function () {
            $scope.SelectedFileForUpload = file[0];
            $scope.SelectedFileForUploadnamemain = file[0].name;
        });

        var category_Id = '';
        var Catalog_ID = $localStorage.getCatalogID;
        dataFactory.setPdfXpressType("ALL", category_Id, "", "", Catalog_ID).success(function (response) {

        });

    };
    $scope.SaveFile = function () {

        $('#opendesigner').hide();
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        $scope.ChechFileValid($scope.SelectedFileForUpload);
        $scope.UploadFile($scope.SelectedFileForUpload).then(function () {
            clearForm();
        }, function (e) {
        });
    };
    function clearForm() {
        $scope.FileDescription = "";
        angular.forEach(angular.element("input[type='file']"), function (inputElem) {
            angular.element(inputElem).val(null);
        });
        $scope.f1.$setPristine();
        $scope.IsFormSubmitted = false;
    }

    $scope.openPDFPath = "";
    $scope.UploadFile = function (file) {
        var formDataxpress = new FormData();
        formDataxpress.append("file", file);
        formDataxpress.append("TYPE", "OPEN");
        formDataxpress.append("CatalogId", $localStorage.getCatalogID);
        formDataxpress.append("ID", 0)

        $http.post("/XpressCatalog/SaveFiles", formDataxpress,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            }).success(function (d) {
                if (d != "") {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'File upload successful.',
                        // type: "info"
                    });


                    $scope.openPDFPath = d;
                    dataFactory.FindProjectType().success(function (response) {
                        if (response !== "Error") {
                            if (response.toLocaleLowerCase().contains("simple")) {
                                $scope.winPDFCatalog.refresh({ url: "../views/app/partials/pdfCatalogDesign.html" });
                                $scope.winPDFCatalog.center().open();
                            } else {
                                // $("#opendesigner").show();
                                window.open("../xpressSimpleCatalog/OpenDesignerTemplate", "_blank")
                            }
                        } else {
                            //$("#opendesigner").show();
                            window.open("../xpressSimpleCatalog/OpenDesignerTemplate", "_blank")
                        }
                    }).error(function (error) {
                        //$("#opendesigner").show();
                        window.open("../xpressSimpleCatalog/OpenDesignerTemplate", "_blank")
                    });
                }
            })
            .error(function () {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });

            });

        return defer.promise;
    };

    //Upload mrt files for Run Template
    $scope.ChechRunFileValid = function (file) {
        var isValid = false;
        if ($scope.SelectedFileForUploadRun != null) {
            if (file.name.contains(".mrt")) {
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type .mrt allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "MRT Files required!";
        }
        $scope.IsFileValid = isValid;
    };

    $scope.selectFileforUploadRun = function (file) {
        $scope.SelectedFileForUploadRun = file[0];
    };
    $scope.SaveRunFile = function () {
        $('#runtemplate').hide();
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        $scope.ChechRunFileValid($scope.SelectedFileForUpload);
        $scope.UploadRunFile($scope.SelectedFileForUpload).then(function () {
            clearRunForm();
        }, function (e) {
        });
    };
    function clearRunForm() {
        $scope.FileDescription = "";
        angular.forEach(angular.element("input[type='file']"), function (inputElem) {
            angular.element(inputElem).val(null);
        });
        $scope.f1.$setPristine();
        $scope.IsFormSubmitted = false;
    }

    $scope.runPDFPath = "";
    $scope.UploadRunFile = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        $http.post("/XpressCatalog/SaveRunFiles", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                $scope.runPDFPath = d;
                $('#runtemplate').show();
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File Upload Success.',
                    type: "info"
                });

            })
            .error(function () {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
            });
        return defer.promise;
    };
    //Upload mdc files for OpenCatalog
    $scope.ChechCatalogFileValid = function (file) {
        var isValid = false;
        if ($scope.SelectedFileForUploadCatalog != null) {
            if (file.name.contains(".mdc")) {
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type .mdc allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "MDC Files required!";
        }
        $scope.IsFileValid = isValid;
    };

    $scope.selectFileforUploadCatalog = function (file) {
        $scope.SelectedFileForUploadCatalog = file[0];
    };
    $scope.SaveCatalogFile = function () {
        $('#catalogtemplate').hide();
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        $scope.ChechCatalogFileValid($scope.SelectedFileForUploadCatalog);
        $scope.UploadCatalogFile($scope.SelectedFileForUploadCatalog).then(function () {
            clearCatalogForm();
        }, function (e) {
        });
    };
    function clearCatalogForm() {
        $scope.FileDescription = "";
        angular.forEach(angular.element("input[type='file']"), function (inputElem) {
            angular.element(inputElem).val(null);
        });
        $scope.f1.$setPristine();
        $scope.IsFormSubmitted = false;
    }

    $scope.CatalogPDFPath = "";
    $scope.UploadCatalogFile = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        $http.post("/XpressCatalog/SaveCatalogFiles", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                $scope.CatalogPDFPath = d;
                $('#catalogtemplate').show();
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File Upload Success.',
                    type: "info"
                });
            })
            .error(function () {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
            });
        return defer.promise;
    };
    $scope.Open = function () {
        $('#opendesigner').show();
    };
    //$scope.getTemplateFileValuesDatasource = new kendo.data.DataSource({
    //    type: "json",
    //    serverFiltering: true, pageable: true, pageSize: 5, serverPaging: true,
    //    serverSorting: true,
    //    transport: {
    //        read: function (options) {
    //            dataFactory.getTemplate().success(function (response) {
    //                options.success(response);
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }, schema: {

    //        model: {
    //            id: "FILENAME",
    //            fields: {
    //                FILENAME: { editable: true },
    //                SOURCE: { editable: true },
    //                MODIFIED_DATE: { type: "date" }
    //            }
    //        }
    //    }
    //});

    $scope.getTemplateFileValuesDatasource = new kendo.data.DataSource({
        pageSize: 20,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        transport: {
            read: function (options) {
                dataFactory.getTemplate().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FILENAME",
                fields: {
                    FILENAME: { editable: true },
                    SOURCE: { editable: true },
                    MODIFIED_DATE: { type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });


            }
        }
    });
    $scope.OpenManage = function () {
        $scope.getTemplateFileValuesDatasource.read();
    }
    //$scope.getTemplateValues = {
    //    dataSource: $scope.getTemplateFileValuesDatasource,
    //    pageable: false, editable: "inline", sortable: true, scrollable: true, resizable: true,
    //    selectable: "row",
    //    columns: [{ field: "FILENAME", title: "Filename" },
    //        { field: "SOURCE", title: "Source" },
    //        { field: "MODIFIED_DATE", title: "Modified Date", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
    //     { title: "Download", template: "<button class=\"k-button\" ng-click=\"DownloadTemplate(this)\">Download File</button>" }],
    //    dataBound: function (e) {

    //        if (e.sender._data.length > 0) {

    //        }
    //    }
    //};

    $scope.getTemplateValuesColumns = [{ field: "FILENAME", title: "Filename" },
            { field: "SOURCE", title: "Source" },
            { field: "MODIFIED_DATE", title: "Modified Date", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
         { title: "Download", template: "<a class=\"k-item girdicons\" ng-click=\"DownloadTemplate(this)\"><div title=\'Download\' class=\'glyphicon glyphicon-download-alt blue btn-xs-icon\'></div></a>" }
//    { title: "Run", template: "<button class=\"k-button\" ng-click=\"\">Run</button>" },
//{ title: "Edit", template: "<button class=\"k-button\" ng-click=\"\">Edit</button>" }
 //{
 //    command: [
 //        { name: "download", text: "", width: "10px", template: "<a class=\'k-item girdicons\' ng-click=\"DownloadTemplate(this)\" style=\'padding-right:10px\'><div title=\'Download\' class=\'glyphicon glyphicon-download-alt blue btn-xs-icon\'></div></a>" },
 //        { name: "edit", text: "", width: "10px", template: "<a type=\'file\' class=\'k-item girdicons\' id=\'button\' style=\'padding-right:10px\' ng-click=\"EditTemplate(this)\"><div title=\'Run\' class=\'glyphicon glyphicon-play blue btn-xs-icon\'></div></a>" },
 //        { name: 'run', text: "", width: "10px", template: "<input id=\'input\' type=\'file\' class=\'k-item girdicons\'style=\'padding-right:10px\' ng-click=\"RunTemplate(this)\"> <div title=\'Edit\' class=\'glyphicon glyphicon-edit blue btn-xs-icon\'></div></input>" },
 //    ],
 //    title: "Actions", headerTemplate: '<span title="Actions">Actions</span>',
 //    width: "120px",
 //}
    ];

    $scope.closeDesignTemplate = function (e) {
        $('#newTemp').addClass('active');
        $('#designTemp').removeClass('active');
        $('#openTemp').removeClass('active');
        $('#runTemp').removeClass('active');
        $('#opencatalogTemp').removeClass('active');
        $('#downloadTemp').removeClass('active');

        $('#NewTab').addClass('active in');
        $('#DesignTemplate').removeClass('active in');
        $('#OpenTemplate').removeClass('active in');
        $('#Run').removeClass('active in');
        $('#openCatalog').removeClass('active in');
        $('#Template').removeClass('active in');


    };

    $scope.EditTemplate = function (e) {

    };

    $scope.RunTemplate = function (e) {
        var element = angular.element(document.getElementById('input'));
        element.triggerHandler('click');
        $scope.clicked = true;
    };

    $scope.DownloadTemplate = function (e) {

        $scope.selectedItem = e.dataItem;
        var filename;
        var sourcePath;
        var userName;
        var windowlocation = window.location.origin;
        if (e.dataItem.FILENAME.toLowerCase().contains(".mrt")) {
            filename = e.dataItem.FILENAME;
            sourcePath = e.dataItem.SOURCE;
            userName = e.dataItem.USERNAME;
        } else if (e.dataItem.FILENAME.toLowerCase().contains(".mdc")) {
            filename = e.dataItem.FILENAME;
            sourcePath = e.dataItem.SOURCE;
            userName = e.dataItem.USERNAME;
        }
        //else {
        //    filename = e.dataItem.FILENAME + ".mdc";
        //    sourcePath = e.dataItem.SOURCE;
        //    userName = e.dataItem.USERNAME;
        //}
        $.ajax({
            url: windowlocation,
            success: function () {
                window.open("DownloadPDFTemplate.ashx?FileName=" + filename + "&Path=" + sourcePath + "&UserName=" + userName);
            },
            error: function () {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Does not exist in server location.',
                    type: "error"
                });
            },
        });
    };

    $scope.userRoleAddPDFCatalog = false;
    $scope.userRoleModifyPDFCatalog = false;

    $scope.getUserRoleRightsPDFCatalog = function () {
        var id = 40302;
        dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
            if (response !== "" && response !== null) {
                $scope.userRoleAddPDFCatalog = response[0].ACTION_ADD;
                $scope.userRoleModifyPDFCatalog = response[0].ACTION_MODIFY;
            }

        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.init = function () {

        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        }
        else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        $scope.SelectedCatalogId = $rootScope.selecetedCatalogId;
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerID = 0;
        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
            $scope.currentUserName = $localStorage.getUserName;
        }
        if ($scope.SelectedCatalogName === undefined) {
            $scope.SelectedCatalogName = $localStorage.getCatalogName;
        }

        $http.get("../XpressCatalog/getCatalogID?catalogID=" + $scope.SelectedCatalogId).
            then(function (data) {
            });
        $http.get("../xpressSimpleCatalog/getCurrentCatalogID?catalogID=" + $scope.SelectedCatalogId).
                then(function (data) {
                    $scope.SelectedCatalogName = data.data;
                });

        $scope.getUserRoleRightsPDFCatalog();
        //dataFactory.getCatalogID($scope.SelectedCatalogId).success(function (response) {
        //    options.success(response);
        //    alert(response);
        //}).error(function (error) {
        //    options.error(error);
        //});
        $scope.SelectedCatalogId = $rootScope.selecetedCatalogId;
       // $scope.SimpleTemplateDesign();
    };

    $scope.test = function (e) {
        $scope.FileName_upload = e.filename;
    }

    $scope.init();


    //Pdf Express


    $scope.hierarchychange = function () {

        if ($scope.hierarchy == "No") {
            $scope.partialPDF = false;
            $rootScope.hierarchy_Values = "No";
        } else if ($scope.hierarchy == "Yes") {
            $scope.partialPDF = true;
            $rootScope.hierarchy_Values = "Yes";
        }
    }

    //--------------For select all check box cateogires --------------------


    $scope.CheckOrUncheck1 = function () {


        if (chkEnabled1.checked == true) {
            $scope.CheckOrUncheckBox1 = true;
            $scope.SelectedcategoryIdexp = "All";
        }
        else {
            $scope.CheckOrUncheckBox1 = false;
            $scope.SelectedcategoryIdexp = "";
        }

        if ($scope.CheckOrUncheckBox1) {
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"]').prop('checked', true).trigger("change");
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"]').prop('disabled', true).trigger("change");
            $scope.SelectedcategoryIdexp = "All";
            $scope.CheckOrUncheckBox1 = true;
        }
        else {
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"]').prop('checked', false).trigger("change");
            $('#leftNavTreeViewKendoNavigator input[type="checkbox"]').prop('disabled', false).trigger("change");
            $scope.CheckOrUncheckBox1 = false;
        }
    };

    //-----------------------------


    //--------------For select all check box Families --------------------



    $scope.CheckOrUncheck1fam = function (e) {

        $scope.CheckedStatus = e.CheckOrUncheckBox2;
        $scope.selectallcheckbox = "Checkedall";
        if (chkEnabled2.checked == true) {
            $scope.CheckOrUncheckBox2 = true;
            //$scope.SelectedcategoryIdexp = "All";
        } else {
            $scope.CheckOrUncheckBox2 = false;
            //$scope.SelectedcategoryIdexp = "";
        }
        if ($scope.CheckOrUncheckBox2) {
            $('#leftNavTreeViewKendoNavigatorfam input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox2 = true;

        } else {
            $('#leftNavTreeViewKendoNavigatorfam input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox2 = false;
        }

    };


    $scope.ExporttreeData = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false, autoBind: false,
        transport: {
            read: function (options) {
                dataFactory.GetCategoryDetails($scope.SelectedCatalogId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: ""
            }
        }
    });


    $scope.ExporttreeDatafam = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        autoBind: false,
        transport: {

            read: function (options) {

                if ($scope.checkeditems != "" && $scope.checkeditems != "undefined") {

                    dataFactory.getCategoryFamilyExport($scope.checkeditems, $scope.SelectedCatalogId).success(function (response) {
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });
                }

            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: ""
            }
        }
    });

    $("#leftNavTreeViewKendoNavigator").on("change", function () {

        var treeView = $("#treeViewDiv").data("kendoTreeView");
        var len = $('#treeViewDiv').find('input:checkbox:checked').length;
        if (len > 0) {
            $("#nextBtn").removeAttr('disabled');
        } else {
            $("#nextBtn").attr('disabled', 'disabled');
        }


        //$("#treeViewDiv").find("input:checkbox:not(:checked)").each(function()
        //{
        //  $("#nextBtn").attr('disabled', 'disabled');
        //});

    });


    $scope.ExporttreeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {

            //console.log(e);
            if (!e.node) {
                $scope.attachChangeEvent();
            }
        }
    };

    $scope.ExporttreeOptionsCatalogfam = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {

            //console.log(e);
            if (!e.node) {
                $scope.attachChangeEventFamily();
            }
        }
    };

    $scope.attachChangeEvent = function () {
        $scope.Categorycheckeditems = "";
        var dataSource = $scope.ExporttreeData;
        $scope.checkeditems = '';
        $scope.SelectedcategoryIdexp = "0";
        dataSource.bind("change", function () {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.checkedNodeIds(dataSource.view(), checkedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            //console.log(selectedNodes);
            $scope.checkeditems = checkedNodes.join(",");
            if ($scope.SelectedcategoryIdexp != "All") {
                $scope.SelectedcategoryIdexp = checkedNodes.join(",");

                $scope.Categorycheckeditems = $scope.SelectedcategoryIdexp;
            }
            if ($scope.SelectedcategoryIdexp == "") {
                $scope.SelectedcategoryIdexp = "0";
                $scope.checkeditems = "0";
            }
            $scope.ExporttreeDatafam.read();
        });



    };

    $scope.attachChangeEventFamily = function () {

        var dataSource = $scope.ExporttreeDatafam;
        $scope.familycheckeditems = '';
        dataSource.bind("change", function () {

            var selectedNodes = 0;
            $scope.famarray = [];
            var checkedNodesfam = [];
            $scope.selectallcheckbox != "";

            $scope.checkedNodeIdsfamily(dataSource.view(), checkedNodesfam);
            for (var i = 0; i < checkedNodesfam.length; i++) {
                var nd = checkedNodesfam[i];
                $scope.famarray[i] = checkedNodesfam[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            if ($scope.selectallcheckbox != "Checkedall") {
                chkEnabled2.checked = false;
            }
            //console.log(selectedNodes);

            $scope.familycheckeditems = checkedNodesfam.join(",");
            // $scope.selectedFamilyId = checkedNodesfam.join(",");
        });



    };




    $scope.checkedNodeIdsfamily = function (nodes, checkedNodesfam) {

        var varbool = 0;
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodesfam.push(nodes[i].FAMILY_ID);
                varbool += 1;
            }
            else {
                if (nodes[i].checked == false) {
                    chkEnabled2.checked = false;

                }
                //    if (chkEnabled2.checked == true)
                //        $scope.selectallcheckbox = "";
                //    //    else if (chkEnabled2.checked == false && checkedNodesfam.length == nodes.length)
                //    //        chkEnabled2.checked = true;
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIdsfamily(nodes[i].children.view(), checkedNodesfam);
            }
        }
        if (varbool == nodes.length)
            chkEnabled2.checked = true;

    };

    $scope.checkedNodeIds = function (nodes, checkedNodes) {

        $scope.uncheckednodes = "";
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].CATEGORY_ID);

            }
            else {
                $scope.uncheckednodes = nodes[i].CATEGORY_ID;
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
            if ($scope.uncheckednodes != "") {

                $scope.SelectedcategoryIdexp = "0";

            }
        }
    };


    function onCheck() {
        checkedNodes = [];
        $scope.checkedNodeIdsPDFExpress($scope.PDFExpressTreeData._data, checkedNodes);

        if (checkedNodes.length > 0) {
            message = "IDs of checked nodes: " + checkedNodes.join(",");
        } else {
            message = "No nodes checked.";
        }
        //alert(message);
        //$("#result").html(message);
    }

    $scope.attachChangeEvent = function () {

        var dataSource = $scope.PDFExpressTreeData;
        $scope.checkeditems = '';
        dataSource.bind("change", function (e) {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.checkedNodeIdsPDFExpress(dataSource.view(), checkedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            $scope.checkeditems = checkedNodes.join(",");
        });
    };

    $scope.checkedNodeIdsPDFExpress = function (nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {

                if (nodes[i].CATEGORY_ID.contains("~")) {

                    //checkedNodes.push(nodes[i].parent().parent().CATEGORY_ID + "!" + nodes[i].CATEGORY_ID);
                } else {
                    checkedNodes.push(nodes[i].CATEGORY_ID);
                }
            }
        }
    };

    $scope.PDFExpressTreeData = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false, autoBind: false,
        transport: {
            read: function (options) {
                dataFactory.CategoriesForPDFExpress($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: ""
            }
        }
    });

    $scope.PDFExpressTreeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        }, check: onCheck,
        loadOnDemand: false,
        dataBound: function (e) {
            //console.log(e);
            $scope.checkeditems = '';
            if (!e.node) {
                $scope.attachChangeEvent();
            }
        },
        dataSource: $scope.PDFExpressTreeData
    };

    $scope.CheckOrUncheckBox = false;

    $scope.selectAll = function () {

        if (chkEnabled.checked == true) {
            $scope.CheckOrUncheckBox = true;
        } else {
            $scope.CheckOrUncheckBox = false;
        }
        if ($scope.CheckOrUncheckBox) {
            $('#leftNavTreeViewKendoNavigator1 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox = true;
        } else {
            $('#leftNavTreeViewKendoNavigator1 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox = false;
        }
    };

    $scope.switchMethod = function () {
        window.open("/XpressCatalog/DesignerTemplate", "theFrame");

    }
   

}]);