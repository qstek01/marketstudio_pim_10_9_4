﻿LSApp.controller('XpressDesignPDFCatalogController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $rootScope, $localStorage) {
    $scope.catalogtype = "simple";
    $scope.PDFGetAllCatalogattributesdataSource = new kendo.data.DataSource({
        type: "json",
        filterable: true,
        serverFiltering: false,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetOpenPDFCatalogattributes($scope.SelectedCatalogId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });
    $scope.OpenmainGridOptions = {
        dataSource: $scope.PDFGetAllCatalogattributesdataSource,
        filterable: { mode: "row" },
        columns: [
            { field: "ISAvailable", title: "<input type='checkbox' id='chkSelectAll' ng-click='checkAll($event, this)'/>", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', filterable: false },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px", filterable: true },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                filterable: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderNameall
                // groupHeaderNameall groupHeaderName
            }]
    };


    $scope.group1 = false;
    $scope.group3 = false;
    $scope.group4 = false;
    $scope.group7 = false;
    $scope.group6 = false;
    $scope.group9 = false;
    $scope.group12 = false;
    $scope.group11 = false;
    $scope.group13 = false;
    $scope.group0 = false;
    $scope.group2 = false;

    $scope.checkgroupAll1 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group1 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);

        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll17 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group17 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);

        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll3 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        //tar.dirty = e.currentTarget.checked;

        $scope.group3 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }

        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll4 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group4 = e.currentTarget.checked;
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
    };
    $scope.checkgroupAll7 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group7 = e.currentTarget.checked;
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
    };
    $scope.checkgroupAll6 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group6 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll9 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group9 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll11 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group11 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll12 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group12 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll13 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group13 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll14 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group13 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll15 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group13 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll0 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group0 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }
    };
    $scope.checkgroupAll2 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group2 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };

    $scope.checkgroupAll6 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.Opencatselectgrid.refresh();
        $scope.group16 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }


    };

    function groupHeaderNameall(e) {
        if (e.value === 1) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll1($event," + e.value + ")'/> Item Specifications (" + e.count + " items)";
        }
        else if (e.value === 3) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll3($event," + e.value + ")'/> Item Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 4) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll4($event," + e.value + ")'/> Item Price (" + e.count + " items)";
        } else if (e.value === 7) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll7($event," + e.value + ")'/> Product Description (" + e.count + " items)";
        } else if (e.value === 6) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll6($event," + e.value + ")'/> Item Key (" + e.count + " items)";
        } else if (e.value === 9) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll9($event," + e.value + ")'/> Product Image / Attachment (" + e.count + " items)";
        } else if (e.value === 11) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll11($event," + e.value + ")'/> Product Specifications (" + e.count + " items)";
        }
        else if (e.value === 12) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll12($event," + e.value + ")'/> Product Price (" + e.count + " items)";
        }
        else if (e.value === 13) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll13($event," + e.value + ")'/> Product Key (" + e.count + " items)";
        }
        else if (e.value === 14) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll14($event," + e.value + ")'/> Multiple Table (" + e.count + " items)";
        }
        else if (e.value === 15) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll15($event," + e.value + ")'/>Reference Table(" + e.count + " items)";
        }
        else if (e.value === 0) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll0($event," + e.value + ")'/> Category (" + e.count + " items)";
        }
        else if (e.value === 2) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll2($event," + e.value + ")'/> Product (" + e.count + " items)";
        }
        else if (e.value === 16) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll2($event," + e.value + ")'/> Item Product (" + e.count + " items)";
        }
        else {

            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll($event," + e.value + ")'/> Item Specifications (" + e.count + " items)";
        }

    }
    $scope.updateSelection = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);

    };

    $scope.checkAll = function (e) {
        var state = e.currentTarget.checked;
        //  var grid = $scope.catselectgrid;
        $.each($scope.Opencatselectgrid.dataSource._data, function () {
            if (this['ISAvailable'] != state)
                this.dirty = true;
            this['ISAvailable'] = state;
        });
        //$(".chkbxforallattributes").each(function () {
        //    $(this).prop("checked", true);
        //});
        $scope.Opencatselectgrid.refresh();
    };

    $scope.PDFprodfamilyattrdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetOpenPDFSelectedattributes($scope.SelectedCatalogId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });
    $scope.OpenselectedGridOptions = {
        dataSource: $scope.PDFprodfamilyattrdataSource,
        selectable: "multiple",
        columns: [
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderName
            }]
    };

    function groupHeaderName(e) {
        if (e.value === 1) {
            return "Item Specifications (" + e.count + " items)";
        }
        else if (e.value === 3) {
            return "Item Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 4) {
            return "Item Price (" + e.count + " items)";
        } else if (e.value === 6) {
            return "Item Key (" + e.count + " items)";
        } else if (e.value === 7) {
            return "Product Description (" + e.count + " items)";
        } else if (e.value === 9) {
            return "Product Image / Attachment (" + e.count + " items)";
        } else if (e.value === 11) {
            return "Product Specifications (" + e.count + " items)";
        }
        else if (e.value === 12) {
            return "Product Price (" + e.count + " items)";
        }
        else if (e.value === 13) {
            return "Product Key (" + e.count + " items)";
        }
        else if (e.value === 0) {
            return "Category (" + e.count + " items)";
        }
        else if (e.value === 2) {
            return "Product (" + e.count + " items)";
        }
        else if (e.value === 14) {
            return "Multiple Table(" + e.count + "items)";

        }
        else if (e.value === 15) {
            return "Reference Table(" + e.count + "items)";

        }
        else if (e.value === 16) {
            return "Item Product(" + e.count + "items)";

        }
        else if (e.value === 17) {
            return "Item Specification(" + e.count + "items)";

        }
        else {
            return "Item Specifications(" + e.count + " items)";
        }
    }

    $scope.updateSelection1 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };

    $scope.OpenSavePublishAttributes = function () {
        var removeitems = [];
        for (var i = 0; i < $scope.PDFGetAllCatalogattributesdataSource._data.length; i++) {
            var entity = $scope.PDFGetAllCatalogattributesdataSource._data[i];
            if (entity.ISAvailable) {
                $scope.PDFprodfamilyattrdataSource.add(entity);
                removeitems.push(entity);
            }
        }
        for (var j = 0; j < removeitems.length; j++) {
            $scope.PDFGetAllCatalogattributesdataSource.remove(removeitems[j]);
        }
    };

    $scope.OpenDeletePublishAttributes = function () {

        var g = $scope.Openprodselectgrid.select();

        var removeitems = [];
        var defaultvalues = ["CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_SHORT", "CATEGORY_NAME", "FAMILY_ID", "FAMILY_NAME", "SUBFAMILY_ID", "SUBFAMILY_NAME", "PRODUCT_ID"];
        g.each(function (index, row) {
            var selectedItem = $scope.Openprodselectgrid.dataItem(row);
            if (defaultvalues.indexOf(selectedItem.ATTRIBUTE_NAME) >= 0) {

            }
            else {
                removeitems.push(selectedItem);
                $scope.PDFGetAllCatalogattributesdataSource.add(selectedItem);
            }
        });
        for (var i = 0; i < removeitems.length; i++) {
            $scope.PDFprodfamilyattrdataSource.remove(removeitems[i]);
        }

    };
    //Cancel Function
    $scope.exitPDFXpressCatalog = function () {
        window.location.reload("../App/AdminDashboard/");
    };

    $scope.GetOpenPDFSummary = function () {

        $('#pdfselection').hide();
        $('#pdfSummary').show();
        dataFactory.OpenUpdateXpressProject($scope.PDFprodfamilyattrdataSource._data).success(function () {
            dataFactory.GetOpenPDFSummary().success(function (response) {
                if (response != null) {
                    $('#BOTTOM_MARGIN').text(response[0].BOTTOM_MARGIN);
                    $('#CATALOG_ID').text(response[0].CATALOG_ID);
                    $('#CATALOG_NAME').text(response[0].CATALOG_NAME);
                    $('#COMMENT').text(response[0].COMMENT);
                    $('#CONTENT_PAGE').text(response[0].CONTENT_PAGE);
                    $('#CONTENT_PAGE_WITH_CATEGORY_FAMILY').text(response[0].CONTENT_PAGE_WITH_CATEGORY_FAMILY);
                    $('#COVER_PAGE').text(response[0].COVER_PAGE);
                    $('#CUSTOM_ATTRIBUTE').text(response[0].CUSTOM_ATTRIBUTE);
                    $('#INDEX_PAGE').text(response[0].INDEX_PAGE);
                    $('#INDEX_PAGE_WITH_CATALOG_ITEM_NO').text(response[0].INDEX_PAGE_WITH_CATALOG_ITEM_NO);
                    $('#INDEXPAGE_COLUMNS').text(response[0].INDEXPAGE_COLUMNS);
                    $('#LEFT_MARGIN').text(response[0].LEFT_MARGIN);
                    $('#MULTIPLE_TABLES').text(response[0].MULTIPLE_TABLES);
                    $('#ORIENTATION').text(response[0].ORIENTATION);
                    $('#PAGE_COLUMN_GAP').text(response[0].PAGE_COLUMN_GAP);
                    $('#PAGE_COLUMN_WIDTH').text(response[0].PAGE_COLUMN_WIDTH);
                    $('#PAGE_COLUMNS').text(response[0].PAGE_COLUMNS);
                    $('#PAPER_HEIGHT').text(response[0].PAPER_HEIGHT);
                    $('#PAPER_SIZE').text(response[0].PAPER_SIZE);
                    $('#PAPER_WIDTH').text(response[0].PAPER_WIDTH);
                    $('#PROJECT_ID').text(response[0].PROJECT_ID);
                    $('#PROJECT_NAME').text(response[0].PROJECT_NAME);
                    $('#PUBLICATION_TYPE').text(response[0].PUBLICATION_TYPE);
                    $('#REFERENCE_TABLES').text(response[0].REFERENCE_TABLES);
                    $('#RIGHT_MARGIN').text(response[0].RIGHT_MARGIN);
                    $('#SEGMENT_PER_HEIGHT').text(response[0].SEGMENT_PER_HEIGHT);
                    $('#SEGMENT_PER_WIDTH').text(response[0].SEGMENT_PER_WIDTH);
                    $('#SYSTEM_ATTRIBUTE').text(response[0].SYSTEM_ATTRIBUTE);
                    $('#TOP_MARGIN').text(response[0].TOP_MARGIN);
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Response failed, missing ProjectID.',
                        type: "error"
                    });
                }
            }).error(function (error) {
                options.error(error);
            });
        }).error(function (error) {
            options.error(error);
        });





    };
    $scope.simplePDFTemplate = function () {
        $('#pdfselection').hide();
        $('#pdfSummary').hide();
        $scope.winPDFCatalog.close();
        window.open("../xpressSimpleCatalog/OpenDesignerTemplate", "_blank")
       // $("#opendesigner").show();

    };
}]);