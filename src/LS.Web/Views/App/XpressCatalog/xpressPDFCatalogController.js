﻿




LSApp.controller('xpressPDFCatalogController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $rootScope, $localStorage) {
    $scope.catalogtype = 'simple';
    $scope.changeName = 'A4';
    $scope.widthValue = '8.27';
    $scope.heightValue = '11.69';
    $scope.PublicationType = true;
    $scope.PageSetup = true;
    $scope.PageSetupAdvanced = true;
    $scope.DataFields = true;
    $scope.HierarchyOption = true;
    $scope.SelectGroup = true;
    $scope.Summary = true;
    $("#PublicationType").addClass("leftMenuactive");
    $("#PageSetup").removeClass("leftMenuactive");
    $("#PageSetupAdvanced").removeClass("leftMenuactive");
    $("#DataFields").removeClass("leftMenuactive");
    $("#HierarchyOption").removeClass("leftMenuactive");
    $("#SelectGroup").removeClass("leftMenuactive");
    $("#Summary").removeClass("leftMenuactive");



    $scope.catalogDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetCatalogDetails($scope.SelectedItem, $scope.getCustomerID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.catalogChange = function (e) {
        if (e.sender.value() == "") {
            $('#btnNext').attr("disabled", true);
        }
        if (e.sender.value() != "") {
            $('#btnNext').attr("disabled", false);
            $scope.SelectedCatalogId = e.sender.value();
            $scope.SelectedCatalogName = e.sender.text();
            $localStorage.getCatalogID = $scope.SelectedCatalogId;
            $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;

            $http.get("../XpressCatalog/getCatalogID?catalogID=" + $scope.SelectedCatalogId).then(function () { });
            $http.get("../xpressSimpleCatalog/getCurrentCatalogID?catalogID=" + $scope.SelectedCatalogId).then(function () { });
        }

    };

    //Page Size 
    $scope.pagesize = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetPaperSize().success(function (response) {
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.papersizeChange = function (e) {
        $scope.changeName = e.sender.text();
        $scope.pagewidth.read();
    };

    $scope.pagewidth = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetPaperWidth($scope.changeName).success(function (response) {
                    options.success(response);
                    if (response.length > 0) {
                        //$scope.widthValue = response[0].width / 100;
                        //$scope.heightValue = response[0].height / 100;
                        $scope.widthValue = response[0].Width / 100;
                        $scope.heightValue = response[0].Height / 100;
                    } else {
                        $scope.changeName = 'A4';
                        $scope.widthValue = '8.27';
                        $scope.heightValue = '11.69';
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.paperwidthChange = function (e) {
        $scope.widthValue = e.sender.text();
    };

    $scope.paperheightChange = function (e) {
        $scope.heightValue = e.sender.text();
    };

    $scope.displayProduct_ = false;
    $scope.displayProductdetails = [
          { type: "16", value: "PRODUCT_ID" },
   { type: "17", value: "ATTRIBUTE_NAME" },
    { type: "17", value: "STRING_VALUE" },
    { type: "17", value: "NUMERIC_VALUE" }];

    function disprod() {
        $scope.countdisplayProduct = 0;
        var countdisProduct = 0;
        for (var i = 0; i < $scope.GetAllCatalogattributesdataSource._data.length; i++) {
            var entity = $scope.GetAllCatalogattributesdataSource._data[i];
            for (var j = 0; j < $scope.displayProductdetails.length; j++) {
                var prod = $scope.displayProductdetails[j].value;
                var type = $scope.displayProductdetails[j].type;
                var type1 = entity.ATTRIBUTE_TYPE;
                if (entity.ATTRIBUTE_NAME == prod && type1 == parseInt(type)) {
                    countdisProduct = countdisProduct + 1;
                    $scope.prodfamilyattrdataSource.remove(entity);
                    $scope.prodfamilyattrdataSource.add(entity);


                } else if (entity.ATTRIBUTE_NAME != "STRING_VALUE" && entity.ATTRIBUTE_NAME != "ATTRIBUTE_NAME" && entity.ATTRIBUTE_NAME != "CATEGORY_Name" && entity.ATTRIBUTE_NAME != "CATEGORY_ID" && entity.ATTRIBUTE_NAME != "FAMILY_NAME" && entity.ATTRIBUTE_NAME != "PRODUCT_ID" && (entity.ATTRIBUTE_TYPE == 16 || entity.ATTRIBUTE_TYPE == 17)) {
                    $scope.prodfamilyattrdataSource.remove(entity);
                }
            }
        }
        $scope.countdisplayProduct = countdisProduct;
    }
    function disprodLeft() {
        $scope.countdisplayProduct = 0;
        var countdisProduct = 0;
        var removeProduct = [];
        //$scope.GetAllCatalogattributesdataSource.read();
        for (var i = 0; i < $scope.prodfamilyattrdataSource._data.length; i++) {
            var entity = $scope.prodfamilyattrdataSource._data[i];
            for (var j = 0; j < $scope.displayProductdetails.length; j++) {
                var prod = $scope.displayProductdetails[j].value;
                var type = $scope.displayProductdetails[j].type;
                var type1 = entity.ATTRIBUTE_TYPE;
                if (entity.ATTRIBUTE_NAME == prod && type1 == parseInt(type)) {
                    countdisProduct = countdisProduct + 1;
                    //$scope.prodfamilyattrdataSource.remove(entity);
                    removeProduct.push(entity);


                }
            }
        }
        return removeProduct;
    }
    $scope.displayProduct = function (e) {
        if (e == true) {
            $scope.displayProduct_ = e;
            $scope.catalogAttr = $scope.GetAllCatalogattributesdataSource;
            $scope.prodFam = $scope.prodfamilyattrdataSource;
            var prodFam111 = $scope.prodfamilyattrdataSource;
            var removeitems = [];
            var removeitems11 = [];
            var count = $scope.prodFam._data.length;
            for (var ii = 0; ii < count; ii++) {
                var aa = $scope.prodfamilyattrdataSource._data[ii];
                //if (aa.ATTRIBUTE_NAME != "STRING_VALUE" && aa.ATTRIBUTE_NAME != "ATTRIBUTE_NAME" && aa.ATTRIBUTE_NAME != "CATEGORY_NAME" && aa.ATTRIBUTE_NAME != "CATEGORY_ID" && aa.ATTRIBUTE_NAME != "FAMILY_NAME" && aa.ATTRIBUTE_NAME != "PRODUCT_ID") {
                //    removeitems11.push(aa);
                //}
                //else 
                if (aa.ATTRIBUTE_TYPE == 16 || aa.ATTRIBUTE_TYPE == 17) {
                    removeitems11.push(aa);
                }
            }

            $scope.countdisplayProduct = 0;
            disprod();

            var disdata = disprodLeft();

            for (i = 0; i < removeitems11.length; i++) {
                $scope.prodfamilyattrdataSource.remove(removeitems11[i]);
            }
            for (i = 0; i < disdata.length; i++) {
                $scope.prodfamilyattrdataSource.remove(disdata[i]);
                $scope.prodfamilyattrdataSource.add(disdata[i]);
            }
            for (var jj1 = 0; jj1 < $scope.GetAllCatalogattributesdataSource._data.length; jj1++) {
                var entity1 = $scope.GetAllCatalogattributesdataSource._data[jj1];
                for (i = 0; i < $scope.prodfamilyattrdataSource._data.length; i++) {
                    var prodEntity = $scope.prodfamilyattrdataSource._data[i];
                    if (entity1.ATTRIBUTE_TYPE == prodEntity.Attribute_type && entity1.ATTRIBUTE_NAME == prodEntity.ATTRIBUTE_NAME) {
                        removeitems.push(entity1);
                    } else if (entity1.ATTRIBUTE_TYPE == 16 || entity1.ATTRIBUTE_TYPE == 17) {
                        removeitems.push(entity1);
                    }
                }
            }
            for (var jj = 0; jj < removeitems.length; jj++) {
                $scope.GetAllCatalogattributesdataSource.remove(removeitems[jj]);
            }
        } else {
            $scope.displayProduct_ = e;
            var additems = [];
            for (var jj1 = 0; jj1 < $scope.GetAllCatalogattributesdataSource._destroyed.length; jj1++) {
                var entity1 = $scope.GetAllCatalogattributesdataSource._destroyed[jj1];
                if (entity1.ATTRIBUTE_TYPE == 17 || entity1.ATTRIBUTE_TYPE == 16) {
                    additems.push(entity1);
                }
            }
            for (j = 0; j < additems.length; j++) {
                $scope.GetAllCatalogattributesdataSource.add(additems[j]);
            }
            for (i = 0; i < additems.length; i++) {
                $scope.prodfamilyattrdataSource.remove(additems[i]);
            }
            //$scope.GetAllCatalogattributesdataSource.read();
            //$scope.prodfamilyattrdataSource.read();
            //for (var iii = 0; iii < $scope.catalogAttr._data.length; iii++) {
            //    var dd = $scope.catalogAttr._data[iii];
            //    $scope.GetAllCatalogattributesdataSource = $scope.catalogAttr._data;
            ////}
            //for (var jjj = 0; jjj < $scope.prodFam._data.length; jjj++) {
            //    var gg = $scope.prodFam._data[jjj];
            //    $scope.prodfamilyattrdataSource.add(gg);
            //}

        }

    };
    //load values for page setup
    var datasegwdth = [{ type: "1", value: "1" },
           { type: "2", value: "2" },
           { type: "3", value: "3" },
           { type: "4", value: "4" },
    { type: "5", value: "5" }];

    $("#segwidth").kendoDropDownList({
        dataTextField: "type",
        dataValueField: "value",
        dataSource: datasegwdth,
        index: 0
    });
    var datasegheight = [{ type: "1", value: "1" },
          { type: "2", value: "2" },
          { type: "3", value: "3" },
          { type: "4", value: "4" },
   { type: "5", value: "5" }];

    $("#segheight").kendoDropDownList({
        dataTextField: "type",
        dataValueField: "value",
        dataSource: datasegheight,
        index: 0
    });

    var datacols = [{ type: "1", value: "1" },
         { type: "2", value: "2" },
         { type: "3", value: "3" },
         { type: "4", value: "4" },
         { type: "5", value: "5" }];

    $("#cols").kendoDropDownList({
        dataTextField: "type",
        dataValueField: "value",
        dataSource: datacols,
        index: 0
    });
    var dataindexcols = [{ type: "1", value: "1" },
        { type: "2", value: "2" },
        { type: "3", value: "3" },
        { type: "4", value: "4" },
        { type: "5", value: "5" },
    { type: "6", value: "6" },
    { type: "7", value: "7" },
    { type: "8", value: "8" }];

    $("#indexcols").kendoDropDownList({
        dataTextField: "type",
        dataValueField: "value",
        dataSource: dataindexcols,
        index: 0
    });
    //New Project
    $scope.NewProject = {
        catalogID: $scope.SelectedCatalogId,
        projectName: '',
        projectType: $("#projecttype").val(),
        comments: ''
    };

    $scope.Project = {
        PROJECT_ID: '',
        PROJECT_NAME: '',
        PROJECT_TYPE: '',
        COMMENTS: '',
        CATALOG_NAME: ''
    };
    //Page advance Options
    $scope.Footer = '';
    $scope.pageadvance = {
        Header: $scope.SelectedCatalogName,
        Footer: $scope.Footer
    };

    $scope.newValue = function (value) {
        $scope.catalogtype = value;
    };

    $scope.submitForm = function () {
        $scope.catalogtype;

    };
    $scope.pagesetup = function () {
        $scope.orientn;
    };
    $scope.orientn = 'Portrait';
    $scope.nValue = function (value) {
        $scope.orientn = value;
    };

    $scope.coverpage = 'Yes';
    $scope.tocpage = 'Yes';
    $scope.cattocpage = 'Yes';
    $scope.gindexpage = 'Yes';
    $scope.catindexpage = 'Yes';
    $scope.advanceOption = function () {

        $scope.coverpage;
        $scope.tocpage;
        $scope.cattocpage;
        $scope.gindexpage;
        $scope.catindexpage;
    };

    $scope.Cover = function (value) {

        $scope.coverpage = value;
    };

    $scope.Toc = function (value) {

        $scope.tocpage = value;
    };

    $scope.Cattoc = function (value) {

        $scope.cattocpage = value;
    };

    $scope.Gindex = function (value) {

        $scope.gindexpage = value;
    };

    $scope.Catindex = function (value) {

        $scope.catindexpage = value;
    };
    //Grouping 
    $scope.group = 'Create groups';
    $scope.Group = function (value) {
        $scope.group = value;
    };

    //Next &back Button Functions
    $scope.pdfPageSetup = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").addClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").show();
        $("#pdfAdvanceOptions").hide();
        //$('.nav-tabs > .active').next('li').find('a').trigger('click');
    };

    $scope.pdfAdvanceOptions = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").addClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").show();
    };

    $scope.backpdfPageSetup = function () {
        $("#PublicationType").addClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").show();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
    };

    $scope.backpdfAdvanceOptions = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").addClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").show();
        $("#pdfAdvanceOptions").hide();
    };

    $scope.pdfselection = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").addClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $scope.GetAllCatalogattributesdataSource.read();
        $scope.prodfamilyattrdataSource.read();
        $('#pdfselection').show();
        //  alert($scope.catalogtype);
        $http.get("../XpressCatalog/getProjectID?catalogID=" + $scope.SelectedItem + "&&projectName=" + $('#txtprjnm').val() + "&&projectType=" + $("#projecttype").val() + "&&comments=" + $("#txtprjcmnts").val()).
       then(function (objPrjName) {
           $scope.Project = objPrjName.data;
           $('#txtprjid').val($scope.Project.PROJECT_ID);
           $("#txtprjname").val($scope.Project.PROJECT_NAME);
           $('#txtprjtype').val($scope.Project.PROJECT_TYPE);
           $("#txtprjcmmts").val($scope.Project.COMMENTS);
           $scope.SelectedCatalogName = $scope.Project.CATALOG_NAME;
       });
    };
    $scope.backDNTGroup = function () {
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').hide();
        $('#pdfgroup').show();
        $('#pdfSummary').hide();
    };
    $scope.backpdfselection = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").addClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").show();
        $('#pdfselection').hide();
    };

    $scope.UpdatePrjtblegrupsetup = function () {

        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").addClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        if ($rootScope.hierarchy == "No") {
            $scope.familycheckeditems = "ALL"
            $scope.Categorycheckeditems = "ALL"
        }
        else {
            if (($scope.familycheckeditems == "undefined" || $scope.familycheckeditems == "") && ($scope.Categorycheckeditems == "undefined" || $scope.Categorycheckeditems == "")) {
                $scope.familycheckeditems = "ALL"
                $scope.Categorycheckeditems = "ALL"
            }

        }



        dataFactory.UpdateXpressProject($scope.Project.PROJECT_ID, $scope.Project.PROJECT_NAME, $scope.Categorycheckeditems, $scope.familycheckeditems,
        $scope.Project.COMMENTS, $scope.SelectedCatalogId, $scope.SelectedCatalogName,
        $scope.catalogtype, $scope.changeName, $scope.widthValue, $scope.heightValue, $('#segwidth').val(), $('#segheight').val(), $('#txtleftmrgn').val()
        , $('#txtrightmrgn').val(), $('#txttopmrgn').val(), $('#txtbottommrgn').val(), $('#cols').val(), $('#txtcolswidth').val(), $('#txtcolsgap').val(), $scope.orientn
        , $scope.coverpage, $scope.tocpage, $scope.cattocpage, $scope.gindexpage, $scope.catindexpage, $('#indexcols').val(), $scope.prodfamilyattrdataSource._data).success(function () {
        }).error(function (error) {
            options.error(error);
        });

        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').hide();
        $('#pdfgroup').show();
        //  $('#showgroup').show();
        //dataFactory.GetPDFSummary($scope.Project.PROJECT_ID).success(function (response) {
        //    //$scope.sumbtmMrgn = response[0].BOTTOM_MARGIN;
        //    if (response != null) {
        //        $('#BOTTOM_MARGIN').text(response[0].BOTTOM_MARGIN);
        //        $('#CATALOG_ID').text(response[0].CATALOG_ID);
        //        $('#CATALOG_NAME').text(response[0].CATALOG_NAME);
        //        $('#COMMENT').text(response[0].COMMENT);
        //        $('#CONTENT_PAGE').text(response[0].CONTENT_PAGE);
        //        $('#CONTENT_PAGE_WITH_CATEGORY_FAMILY').text(response[0].CONTENT_PAGE_WITH_CATEGORY_FAMILY);
        //        $('#COVER_PAGE').text(response[0].COVER_PAGE);
        //        $('#CUSTOM_ATTRIBUTE').text(response[0].CUSTOM_ATTRIBUTE);
        //        $('#INDEX_PAGE').text(response[0].INDEX_PAGE);
        //        $('#INDEX_PAGE_WITH_CATALOG_ITEM_NO').text(response[0].INDEX_PAGE_WITH_CATALOG_ITEM_NO);
        //        $('#INDEXPAGE_COLUMNS').text(response[0].INDEXPAGE_COLUMNS);
        //        $('#LEFT_MARGIN').text(response[0].LEFT_MARGIN);
        //        $('#MULTIPLE_TABLES').text(response[0].MULTIPLE_TABLES);
        //        $('#ORIENTATION').text(response[0].ORIENTATION);
        //        $('#PAGE_COLUMN_GAP').text(response[0].PAGE_COLUMN_GAP);
        //        $('#PAGE_COLUMN_WIDTH').text(response[0].PAGE_COLUMN_WIDTH);
        //        $('#PAGE_COLUMNS').text(response[0].PAGE_COLUMNS);
        //        $('#PAPER_HEIGHT').text(response[0].PAPER_HEIGHT);
        //        $('#PAPER_SIZE').text(response[0].PAPER_SIZE);
        //        $('#PAPER_WIDTH').text(response[0].PAPER_WIDTH);
        //        $('#PROJECT_ID').text(response[0].PROJECT_ID);
        //        $('#PROJECT_NAME').text(response[0].PROJECT_NAME);
        //        $('#PUBLICATION_TYPE').text(response[0].PUBLICATION_TYPE);
        //        $('#REFERENCE_TABLES').text(response[0].REFERENCE_TABLES);
        //        $('#RIGHT_MARGIN').text(response[0].RIGHT_MARGIN);
        //        $('#SEGMENT_PER_HEIGHT').text(response[0].SEGMENT_PER_HEIGHT);
        //        $('#SEGMENT_PER_WIDTH').text(response[0].SEGMENT_PER_WIDTH);
        //        $('#SYSTEM_ATTRIBUTE').text(response[0].SYSTEM_ATTRIBUTE);
        //        $('#TOP_MARGIN').text(response[0].TOP_MARGIN);
        //    } else {
        //        alert('Response Failed,Missing ProjectID');

        //    }
        //}).error(function (error) {
        //    options.error(error);


        //});

    };

    $scope.UpdateProjectHierarchy = function () {


        if ($rootScope.hierarchy == "No") {
            $scope.familycheckeditems = "ALL"
            $scope.Categorycheckeditems = "ALL"
        }
        else {
            if (($scope.familycheckeditems == "undefined" || $scope.familycheckeditems == "") && ($scope.Categorycheckeditems == "undefined" || $scope.Categorycheckeditems == "")) {
                $scope.familycheckeditems = "ALL"
                $scope.Categorycheckeditems = "ALL"
            }

        }

        dataFactory.UpdateXpressProject($scope.Project.PROJECT_ID, $scope.Project.PROJECT_NAME, $scope.Categorycheckeditems, $scope.familycheckeditems,
                $scope.Project.COMMENTS, $scope.SelectedCatalogId, $scope.SelectedCatalogName,
                 $scope.catalogtype, $scope.changeName, $scope.widthValue, $scope.heightValue, $('#segwidth').val(), $('#segheight').val(), $('#txtleftmrgn').val()
                , $('#txtrightmrgn').val(), $('#txttopmrgn').val(), $('#txtbottommrgn').val(), $('#cols').val(), $('#txtcolswidth').val(), $('#txtcolsgap').val(), $scope.orientn
                , $scope.coverpage, $scope.tocpage, $scope.cattocpage, $scope.gindexpage, $scope.catindexpage, $('#indexcols').val(), $scope.prodfamilyattrdataSource._data).success(function (response) {
                    if (response != null) {
                        dataFactory.GetPDFSummary($scope.Project.PROJECT_ID).success(function (response) {
                            //$scope.sumbtmMrgn = response[0].BOTTOM_MARGIN;
                            if (response != null) {
                                $('#BOTTOM_MARGIN').text(response[0].BOTTOM_MARGIN);
                                $('#CATALOG_ID').text(response[0].CATALOG_ID);
                                $('#CATALOG_NAME').text(response[0].CATALOG_NAME);
                                $('#COMMENT').text(response[0].COMMENT);
                                $('#CONTENT_PAGE').text(response[0].CONTENT_PAGE);
                                $('#CONTENT_PAGE_WITH_CATEGORY_FAMILY').text(response[0].CONTENT_PAGE_WITH_CATEGORY_FAMILY);
                                $('#COVER_PAGE').text(response[0].COVER_PAGE);
                                $('#CUSTOM_ATTRIBUTE').text(response[0].CUSTOM_ATTRIBUTE);
                                $('#INDEX_PAGE').text(response[0].INDEX_PAGE);
                                $('#INDEX_PAGE_WITH_CATALOG_ITEM_NO').text(response[0].INDEX_PAGE_WITH_CATALOG_ITEM_NO);
                                $('#INDEXPAGE_COLUMNS').text(response[0].INDEXPAGE_COLUMNS);
                                $('#LEFT_MARGIN').text(response[0].LEFT_MARGIN);
                                $('#MULTIPLE_TABLES').text(response[0].MULTIPLE_TABLES);
                                $('#ORIENTATION').text(response[0].ORIENTATION);
                                $('#PAGE_COLUMN_GAP').text(response[0].PAGE_COLUMN_GAP);
                                $('#PAGE_COLUMN_WIDTH').text(response[0].PAGE_COLUMN_WIDTH);
                                $('#PAGE_COLUMNS').text(response[0].PAGE_COLUMNS);
                                $('#PAPER_HEIGHT').text(response[0].PAPER_HEIGHT);
                                $('#PAPER_SIZE').text(response[0].PAPER_SIZE);
                                $('#PAPER_WIDTH').text(response[0].PAPER_WIDTH);
                                $('#PROJECT_ID').text(response[0].PROJECT_ID);
                                $('#PROJECT_NAME').text(response[0].PROJECT_NAME);
                                $('#PUBLICATION_TYPE').text(response[0].PUBLICATION_TYPE);
                                $('#REFERENCE_TABLES').text(response[0].REFERENCE_TABLES);
                                $('#RIGHT_MARGIN').text(response[0].RIGHT_MARGIN);
                                $('#SEGMENT_PER_HEIGHT').text(response[0].SEGMENT_PER_HEIGHT);
                                $('#SEGMENT_PER_WIDTH').text(response[0].SEGMENT_PER_WIDTH);
                                $('#SYSTEM_ATTRIBUTE').text(response[0].SYSTEM_ATTRIBUTE);
                                $('#TOP_MARGIN').text(response[0].TOP_MARGIN);
                            } else {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Response failed, missing ProjectID.',
                                    type: "error"
                                });


                            }
                        }).error(function (error) {
                            options.error(error);
                        });
                    }

                }).error(function (error) {
                    options.error(error);
                });

        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').hide();
        $('#pdfSummary').show();
    };

    $scope.backpdfgroup = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").addClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').show();
        $('#pdfgroup').hide();
        $('#pdfgroupingattributes').hide();
        $('#pdfSummary').hide();
    };

    $scope.pdfgroupingattributes = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").addClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').hide();
        $('#pdfgroup').hide();
        if ($scope.group == 'Create groups') {
            $scope.GetAllCatalogObjects.read();
        }
        dataFactory.GetPDFSummary($scope.Project.PROJECT_ID).success(function (response) {
            if (response != null) {
                $('#BOTTOM_MARGIN').text(response[0].BOTTOM_MARGIN);
                $('#CATALOG_ID').text(response[0].CATALOG_ID);
                $('#CATALOG_NAME').text(response[0].CATALOG_NAME);
                $('#COMMENT').text(response[0].COMMENT);
                $('#CONTENT_PAGE').text(response[0].CONTENT_PAGE);
                $('#CONTENT_PAGE_WITH_CATEGORY_FAMILY').text(response[0].CONTENT_PAGE_WITH_CATEGORY_FAMILY);
                $('#COVER_PAGE').text(response[0].COVER_PAGE);
                $('#CUSTOM_ATTRIBUTE').text(response[0].CUSTOM_ATTRIBUTE);
                $('#INDEX_PAGE').text(response[0].INDEX_PAGE);
                $('#INDEX_PAGE_WITH_CATALOG_ITEM_NO').text(response[0].INDEX_PAGE_WITH_CATALOG_ITEM_NO);
                $('#INDEXPAGE_COLUMNS').text(response[0].INDEXPAGE_COLUMNS);
                $('#LEFT_MARGIN').text(response[0].LEFT_MARGIN);
                $('#MULTIPLE_TABLES').text(response[0].MULTIPLE_TABLES);
                $('#ORIENTATION').text(response[0].ORIENTATION);
                $('#PAGE_COLUMN_GAP').text(response[0].PAGE_COLUMN_GAP);
                $('#PAGE_COLUMN_WIDTH').text(response[0].PAGE_COLUMN_WIDTH);
                $('#PAGE_COLUMNS').text(response[0].PAGE_COLUMNS);
                $('#PAPER_HEIGHT').text(response[0].PAPER_HEIGHT);
                $('#PAPER_SIZE').text(response[0].PAPER_SIZE);
                $('#PAPER_WIDTH').text(response[0].PAPER_WIDTH);
                $('#PROJECT_ID').text(response[0].PROJECT_ID);
                $('#PROJECT_NAME').text(response[0].PROJECT_NAME);
                $('#PUBLICATION_TYPE').text(response[0].PUBLICATION_TYPE);
                $('#REFERENCE_TABLES').text(response[0].REFERENCE_TABLES);
                $('#RIGHT_MARGIN').text(response[0].RIGHT_MARGIN);
                $('#SEGMENT_PER_HEIGHT').text(response[0].SEGMENT_PER_HEIGHT);
                $('#SEGMENT_PER_WIDTH').text(response[0].SEGMENT_PER_WIDTH);
                $('#SYSTEM_ATTRIBUTE').text(response[0].SYSTEM_ATTRIBUTE);
                $('#TOP_MARGIN').text(response[0].TOP_MARGIN);
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Response failed, missing ProjectID.',
                    type: "error"
                });

            }
        }).error(function (error) {
            options.error(error);
        });
        $('#pdfgroupingattributes').show();
    };

    $scope.backpdfgroupingattributes = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").addClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').hide();
        $('#pdfgroup').show();
        $('#pdfgroupingattributes').hide();
    };

    $scope.pdfSummary = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").removeClass("leftMenuactive");
        $("#Summary").addClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').hide();
        $('#pdfgroup').hide();
        $('#pdfgroupingattributes').hide();
        if ($scope.catalogtype == "simple") {
            $('#showgroup').show();
            dataFactory.UpdateGroupByFields($scope.Project.PROJECT_ID, $scope.prodCatalogObjects._data).success(function () {
                //  alert("Success");
            }).error(function (error) {
                options.error(error);
            });
        } else {
            $('#showgroup').hide();
        }
        dataFactory.GetPDFSummary($scope.Project.PROJECT_ID).success(function (response) {
            if (response != null) {
                $('#BOTTOM_MARGIN').text(response[0].BOTTOM_MARGIN);
                $('#CATALOG_ID').text(response[0].CATALOG_ID);
                $('#CATALOG_NAME').text(response[0].CATALOG_NAME);
                $('#COMMENT').text(response[0].COMMENT);
                $('#CONTENT_PAGE').text(response[0].CONTENT_PAGE);
                $('#CONTENT_PAGE_WITH_CATEGORY_FAMILY').text(response[0].CONTENT_PAGE_WITH_CATEGORY_FAMILY);
                $('#COVER_PAGE').text(response[0].COVER_PAGE);
                $('#CUSTOM_ATTRIBUTE').text(response[0].CUSTOM_ATTRIBUTE);
                $('#INDEX_PAGE').text(response[0].INDEX_PAGE);
                $('#INDEX_PAGE_WITH_CATALOG_ITEM_NO').text(response[0].INDEX_PAGE_WITH_CATALOG_ITEM_NO);
                $('#INDEXPAGE_COLUMNS').text(response[0].INDEXPAGE_COLUMNS);
                $('#LEFT_MARGIN').text(response[0].LEFT_MARGIN);
                $('#MULTIPLE_TABLES').text(response[0].MULTIPLE_TABLES);
                $('#ORIENTATION').text(response[0].ORIENTATION);
                $('#PAGE_COLUMN_GAP').text(response[0].PAGE_COLUMN_GAP);
                $('#PAGE_COLUMN_WIDTH').text(response[0].PAGE_COLUMN_WIDTH);
                $('#PAGE_COLUMNS').text(response[0].PAGE_COLUMNS);
                $('#PAPER_HEIGHT').text(response[0].PAPER_HEIGHT);
                $('#PAPER_SIZE').text(response[0].PAPER_SIZE);
                $('#PAPER_WIDTH').text(response[0].PAPER_WIDTH);
                $('#PROJECT_ID').text(response[0].PROJECT_ID);
                $('#PROJECT_NAME').text(response[0].PROJECT_NAME);
                $('#PUBLICATION_TYPE').text(response[0].PUBLICATION_TYPE);
                $('#REFERENCE_TABLES').text(response[0].REFERENCE_TABLES);
                $('#RIGHT_MARGIN').text(response[0].RIGHT_MARGIN);
                $('#SEGMENT_PER_HEIGHT').text(response[0].SEGMENT_PER_HEIGHT);
                $('#SEGMENT_PER_WIDTH').text(response[0].SEGMENT_PER_WIDTH);
                $('#SYSTEM_ATTRIBUTE').text(response[0].SYSTEM_ATTRIBUTE);
                $('#TOP_MARGIN').text(response[0].TOP_MARGIN);
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Response failed, missing ProjectID.',
                    type: "error"
                });

            }
        }).error(function (error) {
            options.error(error);
        });
        $('#pdfSummary').show();

    };

    $scope.backpdfSummary = function () {
        $("#PublicationType").removeClass("leftMenuactive");
        $("#PageSetup").removeClass("leftMenuactive");
        $("#PageSetupAdvanced").removeClass("leftMenuactive");
        $("#DataFields").removeClass("leftMenuactive");
        $("#HierarchyOption").removeClass("leftMenuactive");
        $("#SelectGroup").addClass("leftMenuactive");
        $("#Summary").removeClass("leftMenuactive");
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').hide();
        $('#pdfgroup').hide();
        $('#pdfgroupingattributes').show();
        $('#pdfSummary').hide();
    };
    //Selection & Grouping Attributes Functions
    function groupHeaderName(e) {
        if (e.value === 1) {
            return "Item Specifications (" + e.count + " items)";
        }
        else if (e.value === 3) {
            return "Item Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 4) {
            return "Item Price (" + e.count + " items)";
        } else if (e.value === 6) {
            return "Item Key (" + e.count + " items)";
        }
        else if (e.value === 7) {
            return "Product Description (" + e.count + " items)";
        }  else if (e.value === 9) {
            return "Product Image / Attachment (" + e.count + " items)";
        } else if (e.value === 11) {
            return "Product Specifications (" + e.count + " items)";
        }
        else if (e.value === 12) {
            return "Product Price (" + e.count + " items)";
        }
        else if (e.value === 13) {
            return "Product Key (" + e.count + " items)";
        }
        else if (e.value === 0) {
            return "Category (" + e.count + " items)";
        }
        else if (e.value === 2) {
            return "Product (" + e.count + " items)";
        }
        else if (e.value === 14) {
            return "Multiple Table(" + e.count + "items)";

        }
        else if (e.value === 15) {
            return "Reference Table(" + e.count + "items)";

        }
        else if (e.value === 16) {
            return "Item Family(" + e.count + "items)";

        }
        else if (e.value === 17) {
            return "Item Specification(" + e.count + "items)";
        }
        else if (e.value === 21) {
            return "Category Specification(" + e.count + "items)";
        }
        else if (e.value === 23) {
            return "Category Image / Attachment(" + e.count + "items)";
        }
        else if (e.value === 25) {
            return "Category Description(" + e.count + "items)";
        }
        else {
            return "Item Specifications(" + e.count + " items)";
        }
    }
    function groupHeaderName1(e) {
        if (e.value === 1) {
            return "Catalog Objects (" + e.count + " items)";
        }

        else {
            return "Catalog Objects (" + e.count + " items)";
        }
    }
    $scope.GetAllCatalogObjects = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, autoBind: false,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetPDFCatalogobjects($scope.SelectedCatalogId).success(function (response) {

                    for (var i = 0 ; response.length > i ; i++) {
                        if (response[i].ATTRIBUTE_NAME == "ITEM#") {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                        }
                        if (response[i].ATTRIBUTE_NAME == "SUBITEM#") {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                        }

                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });
    $scope.mainCatalogOptions = {
        autoBind: false,
        dataSource: $scope.GetAllCatalogObjects,
        columns: [
            { field: "ISAvailable", title: "<input type='checkbox' id='chkSelectAll' ng-click='checkAllObjects($event, this)'/>", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionPDF($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderName1
            }]
    };
    $scope.prodCatalogObjects = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, autoBind: false,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetPDFSelectedobjects().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });
    $scope.selectedCatalogObjects = {
        dataSource: $scope.prodCatalogObjects, autoBind: false,
        selectable: "multiple",
        columns: [
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderName1
            }]
    };
    $scope.updateSelectionPDF = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);

    };
    $scope.checkAllObjects = function (e) {
        var state = e.currentTarget.checked;

        $.each($scope.catselectobjects.dataSource._data, function () {
            if (this['ISAvailable'] != state)
                this.dirty = true;
            this['ISAvailable'] = state;
        });

        $scope.catselectobjects.refresh();
    };
    $scope.updateSelection2 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };
    $scope.SavePublishPDF = function () {
        var removeitems = [];
        for (var i = 0; i < $scope.GetAllCatalogObjects._data.length; i++) {
            var entity = $scope.GetAllCatalogObjects._data[i];
            if (entity.ISAvailable) {
                $scope.prodCatalogObjects.add(entity);
                removeitems.push(entity);
            }
        }
        for (var j = 0; j < removeitems.length; j++) {
            $scope.GetAllCatalogObjects.remove(removeitems[j]);
        }
    };
    $scope.DeletePublishPDF = function () {
        var g = $scope.prodselectObjects.select();

        var removeitems = [];
        var defaultvalues = ["CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_SHORT", "CATEGORY_NAME", "FAMILY_ID", "FAMILY_NAME", "SUBFAMILY_ID", "SUBFAMILY_NAME", "PRODUCT_ID"];
        g.each(function (index, row) {
            var selectedItem = $scope.prodselectObjects.dataItem(row);
            if (defaultvalues.indexOf(selectedItem.ATTRIBUTE_NAME) >= 0) {

            }
            else {
                removeitems.push(selectedItem);
                $scope.GetAllCatalogObjects.add(selectedItem);
            }
        });
        for (var i = 0; i < removeitems.length; i++) {
            $scope.prodCatalogObjects.remove(removeitems[i]);
        }

    };
    $scope.GetAllCatalogattributesdataSource = new kendo.data.DataSource({
        type: "json",
        filterable: true,
        serverFiltering: false,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetPDFCatalogattributes($scope.SelectedCatalogId, 0, $scope.catalogtype).success(function (response) {

                    for (var i = 0 ; response.length > i ; i++) {


                        if (response[i].ATTRIBUTE_ID == 1) {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            response[i].DISPLAY_NAME = $localStorage.CatalogItemNumber;

                        }
                        if (response[i].ATTRIBUTE_NAME == "SUBITEM#") {
                            response[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            response[i].DISPLAY_NAME = $localStorage.CatalogSubItemNumber;
                        }

                    }
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });
    $scope.mainGridOptions = {
        dataSource: $scope.GetAllCatalogattributesdataSource,
        filterable: { mode: "row" },
        columns: [
            { field: "ISAvailable", title: "<input type='checkbox' id='chkSelectAll' ng-click='checkAll($event, this)'/>", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', filterable: false },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px", filterable: true },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                filterable: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderNameall
                // groupHeaderNameall groupHeaderName
            }]
    };
    $scope.group1 = false;
    $scope.group3 = false;
    $scope.group4 = false;
    $scope.group7 = false;
    $scope.group6 = false;
    $scope.group9 = false;
    $scope.group12 = false;
    $scope.group11 = false;
    $scope.group13 = false;
    $scope.group0 = false;
    $scope.group2 = false;

    $scope.checkgroupAll1 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group1 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);

        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll17 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group17 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);

        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll3 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        //tar.dirty = e.currentTarget.checked;

        $scope.group3 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }

        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll4 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group4 = e.currentTarget.checked;
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
    };
    $scope.checkgroupAll7 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group7 = e.currentTarget.checked;
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
    };
    $scope.checkgroupAll6 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group6 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll9 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group9 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll11 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group11 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll12 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group12 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll13 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group13 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll14 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group13 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll15 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group13 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };
    $scope.checkgroupAll0 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group0 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        } if ($scope.group2) {
            $("#chk2").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }
    };
    $scope.checkgroupAll2 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group2 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }
        if ($scope.group16) {
            $("#chk16").prop("checked", true);
        }

    };

    $scope.checkgroupAll6 = function (e, tar) {
        var state = e.currentTarget.checked;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ATTRIBUTE_TYPE'] === tar) {
                if (this['ISAvailable'] != state)
                    this.dirty = true;
                this['ISAvailable'] = state;
            }
        });
        $scope.catselectgrid.refresh();
        $scope.group16 = e.currentTarget.checked;
        $("#" + e.currentTarget.id).prop("checked", e.currentTarget.checked);
        if ($scope.group3) {
            $("#chk3").prop("checked", true);
        }
        if ($scope.group4) {
            $("#chk4").prop("checked", true);
        }
        if ($scope.group7) {
            $("#chk7").prop("checked", true);
        } if ($scope.group6) {
            $("#chk6").prop("checked", true);
        } if ($scope.group9) {
            $("#chk9").prop("checked", true);
        } if ($scope.group12) {
            $("#chk12").prop("checked", true);
        } if ($scope.group11) {
            $("#chk11").prop("checked", true);
        }
        if ($scope.group13) {
            $("#chk13").prop("checked", true);
        } if ($scope.group0) {
            $("#chk0").prop("checked", true);
        } if ($scope.group1) {
            $("#chk1").prop("checked", true);
        }
        if ($scope.group17) {
            $("#chk17").prop("checked", true);
        }


    };
    function groupHeaderNameall(e) {
        if (e.value === 1) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll1($event," + e.value + ")'/> Item Specifications (" + e.count + " items)";
        }
        else if (e.value === 3) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll3($event," + e.value + ")'/> Item Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 4) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll4($event," + e.value + ")'/> Item Price (" + e.count + " items)";
        } else if (e.value === 7) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll7($event," + e.value + ")'/> Product Description (" + e.count + " items)";
        } else if (e.value === 6) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll6($event," + e.value + ")'/> Product Key (" + e.count + " items)";
        } else if (e.value === 9) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll9($event," + e.value + ")'/> Product Image / Attachment (" + e.count + " items)";
        } else if (e.value === 11) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll11($event," + e.value + ")'/> Product Specifications (" + e.count + " items)";
        }
        else if (e.value === 12) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll12($event," + e.value + ")'/> Product Price (" + e.count + " items)";
        }
        else if (e.value === 13) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll13($event," + e.value + ")'/> Product Key (" + e.count + " items)";
        }
        else if (e.value === 14) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll14($event," + e.value + ")'/> Multiple Table (" + e.count + " items)";
        }
        else if (e.value === 15) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll15($event," + e.value + ")'/>Reference Table(" + e.count + " items)";
        }
        else if (e.value === 0) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll0($event," + e.value + ")'/> Category (" + e.count + " items)";
        }
        else if (e.value === 2) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll2($event," + e.value + ")'/> Product (" + e.count + " items)";
        }
        else if (e.value === 16) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll6($event," + e.value + ")'/> Item Product (" + e.count + " items)";
        }
        else if (e.value === 21) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll6($event," + e.value + ")'/> Category Specification (" + e.count + " items)";
        }
        else if (e.value === 23) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll6($event," + e.value + ")'/> Category Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 25) {
            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll6($event," + e.value + ")'/> Category Description (" + e.count + " items)";
        }
        else {

            return "<input type='checkbox' id=chk" + e.value + " ng-click='checkgroupAll17($event," + e.value + ")'/> Item Specifications (" + e.count + " items)";
        }

    }
    $scope.updateSelection = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);

    };

    $scope.checkAll = function (e) {
        var state = e.currentTarget.checked;
        //  var grid = $scope.catselectgrid;
        $.each($scope.catselectgrid.dataSource._data, function () {
            if (this['ISAvailable'] != state)
                this.dirty = true;
            this['ISAvailable'] = state;
        });
        //$(".chkbxforallattributes").each(function () {
        //    $(this).prop("checked", true);
        //});
        $scope.catselectgrid.refresh();
    };

    $scope.prodfamilyattrdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetPDFSelectedattributes($scope.SelectedCatalogId, 0, $scope.catalogtype).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });
    $scope.selectedGridOptions = {
        dataSource: $scope.prodfamilyattrdataSource,
        selectable: "multiple",
        columns: [
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderName
            }]
    };
    $scope.updateSelection1 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };

    $scope.SavePublishAttributes = function () {
        var removeitems = [];
        for (var i = 0; i < $scope.GetAllCatalogattributesdataSource._data.length; i++) {
            var entity = $scope.GetAllCatalogattributesdataSource._data[i];
            if (entity.ISAvailable) {
                $scope.prodfamilyattrdataSource.add(entity);
                removeitems.push(entity);
            }
        }
        for (var j = 0; j < removeitems.length; j++) {
            $scope.GetAllCatalogattributesdataSource.remove(removeitems[j]);
        }
    };

    $scope.DeletePublishAttributes = function () {
        var g = $scope.prodselectgrid.select();

        var removeitems = [];
        var defaultvalues = ["CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_SHORT", "CATEGORY_NAME", "FAMILY_ID", "FAMILY_NAME", "SUBFAMILY_ID", "SUBFAMILY_NAME", "PRODUCT_ID"];
        g.each(function (index, row) {
            var selectedItem = $scope.prodselectgrid.dataItem(row);
            if (defaultvalues.indexOf(selectedItem.ATTRIBUTE_NAME) >= 0) {

            }
            else {
                removeitems.push(selectedItem);
                $scope.GetAllCatalogattributesdataSource.add(selectedItem);
            }
        });
        for (var i = 0; i < removeitems.length; i++) {
            $scope.prodfamilyattrdataSource.remove(removeitems[i]);
        }

    };
    //Cancel Function
    $scope.exitPDFXpressCatalog = function () {
        window.location.reload("../App/AdminDashboard/");
    };

    $scope.simplePDFTemplate = function () {
        $http.get("../XpressCatalog/GetProjectHeaderFooter?headertext=" + $scope.pageadvance.Header + "&footertext=" + $scope.pageadvance.Footer + "&projectId=" + $scope.Project.PROJECT_ID + "&productTablechecked=" + $scope.displayProduct_).
      then(function (response) {
         // $("#simplePDFTemplateDesinger").show();
      });
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').hide();
        $('#pdfgroup').hide();
        $('#pdfgroupingattributes').hide();
        $('#pdfSummary').hide();
        $("#hierarchyPDFTemplateDesinger").hide();
        $scope.winPDFCatalog.close();
        window.open("../xpressSimpleCatalog/SimpleDesignerTemplate", "DesignerTemplate")
        // $("#simplePDFTemplateDesinger").show();

    };
    $scope.hierarchyPDFTemplate = function () {
        $http.get("../XpressCatalog/GetProjectHeaderFooter?headertext=" + $scope.pageadvance.Header + "&footertext=" + $scope.pageadvance.Footer + "&projectId=" + $scope.Project.PROJECT_ID + "&productTablechecked=" + $scope.displayProduct_).
      then(function (response) {
      
         // $("#hierarchyPDFTemplateDesinger").show();
      });
        $("#pdfcatalogtyp").hide();
        $("#pdfPageSetup").hide();
        $("#pdfAdvanceOptions").hide();
        $('#pdfselection').hide();
        $('#pdfgroup').hide();
        $('#pdfgroupingattributes').hide();
        $('#pdfSummary').hide();
        $("#simplePDFTemplateDesinger").hide();
        $scope.winPDFCatalog.close();
        window.open("../xpressSimpleCatalog/hierarchicalDesignerTemplate", "_blank")
    

    };
    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            //  $scope.SelectedItem = 0;
            $scope.SelectedCatalogId = 0;
        }
        else {
            // $scope.SelectedItem = $localStorage.getCatalogID;
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
        }
        if ($rootScope.selecetedCatalogId != '' || $rootScope.selecetedCatalogId != 0) {
            $scope.SelectedItem = $rootScope.selecetedCatalogId;
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
        }
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerID = 0;
        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
        }
    };


    $scope.init();
}]);