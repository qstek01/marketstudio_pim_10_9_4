﻿LSApp.controller('DemoTestCatalogController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$filter', 'NgTableParams', '$compile', 'productService', '$localStorage', 'blockUI',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $filter, NgTableParams, $compile, productService, $localStorage, blockUI) {
        // --------------------------------------------------- Start Catalog--------------------------------------------------------------------------------
        $scope.Catalog = {
            CATALOG_NAME: '',
            CATALOG_ID: 0,
            VERSION: '1.0',
            DESCRIPTION: 'Sample Catalog',
            FAMILY_FILTERS: '',
            PRODUCT_FILTERS: '',
            familyfilter: [],
            productfilter: [],
            DEFAULT_FAMILY: ''
        };
        $scope.selects = function (catalogid) {
            debugger;
            $http.get("../Catalog/GetCatalog?catalogId=" + catalogid).
              then(function (objcategoryDetails) {
                  $scope.Catalog = objcategoryDetails.data[0];
                  if ($scope.newcatalogFlag === "1" && $scope.Catalog.CATALOG_ID != 0) {
                      $("#showcatalogTab").hide();

                      $scope.Catalog.CATALOG_NAME = "",
                     $scope.Catalog.VERSION = "",
                    $scope.Catalog.DESCRIPTION = "";

                  }

                  $scope.itemsToAdd = objcategoryDetails.data[0].familyfilter;
                  $scope.prodItemsToAdd = objcategoryDetails.data[0].productfilter;
                  angular.forEach($scope.itemsToAdd, function (value, key) {
                      $scope.GetFamilyAttr(value, $scope.Catalog.CATALOG_ID);
                  });
                  angular.forEach($scope.prodItemsToAdd, function (value, key) {
                      $scope.GetProductAttr(value, $scope.Catalog.CATALOG_ID);
                  });
              });
        };
        //  alert($scope.Catalog.DEFAULT_FAMILY);
        $scope.GetFamilyAttr = function (attributeId, catalogid) {
            debugger;
            attributeId.Spec_List = new kendo.data.DataSource({
                type: "json",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        dataFactory.familyattrvalues(attributeId.Attribute_id, catalogid).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        };
        $scope.GetProductAttr = function (attributeId, catalogid) {
            attributeId.Spec_List = new kendo.data.DataSource({
                type: "json",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        dataFactory.productattrvalues(attributeId.Attribute_id, catalogid).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        };

        $scope.products = productService.getProducts();
        debugger
        $scope.catalogDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogDetails($scope.SelectedItem, $scope.getCustomerID).success(function (response) {
                        blockUI.start();
                        options.success(response);
                        blockUI.stop();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            cache: false
        });

        $scope.attrcount = 0;
        $scope.prodimageattrcount = 0;
        $scope.familydesccount = 0;
        $scope.prodpriceattrcount = 0;
        $scope.prodkeyattrcount = 0;
        $scope.familyimgcount = 0;
        $scope.familyattrcount = 0;
        $scope.familypricecount = 0;
        $scope.familykeycount = 0;
        debugger
        $scope.catalogChange = function (e) {
            if (e.sender.value() !== "") {
                $scope.selects(e.sender.value());
                $scope.attrcount = 0;
                $scope.prodimageattrcount = 0;
                $scope.familydesccount = 0;
                $scope.prodpriceattrcount = 0;
                $scope.prodkeyattrcount = 0;
                $scope.familyimgcount = 0;
                $scope.familyattrcount = 0;
                $scope.familypricecount = 0;
                $scope.familykeycount = 0;
                $scope.selectcatalogattr(e.sender.value());
                $scope.dataSource.read();
                $scope.treeData.read();
                $scope.catalogfamilyattrDataSource.read();
                $scope.catalogproductattrDataSource.read();
                $scope.familyDataSource.read();

            }
        };
        $scope.newcatalog = function () {
            $scope.Catalog.CATALOG_ID = 0,
            $scope.Catalog.CATALOG_NAME = "",
            $scope.Catalog.VERSION = "",
            $scope.Catalog.DESCRIPTION = "";
            $("#showcatalogTab").hide();
            $scope.newcatalogFlag = "0";
            $scope.MirrorCatalog = false;
            $scope.familyDataSource.read();
        };

        $scope.MirrorCatalog = false;
        $scope.MirrorCatalogclick = function ($event) {
            if ($event) {
                $scope.newcatalogFlag = "1";
                $("#showcatalogTab").hide();
                $scope.Catalog.CATALOG_NAME = "",
                 $scope.Catalog.VERSION = "",
                $scope.Catalog.DESCRIPTION = "";

            }
            else {
                $("#showcatalogTab").show();
                $scope.newcatalogFlag = "0";
            }
        };

        $scope.savecatalog = function (catalog) {
            if (catalog.CATALOG_NAME.trim() == "") {
                alert("Please Enter Valid Name");
            }
            else {
                if ($scope.newcatalogFlag === "1" && $scope.Catalog.CATALOG_ID != 0) {
                    dataFactory.saveMirrorCatalogDetails(catalog).success(function (response) {
                        blockUI.start();
                        var responses = response;

                        if (responses.toString().contains("Exists")) {
                            alert(responses);
                        }
                        else if (responses != "") {
                            $scope.Catalog.CATALOG_ID = responses;
                            $scope.getCatalogId = responses;
                            $scope.selects($scope.getCatalogId);
                            alert("Catalog Created Successfully.");
                            $scope.newcatalogFlag = "0";
                            $scope.MirrorCatalog = false;
                        }
                        blockUI.stop();

                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    if (catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogdetails(catalog).success(function (response) {
                            blockUI.start();
                            var responses = response;
                            if (responses.toString().contains('Updated')) {
                                alert("Catalog Details Updated Successfully.");
                                $scope.catalogDataSource.read();
                                $scope.newcatalogFlag = "0";
                                $scope.MirrorCatalog = false;

                            } else if (responses.toString().contains('already exists')) {
                                alert("Catalog Name already exists.");
                            } else {
                                $scope.getCatalogId = responses;
                                $scope.catalogDataSource.read();
                                $scope.selects($scope.getCatalogId);
                                alert("Catalog Created Successfully.");
                                $scope.newcatalogFlag = "0";
                                $scope.MirrorCatalog = false;
                            }
                            blockUI.stop();
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            }
        };
        $scope.deletecatalog = function (catalog) {
            if (catalog.CATALOG_ID === 0 || catalog.CATALOG_ID === "") {
                alert("Please select the catalog");
            } else if (catalog.CATALOG_ID === 1) {
                alert("You cannot delete the Master Catalog");
            } else {

                dataFactory.deletecatalogdetails(catalog).success(function (response) {
                    blockUI.start();
                    $scope.catalogDataSource.read();
                    //$scope.selects($scope.getCatalogId);
                    $scope.newcatalog();
                    alert(response);
                    blockUI.stop();
                }).error(function (error) {
                    options.error(error);
                });
            }

        };
        // ---------------------------------------------------End Catalog--------------------------------------------------------------------------------


        //$scope.tableParams = [];
        // --------------------------------------------------- Start Attribute Tab--------------------------------------------------------------------------------
        $scope.selectcatalogattr = function (catalogid) {
            blockUI.start();
            $scope.attrcount = 0;
            $scope.prodimageattrcount = 0;
            $scope.familydesccount = 0;
            $scope.prodpriceattrcount = 0;
            $scope.prodkeyattrcount = 0;
            $scope.familyimgcount = 0;
            $scope.familyattrcount = 0;
            $scope.familypricecount = 0;
            $scope.familykeycount = 0;
            if (catalogid === "") {
                alert("Please select the catalog to view the attributes");
            } else {
                $("#showcatalogTab").show();
                $scope.productSpecificationsdataSource.read();
                $scope.productPricedataSource.read();
                $scope.productImagedataSource.read();
                $scope.productKeydataSource.read();
                $scope.familyKeydataSource.read();
                $scope.familyAttributedataSource.read();
                $scope.familyPricedataSource.read();
                $scope.familyImagedataSource.read();
                $scope.FamilyDescriptiondataSource.read();
            }
            blockUI.stop();
        };


        //---------------------------------------------------Product Specifications-----------------------------------------------------------------------------

        // $scope.attrcount = 0;
        //$scope.prodimageattrcount = 0;

        $scope.productSpecificationsdataSource = new kendo.data.DataSource({
            type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 1, options.data).success(function (response) {
                        $scope.attrcount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                            alert("Process Completed.");
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });

        $scope.productSpecificationsGridOptions = {
            dataSource: $scope.productSpecificationsdataSource,
            sortable: true, scrollable: true, editable: true, pageable: true, autoBind: false,
            toolbar: ["save", "cancel"],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", sortable: false, template: '<input type="checkbox" ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //------------------------------------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Product Image------------------------------------------------------------------
        $scope.productImagedataSource = new kendo.data.DataSource({
            type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 3, options.data).success(function (response) {
                        $scope.prodimageattrcount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                            alert("Process Completed.");
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.productImageGridOptions = {
            dataSource: $scope.productImagedataSource,
            sortable: true, scrollable: true, editable: true, pageable: true, autoBind: false,
            toolbar: ["save", "cancel"],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Product Price------------------------------------------------------------------
        $scope.productPricedataSource = new kendo.data.DataSource({
            type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 4, options.data).success(function (response) {
                        $scope.prodpriceattrcount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                            alert("Process Completed.");
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.productPriceGridOptions = {
            dataSource: $scope.productPricedataSource,
            sortable: true, scrollable: true, editable: true, pageable: true, autoBind: false,
            toolbar: ["save", "cancel"],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Product Key------------------------------------------------------------------
        $scope.productKeydataSource = new kendo.data.DataSource({
            type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 6, options.data).success(function (response) {
                        $scope.prodkeyattrcount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                            alert("Process Completed.");
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.productKeyGridOptions = {
            dataSource: $scope.productKeydataSource,
            sortable: true, scrollable: true, editable: true, pageable: true, autoBind: false,
            toolbar: ["save", "cancel"],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Family Description------------------------------------------------------------------
        $scope.FamilyDescriptiondataSource = new kendo.data.DataSource({
            type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 7, options.data).success(function (response) {
                        $scope.familydesccount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                            alert("Process Completed.");
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.FamilyDescriptionGridOptions = {
            dataSource: $scope.FamilyDescriptiondataSource,
            sortable: true, scrollable: true, editable: true, pageable: true, autoBind: false,
            toolbar: ["save", "cancel"],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Family Image------------------------------------------------------------------
        $scope.familyImagedataSource = new kendo.data.DataSource({
            type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 9, options.data).success(function (response) {
                        $scope.familyimgcount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                            alert("Process Completed.");
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.familyImageGridOptions = {
            dataSource: $scope.familyImagedataSource,
            sortable: true, scrollable: true, editable: true, pageable: true, autoBind: false,
            toolbar: ["save", "cancel"],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Family Price------------------------------------------------------------------
        $scope.familyPricedataSource = new kendo.data.DataSource({
            type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 12, options.data).success(function (response) {
                        $scope.familypricecount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                            alert("Process Completed.");
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.familyPriceGridOptions = {
            dataSource: $scope.familyPricedataSource,
            sortable: true, scrollable: true, editable: true, pageable: true, autoBind: false,
            toolbar: ["save", "cancel"],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Family Attribute------------------------------------------------------------------
        $scope.familyAttributedataSource = new kendo.data.DataSource({
            type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 11, options.data).success(function (response) {
                        $scope.familyattrcount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                            alert("Process Completed.");
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.familyAttributeGridOptions = {
            dataSource: $scope.familyAttributedataSource,
            sortable: true, scrollable: true, editable: true, pageable: true, autoBind: false,
            toolbar: ["save", "cancel"],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Family Key------------------------------------------------------------------
        $scope.familyKeydataSource = new kendo.data.DataSource({
            type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 13, options.data).success(function (response) {
                        $scope.familykeycount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        alert("You are not allowed to update the Master Catalog");
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                            alert("Process Completed.");
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });

        $scope.familyKeyGridOptions = {
            dataSource: $scope.familyKeydataSource,
            sortable: true, scrollable: true, editable: true, pageable: true, autoBind: false,
            toolbar: ["save", "cancel"],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };
        //---------------------------------------------------------------------------------------------------------------------

        //$scope.loadproductprice = function () {
        //    $scope.AttributeType = 4;
        //    $scope.productPricedataSource.read();
        //};
        //$scope.loadproductimage = function () {
        //    $scope.AttributeType = 3;
        //    $scope.productImagedataSource.read();
        //};
        //$scope.loadproductkey = function () {
        //    $scope.AttributeType = 6;
        //    $scope.productKeydataSource.read();
        //};
        //$scope.loadfamilykey = function () {
        //    $scope.AttributeType = 13;
        //    $scope.familyKeydataSource.read();
        //};
        //$scope.loadfamilyattribute = function () {
        //    $scope.AttributeType = 11;
        //    $scope.familyAttributedataSource.read();
        //};
        //$scope.loadfamilyprice = function () {
        //    $scope.AttributeType = 12;
        //    $scope.familyPricedataSource.read();
        //};
        //$scope.loadfamilyimage = function () {
        //    $scope.AttributeType = 9;
        //    $scope.familyImagedataSource.read();
        //};
        //$scope.loadfamilydesc = function () {
        //    $scope.AttributeType = 7;
        //    $scope.familyImagedataSource.read();
        //};

        $scope.updateSelection = function (e, id) {
            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };

        $scope.selectAll = function (ev) {
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };

        // ---------------------------------------------------End Attribute Tab--------------------------------------------------------------------------------

        // --------------------------------------------------- Start Category Sort Tab--------------------------------------------------------------------------------

        $scope.dataSource = new kendo.data.DataSource({
            pageSize: 5,
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCategorySortDetails("0", $scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "CATEGORY_ID",
                    fields: {
                        CATEGORY_ID: { editable: false, nullable: true },
                        CATEGORY_NAME: { editable: false, validation: { required: true } },
                        SORT_ORDER: { type: "number", validation: { required: true, min: 1 } },
                        PARENT_CATEGORY: { editable: false, nullable: true }
                    }
                }
            }
        });
        $scope.ParentCategoryID = '';
        $scope.ddlDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: function (options) {
                    dataFactory.GetCategorySortDetails($scope.ParentCategoryID, $scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.categoryDropDownEditor = function (container, options) {
            $scope.ParentCategoryID = options.model.PARENT_CATEGORY;

            $scope.ddlDataSource.read();
            var editor = $('<input kendo-drop-down-list required k-data-text-field="\'SORT_ORDER\'" k-data-value-field="\'SORT_ORDER\'"  k-on-change=\"categorysortChange(kendoEvent)\" k-data-source="ddlDataSource" data-bind="value:' + options.field + '"/>')
            .appendTo(container);

            $compile(editor)($scope);
            editor.css("visibility", "visible");
        };
        $scope.mainGridOptions = {
            dataSource: $scope.dataSource,
            pageable: true,
            selectable: "row",
            columns: [
              { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryDropDownEditor, template: "#=SORT_ORDER#" },
              { field: "CATEGORY_ID", title: "Category ID" },
              { field: "CATEGORY_NAME", title: "Category Name" }],

            editable: true, detailTemplate: kendo.template($("#template").html()),
            dataBound: function () {
                //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                this.expandRow(this.tbody.find("tr.k-master-row"));
            }
        };
        $scope.mainGridOptionsWithoutSort = {
            dataSource: $scope.dataSource,
            pageable: true,
            selectable: "row",
            columns: [
              { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryDropDownEditor, template: "#=SORT_ORDER#" },
              { field: "CATEGORY_ID", title: "Category ID" },
              { field: "CATEGORY_NAME", title: "Category Name" }],

            editable: false, detailTemplate: kendo.template($("#templatewithout").html()),
            dataBound: function () {
                //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                this.expandRow(this.tbody.find("tr.k-master-row"));
            }
        };

        $scope.detailGridOptions = function (dataItem) {
            return {
                dataSource: {
                    type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetCategorySortDetails(dataItem.CATEGORY_ID, $scope.Catalog.CATALOG_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "CATEGORY_ID",
                            fields: {
                                CATEGORY_ID: { editable: false, nullable: true },
                                CATEGORY_NAME: { editable: false, validation: { required: true } },
                                SORT_ORDER: { type: "number", validation: { required: true, min: 1 } },
                                PARENT_CATEGORY: { editable: false, nullable: true }
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 5
                },
                scrollable: false,
                sortable: true,
                selectable: "row",
                pageable: true,
                columns: [
        { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryDropDownEditor, template: "#=SORT_ORDER#" },
          { field: "CATEGORY_ID", title: "Category ID" },
          { field: "CATEGORY_NAME", title: "Category Name" }],
                editable: true, detailTemplate: kendo.template($("#template").html()),
                dataBound: function () {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#grid").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {
                        this.expandRow(this.tbody.find("tr.k-master-row"));
                        //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                    }
                }
            };
        };


        $scope.detailGridOptionsWithoutSort = function (dataItem) {
            return {
                dataSource: {
                    type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetCategorySortDetails(dataItem.CATEGORY_ID, $scope.Catalog.CATALOG_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "CATEGORY_ID",
                            fields: {
                                CATEGORY_ID: { editable: false, nullable: true },
                                CATEGORY_NAME: { editable: false, validation: { required: true } },
                                SORT_ORDER: { type: "number", validation: { required: true, min: 1 } },
                                PARENT_CATEGORY: { editable: false, nullable: true }
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 5
                },
                scrollable: false,
                sortable: true,
                selectable: "row",
                pageable: true,
                columns: [
        { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryDropDownEditor, template: "#=SORT_ORDER#" },
          { field: "CATEGORY_ID", title: "Category ID" },
          { field: "CATEGORY_NAME", title: "Category Name" }],
                editable: false, detailTemplate: kendo.template($("#templatewithout").html()),
                dataBound: function () {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#grid2").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {
                        this.expandRow(this.tbody.find("tr.k-master-row"));
                        //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                    }
                }
            };
        };
        //}
        $scope.changecategory = [];

        $scope.exchangeRows = function (dataItem, e) {
            $scope.changecategory = dataItem;
        };

        $scope.categorysortChange = function () {
            dataFactory.savecategorysort($scope.changecategory, $scope.Catalog.CATALOG_ID, $scope.changecategory.PARENT_CATEGORY).success(function (response) {
                alert("Updated Successfully");
                $scope.dataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.sortcategoryorder = function () {
            //alert($scope.categoryidvalue);
            dataFactory.savecategorysortasc($scope.categorynamesortvalue, $scope.Catalog.CATALOG_ID, $scope.categoryidvalue).success(function (response) {
                alert(response);
                $scope.dataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.categoryidvalue = 0;
        $("#categoryids").hide();
        $scope.logIt = function () {
            $("#categoryids").show();
        };

        $scope.hideIt = function () {
            $scope.categoryidvalue = 0;
            $("#categoryids").hide();
        };


        // --------------------------------------------------- End Category Sort Tab--------------------------------------------------------------------------------
        // --------------------------------------------------- Start Family Sort Tab--------------------------------------------------------------------------------
        $scope.treeData = new kendo.data.HierarchicalDataSource({
            type: "json",
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogCategorySortDetails($scope.Catalog.CATALOG_ID, options.data.id).success(function (response) {
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            }
        });
        $scope.categoryID = "0";

        $scope.clickSelectedcategoryfamily = function (dataItem) {
            $scope.categoryID = dataItem.id;
            $scope.familysort(dataItem.id);
        };

        $scope.catalogfamilydataSource = new kendo.data.DataSource({
            pageSize: 5,
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCategoryfamilyDetails($scope.categoryID, $scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "FAMILY_ID",
                    fields: {
                        FAMILY_ID: { editable: false, nullable: true },
                        CATEGORY_ID: { editable: false, nullable: true },
                        FAMILY_NAME: { editable: false, nullable: true },
                        SORT_ORDER: {
                            type: "number", validation: { required: true, min: 1 }
                        }
                    }
                }
            }
        });

        $scope.CategoryIDFamily = '';
        $scope.ddlCategoryDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: function (options) {
                    dataFactory.GetCategoryfamilyDetails($scope.CategoryIDFamily, $scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.categoryfamilyDropDownEditor = function (container, options) {
            $scope.CategoryIDFamily = options.model.CATEGORY_ID;
            $scope.ddlCategoryDataSource.read();
            var editor = $('<input kendo-drop-down-list required k-data-text-field="\'SORT_ORDER\'" k-data-value-field="\'SORT_ORDER\'" k-on-change=\"familysortChange(kendoEvent)\" k-data-source="ddlCategoryDataSource" data-bind="value:' + options.field + '"/>')
            .appendTo(container);
            $compile(editor)($scope);
            editor.css("visibility", "visible");
        };

        $scope.mainGridcategoryfamilyOptions = {
            dataSource: $scope.catalogfamilydataSource,
            pageable: true,
            selectable: "row",
            columns: [
                  { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryfamilyDropDownEditor, template: "#=SORT_ORDER#" },
              { field: "FAMILY_ID", title: "Family ID" },
              { field: "FAMILY_NAME", title: "Family Name" }],

            editable: true, detailTemplate: kendo.template($("#templates").html()),
            dataBound: function () {
                this.expandRow(this.tbody.find("tr.k-master-row"));
            }
        };

        $scope.mainGridcategoryfamilyOptionsWithoutSort = {
            dataSource: $scope.catalogfamilydataSource,
            pageable: true,
            selectable: "row",
            columns: [
                  { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryfamilyDropDownEditor, template: "#=SORT_ORDER#" },
              { field: "FAMILY_ID", title: "Family ID" },
              { field: "FAMILY_NAME", title: "Family Name" }],

            editable: false, detailTemplate: kendo.template($("#withouttemplates").html()),
            dataBound: function () {
                this.expandRow(this.tbody.find("tr.k-master-row"));
            }
        };


        $scope.detailGridcategoryfamilyOptions = function (dataItem) {
            return {
                dataSource: {
                    type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetCategorysubfamilyDetails(dataItem.FAMILY_ID, dataItem.CATEGORY_ID, $scope.Catalog.CATALOG_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "FAMILY_ID",
                            fields: {
                                FAMILY_ID: { editable: false, nullable: true },
                                CATEGORY_ID: { editable: false, nullable: true },
                                FAMILY_NAME: { editable: false, nullable: true },
                                SORT_ORDER: { type: "number", validation: { required: true, min: 1 } }
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 5
                },
                scrollable: false,
                sortable: true,
                selectable: "row",
                pageable: true,
                columns: [
                            { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryfamilyDropDownEditor, template: "#=SORT_ORDER#" },
                            { field: "FAMILY_ID", title: "Family ID" },
                            { field: "FAMILY_NAME", title: "Family Name" }
                ],
                editable: true, detailTemplate: kendo.template($("#templates").html()),
                dataBound: function (e) {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#gridcategoryfamily").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {
                        this.expandRow(this.tbody.find("tr.k-master-row"));
                    }
                }
            };
        };

        $scope.detailGridcategoryfamilyOptionsWithoutSort = function (dataItem) {
            return {
                dataSource: {
                    type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetCategorysubfamilyDetails(dataItem.FAMILY_ID, dataItem.CATEGORY_ID, $scope.Catalog.CATALOG_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "FAMILY_ID",
                            fields: {
                                FAMILY_ID: { editable: false, nullable: true },
                                CATEGORY_ID: { editable: false, nullable: true },
                                FAMILY_NAME: { editable: false, nullable: true },
                                SORT_ORDER: { type: "number", validation: { required: true, min: 1 } }
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 5
                },
                scrollable: false,
                sortable: true,
                selectable: "row",
                pageable: true,
                columns: [
                            { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryfamilyDropDownEditor, template: "#=SORT_ORDER#" },
                            { field: "FAMILY_ID", title: "Family ID" },
                            { field: "FAMILY_NAME", title: "Family Name" }
                ],
                editable: false, detailTemplate: kendo.template($("#withouttemplates").html()),
                dataBound: function (e) {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#gridcategoryfamilyWithoutSort").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {
                        this.expandRow(this.tbody.find("tr.k-master-row"));
                    }
                }
            };
        };

        $scope.familysort = function (categoryID) {
            $scope.categoryID = categoryID;
            $scope.catalogfamilydataSource.read();
        };
        $scope.selectedAttrVal = '';
        $scope.onAttributeSelected = function (e) {
            $scope.selectedAttrVal = e.sender._old;
            if ($scope.selectedAttrVal == '') {
                $("#familyids").hide();
            }
            else {
                $("#familyids").show();
            }
        };
        $scope.newValue = function (value) {
            $scope.familynamesortval = value;
            $scope.selectedAttrVal = '--- Select Family Attribute ---';
            $("#familyids").hide();
        }
        $scope.catalogfamilyattrDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogfamilyattrlistDetails($scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.changefamily = [];

        $scope.familyexchangeRows = function (dataItem, e) {
            $scope.changefamily = dataItem;
        };

        $scope.familysortChange = function (e) {
            dataFactory.savefamilysort($scope.changefamily, $scope.Catalog.CATALOG_ID, $scope.changefamily.PARENT_FAMILY_ID).success(function (response) {
                alert("Success");
                $scope.catalogfamilydataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };


        $scope.sortfamilyorder = function () {
            var attributeid = $("#attributedropdown").val();
            dataFactory.savefamilysortasc($scope.familynamesortvalue, $scope.Catalog.CATALOG_ID, $scope.categoryID, attributeid, $scope.familyidvalue).success(function (response) {
                alert(response);
                $scope.catalogfamilydataSource.read();
                $("#familyids").hide();
            }).error(function (error) {
                options.error(error);
            });

        };
        // --------------------------------------------------- End Family Sort Tab--------------------------------------------------------------------------------
        // --------------------------------------------------- Start Product Sort Tab--------------------------------------------------------------------------------
        $scope.catalogproductattrDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogproductattrlistDetails($scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $("#productattribute1").hide();
        $("#productattribute2").hide();
        $("#productattribute3").hide();
        $("#productattribute4").hide();

        $scope.productAttrSortValue = "asc";
        $scope.productAttrSortValue2 = "asc";
        $scope.productAttrSortValue3 = "asc";
        $scope.productAttrSortValue4 = "asc";
        $scope.productAttrSortValue5 = "asc";

        $scope.firstAttr = '0';
        $scope.secondAttr = '0';
        $scope.thirdAttr = '0';
        $scope.fourthAttr = '0';
        $scope.fifthAttr = '0';

        $scope.attributedrpchange = function (e) {
            $scope.firstAttr = e.sender._old;
            $("#productattribute1").show();
        };
        $scope.attributedrpchange1 = function (e) {
            $scope.secondAttr = e.sender._old;
            $("#productattribute2").show();
        };
        $scope.attributedrpchange2 = function (e) {
            $scope.thirdAttr = e.sender._old;
            $("#productattribute3").show();
        };
        $scope.attributedrpchange3 = function (e) {
            $scope.fourthAttr = e.sender._old;
            $("#productattribute4").show();
        };
        $scope.attributedrpchange4 = function (e) {
            $scope.fifthAttr = e.sender._old;
        };
        $scope.productsortorder = function () {
            dataFactory.saveProductSortOrder($scope.firstAttr, $scope.secondAttr, $scope.thirdAttr, $scope.fourthAttr, $scope.fifthAttr,
                 $scope.productAttrSortValue, $scope.productAttrSortValue2, $scope.productAttrSortValue3, $scope.productAttrSortValue4, $scope.productAttrSortValue5,
                 $scope.Catalog.CATALOG_ID, $scope.categoryID).success(function (response) {
                     alert(response);
                 }).error(function (error) {
                     options.error(error);
                 });
        };
        // --------------------------------------------------- End Product Sort Tab--------------------------------------------------------------------------------
        // --------------------------------------------------- Start Family Filter Tab--------------------------------------------------------------------------------
        var counter = 1;
        $scope.data = {
            fields: []
        };
        $scope.selectedattr = 0;

        //$scope.addField = function () {
        //    $scope.itemsToAdd = {
        //        Attribute: 'name',
        //        Attribute_id: 0,
        //        Condition: 'NONE',
        //        Operator: '=',
        //        Value: 'NONE',
        //        Spec_List: []
        //    };
        //    if ($scope.addField.fields == null)
        //        $scope.addField.fields = [];
        //    //  if ($scope.getFamilyVal.Condition != "NONE")
        //    //  {
        //    $scope.data.fields.push($scope.itemsToAdd);
        //    // }
        //};
        $scope.familyattrChange = function (e, item) {
            $scope.selectedattr = e.sender.value();
            item.Spec_List = new kendo.data.DataSource({
                type: "json",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        dataFactory.familyattrvalues($scope.selectedattr, $scope.Catalog.CATALOG_ID).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        };
        $scope.items = [];

        $scope.itemsToAdd = [{
            Attribute: 'name',
            Attribute_id: 0,
            Condition: 'NONE',
            Operator: '=',
            Value: 'NONE',
            Spec_List: []
        }];
        $scope.getItemValue = [];
        $scope.addNew = function () {
            if ($scope.itemsToAdd == null) {
                $scope.itemsToAdd = [{
                    Attribute: 'name',
                    Attribute_id: 0,
                    Condition: 'NONE',
                    Operator: '=',
                    Value: 'NONE',
                    Spec_List: []
                }];
            }
            angular.forEach($scope.itemsToAdd, function (value, key) {
                $scope.getItemValue = value;
            });
            $scope.filterCondition($scope.getItemValue);
        };
        $scope.filterCondition = function (itemVal) {
            if (itemVal.Condition != 'NONE') {
                $scope.itemsToAdd.push({
                    Attribute: '',
                    Attribute_id: '',
                    Condition: 'NONE',
                    Operator: '=',
                    Value: 'NONE',
                    Spec_List: []
                });
            }
        };
        $scope.Removefilter = function (itemaaa) {
            var i = -1;
            angular.forEach($scope.itemsToAdd, function (value, key) {
                // $scope.getItemValue = value;
                if (itemaaa == value) {
                    i = key;
                    // $scope.itemsToAdd.splice(itemaaa, 1);
                };
            });
            if (i != -1) {
                $scope.itemsToAdd.splice(i, 1);
            }
        };

        // --------------------------------------------------- End Family Filter Tab--------------------------------------------------------------------------------

        // --------------------------------------------------- Start Product Filter Tab--------------------------------------------------------------------------------

        $scope.proddata = {
            fields: []
        };

        $scope.prodattrChange = function (e, field) {
            field.Spec_List = new kendo.data.DataSource({
                type: "json",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        dataFactory.productattrvalues(e.sender.value(), $scope.Catalog.CATALOG_ID).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        };
        $scope.prodItemsToAdd = [{
            Attribute: 'name',
            Attribute_id: 0,
            Condition: 'NONE',
            Operator: '=',
            Value: 'NONE',
            Spec_List: []
        }];
        $scope.getProdValue = '';
        $scope.prodaddField = function () {
            if ($scope.prodItemsToAdd == null) {
                $scope.prodItemsToAdd = [{
                    Attribute: 'name',
                    Attribute_id: 0,
                    Condition: 'NONE',
                    Operator: '=',
                    Value: 'NONE',
                    Spec_List: []
                }];
            }
            angular.forEach($scope.prodItemsToAdd, function (value, key) {
                $scope.getProdValue = value;
            });
            $scope.filterProdCondition($scope.getProdValue.Condition);
        };
        $scope.filterProdCondition = function (itemProdVal) {
            if (itemProdVal != 'NONE') {
                $scope.prodItemsToAdd.push({
                    Attribute: '',
                    Attribute_id: '',
                    Condition: 'NONE',
                    Operator: '=',
                    Value: 'NONE',
                    Spec_List: []
                });
            }
        };


        $scope.familyitems = [{
            Attribute_id: "",
            Condition: "",
            Operator: "",
            Value: ""
        }];
        //$scope.prodaddField = function () {
        //    var item = {
        //        Attribute_id: "",
        //        Condition: "",
        //        Spec_Value: "",
        //        Spec_Condition: "",
        //        Spec_List: [],
        //    };

        //    if ($scope.prodaddField.fields == null)
        //        $scope.prodaddField.fields = [];
        //    $scope.proddata.fields.push(item);
        //};
        //$scope.proddata.fields
        $scope.productSaveField = function () {
            //  var sdte = $scope.proddata.fields;
            $scope.familyitems = [];
            angular.forEach($scope.prodItemsToAdd, function (value, key) {
                // familyitem = "";

                // familyitems.push(familyitem);
                $scope.familyitems.push({
                    Attribute_id: value.Attribute_id,
                    Condition: value.Condition,
                    Operator: value.Operator,
                    Value: value.Value
                });
            });
            // item = $scope.prodItemsToAdd;
            dataFactory.SaveProductFilters($scope.Catalog.CATALOG_ID, $scope.familyitems).success(function () {
                //  alert("Success");
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.familySaveField = function () {
            $scope.familyitems = [];
            angular.forEach($scope.itemsToAdd, function (value, key) {
                $scope.familyitems.push({
                    Attribute_id: value.Attribute_id,
                    Condition: value.Condition,
                    Operator: value.Operator,
                    Value: value.Value
                });
            });
            dataFactory.SaveFamilyFilters($scope.Catalog.CATALOG_ID, $scope.familyitems).success(function () {
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.ProdRemovefilter = function (itemaaa) {
            var i = -1;
            angular.forEach($scope.prodItemsToAdd, function (value, key) {
                // $scope.getItemValue = value;
                if (itemaaa == value) {
                    i = key;
                    // $scope.itemsToAdd.splice(itemaaa, 1);
                };
            });
            if (i != -1) {
                $scope.prodItemsToAdd.splice(i, 1);
            }
        };
        // --------------------------------------------------- End Product Filter Tab--------------------------------------------------------------------------------
        $scope.familyDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    if ($scope.Catalog.CATALOG_ID !== 0) {
                        dataFactory.GetCatalogFamily($scope.SelectedItem, $scope.Catalog.CATALOG_ID).success(function (response) {
                            blockUI.start();
                            options.success(response);
                            blockUI.stop();
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                    else {
                        dataFactory.GetCatalogFamily($scope.SelectedItem, 0).success(function (response) {
                            blockUI.start();
                            options.success(response);
                            blockUI.stop();
                        }).error(function (error) {
                            options.error(error);
                        });

                    }
                }
            },
            cache: false
        });
        $scope.familyChange = function (e) {
            if (e.sender.value() !== "") {
                $scope.Catalog.DEFAULT_FAMILY = e.sender.value();
            }
        };

        $scope.userRoleAddCatalog = false;
        $scope.userRoleModifyCatalog = false;
        $scope.userRoleDeleteCatalog = false;
        $scope.userRoleViewCatalog = false;

        $scope.getUserRoleRightsCatalog = function () {
            var id = 40201;
            dataFactory.getUserRoleRights(id).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddCatalog = response[0].ACTION_ADD;
                    $scope.userRoleModifyCatalog = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteCatalog = response[0].ACTION_REMOVE;
                    $scope.userRoleViewCatalog = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.userRoleAddSort = false;
        $scope.userRoleModifySort = false;
        $scope.userRoleDeleteSort = false;
        $scope.userRoleViewSort = false;

        $scope.getUserRoleRightsSort = function () {
            var id = 4020102;
            dataFactory.getUserRoleRights(id).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddSort = response[0].ACTION_ADD;
                    $scope.userRoleModifySort = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteSort = response[0].ACTION_REMOVE;
                    $scope.userRoleViewSort = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.userRoleAddFilter = false;
        $scope.userRoleModifyFilter = false;
        $scope.userRoleDeleteFilter = false;
        $scope.userRoleViewFilter = false;

        $scope.getUserRoleRightsFilter = function () {
            var id = 4020103;
            dataFactory.getUserRoleRights(id).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddFilter = response[0].ACTION_ADD;
                    $scope.userRoleModifyFilter = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteFilter = response[0].ACTION_REMOVE;
                    $scope.userRoleViewFilter = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };


        $scope.userRoleAddCatalogAttribute = false;
        $scope.userRoleModifyCatalogAttribute = false;
        $scope.userRoleDeleteCatalogAttribute = false;
        $scope.userRoleViewCatalogAttribute = false;

        $scope.getUserRoleRightsCatalogAttribute = function () {
            var id = 4020101;
            dataFactory.getUserRoleRights(id).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddCatalogAttribute = response[0].ACTION_ADD;
                    $scope.userRoleModifyCatalogAttribute = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteCatalogAttribute = response[0].ACTION_REMOVE;
                    $scope.userRoleViewCatalogAttribute = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.init = function () {
            $("#categoryids").hide();
            $("#familyids").hide();
            if ($localStorage.getCatalogID === undefined) {
                $scope.getCatalogId = 0;
                $scope.Catalog.CATALOG_ID = 0;
            }
            else {
                $scope.getCatalogId = $localStorage.getCatalogID;
                $scope.Catalog.CATALOG_ID = $scope.getCatalogId;
                $scope.selects($scope.getCatalogId);
                $("#showcatalogTab").hide();
            }
            if ($localStorage.getCustomerID == undefined) {
                $scope.getCustomerID = 0;

            }
            else {
                $scope.getCustomerID = $localStorage.getCustomerID;
            }
            $scope.getUserRoleRightsCatalog();
            $scope.getUserRoleRightsCatalogAttribute();
            $scope.getUserRoleRightsSort();
            $scope.getUserRoleRightsFilter();
        };
        $scope.checkboxes = { 'checked': false, items: {} };
        $scope.init();
    }]);
