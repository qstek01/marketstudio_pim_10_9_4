﻿LSApp.controller('familyProductController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope) {

    $scope.columnsForAtt = [];
    $scope.prodData = [];
   
    
   
    function onChange(e) {
        $scope.callProductGridPagePer(e.sender.text().trim());
    };
    $(".MainitemSelect").kendoDropDownList({
        change: onChange,
        dataSource: [
            { Value: "25" },
            { Value: "50" },
            { Value: "100" },
            { Value: "300" },
            { Value: "400" },
            { Value: "500" },
        ],
        dataTextField: "Value",
        dataValueField: "Value",

    });
    $scope.displayID = "true";
    $scope.ProdCurrentPageTest = "1";
    $scope.ProdCountPerPageTest = "50";
    $scope.gridCurrentPageTest = "50";
    $scope.callProductGridPagePerTest = function (pageper) {
        if (pageper != null && pageper != "0") {
            $scope.ProdCountPerPageTest = pageper;
            $scope.ProdCurrentPageTest = "1";
            $scope.LoadProdData1(2, "~293", "CAT4~293");
        }

    };
    $scope.callProductGridPagingTest = function (pageno) {
        if (pageno != null && pageno <= $scope.totalProdPageCount && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {
            if (pageno == 'PREV' && $scope.ProdCurrentPageTest != 1) {
                $scope.ProdCurrentPageTest = parseInt($scope.ProdCurrentPageTest) - 1;
            } else if (pageno == 'PREV' && $scope.ProdCurrentPageTest == 1) {
                $scope.ProdCurrentPageTest = "1";
            } else if (pageno == 'NEXT') {
                $scope.ProdCurrentPageTest = parseInt($scope.ProdCurrentPageTest) + 1;
                if ($scope.ProdCurrentPageTest > $scope.totalProdPageCount) {
                    $scope.ProdCurrentPageTest = $scope.totalProdPageCount;
                }
            } else {
                $scope.ProdCurrentPageTest = pageno;
            }

            $scope.LoadProdData1(2, "~293", "CAT4~293");
        }
    };
    $scope.LoadProdData1 = function (catalogId, id, cat_id) {
        $scope.sub_id = '0';
        //dataFactory.getprodspecs(catalogId, id, $scope.displayID).success(function (response) {
        //document.getElementById("gridpaging" + $scope.ProdCountPerPageTest).className = "gridpaginghighlight";
        var prodCount = [];
        dataFactory.getprodspecsWithoutAttributes(catalogId, id, true, cat_id).success(function (response) {
            if (response.Data.Columns.length) {
                var obj = jQuery.parseJSON(response.Data.Data);
                var prodEditPageloopcnt = Math.floor(obj.length / parseInt($scope.ProdCountPerPageTest));
                if ((obj.length % parseInt($scope.ProdCountPerPageTest)) > 0) {
                    prodEditPageloopcnt = prodEditPageloopcnt + 1;
                }
                $scope.changeProdPage = "1";

                for (var i = 1; i <= prodEditPageloopcnt; i++) {
                    prodCount.push(i);
                }
                $scope.ProdPageCount = prodCount;
                if ($scope.ProdCurrentPageTest == "1") {
                    $scope.changeProdPage = 1;
                } else {
                    $scope.changeProdPage = $scope.ProdCurrentPageTest;
                }
                $scope.ProdCurrentPageTest = $scope.changeProdPage;
                $scope.totalProdPageCount = prodEditPageloopcnt;
            }
        });
        dataFactory.getprodspecs(catalogId, id, true, cat_id, $scope.ProdCurrentPageTest, $scope.ProdCountPerPageTest, $scope.sub_id).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.prodData = obj;
            $scope.columnsForAtt = response.Data.Columns;
            angular.forEach($scope.prodData, function (value) {
                value.SORT = parseFloat(value.SORT);
            });
           // $scope.tblDashBoardTest.reload();
           
        }).error(function (error) {
            options.error(error);
        });

    };


    //$scope.tblDashBoardTest = new ngTableParams(
    // { page: 1, count: 1000 },
    // {
    //     counts: [], //5, 10, 25, 50, 100
    //     sorting: {
    //         SORT: 'asc'     // initial sorting
    //     },
    //     total: function () { return $scope.prodData.length; },
    //     $scope: { $data: {} },
    //     getData: function ($defer, params) {
    //         var filteredData = $scope.prodData;
    //         var orderedData = filteredData;
    //         params.total(orderedData.length);
           
    //         return $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    //     }

    // });
    $scope.LoadProdData1(2, "~293", "CAT4~293");
    

}]);



