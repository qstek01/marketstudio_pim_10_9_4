﻿/// <reference path="ApplicationSettings.cshtml" />
/// <reference path="../App/Preference/PreferenceDetails.cshtml" />
/// <reference path="../App/Preference/PreferenceDetails.cshtml" />
LSApp.controller('userAdminController', ['$scope', '$window', '$location', '$routeParams', '$route', 'dataFactory', '$q', '$timeout', '$rootScope', '$http', '$filter', '$localStorage',
    function ($scope, $window, $location, $routeParams, $route, dataFactory, $q, $timeout, $rootScope, $http, $filter, $localStorage) {
        $scope.UserImportdiv = false;
      
        $("#editGroupName").hide();
        $("#workflowtab").hide();
        $scope.showImportdiv = false;
        $("#showUserImportdiv").hide();
        $scope.showImportRolediv = false;
        $("#showRoleImportdiv").hide();
        $scope.RoleImportdiv = false;
        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
        $scope.showImportdiv = true;
        $scope.Importdiv = true;
        $scope.UserImportdiv = true;
        $scope.usertypesdiv = true;
        $scope.showImportdiv = false;
        $scope.Importdiv = false;
        $scope.userValuediv = true;
        //$scope.showNewUserBtn = true;
        $scope.exporttype = "User";
        $scope.userexporttypes = 1;
        $("#hideExportTypeDiv").show();
        $scope.Selectedfromstatus = "";
        $scope.items = [];
        $scope.emailpattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        ///^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

        $scope.UserAdminGroup = [];
        $scope.UserFunctionsAllowedData = [];
        $scope.UserRolesListViewTemplate = $("#UserRolesListViewTemplate").html();

        $scope.Editflag = false;
        $scope.Attritems = new kendo.data.DataSource({
            data: [{ Text: "tbadmin", Value: "1" }, { Text: "tbuser", Value: "2" }]
        });
        $scope.AllowChar = function (e) {

            if (e.ctrlKey || e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                    e.preventDefault();
                }
            }
        }

        $scope.WebConfigFiles = {
            announcementCount: '',
            ImportSheetProductCount: '',
            RestoreSourcePath: '',
            RestoreDestPath: '',
            ServerPath: '',
            DataSource: '',
            UserID: '',
            Password: '',
            BackUpAttachement: '',
            BackUpMdfPath: '',
            BackUpDataBasePath: '',
        };
        $scope.Userweb =
            {
                organisation: '',
                role: '',
                FirstName: '',
                LastName: '',
                Email: '',
                Phone: '',
                Mobile: '',
                Skype: '',
                WhatsApp: '',
                Company: '',
                Designation: '',
                Notes: '',
            }
        $scope.SaveWebUser =
            {
                organization: '',
            }


        $scope.selectedPlanDataList = {
            FUNCTION_GROUP_ID: 0,
            FUNCTION_ID: 0,
            FUNCTION_NAME: '',
            ACTION_VIEW: true,
            ACTION_ADD: true,
            ACTION_MODIFY: true,
            ACTION_REMOVE: true
        };
        $scope.selectedPlanData = [];
        $scope.selectedRole = {};
        $scope.CustomerName = '';
        $scope.myProfileName = $localStorage.getUserName;
        $scope.IsActive = false;
        $scope.CustomerId = $localStorage.getCustomerID;
        $scope.changedcatalogid = 0;
        $rootScope.userSuperAdmin = false;
        $rootScope.userAdmin = false;
        $rootScope.userAdminUsers = false;
        $scope.userInvitecustomerID = $scope.CustomerId;
        $scope.userWeb = '';
        $scope.value = '6';
        $scope.Role =
            { RoleId: '1' };

        $scope.User = {
            TB_USER_ID: '',
            NEW_PASSWORD: '',
            CONFIRM_PASSWORD: '',
            USER_ROLE: '',
            STATUS: ''
        };


        $scope.User1 = {
            User_Name: '',
            Email: '',
            Password: '',
            ConfirmPassword: '',
            FirstName: '',
            LastName: '',
            Title: '',
            Phone: '',
            Fax: '',
            City: '',
            State: '',
            Zip: '',
            Country: '',
            Active: 'true',
            UserRoles: '',
            CompanyName: '',
            Address: '',
            Customer_User_Id: 0,
            UserRoleName: '',
            CATALOG_ID: 1,

        };
        $scope.UserWorkFlow = {
            TB_USER_ID: '',
            NEW: '',
            UPDATED: '',
            REVIEW_PROCESS: '',
            SUBMIT: '',
            APPROVE: ''
        };
        $scope.UserFunctionAllowed = {
            FUNCTION_ID: '',
            FUNCTION_NAME: '',
            ROLE_ID: '',
            ACTION_VIEW: '',
            ACTION_MODIFY: '',
            ACTION_ADD: '',
            ACTION_REMOVE: ''
        };

        $("#saverole").hide();



        $scope.myprofileDatasource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: function (options) {
                    dataFactory.getMyprofileList($localStorage.getCustomerID).success(function (response) {
                        var rolename = response[0].RoleName.split('_');
                        $scope.CustomerName = rolename[1];
                        $scope.IsActive = response[0].Active;
                        options.success(response);
                        $scope.myProfileName = response[0].CustomerName;
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.filterClick = function (d) {
          
            $scope.filterBasedonSelect = d;
            $scope.userListDataSource.read();

        }

        //Start of My Profile Tab Grid
        $scope.userListDataSource = new kendo.data.DataSource({
            pageSize: 10,
            batch: false,
            autoBind: true,
            serverPaging: false,
            serverSorting: false,
            serverFiltering: false,
            transport: {
                read: function (options) {
                  
                    if ($scope.filterBasedonSelect == undefined) {
                        $scope.filterBasedonSelect = 1;
                        $scope.filter = $scope.filterBasedonSelect;
                    } else {
                        $scope.filter = $scope.filterBasedonSelect;
                    }
                    dataFactory.getUserList($scope.CustomerId, $scope.filter).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.userListGridOptions = {
            dataSource: $scope.userListDataSource,
            autoBind: true,
            sortable: true,
            filterable: true,
            autoBind: true,
            selectable: false,
            editable: "popup",

            pageable: { buttonCount: 5 },
            columns: [
            {
                field: "FIRSTNAME", title: "Name", width: "20%", template: "#= LASTNAME +', ' + FIRSTNAME #"
            },
                {
                    field: "USER_NAME", title: "User ID", width: "20%",
                },
                  {
                      field: "ORGANIZATION_NAME", title: "Organization", width: "25%"
                  },

                { field: "ROLE_NAME", title: "Group(s)", width: "20%" },
                //   { field: "ROLE_NAME", title: "ROLE_NAME", editor: $scope.roleDropDownEditor, template: "#=ROLE_NAME#" },
                {
                    field: "STATUS", title: "Status", width: "10%"
                },

                {
                    field: "edit", title: "Actions", template: "<a class='glyphicon glyphicon-edit blue btn-xs-icon' style='padding: 6px 1px;' title = \'EDIT\' ng-click='\editCustomer(this)\'></a>" + "<a class='glyphicon fa fa-user-plus blue btn-xs-icon btn org-btn'  style='padding : 7px 9px 12px 7px;'  title = \'Add\' ng-click='\addNewUser()\'></a>" + "<a class='glyphicon glyphicon-remove blue btn-xs-icon'  style='padding:4px 11px -2px 4px;'  title = \'Delete\' ng-click='\deleteUser(this)\'></a>" + "<a class='glyphicon glyphicon-ok-circle blue btn-xs-icon'  style='padding: 6px 10px;' title = \'STATUS\' ng-click='changeStatus(this)'></a>", width: "10%"
                }
            ]

        };
        $scope.CatalogIds = 0;
        $scope.catalogDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    if ($scope.User1.UserRoles == "")
                        $scope.User1.UserRoles = 0;
                    dataFactory.GetCatalogDetailsUserRole($scope.SelectedItem, $scope.CustomerId, $scope.User1.UserRoles).success(function (response) {

                        options.success(response);
                        if (response.length > 0) {

                            if ($scope.IsUserEdit == "false") {
                                $scope.User1.CATALOG_ID = response[0].CATALOG_ID;
                            }
                            else {
                                $scope.User1.CATALOG_ID = $scope.CatalogIds;
                            }
                        }
                        else {
                            $scope.User1.CATALOG_ID = $scope.CatalogIds;
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            cache: false
        });
        //$scope.selectOptions = {
        //    placeholder: "Select Groups",
        //    dataTextField: "roleName",
        //    dataValueField: "code",
        //    valuePrimitive: true,
        //    autoBind: false,
        //    dataSource: {
        //        serverFiltering: true,
        //        transport: {
        //            read: function (options) {
        //                debugger;
        //                dataFactory.getGroupNames($scope.CustomerId).success(function (response) {
        //                    $scope.selectedIds = response;
        //                });
        //            }
        //        }
        //    }
        //};
        $scope.selectOptions = new kendo.data.DataSource({
            type: "json",
            valuePrimitive: true,
            async: false,
            editable: false,
            serverSorting: true,
            serverFiltering: true,
            sortable: true,
            batch: true,
            transport: {
                read: function (options) {
                    dataFactory.getGroupNames($scope.CustomerId).success(function (response) {
                        options.success(response);
                        //$scope.User1.selectedIds = response;

                    }).error(function (error) {
                        options.error(error);
                    });

                }
            },

            sort: { field: "organisationName", dir: "desc" },
            cache: false

        });

        $scope.editCustomer = function (row) {
          
            if (row.dataItem.ROLE_NAME == "Admin")
            {
                $scope.Statusflag = true;
            }
            else
            {
                $scope.Statusflag = false;

            }
            $("#showfilter").hide();
            $scope.organisationloadDataSource = new kendo.data.DataSource({
                type: "json",
                async: false,
                editable: false,
                serverSorting: true,
                serverFiltering: true,
                sortable: true,
                batch: true,
                transport: {
                    read: function (options) {
                        dataFactory.loadOrganizationName().success(function (response) {
                            options.success(response);

                        }).error(function (error) {
                            options.error(error);
                        });

                    }
                },

                sort: { field: "organisationName", dir: "desc" },
                cache: false

            });
            $scope.IsUserEdit = "true";
            $scope.Editflag = true;
            $scope.countryCode = row.dataItem.COUNTRY;
            // $scope.stateListDataSource.read();
            $scope.User1.Customer_User_Id = row.dataItem.ID;
            //  $scope.User1.User_Name = row.dataItem.TB_USER_ID;
            $scope.User1.LastName = row.dataItem.LASTNAME;
            $scope.User1.FirstName = row.dataItem.FIRSTNAME;
            $scope.User1.Title = row.dataItem.TITLE;
            $scope.User1.Phone = row.dataItem.PHONE;
            $scope.User1.Fax = row.dataItem.FAX;
            $scope.User1.City = row.dataItem.CITY;
            $scope.User1.State = row.dataItem.STATE;
            $scope.User1.Zip = row.dataItem.ZIP;
            $scope.User1.Country = row.dataItem.COUNTRY;
            $scope.User1.CATALOG_ID = row.dataItem.CATALOGID;
            if (row.dataItem.STATUS.toUpperCase() == "ACTIVE")
                $scope.User1.Active = 1;
            else
                $scope.User1.Active = 0;


           // $scope.User1.selectedIds= 46;
            //$scope.User1.UserRoleName = row.dataItem.ROLE_NAME;
            $scope.User1.Address = row.dataItem.ADDRESS;
            $scope.User1.Email = row.dataItem.EMAIL;
            $scope.CatalogIds = row.dataItem.CATALOGID;
            $scope.organisationName = row.dataItem.ORGANIZATION_NAME;
            dataFactory.getOrganizationId($scope.organisationName).success(function (response) {
                $scope.User1.organisation = response;
                $scope.organizationId = response;
            });
            $scope.GroupNames=row.dataItem.ROLE_NAME;
            dataFactory.getRoleDetails($scope.CustomerId,$scope.GroupNames).success(function (response) {
                $scope.User1.selectedIds = response;

            });

           // $scope.User1.organisation = 10;
            $scope.catalogDataSource.read();

            $("#userListView").hide();
            $("#newUserEntryView").show();

        };

        $scope.getMessage = function (e) {
            var uid = this.element.closest("[data-uid]").data("uid"),
                dataSource = $("#userListViewGrid").data("kendoGrid").dataSource,
                item = dataSource.getByUid(uid);
            item.dirty = true;
        };



        $scope.userFunctionsAllowedGridDatasource = new kendo.data.DataSource({
            type: "json",
            batch: true,
            pageSize: 5,
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.getUsersFunctionAllowedData($scope.myProfileName).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    dataFactory.saveUserFunctionAllowed(options).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return { models: kendo.stringify(options.models) };
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return data.length;
                },
                model: {
                    id: "FUNCTION_ID",
                    fields: {
                        ROLE_ID: { type: "int", editable: false },
                        FUNCTION_NAME: { type: "string", editable: false },
                        ACTION_VIEW: { type: "boolean" },
                        ACTION_MODIFY: { type: "boolean" },
                        ACTION_ADD: { type: "boolean" },
                        ACTION_REMOVE: { type: "boolean" }
                    }
                }
            },
            error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });

            }
        });


        $("#userFunctionsAllowedGrid").kendoGrid({
            dataSource: $scope.userFunctionsAllowedGridDatasource,
            pageable: { buttonCount: 5 },
            height: 430,
            //toolbar:
            //    ["save", "cancel"],
            columns: [
                { field: "FUNCTION_NAME", title: "Active Features", width: "200px" },
                { field: "ACTION_VIEW", title: "Action View", width: 130, template: '<input type="checkbox" class="chkbx1" #= ACTION_VIEW ? \'checked="checked"\' : "" # disabled></input>' },
                { field: "ACTION_MODIFY", title: "Action Modify", width: 130, template: '<input type="checkbox" class="chkbx2" #= ACTION_MODIFY ? \'checked="checked"\' : "" # disabled></input>' },
                { field: "ACTION_ADD", title: "Action Add", width: 130, template: '<input type="checkbox" class="chkbx3" #= ACTION_ADD ? \'checked="checked"\' : "" # disabled></input>' },
                { field: "ACTION_REMOVE", title: "Action Remove", width: 130, template: '<input type="checkbox" class="chkbx4" #= ACTION_REMOVE ? \'checked="checked"\' : "" # disabled></input>' }
            ]
            //,
            //editable: {
            //    mode: "inline",
            //    update: true,
            //}
        });

        $("#userFunctionsAllowedGrid .k-grid-content").on("change", "input.chkbx1", function (e) {
            var grid = $("#userFunctionsAllowedGrid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
            dataItem.set("ACTION_VIEW", this.checked);
        });

        $("#userFunctionsAllowedGrid .k-grid-content").on("change", "input.chkbx2", function (e) {
            var grid = $("#userFunctionsAllowedGrid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
            dataItem.set("ACTION_MODIFY", this.checked);
        });

        $("#userFunctionsAllowedGrid .k-grid-content").on("change", "input.chkbx3", function (e) {
            var grid = $("#userFunctionsAllowedGrid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
            dataItem.set("ACTION_ADD", this.checked);
        });

        $("#userFunctionsAllowedGrid .k-grid-content").on("change", "input.chkbx4", function (e) {
            var grid = $("#userFunctionsAllowedGrid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
            dataItem.set("ACTION_REMOVE", this.checked);
        });

        $scope.userReadOnlyAttributesDataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            batch: true,
            transport: {
                read: function (options) {
                    dataFactory.getUsersReadOnlyAttributeList($scope.myProfileName).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        //$scope.userReadOnlyAttributesGridOptions = {
        //    dataSource: $scope.userReadOnlyAttributesDataSource,
        //    autoBind: true,
        //    batch: true,
        //    filterable: true,
        //    sortable: true,
        //    pageable: { buttonCount: 5 },
        //    columns: [
        //        { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "280px" }
        //    ]
        //};

        // End of My Profile Tab Grid

        //Start of Roles Tab Grid
        $scope.ROLE_ID = 0;

        $scope.userRolesFunctionsAllowedGridDatasource = new kendo.data.DataSource({
            type: "json",
            batch: true,
            pageSize: 5,
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.getUsersRolesFunctionAllowedData($scope.ROLE_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    dataFactory.saveUserFunctionAllowed(options).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return { models: kendo.stringify(options.models) };
                }
            },
            schema: {
                data: function (data) {
                    return data;
                },
                total: function (data) {
                    return data.length;
                },
                model: {
                    id: "FUNCTION_ID",
                    fields: {
                        FUNCTION_NAME: { type: "string", editable: false },
                        ACTION_VIEW: { type: "boolean" },
                        ACTION_MODIFY: { type: "boolean" },
                        ACTION_ADD: { type: "boolean" },
                        ACTION_REMOVE: { type: "boolean" }
                    }
                }
            },
            error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }
        });

        $scope.UserRoleAttributeDatasourceallow = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    dataFactory.GetUsersRolesFunctionAllowedDatamodifiedforAttribute($scope.ROLE_ID, options.data).success(function (response) {

                        if (response.Data.length != null) {

                            //To Change Dynamice CatalogNo
                            for (var i = 0 ; i < response.Data.length; i++) {
                                if (response.Data[i].attribute_name.toUpperCase() == "ITEM#") {
                                    response.Data[i].attribute_name = $localStorage.CatalogItemNumber;
                                }
                                if (response.Data[i].attribute_name.toUpperCase() == "SUBITEM#") {
                                    response.Data[i].attribute_name = $localStorage.CatalogSubItemNumber;
                                }
                            }
                        }
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                }
            },
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "attribute_id",
                    fields: {
                        attribute_name: { type: "string" },
                        readonly: { type: "boolean" }
                    }
                }
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }
        });

        $scope.UserRoleAttributeGridOptions = {
            autoBind: true,
            dataSource: $scope.UserRoleAttributeDatasourceallow,
            filterable: true,
            sortable: true,
            pageable: { buttonCount: 5 },
            batch: true,
            columns: [
                { field: "attribute_name", title: "Attribute Name", width: "365px" },
                {
                    field: "readonly", title: "Allow Edit", template: '<input type="checkbox" id="yes" #= readonly?checked="" : "checked" # ng-click="readOnly($event, this)">No</input> &nbsp; &nbsp; <input type="checkbox" id="no" #= readonly?checked="checked" : "" # ng-click="readOnly($event, this)">Yes</input>'
                }],
            editable: false
        };


        $scope.readOnly = function (e, id) {
            id.dataItem.set("readonly", e.target.checked);
            //id.dataItem.role_id
            //id.dataItem.attribute_id
            if (e.target.id === "yes")
                $scope.val = false;
            else
                $scope.val = true;
            dataFactory.updateReadonlyattributes(id.dataItem.role_id, id.dataItem.attribute_id, $scope.val).success(function (response) {
            }).error(function (error) {
                options.error(error);
            });
            $scope.UserRoleAttributeDatasourceallow.read();
        };


        $scope.removeRole = function () {
            if ($scope.ROLE_ID !== 0) {
                if ($scope.ROLE_ID !== undefined && $scope.ROLE_ID !== '' && $scope.ROLE_ID !== null) {
                    if ($scope.ROLE_ID !== 1 && $scope.ROLE_ID !== 2 && $scope.ROLE_ID !== 3 && $scope.ROLE_ID !== 4) {

                        dataFactory.deleteRole($scope.ROLE_ID).success(function (response) {


                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            // $scope.ROLE_ID = 0;
                            $scope.ROLE_ID = $scope.Defaultrole_id;
                            $scope.UserRolesListDataSource.read();
                            $scope.UserRoleDatasource.read();
                            $scope.UserRoleAttributeDatasourceallow.read();
                            $scope.userRoledropdowndatasource.read();
                            $scope.userRoleforGroupdropdowndatasource.read();

                        }).error(function (error) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Cannot be deleted.',
                                type: "error"
                            });
                        });

                    } else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Default roles cannot be deleted.',
                            type: "error"
                        });

                    }

                }
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Role for the User.',
                    //type: "info"
                });
            }

        };

        $scope.UserRoleDatasource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    dataFactory.GetUsersRolesFunctionAllowedDatamodified($scope.ROLE_ID, options.data).success(function (response) {
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                },
                update: function (options) {
                    dataFactory.saveUserFunctionAllowed(options).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return { models: kendo.stringify(options.models) };
                },
                batch: true,
            },
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "FUNCTION_ID",
                    fields: {
                        FUNCTION_NAME: { type: "string" },
                        ACTION_VIEW: { type: "boolean" },
                        ACTION_MODIFY: { type: "boolean" },
                        ACTION_ADD: { type: "boolean" },
                        ACTION_REMOVE: { type: "boolean" }
                    }
                }
            },
            serverFiltering: true,
            serverPaging: false,
            serverSorting: false, editable: true, pageable: false,
            error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });

            }
        });


        $scope.UserRoleplanGridOptions = {
            autoBind: false,
            dataSource: $scope.UserRoleDatasource,
            batch: true,
            sortable: false, scrollable: true, editable: true, pageable: false, filterable: true,
            selectable: "row",
            columns: [
                { field: "FUNCTION_NAME", title: "Funtion Name", width: "250px" },
                { field: "ACTION_VIEW", title: "Action View", width: 100, template: '<input type="checkbox" id="view" #= ACTION_VIEW?checked="checked" : "" #  ng-click="updateRole($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll_ACTION_VIEW($event)'/>  VIEW" },
                { field: "ACTION_MODIFY", title: "Action Modify", width: 100, template: '<input type="checkbox" id="modify" #= ACTION_MODIFY?checked="checked" : "" # ng-click="updateRole($event, this)" ></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll_ACTION_MODIFY($event)'/> MODIFY" },
                { field: "ACTION_ADD", title: "Action Add", width: 100, template: '<input type="checkbox" id="add" class="chkbx3" #= ACTION_ADD ? \'checked="checked"\' : "" #  ng-click="updateRole($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll_ACTION_ADD($event)'/> ADD" },
                { field: "ACTION_REMOVE", title: "Action Remove", width: 100, template: '<input type="checkbox" id="remove" class="chkbx4" #= ACTION_REMOVE ? \'checked="checked"\' : ""  #  ng-click="updateRole($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll_ACTION_REMOVE($event)'/> REMOVE" }, //ng-click="updateRole($event, this)"
            ],
            editable: false
        };
        $scope.selectAll_ACTION_VIEW = function (ev) {
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            items.forEach(function (item) {

                if (ev.target.checked == false) {
                    item.set("ACTION_VIEW", ev.target.checked);
                    item.set("ACTION_MODIFY", ev.target.checked);
                    item.set("ACTION_ADD", ev.target.checked);
                    item.set("ACTION_REMOVE", ev.target.checked);
                }
                else {
                    item.set("ACTION_VIEW", ev.target.checked);
                }
            });
        };

        $scope.selectAll_ACTION_MODIFY = function (ev) {

            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            items.forEach(function (item) {
                if (ev.target.checked == false) {
                    item.set("ACTION_MODIFY", ev.target.checked);
                    item.set("ACTION_ADD", ev.target.checked);
                }
                else {
                    item.set("ACTION_MODIFY", ev.target.checked);
                    item.set("ACTION_VIEW", ev.target.checked);
                }
            });
        };
        $scope.selectAll_ACTION_ADD = function (ev) {

            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            items.forEach(function (item) {
                if (ev.target.checked == false) {
                    item.set("ACTION_ADD", ev.target.checked);
                }
                else {
                    item.set("ACTION_ADD", ev.target.checked);
                    item.set("ACTION_MODIFY", ev.target.checked);
                    item.set("ACTION_VIEW", ev.target.checked);
                }
            });
        };

        $scope.selectAll_ACTION_REMOVE = function (ev) {

            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            items.forEach(function (item) {
                if (ev.target.checked == false) {
                    item.set("ACTION_REMOVE", ev.target.checked);
                }
                else {
                    item.set("ACTION_REMOVE", ev.target.checked);
                    item.set("ACTION_VIEW", ev.target.checked);
                }

            });
        };

        $scope.updateRoleFullGrid = function () {
            var userupdateRole = $scope.UserRoleplanGridOptions.dataSource.data();
            $("UserRoleplanGridOptions").kendoGrid({
                autoBind: false,
                dataSource: $scope.UserRoleDatasource
            });
           
            // $scope.UserRoleDatasource.read(); // "read()" will fire the "change" event of the dataSource and the widget will be bound
            // data = $scope.UserRoleplanGridOptions.dataSource.data();
            $scope.selectedRole.PLAN_ID = $scope.ROLE_ID;
            $scope.selectedRole.FunctionModels = $scope.selectedPlanData;
            dataFactory.saveUserRoleFunctions($scope.selectedRole).success(function (response) {
                $scope.UserRoleDatasource.read();
                //$scope.dataSource.read();

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + response + '.',
                    type: "confirm",
                    buttons: [{ value: "Ok" }
                    ],
                    success: function (result) {
                        if (result === "Ok") {
                            location.reload();
                        }
                    }
                });

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.updateRole = function (e, id) {

            if (e.target.id === "view") {
                id.dataItem.set("ACTION_VIEW", e.target.checked);
                if (e.target.checked === false) {
                    id.dataItem.set("ACTION_MODIFY", e.target.checked);
                    id.dataItem.set("ACTION_ADD", e.target.checked);
                    id.dataItem.set("ACTION_REMOVE", e.target.checked);
                    id.dataItem.set("ACTION_DETACH", e.target.checked);
                }
            }
            else if (e.target.id === "modify") {
                if (e.target.checked === true) {
                    id.dataItem.set("ACTION_MODIFY", e.target.checked);
                    id.dataItem.set("ACTION_VIEW", e.target.checked);
                }
                if (e.target.checked === false) {
                    id.dataItem.set("ACTION_ADD", e.target.checked);
                    id.dataItem.set("ACTION_MODIFY", e.target.checked);
                }

            }
            else if (e.target.id === "add") {
                id.dataItem.set("ACTION_ADD", e.target.checked);
                if (e.target.checked === true) {
                    id.dataItem.set("ACTION_MODIFY", e.target.checked);
                    id.dataItem.set("ACTION_VIEW", e.target.checked);
                }
            }
            else if (e.target.id === "detach") {
                id.dataItem.set("ACTION_DETACH", e.target.checked);
                if (e.target.checked === true) {
                    id.dataItem.set("ACTION_DETACH", e.target.checked);
                }
            }
            else {

                id.dataItem.set("ACTION_REMOVE", e.target.checked);
                if (e.target.checked === true) {
                    id.dataItem.set("ACTION_VIEW", e.target.checked);
                }
            }

            $scope.selectedPlanData.push({
                FUNCTION_GROUP_ID: id.dataItem.FUNCTION_GROUP_ID,
                FUNCTION_ID: id.dataItem.FUNC_ID,
                FUNCTION_NAME: id.dataItem.FUNCTION_NAME,
                ACTION_VIEW: id.dataItem.ACTION_VIEW,
                ACTION_MODIFY: id.dataItem.ACTION_MODIFY,
                ACTION_ADD: id.dataItem.ACTION_ADD,
                ACTION_REMOVE: id.dataItem.ACTION_REMOVE,
                ACTION_DETACH: id.dataItem.ACTION_DETACH
            });
        };




        $scope.userRolesFunctionsAllowedGrid = {
            dataSource: $scope.userRolesFunctionsAllowedGridDatasource,
            autoBind: true,
            sortable: false,
            pageable: { buttonCount: 5 },
            columns: [
                { field: "FUNCTION_NAME", title: "Active Features", width: "250px" },
                { field: "ACTION_VIEW", title: "Action View", width: 150, template: '<input type="checkbox" #= ACTION_VIEW?checked="checked" : ""></input>' },
                { field: "ACTION_MODIFY", title: "Action Modify", width: 150, template: '<input type="checkbox" #= ACTION_MODIFY?checked="checked" : ""></input>' },
                { field: "ACTION_ADD", title: "Action Add", width: 130, template: '<input type="checkbox" class="chkbx3" #= ACTION_ADD ? \'checked="checked"\' : "" ></input>' },
                { field: "ACTION_REMOVE", title: "Action Remove", width: 130, template: '<input type="checkbox" class="chkbx4" #= ACTION_REMOVE ? \'checked="checked"\' : ""></input>' }

            ],
            editable: true
        };

        $("#userRolesFunctionsAllowedGrid .k-grid-content").on("change", "input.chkbx1", function (e) {
            var grid = $("#userRolesFunctionsAllowedGrid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
            dataItem.set("ACTION_VIEW", this.checked);
        });

        $("#userRolesFunctionsAllowedGrid .k-grid-content").on("change", "input.chkbx2", function (e) {
            var grid = $("#userRolesFunctionsAllowedGrid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
            dataItem.set("ACTION_MODIFY", this.checked);
        });

        $("#userRolesFunctionsAllowedGrid .k-grid-content").on("change", "input.chkbx3", function (e) {
            var grid = $("#userRolesFunctionsAllowedGrid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
            dataItem.set("ACTION_ADD", this.checked);
        });

        $("#userRolesFunctionsAllowedGrid .k-grid-content").on("change", "input.chkbx4", function (e) {
            var grid = $("#userRolesFunctionsAllowedGrid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));
            dataItem.set("ACTION_REMOVE", this.checked);
        });


        $scope.UserRolesListDataSource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    dataFactory.getUserRolesList($scope.CustomerId).success(function (response) {
                        options.success(response);
                        //$scope.loadUserRolesFunctionAllowed(0);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            serverPaging: true,
            pageSize: 5,
            schema: {
                data: function (data) { return data; },
                total: function (data) {
                    return data.length;
                }
            },
            error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "info"
                });
            }
        });
        //$scope.UserRoleDatasource
        $scope.onChange = function (e) {
            var list = e.sender;
            var selectedStatus = list.dataItem(list.select());
            $scope.ROLE_ID = selectedStatus.ROLE_ID;
            //$scope.userRolesFunctionsAllowedGridDatasource.read();
            $scope.UserRoleDatasource.read();
            $scope.UserRoleAttributeDatasourceallow.read();
            $scope.customercatalogGridDatasource.read();

        };
        $scope.onUserRoleChange = function (e) {
          
            $scope.User1.UserRoles = e.sender.value();
            //$scope.userReadOnlyAttributesDataSource.read();
            $scope.catalogDataSource.read();
        };

        $scope.SaveModifiedUserData = function (e) {

            var data = $("#userListViewGrid").data().kendoGrid._data;
            dataFactory.saveModifiedUserData(data).success(function (response) {

                if (response == "Updated")
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Update completed.',
                        type: "info"
                    });
                $scope.userListDataSource.read();
            }).error(function (error) {
                options.error(error);
            });

        };

        // End of Roles Tab Grid

        $scope.userWorkFlow = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,
            pageSize: 5,
            batch: true,
            transport: {
                read: function (options) {
                    dataFactory.getUserWorkFlow().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.userWorkFlowGridOptions = {
            dataSource: $scope.userWorkFlow,
            autoBind: true,
            sortable: false,
            pageable: { buttonCount: 5 },
            columns: [
                { field: "TB_USER_ID", title: "Users", width: "80px" },
                { field: "SUBMIT", title: "Submit", width: 150, template: '<input type="checkbox" #= SUBMIT ? "checked=checked" : "" # ng-click="updateSelection($event, this)"></input>' },
                { field: "APPROVE", title: "Approve", width: 150, template: '<input type="checkbox" #= APPROVE ? "checked=checked" : "" # ng-click="updateApproveSelection($event, this)"></input>' }

            ],
            editable: true
        };

        $scope.updateSelection = function (e, id) {
            if (id.dataItem.SUBMIT != true) {
                id.dataItem.set("SUBMIT", e.target.checked);
                id.dataItem.set("APPROVE", false);
            } else {
                id.dataItem.set("SUBMIT", false);
                id.dataItem.set("APPROVE", e.target.checked);
            }
        };
        $scope.updateApproveSelection = function (e, id) {

            if (id.dataItem.APPROVE == true) {
                id.dataItem.set("SUBMIT", e.target.checked);
                id.dataItem.set("APPROVE", false);
            } else {
                id.dataItem.set("SUBMIT", false);
                id.dataItem.set("APPROVE", e.target.checked);
            }
        };

        $scope.userWorkFlowUpdate = function (userWorkFlow) {
            var userWorkFlow1 = $scope.userWorkFlowGridOptions.dataSource.data();
            $("userWorkFlowGridOptions").kendoGrid({
                autoBind: false,
                dataSource: $scope.userWorkFlow
            });
            $scope.userWorkFlow.read(); // "read()" will fire the "change" event of the dataSource and the widget will be bound

            userWorkFlow = $scope.userWorkFlowGridOptions.dataSource.data();
            dataFactory.saveUserWorkFlow(userWorkFlow).success(function (response) {
                options.success(response);
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.GetUsersFunctionAllowedDataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            batch: true,
            transport: {
                read: function (options) {
                    dataFactory.getUsersFunctionAllowed(options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.userselects = function (catalogid) {
            $http.get("../Catalog/GetCatalog?CatalogID=" + catalogid).
                then(function (categoryDetails) {
                    $scope.User = categoryDetails.data[0];
                });
        };

        $scope.userChange = function (e) {
            $scope.userselects(e.sender.value());
        };

        $scope.addRole = function () {
           
            $("#ddluserrole2").hide();
            $("#userGrid").hide();
            $("#newRoleEntryView").show();
            $("#saverole").show();
            $scope.Role.RoleId = '';
        };
        $scope.back = function () {
            //$scope.userRoleforGroupdropdowndatasource.read();
            //$("#roleListView").show();
            $("#ddluserrole2").show();
            $("#newRoleEntryView").hide();
            $("#userGrid").show();
            //$("#addrole").show();
            /// $("#saverole").hide();
        };


        $scope.saveRole = function (roleid) {

            if ($scope.Role.RoleId.trim() == '') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter the Role Name.',
                    //type: "info"
                });
                return;
            }
            $scope.saveFunctionAllowedAttributes();
            dataFactory.saveuserrole(roleid).success(function (response) {
                var id = response.split('~');
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + id[0] + '.',
                    //type: "info"
                });
                $scope.ROLE_ID = id[1];
                $scope.UserRolesListDataSource.read();
                $scope.UserRoleDatasource.read();
                $scope.UserRoleAttributeDatasourceallow.read();
                $scope.userRoledropdowndatasource.read();
                $scope.userRoleforGroupdropdowndatasource.read();
            }).error(function (error) {
                options.error(error);
            });
            //$("#roleListView").show();
            //$("#newRoleEntryView").hide();
            //$("#saverole").show();
            //$("#addrole").show();
            $scope.selectedPlanData = [];
        };
        $scope.IsUserEdit = "false";
        $scope.addNewUser = function () {
           
            $("#showfilter").hide();
            $scope.organisationloadDataSource = new kendo.data.DataSource({
                type: "json",
                async: false,
                editable: false,
                serverSorting: true,
                serverFiltering: true,
                sortable: true,
                batch: true,
                transport: {
                    read: function (options) {
                        dataFactory.loadOrganizationName().success(function (response) {
                            options.success(response);

                        }).error(function (error) {
                            options.error(error);
                        });

                    }
                },

                sort: { field: "organisationName", dir: "desc" },
                cache: false

            });
            $("#showUserImportdiv").hide();
            $("#hideExportTypeDiv").hide();
            $scope.Editflag = false;
            $scope.User1.Customer_User_Id = 0;
            $scope.User1.User_Name = '';
            $scope.User1.LastName = '';
            $scope.User1.FirstName = '';
            $scope.User1.Title = '';
            $scope.User1.Phone = '';
            $scope.User1.Fax = '';
            $scope.User1.City = '';
            $scope.User1.State = '';
            $scope.User1.Zip = '';
            $scope.User1.Country = '';
            $scope.User1.Active = "true";
            $scope.IsUserEdit = "false";
            $scope.User1.Address = '';
            $scope.User1.Email = '';
            $scope.User1.Password = '';
            $scope.User1.ConfirmPassword = '';
            $("#userListView").hide();
            $("#newUserEntryView").show();
            //$scope.showNewUserBtn = false;
            var data = $("#ddluserrole").data("kendoDropDownList");
            if (data.dataSource._data.length > 0) {
                $scope.User1.UserRoles = data.dataSource._data[0].ROLE_ID;
                $scope.catalogDataSource.read();
            }
        };

        $scope.cancelNewUser = function () {
           
            //$scope.showNewUserBtn = true;
            $("#showfilter").show();
            $("#userbtn").show();
            $("#hideExportTypeDiv").show();
            // $("#hideExportTypeDiv").hide();
            $("#showUserImportdiv").hide();
            $("#userListView").show();
            $("#newUserEntryView").hide();
            $scope.Userweb.FirstName = "";
            $scope.Userweb.LastName = "";
            $scope.Userweb.Email = "";
            //$scope.Userweb.Phone = response[0].PHONE;
            //$scope.Userweb.Mobile = response[0].MOBILE;
            //$scope.Userweb.Skype = response[0].SKYPE_ID;
            ///$scope.Userweb.WhatsApp = response[0].WHATSAPP_NUMBER;
            $scope.Userweb.Company = "";
            //$scope.Userweb.Designation = response[0].DESIGNATION;
            $scope.Userweb.Notes = "";
            $scope.Userweb.active = "";
            $scope.Userweb.role = "";
            $scope.Userweb.organisation = "";
            $scope.value = "";

        };

        //Active Directory
        //Modified by Jothipriya at April 20 2022
        $scope.SaveWindowsUserDetails = function () {
            //Active Directory Update Process 
            $scope.SysUser_Name = $scope.User1.SysUser_Name.trim();
            $scope.SysDomain_Name = $scope.User1.SysDomain_Name.trim();
            $scope.Status = 1; /* Active User */

            if ($scope.SysDomain_Name.trim() != "" && $scope.SysUser_Name.trim() != "" && $scope.SysDomain_Name.trim() != undefined && $scope.SysUser_Name.trim() != undefined) {
                $scope.User1.User_Name = $scope.User1.Email;
                dataFactory.SaveWindowsUserDetails($scope.User1.User_Name, $scope.SysUser_Name, $scope.SysDomain_Name, $scope.Status).success(function (response) {
                    if (response != null)
                        options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }

            //Active Directory Update Process 
        }

        
        $scope.saveNewUser = function (user) {
          

            $("#hideExportTypeDiv").show();
            $("#showUserImportdiv").hide();

            //To get the User_Email...
            $scope.User1.User_Name = $scope.User1.Email;

            if (user.Customer_User_Id != 0) {
                // To Validate Zip Code...
                var re = /^[a-zA-Z0-9]{5,6}$/;
                if ($scope.User1.Zip) {
                    if (!re.test($scope.User1.Zip)) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please enter a valid ZIP Code.',
                            type: "error"
                        });
                       
                        $scope.SaveWindowsUserDetails.stopPropagation();
                        return false;


                    }
                   
                }

                //To Validate Phone Number..
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                if ($scope.User1.Phone) {
                    if (!$scope.User1.Phone.match(phoneno)) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please enter a valid Phone Number.',
                            //type: "error"
                        });
                        $scope.SaveWindowsUserDetails.stopPropagation();
                        return false;
                    }
                }

                //To check the Mandatory fields...
                $scope.UserRoles;
                if ($scope.User1.selectedIds != "" && $scope.User1.FirstName != "" && $scope.User1.FirstName != undefined && $scope.User1.organisation != "" && $scope.roleNames != "") {

                    //To collect the role name of the selectedids in an array...
                    roleNames = [];
                    angular.forEach($scope.User1.selectedIds, function (value, key) {
                        roleNames.push(value.roleName);
                    });

                    $scope.roleNames = roleNames;

                    //To update the User_RoleName in database..
                    dataFactory.Updateuserdetails(user, $scope.roleNames).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });

                        //To display the UI controls...
                        $("#userListView").show();
                        $("#showfilter").show();
                        $("#newUserEntryView").hide();


                        $scope.userListDataSource.read();

                        return true;

                    }).error(function (error) {
                        options.error(error);
                        return false;
                    });

                } //End of mandatory fields if loop

                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Mandatory fields cannot be empty.',
                        //type: "info"
                    });
                    $scope.SaveWindowsUserDetails.stopPropagation();
                    return false;
                }


            }//End of User_ID If loop.. 
            else {

                //To validate the Catalog_Id...
                if (user.CATALOG_ID === undefined) {
                    if (user.Active == 'true') {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please select a Catalog for the User.'
                        });
                        $scope.SaveWindowsUserDetails.stopPropagation();
                        return false;
                    }
                }
                //To Validate Customer_User_ID...
                if ($scope.User1.Customer_User_Id == 0) {
                    if ($scope.User1.Password.trim() == "" || $scope.User1.ConfirmPassword.trim() == "" || $scope.User1.FirstName.trim() == "" || $scope.User1.Email.trim() == "") {
                        $scope.SaveWindowsUserDetails.stopPropagation();
                        return false;
                    }
                }
                else {
                    if ($scope.User1.FirstName.trim() == "" || $scope.User1.Email.trim() == "") {
                        $scope.SaveWindowsUserDetails.stopPropagation();
                        return false;
                    }
                }
                //To check if the User_Role is selected..
                if ($scope.User1.selectedIds == "") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select a Role for the User.',
                        //type: "info"
                    });
                    $scope.SaveWindowsUserDetails.stopPropagation();
                    return false;
                }
                //To validate the Email_ID...
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!re.test($scope.User1.Email)) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please enter a valid Email Address.',
                        type: "error"
                    });
                    $scope.SaveWindowsUserDetails.stopPropagation();
                    return false;
                }
                //To validate the Zip code...
                var re = /^[a-zA-Z0-9]{5,6}$/;
                if ($scope.User1.Zip) {
                    if (!re.test($scope.User1.Zip)) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please enter a valid ZIP Code.',
                            type: "error"
                        });
                        $scope.SaveWindowsUserDetails.stopPropagation();
                        return false;
                    }
                }
                //To validate the password length...
                if (($scope.User1.Password.length < 6 || $scope.User1.Password.length > 12) && $scope.User1.Customer_User_Id == 0) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Password should contain a minimum of 6 characters and a maximum of 12 characters.',
                        //type: "error"
                    });
                    $scope.SaveWindowsUserDetails.stopPropagation();
                    return false;
                }
                //To validate the phone number...
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                if ($scope.User1.Phone) {
                    if (!$scope.User1.Phone.match(phoneno)) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please enter a valid Phone Number.',
                            //type: "error"
                        });
                        $scope.SaveWindowsUserDetails.stopPropagation();
                        return false;
                    }
                }

                if ($scope.User1.Customer_User_Id > 0) {


                    if ($scope.User1.UserRoles != "" && $scope.User1.FirstName != "") {
                        $scope.User1.User_Name = $scope.User1.Email;
                        dataFactory.saveuserdetails(user, $scope.User1.UserRoles).success(function (response) {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            if (response == "A user name for that e-mail address already exists, please enter a different e-mail address" || response == "User name already exists, please enter a different Name") {
                                $("#userListView").hide();
                                $("#newUserEntryView").show();
                            } else {
                                $("#userListView").show();
                                $("#newUserEntryView").hide();
                            }

                            $scope.userListDataSource.read();
                            return true;
                        }).error(function (error) {
                            options.error(error);
                            $scope.SaveWindowsUserDetails.stopPropagation();
                            return false;
                        });

                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Mandatory fields cannot be empty.',
                            //type: "info"
                        });
                        $scope.SaveWindowsUserDetails.stopPropagation();
                        return false;
                    }
                }
                
                else if ($scope.User1.User_Name != "" && $scope.User1.FirstName != undefined && $scope.User1.Password != "" && $scope.User1.ConfirmPassword != "" && $scope.User1.selectedIds != "" && $scope.User1.FirstName != "" && $scope.User1.Email != "") {
                    if ($scope.User1.Password == $scope.User1.ConfirmPassword) {
                     
                        roleNames = [];
                        angular.forEach($scope.User1.selectedIds, function (value, key) {
                            roleNames.push(value.roleName);
                        });

                        $scope.roleNames = roleNames;
                        dataFactory.saveuserdetails(user, $scope.roleNames).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            if (response == "A user name for that e-mail address already exists, please enter a different e-mail address" || response == "User name already exists, please enter a different Name") {
                                $("#userListView").hide();
                                $("#newUserEntryView").show();
                            } else {
                                $("#userListView").show();
                                $("#newUserEntryView").hide();
                            }

                            $scope.userListDataSource.read();
                            return true;
                        }).error(function (error) {
                            options.error(error);
                            $scope.SaveWindowsUserDetails.stopPropagation();
                            return false;
                        });

                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Entered Password & Confirm Password do not match.',
                            type: "error"
                        });
                        $scope.SaveWindowsUserDetails.stopPropagation();
                        return false;
                    }
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Mandatory fields cannot be empty.',
                        //type: "info"
                    });
                    $scope.SaveWindowsUserDetails.stopPropagation();
                    return false;
                }

            }

        };
        //Jothipriya
        //Active directory end
        //END
        
        //$('#roleListgrid').kendoGrid({
        $scope.saveFunctionAllowedAttributes = function () {
            var grid = $('#roleListgrid').data("kendoGrid");
            $(".roleListgrid").each(function (index, value) {
                var grid = $(this).data("kendoGrid");
                grid.dataSource.sync();
                dataFactory.saveUserWorkFlow(grid).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            });
        };
        $scope.showEditUserRolesFunction = function (item) {
            $scope.winEditUserRolesFunction.refresh({ url: "../Views/App/Partials/editUserRolesFunction.html" });
            $scope.winEditUserRolesFunction.title("Update User Roles Allowed Function");
            $scope.winEditUserRolesFunction.wrapper.css({
                width: 500,
                height: 200
            });
            $scope.winEditUserRolesFunction.center().open();
            $scope.editUserRolesFunction = {};
            $scope.editUserRolesFunction = item.dataItem;
            //$scope.$apply();
        };

        $scope.updateUserRolesAllowedFunction = function () {
            dataFactory.updateUserRolesAllowedFunction($scope.editUserRolesFunction).success(function (response) {
                options.success(response);
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + response + '.',
                    type: "info"
                });
                $scope.loadUserRolesFunctionAllowed(0);
                $scope.winEditUserRolesFunction.close();
            }).error(function (error) {
                options.error(error);
                $scope.error = "An Error has occured while Updating User Roles Allowed Function! " + error;
                $scope.loading = false;
            });
        };

        $scope.companyDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: function (options) {
                    dataFactory.GetCompanyList().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.companyChange = function (e) {
            if (e.sender.value() == "") {
                $scope.CustomerId = '0';
            }
            else {
                $scope.CustomerId = e.sender.value();
            }
            $scope.userListDataSource.read();
        };

        $scope.userNameChange = function (e) {
            if (e.sender.value() == "") {
                $scope.myProfileName = $localStorage.getUserName;
            }
            else {
                $scope.myProfileName = e.sender.value();
            }
            dataFactory.getMyprofileListStatus($scope.myProfileName).success(function (response) {
                var rolename = response[0].RoleName.split('_');
                $scope.CustomerName = rolename[1];
                $scope.IsActive = response[0].Active;
                //$scope.User1.UserRoles = $scope.ROLE_ID;
                $scope.User1.UserRoleName = rolename[1];
                // options.success(response[0]);

            }).error(function (error) {
                options.error(error);
            });
            $scope.userFunctionsAllowedGridDatasource.read();
            $scope.userReadOnlyAttributesDataSource.read();
        };

        $scope.userRoledropdowndatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetUsersFordropdown().success(function (response) {
                        options.success(response);
                        $scope.getName = response;
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.closeEdittUserRolesAllowedFunction = function () {
            $scope.winEditUserRolesFunction.close();
        };
        var i = 0;
        $scope.UserRolesListDataSource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    dataFactory.getUserRolesList($scope.CustomerId).success(function (response) {
                        options.success(response);
                        //$scope.loadUserRolesFunctionAllowed(0);
                        //if (i == 0) {
                        if ($scope.UserRolesListDataSource._data.length > 0) {

                            $scope.ROLE_ID = $scope.UserRolesListDataSource._data[0].ROLE_ID;
                            $scope.Defaultrole_id = $scope.UserRolesListDataSource._data[0].ROLE_ID;
                            //$scope.userRolesFunctionsAllowedGridDatasource.read();
                            $scope.UserRoleDatasource.read();
                            $scope.UserRoleAttributeDatasourceallow.read();
                            $scope.customercatalogGridDatasource.read();
                            if ($scope.ROLE_ID != 0) {
                                $scope.dataSource.read();
                                //this.$('.gridActive').find('div:first')[0].className = 'list-group-item clearfix ng-scope k-state-selected';
                            }
                            //    i = 1;

                            //} else {

                            //}
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            serverPaging: true,
            pageSize: 5,
            schema: {
                data: function (data) { return data; },
                total: function (data) {
                    return data.length;
                }
            },
            error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }
        });
        //$scope.UserRoleDatasource
        $scope.onChange = function (e) {
            var list = e.sender;
            var selectedStatus = list.dataItem(list.select());
            $scope.ROLE_ID = selectedStatus.ROLE_ID;
            //$scope.userRolesFunctionsAllowedGridDatasource.read();
            $scope.UserRoleDatasource.read();
            $scope.UserRoleAttributeDatasourceallow.read();
            $scope.customercatalogGridDatasource.read();
            if ($scope.ROLE_ID != 0)
                $scope.dataSource.read();
        };

        $scope.SaveModifiedUserData = function (e) {
            var data = $("#userListViewGrid").data().kendoGrid._data;
            dataFactory.saveModifiedUserData(data).success(function (response) {
                if (response == "Updated")
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Update completed.',
                        type: "info"
                    });

                if (response == "userexceeded")
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'You have reached the maximum limit of Active Users, please contact your administrator or support@questudio.com.',
                        //type: "info"
                    });

                $scope.userListDataSource.read();
            }).error(function (error) {
                options.error(error);
            });

        };

        // End of Roles Tab Grid

        $scope.userWorkFlow = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            batch: true,
            serverPaging: false,
            serverSorting: true,
            transport: {
                read: function (options) {
                    dataFactory.getUserWorkFlow($scope.CustomerId).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.userWorkFlowGridOptions = {
            dataSource: $scope.userWorkFlow,
            autoBind: true,
            sortable: false,
            pageable: false,
            columns: [
                { field: "TB_USER_ID", title: "Users", width: "80px" },
                { field: "SUBMIT", title: "Submit", width: 150, template: '<input type="checkbox" #= SUBMIT ? "checked=checked" : "" # ng-click="updateSelection($event, this)"></input>' },
                { field: "APPROVE", title: "Approve", width: 150, template: '<input type="checkbox" #= APPROVE ? "checked=checked" : "" # ng-click="updateApproveSelection($event, this)"></input>' }

            ],
            editable: false
        };

        $scope.updateSelection = function (e, id) {
            if (id.dataItem.SUBMIT != true) {
                id.dataItem.set("SUBMIT", e.target.checked);
                id.dataItem.set("APPROVE", false);
            } else {
                id.dataItem.set("SUBMIT", false);
                id.dataItem.set("APPROVE", e.target.checked);
            }
        };
        $scope.updateApproveSelection = function (e, id) {
            if (id.dataItem.APPROVE == true) {
                id.dataItem.set("SUBMIT", e.target.checked);
                id.dataItem.set("APPROVE", false);
            } else {
                id.dataItem.set("SUBMIT", false);
                id.dataItem.set("APPROVE", e.target.checked);
            }
        };

        $scope.userWorkFlowUpdate = function (userWorkFlow) {
            var userWorkFlow1 = $scope.userWorkFlowGridOptions.dataSource.data();
            $("userWorkFlowGridOptions").kendoGrid({
                autoBind: false,
                dataSource: $scope.userWorkFlow
            });


            userWorkFlow = $scope.userWorkFlowGridOptions.dataSource.data();
            dataFactory.saveUserWorkFlow(userWorkFlow).success(function (response) {
                $scope.userWorkFlow.read(); // "read()" will fire the "change" event of the dataSource and the widget will be bound
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + response + '.',
                    type: "info"
                });
                //options.success(response);
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.activeReadclick = function (e, id) {
            $scope.readCatalogVal = e.target.checked;

            if (!id.dataItem.ACTIVE && $scope.readCatalogVal) {
                $scope.val = true;
                $scope.activeclick(e, id);
            }
            //$scope.customercatalogGridDatasource._data

            $scope.changedcatalogid = id.dataItem.CATALOG_ID;
            dataFactory.updatecustomerrolecatalog($scope.ROLE_ID, $scope.CustomerId, $scope.changedcatalogid, $scope.readCatalogVal, 'read').success(function (response) {
            }).error(function (error) {
                options.error(error);
            });
            $scope.customercatalogGridDatasource.read();
        };


        $scope.activeclick = function (e, id) {
            var activeCount = 0;
            angular.forEach($scope.CustomerAllCatalog, function (value) {
                if (value.ACTIVE == true) {
                    activeCount = activeCount + 1;
                }
            });
            if (activeCount == 1 && e.target.checked == false) {
                e.target.checked = true;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Select at least one catalog.',
                    type: "error"
                });
                return;
            }
            else {
                $scope.val = e.target.checked;
                if (!$scope.val) {
                    $scope.readCatalogVal = false;
                    $scope.activeReadclick(e, id);
                }


                $scope.changedcatalogid = id.dataItem.CATALOG_ID;

                dataFactory.updatecustomerrolecatalog($scope.ROLE_ID, $scope.CustomerId, $scope.changedcatalogid, $scope.val, 'active').success(function (response) {
                    if (response == "No") {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Select at least one catalog.',
                            type: "error"
                        });
                    }
                }).error(function (error) {
                    options.error(error);
                });
                $scope.CustomerAllCatalog = [];
                $scope.customercatalogGridDatasource.read();
            }
        };
        $scope.CustomerAllCatalog = [];
        $scope.customercatalogGridDatasource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    //            dataFactory.getAllCatalogsByRoleId($scope.ROLE_ID, $scope.CustomerId).success(function (response) {
                    //                options.success(response);
                    //            }).error(function (error) {
                    //                options.error(error);
                    //            });
                    dataFactory.getAllCatalogsByRoleId($scope.ROLE_ID, $scope.CustomerId, options.data).success(function (response) {
                        if (response.Data.length > 0) {
                            if (response !== "" && response !== null && response.Data.length > 0) {

                                options.success(response);
                            }
                            angular.forEach(response.Data, function (data) {
                                if ($scope.CustomerAllCatalog.length == 0) {
                                    $scope.CustomerAllCatalog.push(data);
                                }
                                else {
                                    var idCount = 0;
                                    angular.forEach($scope.CustomerAllCatalog, function (data1) {
                                        if (data1.CATALOG_ID == data.CATALOG_ID) {
                                            idCount = idCount + 1;
                                        }
                                    });
                                    if (idCount == 0) {
                                        $scope.CustomerAllCatalog.push(data);
                                    }
                                }
                            });
                        }
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                }

            },
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "CATALOG_ID",
                    fields: {
                        CATALOG_Name: { type: "string" },
                        ACTIVE: { type: "boolean" }
                    }
                }
            },
            serverPaging: false,
            serverSorting: false,
            serverFiltering: false,

            error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }
        });
        $scope.customercatalogGrid = {
            autoBind: false,
            dataSource: $scope.customercatalogGridDatasource,
            filterable: false,
            sortable: true,
            pageable: false,
            batch: true,
            columns: [
                { field: "CATALOG_NAME", title: "Catalog Name", width: "365px" },
                { field: "ACTIVE", title: "Active", template: '<input type="checkbox" id="ACTIVE" #= ACTIVE?checked="checked" : "" # ng-click="activeclick($event, this)"></input>' },
            { field: "READ", title: "Read", template: '<input type="checkbox" id="READ" #= READ?checked="checked" : "" # ng-click="activeReadclick($event, this)"></input>' }
            ],
            editable: {
                update: false,
            }

        };


        $scope.catalogChange = function (e) {
            if (e.sender.selectedIndex == 0) {
                $scope.User1.CATALOG_ID = 0;
            }
            //if (e.sender.value() !== "") {
            //    $scope.selects(e.sender.value());
            //    $scope.attrcount = 0;
            //    $scope.prodimageattrcount = 0;
            //    $scope.familydesccount = 0;
            //    $scope.prodpriceattrcount = 0;
            //    $scope.prodkeyattrcount = 0;
            //    $scope.familyimgcount = 0;
            //    $scope.familyattrcount = 0;
            //    $scope.familypricecount = 0;
            //    $scope.familykeycount = 0;
            //    $scope.selectcatalogattr(e.sender.value());
            //    $scope.dataSource.read();
            //    $scope.treeData.read();
            //    $scope.catalogfamilyattrDataSource.read();
            //    $scope.catalogproductattrDataSource.read();
            //    $scope.familyDataSource.read();

            //}
        };

        $scope.getUserSuperAdmin = function () {

            dataFactory.getUserSuperAdmin().success(function (response) {
                if (response !== "" && response !== null) {
                    if (response == "1") {
                        $rootScope.userSuperAdmin = true;
                        $rootScope.userAdmin = false;
                        $rootScope.userAdminUsers = false;
                    }
                    else if (response == "2") {
                        $rootScope.userSuperAdmin = false;
                        $rootScope.userAdmin = true;
                        $rootScope.userAdminUsers = false;
                    }
                    else {
                        $rootScope.userSuperAdmin = false;
                        $rootScope.userAdmin = false;
                        $rootScope.userAdminUsers = true;
                    }

                }

            }).error(function (error) {
                options.error(error);
            });
        };






        $scope.dataSource = new kendo.data.HierarchicalDataSource({
            type: "json",
            serverFiltering: false,
            transport: {
                read: function (options) {
                    dataFactory.getRoleFunctionGroup($scope.ROLE_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "FUNCTION_GROUP_ID",
                    fields: {
                        FUNCTION_GROUP_ID: { editable: false, nullable: true },
                        FUNCTION_GROUP_NAME: { editable: false, validation: { required: true } },
                        SORT_ORDER: { type: "number", validation: { required: true, min: 1 } },
                        DEFAULT_ACTION_ALLOW: { editable: false, nullable: true }
                    }
                }
            }
        });

        $scope.roleplanMainGridOptions = {
            dataSource: $scope.dataSource,
            pageable: false,
            selectable: "row",
            columns: [
                //{ field: "SORT_ORDER", title: "Sort Order", template: "#=SORT_ORDER#" },
                //  { field: "FUNCTION_GROUP_ID", title: " ID" },
                { field: "FUNCTION_GROUP_NAME", title: "MarketStudio Modules" },
                { field: "DEFAULT_ACTION_ALLOW", title: "Active", width: 160, template: '<input type="checkbox" id="allow" class="chkbx5" #= DEFAULT_ACTION_ALLOW ?checked="checked" : "" # ng-click="roleplanGroupfunctionset($event, this)"> Check All</input>' }],

            editable: false, detailTemplate: kendo.template($("#plantemplate").html()),
            dataBound: function () {
                //    this.expandRow(this.tbody.find("tr.k-master-row").first());
                // this.expandRow(this.tbody.find("tr.k-master-row"));
            }
        };




        $scope.roleplanDetailGridOptions = function (dataItem) {
            return {
                dataSource: {
                    type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.getRolePlanFunctionsByGroupId($scope.ROLE_ID, dataItem.FUNCTION_GROUP_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "FUNCTION_GROUP_ID",
                            fields: {
                                FUNCTION_GROUP_ID: { editable: false, nullable: true },
                                FUNC_ID: { editable: false, nullable: true },
                                FUNCTION_NAME: { editable: false, nullable: true },
                                //ACTION_VIEW: { editable: false, nullable: true },
                                //ACTION_MODIFY: { editable: false, nullable: true },
                                //ACTION_ADD: { editable: false, nullable: true },
                                //ACTION_REMOVE: { editable: false, nullable: true },
                            }
                        }
                    },
                    serverPaging: false,
                    serverSorting: true,
                    serverFiltering: false

                },
                scrollable: true,
                sortable: true,
                selectable: "row",
                pageable: false,
                columns: [
                    //{ field: "FUNCTION_GROUP_ID", title: "Sort Order", template: "#=FUNCTION_GROUP_ID#" },
                    //  { field: "FUNC_ID", title: "FUNCTION ID", width: 150, template: "#=FUNC_ID#" },
                    { field: "FUNCTION_NAME", title: "Active Features" },
                    { field: "ACTION_VIEW", title: "View", width: 130, template: '<input type="checkbox" id="view"  class="mc-checkbox text-center" #= ACTION_VIEW  && ALLOW_VIEW ? checked="checked" : !ACTION_VIEW && ALLOW_VIEW ? checked="" : "checked disabled" # ng-click="updateRole($event, this)"></input>' },
                    { field: "ACTION_MODIFY", title: "Modify", width: 130, template: '<input type="checkbox" id="modify" class="chkbx5" #= ACTION_MODIFY && ALLOW_MODIFY ? checked="checked" : !ACTION_MODIFY && ALLOW_MODIFY ? checked="" : "checked disabled" # ng-click="updateRole($event, this)"></input>' },
                    { field: "ACTION_ADD", title: "Add / Allow", width: 130, template: '<input type="checkbox" id="add" class="chkbx5" #= ACTION_ADD && ALLOW_ADD ? checked="checked" : !ACTION_ADD && ALLOW_ADD ? checked="" : "checked disabled" # ng-click="updateRole($event, this)"></input>' },
                    { field: "ACTION_REMOVE", title: "Remove", width: 130, template: '<input type="checkbox" id="remove" class="chkbx5" #= ACTION_REMOVE && ALLOW_REMOVE ? checked="checked" : !ACTION_REMOVE && ALLOW_REMOVE ? checked="" : "checked disabled"  # ng-click="updateRole($event, this)" ></input>' },
                    { field: "ACTION_DETACH", title: "Detach", width: 130, template: '<input type="checkbox" id="detach" class="chkbx5" #= ACTION_DETACH && ALLOW_DETACH ? checked="checked" : !ACTION_DETACH && ALLOW_DETACH ? checked="" : "checked disabled"  # ng-click="updateRole($event, this)" ></input>' }
                ],

                editable: false,
                dataBound: function () {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#grid").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {
                        //  this.expandRow(this.tbody.find("tr.k-master-row"));
                        //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                    }
                }
            };
        };


        $scope.roleplanGroupfunctionset = function (e, id) {
            if (e.target.id === "allow") {
                id.dataItem.set("DEFAULT_ACTION_ALLOW", e.target.checked);
                //$scope.selectedPlanDataList.FUNCTION_GROUP_ID = id.dataItem.FUNCTION_GROUP_ID;
                //$scope.selectedPlanDataList.FUNCTION_ID = id.dataItem.FUNCTION_ID;
                //$scope.selectedPlanDataList.FUNCTION_NAME = id.dataItem.FUNCTION_NAME;
                //$scope.selectedPlanDataList.DEFAULT_ACTION_ALLOW = e.target.checked;
                $scope.selectedPlanData.push({
                    FUNCTION_GROUP_ID: id.dataItem.FUNCTION_GROUP_ID,
                    FUNCTION_ID: 0,
                    FUNCTION_NAME: id.dataItem.FUNCTION_NAME,
                    ACTION_VIEW: e.target.checked,
                    ACTION_MODIFY: e.target.checked,
                    ACTION_ADD: e.target.checked,
                    ACTION_REMOVE: e.target.checked,
                    ACTION_DETACH: e.target.checked
                });
                // $scope.selectedPlanData.push($scope.selectedPlanDataList);
                $scope.updateRoleFullGrid();
                $scope.dataSource.read();
                $scope.selectedPlanData = [];
            }
        };


        $scope.myprofileFunctionsdataSource = new kendo.data.HierarchicalDataSource({
            type: "json",
            serverFiltering: false,
            transport: {
                read: function (options) {
                    dataFactory.getMyProfileFunctionGroup($scope.myProfileName).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "FUNCTION_GROUP_ID",
                    fields: {
                        FUNCTION_GROUP_ID: { editable: false, nullable: true },
                        FUNCTION_GROUP_NAME: { editable: false, validation: { required: true } },
                        SORT_ORDER: { type: "number", validation: { required: true, min: 1 } },
                        DEFAULT_ACTION_ALLOW: { editable: false, nullable: true }
                    }
                }
            }
        });

        //$scope.myprofileplanMainGridOptions = {
        //    dataSource: $scope.myprofileFunctionsdataSource,
        //    pageable: false,
        //    selectable: "row",
        //    columns: [
        //        //{ field: "SORT_ORDER", title: "Sort Order", template: "#=SORT_ORDER#" },
        //        //  { field: "FUNCTION_GROUP_ID", title: " ID" },
        //        { field: "FUNCTION_GROUP_NAME", title: "Group Name" },
        //        { field: "DEFAULT_ACTION_ALLOW", title: "Active", width: 130, template: '<input type="checkbox" id="allow" disabled class="chkbx5" #= DEFAULT_ACTION_ALLOW ?checked="checked" : "" # ng-click="roleplanGroupfunctionset($event, this)"></input>' }],

        //    editable: false, detailTemplate: kendo.template($("#myprofileplantemplate").html()),
        //    dataBound: function () {
        //        //   this.expandRow(this.tbody.find("tr.k-master-row").first());
        //        //  this.expandRow(this.tbody.find("tr.k-master-row"));
        //    }
        //};


        //$scope.myprofileplanDetailGridOptions = function (dataItem) {
        //    return {
        //        dataSource: {
        //            type: "json",
        //            transport: {
        //                read: function (options) {
        //                    dataFactory.getMyProfilePlanFunctionsByGroupId($scope.myProfileName, dataItem.FUNCTION_GROUP_ID).success(function (response) {
        //                        options.success(response);
        //                    }).error(function (error) {
        //                        options.error(error);
        //                    });
        //                }
        //            }, schema: {
        //                model: {
        //                    id: "FUNCTION_GROUP_ID",
        //                    fields: {
        //                        FUNCTION_GROUP_ID: { editable: false, nullable: true },
        //                        FUNC_ID: { editable: false, nullable: true },
        //                        FUNCTION_NAME: { editable: false, nullable: true },
        //                        ACTION_ADD: { editable: false, nullable: true },
        //                        ACTION_VIEW: { editable: false, nullable: true },
        //                        ACTION_MODIFY: { editable: false, nullable: true },
        //                        ACTION_REMOVE: { editable: false, nullable: true },
        //                    }
        //                }
        //            },
        //            serverPaging: false,
        //            serverSorting: true,
        //            serverFiltering: false,


        //        },
        //        scrollable: true,
        //        sortable: true,
        //        selectable: "row",
        //        pageable: false,
        //        columns: [
        //            //{ field: "FUNCTION_GROUP_ID", title: "Sort Order", template: "#=FUNCTION_GROUP_ID#" },
        //            // { field: "FUNC_ID", title: "FUNCTION ID", width: 100, template: "#=FUNC_ID#" },
        //            { field: "FUNCTION_NAME", title: "Active Features" },
        //            { field: "ACTION_VIEW", title: "View", width: 130, template: '<input type="checkbox" id="view" disabled class="mc-checkbox" #= ACTION_VIEW ?checked="checked" : "" # ng-click="updateRole($event, this)"></input>' },
        //            { field: "ACTION_MODIFY", title: "Modify", width: 70, template: '<input type="checkbox" id="modify" disabled class="chkbx5" #= ACTION_MODIFY ?checked="checked" : "" # ng-click="updateRole($event, this)"></input>' },
        //            { field: "ACTION_ADD", title: "Add / Allow", width: 70, template: '<input type="checkbox" id="add" disabled class="chkbx5" #= ACTION_ADD ?checked="checked" : "" # ng-click="updateRole($event, this)"></input>' },
        //            { field: "ACTION_REMOVE", title: "Remove", width: 70, template: '<input type="checkbox" id="remove" disabled class="chkbx5" #= ACTION_REMOVE ?checked="checked" : "" # ng-click="updateRole($event, this)"></input>' },
        //            { field: "ACTION_DETACH", title: "Detach", width: 70, template: '<input type="checkbox" id="detach" disabled class="chkbx5" #= ACTION_DETACH ?checked="checked" : "" # ng-click="updateRole($event, this)"></input>' }
        //        ],

        //        editable: true,
        //        dataBound: function () {
        //            if (this.dataSource.data().length === 0) {
        //                var masterRow = this.element.closest("tr.k-detail-row").prev();
        //                $("#grid").data("kendoGrid").collapseRow(masterRow);
        //                masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
        //            }
        //            else {
        //                //  this.expandRow(this.tbody.find("tr.k-master-row"));
        //                //  this.expandRow(this.tbody.find("tr.k-master-row").first());
        //            }
        //        }
        //    };
        //};

        $scope.init = function () {
         
            $("#AddUserToGroup").hide();
            //dataFactory.GetUsersFordropdown().success(function (response) {
            //    options.success(response);
            //});
           
            $scope.myProfileName = $localStorage.getUserName;
            $scope.getUserSuperAdmin();
            $scope.myprofileFunctionsdataSource.read();
            $scope.myProfileName = $localStorage.getUserName;
            if ($localStorage.getCatalogID === undefined) {
                $scope.SelectedCatalogId = 0;
            }
            else {
                $scope.SelectedCatalogId = $localStorage.getCatalogID;
            }
            if ($localStorage.getCustomerID === undefined) {
                $scope.getCustomerID = 0;

            }
            else {
                $scope.getCustomerID = $localStorage.getCustomerID;
            }
            if ($scope.SelectedCatalogId == '') {

                $scope.SelectedCatalogId = $localStorage.getCatalogID;
            }
            $scope.selectedBackupFile = "Database";
            dataFactory.GetApplicationSettingDetails($scope.getCustomerIDs).success(function (response) {
                $scope.WebConfigFiles = response[0];
            });
            dataFactory.getSAASCustomer().success(function (response) {
              
                $scope.GetSAASCustomer = response;
                if ($scope.GetSAASCustomer=="true")
                {
                    $(".BackupRestore").show();
                    $("#CustomSaas").show();
                    $("#MaintenanceSaas").show();
                    $("#AnnouncementSaas").show();


                }
                else
                {
                    $(".BackupRestore").hide();
                    $("#CustomSaas").hide();
                    $("#MaintenanceSaas").hide();
                    $("#AnnouncementSaas").hide();

                }
            }).error(function (error) {
                options.error(error);
            });
        };


        $scope.init();


        //$scope.countryListDataSource = new kendo.data.DataSource({

        //    type: "json",
        //    serverFiltering: true,
        //    transport: {

        //        read: function (options) {


        //            dataFactory.getListofCountry().success(function (response) {
        //                options.success(response);
        //            }).error(function (error) {
        //                options.error(error);
        //            });
        //        }
        //    }
        //});

        $scope.countryCode = "";
        //$scope.stateListDataSource = new kendo.data.DataSource({

        //    type: "json",
        //    serverFiltering: true,
        //    transport: {

        //        read: function (options) {
        //            if ($scope.countryCode !== "" && $scope.countryCode != undefined) {
        //                dataFactory.getListofStates($scope.countryCode).success(function (response) {
        //                    var state = $scope.User1.State;
        //                    options.success(response);
        //                    $scope.User1.State = state;
        //                }).error(function (error) {
        //                    options.error(error);
        //                });
        //            }
        //            else {
        //                dataFactory.getListofStates().success(function (response) {
        //                    options.success(response);
        //                }).error(function (error) {
        //                    options.error(error);
        //                });
        //            }
        //        }
        //    }
        //});

        $scope.countryChange = function (e) {
            $scope.countryCode = e.sender.value();
            //$scope.stateListDataSource.read();
        };

        //User Invite
        $scope.userEmailInvitation = '';
        $scope.userInviteDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true, pageable: true, pageSize: 10,
            transport: {
                read: function (options) {
                    dataFactory.GetUserInvite($scope.userInvitecustomerID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },

            }, schema: {
                model: {
                    id: "ID",
                    fields: {
                        EMAIL: { editable: false },
                        USER_STATUS: { editable: false }
                    }
                }
            }
        });




        $scope.userInvite = {
            dataSource: $scope.userInviteDataSource,
            sortable: true,
            scrollable: true,
            filterable: true,
            pageable: { buttonCount: 5 },
            autoBind: true,
            editable: true,
            columns: [
                { field: "EMAIL", title: "Email" },
                { field: "USER_STATUS", title: "Status" },
                //{ field: "USER_STATUS", title: "Invite", template: "<button class='btn btn-primary btn-xs'style='margin:2px 0px 5px; padding: 5px 35px;'  ng-click='ClkSendInvite(this)' >Send Invitation</button>" },
                { field: "USER_STATUS", template: "#if(USER_STATUS == 'New')  {#<button class='btn btn-primary btn-xs' title='Add a New Invitee' style='margin:2px 0px 5px; padding: 5px 35px;' ng-click='ClkSendInvite(this)'>Send invitaion</button>#} else if(USER_STATUS == 'Registered'){## ##} else {#<button class='btn btn-primary btn-xs'title='Resend invitaion' style='margin:2px 0px 5px; padding: 5px 35px;' ng-click='ClkSendInvite(this)'>Resend invitaion</button>#}#", title: "Invite", },
                { template: "#if(USER_STATUS == 'Registered'){## ##} else {#<button class='btn btn-primary btn-xs'title='Delete' style='margin:2px 0px 5px; padding: 5px 35px;' ng-click='removeUserInvite(this)'>Delete</button>#}#", title: "Delete", }
               //{ title: "Delete", template: "<button class='btn btn-primary btn-xs'style='margin:2px 0px 5px; padding: 5px 35px;'  ng-click='removeUserInvite(this)' >Delete</button>" }
            ],

            dataBound: function () {

            }

            //dataBound: function (e) {
            //
            //    var grid = $("#userInviteValues").data().kendoGrid._data;
            //    //var gridData = grid.dataSource.view();
            //    for (var i = 0; i < grid.length; i++) {
            //
            //        var currentUid = grid[i].ID;
            //        if (grid[i].USER_STATUS == "New") {
            //            var currenRow = grid.table.find("tr[data-ID='" + currentUid + "']");
            //            var editButton = $(currenRow).find(".k-grid-edit");
            //            editButton.hide();
            //        }
            //    }
            //}
        };


        $scope.removeUserInvite = function (row) {
            $scope.userinviteid = row.dataItem.ID;
            $scope.senduserStatus = row.dataItem.USER_STATUS;
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure want to delete the UserInvitee Value?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {

                    if (result === "Yes") {
                        if ($scope.senduserStatus == "New") {
                            dataFactory.RemoveUserInvite($scope.userinviteid).success(function (response) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                                $scope.userInviteDataSource.read();
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Invited users cannot be deleted.',
                                type: "error"
                            });
                        }
                    }
                    else {
                        return;
                        //  $scope.userInviteDataSource.read();
                    }


                }
            });

        };
        $scope.ClkSendInvite = function (row) {
            $scope.userinviteid = row.dataItem.ID;
            $scope.sendInviteEmail = row.dataItem.EMAIL;
            $scope.sendInviteCustomerId = row.dataItem.CUSTOMERID;
            $scope.senduserStatus = row.dataItem.USER_STATUS;
            if ($scope.senduserStatus == "New") {
                dataFactory.SendInviteToUser($scope.userinviteid, $scope.sendInviteCustomerId, $scope.sendInviteEmail).success(function (response) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                    $scope.userInviteDataSource.read();
                }).error(function (error) {
                    option.error(error);
                });
            }
            else {
                $.msgBox({

                    title: $localStorage.ProdcutTitle,
                    content: 'User is already ' + $scope.senduserStatus + '.',
                    type: "info"
                });
            }


        };


        $scope.SaveUserInvite = function () {

            if ($scope.userEmailInvitation == "" || $scope.userEmailInvitation == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a valid Email.',
                    type: "error"
                });


                return;
            }

            if ($scope.userInvitecustomerID == 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Company.',
                    //type: "error"
                });
                return;
            }
            else {
                dataFactory.SaveUserInvite($scope.userInvitecustomerID, $scope.userEmailInvitation).success(function (response) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                    $scope.userEmailInvitation = '';
                    $scope.userInviteDataSource.read();
                }).error(function (error) {
                    option.error(error);
                });
            }
        };

        $scope.SaveModifiedUserInviteData = function () {

            var data = $("#userInviteValues").data().kendoGrid._data;
            dataFactory.saveModifiedInviteUserData(data).success(function (response) {
                if (response == "Updated")
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Update completed.',
                        type: "info"
                    });
                $scope.userInviteDataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };


        $scope.UserInviteCompanyChange = function (e) {
            $scope.userInvitecustomerID == '';
            if (e.sender.value() == "") {
                $scope.userInvitecustomerID = '0';
            }
            else {
                $scope.userInvitecustomerID = e.sender.value();
            }
            $scope.userInviteDataSource.read();
        };
        //End User Invite

        //-----------------------------------------Custom Menu -------------------------------
        //-----------------------------------------Custom Menu save changes----------------------
        $scope.URLPattern = '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})';
        $scope.SaveCustom = function () {
            if ($scope.Header == '' || $scope.URL == '' || $scope.Header == undefined || $scope.URL == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a valid Header and URL.',
                    type: "error",
                });
            }
            else {
                dataFactory.saveCustom($scope.Header, $scope.URL).success(function (response) {
                    if (response == "Saved.") {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Saved successfully.',
                            type: "info",
                        });
                        // $scope.CustomMenuDatasource.read();
                    }
                    else if (response == "Duplicate") {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Header already available.',
                            type: "info",
                        });
                        // $scope.CustomMenuDatasource.read();
                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please enter a valid Header and URL.',
                            type: "info",
                        });
                    }
                    $scope.CustomMenuDatasource.read();
                    $scope.Header = '';
                    $scope.URL = '';
                }).error(function (error) {
                    options.error(error);
                });
            }
        };
        //------------------------------------Save ends---------------
        $scope.CancelCustom = function () {
            $scope.Header = '';
            $scope.URL = '';
        };
        $rootScope.getHeader = function () {

            dataFactory.getHeader().success(function (response) {
                $rootScope.header = response;
                $scope.CustomMenuDatasource.read();
            }).error(function (error) {
                options.error(error);
            });
        }
        /// ---------Custom Menu Grid datasource -------------------------
        $scope.CustomMenuDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: false,
            serverPaging: true, pageSize: 2,
            autoBind: false,
            serverSorting: true, editable: true, pageable: true, batch: true, pageable: { buttonCount: 5 },
            transport: {
                read: function (options) {
                    dataFactory.getHeaderForMenu().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });

                },
                update: function (options) {
                    dataFactory.updateCustomValues($scope.CustomMenuDatasource._data).success(function (response) {

                        options.success(response);
                        if (response == true) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Update successful.',
                                type: "info"
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Please enter vaild Header and URL.',
                                type: "error"
                            });
                        }
                        $scope.CustomMenuDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Header: { editable: true },
                        ID: { editable: false }
                    }
                }
            },
            dataBound: function (e) {
            }
        });
        $scope.GetCustomDetails = {
            dataSource: $scope.CustomMenuDatasource,
            sortable: true, scrollable: true, pageSizes: 2,
            autoBind: true, filterable: true, editable: true,
            pageable: { buttonCount: 5 },
            columns: [

                { field: "Header", title: "Header" },
                { field: "URL", title: "URL" },

              { command: ["edit", { name: "destroy", text: "", width: "10px", template: "<a class='k-button k-button-icontext ' title = \'Delete\' ng-click='\RemoveData($event,this)\'><span class='k-icon k-delete' title = \'Delete\'></span>Delete</a>" }], title: "Actions", width: "165px" }],

            editable: "inline",
            dataBound: function (e) {
            },
        };
        //------------------Custom menu delete function--------------------
        $scope.RemoveData = function (event, e) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to delete this announcement?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    if (result === "Yes") {
                        dataFactory.deleteCustomemenu(e.dataItem.ID).success(function (response) {
                            $scope.CustomMenuDatasource._data.remove(e.dataItem);
                            if (response == true) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Deleted successfully.',
                                    type: "info",
                                });
                                $scope.CustomMenuDatasource.read();
                            }
                            else
                                $scope.CustomMenuDatasource.read();
                        })
                    }
                }
            })
        }

        //user Export

        $scope.userExport = function (userexporttypes) {
           
            $("#usertabclick").removeClass("k-state-active");
            $("#usertabclick").removeClass("k-state-hover");
            $("#usergroupclick").removeClass("k-state-active");
            $("#usergroupclick").removeClass("k-state-hover");
            $("#userpermissionclick").removeClass("k-state-active");
            $("#userpermissionclick").removeClass("k-state-hover");
            $("#userexportclick").addClass("k-state-active");
            $scope.userexporttype = userexporttypes;
            dataFactory.getUserExportDetails(userexporttypes).success(function (response) {
                if (response == "true") {
                    window.open("DownloadUserExport.ashx?");
                    $.msgBox({
                        title: "CatalogStudio",
                        content: "Export completed successfully.",
                        type: "info"
                    });
                }
                else if (response == "false") {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: "Export failed,Please try again later.",
                        type: "info"
                    });
                }
                else if (response == "No Value") {
                    if ($scope.userexporttype == 2) {
                        var val = "Active";
                    }
                    else if ($scope.userexporttype == 3) {
                        var val = "InActive";
                    }
                    $.msgBox({
                        title: "CatalogStudio",
                        content: "Currently, no InActive users.",
                        type: "info"
                    });
                }
                else {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: "Export failed,Please try again later.",
                        type: "info"
                    });
                }

            })
        }

        //Role Export

        $scope.roleExport = function () {

            dataFactory.getRoleExport().success(function (response) {

                if (response == "true") {
                    window.open("DownloadRoleExport.ashx?");
                    $.msgBox({
                        title: "CatalogStudio",
                        content: "Export completed successfully.",
                        type: "info"

                    });
                }

            })

        }

        //$scope.userRoleExport = function (exporttype, values) {
        //    exporttype = exporttype.toUpperCase();
        //    if (exporttype == "USER") {
        //        $scope.userExport(values);
        //    }
        //    else if (exporttype == "ROLE") {
        //        $scope.roleExport();
        //    }

        //}

        $scope.exportchangeevent = function (change) {
            change = change.toUpperCase();
            if (change == "USER") {
                $scope.usertypesdiv = true;
            }
            else if (change == "ROLE") {
                $scope.usertypesdiv = false;
            }

        }


        //User and Role Import
        $scope.userImport = function () {
          
            $("#showRoleImportdiv").show();
            $("#userbtn").hide();
            $("#hideExportTypeDiv").hide();
            $scope.UserUploadDiv = true;
            $scope.RoleUploadDiv = false;
            $scope.userbuttondiv = true;
            $("#Userbuttonhide").show();
            $scope.rolebuttondiv = false;
            $("#Rolebuttonhide").hide();
            $scope.UserImportUploaddiv = true;
            $scope.RoleImportUploaddiv = false;
            $scope.showImportRolediv = true;
            // $("#showRoleImportdiv").show();
            $scope.RoleImportdiv = false;
            $scope.showImportdiv = true;
            $scope.hideImportType = false;
            $scope.Importdiv = true;
            $scope.UserImportdiv = true;
            $scope.hideImportType = true;
            $scope.importtype = "User";
            $rootScope.importtypeValue = "User";
            localStorage.setItem("saveImportType", $rootScope.importtypeValue);
            $("#userListViewGrid").hide();
            $("#WebuserListViewGrid").hide();
            $("#showUserImportdiv").show();
            $scope.$emit("MyFunction");
        }

        $scope.roleImport = function () {

            $scope.UserUploadDiv = false;
            $scope.RoleUploadDiv = true;

            $scope.userbuttondiv = false;
            $("#Userbuttonhide").hide();
            $scope.rolebuttondiv = true;
            $("#Rolebuttonhide").show();
            $scope.UserImportUploaddiv = false;
            $scope.RoleImportUploaddiv = true;
            $("#grid").hide();
            $("#roleListView").hide();
            $scope.showImportRolediv = true;
            $scope.hideImportType = false;
            $scope.Importdiv = true;
            $scope.RoleImportdiv = true;
            $scope.hideImportType = true;
            $scope.importtype = "Role";
            $rootScope.importtypeValue = "Role";
            localStorage.setItem("saveImportType", $rootScope.importtypeValue);
            $("#userListViewGrid").hide();
            $("#WebuserListViewGrid").hide();
            $("#showRoleImportdiv").show();
            $scope.UserImportdiv = false;
            $scope.showImportdiv = false;
            $("#showUserImportdiv").hide();
            $scope.$emit("MyFunction");

        }

        $scope.exportchangeevent = function (change) {
            change = change.toUpperCase();
            if (change == "USER") {
                $scope.usertypesdiv = true;
            }
            else if (change == "ROLE") {
                $scope.usertypesdiv = false;
            }

        }

        //Reset Page
        $scope.ResetMyProfile = function () {
            $("#Usersettingstab").hide();
            $("#AddUserToGroup").hide();
            $scope.init();
        }




        //-------------------------------------Custom Menu End--------------------------------


        //----------------------------------------------- Backup and Restore start ------------------------------

        // Local And Ftp

        $scope.pathselection = "no";
        // $scope.isFtp = false;
        $("#isFtp").hide();
        $("#isFtp1").show();
        $("#isFtp2").show();
        $scope.Export = {
            COMMENTS: ""
        }
        $scope.pathselectionchange = function (e) {
            if ($scope.pathselection == "no") {
                $("#isFtp").hide();
                $("#isFtp1").show();
                $("#isFtp2").show();
                $scope.pathselection = "no";
            }
            else {

                $("#isFtp").show();
                $("#isFtp1").hide();
                $("#isFtp2").hide();
                $scope.pathselection = "Yes";

            }
        }

        // Data And file

        $scope.dataFiles = "no";


        var BackUpListItem = new kendo.data.DataSource({
            data: [
              { ListItem: "ATTRIBUTE LIST", id: 2 },
              { ListItem: "PRODUCTS WITH HIERARCHY", id: 1 },
              //{ ListItem: "PICKLIST", id: 3 },
              { ListItem: "USER AND ROLE", id: 4 },
              { ListItem: "INDESIGN PROJECTS", id: 5 },
              { ListItem: "TABLE DESIGNER", id: 6 },

            ],
            schema: {
                model: {
                    id: "id",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });
        $scope.BackUPRestoreList = {
            dataSource: BackUpListItem,
            selectable: "row",
            columns: [
              { field: "ListItem", title: "Feature" },
               {
                   // field: "ISAvailable", title: "Select", nullable: true, filterable: false, sortable: false, width: "50px", attributes: { style: "text-align:center" }, headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)' />", template: "<input type='checkbox' class='checkbox' />"
                   field: "ISAvailable", width: 150, title: "Select", template: '<input type="checkbox"  class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" # ng-click="SelectionItems($event, this)"  ></input>', headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox'  ng-click='selectAll($event)'/>"
               },

            ]
        };
        var CheckedValues = [];
        $scope.selectAll = function (ev) {
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            items.forEach(function (item) {
                if (item.ISAvailable == true) {
                    item.set("ISAvailable", false);
                }
                item.set("ISAvailable", ev.target.checked);
            });
            if (CheckedValues == "") {
                CheckedValues = [];
            }

            if (ev.currentTarget.checked == true) {
                for (var i = 0; i < items.length; i++) {
                    CheckedValues.push(items[i].id);
                    if (items[i].ListItem == "PRODUCTS WITH HIERARCHY")
                        $scope.PRODUCTS_WITH_HIERARCHY = 1;
                    if (items[i].ListItem == "ATTRIBUTE LIST")
                        $scope.ATTRIBUTE_LIST = 1;
                    if (items[i].ListItem == "PICKLIST")
                        $scope.PICKLIST = 1;
                    if (items[i].ListItem == "USER AND ROLE")
                        $scope.USER_AND_ROLE = 1;
                    if (items[i].ListItem == "INDESIGN PROJECTS")
                        $scope.INDESIGN_PROJECTS = 1;
                    if (items[i].ListItem == "TABLE DESIGNER")
                        $scope.TABLE_DESIGNER = 1;
                }
            }
            if (ev.currentTarget.checked == false) {
                for (var i = 0; i < items.length; i++) {
                    CheckedValues.pop(items[i].id);
                    if (items[i].ListItem == "PRODUCTS WITH HIERARCHY")
                        $scope.PRODUCTS_WITH_HIERARCHY = 0;
                    if (items[i].ListItem == "ATTRIBUTE LIST")
                        $scope.ATTRIBUTE_LIST = 0;
                    if (items[i].ListItem == "PICKLIST")
                        $scope.PICKLIST = 0;
                    if (items[i].ListItem == "USER AND ROLE")
                        $scope.USER_AND_ROLE = 0;
                    if (items[i].ListItem == "INDESIGN PROJECTS")
                        $scope.INDESIGN_PROJECTS = 0;
                    if (items[i].ListItem == "TABLE DESIGNER")
                        $scope.TABLE_DESIGNER = 0;
                }
            }
        };

        $scope.PRODUCTS_WITH_HIERARCHY = 0;
        $scope.ATTRIBUTE_LIST = 0;
        $scope.PICKLIST = 0;
        $scope.USER_AND_ROLE = 0;
        $scope.INDESIGN_PROJECTS = 0;
        $scope.TABLE_DESIGNER = 0;

        $scope.SelectionItems = function (e, id) {
            var grid = $(e.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            if (e.currentTarget.checked == true) {
                var a = id.dataItem.id;
                if (id.dataItem.ListItem == "PRODUCTS WITH HIERARCHY")
                    $scope.PRODUCTS_WITH_HIERARCHY = 1;
                if (id.dataItem.ListItem == "ATTRIBUTE LIST")
                    $scope.ATTRIBUTE_LIST = 1;
                if (id.dataItem.ListItem == "PICKLIST")
                    $scope.PICKLIST = 1;
                if (id.dataItem.ListItem == "USER AND ROLE")
                    $scope.USER_AND_ROLE = 1;
                if (id.dataItem.ListItem == "INDESIGN PROJECTS")
                    $scope.INDESIGN_PROJECTS = 1;
                if (id.dataItem.ListItem == "TABLE DESIGNER")
                    $scope.TABLE_DESIGNER = 1;
                CheckedValues.push(a);
            }

            if (e.currentTarget.checked == false) {
                for (var i = 0; i < CheckedValues.length ; i++) {
                    var a = id.dataItem.id;
                    var b = CheckedValues[i];

                    if (id.dataItem.id == CheckedValues[i]) {
                        //DeleteValues.pop(DeleteValues[i]);
                        CheckedValues.splice(i, 1);
                        if (id.dataItem.ListItem == "PRODUCTS WITH HIERARCHY")
                            $scope.PRODUCTS_WITH_HIERARCHY = 0;
                        if (id.dataItem.ListItem == "ATTRIBUTE LIST")
                            $scope.ATTRIBUTE_LIST = 0;
                        if (id.dataItem.ListItem == "PICKLIST")
                            $scope.PICKLIST = 0;
                        if (id.dataItem.ListItem == "USER AND ROLE")
                            $scope.USER_AND_ROLE = 0;
                        if (id.dataItem.ListItem == "INDESIGN PROJECTS")
                            $scope.INDESIGN_PROJECTS = 0;
                        if (id.dataItem.ListItem == "TABLE DESIGNER")
                            $scope.TABLE_DESIGNER = 0;
                    }


                }
            }

            if (CheckedValues.length == items.length) {
                $('.mc-checkbox').prop('checked', true);
            }
            else {
                $('.mc-checkbox').prop('checked', false);
            }

        }

        jQuery("#backup_selected").attr('checked', 'true');
        $("#backup_selected").prop("checked", true);
        $("#backup_selected").attr('checked', 'checked');
        $("#backuporRestore").hide();
        $scope.Maintenance = function () {
            $("#backuporRestore").show();
            $(".backuporRestoreCls").show();
            if ($("#backup_selected").prop("checked") == true) {
                $("#backupSelected").show();
                $("#restoreSelected").hide();
                $scope.NewBackUp();
            } else if ($("#restore_selected").prop("checked") == true) {
                $("#restoreSelected").show();
                $scope.Restore_Click();
                $("#backupSelected").hide();
            } else {

            }
        }

        $scope.optionSelectionChange = function () {
            if ($("#backup_selected").prop("checked") == true) {
                $("#backupSelected").show();
                $("#restoreSelected").hide();
                $scope.NewBackUp();
                onChange();
                $scope.OkBtnCancle();
            } else if ($("#restore_selected").prop("checked") == true) {
                var catalogName = $("#Selected_Catalog_Id :selected").text();
                $scope.RestoreAs = catalogName;
                $("#restoreSelected").show();
                $scope.Restore_Click();
                $("#backupSelected").hide();
                onChange_Restore();
            } else {

            }
        }

        $scope.NewBackUp = function () {
            if ($scope.selectedBackupFile == "Database" || $scope.selectedBackupFile == undefined) {
                $('.mc-checkbox').prop('checked', true);
                $('.chkbxforallattributes').prop('checked', true);
                $(".mc-checkbox").prop("disabled", true);
                $(".chkbxforallattributes").prop("disabled", true);
            }
            else {
                $(".mc-checkbox").prop("disabled", false);
                $(".chkbxforallattributes").prop("disabled", false);
            }
            dataFactory.GetCustomerCatalog($scope.SelectedCatalogId, $scope.getCustomerID).success(function (response) {
                if (response != undefined && response != "") {
                    var array = response.split(',');
                    $scope.CatalogName = array[0];
                    $scope.UserFolder = array[1];
                    $scope.Export.COMMENTS = array[1];
                }
            });
            $scope.templatesDataSource.read();
        }

        var templateDetails = [];
        var checkedBackupFile = [];

        $scope.BackUpFile = function (backUpPath) {
            var export_As = $("#dropdownlist").val();
            var from_date = $("#datetimepicker_from").val();
            var end_date = $("#endDate").val();
            var time = $("#time_selected").val();
            if ($scope.selectedBackupFile == "Database") {
                $scope.PRODUCTS_WITH_HIERARCHY = 1;
                $scope.ATTRIBUTE_LIST = 1;
                $scope.PICKLIST = 1;
                $scope.USER_AND_ROLE = 1;
                $scope.INDESIGN_PROJECTS = 1;
                $scope.TABLE_DESIGNER = 1;
            }
            if ($scope.Header == undefined) {
                $scope.Header = "";
            }
            if ($scope.SelectedCatalogId == "" || $scope.SelectedCatalogId == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Catalog name should not be blank.',
                    type: "error"
                });
                return false;
            }
            //if ($scope.pathselection == "Yes") {
            //    if ($scope.ftp_url != "" && $scope.ftp_url != undefined && $scope.ftp_userName != "" && $scope.ftp_userName != undefined && $scope.ftp_Password != "" && $scope.ftp_Password != undefined) {
            //    }
            //    else {
            //        $.msgBox({
            //            title: $localStorage.ProdcutTitle,
            //            content: 'Please enter Data.',
            //            type: "error"
            //        });
            //        return false;
            //    }
            //}

            if ($scope.selectedBackupFile == "Database") {
                $scope.SeletedBackupItems = "All";
                templateDetails = {
                    Catalog_Id: $scope.SelectedCatalogId,
                    Local_or_FTP: $scope.pathselection,
                    Local_Path: $scope.Export.COMMENTS,
                    Template_Name: $scope.Header,
                    Export_As: export_As,
                    PRODUCTS_WITH_HIERARCHY: $scope.PRODUCTS_WITH_HIERARCHY,
                    ATTRIBUTE_LIST: $scope.ATTRIBUTE_LIST,
                    PICKLIST: $scope.PICKLIST,
                    USER_AND_ROLE: $scope.USER_AND_ROLE,
                    INDESIGN_PROJECTS: $scope.INDESIGN_PROJECTS,
                    TABLE_DESIGNER: $scope.TABLE_DESIGNER,
                    Interval: $scope.IntervalType,
                    FROM_DATE: from_date,
                    TO_DATE: end_date,
                    TIME: time,
                    FTP_URL: $scope.ftp_url,
                    FTP_USER_NAME: $scope.ftp_userName,
                    FTP_PASSWORD: $scope.ftp_Password
                };
                dataFactory.CreateDataBase($scope.SelectedCatalogId, $scope.CatalogName, $scope.Export.COMMENTS, $scope.SeletedBackupItems, templateDetails).success(function (response) {
                    var result = response.split('~');
                    $scope.SelectedTemplateId = result[1];
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: result[0] + ".",
                        type: "info"
                    });
                    if ($scope.Header != undefined && $scope.Header != "" && $scope.Header != '') {
                        $('#selectedTempName').data("kendoDropDownList").span[0].innerHTML = $scope.Header;
                    }
                    else {
                        $("#selectedTempName").data("kendoDropDownList").dataSource.read();
                        $("#template_name").attr('disabled', 'disabled');
                    }
                    if (result[0].toUpperCase() != "TEMPLATE NAME ALREADY EXISTS") {
                        $("#template_name").attr('disabled', 'disabled');
                    }
                    $scope.templatesDataSource.read();
                });
            }
            else if ($scope.selectedBackupFile == "Excel File") {
                if ($scope.PRODUCTS_WITH_HIERARCHY != 0 || $scope.ATTRIBUTE_LIST != 0 || $scope.PICKLIST != 0 || $scope.USER_AND_ROLE != 0 || $scope.INDESIGN_PROJECTS != 0 || $scope.TABLE_DESIGNER != 0) {
                    $scope.SeletedBackupItems = $scope.selectedBackupFile;
                    templateDetails = {
                        Catalog_Id: $scope.SelectedCatalogId,
                        Local_or_FTP: $scope.pathselection,
                        Local_Path: $scope.Export.COMMENTS,
                        Template_Name: $scope.Header,
                        Export_As: export_As,
                        PRODUCTS_WITH_HIERARCHY: $scope.PRODUCTS_WITH_HIERARCHY,
                        ATTRIBUTE_LIST: $scope.ATTRIBUTE_LIST,
                        PICKLIST: $scope.PICKLIST,
                        USER_AND_ROLE: $scope.USER_AND_ROLE,
                        INDESIGN_PROJECTS: $scope.INDESIGN_PROJECTS,
                        TABLE_DESIGNER: $scope.TABLE_DESIGNER,
                        Interval: $scope.IntervalType,
                        FROM_DATE: from_date,
                        TO_DATE: end_date,
                        TIME: time,
                        FTP_URL: $scope.ftp_url,
                        FTP_USER_NAME: $scope.ftp_userName,
                        FTP_PASSWORD: $scope.ftp_Password
                    };
                    dataFactory.CreateDataBaseFile($scope.SelectedCatalogId, $scope.CatalogName, $scope.Export.COMMENTS, $scope.SeletedBackupItems, templateDetails).success(function (response) {
                        var result = response.split('~');
                        $scope.SelectedTemplateId = result[1];
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Backup completed.",
                            type: "info"
                        });
                        if ($scope.Header != undefined && $scope.Header != "" && $scope.Header != '')
                            $("#selectedTempName").data("kendoDropDownList").span[0].innerHTML = $scope.Header;
                        else {
                            $("#selectedTempName").data("kendoDropDownList").dataSource.read();
                            $("#template_name").attr('disabled', 'disabled');
                        }

                        if (result[0].toUpperCase() != "TEMPLATE NAME ALREADY EXISTS") {
                            $("#template_name").attr('disabled', 'disabled');
                        }
                        $scope.templatesDataSource.read();
                        //window.open("DownloadBackup.ashx");
                        window.open("DownloadBackup.ashx?CustomerFolder=" + $scope.Export.COMMENTS);
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select atleast one feature.',
                        type: "error"
                    });
                    return false;
                }
            }
        }

        $scope.catalogDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogDetails($scope.SelectedItem, $scope.getCustomerID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        var data = [
               { text: "Database", value: "1" },
               { text: "Excel File", value: "2" }

        ];

        $("#dropdownlist").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            select: onSelect,
            change: onChange,
            dataSource: data,
            select: onSelect,
            dataBound: onDataBound

        });
        function onSelect(e) {

            if (e.item) {
                var dataItem = e.item.context.innerText;
                $scope.selectedBackupFile = dataItem;
            }

        };

        function onSelect_Restore(e) {
            if (e.item) {
                var dataItem = e.item.context.innerText;
                $scope.selectedBackupFile = dataItem;
                if ($scope.selectedBackupFile == "Database") {
                    $("#importfilerestoreXls").hide();
                    $("#importfilerestoreBak").show();
                } else {
                    $("#importfilerestoreXls").show();
                    $("#importfilerestoreBak").hide();
                }
            }
        };

        function onChange() {

            if ($("#dropdownlist").val() == "1") {
                $('.mc-checkbox').prop('checked', true);
                $('.chkbxforallattributes').prop('checked', true);
                $(".mc-checkbox").prop("disabled", true);
                $(".chkbxforallattributes").prop("disabled", true);
                $scope.PRODUCTS_WITH_HIERARCHY = 1;
                $scope.ATTRIBUTE_LIST = 1;
                $scope.PICKLIST = 1;
                $scope.USER_AND_ROLE = 1;
                $scope.INDESIGN_PROJECTS = 1;
                $scope.TABLE_DESIGNER = 1;
            }
            else {
                $scope.PRODUCTS_WITH_HIERARCHY = 0;
                $scope.ATTRIBUTE_LIST = 0;
                $scope.PICKLIST = 0;
                $scope.USER_AND_ROLE = 0;
                $scope.INDESIGN_PROJECTS = 0;
                $scope.TABLE_DESIGNER = 0;
                $(".mc-checkbox").prop("disabled", false);
                $(".chkbxforallattributes").prop("disabled", false);
                $('.mc-checkbox').prop('checked', false);
                $('.chkbxforallattributes').prop('checked', false);
            }

        };
        function onDataBound(e) {
            if ("kendoConsole" in window) {
                kendoConsole.log("event :: dataBound");
            }
        };


        //   $scope.IntervalType = "Once";

        $scope.templatesDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetAllTemplateDetails($scope.SelectedCatalogId).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.changeTemplate = function (e) {
            if (e.sender.text() != "--- New ---") {
                $scope.SelectedTemplateId = e.sender.value();
            } else {
                $("#template_name").removeAttr('disabled');
                $scope.SelectedTemplateId = 0;
            }

            dataFactory.GetTemplateDetails($scope.SelectedTemplateId).success(function (response) {
                if (response[0] != null) {
                    $scope.Header = response[0].TEMPLATE_NAME;
                    $("#template_name").attr('disabled', 'disabled');
                    $('#dropdownlist').data("kendoDropDownList").span[0].innerHTML = response[0].EXPORT_AS;
                    //  $('#Interval_Type').data("kendoDropDownList").span[0].innerHTML = response[0].INTERVAL;
                    $scope.Export.COMMENTS = response[0].LOCAL_PATH;
                    // var start_Date = response[0].FROM_DATE;
                    //  var end_Date = response[0].TO_DATE;
                    //  var start_Date_split = start_Date.split('T');
                    //  var start_Date_val = start_Date_split.toString().split('-');
                    //  var sMonth = start_Date_val[2].toString().split(',')
                    //  $scope.STARTDATE = start_Date_val[1] + "/" + sMonth[0] + "/" + start_Date_val[0];

                    // var end_Date_split = end_Date.split('T');
                    // var end_Date_val = end_Date_split.toString().split('-');
                    //  var eMonth = end_Date_val[2].toString().split(',')
                    //  $scope.ENDDATE = end_Date_val[1] + "/" + eMonth[0] + "/" + end_Date_val[0];
                    //  $scope.TIME = response[0].TIME;
                    //if (response[0].IS_LOCAL == true) {
                    //    $('#local_selected').prop('checked', true);
                    //    $('#local_selected').attr('checked', true);
                    //    $("#isFtp").hide();
                    //}
                    //else if (response[0].IS_LOCAL == false) {
                    //    $("#isFtp").show();
                    //    $scope.ftp_url = response[0].FTP_URL;
                    //    $scope.ftp_userName = response[0].FTP_USER_NAME;
                    //    $scope.ftp_Password = response[0].FTP_PASSWORD;
                    //    $('#ftp_selected').prop('checked', true);
                    //    $('#ftp_selected').attr('checked', true);
                    //}

                    if (response[0].EXPORT_AS.toUpperCase() == "DATABASE") {

                        var dataSource = $("#listofItemsgrid").data("kendoGrid").dataSource;
                        var filters = dataSource.filter();
                        var allData = dataSource.data();
                        var query = new kendo.data.Query(allData);
                        var filteredData = query.filter(filters).data;

                        allData.forEach(function (item) {
                            item.set("ISAvailable", false);
                        });
                        $('.mc-checkbox').prop('checked', true);
                        $('.chkbxforallattributes').prop('checked', true);
                        $(".mc-checkbox").prop("disabled", true);
                        $(".chkbxforallattributes").prop("disabled", true);

                    } else if (response[0].EXPORT_AS.toUpperCase() == "EXCEL FILE") {
                        $(".mc-checkbox").prop("disabled", false);
                        $(".chkbxforallattributes").prop("disabled", false);
                        $('.mc-checkbox').prop('checked', false);
                        $('.chkbxforallattributes').prop('checked', false);

                        var dataSource = $("#listofItemsgrid").data("kendoGrid").dataSource;
                        var filters = dataSource.filter();
                        var allData = dataSource.data();
                        var query = new kendo.data.Query(allData);
                        var filteredData = query.filter(filters).data;
                        if (response[0].PRODUCTS_WITH_HIERARCHY == true)
                            allData[0].set("ISAvailable", true);
                        // item.set("ISAvailable", true);
                        if (response[0].ATTRIBUTE_LIST == true)
                            allData[1].set("ISAvailable", true);
                        if (response[0].PICKLIST == true)
                            allData[2].set("ISAvailable", true);
                        if (response[0].USER_AND_ROLE == true)
                            allData[3].set("ISAvailable", true);
                        if (response[0].INDESIGN_PROJECTS == true)
                            allData[4].set("ISAvailable", true);
                        if (response[0].TABLE_DESIGNER == true)
                            allData[5].set("ISAvailable", true);
                    }

                }
                else {
                    $scope.Header = "";
                    $('#dropdownlist').data("kendoDropDownList").span[0].innerHTML = "Database";
                    //$('#Interval_Type').data("kendoDropDownList").span[0].innerHTML = "Once";
                    //$scope.STARTDATE = "";
                    //$scope.ENDDATE = "";
                    //$scope.TIME = "";
                    //$('#local_selected').prop('checked', true);
                    //$('#local_selected').attr('checked', true);
                    //$("#isFtp").hide();
                    var dataSource = $("#listofItemsgrid").data("kendoGrid").dataSource;
                    var filters = dataSource.filter();
                    var allData = dataSource.data();
                    var query = new kendo.data.Query(allData);
                    var filteredData = query.filter(filters).data;

                    allData.forEach(function (item) {
                        item.set("ISAvailable", false);
                    });
                    $scope.PRODUCTS_WITH_HIERARCHY = 0;
                    $scope.ATTRIBUTE_LIST = 0;
                    $scope.PICKLIST = 0;
                    $scope.USER_AND_ROLE = 0;
                    $scope.INDESIGN_PROJECTS = 0;
                    $scope.TABLE_DESIGNER = 0;
                    $('.mc-ResetUserfromtabcheckbox').prop('checked', true);
                    $('.chkbxforallattributes').prop('checked', true);
                    $(".mc-checkbox").prop("disabled", true);
                    $(".chkbxforallattributes").prop("disabled", true);
                }
            }).error(function (error) {
                options.error(error);
            });

        };

        $scope.resetBackUpFile = function () {
            $scope.templatesDataSource.read();
            $("#template_name").removeAttr('disabled');
            $('#selectedTempName').data("kendoDropDownList").span[0].innerHTML = "--- New ---";
            $scope.Header = "";
            //$('#dropdownlist').data("kendoDropDownList").span[0].innerHTML = "Database";
            $("#dropdownlist").data("kendoDropDownList").value("1");
            //$('#Interval_Type').data("kendoDropDownList").span[0].innerHTML = "Once";
            //$scope.STARTDATE = "";
            //$scope.ENDDATE = "";
            //$scope.TIME = "";
            //$('#local_selected').prop('checked', true);
            //$('#local_selected').attr('checked', true);
            //$("#isFtp").hide();
            var dataSource = $("#listofItemsgrid").data("kendoGrid").dataSource;
            var filters = dataSource.filter();
            var allData = dataSource.data();
            var query = new kendo.data.Query(allData);
            var filteredData = query.filter(filters).data;
            allData.forEach(function (item) {
                item.set("ISAvailable", false);
            });
            $scope.PRODUCTS_WITH_HIERARCHY = 0;
            $scope.ATTRIBUTE_LIST = 0;
            $scope.PICKLIST = 0;
            $scope.USER_AND_ROLE = 0;
            $scope.INDESIGN_PROJECTS = 0;
            $scope.TABLE_DESIGNER = 0;
            $('.mc-checkbox').prop('checked', true);
            $('.chkbxforallattributes').prop('checked', true);
            $(".mc-checkbox").prop("disabled", true);
            $(".chkbxforallattributes").prop("disabled", true);

        };


        // -------------------------------  For Restore -------------------

        $("#importfilerestoreXls").hide();

        function onChange_Restore() {

            if ($("#dropdownlist_Restore").val() == "1") {
                $('.mc-checkbox').prop('checked', true);
                $('.chkbxforallattributes').prop('checked', true);
                $(".mc-checkbox").prop("disabled", true);
                $(".chkbxforallattributes").prop("disabled", true);
                $scope.FileInvalidMessage = "";
                $scope.file_error = $scope.FileInvalidMessage;
            }
            else {
                $(".mc-checkbox").prop("disabled", false);
                $(".chkbxforallattributes").prop("disabled", false);
                $('.mc-checkbox').prop('checked', false);
                $('.chkbxforallattributes').prop('checked', false);
                $scope.FileInvalidMessage = "";
                $scope.file_error = $scope.FileInvalidMessage;
            }

        };

        $scope.Restore_Click = function () {
            $('.mc-checkbox').prop('checked', true);
            $('.chkbxforallattributes').prop('checked', true);
            $(".mc-checkbox").prop("disabled", true);
            $(".chkbxforallattributes").prop("disabled", true);
        };

        $("#dropdownlist_Restore").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            change: onChange_Restore,
            dataSource: data,
            select: onSelect_Restore,
            dataBound: onDataBound

        });

        $scope.RestoreAs = $('#Selected_Catalog_Id :selected').text();

        var result = "";
        $scope.selectFileforUploadRestore = function (file) {
            $scope.enableReferenceImport = true;
            $scope.FileInvalidMessage = "";
            $rootScope.SelectedFileForUploadRestore = file[0].name;
            $scope.file_name = file[0].name;
            $scope.SelectedFileForRestore = file[0];
            $scope.$apply(function () {
                $rootScope.SelectedFileForUploadRestore = file[0].name;
            });
        };
        $("#restoreLog").hide();
        $scope.RestoreCatalog = function () {
            $('#backup_selected').prop("disabled", true);
            var formData = new FormData();
            formData.append("file", $scope.SelectedFileForRestore);
            $scope.CheckFileValid($scope.SelectedFileForRestore);

            //==========================================Separate path changes==============================================//
            //var formData = new FormData();
            //formData.append("file", $scope.SelectedFileForRestore);
            //$scope.CheckFileValid($scope.SelectedFileForRestore);
            //==========================================Separate path changes==============================================//



            //if ($scope.RestoreAs !=undefined &&)

            var checkedItems = "";
            if ($("#dropdownlist_Restore").val() != undefined && $("#dropdownlist_Restore").val() != "" && $("#dropdownlist_Restore").val() == "2") {
                if ($scope.PRODUCTS_WITH_HIERARCHY == 1)
                    checkedItems = checkedItems + ",CATEGORY,FAMILY,PRODUCT";
                if ($scope.ATTRIBUTE_LIST == 1)
                    checkedItems = checkedItems + ",ATTRIBUTELIST";
                if ($scope.PICKLIST == 1)
                    checkedItems = checkedItems + ",PICKLIST";
                if ($scope.USER_AND_ROLE == 1)
                    checkedItems = checkedItems + ",USERDETAILS,ROLECATALOG,ROLEGROUP_NAME,ROLEATTRIBUTE";
                if ($scope.INDESIGN_PROJECTS == 1)
                    checkedItems = checkedItems + ",INDESIGNDETAILS";
                if ($scope.TABLE_DESIGNER == 1)
                    checkedItems = checkedItems + ",TABLEDESIGNER";
            }
            checkedItems = checkedItems.replace(/^,|,$/g, '');
            $scope.selectedItems = checkedItems;

            if ($scope.SelectedCatalogId == "" || $scope.SelectedCatalogId == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Restore catalog name should not be blank.',
                    type: "error"
                });
                return false;
            }

            if ($scope.filePath != undefined && $scope.filePath != null && $scope.filePath != "" && $("#dropdownlist_Restore").val() == "1") {
                $http.post("/Catalog/RestoreCatalog?catalogId=" + $scope.SelectedCatalogId + '&filePath=' + $scope.filePath,
                    {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                    .success(function (d) {
                        //$.msgBox({
                        //    title: $localStorage.ProdcutTitle,
                        //    content: d,
                        //});
                        $scope.resultColumns = d.Columns;
                        var data = d.Data.substr(1, d.Data.length).split('~');

                        for (var i = 0; i < data.length; i++) {
                            var value = data[i];
                            value = value.replace(/^,|,$/g, '');
                            data[i] = value;
                            $scope.resultColumns[i].ColumnName = value + ".";

                        }
                        $scope.resultData = data;
                        if (data[0] == "" && data[1] == "" && data[2] == "" && data[3] == "" && data[4] == "") {
                            $scope.failedDisabled = true;
                        }
                        else if (data[0].includes('Failed') || data[1].includes('Failed') || data[2].includes('Failed') || data[3].includes('Failed') || data[4].includes('Failed')) {
                            $scope.failedDisabled = true;
                        }
                        else {
                            $scope.failedDisabled = false;
                        }
                        $("#restoreLog").show();
                        $(".FeatureGrid").hide();
                        $("#FeatureGrid_Restore").hide();
                    })
                    .error(function () {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'File upload failed, please try again.',
                            type: "error"
                        });
                    });
                return defer.promise;
            }
            else if ($scope.filePath != undefined && $scope.filePath != null && $scope.filePath != "" && $("#dropdownlist_Restore").val() == "2") {
                if ($scope.selectedItems != undefined && $scope.selectedItems != "" || $scope.selectedItems != '') {
                    $http.post("/Catalog/RestoreCatalogfromFiles?catalogId=" + $scope.SelectedCatalogId + '&selectedItems=' + $scope.selectedItems + '&filePath=' + $scope.filePath,
                    {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                    .success(function (d) {
                        $scope.resultColumns = d.Columns;
                        var data = d.Data.substr(1, d.Data.length).split('~');
                        for (var i = 0; i < data.length; i++) {
                            var value = data[i];
                            value = value.replace(/^,|,$/g, '');
                            data[i] = value;
                            $scope.resultColumns[i].ColumnName = value + ".";
                        }
                        $scope.resultData = data;
                        if (data[0] == "" && data[1] == "" && data[2] == "" && data[3] == "" && data[4] == "") {
                            $scope.failedDisabled = true;
                        }
                        else if (data[0].includes('Failed') || data[1].includes('Failed') || data[2].includes('Failed') || data[3].includes('Failed') || data[4].includes('Failed')) {
                            $scope.failedDisabled = true;
                        }
                        else {
                            $scope.failedDisabled = false;
                        }
                        $("#restoreLog").show();
                        $(".FeatureGrid").hide();
                        $("#FeatureGrid_Restore").hide();
                    })
                    .error(function () {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'File upload failed, please try again.',
                            type: "error"
                        });
                    });
                    return defer.promise;
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select any one Feature.',
                        type: "error"
                    });
                }
            }
            else {
                $scope.file_error = "Kindly select a file to restore.";
                return false;
            }

        };


        $scope.CheckFileValid = function (file) {
            var isValid = false;
            if ($scope.SelectedFileForRestore != null) {
                if ($("#dropdownlist_Restore").val() == "1") {
                    if (file.name.toLowerCase().endsWith('.bak')) { // Change to .zip
                        $scope.FileInvalidMessage = "";
                        $scope.excelname = file.name;
                        isValid = true;
                    }
                    else {
                        $scope.FileInvalidMessage = "Selected file is Invalid. (only file type bak is allowed).";
                        isValid = false;
                    }
                } else if ($("#dropdownlist_Restore").val() == "2") {
                    if (file.name.toLowerCase().endsWith('.xls')) {
                        $scope.FileInvalidMessage = "";
                        $scope.excelname = file.name;
                        isValid = true;
                    }
                    else {
                        $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xls is allowed).";
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                }
            }
            else {
                $scope.FileInvalidMessage = "Please select a file.";
                isValid = false;
            }
            $scope.IsFileValid = isValid;
        };
        $scope.excelPath = "";

        $scope.ResetRestore = function () {
            $('#backup_selected').prop("disabled", false);
            $scope.filePath = null;
            $scope.file_name = null;
            $scope.FileInvalidMessage = "";
            $scope.SelectedFileForRestore = null;
            var $el;
            if ($("#dropdownlist_Restore").val() != "1") {
                $el = $('#importfilerestoreXls');
            } else {
                $el = $('#importfilerestoreBak');
            }
            //if ($el.val() != "") {
            //    $el.wrap('<form>').closest('form').get(0).reset();
            //    $el.unwrap();
            //}
            $("#dropdownlist_Restore").data("kendoDropDownList").enable(true);
            $scope.restore_Type = "1";
            $("#dropdownlist_Restore").text("Database");
            $("#dropdownlist_Restore").val("1");
            $("#dropdownlist_Restore").data("kendoDropDownList").value("1");
            $('.mc-checkbox').prop('checked', true);
            $('.chkbxforallattributes').prop('checked', true);
            $(".mc-checkbox").prop("disabled", true);
            $(".chkbxforallattributes").prop("disabled", true);

            if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
                $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        };

        $scope.OkBtnRestore = function () {
            if ($("#dropdownlist_Restore").val() == "1") {
                dataFactory.Restore($scope.SelectedCatalogId, $scope.RestoreAs).success(function (response) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: response + ".",
                        type: "info",
                    });
                    $("#restoreLog").hide();
                    $(".FeatureGrid").show();
                    $("#FeatureGrid_Restore").show();
                    $scope.Restore_Click();
                    $scope.ResetRestore();
                }).error(function (error) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Action not completed.",
                        type: "error",
                    });
                });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Catalog Restored Successfully.",
                    type: "info",
                });
                $("#restoreLog").hide();
                $(".FeatureGrid").show();
                $("#FeatureGrid_Restore").show();
                $scope.Restore_Click();
                $scope.ResetRestore();
            }
        }


        //------------------------------------------------Backup and Restore end
        //----------------------------------------Backup Restore new changes by Mohan-------------------------------------------//
        $scope.OpenPopupWindow = function (e) {
            $rootScope.paramValue = e;
            $localStorage.backupRestore = "backupRestore";
            $scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
            $scope.winManageDrive.title("Asset Management");
            $scope.winManageDrive.center().open();
            $scope.driveMahementIsVisible = true;
            $rootScope.paramValue = 'backupRestore';
        }
        $scope.clearImagePath = function (e) {
            //
            document.getElementById(e).value = "";
            $scope.selectedRow[e] = "";
        };




        $rootScope.saveBackupRestoreFileSelection = function (fileName, path) {
            var backupType = $('#dropdownlist_Restore').val();
            var FileFormat = [];
            var extn = fileName.split(".");
            // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
            if (backupType == '1')
                FileFormat = ["bak"];
            else if (backupType == '2')
                FileFormat = ["xls", "xlsx"];
            if (FileFormat.includes(extn[1].toLowerCase())) {
                $scope.file_error = "";
                $scope.file_name = fileName;
                $scope.filePath = path;
                if ($scope.file_name != null && $scope.file_name != undefined) {
                    $('#backup_selected').prop("disabled", false);
                }
                //document.getElementById('dropdownlist_Restore').disabled=true;
                $("#dropdownlist_Restore").data("kendoDropDownList").enable(false);
                //$scope.readOlyFlagRestore = true;
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Invalid file selection.',
                    type: "info"
                });
            }
            $scope.winManageDrive.center().close();
            $scope.driveMahementIsVisible = false;
        }



        $scope.OkBtnCancle = function () {

            $("#restoreLog").hide();
            $(".FeatureGrid").show();
            $("#FeatureGrid_Restore").show();
            $('#backup_selected').prop("disabled", false);

        };

        $scope.clearRestoreFile = function () {
            $scope.filePath = null;
            $scope.file_name = null;
            $scope.FileInvalidMessage = "";
            $scope.SelectedFileForRestore = null;
            if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
                $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
            $("#dropdownlist_Restore").data("kendoDropDownList").enable(true);
        }

        $scope.SaveAnnouncement = function () {
            $scope.AnnouncementDate = $('#datetimepicker').val();

            if ($scope.AnnouncementDate == '' || $scope.AnnouncementDate == undefined || $scope.AnnouncementDate == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a valid date.',
                    type: "error",
                });
            }

            else if ($scope.AnnouncementSubject == '' || $scope.AnnouncementSubject == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a Announcement Subject .',
                    type: "error",
                });

            }
            else if ($scope.AnnouncementDescription == '' || $scope.AnnouncementDescription == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a Announcement Description .',
                    type: "error",
                });

            }
            else {
                dataFactory.SaveAnnouncement($scope.AnnouncementDate, $scope.AnnouncementSubject, $scope.AnnouncementDescription).success(function (response) {
                    if (response == "Saved.") {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Saved successfully.',
                            type: "info",
                        });
                        // $scope.CustomMenuDatasource.read();
                    }
                    else if (response == "Duplicate") {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Header already available.',
                            type: "info",
                        });
                        // $scope.CustomMenuDatasource.read();
                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please enter a valid Header and URL.',
                            type: "info",
                        });
                    }
                    $scope.AnnouncementDatasource.read();
                    $scope.AnnouncementDate = '';
                    $scope.AnnouncementSubject = '';
                    $scope.AnnouncementDescription = '';

                }).error(function (error) {
                    options.error(error);
                });
            }
        };

        $scope.CancelAnnouncement = function () {
            $scope.AnnouncementDate = '';
            $scope.AnnouncementSubject = '';
            $scope.AnnouncementDescription = '';
        }


        $scope.AnnouncementDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: false,
            serverPaging: false, pageSize: 5,
            autoBind: false,
            serverSorting: true, editable: true, pageable: true, batch: true, pageable: true,
            transport: {

                read: function (options) {
                    dataFactory.getAnnouncement().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });

                },
                update: function (options) {

                    var rows = $scope.AnnouncementDatasource._data[$scope.getRowIndex];

                    var date = new Date(rows.Announcement_Date),
                        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                        day = ("0" + date.getDate()).slice(-2);
                    var getAnnoucementdate = [date.getFullYear(), mnth, day].join("-");
                    dataFactory.updateAnnouncementValues(rows.id, rows.Announcement_Description, rows.Announcement_Header, getAnnoucementdate).success(function (response) {
                        options.success(response);
                        if (response == true) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Update successful.',
                                type: "info"
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Please enter vaild Header and URL.',
                                type: "error"
                            });
                        }
                        $scope.AnnouncementDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                cancel: function (options) {
                    $scope.AnnouncementDatasource.read();
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            },
            schema: {
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false }
                    }
                }
            },
            dataBound: function (e) {

            }
        });


        $scope.GetAnnouncement = {
            dataSource: $scope.AnnouncementDatasource,
            sortable: true, scrollable: true,
            editable: true,
            pageable: true,
            columns: [

                 {
                     field: "Announcement_Date",
                     title: "Date",
                     width: "200px",

                     format: "{0:yyyy-MM-dd }",
                     template: "#= kendo.toString(kendo.parseDate(Announcement_Date, 'yyyy-MM-dd'), 'yyyy-MM-dd') #",


                     // template: "#= kendo.toString(kendo.parseDate(Announcement_Date, 'MM/dd/yyyy),hh:mm') #",

                     editor: dateTimeEditor
                 },

                  { field: "Announcement_Header", title: "Subject" },
                    { field: "Announcement_Description", title: "Description" },



              { command: ["edit", { name: "destroy", text: "", width: "10px", template: "<a class='k-button k-button-icontext ' title = \'Delete\' ng-click='\RemoveAnnouncementData($event,this)\'><span class='k-icon k-delete' title = \'Delete\'></span>Delete</a>" }], title: "Actions", width: "165px" }],

            editable: "inline",
            dataBound: function (e) {
                var grid = $("#Announcementgrid").data("kendoGrid");
                $(grid.tbody).on("click", "td", function (e) {
                    var row = $(this).closest("tr");
                    var rowIdx = $("tr", grid.tbody).index(row);
                    var colIdx = $("td", row).index(this);
                    $scope.getRowIndex = rowIdx;


                });
            },
        };
        function dateTimeEditor(container, options) {
            $('<input class="datepickerEdit" data-text-field="' + options.field + '" data-value-field="' + options.field
                    + '" data-bind="value:' + options.field + '" />')
                    .appendTo(container)
                    .kendoDatePicker({
                        min: new Date(),
                        format: "yyyy-MM-dd",
                        value: (options.model.date)
                    });
            $(".datepickerEdit").on("keydown", function (e) {
                e.preventDefault();
            });
        }
        $scope.RemoveAnnouncementData = function (event, e) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to delete this announcement?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    if (result === "Yes") {
                        dataFactory.deleteAnnouncement(e.dataItem.Id).success(function (response) {
                            $scope.AnnouncementDatasource._data.remove(e.dataItem);
                            if (response == true) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Deleted successfully.',
                                    type: "info",
                                });
                                $scope.AnnouncementDatasource.read();
                            }
                            else
                                $scope.AnnouncementDatasource.read();
                        })
                    }
                }
            })
        }

        $scope.SaveApplicationSettings = function () {
           
            dataFactory.SaveApplicationSettings($scope.CustomerId, $scope.WebConfigFiles.announcementCount, $scope.WebConfigFiles.restoreSourcePath, $scope.WebConfigFiles.restoreDestPath, $scope.WebConfigFiles.serverPath, $scope.WebConfigFiles.dataSource, $scope.WebConfigFiles.userID, $scope.WebConfigFiles.Password, $scope.WebConfigFiles.backUpAttachement, $scope.WebConfigFiles.backUpMdfPath, $scope.WebConfigFiles.backUpDataBasePath, $scope.WebConfigFiles.ImportSheetProductCount).success(function (response) {

                if (response == "Saved.") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Saved successfully.',
                        type: "info",
                    });
                    // $scope.CustomMenuDatasource.read();
                }
                else if (response == "Update.") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Updated Successful.',
                        type: "info",
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Not Saved.',
                        type: "info",
                    });
                }

            });
        }
        $scope.Settings = function () {
           
            $scope.WebConfigFiles.announcementCount = '5';
            $scope.WebConfigFiles.Password = "";

            //dataFactory.retrivedatafromtable(importSheetProductCount).success(function (response) {
            //    $scope.WebConfigFiles.ImportSheetProductCount = response;
            //});

        }

        //user setting click function//
        $scope.UserSettingstest = function () {
           
            $("#userGroupSettings").hide();
            $("#user").show();
            $("#rolesettings").hide();
            $("#Usersettingtest").hide();
            $("#Webuser").hide();
            $("#WebuserListViewGrid").hide();
            $("#invite").hide();


        }
        //user first click//
        $scope.Usersettings = function () {
           
            $("#usertabclick").addClass("k-state-active");
            $("#usergroupclick").removeClass("k-state-active");
            $("#usergroupclick").removeClass("k-state-hover");
            $("#userpermissionclick").removeClass("k-state-active");
            $("#userpermissionclick").removeClass("k-state-hover");
            $("#userexportclick").removeClass("k-state-active");
            $("#userexportclick").removeClass("k-state-hover");
            $("#userGroupSettings").hide();
            $("#Usertab").show();
            $("#user").show();
            $("#rolesettings").hide();
            $("#Usersettingtest").hide();
            $("#invite").hide();
            $("#Webuser").hide();
            $("#WebuserListViewGrid").hide();
        }
        // user import and export//
        $scope.ResetUserfromtab = function () {
           
            $("#usertabclick").addClass("k-state-active");
            $("#usergroupclick").removeClass("k-state-active");
            $("#usergroupclick").removeClass("k-state-hover");
            $("#userpermissionclick").removeClass("k-state-active");
            $("#userpermissionclick").removeClass("k-state-hover");
            $("#userexportclick").removeClass("k-state-active");
            $("#userexportclick").removeClass("k-state-hover");
            $("#AddUserToGroup").hide();
            $("#newRoleEntryView").hide();
            $("#saverole").hide();
            $("#userGroupSettings").hide();
            $("#invite").hide();
            $("#rolesettings").hide();
            $("#Usersettingtest").hide();
            $("#user").show();
            $("#userListViewGrid").show();
            $("#Webuser").hide();
            $("#WebuserListViewGrid").hide();
            $("#showUserImportdiv").hide();
            $scope.selectOptions.read();
            $scope.userListDataSource.read();

        }
        $scope.ResetWebUserfromtab = function () {
           
            $("#selectedattribute").hide();
            $("#invite").hide();
            $("#rolesettings").hide();
            $("#Usersettingtest").hide();
            $("#user").hide();
            $("#userListViewGrid").hide();
            $("#Webuser").show();
            $("#WebuserListViewGrid").show();
            $("#showUserImportdiv").hide();
            $scope.userListWebDataSource.read();
        }


        //kendo for web
        $scope.userListWebDataSource = new kendo.data.DataSource({
            pageSize: 10,
            batch: false,
            autoBind: true,
            serverPaging: false,
            serverSorting: false,
            serverFiltering: false,
            transport: {
                read: function (options) {

                    dataFactory.getUserWebList().success(function (response) {
                        $scope.datas = response;
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            },
            schema: {
                data: function (data) { return data; },
                total: function (data) {
                    return data.length;
                }
            }
        });

        ///option

        $scope.userWeboption = {
            dataSource: $scope.userListWebDataSource,
            autoBind: true,
            sortable: true,
            filterable: true,
            autoBind: true,
            selectable: false,
            editable: "popup",

            pageable: { buttonCount: 5 },


            columns: [

       { field: "FirstName", title: "First Name", width: "100px" },
                { field: "LastName", title: "Last Name", width: "100px" },
                { field: "Email", title: "Email Id", width: "100px" },
                { field: "CompanyName", title: "Company Name", width: "100px" },
                 {
                     field: "ActiveUser", title: "Status", width: 130, template: 'Active  <input type="checkbox"  title = \'Active\' id="allow" disabled class="chkbx5" #= ActiveUser ?checked="checked" : "" # ng-click="roleplanGroupfunctionset($event, this)"></input>'
                     // field: "ActiveUser", title: "Active User", template: "<label> Active <input disabled type='radio' value='#: ActiveUser #' #= ActiveUser== '1' ? 'checked' : ''# >" + " " +
                     // "<label> Inactive <input disabled type='radio' value='#: ActiveUser #' #= ActiveUser== '0' ? 'checked' : ''# >",
                     // editor: "<label>Active<input name='ActiveUser' type='radio' data-bind='checked:STATUS' #= ActiveUser== '1' ? 'checked' : ''#  value='1'>" +
                     // "<label>Inactive<input name='ActiveUser' type='radio' data-bind='checked:ActiveUser' #= ActiveUser== '0' ? 'checked' : ''# value='0'>"
                     //, editable: false,width: "100px"
                 },

                 {

                     command: [{ name: "EDIT", text: "", width: "10px", template: "<a class='k-button k-button-icontext ' title = \'EDIT\' ng-click='\RemoveTextList($event,this)\'><span class='k-icon k-edit' title = \'EDIT\'></span>Edit</a>" }], width: "100px", title: "Edit"
                 }

            ], dataBound: function (e) {
            },

        };



        //role setting //
        $scope.ResetRoles = function () {

            $("#usertabclick").removeClass("k-state-active");
            $("#usertabclick").removeClass("k-state-hover");
            $("#usergroupclick").removeClass("k-state-active");
            $("#usergroupclick").removeClass("k-state-hover");
            $("#userpermissionclick").addClass("k-state-active");
            $("#userexportclick").removeClass("k-state-active");
            $("#userexportclick").removeClass("k-state-hover");
            $("#AddUserToGroup").hide();
            $("#userGroupSettings").hide();
            $("#Usersettingtest").hide();
            $("#user").hide();
            $("#invite").hide();
            $("#rolesettings").show();
            $("#roleListView").show();
            $("#grid").show();
            // $("#showRoleImportdiv").hide();
            $scope.UserRolesListDataSource.read();
            $scope.UserRoleDatasource.read();
            $scope.UserRoleAttributeDatasourceallow.read();
            $scope.userRoledropdowndatasource.read();
            $scope.userRoleforGroupdropdowndatasource.read();
            $("#Webuser").hide();
            $("#WebuserListViewGrid").hide();
        }


        $scope.cancelNewUserweb = function () {
         
            $("#WebnewUserEntryView").hide();
            $("#selectedattribute").hide();
            $("#WebUserListView").show();
            $scope.userListWebDataSource.read();

        }


        $scope.getdetails = function (a) {
          
        }

        $scope.RoleListDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: function (options) {
                    //if ($scope.Userweb.Email == "undefined" || $scope.Userweb.Email == "" )
                    //{
                    //    $scope.Userweb.Email = "Null";
                    //}

                    dataFactory.dropdownRoleList($scope.value, $scope.Userweb.Email).success(function (response) {
                        if ($scope.value == 2 || $scope.value == 3) {
                            $scope.orgFlag = true;
                        }
                        else {
                            $scope.orgFlag = false;
                        }

                        options.success(response);

                        //   $scope.Userweb.role = $rootScope.Role;


                    }).error(function (error) {
                        options.error(error);
                    });

                }
            },
            cache: false
        });
        //$scope.organisation = function () {
        //    $scope.organisationloadDataSource = new kendo.data.DataSource({
        //        type: "json",
        //        async: false,
        //        editable: false,
        //        serverSorting: true,
        //        serverFiltering: true,
        //        sortable: true,
        //        batch: true,
        //        transport: {
        //            read: function (options) {
        //                $scope.value = 6;
        //                if ($scope.value != null && $scope.value != undefined && $scope.value != '')
        //                    dataFactory.dropdownchange($scope.value).success(function (response) {

        //                        options.success(response);

        //                        //  $scope.Userweb.organisation = $rootScope.ORGID;

        //                    }).error(function (error) {
        //                        options.error(error);
        //                    });

        //            }
        //        },

        //        sort: { field: "organisationName", dir: "desc" },
        //        cache: false

        //    });
        //}

        $scope.RemoveTextList = function (event, e) {
           
            $("#WebnewUserEntryView").show();
            $("#selectedattribute").show();
            $("#WebUserListView").hide();

            $scope.datas = e.dataItem;
            clickedData = $scope.datas;
            Email_id = $scope.datas.Email;

            dataFactory.selectrowevent(Email_id).success(function (response) {
              
                //$scope.Userweb = response[0];
                $scope.Userweb.FirstName = response[0].FIRST_NAME;
                $scope.Userweb.LastName = response[0].LAST_NAME;
                $scope.Userweb.Email = response[0].CONTACT_EMAIL;
                //$scope.Userweb.Phone = response[0].PHONE;
                //$scope.Userweb.Mobile = response[0].MOBILE;
                //$scope.Userweb.Skype = response[0].SKYPE_ID;
                ///$scope.Userweb.WhatsApp = response[0].WHATSAPP_NUMBER;
                $scope.Userweb.Company = response[0].COMPANY_NAME;
                //$scope.Userweb.Designation = response[0].DESIGNATION;
                $scope.Userweb.Notes = response[0].NOTES;
                $scope.Userweb.active = response[0].STATUS;
                if (response[0].role == 0) {
                    $scope.Userweb.role = 6;
                    $scope.value = 6;


                }
                else {
                    $scope.Userweb.role = response[0].role;
                    $scope.value = response[0].role;

                }
                $scope.Userweb.organisation = response[0].ORGID;
                //
                //var dropdownlist = $("#color").data("kendoDropDownList");

                //dropdownlist.value($scope.value);
                //if (response[0].role == 0) {
                //    $scope.flag = true;
                //} else {
                //    $scope.flag = false;
                //}
                //$scope.RoleListDataSource.read();
                //$scope.organisationloadDataSource.read();
                //$scope.Userweb.role = response[0].role;
                //$scope.Userweb.organisation = response[0].ORGID;

            }).then(function () {

                //$scope.RoleListDataSource.read();
                $scope.RoleListDataSource = new kendo.data.DataSource({

                    type: "json",
                    transport: {
                        read: function (options) {
                            //if ($scope.Userweb.Email == "undefined" || $scope.Userweb.Email == "" )
                            //{
                            //    $scope.Userweb.Email = "Null";
                            //}

                            dataFactory.dropdownRoleList($scope.value, $scope.Userweb.Email).success(function (response) {
                                if ($scope.value == 2 || $scope.value == 3) {
                                    $scope.orgFlag = true;
                                }
                                else {
                                    $scope.orgFlag = false;
                                }

                                options.success(response);

                                //   $scope.Userweb.role = $rootScope.Role;


                            }).error(function (error) {
                                options.error(error);
                            });

                        }
                    },
                    cache: false
                });
                //$scope.organisationloadDataSource.read();
                ///  $scope.organisationloadDataSource.read();
                $scope.organisationloadDataSource = new kendo.data.DataSource({

                    type: "json",
                    transport: {
                        read: function (options) {
                            //if ($scope.Userweb.Email == "undefined" || $scope.Userweb.Email == "" )
                            //{
                            //    $scope.Userweb.Email = "Null";
                            //}
                            $scope.value = 6;
                            dataFactory.dropdownRoleList($scope.value, $scope.Userweb.Email).success(function (response) {
                                if ($scope.value == 2 || $scope.value == 3) {
                                    $scope.orgFlag = true;
                                }
                                else {
                                    $scope.orgFlag = false;
                                }

                                options.success(response);

                                //   $scope.Userweb.role = $rootScope.Role;


                            }).error(function (error) {
                                options.error(error);
                            });

                        }
                    },
                    cache: false
                });
                //$scope.organisationloadDataSource.read();
                ///  $scope.organisationloadDataSource.read();
                //$scope.organisationloadDataSource = new kendo.data.DataSource({
                //    type: "json",
                //    async: false,
                //    editable: false,
                //    serverSorting: true,
                //    serverFiltering: true,
                //    sortable: true,
                //    batch: true,
                //    transport: {
                //        read: function (options) {
                //            $scope.value = 6;
                //            if ($scope.value != null && $scope.value != undefined && $scope.value != '')
                //                dataFactory.dropdownchange($scope.value).success(function (response) {

                //                    options.success(response);

                //                    //  $scope.Userweb.organisation = $rootScope.ORGID;

                //                }).error(function (error) {
                //                    options.error(error);
                //                });

                //        }
                //    },

                //    sort: { field: "organisationName", dir: "desc" },
                //    cache: false

                //});

            });




        }

        $scope.WebUserRoleChange = function (e) {
          
            $scope.value = $scope.Userweb.role;
            $scope.Userweb.organisation = 0;

            //$scope.organisationloadDataSource = new kendo.data.DataSource({
            //    type: "json",
            //    async: false,
            //    editable: false,
            //    serverSorting: true,
            //    serverFiltering: true,
            //    sortable: true,
            //    batch: true,
            //    transport: {
            //        read: function (options) {
            //            debugger;
            //            if ($scope.value != null && $scope.value != undefined && $scope.value != '')
            //                dataFactory.dropdownchange($scope.value).success(function (response) {

            //                    options.success(response);

            //                    //  $scope.Userweb.organisation = $rootScope.ORGID;

            //                }).error(function (error) {
            //                    options.error(error);
            //                });

            //        }
            //    },

            //    sort: { field: "organisationName", dir: "desc" },
            //    cache: false

            //});


            //$scope.organisationloadDataSource.read();
            $scope.RoleListDataSource.read();

        };



        $scope.saveuserweb = function (s) {
         
            $scope.value = $scope.Userweb.role;
            $scope.formdatas = s;
            $scope.Validorganization = false;
            if ($scope.value != "2" && $scope.value != "3") {
                if ($scope.formdatas.organisation == '' || $scope.formdatas.organisation == '0' || $scope.formdatas.organisation == undefined) {
                    $scope.Validorganization = true;
                }

            }
            if ($scope.formdatas.FirstName != '' && $scope.formdatas.role != undefined && $scope.formdatas.FirstName != undefined && $scope.formdatas.Email != '' && $scope.formdatas.Email != undefined && $scope.Validorganization == false) {
                dataFactory.saveUserWebdetails($scope.value, $scope.formdatas).success(function (response) {
                    if (response == 'Success.') {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Saved successfully.',
                            type: "info",
                        });

                    }

                })
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Fill all the Required Fields.',
                    type: "info",
                });
            }


        }






        //invite click function//
        $scope.ResetInvite = function () {
            $("#rolesettings").hide();
            $("#Usersettingtest").hide();
            $("#invite").show();
            $scope.userInviteDataSource.read();
            $("#Webuser").hide();
            $("#WebuserListViewGrid").hide();
            $("#user").hide();
        }

        //workflow tab//
        $scope.NewWorkFlow = function () {
            $("#workflowtabclick").addClass("k-state-active");
            $("#workflowsettingtab").removeClass("k-state-active");
            $("#workflowsettingtab").removeClass("k-state-hover");
            $("#workflowtab").show();
            $("#workflowstatusanddescription").show();
            $("#workflowsettings").hide();
        }
        //workflowstatus and description//
        $scope.Workflowstatusdesc = function () {
            $("#workflowstatusanddescription").show();
            $("#workflowtabclick").addClass("k-state-active");
            $("#workflowsettingtab").removeClass("k-state-active");
            $("#workflowsettingtab").removeClass("k-state-hover");
            $("#workflowsettings").hide();
        }
        //workflow settings//
        $scope.ResetWorkFlow = function () {
            $("#workflowstatusanddescription").hide();
            $("#workflowtabclick").removeClass("k-state-active");
            $("#workflowtabclick").removeClass("k-state-hover");
            $("#workflowsettingtab").addClass("k-state-active");
            $("#workflowsettings").show();
            $scope.userWorkFlow.read();
        }

        //system Tool/configuration tab//
        $scope.systemConfiguration = function () {
            $("#settings").show();
            $("#settingstabclick").addClass("k-state-active");
            $("#applicationsettingtabclick").removeClass("k-state-active");
            $("#applicationsettingtabclick").removeClass("k-state-hover");
            $("#applicationsettings").hide();
        }


        //settings//
        $scope.Settings = function () {
           
            $("#applicationsettings").hide();
            $("#settings").show();
            $("#settingstabclick").addClass("k-state-active");
            $("#applicationsettingtabclick").removeClass("k-state-active");
            $("#applicationsettingtabclick").removeClass("k-state-hover");
            $scope.WebConfigFiles.announcementCount = "5";
            $scope.WebConfigFiles.Password = "";
        }
        //Application settings//
        $scope.Preference = function () {
            $("#settings").hide();
            $("#settingstabclick").removeClass("k-state-active");
            $("#settingstabclick").removeClass("k-state-hover");
            $("#applicationsettingtabclick").addClass("k-state-active");
            $("#applicationsettings").show();
        }
        $scope.btnresetclick = function () {
          
            //$scope.WebConfigFiles.ImportSheetProductCount = 5000;
            $scope.WebConfigFiles.announcementCount = '5';
            $scope.WebConfigFiles.Password = "";
            $scope.WebConfigFiles.restoreSourcePath = "";
            $scope.WebConfigFiles.restoreDestPath = "";
            $scope.WebConfigFiles.serverPath = "";
            $scope.WebConfigFiles.dataSource = "";
            $scope.WebConfigFiles.serverUserID = "";
            $scope.WebConfigFiles.backUpAttachement = "";
            $scope.WebConfigFiles.backUpMdfPath = "";
            $scope.WebConfigFiles.backUpDataBasePath = "";

        };

        $scope.sports = new kendo.data.DataSource({
            data: [{
                id: 5,
                name: '5'
            }, {
                id: 10,
                name: '10'
            }, {
                id: 15,
                name: '15'
            },
            {
                id: 10,
                name: '20'
            }]
        });

        //----------------------------------------Backup Restore new changes by Mohan-------------------------------------------//
        //----------------------------------------Backup Restore new changes by Mohan-------------------------------------------//
        $scope.GetAllWorkflowdataSource = new kendo.data.DataSource({
            pageSize: 30,
            type: "json",
            serverFiltering: true,
            editable: true,
            serverPaging: true,
            serverSorting: true, pageable: true,
            transport: {
                read: function (options) {
                    dataFactory.GetAllWorkFlowDataSourceValue(options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },

                update: function (options) {
                    dataFactory.UpdateWorkFlowData(options.data).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update completed.',
                            type: "info"
                        });
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                    $scope.GetAllWorkflowdataSourceTOSTATUS.read();
                },
                Delete: function (options) {
                    dataFactory.UpdateWorkFlowData(options.data).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update completed.',
                            type: "info"
                        });
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return kendo.stringify(options);

                }
            },

            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "STATUS_CODE",
                    fields: {
                        STATUS_NAME: { editable: true, nullable: true },
                        STATUS_DESCRIPTION: { editable: true, validation: { required: true } }
                    }
                }
            }

        });
        $scope.workflowGridOptions = {
            dataSource: $scope.GetAllWorkflowdataSource, autobind: true,
            sortable: true,
            scrollable: true,
            filterable: true,
            pageable: { buttonCount: 5 },
            autoBind: true,
            selectable: "row",
            editable: "inline",
            columns: [{ field: "STATUS_NAME", title: "Status Name" }, { field: "STATUS_DESCRIPTION", title: "Status Description" },
             { command: ["edit"], title: "&nbsp;" },
             { command: [{ name: "destroy", text: "", width: "10px", template: "<a class=\'k-item girdicons\' ng-click=\'DeleteWorkflowStatus($event,this)\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" }] }
            //{ command: ["Delete"], title: "&nbsp;" }
            ],

            dataBound: function (e) {

            },
        };


        $scope.saveworkflow = function () {
            if ($scope.workflowstatus == undefined || $scope.workflowdescription == undefined || $scope.workflowstatus == '' || $scope.workflowdescription == '') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Enter the Workflow Status information before submit.',
                    type: "info",
                });
            }
            else {
                dataFactory.saveworkflow($scope.workflowstatus, $scope.workflowdescription).success(function (response) {
                    if (response == "Saved.") {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Saved successfully.',
                            type: "info",
                        });
                        $scope.GetAllWorkflowdataSource.read();
                        $scope.GetAllWorkflowdataSourceTOSTATUS.read();
                    }


                });
            }
        }

        $scope.Saveworkflowsettings = function () {
            debugger
            if ($scope.Selectedrole == undefined || $scope.Selectedfromstatus == undefined || $scope.SelectedTostatus == undefined || $scope.Selectedrole == '' || $scope.Selectedfromstatus == '' || $scope.SelectedTostatus == '') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select Workflow Status information before submit.',
                    type: "info",
                });
            }
            else {
                dataFactory.Saveworkflowsettings($scope.Selectedrole, $scope.Selectedfromstatus, $scope.SelectedTostatus).success(function (response) {
                    if (response == "Saved.") {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Saved successfully.',
                            type: "info",
                        });

                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Workflow already assigned to this User Role. Please, select the different status.',
                            type: "info",
                        });
                    }
                    $scope.GetAllWorkflowdsettingsataSource.read();

                });
            }
        }
        $scope.GetAllWorkflowdsettingsataSource = new kendo.data.DataSource({
            pageSize: 10,
            type: "json",
            serverFiltering: true,
            editable: true,
            serverPaging: false,
            serverSorting: true, pageable: true,
            transport: {
                read: function (options) {
                    dataFactory.GetAllWorkFlowsettingsDataSourceValue(options.data).success(function (response) {
                        options.success(response.Data);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
            },

        });

        $scope.userWorkFlowSettingsGrid = {

            dataSource: $scope.GetAllWorkflowdsettingsataSource, autobind: true,
            sortable: true,
            scrollable: true,
            filterable: true,
            pageable: { buttonCount: 5 },
            autoBind: true,
            selectable: "row",
            editable: "inline",
            columns: [{ field: "USER_ROLE", title: "Role ID" }, { field: "ROLE_NAME", title: "Role Name" }, { field: "FROM_STATUS", title: "From Status" }, { field: "TO_STATUS", title: "To Status" },
            { command: [{ name: "destroy", text: "", width: "10px", template: "<a class=\'k-item girdicons\' ng-click=\'DeleteFromStatus($event,this)\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" }] }

            ],
            dataBound: function (e) {

            },
        };



        $scope.Rolechange = function () {
            dataFactory.GetAllWorkFlowsettingsDataSourceValue(options.data).success(function (response) {
                options.success(response);
            }).error(function (error) {
                options.error(error);
            });
        }


        $scope.DeleteFromStatus = function ($event, e) {
            dataFactory.DeleteFromStatus(e.dataItem.FROM_STATUS, e.dataItem.TO_STATUS, e.dataItem.USER_ROLE).success(function (response) {
                //options.success(response);

                $.msgBox({
                    title: "Workflow status",
                    content: '' + response + '.',
                    type: "info"
                });
                $scope.GetAllWorkflowdsettingsataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        }


        $scope.DeleteWorkflowStatus = function ($event, e) {
            dataFactory.DeleteWorkflowStatus(e.dataItem.STATUS_NAME, e.dataItem.STATUS_CODE).success(function (response) {
                //options.success(response);

                $.msgBox({
                    title: "Workflow status",
                    content: '' + response + '.',
                    type: "info"
                });
                $scope.GetAllWorkflowdataSource.read();
                $scope.GetAllWorkflowdataSourceTOSTATUS.read();
            }).error(function (error) {
                options.error(error);
            });
        }

        $scope.GetAllWorkflowdataSourceTOSTATUS = new kendo.data.DataSource({
            pageSize: 30,
            type: "json",
            serverFiltering: true,
            editable: true,
            serverPaging: true,
            serverSorting: true, pageable: true,
            transport: {
                read: function (options) {
                    dataFactory.GetAllWorkFlowDataSourceValueTOSTATUS($scope.Selectedfromstatus, options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },

                update: function (options) {
                    dataFactory.UpdateWorkFlowData(options.data).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update completed.',
                            type: "info"
                        });
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                Delete: function (options) {
                    dataFactory.UpdateWorkFlowData(options.data).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update completed.',
                            type: "info"
                        });
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return kendo.stringify(options);

                }
            },

            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "STATUS_CODE",
                    fields: {
                        STATUS_NAME: { editable: true, nullable: true },
                        STATUS_DESCRIPTION: { editable: true, validation: { required: true } }
                    }
                }
            }

        });

        $scope.workflowchange = function ($event, e) {
            $scope.GetAllWorkflowdataSourceTOSTATUS.read();
        }

        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;


        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

        $scope.KendoThemes = {
            dataSource: {
                data: [
                    { Text: "Black", value: "black" },
                    { Text: "Blue Opal", value: "blueopal" },
                    { Text: "Bootstrap", value: "bootstrap" },
                    { Text: "Default", value: "default" },
                    { Text: "HighContrast", value: "highcontrast" },
                    { Text: "Metro", value: "metro" },
                    { Text: "MetroBlack", value: "metroblack" },
                    { Text: "Moonlight", value: "moonlight" },
                    { Text: "Silver", value: "silver" },
                    { Text: "Uniform", value: "uniform" }
                ]
            },
            dataTextField: "Text",
            dataValueField: "value"
        };
        $scope.Theme_Name = "";
        $http.get('../Preference/getmethod')
                            .then(function (data) {
                                $scope.Theme_Name = data.data;
                            });
        $scope.NewProject = {
            ErrorLogFilePath: { validation: { required: true } },
            Temporary_File_Path: '',
            TechSupportURL: '',
            WebcatSyncURL: '',
            WhiteboardWebserviceURL: '',
            DisplayIDColumns: false,
            AllowDuplicateItem_PartNum: false,
            FamilyLevelMultitablePreview: false,
            ShowCustomAPPS: false,
            ProductLevelMultitablePreview: false,
            EnableWorkFlow: false,
            SpellCheckerMode: '',
            // ---CatalogSettings--//
            CatalogFilePath: '',
            CatalogTemplatePath: '',
            CatalogXMLPath: '',
            Export_ImportPath: '',
            // ---Image Settings---//
            ImagePath: '',
            ImageConversionUtilityPath: '',
            WebCatImagePath: '',
            ReferenceTableImagePath: '',
            //---UserSettings--//
            DisplayCategoryIdinNavigator: false,
            DisplayFamilyandRelatedFamily: false,
            UploadImagesPDFForWebSync: false,
            CreateXMLForSelectedAttributesToAllFamily: false,
            DisplaySkuProductCountInAlert: true,
            SKUAlertPercentage: 90,
            Customer_MasterCatalog: 1,
            Subcatalog_Items: 0,
            AssetLimitPercentage: 90,
            EnableSubProduct: false,
            ImagePdfAttachemnt: false,
            ConverterType: '',
            //autoWebSync: false,

        };

        $scope.submit = function () {
            if (!$scope.myForm.$valid) {
                $scope.myForm.Filepath.$dirty = true;
            }
        };
        $scope.catalogDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogDetails($scope.SelectedItem, $localStorage.getCustomerID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            cache: false
        });
        $scope.customerCatalogChange = function (e) {
            if (e.sender.value() !== "") {
            }
        };
        $scope.KendoThemeChange = function (e) {
            //$scope.selectattributes(e.sender.value());
            var value = e.sender.value();
            $scope.ChangeTheme(value);
            $scope.selecteddrpdowntheme = value;
            $http.post('../Preference/ChangeTheme?SelectedTheme=' + $scope.selecteddrpdowntheme)
                            .then(function (data) {
                            });
            $scope.ChangeTheme(value);

        };

        $scope.function = function () {

        };

        $scope.ChangeTheme = function (skinName, animate) {
            window.localStorage.setItem('item_name', skinName);
            var doc = document,
            kendoLinks = $("link[href*='kendo.']", doc.getElementsByTagName("head")[0]),
            commonLink = kendoLinks.filter("[href*='kendo.common']"),
            skinLink = kendoLinks.filter(":not([href*='kendo.common'])"),
            href = location.href,
            skinRegex = /kendo\.\w+(\.min)?\.css/i,
            extension = skinLink.attr("rel") === "stylesheet" ? ".css" : ".less",
            url = commonLink.attr("href").replace(skinRegex, "kendo." + skinName + "$1" + extension),
            exampleElement = $("#example");

            function preloadStylesheet(file, callback) {
                var element = $("<link rel='stylesheet' media='print' href='" + file + "'").appendTo("head");

                setTimeout(function () {
                    callback();
                    element.remove();
                }, 100);
            }

            function replaceTheme() {
                var oldSkinName = $(doc).data("kendoSkin"),
                newLink;
                // if ($.browser.msie) {
                //     newLink = doc.createStyleSheet(url);
                //} else {
                newLink = skinLink.eq(0).clone().attr("href", url);
                newLink.insertBefore(skinLink[0]);
                //}
                skinLink.remove();
                $(doc.documentElement).removeClass("k-" + oldSkinName).addClass("k-" + skinName);
            }
            $scope.onSelect1 = function (e) {

                //    alert('asd');
                //var message = $.map(e.files, function (file) { return file.name; }).join(", ");
                //$scope.LogoFile= '\\Images\\' + message;
                // $scope.CategoryImage = '~/Images/' + message;
                // alert(message);
                //kendoConsole.log("event :: select (" + message + ")");
            };
            $scope.onSuccess1 = function (e) {
                // var someInfo = e.response.someInfo;
                //if (e.operation === "remove") {
                //    $scope.LogoFile = "";
                //} else {
                //    var message = $.map(e.files, function (file) { return file.name; }).join(", ");
                //    $scope.LogoFile = '\\Images\\' + message;
                //}
                //  alert(someInfo);
            };
            if (animate) {
                preloadStylesheet(url, replaceTheme);
            } else {
                replaceTheme();
            }
        };

        $("#files").kendoUpload({
            multiple: false,
            async: {
                saveUrl: "Preference/SaveLogo",
                removeUrl: "Preference/RemoveBannerImage",
                autoUpload: true,
                select: "onSelect1",


            },

        });
        $("#Bannerfiles").kendoUpload({
            multiple: false,
            async: {
                saveUrl: "Preference/SaveBanner",
                removeUrl: "Preference/RemoveBannerImage",
                autoUpload: true,
                select: "onSelect1",


            },

        });
        $scope.PreferenceOption = {
            dataSource: {
                data: [
                    { Pref_Name: "Appletini.isl", id: 1 },
                     { Pref_Name: "Chamelon.isl", id: 2 },
                       { Pref_Name: "Claymation.isl", id: 3 },
                    { Pref_Name: "CS5EnglishDefault.isl", id: 4 },
                    { Pref_Name: "CS5EnglishGreen.isl", id: 5 },
                      { Pref_Name: "ElectricBlue.isl", id: 6 },
                 { Pref_Name: "Harvest.isl", id: 7 },
                   { Pref_Name: "Obsindian.isl", id: 8 },
                   { Pref_Name: "Office2007Black.isl", id: 9 },
                    { Pref_Name: "Office2007Silver.isl", id: 10 },

                       { Pref_Name: "Peach.isl", id: 11 },
                   { Pref_Name: "Pear.isl", id: 12 },
                 { Pref_Name: "RadioFlyer.isl", id: 13 },
                { Pref_Name: "RubberBlack.isl", id: 14 },
                    { Pref_Name: "Trendy.isl", id: 15 }]
            },
            dataTextField: "Pref_Name",
            dataValueField: "id"

        };
        $scope.SpellCheckerOption = {
            dataSource: {
                data: [
                    { List: "None", Spel_id: 1 },
                     { List: "Dialog on Validating", Spel_id: 2 },
                       { List: "As You Type", Spel_id: 3 },
                    { List: "CS5EnglishDefault.isl", Spel_id: 4 },
                    { List: "Dialog on Validating and As", Spel_id: 5 }
                ]

            },
            dataTextField: "List",
            dataValueField: "Spel_id"
        };

        $scope.Display_IDs = function (value) {
            $scope.Display_ID_Column = value;
        };

        $scope.Encrypt = function () {
            var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse("ES001"), key,
            { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

            var encrypted_catlogid = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse("20"), key,
              { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

            window.location = "../Category/Sample?catid=" + encrypted + "&catalogid=" + encrypted_catlogid;
        };
        $(document).keypress(function (e) {
            if (e.which == 13) {
                return false;
            }
        });

        // In this function for reset
        $scope.btnresetclick = function () {

            $scope.init();
        };

        //End
        $scope.convertchangeevent = function (e) {

            if ($scope.convertype == "Image Magic") {
                $scope.ConvertionType = "Image Magic";
            }
            else {
                $scope.ConvertionType = "ReaConverter";
            }
        };

        $scope.savecatalog = function () {
            var Theme_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(null), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            //var DisplayIDColumns_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.DisplayIDColumns), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var ErrorLogFilePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ErrorLogFilePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var Temporary_File_Path_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.Temporary_File_Path), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var TechSupportURL_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.TechSupportURL), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            //var AllowDuplicateItem_PartNum_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.AllowDuplicateItem_PartNum), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var WebcatSyncURL_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.WebcatSyncURL), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var SpelValue_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.SpelValue), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var WhiteboardWebserviceURL_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.WhiteboardWebserviceURL), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            //var ShowCustomAPPS_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ShowCustomAPPS), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            //var EnableWorkFlow_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.EnableWorkFlow), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            //var FamilyLevelMultitablePreview_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.FamilyLevelMultitablePreview), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            //var ProductLevelMultitablePreview_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ProductLevelMultitablePreview), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            //dataFactory.PreferecesApplicationSettings(Theme_Encrypt, DisplayIDColumns_Encrypt, ErrorLogFilePath_Encrypt, Temporary_File_Path_Encrypt, TechSupportURL_Encrypt, AllowDuplicateItem_PartNum_Encrypt, WebcatSyncURL_Encrypt, SpelValue_Encrypt, WhiteboardWebserviceURL_Encrypt, ShowCustomAPPS_Encrypt, EnableWorkFlow_Encrypt, FamilyLevelMultitablePreview_Encrypt, ProductLevelMultitablePreview_Encrypt).success(function (response) {
            if ($scope.NewProject.SubCatalog_Items == null) {
                $scope.NewProject.SubCatalog_Items = 0;
            }
            if ($scope.NewProject.SubCatalog_Items === true) {
                $scope.NewProject.SubCatalog_Items = 1;
            }
            else if ($scope.NewProject.SubCatalog_Items === false) {
                $scope.NewProject.SubCatalog_Items = 0;
            }
            if ($scope.NewProject.Customer_MasterCatalog != null && $scope.NewProject.Customer_MasterCatalog != "" && $scope.ConvertionType != null) {

                if ($scope.webSyncFromDate == undefined) {
                    var ts = new Date();
                    $scope.webSyncFromDate = ts.toLocaleDateString();
                }
                if ($scope.NewProject.autoWebSync == undefined) {
                    $scope.NewProject.autoWebSync = false;
                }
                if ($scope.NewProject.ImagePdfAttachemnt == undefined) {
                    $scope.NewProject.ImagePdfAttachemnt = false;
                }
                if ($scope.NewProject.EnableSubProduct == undefined) {
                    $scope.NewProject.EnableSubProduct = false;
                }
                if ($scope.ConverterType == undefined) {
                    $scope.ConverterType = false;
                }

                dataFactory.PreferecesApplicationSettings(Theme_Encrypt, $scope.NewProject.DisplayIDColumns, ErrorLogFilePath_Encrypt, Temporary_File_Path_Encrypt, TechSupportURL_Encrypt, $scope.NewProject.AllowDuplicateItem_PartNum, WebcatSyncURL_Encrypt, SpelValue_Encrypt, WhiteboardWebserviceURL_Encrypt, $scope.NewProject.ShowCustomAPPS, $scope.NewProject.EnableWorkFlow, $scope.NewProject.FamilyLevelMultitablePreview, $scope.NewProject.ProductLevelMultitablePreview, $scope.getCustomerIDs, $scope.NewProject.DisplaySkuProductCountInAlert, $scope.NewProject.SKUAlertPercentage, $scope.NewProject.Customer_MasterCatalog, $scope.NewProject.SubCatalog_Items, $scope.defaultImagePath, $scope.NewProject.EnableSubProduct, $scope.CatalogItemNumber + "~" + $scope.CatalogSubItemNumber, $scope.NewProject.autoWebSync, $scope.webSyncFromDate, $scope.NewProject.ImagePdfAttachemnt, $scope.ConvertionType).success(function (response) // $scope.NewProject.autoWebSyncImagePdfAttachemnt
                {

                    if ($scope.NewProject.SubCatalog_Items == null) {
                        $scope.NewProject.SubCatalog_Items = 0;
                    }

                    if (response === 1) {
                        // Here assign the coloumId value in to ["$rootScope.DisplayIdcolumns"]
                        $localStorage.CatalogItemNumber = $scope.CatalogItemNumber;
                        $localStorage.CatalogSubItemNumber = $scope.CatalogSubItemNumber;

                        $rootScope.CatalogItemNumber = $scope.CatalogItemNumber;
                        $rootScope.CatalogSubItemNumber = $scope.CatalogSubItemNumber;

                        $localStorage.DisplayIdBycoloumValue = $scope.NewProject.DisplayIDColumns;
                        $rootScope.DisplayIdcolumns = $scope.NewProject.DisplayIDColumns;

                        $rootScope.EnableWorkflow = $scope.NewProject.EnableWorkFlow;
                        $localStorage.EnableSubProductLocal = $scope.NewProject.ENABLESUBPRODUCT;
                        $rootScope.EnableSubProduct = $scope.NewProject.ENABLESUBPRODUCT;
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Saved successfully.',
                            type: "info"
                        });
                    }
                }).error(function (error) {
                    options.error(error);
                });
                $rootScope.EnableSubProduct = $scope.NewProject.ENABLESUBPRODUCT;
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Select default Master Catalog.',
                    type: "info"
                });
            }
        };

        $scope.savecatalogsettings = function () {
            var CatalogFilePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.CatalogFilePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var CatalogTemplatePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.CatalogTemplatePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var CatalogXMLPath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.CatalogXMLPath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var Export_ImportPath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.Export_ImportPath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

            dataFactory.PreferecesCatalogSettings(CatalogFilePath_Encrypt, CatalogTemplatePath_Encrypt, CatalogXMLPath_Encrypt, Export_ImportPath_Encrypt, $scope.getCustomerIDs).success(function (response) {

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.saveImageSettings = function () {
            var ImagePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ImagePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var ImageConversionUtilityPath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ImageConversionUtilityPath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var WebCatImagePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.WebCatImagePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            var ReferenceTableImagePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ReferenceTableImagePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

            dataFactory.PreferecesImageSettings(ImagePath_Encrypt, ImageConversionUtilityPath_Encrypt, WebCatImagePath_Encrypt, ReferenceTableImagePath_Encrypt, $scope.ImageSettings.Size1Width, $scope.ImageSettings.Size1Hht, $scope.ImageSettings.Size1Folder, $scope.ImageSettings.Size2Width, $scope.ImageSettings.Size2Hht, $scope.ImageSettings.Size2Folder, $scope.ImageSettings.Size3Width, $scope.ImageSettings.Size3Hht, $scope.ImageSettings.Size3Folder, $scope.ImageSettings.Size4Width, $scope.ImageSettings.Size4Hht, $scope.ImageSettings.Size4Folder, $scope.ImageSettings.Size5Width, $scope.ImageSettings.Size5Hht, $scope.ImageSettings.Size5Folder, $scope.ImageSettings.Size6Width, $scope.ImageSettings.Size6Hht, $scope.ImageSettings.Size6Folder, $scope.getCustomerIDs).success(function (response) {

            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.saveUsersettings = function () {
            dataFactory.PrferencesaveUsersettings($scope.NewProject.DisplayCategoryIdinNavigator, $scope.NewProject.DisplayFamilyandRelatedFamily, $scope.NewProject.UploadImagesPDFForWebSync, $scope.NewProject.CreateXMLForSelectedAttributesToAllFamily, $scope.getCustomerIDs).success(function (response) {
                if (response == 1) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Saved successfully.',
                        type: "info"
                    });
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.Theme = "Appletini.isl";
        $scope.SpelValue = "None";
        $scope.catalogChange = function (e) {
            $scope.Theme = e.sender.text();

        };
        $scope.SpellChange = function (e) {

            $scope.SpelValue = e.sender.text();
        };

        $scope.select = function () {
            value: " ";
        };

        $scope.ImageSettings = {
            Size1Width: 0,
            Size1Hht: 0,
            Size1Folder: '',
            Size2Width: 0,
            Size2Hht: 0,
            Size2Folder: '',
            Size3Width: 0,
            Size3Hht: 0,
            Size3Folder: '',
            Size4Width: 0,
            Size4Hht: 0,
            Size4Folder: '',
            Size5Width: 0,
            Size5Hht: 0,
            Size5Folder: '',
            Size6Width: 0,
            Size6Hht: 0,
            Size6Folder: '',
        };

        // $scope.GetAllWorkflowdataSource.read();
        $scope.GetAllWorkflowdataSource = new kendo.data.DataSource({
            pageSize: 10,
            type: "json",
            serverFiltering: true,
            editable: true,
            serverPaging: true,
            serverSorting: true, pageable: true,
            transport: {
                read: function (options) {
                    dataFactory.GetAllWorkFlowDataSourceValue(options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },

                update: function (options) {
                    dataFactory.UpdateWorkFlowData(options.data).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update completed.',
                            type: "info"
                        });
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },

                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return kendo.stringify(options);

                }
            },

            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "STATUS_CODE",
                    fields: {
                        STATUS_NAME: { editable: true, nullable: true },
                        STATUS_DESCRIPTION: { editable: true, validation: { required: true } }
                    }
                }
            }

        });

        $scope.workflowGridOptions = {
            dataSource: $scope.GetAllWorkflowdataSource, autobind: true,
            sortable: true,
            scrollable: true,
            filterable: true,
            pageable: { buttonCount: 5 },
            autoBind: true,
            selectable: "row",
            editable: "inline",
            columns: [{ field: "STATUS_NAME", title: "Status Name" }, { field: "STATUS_DESCRIPTION", title: "Status Description" },
             { command: ["edit"], title: "&nbsp;" }],

            dataBound: function (e) {

            },
        };

        /**Check the WebApiConnection Start**/
        $scope.checkWebApiConnection = function () {
            var destinationUrl = $scope.NewProject.WebcatSyncURL + '\CheckWebApiConnection';
            $.get(destinationUrl, function (responseText) {
                if (responseText == true) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Connection Successful',
                        type: "info"
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Connection failed.Please verify connection url.',
                        type: "info"
                    });
                }
            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Connection failed.Please verify connection url.',
                    type: "info"
                });
            });;
        }
        /**End Connection**/

        ///-Get Role OF user
        $scope.getUserSuperAdmin = function () {

            dataFactory.getUserSuperAdmin().success(function (response) {
                if (response !== "" && response !== null) {
                    if (response == "1") {
                        $("#idCatalogValues").show();
                        $("#autoWebSync").show();
                    }
                    else if (response == "2") {
                        $("#idCatalogValues").show();
                        $("#autoWebSync").show();
                    }
                    else {
                        $("#idCatalogValues").hide();
                        $("#autoWebSync").hide();

                    }
                }

            }).error(function (error) {
                options.error(error);
            });
        };
        ///

        $scope.init = function () {
            $scope.UserSettingstest();
            $("#editablepop").hide();

            if ($localStorage.getCatalogID === undefined) {
                $scope.selecetedCatalogId = 0;
            } else {
                $scope.selecetedCatalogId = $localStorage.getCatalogID;
            }
            if ($localStorage.getCustomerID == undefined) {
                $scope.getCustomerIDs = 0;
            }
            else {
                $scope.getCustomerIDs = $localStorage.getCustomerID;
                dataFactory.GetPreferencesDetails($scope.getCustomerIDs).success(function (response) {
                    $scope.NewProject = response[0];
                });
            }
            dataFactory.GetConverttypeDetails($localStorage.getCustomerID).success(function (response) {
                $scope.ImagepageConvertType = response;
                //options.success(response);
            }).error(function (error) {
                options.error(error);
            });
            $("#productpreview").hide();
            //$scope.GetAllWorkflowdataSource.read();
            $scope.getUserSuperAdmin();
        };

        $scope.ImageConvert = new kendo.data.DataSource({
            data: ['Image Magic', 'ReaConverter'],


        });

        $scope.UserRoleChange = function (e) {
          

            if (e.sender.value() == "") {

                $scope.flag = true;
            }
            else {
                $scope.flag = false;
            }

        };

        $scope.ConvertionType = ''

        $scope.ImagepageConvertType = 'ReaConverter'
        $scope.init();



        $scope.userReadOnlyAttributesDataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            batch: true,
            transport: {
                read: function (options) {
                    dataFactory.getUserNameBasedonGroupName($scope.User1.UserRoles).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.userRoleforGroupdropdowndatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetUsersFordropdown().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });


        $scope.onUserRoleChangeGroup = function (e) {
            
            $scope.userRoleId = e.sender.value();

            $scope.userGroupManagment();
            $scope.loadUserNameBasedOnGroupDataSource.read();
        };



        $scope.userGroupManagment = function () {
          
            $("#usertabclick").removeClass("k-state-active");
            $("#usertabclick").removeClass("k-state-hover");
            $("#usergroupclick").addClass("k-state-active");
            $("#userpermissionclick").removeClass("k-state-active");
            $("#userpermissionclick").removeClass("k-state-hover");
            $("#userexportclick").removeClass("k-state-active");
            $("#userexportclick").removeClass("k-state-hover");
            $("#ddluserrole2").show();
            $("#AddUserToGroup").hide();
            $("#user").hide();
            $("#rolesettings").hide();
            $("#userGroupSettings").show();
            $scope.loadUserNameBasedOnGroupDataSource = new kendo.data.DataSource({
                pageSize: 10,
                batch: false,
                autoBind: true,
                serverPaging: false,
                serverSorting: false,
                serverFiltering: false,
                transport: {
                    read: function (options) {
                        //$scope.User1.UserRoles = 30;
                        if ($scope.userRoleId == null || $scope.userRoleId == "") {
                            $scope.RoleId = $scope.userRoleId;
                            $scope.RoleId = 0;
                        } else {
                            $scope.RoleId = $scope.userRoleId;
                        }
                        dataFactory.getUserNameBasedonGroupName($scope.RoleId, $scope.CustomerId).success(function (response) {
                           
                            options.success(response);

                        }).error(function (error) {
                            options.error(error);
                        });


                    },
                    parameterMap: function (options) {
                        return kendo.stringify(options);
                    }
                },
                schema: {
                    data: function (data) { return data; },
                    total: function (data) {
                        return data.length;
                    }
                }
            });
        }
        ///option

        $scope.loadUserNameBasedOnGroupGridOptions = {
            dataSource: $scope.loadUserNameBasedOnGroupDataSource,
            autoBind: true,
            sortable: true,
            filterable: true,
            autoBind: true,
            selectable: false,
            pageable:true,
            editable: "popup",

            pageable: { buttonCount: 5 },


            columns: [
            { field: "RoleName", title: "Group", width: "10%" },
            { field: "Email", title: "User ID", width: "15%",},
             {
                 field: "Edit", title: "Actions", template: "<a class='glyphicon fa fa-user-plus blue btn-xs-icon btn org-btn'  style='padding : 7px 9px 12px 7px;'  title = \'Add Users To Group\' ng-click='\addGroupTousers()\'></a>" + "<a class='glyphicon glyphicon-remove blue btn-xs-icon btn org-btn'  style='padding : 7px 9px 12px 7px;'  title = \'Remove This User from Group\' ng-click='\deleteGroupfromusers()\'></a>"
                 , width: "5%"
             },


            ], dataBound: function (e) {
            },

        };
        //$scope.deleteGroupfromusers = function () {
        //    debugger;
        //}
        //user delete////
        $scope.deleteUser = function (row) {
           
            $scope.selecteduserEmail = row.dataItem.EMAIL;
            dataFactory.deleteUser($scope.selecteduserEmail).success(function (response) {
                if (response == "Update.") {
                    $.msgBox({
                        title: "MarketStudio",
                        content: "User Deleted Successfully!",
                        type: "Success"
                    });
                    $scope.userListDataSource.read();
                } else {
                    $.msgBox({
                        title: "MarketStudio",
                        content: "Admin user Cannot be Deleted",
                        type: "error"
                    });

                }
            });

        }
        //set status active and inactive///
        $scope.changeStatus = function (row) {
            $scope.status = row.dataItem.EMAIL;
            dataFactory.changeStatus($scope.status).success(function (response) {
                if (response == "InActive") {
                    $.msgBox({
                        title: "MarketStudio",
                        content: "Status InActivated!",
                        type: "Success"
                    });
                    $scope.userListDataSource.read();
                } else if (response == "Active.") {
                    $.msgBox({
                        title: "MarketStudio",
                        content: "Status Activated!",
                        type: "Success"
                    });
                    $scope.userListDataSource.read();
                } else {
                    $.msgBox({
                        title: "MarketStudio",
                        content: "Admin Status Cannot be Modified!",
                        type: "Success"
                    });
                    $scope.userListDataSource.read();

                }


            })
        }
        $scope.getRoleName = '';

        $scope.addGroupTousers = function () {
          
            if ($scope.userRoleId != "" && $scope.userRoleId != undefined) {
                $("#ddluserrole2").hide();
                $("#userGroupSettings").hide();
                $("#AddUserToGroup").show();
                dataFactory.getGroupName($scope.userRoleId).success(function (response) {
                    $scope.getRoleName = response;
                });

                $scope.loadUserListExceptSelectedDataSource = new kendo.data.DataSource({
                    pageSize: 10,
                    batch: false,
                    autoBind: true,
                    serverPaging: false,
                    serverSorting: false,
                    serverFiltering: false,
                    transport: {
                        read: function (options) {
                            //$scope.User1.UserRoles = 30;
                            if ($scope.userRoleId != null && $scope.userRoleId != "") {
                                dataFactory.getUsernameListExceptSelected($scope.userRoleId, $scope.CustomerId).success(function (response) {
                                 
                                    options.success(response);
                                    $scope.allUserDetails = response;

                                }).error(function (error) {
                                    options.error(error);
                                });
                            } else {
                                $.msgBox({
                                    title: "Add Users To Groups",
                                    content: "Please select the Group",
                                    type: "error"
                                });


                            }

                        },
                        parameterMap: function (options) {
                            return kendo.stringify(options);
                        }
                    },
                    schema: {
                        data: function (data) { return data; },
                        total: function (data) {
                            return data.length;
                        }
                    }
                });

            } else {
                $.msgBox({
                    title: "MarketStudio",
                    content: "Please select the Group!",
                    type: "error"
                });
            }

        }



        $scope.loadUserNameExceptSelectedOptions = {
            dataSource: $scope.loadUserListExceptSelectedDataSource,
            autoBind: true,
            sortable: true,
            filterable: true,
            autoBind: true,
            selectable: false,
            editable: "popup",



            pageable: { buttonCount: 5 },


            columns: [

               { field: "LastName", title: "Users", width: "5px", template: "#= LastName +', ' + FirstName #" },
                //{ field: "Flag", title: "Selected", width: "5px", template: "#if(Flag == 'true'){checked='checked'} "},
                { field: "flag", title: "Selected", width: "5px", template: '# if(flag==true) {#<input type="checkbox" checked="checked"  ng-click="selectedUser1($event, this)"></input>#} else if(flag==false) {#<input type="checkbox"  ng-click="selectedUser($event, this)"></input>#}#', filterable: false },




            ], dataBound: function (e) {
            },

        };

        $scope.checkAll = function (e, id) {
         
            $scope.checkall = true;
            $scope.items = $scope.allUserDetails;
        }
        $scope.selectedUser = function (e, id) {
          
            $scope.items.push({
                "FirstName": id.dataItem.FirstName,
                "LastName": id.dataItem.LastName,
                "Email": id.dataItem.Email
            })
        }
        $scope.items2 = [];
        $scope.selectedUser1 = function (e, id) {
           
            $scope.items2.push(id.dataItem.Email);
            dataFactory.uncheckUserfromGroup($scope.userRoleId, $scope.items2).success(function (response) {
                if (response == "Deleted.") {
                    $scope.items2 = [];
                    $scope.loadUserListExceptSelectedDataSource.read();
                }
            });

        }



        $scope.selectedUserUpdate = function (event, e) {
           
            dataFactory.updateGroupName($scope.CustomerId, $scope.userRoleId, $scope.items).success(function (response) {
                if (response = "Update") {
                    $.msgBox({
                        title: "Add Users To Groups",
                        content: "Groups updated Succesfully",
                        type: "success"
                    });
                    $scope.items = [];
                    //$scope.checkall = false;
                    //scope.loadUserListExceptSelectedDataSource.read();

                } else {
                    $.msgBox({
                        title: "Add Users To Groups",
                        content: "Admin group member Cannot be Changed",
                        type: "success"
                    });
                    $scope.items = [];
                    $scope.checkall = false;
                    $scope.loadUserListExceptSelectedDataSource.read();
                }

            });

        }

        $scope.backtoGroups = function () {
           
            $scope.loadUserNameBasedOnGroupDataSource.read();
            $scope.checkall = false;
            $("#ddluserrole2").show();
            $("#userGroupSettings").show();
            $("#AddUserToGroup").hide();

        }
        //add organisation Name to table //
        $scope.addOrganisationname = function () {
         
            $("#newUserEntryView").hide();
            $("#AddNewOrganisation").show();
            $("#editGroupName").hide();
        }
        $scope.backtoAdduser = function () {
          
            $("#newUserEntryView").show();
            $("#AddNewOrganisation").hide();
            $("#editGroupName").hide();
            $("#EditNewOrganisation").hide();
            $scope.editCustomer();

        }
        $scope.saveOrganisation = function (a) {
          
            $scope.organizationName = a;
            if ($scope.organizationName != "" && $scope.organizationName != undefined) {
                dataFactory.saveOrganisation($scope.organizationName).success(function (response) {
                    if (response == 'Inserted') {
                        $.msgBox({
                            title: "MarketStudio",
                            content: "Organization Created Succesfully",
                            type: "success"
                        });

                        $scope.organizationName.Name = "";
                    } else {
                        $.msgBox({
                            title: "MarketStudio",
                            content: "Organization Name already Exsist!",
                            type: "error"
                        });
                        $scope.organizationName = "";
                    }

                })
            } else {
                $.msgBox({
                    title: "MarketStudio",
                    content: "Enter Organization Name!",
                    type: "error"
                });
                $scope.organizationName.Name = "";
            }
        }

        $scope.organizationChange = function (row) {
           
            $scope.organizationId = row;
        }
        $scope.organizationNameEdit = "";
        $scope.editOrganisationname = function () {
          
            if ($scope.organizationId != "" && $scope.organizationId != undefined) {
                dataFactory.getOrganisationName($scope.organizationId).success(function (response) {
                    $scope.organizationNameEdit = response;
                    $scope.exisitingValue = $scope.organizationNameEdit;
                    if ($scope.organizationNameEdit != null) {
                        $("#newUserEntryView").hide();
                        $("#EditNewOrganisation").show();


                    }

                });
            } else {
                $.msgBox({
                    title: "MarketStudio",
                    content: "Select Organization Name!",
                    type: "error"
                });

            }
        }
        $scope.saveEditOrganisationName = function (b) {
           
            dataFactory.updateOrganisationName(b, $scope.exisitingValue).success(function (response) {
                if (response == "Updated") {

                    $.msgBox({
                        title: "MarketStudio",
                        content: "Updated Succesfully",
                        type: "success"
                    });

                }
            });

        }

        $scope.deleteOrganizationName = function () {
          
            if ($scope.organizationId != undefined) {
                dataFactory.checkIforganizationIsassociated($scope.organizationId).success(function (response) {
                    if (response == "Not Associated") {
                        $scope.checkUserAssociated = false;
                        dataFactory.deleteOrganization($scope.organizationId, $scope.checkUserAssociated).success(function (response) {
                            if (response == 'Deleted') {

                                $.msgBox({
                                    title: "MarketStudio",
                                    content: "Organization Deleted Succesfully",
                                    type: "success"
                                });

                            }
                        });

                    } else {
                        $.msgBox({
                            title: "MarketStudio",
                            content: "This Organization is been associated to another User.Are you sure do you want delete it?",
                            type: "confirm",
                            buttons: [{ value: "Yes" }, { value: "No" }],
                            success: function (result) {
                                if (result === "Yes") {
                                    $scope.checkUserAssociated = true;
                                    dataFactory.deleteOrganization($scope.organizationId, $scope.checkUserAssociated).success(function (response) {
                                        if (response == 'Group Deleted Users Moved to Default Organization') {

                                            $.msgBox({
                                                title: "MarketStudio",
                                                content: "Organization Deleted.User Associated to this Organization is Set to Default Organization!",
                                                type: "success"
                                            });
                                            $scope.organisationloadDataSource.read();
                                        }
                                    });

                                }

                            }
                        });

                    }

                });
            } else {
                $.msgBox({
                    title: "MarketStudio",
                    content: "Select Organization",
                    type: "Error"
                });
            }
        }
        $scope.deleteGroup = function () {
           
            if ($scope.userRoleId != undefined && $scope.userRoleId != "") {
                dataFactory.checkifUserExistInTheSelectedGroup($scope.userRoleId).success(function (response) {
                    if(response>=1){
                        $.msgBox({
                            title: "MarketStudio",
                            content: "There are users assinged to this group. Please remove those users then try again.",
                            type: "Error"
                        });
                    } else {
                        $.msgBox({
                            title: "MarketStudio",
                            content: "Are you sure do you want delete this Group?",
                            type: "confirm",
                            buttons: [{ value: "Yes" }, { value: "No" }],
                            success: function (result) {
                                $scope.userDelete = false;
                                dataFactory.deleteGroups($scope.userRoleId, $scope.userDelete).success(function (response) {
                                    if (response == 'Group Deleted!') {
                                        $.msgBox({
                                            title: "MarketStudio",
                                            content: "Group Deleted successfully",
                                            type: "Error"
                                        });
                                        $scope.userRoleforGroupdropdowndatasource.read();
                                    }


                                });

                            }
                        })
                    }

                });

            }else
            {
                $.msgBox({
                    title: "MarketStudio",
                    content: "Select Group To Delete!",
                    type: "Error"
                });
            }
        }

        $scope.deleteGroupfromusers = function () {
           
            if ($scope.userRoleId != undefined && $scope.userRoleId != "") {
                dataFactory.removeGroupsfromUser($scope.userRoleId).success(function (response) {
                    if (response == 'Group Removed!') {
                        $.msgBox({
                            title: "MarketStudio",
                            content: "Group Removed Successfully",
                            type: "Error"
                        });
                        $scope.loadUserNameBasedOnGroupDataSource.read();
                        $route.reload();
                    }
                });

            } else {
                $.msgBox({
                    title: "MarketStudio",
                    content: "Select Group!",
                    type: "Error"
                });
            }

        }

        $scope.editGroup = function () {
           
            if ($scope.userRoleId != undefined && $scope.userRoleId != "") {
                $("#userGroupSettings").hide();
                $("#editGroupName").show();
                dataFactory.editGroupName($scope.userRoleId).success(function (response) {
                    $scope.editGroupName = "";
                    $scope.editGroupName = response;
                    $scope.editGroupNameold = $scope.editGroupName;
                });
            } else {
                $.msgBox({
                    title: "MarketStudio",
                    content: "Select Group!",
                    type: "Error"
                });
            }
        }

        $scope.saveEditedGroupName = function (editedGroupName) {
          
            $scope.editGroupNameNew = editedGroupName;
            dataFactory.saveEditName($scope.editGroupNameNew, $scope.editGroupNameold, $scope.CustomerId).success(function (response) {
                if (response == 'Updated.') {
                    $.msgBox({
                        title: "MarketStudio",
                        content: "Group Saved!",
                        type: "success"
                    });
                }
            });
        }
        $scope.backToAddGroup = function () {
           
            $("#userGroupSettings").show();
            $("#editGroupName").hide();
            $scope.userRoleforGroupdropdowndatasource.read();
        }



    }]);
