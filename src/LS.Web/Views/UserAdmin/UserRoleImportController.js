﻿LSApp.controller('UserRoleImportController', ['$scope', 'dataFactory', '$http', '$compile', '$rootScope', '$timeout', 'ngTableParams', 'blockUI', '$localStorage', '$location', function ($scope, dataFactory, $http, $compile, $rootScope, $timeout, ngTableParams, blockUI, $localStorage, $location) {
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    $scope.hideImportType = false;
    $scope.Attributediv = "true";
    $scope.errorlogtype = "Error Log1";
    $scope.roleerrorlog = false;
    $scope.passvalidationNext = false;
    $scope.UploadFile = null;
    $scope.createTemplate = false;
    $scope.choosefileimp = true;
    $scope.Atrmappingimp = false;
    $scope.impvalidation = true;
    $scope.import = true;
    $scope.templateDetails = true;
    $scope.logim = true;
    $scope.NextToTemplate = false;
    $scope.ImportFormat = "XLS";
    $scope.Errorlogoutputformat = "XLS";
    $scope.IsFormSubmitted = false;
    $scope.fileChoose = false;
    $scope.AttributeMappingdiv = false;
    $scope.excelPath = "";
    $scope.passvalidationNext = true;
    $scope.allowDuplication = "0";
    $scope.importErrorWindowdiv = false;
    $scope.importpicklistdiv = false;
    $scope.importErrorFileWindowdiv = false;
    $scope.sheetvalidationdiv = false;
    $scope.importSuccessWindowdiv = false;
    $scope.ProcessName = "";
    $scope.ResultDisplay = false;
    $scope.missingColumnInValid = false;
    $scope.validationPassed = false;
    $scope.deleteEnable = true;
    $scope.showValidatediv = false;
    $scope.templateFirst = true;
    $scope.templatesecond = false;
    $scope.attrMap = false;
    $scope.nextSheetProcess = false;
    $scope.importCompletedStatus = true;
    $scope.ValidateDiv = false;
    $scope.validationSuccess = true;
    $scope.ImportType = "";
    $scope.sampleEnable = false;
    $scope.EnableBatch = false;
    $scope.BatchImport = false;
    $scope.ItemImport = false;
    $scope.validated = { Column: true, Id: true, Picklist: true, dataType: true, subcatalog: true };
    $scope.displayFileName = "";
    $scope.ValidationBackdiv = true;
    $scope.userTypeCheck = false;
    $scope.scheduleImport = false;
    $scope.radioButtonYes = function () {
        $scope.allowDuplication = "1";
    };
    $scope.radioButtonNo = function () {
        $scope.allowDuplication = "0";
    };

    $scope.allowDuplication1 = "0";
    $scope.radioButtonYes1 = function () {
        $scope.allowDuplication1 = "1";
    };
    $scope.radioButtonNo1 = function () {
        $scope.allowDuplication1 = "0";
    };
    $scope.importLog = {

        CatalogName: "",
        InsertedRecords: 0,
        UpdatedRecords: 0,
        DeletedRecords: 0,
        TimeTaken: "",
        SkippedRecords: 0
    }

    $scope.selectFileforUploadRead = function (file) {
        this.value = null;
        $rootScope.fileHeader = XLSX.utils.sheet_to_json(file.Sheets[file.SheetNames[0]], { header: 1 })[0];
        $rootScope.fileData = XLSX.utils.sheet_to_json(file.Sheets[file.SheetNames[0]]);
        $scope.SelectedFileForUpload = file[0];
        $scope.$apply(function () {
            $scope.SelectedFileForUploadnamemainproduct = file[0].name;
            $rootScope.SelectedFileForUploadnamemain = file[0].name;
            if ($rootScope.SelectedFileForUploadnamemain.length <= 40) {
                $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain;
            }
            else {
                $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain.substring(0, 40) + "." + $rootScope.SelectedFileForUploadnamemain.split('.')[1];
            }
            $scope.IsFormSubmitted = true;
            $scope.fileChoose = true;
        });
    };


    $scope.selectFileforUpload = function (file) {
        this.value = null;
        $scope.SelectedFileForUpload = file[0];
        $scope.$apply(function () {
            $scope.SelectedFileForUploadnamemainproduct = file[0].name;
            $rootScope.SelectedFileForUploadnamemain = file[0].name;
            if ($rootScope.SelectedFileForUploadnamemain.length <= 40) {
                $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain;
            }
            else {
                $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain.substring(0, 40) + "." + $rootScope.SelectedFileForUploadnamemain.split('.')[1];
            }
            $scope.IsFormSubmitted = true;
            $scope.fileChoose = true;
        });
    };

    $scope.ResetPage = function () {
        angular.element("input[type='file']").val(null);
        $scope.SelectedFileForUpload = null;
        $scope.TemplateNameImport = "";
        $scope.backbutton = false;        
        $rootScope.SelectedFileForUploadnamemain = "";
        $scope.SelectedFileForUploadnamemainsam = "";
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#choosefileimp").addClass("leftMenuactive");
        $scope.templateDetails = true;
        $scope.AttributeMappingdiv = false;
        $scope.fileChoose = false;
        $scope.showValidatediv = false;
        $scope.templateNamedisabled = false;
        $scope.ImportcatalogIdAttr = "";
        if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
            $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);        
    };

    $scope.SaveFile = function (templateDetails, importtype) {
        if (importtype != undefined) {
            $rootScope.importtypeValue = localStorage.getItem("saveImportType");
            $scope.ImportType = $rootScope.importtypeValue;
            $rootScope.valimporttype = $rootScope.importtypeValue;
            blockUI.start();
            $scope.IsFormSubmitted = true;
            $scope.Message = "";
            if ($scope.SelectedFileForUpload == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose a file.',
                });
                blockUI.stop();
            }
            if ($scope.SelectedFileForUpload !== null) {
                $scope.ImportFormat = 'xls';
                var fileExtension = $scope.SelectedFileForUpload.name.split('.')[1];
                if (fileExtension == 'txt') {
                    fileExtension = 'csv';
                }
                if ($scope.ImportFormat.toString().toUpperCase() != fileExtension.toString().toUpperCase()) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please choose a valid file.',
                    });
                    blockUI.stop();
                    return true;
                }

                $scope.ChechFileValid($scope.SelectedFileForUpload);
                $scope.UploadFile($scope.SelectedFileForUpload);
            }
            else {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose a file.',
                });
                blockUI.stop();
            }
        }

    };

    $scope.SelectedValues = [];
    $scope.ChechFileValid = function (file) {
        var isValid = false;
        $scope.enablevalidation();
        if ($scope.SelectedFileForUpload != null) {
            if (file.name.contains(".xls") || file.name.contains(".xlsx")) {
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
        }
        else {
            $scope.FileInvalidMessage = "File required!";
        }
        $scope.IsFileValid = isValid;
    };

    $scope.UploadFile = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        $rootScope.SelectedFileForUploadnamemain = file.name;
        $http.post("/Import/SaveFilesFullImport?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                if (d.split(',')[1].toUpperCase() == "TRUE") {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Invalid sheet, please download the sample template.',
                        type: "error"
                    });
                    if ($scope.ImportcatalogIdAttr != undefined && $scope.ImportcatalogIdAttr != '')
                        $scope.IsFormSubmitted = false;
                    else
                        $scope.IsFormSubmitted = false;
                    $scope.Message = "";
                    var $el = $('#importfile1');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    var $el1 = $('#importfile2');
                    $el1.wrap('<form>').closest('form').get(0).reset();
                    $el1.unwrap();
                    $rootScope.SelectedFileForUploadnamemain = "";
                    blockUI.stop();
                    return;
                }
                else {
                    if ($scope.ImportFormat.toString().toUpperCase().includes('XLS')) {
                        $scope.excelPath = d.split(',')[0];
                        $scope.importExcelSheetSelction.read();
                    }
                }

            }).error(function () {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
            });
        return;
    };

    $scope.enablevalidation = function () {
        $scope.passvalidationimportenable = true;
        return;
        dataFactory.enableitemvalidation().success(function (response) {
            $scope.passvalidation = response;
            if (response == "true") {
                $scope.passvalidationimport = false;
                $scope.passvalidation = true;
            }
            else {
                $scope.passvalidationimport = true;
                $scope.passvalidation = false;
            }
            $scope.passvalidationimport = true;
            $scope.passvalidation = false;
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.skipValidateimport = function () {
        $scope.passvalidationimportenable = false;
        $scope.ResultDisplay = false;
        $scope.AttributeMappingdiv = true;
        $scope.ProcessName = "Import Content";
        $scope.passvalidationNext = false;
        $scope.showValidatediv = false;
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#imptamp").removeClass("leftMenuactive");
        $("#import").addClass("leftMenuactive");
        $("#importSelectionTable").css("margin-top", "50px");
    };

    if ($scope.excelPath !== "") {
        $scope.tblBasicDashImportBoard = new ngTableParams({ page: 1, count: 5 },
            {
                count: [],
                total: function () { return $scope.basicprodImportData.length; },
                getData: function ($defer, params) {
                    var filteredData = $scope.basicprodImportData;
                    var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
    }

    $scope.btnNextImport = function (templateDetails) {
        $scope.nextSheetProcess = false;
        if (($scope.TemplateNameImport == "" || $scope.selectedTemplate == "") && $scope.catalogIdAttr == "" && $scope.ImportFormat == "") {
            return;
        };
        if ($scope.ImportcatalogIdAttr == undefined || $scope.ImportcatalogIdAttr.toString().toUpperCase() == "ALL" || $scope.ImportcatalogIdAttr == "") {
            $scope.ImportcatalogIdAttr = 0;
        }
        if ($scope.TemplateNameImport == undefined) {
            $scope.TemplateNameImport = "";
        }
        $scope.importGridOptions($scope.importSelectedvalue, $scope.excelPath);
        $scope.templateDetails = false;
        $("#importSelectionTable").css("margin-top", "0px");
        $scope.AttributeMappingdiv = false;
        $("#choosefileimp").removeClass("leftMenuactive");
        $scope.SheetValidationImport();
        $scope.Next();
        blockUI.stop();
    };

    $scope.DeleteTemplatedetails = function (selectedTemplate) {
        if (selectedTemplate == null || selectedTemplate == "" || selectedTemplate == undefined) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Template.',
                type: "info"
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to delete the template?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        dataFactory.DeleteTemplate(selectedTemplate).success(function (response) {
                            if (response == "Deleted Success") {
                                $scope.TemplateNameImport = "";
                                $scope.ResetPage();

                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Delete successful.',
                                    type: "info"
                                });
                                $scope.selectedTemplate = 0;
                            }
                        });
                    }
                    else {
                        return;
                    }
                }
            });
        }
    };
    $scope.listAllSheet = [];
    $scope.importExcelSheetSelction = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.importExcelSheetSelection($scope.excelPath).success(function (response) {
                    options.success(response);
                    if ($rootScope.EnableSubProduct == false) {
                        var subProdList = [];
                        if (response != null && response.length != 0) {
                            $scope.subproduct = response[0].TABLE_NAME;
                        }
                    }
                    if (response != null && response.length != 0) {
                        $scope.importSelectedvalue = response[0].TABLE_NAME;
                        $scope.importSheetSelected = response[0].TABLE_NAME;
                        if ($scope.importSelectedvalue.toUpperCase().includes("USER")) {
                            $('#importSelectionTable').hide();
                            $scope.attrMappingEnabled = true;
                        }
                        else {
                            $('#importSelectionTable').show();
                        }
                        $scope.importExcelSheetDDSelectionValue = $scope.importSelectedvalue;
                        $scope.btnNextImport(templateDetails);
                    }
                    else {
                        blockUI.stop();
                    }
                }).error(function (error) {
                    options.error(error);
                    blockUI.stop();
                });
            }
        }
    });

    $scope.importSheetType = "Families";
    $scope.importExcelSheet = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.importExcelSheetSelection($scope.excelPath).success(function (response) {
                    options.success(response);
                    if (response != null && response.length != 0) {
                        $scope.importSheetSelected = response[0].TABLE_NAME;
                        blockUI.stop();
                    }
                    else {
                        blockUI.stop();
                    }
                }).error(function (error) {
                    options.error(error);
                    blockUI.stop();
                });
            }
        }
    });

    $scope.importSheetType = "Products";
    $scope.importSheetChange = function (e) {
    };

    if ($scope.TemplateNameImport != undefined && $scope.ImportcatalogIdAttr != undefined && $scope.ImportFormat != undefined) {
        dataFactory.SaveTemplateDetails($scope.TemplateNameImport, $scope.ImportcatalogIdAttr, $scope.ImportFormat).success(function (response) {
            var result = response.split("_");
            if (result.length > 3) {
                $scope.PicklistValidation = result[3].toString().toUpperCase() == "FALSE" ? false : true;
                $scope.subcatalogItem = result[4] == "0" ? false : true;
                $scope.templateId = result.length > 4 ? result[5] : 0;
            }
            $scope.importGridOptions($scope.importSelectedvalue, $scope.excelPath);
            $scope.templateDetails = false;
            $("#importSelectionTable").css("margin-top", "0px");
            $scope.AttributeMappingdiv = true;
            $("#choosefileimp").removeClass("leftMenuactive");
        }).error(function (error) {
            options.error(error);
        });
    }

    $scope.SelectToImportClick = function (row, dd) {
        if ($('.select').length > 0) {
            angular.forEach($('.select').closest("tr"), function (data) {
                if (data.children[1].children[0].checked == false) {
                    data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                }
                else if (data.children[1].children[0].checked == true && data.children[3].children[0].children[0].children[0].children[1].value == "") {
                    data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header unselect";
                }
                else {
                    data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                }
            });
        }
        if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
            $scope.attrMap = true;
        }
        else {
            $scope.attrMap = false;
        }
        if ($('.selectattrList').length > 0) {
            angular.forEach($('.selectattrList').closest("tr"), function (data) {
                if (data.children[1].children[0].checked == false) {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                }
                else if (data.children[1].children[0].checked == true && data.children[2].children[0].children[0].children[1].value == "") {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header unselectattrList";
                }
                else {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                }
            });
        }
        if ($('.unselectattrList').length > 0 && ($('.unselectattrList option:selected').text() === "" || $('.unselectattrList option:selected').text() === "--- Select Attribute type ---")) {
            $scope.attrMap = true;
        }
        else if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
            $scope.attrMap = true;
        }
        else {
            $scope.attrMap = false;
        }
    }

    $scope.importGridOptions = function (importExcelSheetDDSelectionValue, excelPath) {
        if ($scope.templateId == undefined) {
            $scope.templateId = 0;
            blockUI.stop();
        };
        if ($scope.importSelectedvalue.toUpperCase().includes("USER") && $scope.userTypeCheck == true) {
            $scope.importTypeSelection = "User";
            $scope.importSheetType = "User";
        } else if ($scope.importSelectedvalue.toUpperCase().includes("ROLE") && $scope.userTypeCheck == true) {
            $scope.importTypeSelection = "Role";
            $scope.importSheetType = "Role";
        }
        else {
            $scope.importTypeSelection = "User";
            $scope.importSheetType = "User";
        }
        $scope.ImportType = $scope.importSheetType;
        if ($scope.selectedTemplate != "" && $scope.selectedTemplate != undefined && $scope.templateId == "0") {
            $scope.templateId = $scope.selectedTemplate;
        }
        dataFactory.GetImportSpecs(importExcelSheetDDSelectionValue, excelPath, $scope.templateId, $scope.ImportType).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.prodImportData = obj.Data;
            $scope.columnsForImportAtt = obj.Columns;
            $scope.attrMappingEnabled = false;
            angular.forEach($scope.prodImportData, function (data) {
                if (data.FieldType != "0" && data.ExcelColumn.toString().toUpperCase() != "ITEM#" && data.ExcelColumn.toString().toUpperCase() != "SUBITEM#") {
                    $scope.attrMappingEnabled = true;
                }
            });

            if ($('.unselect').length > 0) {
                angular.forEach($('.unselect').closest("tr"), function (data) {
                    if (data.children[1].children[0].checked == false) {
                        data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                    }
                    else if (data.children[1].children[0].checked == true && data.children[3].children[0].children[0].children[0].children[1].value == "") {
                        data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header unselect";
                    }
                    else {
                        data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                    }
                });
            }
            if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
                $scope.attrMap = true;
            }
            else {
                $scope.attrMap = false;
            }
            if ($('.selectattrList').length > 0) {
                angular.forEach($('.selectattrList').closest("tr"), function (data) {
                    if (data.children[1].children[0].checked == false) {
                        data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                    }
                    else if (data.children[1].children[0].checked == true && data.children[2].children[0].children[0].children[1].value == "") {
                        data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header unselectattr";
                    }
                    else {
                        data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                    }
                });
            }
            if ($('.unselectattrList').length > 0 && $('.unselectattrList option:selected').text() === "") {
                $scope.attrMap = true;
            }
            else if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
                $scope.attrMap = true;
            }
            else {
                $scope.attrMap = false;
            }
            if (obj.Data.length == 0) {
                $scope.attrMap = true;
            }
            if ($scope.attrMappingEnabled == false) {
                $scope.attrMap = false;
                blockUI.stop();
            }
        }).error(function (error) {
            options.error(error);
            blockUI.stop();
        });
        if ($scope.importSelectedvalue.toUpperCase().includes("USER")) {
            $('#importSelectionTable').hide();
            $scope.attrMappingEnabled = true;
        }
        else {
            $('#importSelectionTable').show();
        }
    };


    $scope.importTypeSelect = function () {
        if ($scope.ImportType.toUpperCase().includes('USER') && $scope.importSelectedvalue.toUpperCase().includes("USER") && $scope.userTypeCheck == true) {
            $scope.importTypeSelection = "User";
            $scope.importSheetType = "User";
        } else if ($scope.ImportType.toUpperCase().includes('ROLE') || $scope.importSelectedvalue.toUpperCase().includes("ROLE") && $scope.userTypeCheck == true) {
            $scope.importTypeSelection = "Role";
            $scope.importSheetType = "Role";
        }
        else {
            $scope.importTypeSelection = "User";
            $scope.importSheetType = "Role";
        }
        $scope.ImportType = $scope.importSheetType;
    };


    $("#chkselectall").change(function () {  //"select all" change 
        $(".chk").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
        $("#validateImport").attr("disabled", "disabled");
        $(".chk").each(function () {
            if ($(this).is(":checked") && $(this)[0].id != "chkselectall") {

                $("#validateImport").removeAttr("disabled");
            }
        });
    });

    $('.chk').change(function () {
        if (false == $(this).prop("checked")) { //if this item is unchecked
            $("#chkselectall").prop('checked', false); //change "select all" checked status to false
        }
        if ($('.chk:checked').length == $('.chk').length) {
            $("#chkselectall").prop('checked', true);
        }
        $("#validateImport").attr("disabled", "disabled");
        $(".chk").each(function () {
            if ($(this).prop("checked") && $(this)[0].id != "chkselectall") {
                $("#validateImport").removeAttr("disabled");
            }
        });
    });

    //$scope.Attrmapping = new kendo.data.DataSource({
    //    type: "json",
    //    serverFiltering: true,
    //    filter: "startswith",
    //    transport: {
    //        read: function (options) {
    //            //dataFactory.attributeimpmapping($scope.ImportType).success(function (response) {
    //            //    $scope.attributeList = response;
    //            //    options.success(response);
    //            //    $timeout(function () {
    //            //        blockUI.stop();
    //            //    }, 200)
    //            //}).error(function (error) {
    //            //    options.error(error);
    //            //});
    //        }
    //    },
    //    sort: {
    //        field: 'ATTRIBUTE_NAME',
    //        dir: 'asc'
    //    }
    //});

    $scope.importResultDatasource = new kendo.data.DataSource({
        type: "json", autobind: false,
        sort: { field: "ErrorMessage", dir: "asc" },
        transport: {
            read: function (options) {
                if ($scope.ItemImport == false) {
                    dataFactory.getFinishImportResults($scope.importSessionID).success(function (response) {
                        $('#myModal').hide();
                        $scope.progressComplete = true;
                        $("#completed").text("Import Failed");
                        $scope.showOkButton = false;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    dataFactory.GetItemImportErrorlist().success(function (errorList) {
                        $('#myModal').hide();
                        $scope.progressComplete = true;
                        $("#completed").text("Import Failed");

                        $scope.showOkButton = false;
                        options.success(errorList);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        }, schema: {
            model: {
                id: "ErrorMessage",
                fields: {
                    ErrorMessage: { type: "string" },
                    ErrorProcedure: { type: "string" },
                    ErrorSeverity: { type: "string" },
                    ErrorState: { type: "string" },
                    ErrorNumber: { type: "string" },
                    ErrorLine: { type: "string" }
                }
            }
        }
    });


    $scope.validatetResultDatasource = new kendo.data.DataSource({
        type: "json", autobind: false,
        transport: {
            read: function (options) {
                dataFactory.validateImportResults($scope.importSessionID, $scope.ImportType).success(function (response) {
                    options.success(response);
                    if (response != null) {
                        var obj = jQuery.parseJSON(response.Data.Data);
                        $scope.validateLogprod = obj;
                        $scope.validateLogcolumn = response.Data.Columns;
                        $scope.importErrorFileWindowdiv = true;
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.GetValidationResult = function () {
        dataFactory.validateImportResults($scope.importSessionID[$scope.importSessionID.length - 1], $scope.ImportType).success(function (response) {
            if (response != null) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.validateLogprod = obj;
                $scope.validateLogcolumn = response.Data.Columns;
                $scope.importErrorFileWindowdiv = true;
            }
        });
    };

    $scope.importResultGridOptionsvalidation = {
        dataSource: $scope.validatetResultDatasource,
        scrollable: true, resizable: true,
        autoBind: true,

    };

    $scope.importResultGridOptions = {
        dataSource: $scope.importResultDatasource,
        scrollable: true, resizable: true,
        autoBind: false,
        columns: [
            { field: "ErrorMessage", title: "Error Message", width: "200px" },
            { field: "ErrorProcedure", title: "Error Procedure", width: "200px" },
            { field: "ErrorSeverity", title: "Error Severity", width: "200px" },
            { field: "ErrorState", title: "Error State", width: "200px" },
            { field: "ErrorNumber", title: "Error Number", width: "200px" },
            { field: "ErrorLine", title: "Error Line" }
        ]
    };
    $scope.closePopup = function () {
        $scope.showOkButton = false;
        $('#myModal').hide();
        $("#importCompleteness").kendoProgressBar({
            type: "chunk",
            chunkCount: 10,
            min: 0,
            max: 10,
            value: 1
        }).data("kendoProgressBar");
        $("#completed").text("10 %");
    };
    $scope.importSuccessResultDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autobind: false,
        scrollable: true,
        sort: { field: "CATALOG_NAME", dir: "asc" },
        transport: {
            read: function (options) {

                dataFactory.getFinishImportSuccessResults($scope.importSessionID, $scope.ImportType).success(function (response) {
                    options.success(response);
                    var importProgress = $("#importCompleteness").kendoProgressBar({
                        type: "chunk",
                        chunkCount: 10,
                        min: 0,
                        max: 10,
                        value: 1
                    }).data("kendoProgressBar");
                    importProgress.value(10);
                    $scope.importCompletedStatus = false;
                    $("#completed").text("Import Successful");
                    $scope.showOkButton = true;
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                fields: {
                    CATALOG_NAME: { type: "string" },
                    CATEGORY_DETAILS: { type: "string" },
                    FAMILY_DETAILS: { type: "string" },
                    SUBFAMILY_DETAILS: { type: "string" },
                    PRODUCT_DETAILS: { type: "string" },
                    STATUS: { type: "string" }
                }
            }
        }

    });

    $scope.Next = function () {

        $scope.passvalidationNext = false;
        $scope.valimport = $rootScope.importtypeValue;
        $scope.valimport = $scope.valimport.toUpperCase();
        if ($scope.valimport == "USER") {
            $scope.ValidateDiv = false;
            $scope.ValidateUserImportDiv = true;
            $scope.ValidateRoleImportDiv = false;
            $scope.RoleValidateDiv = false;
            $scope.ResultDisplay = false;
            $scope.ResultUserDisplay = false;
            $scope.ResultRoleDisplay = false;
            $scope.showUserValidatediv = false;
            $scope.showRoleValidatediv = false;

        }
        else if ($scope.valimport == "ROLE") {
            $scope.ValidateDiv = false;
            $scope.ValidateRoleImportDiv = true;
            $scope.UserValidationBackdiv = false;
            $scope.ValidateUserImportDiv = false;
            $scope.RoleValidateDiv = false;
            $scope.ResultDisplay = false;
            $scope.ResultUserDisplay = false;
            $scope.ResultRoleDisplay = false;
            $scope.showUserValidatediv = false;
            $scope.showRoleValidatediv = false;
        }
        $scope.showValidatediv = false;
        $scope.showexpressiondiv = true;
        $scope.AttributeMappingdiv = false;
        $scope.ProcessName = "Validate Content"
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#impvalidation").addClass("leftMenuactive");
        $("#import").removeClass("leftMenuactive");
        blockUI.stop();
    };

    $scope.GoToSetup = function () {
        $scope.ImportType = $rootScope.importtypeValue;
        $scope.ProcessName = "View Log";
        $("#import").removeClass("leftMenuactive");
        $scope.BatchImport = false;
        $scope.ImportCompeted();
        $scope.ResetPage();
    };
    $scope.ImportCompeted = function () {
        $scope.validationPassed = false;
        $scope.ResultDisplay = false;
        $scope.showValidatediv = false;
        $scope.listAllSheet = [];
        $scope.templateDetails = false;
        $scope.ResultDisplay = false;
        $scope.AttributeMappingdiv = false;
        $scope.ProcessName = "Validate Content";
        $scope.Validateback();
    };
    $scope.progressComplete = false;
    $("#importCompleteness").kendoProgressBar({
        type: "chunk",
        chunkCount: 10,
        min: 0,
        max: 10,
        value: 1
    }).data("kendoProgressBar");
    $scope.GetImportStatus = function (percentage, rowCount) {

        $timeout(function () {
            if (percentage <= 6) {
                if ($scope.progressComplete != true) {
                    var importProgress = $("#importCompleteness").kendoProgressBar({
                        type: "chunk",
                        chunkCount: 10,
                        min: 0,
                        max: 10,
                        value: 0.5
                    }).data("kendoProgressBar");

                    importProgress.value((percentage));
                    $("#completed").text(((percentage) * 10) + "%");
                    $scope.GetImportStatus(percentage + 1, rowCount);
                }
            }
            else {
                if ($scope.progressComplete != true) {
                    var importProgress = $("#importCompleteness").kendoProgressBar({
                        type: "chunk",
                        chunkCount: 10,
                        min: 0,
                        max: 10,
                        value: 1
                    }).data("kendoProgressBar");

                    importProgress.value(6);
                    $("#completed").text("60%");
                }
                $timeout(function () {
                    if ($scope.progressComplete != true) {
                        var importProgress = $("#importCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");

                        importProgress.value(7);
                        $("#completed").text("70%");
                    }
                }, 5000);
                $timeout(function () {
                    if ($scope.progressComplete != true) {
                        var importProgress = $("#importCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");

                        importProgress.value(8);
                        $("#completed").text("80%");
                    }
                }, 10000);
                $timeout(function () {
                    if ($scope.progressComplete != true) {
                        var importProgress = $("#importCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");
                        importProgress.value(9);
                        $("#completed").text("90%");
                    }
                }, 15000);
            }
        }, 4000);
    };


    $scope.finishImport = function () {
        blockUI.stop();
        $scope.importCompletedStatus = true;
        $scope.progressComplete = false;
        $scope.scheduleImport = true;
        $scope.ItemImport = false;
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#createTemplate").removeClass("leftMenuactive");
        $("#imptamp").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#import").addClass("leftMenuactive");
        $('.imageloader').hide();
        var DateTime = new Date($('#txtDataTime').val());
        var hour = DateTime.getHours() == '0' ? '00' : DateTime.getHours().toString().length == 1 ? '0' + DateTime.getHours() : DateTime.getHours();
        var min = DateTime.getMinutes() == '0' ? '00' : DateTime.getMinutes().toString().length == 1 ? '0' + DateTime.getMinutes() : DateTime.getMinutes();
        var scheduleDate = DateTime.getDate() + "/" + (DateTime.getMonth() + 1) + "/" + DateTime.getFullYear() + " " + hour + ":" + min;
        if ($scope.EnableBatch == false) {
            scheduleDate = "";
        }
        $scope.impimp = true;
    };


    $scope.validatefile = function () {
        dataFactory.basicimportExcelSheetSelection($scope.allowDuplication, $scope.basicImportExcelSheetDDSelectionvalue, $scope.excelPath, $scope.basicprodImportData).success(function (response) {
            $scope.successREsultBasic = jQuery.parseJSON(response.Data);
            $scope.tblImportBasic.reload();
            $scope.tblImportBasic.$params.page = 1;
            $("#basicImportSuccessWindow").show();
        }).error(function (error) {
            options.error(error);
        });;
    };

    $scope.backexpression = function () {
        if ($scope.passvalidationNext == false) {
            $scope.ProcessName = "Map Attributes";
            $scope.passvalidationNext = true;
            ("#imptamp").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#imptamp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
        }
        else if ($scope.passvalidation == true) {
            $scope.templateDetails = true;
            $scope.AttributeMappingdiv = false;
            $scope.showValidatediv = false;
            $("#choosefileimp").addClass("leftMenuactive");
        }
    }
    $("#validationResultForAll").kendoGrid({
        autoBind: false,
        dataSource: $scope.validatetResultDatasource
    });


    $scope.ViewLog = function () {
        $scope.showValidatediv = false;
        $scope.importErrorFileWindowdiv = true;
        $scope.ResultDisplay = true;
        $scope.importSuccessWindowdiv = false;
        $scope.ProcessName = "View Log";
        $("#imptamp").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#createTemplate").removeClass("leftMenuactive");
        $("#logim").addClass("leftMenuactive");
        $scope.GetValidationResult();
    }

    $scope.getFinishImportFailedpicklistResults = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: false,
        scrollable: true,
        sort: { field: "STATUS", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportFailedpicklistResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {

                fields: {
                    STATUS: { type: "string" },
                    ITEM_NO: { type: "string" },
                    ATTRIBUTE_NAME: { type: "string" },
                    PICKLIST_VALUE: { type: "string" }
                }
            }
        }

    });

    $scope.NextToValidate = function () {
        $scope.validationPassed = false;
        $scope.skipValidateimport();
    };
    $scope.nextSheet = false;
    $scope.Validateback = function () {
        $scope.BatchImport = false;
        $scope.EnableBatch = false;
        $scope.ImportType = $rootScope.importtypeValue;
        if ($scope.ProcessName == "Validate Content" && $scope.ValidateDiv == true) {
            $scope.passvalidationimportenable = false;
            $scope.templateDetails = false;
            $scope.AttributeMappingdiv = true;
            $scope.passvalidationNext = true;
            $scope.validationPassed = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = false;
            $scope.showValidatediv = false;
            $scope.ProcessName = "Map Attributes";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            return;
        }
        else if ($scope.ProcessName == "Validate Content" && $scope.AttributeMappingdiv == false && $scope.ImportType == "User") {
            $scope.importSuccessWindow = false;
            $scope.importSuccessWindowdiv = false;
            $scope.ResultUserDisplay = false;
            $scope.passvalidationNext = true;
            $scope.roleerrorlog = false;;
            $scope.ResultRoleDisplay = false;
            $("#impvalidation").removeClass("leftMenuactive");
            $scope.ValidateUserImportDiv = false;
            $scope.ValidateRoleImportDiv = false;
            $scope.RoleValidateDiv = false;
            $scope.ResultDisplay = false;
            $scope.showUserValidatediv = false;
            $scope.showRoleValidatediv = false;
            $scope.UserValidationBackdiv = false;
            $scope.UservalidationSuccess = false;
        }
        else if ($scope.ProcessName == "Validate Content" && $scope.AttributeMappingdiv == false && $scope.ImportType == "Role") {
            $scope.roleerrorlog = false;
            $scope.importSuccessWindow = false;
            $scope.importSuccessWindowdiv = false;
            $scope.ResultRoleDisplay = false;
            $scope.passvalidationNext = true;
            $scope.importSuccessWindow = false;
            $scope.importSuccessWindowdiv = false;
            $scope.ResultUserDisplay = false;
            $scope.ValidateUserImportDiv = false;
            $scope.ValidateRoleImportDiv = false;
            $scope.RoleValidateDiv = false;
            $scope.ResultDisplay = false;
            $scope.showUserValidatediv = false;
            $scope.showRoleValidatediv = false;
            $("#impvalidation").removeClass("leftMenuactive");
        }
        else if ($scope.ProcessName == "Validate Content" && $scope.AttributeMappingdiv == true) {
            $scope.passvalidationimportenable = false;
            $scope.templateDetails = false;

            $scope.AttributeMappingdiv = true;
            $scope.passvalidationNext = true;
            $scope.validationPassed = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = false;
            $scope.showValidatediv = false;
            $scope.ProcessName = "Map Attributes";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            return;
        }
        else if ($scope.ProcessName == "Validate Content" && $scope.showValidatediv == true) {
            $scope.passvalidationimportenable = false;
            $scope.templateDetails = false;
            $scope.AttributeMappingdiv = false;
            $scope.passvalidationNext = false;
            $scope.validationPassed = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = true;
            $scope.showValidatediv = false;
            $scope.ProcessName = "Validate Content";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").addClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            return;
        }
        else if ($scope.ProcessName == "Import Content" && $scope.AttributeMappingdiv == false) {
            $scope.validationPassed = false;
            $scope.ResultDisplay = false;
            $scope.showValidatediv = false;
            $scope.ValidateDiv = true;
            $scope.templateDetails = false;
            $scope.ResultDisplay = false;
            $scope.AttributeMappingdiv = true;
            $scope.progressComplete = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = true;
            $scope.ResultDisplay = false;
            $scope.AttributeMappingdiv = false;
            $scope.ProcessName = "Validate Content";
        }
        else if ($scope.ProcessName == "View Log" && $scope.ResultDisplay == false && $scope.ImportType == "User") {
            if ($scope.importErrorFileWindowdiv == true) {
                $scope.importSuccessWindowdiv = false;
                $scope.roleerrorlog = false;
                $scope.importSuccessWindowdiv = false;
                $("#logim").removeClass("leftMenuactive");
                $scope.ResultUserDisplay = false;
                $scope.UserValidationBackdiv = true;
                $scope.UservalidationSuccess = false;
                $scope.ProcessName = "Import Content";
                $scope.showUserValidatediv = true;
                $scope.roleerrorlog = false;
                $scope.importSuccessWindowdiv = false;
                $scope.ResultRoleDisplay = false;
                $scope.RolevalidationSuccess = false;
                $("#import").addClass("leftMenuactive");
                $scope.ValidateDiv = false;
                $scope.ValidateUserImportDiv = false;
                $scope.ValidateRoleImportDiv = false;
                $scope.RoleValidateDiv = false;
                $scope.ResultDisplay = false;
                $scope.showRoleValidatediv = false;
            }
            else {
                $scope.roleerrorlog = false;
                $("#logim").removeClass("leftMenuactive");
                $scope.ResultUserDisplay = false;
                $scope.UserValidationBackdiv = true;
                $scope.UservalidationSuccess = true;
                $scope.ProcessName = "Import Content";
                $scope.showUserValidatediv = true;
                $scope.ResultRoleDisplay = false;
                $("#import").addClass("leftMenuactive");
                $scope.ValidateDiv = false;
                $scope.RoleValidateDiv = false;
                $scope.ResultDisplay = false;
                $scope.showRoleValidatediv = false;
                $scope.importSuccessWindow = false;
                $scope.importSuccessWindowdiv = false;
                $scope.passvalidationNext = true;
                $scope.ValidateUserImportDiv = false;
                $scope.ValidateRoleImportDiv = false;
            }

        }

        else if ($scope.ProcessName == "View Log" && $scope.ResultDisplay == false && $scope.ImportType == "Role") {
            if ($scope.importErrorFileWindowdiv == true) {
                $scope.roleerrorlog = false;
                $scope.importSuccessWindowdiv = false;
                $("#logim").removeClass("leftMenuactive");
                $scope.ResultRoleDisplay = false;
                $scope.RoleValidationBackdiv = true;
                $scope.RolevalidationSuccess = false;
                $scope.ProcessName = "Import Content";
                $scope.showRoleValidatediv = true;
                $scope.importSuccessWindowdiv = false;
                $scope.roleerrorlog = false;
                $scope.importSuccessWindowdiv = false;
                $scope.ResultUserDisplay = false;
                $scope.UservalidationSuccess = false;

                $("#import").addClass("leftMenuactive");
            }
            else {
                $scope.roleerrorlog = false;
                $scope.importSuccessWindowdiv = false;
                $("#logim").removeClass("leftMenuactive");
                $scope.ResultRoleDisplay = false;
                $scope.RoleValidationBackdiv = true;
                $scope.RolevalidationSuccess = true;
                $scope.ProcessName = "Import Content";
                $scope.showRoleValidatediv = true;

                $scope.roleerrorlog = false;
                $scope.importSuccessWindowdiv = false;
                $scope.ResultUserDisplay = false;

                $("#import").addClass("leftMenuactive");
            }

        }
        else if ($scope.ProcessName == "View Log" && $scope.ResultDisplay == true) {
            if ($scope.validationCompleted == true) {
                $scope.ValidateDiv = false;
                $scope.roleerrorlog = false;
                $scope.validationPassed = true;
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.validationSuccess = true;
                $scope.validationCompleted = true;
                $scope.ResultDisplay = false;
                $scope.AttributeMappingdiv = false;
                $scope.showValidatediv = true;
                $scope.importErrorFileWindowdiv = false;
                $scope.ProcessName = "Import Content";
                $("#import").addClass("leftMenuactive");
                $("#impvalidation").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");
                $("#createTemplate").removeClass("leftMenuactive");
                $("#logim").removeClass("leftMenuactive");
                return;
            }
            else if ($scope.validationSuccess == false) {
                $scope.roleerrorlog = false;
                $scope.ValidateDiv = false;
                $scope.validationPassed = true;
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.validationCompleted = false;
                $scope.ResultDisplay = false;
                $scope.AttributeMappingdiv = false;
                $scope.showValidatediv = true;
                $scope.importErrorFileWindowdiv = false;
                $scope.ProcessName = "Validate Content";
                $("#import").removeClass("leftMenuactive");
                $("#impvalidation").addClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");
                $("#createTemplate").removeClass("leftMenuactive");
                $("#logim").removeClass("leftMenuactive");
                return;
            }
            $scope.roleerrorlog = false;
            $scope.ProcessName = "Validate Content";
            $scope.progressComplete = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = true;
            $scope.ResultDisplay = false;
            $scope.AttributeMappingdiv = false;
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").addClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
        }
        else {
            $scope.roleerrorlog = false;
            $scope.templateDetails = true;
            $scope.AttributeMappingdiv = false;
            $scope.IsFormSubmitted = true;
            $scope.passvalidationNext = true;
            $scope.ValidateDiv = false;
            $scope.attrMap = false;
            $("#imptamp").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#choosefileimp").addClass("leftMenuactive");
            $scope.nextSheet = false;
            if ($scope.sheetCompleted == true) {
                $scope.ResetPage();
                $scope.sheetCompleted = false;
            }
            if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
                $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        }

    };

    $scope.Validateback1 = function () {
        if ($scope.BatchImport == true)
            $scope.BatchImport = false;
        else
            $scope.EnableBatch = false;
    }

    $scope.export_log = function () {
        dataFactory.validationresult($scope.Errorlogoutputformat, $rootScope.SelectedFileForUploadnamemain).success(function (response) {
            window.open("DownloadFile.ashx?Path=" + response + "&importType=" + $scope.ImportType);
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.DownLoadSourceFile = function () {
        window.open("DowloadImageManagementFiles.ashx?Path=" + $scope.importSessionID[$scope.importSessionID.length - 1] + "&Type=." + $scope.ImportFormat.toString().toLowerCase() + "&DownloadType=" + $scope.SelectedFileForUploadnamemain);
        return false;
    };

    $scope.sheetdownload = function () {
        window.open("DowloadImageManagementFiles.ashx?Path=" + $scope.excelPath + "&Type=.xls");
        return false;
    };


    $scope.sheetCompleted = false;
    $scope.nextSheetSelection = function () {
        $scope.nextSheet = true;
        $scope.importSuccessWindowdiv = false;
        $scope.ResultDisplay = false;
        $scope.AttributeMappingdiv = true;
        $scope.progressComplete = false;
        $scope.importCompletedStatus = true;
        var continueLoop = true;
        var excelSheets = $scope.importExcelSheetSelction._data;
        var nextValue = 0;
        if ($scope.nextSheetProcess == true && excelSheets.length >= 2 && $scope.listAllSheet.length <= excelSheets.length) {
            $scope.nextSheetProcess = false;
            angular.forEach(excelSheets, function (sheet) {
                if (continueLoop) {
                    if (nextValue == 0) {
                        nextValue = 1;
                    } else {
                        if (!$scope.listAllSheet.includes(sheet.TABLE_NAME)) {
                            $scope.importSelectedvalue = sheet.TABLE_NAME;
                            $scope.importSheetSelected = sheet.TABLE_NAME;
                            $scope.importExcelSheetDDSelectionValue = sheet.TABLE_NAME;
                            continueLoop = false;
                        }
                    }
                }
            });
        }
        $scope.importGridOptions($scope.importSelectedvalue, $scope.excelPath);
        $scope.ProcessName = "Validate Content"
        $scope.Validateback();
    };

    $scope.btnNextSheet = function () {
        $scope.templateDetails = false;
        $scope.AttributeMappingdiv = true;
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
    };

    $scope.btnBackSheet = function () {
        $scope.SelectedFileForUpload = null;
        $scope.TemplateNameImport = "";
        $scope.backbutton = false;
        var $el = $('#importfile1');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        var $el1 = $('#importfile2');
        $el1.wrap('<form>').closest('form').get(0).reset();
        $el1.unwrap();
        $rootScope.SelectedFileForUploadnamemain = "";
        var dropdownlistCatalog = $("#catalogDrpDownLisr").data("kendoDropDownList");
        $scope.ImportcatalogIdAttr = dropdownlistCatalog.element.val(0)[0][0].text;
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#choosefileimp").addClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $scope.templateFirst = true;
        $scope.templatesecond = false;
        $scope.templateDetails = true;
        $scope.AttributeMappingdiv = false;
        $scope.fileChoose = false;
        $scope.IsFormSubmitted = false;
        $("#drpTemplateDetails").data("kendoDropDownList").value(-1);
    };
    $scope.ViewBachLog = function () {
        $scope.ImportViewLogGridOptionsDatasource.read();
        $scope.winBatchLog.refresh({ url: "../views/app/partials/BatchLog.html" });
        $scope.winBatchLog.center().open();
    }



    $scope.PauseDeatils = function (data) {
        dataFactory.PauseImportProcess(data.dataItem.BatchId).success(function (response) {
            if (response == "Import is already running.Please wait for few minutes") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Import is already running.Please wait for few minutes.',
                    type: "info"
                });
            }
            else
                if (response == "Import Paused") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Import Paused.',
                        type: "info"
                    });
                }
            $scope.ImportViewLogGridOptionsDatasource.read();
        });
    }


    $scope.RunImportProcess = function (data) {
        dataFactory.RunImportProcess(data.dataItem.BatchId).success(function (response) {
            if (response == "Import is already running.Please wait for few minutes") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Import is already running.Please wait for few minutes.',
                    type: "info"
                });
            }
            $scope.ImportViewLogGridOptionsDatasource.read();
        });
    }

    $scope.EnableBachfun = function () {
        $scope.EnableBatch = true;
        $scope.scheduleTime = new Date();
        var currentdate = new Date();
        $('#txtDataTime').kendoDateTimePicker({
            value: $scope.scheduleTime,
            parseFormats: ['MM/dd/yyyy'],
            min: $scope.scheduleTime,
        });
    }
    $scope.EnableBachfun1 = function () {
        $scope.EnableBatch = true;
    }

    $scope.RefreshBatchLog = function (batchId) {
        $scope.ImportViewLogGridOptionsDatasource.read();
    };


    $scope.DownLoadErrorLog = function (batchId) {
        dataFactory.DownLoadErrorLog(batchId).success(function (response) {
            window.open("DownloadFile.ashx?Path=" + response);
        });
    }

    $scope.scheduleTime = new Date();

    $('#txtDataTime').kendoDateTimePicker({
        value: $scope.scheduleTime,

        parseFormats: ['MM/dd/yyyy'],
        min: $scope.scheduleTime,

    });


    // --------To get EnableSubProduct value from preference page-------
    $rootScope.getEnableSubProduct = function () {

        dataFactory.getEnableSubProduct().success(function (response) {
            $rootScope.EnableSubProduct = response;
            $localStorage.EnableSubProductLocal = response;
        });
    };
    var clearedBatchValues = [];

    $scope.ClearLogByid = function (data) {
        clearedBatchValues = [];
        clearedBatchValues.push(data.dataItem.BatchId);
        dataFactory.ClearLog(clearedBatchValues).success(function (response) {
            $scope.ImportViewLogGridOptionsDatasource.read();

            clearedBatchValues = [];
        })
    }

    $scope.ClearLog = function () {
        var getselectedValues = clearedBatchValues;

        if (clearedBatchValues.length != 0) {
            dataFactory.ClearLog(getselectedValues).success(function (response) {
                if (response == "Error") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Multiple delete having error.',
                        type: "info"
                    });
                }
                $scope.ImportViewLogGridOptionsDatasource.read();
                // $scope.GetBatchLogList();
                clearedBatchValues = [];
            })
        } else {
            return;
        }



    }


    $scope.updateSelection = function (e, id) {
        var batchId = id.dataItem.BatchId;
        var CurrentCheck = e.currentTarget.checked;
        if (CurrentCheck == true) {
            clearedBatchValues.push(batchId);
        }
        if (CurrentCheck == false) {
            for (var i = 0 ; clearedBatchValues.length > i ; i++) {
                if (batchId == clearedBatchValues[i]) {
                    clearedBatchValues.pop(clearedBatchValues[i]);
                }
            }
        }

    }

    $scope.ViewDeatils = function (data) {
        $rootScope.Batchviewdetails = data.dataItem;

        $scope.ImportViewLogGridOptionsDatasource.read();
        $scope.winBatchLogvalue.refresh({ url: "../views/app/partials/BatchViewDetails.html" });
        $scope.winBatchLogvalue.center().open();
    }

    $scope.selectAlllogSelection = function (data, e) {
        clearedBatchValues = [];
        var CurrentCheck = e.currentTarget.checked;
        if (CurrentCheck == true) {
            for (var i = 0 ; data.length > i ; i++) {
                if (data[i].Status == "UPDATE" || data[i].Status == "ERROR") {
                    clearedBatchValues.push(data[i].BatchId);
                }
            }
        }
        if (CurrentCheck == false) {
            clearedBatchValues = [];
        }
    }


    $scope.ImportViewLogGridOptions =
          {
              dataSource: $scope.ImportViewLogGridOptionsDatasource,
              sortable: true, scrollable: true, editable: false, pageable: { buttonCount: 5 }, autoBind: false,
              toolbar: [
   { name: "save", text: "", template: '<button id="refreshLog" class="btn btn-primary btn-xs" title="Refresh" ng-click="RefreshBatchLog()">Refresh</button>' },
   { name: "cancel", text: "", template: '<button id="clearLog" class="btn btn-primary btn-xs" style="float: right;margin-right: 0px;" title="Delete Multiple Records" ng-click="ClearLog()">Delete</button>' }
              ],
              columns: [
              { field: "CatalogName", title: "Catalog name", width: "100px", headerTemplate: '<span title="Catalog name">Catalog name</span>' },
              { field: "FileName", title: "File name", width: "100px", headerTemplate: '<span title="File name">File name</span>' },
              { field: "NoOfRecords", title: "No of records", width: "120px", headerTemplate: '<span title="No of records">No of records</span>' },
              { field: "ImportType", title: "Import type", width: "100px", headerTemplate: '<span title="Import type">Import type</span>' },
              { field: "CustomStatus", title: "Status", width: "100px", headerTemplate: '<span title="Status">Status</span>' },
              { field: "ElapsedTime", title: "Elapsed time", width: "100px", headerTemplate: '<span title="Elapsed time">Elapsed time</span>' },
              { field: "scheduleTime", title: "Schedule time", width: "100px", headerTemplate: '<span title="Schedule time">Schedule time</span>' },
              {
                  field: "CustomStatus",
                  title: "Action",
                  template: '<button #= (CustomStatus=="Completed" || CustomStatus=="Error") ? "" : "disabled=true" # ng-click="ViewDeatils(this)" title="View Log" class="btn btn-info cus"><i class="fa fa-eye"></i> </button>  &nbsp <button #= CustomStatus=="Pending" ? "" : "disabled=true" # ng-click="RunImportProcess(this)" title="Run"  class="btn btn-info cus"><i class="icon-play"></i> </button> &nbsp <button #= CustomStatus=="Inprogress" ? "disabled=true" : "" # ng-click="ClearLogByid(this)" title="Delete"  class="btn btn-info cus"><i class="fa fa-remove"></i> </button>',
                  width: "110px",
                  sortable: false,
                  headerTemplate: '<span title="Action">Action</span>',
                  filterable: false
              },
             {
                 field: "ISAvailable",
                 title: "Select",
                 sortable: false,

                 template: '<input type="checkbox"  #= CustomStatus=="Inprogress" ? checked="disabled" : !ISAvailable && CustomStatus!="Inprogress" ? checked="" : "checked" #  ng-click="updateSelection($event, this)"></input>',
                 headerTemplate: "<input type='checkbox' title='select all'  ng-click='selectAll($event,this)'/>",
                 width: "40px",
                 filterable: false
             },

              ],
              filterable: true,
              dataBound: function (e) {
              },

          };

    $scope.ImportViewLogGridOptionsDatasource = new kendo.data.DataSource({
        pageSize: 10,
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetBatchLogList().success(function (response) {

                    for (var i = 0 ; response.length > i ; i++) {
                        if (response[i]["ImportType"] == "ITEM#" || response[i].ImportType == "CATALOG_ITEM_NO") {
                            response[i]["ImportType"] = $localStorage.CatalogItemNumber;
                        }
                        if (response[i]["ImportType"] == "SUBITEM#" || response[i].ImportType == "SUBCATALOG_ITEM_NO") {
                            response[i]["ImportType"] = $localStorage.CatalogSubItemNumber;
                        }
                    }

                    options.success(response);

                    angular.forEach($('.k-i-filter'), function (value) {
                        value.className = "k-icon k-filter";
                    });
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        },
        schema: {
            model: {
                // id: "BATCHID",
                fields: {
                    // CatalogName: { editable: false, nullable: true },
                    ISAvailable: { editable: true, type: "boolean", sortable: false },

                    CatalogName: { editable: false, nullable: true },
                    FileName: { editable: false, nullable: true },
                    ImportType: { editable: false, nullable: true },
                    NoOfRecords: { editable: false, nullable: true },
                    FAMILY_NAME: { editable: false, nullable: true },
                    CustomStatus: { editable: false, nullable: true },
                    ElapsedTime: { editable: false, nullable: true },
                    scheduleTime: { editable: false, nullable: true },
                }
            }
        }
    });
    $scope.selectAll = function (ev, th) {

        var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
        var items = grid.dataItems();
        //Start
        var CurrentCheck = ev.currentTarget.checked;
        if (CurrentCheck == true) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].CustomStatus != "Inprogress") {
                    clearedBatchValues.push(items[i].BatchId);
                }
            }
            // clearedBatchValues.push(batchId);
        }
        if (CurrentCheck == false) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].CustomStatus != "Inprogress") {
                    clearedBatchValues.pop(items[i].BatchId);
                }
            }
        }
        //End
        items.forEach(function (item) {
            if (item.ISAvailable == false) {

                if (item.CustomStatus == "Inprogress") {
                    item.set("ISAvailable", false);
                } else {
                    {
                        item.set("ISAvailable", ev.target.checked);
                    }
                }
            }
            else if (item.ISAvailable == true) {
                if (item.CustomStatus == "Inprogress") {
                    item.set("ISAvailable", false);
                } else {
                    item.set("ISAvailable", ev.target.checked);
                }
            }
        });


    };

    $scope.init = function () {
        $scope.getEnableSubProduct();
    };
    $scope.init();

    $scope.ValidateUserImport = function () {
        $scope.importSheetType = "User";
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Proceed to validate import file.",
            type: "confirm",
            buttons: [{ value: "Ok" },
            { value: "Cancel" }],
            success: function (result) {
                if (result === "Ok") {
                    dataFactory.validateUserImport($scope.importExcelSheetDDSelectionValue, $scope.excelPath, $scope.ImportType, $scope.ImportFormat).success(function (validationuserresult) {
                        var valResult = validationuserresult.split("~");
                        if (valResult[0] == "true") {
                            $scope.ValidateUserImportDiv = false;
                            $scope.RoleValidateDiv = false;
                            $scope.showUserValidatediv = true;
                            $scope.UserValidationBackdiv = true;
                            $scope.UservalidationSuccess = true;
                            $scope.showRoleValidatediv = false;
                            $("#imptamp").removeClass("leftMenuactive");
                            $("#logim").removeClass("leftMenuactive");
                            $("#impvalidation").removeClass("leftMenuactive");
                            $("#impvalidation").removeClass("leftMenuactive");
                            $("#sheetselectimp").removeClass("leftMenuactive");
                            $("#imptamp").removeClass("leftMenuactive");
                            $("#import").addClass("leftMenuactive");
                            $("#importSelectionTable").css("margin-top", "50px");
                            $scope.ValidateDiv = false;
                            $scope.ValidateRoleImportDiv = false;
                            $scope.ResultDisplay = false;
                            $scope.ResultUserDisplay = false;
                            $scope.ResultRoleDisplay = false;

                            if (valResult[1] == 0) {
                                $scope.UserMisssingColumnItemerror = false;
                                $scope.UserMisssingColumnItem = true;

                            }
                            else {
                                $scope.UservalidationSuccess = false;
                                $scope.UserMisssingColumnItemerror = true;
                                $scope.UserMisssingColumnItem = false;
                            }

                            if (valResult[2] == 0) {

                                $scope.UserPicklistValidationerror = false;
                                $scope.UserPicklistValidation = true;
                            }
                            else {
                                $scope.UservalidationSuccess = false;
                                $scope.UserPicklistValidationerror = true;
                                $scope.UserPicklistValidation = false;
                            }

                            if (valResult[3] == 0) {
                                $scope.UserEmailValidationItem = true;
                                $scope.UserEmailValidationItemerror = false;
                            }
                            else {
                                $scope.UservalidationSuccess = false;
                                $scope.UserEmailValidationItem = false;
                                $scope.UserEmailValidationItemerror = true;
                            }

                            if (valResult[4] == 0) {
                                $scope.UserRoleNameItemerror = false;
                                $scope.UserRoleNameItem = true;
                            }
                            else {
                                $scope.UservalidationSuccess = false;
                                $scope.UserRoleNameItemerror = true;
                                $scope.UserRoleNameItem = false;
                            }

                            if (valResult[5] == 0) {
                                $scope.UserDuplicateItemerror = false;
                                $scope.UseDuplicateItemerror = true;
                            }
                            else {
                                $scope.UservalidationSuccess = false;
                                $scope.UserDuplicateItemerror = true;
                                $scope.UseDuplicateItemerror = false;
                            }

                        }
                        if (valResult[1] == 0 && valResult[2] == 0 && valResult[3] == 0 && valResult[4] == 0 && valResult[5] == 0) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Validation completed successfully.',
                                type: "info"
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Validation unsuccessful.',
                                type: "info"
                            });
                        }
                    })
                }
            }
        });
    }

    $scope.UserfinishImport = function () {
        dataFactory.userImport($scope.importExcelSheetDDSelectionValue, $scope.excelPath, $scope.ImportType, $scope.ImportFormat).success(function (importuserresult) {
            var importresult = [];
            importresult = importuserresult.split('~');
            if (importresult[0] == "true") {
                $scope.importLog.TimeTaken = importresult[3];
                $scope.importLog.InsertedRecords = importresult[1];
                $scope.importLog.UpdatedRecords = (importresult[2] - importresult[4]);
                $scope.importLog.SkippedRecords = importresult[4];
                $scope.importLog.DeletedRecords = importresult[5];
                $scope.importLog.CustomerName = importresult[6];
                $scope.showUserValidatediv = false;
                $scope.importErrorFileWindowdiv = false;
                $scope.showUserValidatediv = false;
                $scope.importSuccessWindowdiv = true;
                $scope.ResultUserDisplay = true;
                $scope.ProcessName = "View Log";
                $("#imptamp").removeClass("leftMenuactive");
                $("#impvalidation").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");
                $("#createTemplate").removeClass("leftMenuactive");
                $("#import").removeClass("leftMenuactive");
                $("#logim").addClass("leftMenuactive");

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'User Import successfully completed.',
                    type: "info"
                });


            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Import failed, please try again.',
                    type: "error"
                });
            }


        });
    }


    $scope.UserViewLog = function () {
        dataFactory.userViewLog().success(function (response) {

            $scope.showUserValidatediv = false;
            $scope.importErrorFileWindowdiv = true;
            $scope.importSuccessWindowdiv = false;
            $scope.showUserValidatediv = false;
            $scope.ResultUserDisplay = true;
            $scope.ProcessName = "View Log";
            $("#imptamp").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#createTemplate").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").addClass("leftMenuactive");
            if (response != null) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.validateLogprod = obj;
                $scope.validateLogcolumn = response.Data.Columns;
                $scope.importErrorFileWindowdiv = true;
            }
        });
    }

    $scope.ValidateUserback = function () {
        $scope.ValidateUserImportDiv = false;
        $scope.templateDetails = false;
        $("#importSelectionTable").css("margin-top", "0px");
        $scope.AttributeMappingdiv = false;
        $scope.passvalidationNext = true;
        $scope.templateDetails = true;
        $scope.AttributeMappingdiv = false;
        $scope.IsFormSubmitted = true;
        $scope.passvalidationNext = true;
        $scope.ValidateDiv = false;
        $scope.attrMap = false;
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#choosefileimp").addClass("leftMenuactive");

    }

    $scope.UserValidateback = function () {
        $scope.UserValidationBackdiv = false;
        $scope.ValidateUserImportDiv = true;
        $scope.showUserValidatediv = false;
        $scope.ProcessName = "Validate Content";
        $("#import").removeClass("leftMenuactive");
        $("#impvalidation").addClass("leftMenuactive");
    }

    $scope.UserEnableBachfun = function () {

        $scope.UserEnableBatch = true;
        $scope.UserValidationBackdiv = false;
        $scope.showUserValidatediv = true;
        $scope.scheduleTime = new Date();
        var currentdate = new Date();
        $('#txtDataTime').kendoDateTimePicker({
            value: $scope.scheduleTime,

            parseFormats: ['MM/dd/yyyy'],
            min: $scope.scheduleTime,

        });

    }

    $scope.UserValidateback1 = function () {
        $scope.UserValidationBackdiv = true;
        $scope.UserEnableBatch = false;
        $scope.UservalidationSuccess = true;
        $scope.ValidateDiv = false;
        $scope.ValidateUserImportDiv = false;
        $scope.ValidateRoleImportDiv = false;
        $scope.RoleValidateDiv = false;
        $scope.ResultDisplay = false;
        $scope.ResultUserDisplay = false;
        $scope.ResultRoleDisplay = false;
        $scope.showUserValidatediv = true;
        $scope.showRoleValidatediv = false;
    }

    //$scope.checkUser = function () {
    //    dataFactory.checkUser().success(function (importuserresult) {

    //        var userType = importuserresult;

    //        if (userType == "Admin User") {
    //            $scope.userTypeCheck = true;
    //        }
    //    });
    //}
    //$scope.checkUser();

    $scope.UserScheduleImport = function () {
        $scope.scheduleTime = new Date();
        var currentdate = new Date();
        $('#txtDataTime').kendoDateTimePicker({
            value: $scope.scheduleTime,

            parseFormats: ['MM/dd/yyyy'],
            min: $scope.scheduleTime,

        });
        var DateTime = new Date($('#txtDataTime').val());
        var hour = DateTime.getHours() == '0' ? '00' : DateTime.getHours().toString().length == 1 ? '0' + DateTime.getHours() : DateTime.getHours();
        var min = DateTime.getMinutes() == '0' ? '00' : DateTime.getMinutes().toString().length == 1 ? '0' + DateTime.getMinutes() : DateTime.getMinutes();
        var scheduleDate = DateTime.getDate() + "/" + (DateTime.getMonth() + 1) + "/" + DateTime.getFullYear() + " " + hour + ":" + min;

        dataFactory.userScheduleImport($scope.importExcelSheetDDSelectionValue, $scope.excelPath, $scope.ImportType, $scope.ImportFormat, $rootScope.SelectedFileForUploadnamemain, scheduleDate).success(function (userScheduleResult) {

            if (userScheduleResult == "true") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Added to queue.Please check the Batch log.',
                    type: "info"
                });
            }
            else if (userScheduleResult == "false") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Import may take some time to complete.",
                    type: "info"
                });
            }


        });
    }
    //Role import

    $scope.ValidateRoleback = function () {
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#choosefileimp").addClass("leftMenuactive");
        $scope.ValidateRoleImportDiv = false;
        $scope.passvalidationNext = true;
        $scope.RolevalidationSuccess = false;
        $scope.showRoleValidatediv = false;
        $scope.RoleValidationBackdiv = false;
        $scope.templateDetails = true;
        $scope.IsFormSubmitted = true;
        $scope.passvalidationNext = true;
        $scope.ValidateDiv = false;
        $scope.attrMap = false;

    }


    $scope.ValidateRoleImport = function () {
        $scope.importSheetType = "Role";
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Proceed to validate import file.",
            type: "confirm",
            buttons: [{ value: "Ok" },
            { value: "Cancel" }],
            success: function (result) {
                if (result === "Ok") {

                    dataFactory.validateRoleImport($scope.importExcelSheetDDSelectionValue, $scope.excelPath, $scope.ImportType, $scope.ImportFormat).success(function (validateRoleImportResult) {

                        if (validateRoleImportResult == "fail Column name validation") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Invalid import sheet column names, Please check the sheet.',
                                type: "error"
                            });
                            return;
                        }

                        var valResult = validateRoleImportResult.split("~");
                        $rootScope.tableCount = 0;
                        $scope.ValidateDiv = false;
                        $scope.RoleValidateDiv = false;
                        $scope.ValidateRoleImportDiv = false;
                        $scope.showUserValidatediv = false;
                        $scope.ValidateUserImportDiv = false;
                        $scope.showRoleValidatediv = true;
                        $scope.RoleValidationBackdiv = true;
                        $scope.RolevalidationSuccess = true;
                        $rootScope.tableCount = valResult[3];
                        $("#imptamp").removeClass("leftMenuactive");
                        $("#logim").removeClass("leftMenuactive");
                        $("#impvalidation").removeClass("leftMenuactive");
                        $("#choosefileimp").removeClass("leftMenuactive");
                        $("#sheetselectimp").removeClass("leftMenuactive");
                        $("#import").addClass("leftMenuactive");

                        if (valResult[1] != 0) {
                            $scope.RolevalidationSuccess = false;
                            $scope.RolePicklistValidationerror = true;
                            $scope.RolePicklistValidation = false;
                            $scope.RoleMisssingColumnItemerror = false;
                            $scope.RoleMisssingColumnItem = true;
                        }
                        else {
                            $scope.RolePicklistValidationerror = false;
                            $scope.RolePicklistValidation = true;
                            $scope.RoleMisssingColumnItemerror = false;
                            $scope.RoleMisssingColumnItem = true;
                        }
                        if (valResult[2] != 0) {
                            $scope.RolevalidationSuccess = false;
                            $scope.RoleMisssingColumnItemerror = true;
                            $scope.RoleMisssingColumnItem = false;
                        }
                        else {
                            $scope.RoleMisssingColumnItemerror = false;
                            $scope.RoleMisssingColumnItem = true;
                        }
                        if (valResult[3] != 0) {
                            $scope.RolevalidationSuccess = false;
                            $scope.RoleDuplicateColumnItemerror = true;
                            $scope.RoleDuplicateItem = false;
                        }
                        else {
                            $scope.RoleDuplicateColumnItemerror = false;
                            $scope.RoleDuplicateItem = true;
                        }
                        if (valResult[1] == 0 && valResult[2] == 0 && valResult[3] == 0) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Validation completed successfully.',
                                type: "info"
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Validation unsuccessful.',
                                type: "info"
                            });
                        }
                    });

                }
            }
            });
    }

    $scope.RoleValidateback = function () {
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").addClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#import").removeClass("leftMenuactive");
        $scope.showRoleValidatediv = false;
        $scope.RoleValidationBackdiv = false;
        $scope.RolevalidationSuccess = false;
        $scope.RoleValidationBackdiv = false;
        $scope.ValidateRoleImportDiv = true;
        $scope.RoleValidationBackdiv = true;
    }


    $scope.RolefinishImport = function () {
        dataFactory.rolefinishImport($scope.importExcelSheetDDSelectionValue, $scope.excelPath, $scope.ImportType, $scope.ImportFormat).success(function (roleImportResult) {
            if (roleImportResult == null) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Import failed, please try again.',
                    type: "error"
                });
                return;
            }


            var roleResult = roleImportResult.split("~");
            if (roleResult[0] == "true") {
                $scope.importLog.TimeTaken = roleResult[3];
                $scope.importLog.InsertedRecords = roleResult[1];
                $scope.importLog.UpdatedRecords = roleResult[2];
                $scope.importLog.CustomerName = roleResult[4];
                $scope.importLog.SkippedRecords = roleResult[5];
                $scope.RolevalidationSuccess = false;
                $scope.importErrorFileWindowdiv = false;
                $scope.RoleValidationBackdiv = false;
                $scope.showRoleValidatediv = false;
                $scope.importSuccessWindowdiv = true;
                $scope.ResultRoleDisplay = true;
                $scope.ProcessName = "View Log";
                $("#imptamp").removeClass("leftMenuactive");
                $("#impvalidation").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");
                $("#createTemplate").removeClass("leftMenuactive");
                $("#import").removeClass("leftMenuactive");
                $("#logim").addClass("leftMenuactive");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Role Import successfully completed.',
                    type: "info"
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Import failed, please try again.',
                    type: "error"
                });
            }
        });
    }

    $scope.RoleViewLog = function () {
        dataFactory.roleViewLog().success(function (response) {

            $scope.roleerrorlog = true;
            $scope.showRoleValidatediv = false;
            $scope.importErrorFileWindowdiv = true;
            $scope.importSuccessWindowdiv = false;
            $scope.RoleValidationBackdiv = false;
            $scope.showUserValidatediv = false;
            $scope.ResultRoleDisplay = true;
            $scope.ProcessName = "View Log";
            $("#imptamp").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#createTemplate").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").addClass("leftMenuactive");
            if ($rootScope.tableCount == 1) {
                $scope.roleerrorlog = false;
            }
            if (response != null) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.validateLogprod = obj;
                $scope.validateLogcolumn = response.Data.Columns;
                $scope.importErrorFileWindowdiv = true;
            }
        });
    }

    $scope.errorlogevent = function (e) {
        if (e == "Error Log1") {
            $scope.RoleViewChangeLog(0);
        }
        else if (e == "Error Log2") {
            $scope.RoleViewChangeLog(1);
        }
        else if (e == "Error Log3") {
            $scope.RoleViewChangeLog(2);
        }
    }

    $scope.RoleViewChangeLog = function (tableValues) {
        blockUI.start();
        dataFactory.changeRoleViewLog(tableValues).success(function (response) {
            $scope.roleerrorlog = true;
            $scope.showRoleValidatediv = false;
            $scope.importErrorFileWindowdiv = true;
            $scope.importSuccessWindowdiv = false;
            $scope.RoleValidationBackdiv = false;
            $scope.showUserValidatediv = false;
            $scope.ResultRoleDisplay = true;
            $scope.ProcessName = "View Log";
            $("#imptamp").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#createTemplate").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").addClass("leftMenuactive");
            if (response != null) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.validateLogprod = obj;
                $scope.validateLogcolumn = response.Data.Columns;
                $scope.importErrorFileWindowdiv = true;
                blockUI.stop();
                return;
            }
        });

    }

    $scope.SheetValidationImport = function () {
        $scope.importtype = $rootScope.valimporttype;
        $scope.importtype = $rootScope.importtypeValue;
        dataFactory.SheetValidation($scope.excelPath, $scope.importtype).success(function (sheetValidationResult) {

            if (sheetValidationResult == "1") {
                if ($scope.importtype != "User") {
                    $scope.passvalidationNext = true;
                    blockUI.stop();
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select the valid sheet.',
                        type: "info"
                    });
                    return;
                }
                else {
                    blockUI.stop();
                    return;
                }

            }
            else if (sheetValidationResult == "3") {
                if ($scope.importtype != "Role") {
                    blockUI.stop();
                    $scope.passvalidationNext = true;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select the valid sheet.',
                        type: "info"
                    });
                    return;
                }

            }
            else if (sheetValidationResult == "false") {
                blockUI.stop();
                $scope.passvalidationNext = true;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Import failed, please try again.',
                    type: "error"
                });
                return;
            }
            else if (sheetValidationResult == "invalid sheet name") {
                blockUI.stop();
                $scope.passvalidationNext = true;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please give vaild sheet name',
                    type: "error"
                });
                return;
            }
            else if (sheetValidationResult == "empty value") {
                blockUI.stop();
                $scope.passvalidationNext = true;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Sheet has empty values',
                    type: "error"
                });
                return;
            }
            else if (sheetValidationResult == "true") {
                blockUI.stop();
                return;
            }
            else {
                $scope.passvalidationNext = true;
                blockUI.stop();
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select the valid sheet.',
                    type: "info"
                });
                return;
            }
            blockUI.stop();
            return;
        });
    }

    $rootScope.$on("MyFunction", function () {
        $scope.SelectedFileForUpload = null;
        $scope.TemplateNameImport = "";
        $scope.backbutton = false;
        var $el = $('#UserFile');
        if ($el.val() != "") {
            $el.wrap('<form>').closest('form').get(0).reset();
        }
        var $el1 = $('#RoleFile1');
        if ($el1.val() != "") {
            $el1.wrap('<form>').closest('form').get(0).reset();
            $el1.unwrap();
        }
        $rootScope.SelectedFileForUploadnamemain = "";
        $scope.SelectedFileForUploadnamemainsam = "";
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#choosefileimp").addClass("leftMenuactive");
        $scope.templateDetails = true;
        $scope.AttributeMappingdiv = false;
        $scope.fileChoose = false;
        $scope.showValidatediv = false;
        $scope.templateNamedisabled = false;
        $scope.ValidateUserImportDiv = false;
        $scope.ValidateRoleImportDiv = false;
        $scope.RoleValidateDiv = false;
        $scope.ResultDisplay = false;
        $scope.ResultUserDisplay = false;
        $scope.ResultRoleDisplay = false;
        $scope.showUserValidatediv = false;
        $scope.showRoleValidatediv = false;
        $scope.ImportcatalogIdAttr = "";
        if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
            $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);

        //$scope.ResetImportPage();
        //$scope.ResetPage();
    });

    $scope.ResetImportPage = function () {
        $scope.SelectedFileForUpload = null;
        $scope.TemplateNameImport = "";
        $scope.backbutton = false;
        var $el = $('#importfile1');
        if ($el.val() != "") {
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
        }
        var $el1 = $('#importfile2');
        if ($el1.val() != "") {
            $el1.wrap('<form>').closest('form').get(0).reset();
            $el1.unwrap();
        }
        $rootScope.SelectedFileForUploadnamemain = "";
        $scope.SelectedFileForUploadnamemainsam = "";
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#choosefileimp").addClass("leftMenuactive");
        $scope.templateDetails = true;
        $scope.AttributeMappingdiv = false;
        $scope.fileChoose = false;
        $scope.showValidatediv = false;
        $scope.templateNamedisabled = false;
        $scope.ValidateUserImportDiv = false;
        $scope.ValidateRoleImportDiv = false;
        $scope.RoleValidateDiv = false;
        $scope.ResultDisplay = false;
        $scope.ResultUserDisplay = false;
        $scope.ResultRoleDisplay = false;
        $scope.showUserValidatediv = false;
        $scope.showRoleValidatediv = false;
        $scope.ImportcatalogIdAttr = "";
        if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
            $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);

    }

    $scope.BackImport = function (importtype) {
        $scope.ResetPage();
        importtype = importtype.toUpperCase();
        if (importtype == "USER") {
            $("#showUserImportdiv").hide();
            $("#showRoleImportdiv").hide();
            $("#userListViewGrid").show();
        }
        else if (importtype == "ROLE") {
            $("#roleListView").show();
            $("#grid").show();
            $("#showRoleImportdiv").hide();
            $("#showUserImportdiv").hide();
            $scope.RoleImportdiv = false;
            $scope.showImportRolediv = false;
        }

    }

    $scope.BackImport1 = function () {
        var importtype = "Role";
        $scope.BackImport(importtype);
        $scope.ResetPage();
    }

    $scope.ResetPage1 = function () {
        $scope.ResetPage();
    }

    $scope.SaveFile1 = function (templateDetails) {
        var importtype = "Role";
        $scope.SaveFile(templateDetails, importtype);
    }
    //Role import
    return $scope.UploadFile;

}]);
