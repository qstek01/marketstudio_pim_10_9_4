﻿LSApp.controller('PDFIndexController', ['$scope', 'dataFactory', '$http', '$compile', '$rootScope', '$timeout', 'ngTableParams', 'blockUI', '$localStorage', '$location', '$q', function ($scope, dataFactory, $http, $compile, $rootScope, $timeout, ngTableParams, blockUI, $localStorage, $location, $q) {

    $scope.selectSheet = false;
    $scope.missingValidation = false;
    $scope.Missing = false;
    $scope.Duplicate = false;
    $scope.newColumn = false;
    $scope.validationPassed = false;
    $scope.ImportExempteValidation = false;
    $scope.hideKeyword = true;
    $scope.importEnable = false;
    $scope.validateresult = true;
    $scope.validatendexpdfresult = true;
    $scope.selectedFilesToMerge = [];  //for multiple PDF merge functionality
    $scope.deletePdfTempEnable = true;  //for PDF template delete functionality
    $("#keywordImportErrorWindow").hide();
    $('#keywordImportSuccessWindow').hide();
    $('#exemptImportSuccessWindow').hide();
    $('#exemptImportErrorWindow').hide();
    $scope.radioCheck = "top";
    //------------------------CONTENT LIBRARY---------------------------------------------//
    //alert("success");
    $scope.Importvalidation = false;
    $scope.addKeywordWord = function (keyword) {

        if (keyword.trim().length == 1) {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Single Letter is not allowed.",
                type: "error"
            });

            $scope.txt_keyword = '';

            return;
        }




        if ($scope.SelectedContentTemplate != "") {
            dataFactory.addKeyword(keyword, $scope.SelectedContentTemplate).success(function (saveKeyResponse) {
                if (saveKeyResponse != null) {
                    $scope.txt_keyword = "";
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: saveKeyResponse,
                        type: "error"
                    });
                    $scope.getKeywordsList($scope.SelectedContentTemplate);
                }
            });
        }
    }

    $scope.getKeywordsList = function (groupType) {
        $scope.groupTypes = groupType;
        dataFactory.getKeywordList(groupType).success(function (listResponse) {
            if (listResponse != null) {
                $scope.keywordsList = listResponse;
                $scope.keywordListSelected = $scope.keywordsList;
            }
        });
    }

    var searchOptions = [
                        { text: "Contains", value: "1" },
                        { text: "Startswith", value: "2" },
                        { text: "Endswith", value: "3" },
    ];
    $scope.searchOptionDatasource = new kendo.data.ObservableArray(searchOptions);

    //------------------------CONTENT LIBRARY---------------------------------------------//
    $scope.contentGridData = [];
    $scope.getContentData = function (templateName) {
        dataFactory.getContentData(templateName).success(function (response) {
            if (response != null) {
                $scope.contentGridData = response;
            }
        });
    }

    $scope.contentTemplateDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getContentTemplateDetails().success(function (responseContent) {
                    $scope.selectedKeywords = [];
                    options.success(responseContent);
                });
            }
        }
    });

    $scope.PDFContentRead = function () {
        if ($scope.SelectedContentTemplate == "") {
            if ($scope.txt_ContentTemplate != undefined && $scope.txt_ContentTemplate.trim() != "" && $scope.txt_ContentTemplate != null) {
                dataFactory.saveTemplateDetail($scope.txt_ContentTemplate).success(function (response) {
                    if (response != null) {
                        $scope.contentTemplateDatasource.read();
                        $scope.deletePdfTempEnable = false;
                        $scope.sourceDataRead($scope.txt_ContentTemplate);
                    }
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a PDF file.',
                });
            }
        }
        else {
            //$scope.sourceDataRead($scope.SelectedContentTemplate);
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select "New" to create new template.',
            });
        }
    }

    $scope.sourceDataRead = function (templateName) {

        var path = $scope.excelPath;
        if (path != "" && path != undefined && path != null) {
            dataFactory.sourceContentRead(templateName, path).success(function (response) {
                if (response != null) {
                    if (response == "success") {
                        $scope.SelectedContentTemplate = templateName;
                        $scope.txt_ContentTemplate = "";
                        $rootScope.SelectedFileForUploadnamemain = "";
                        $scope.getContentData(templateName);
                        $scope.getKeywordsList(templateName);
                        $scope.init();
                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Cannot read unsupported PDF file.',
                            type: "error"
                        });
                    }
                }
            });
        }
    };

    $scope.contentTemplateChange = function (e) {
        if (e.sender.value() != "") {
            $scope.deletePdfTempEnable = false;
            $scope.templateName = e.sender.value();
            $scope.getContentData($scope.templateName);
            $scope.getKeywordsList($scope.templateName);
            $scope.init();
            $scope.Cancelfile();
        }
        else {
            $scope.keywordsList = [];
            $scope.deletePdfTempEnable = true;
            $scope.txt_ContentTemplate = "";
            $rootScope.SelectedFileForUploadnamemain = "";
            $scope.Cancelfile();
        }
    }

    $scope.clearData = function () {
        $scope.txt_keyword = "";
    }
    //file selection process//
    $scope.selectFileforUpload = function (file) {
        this.value = null;
        $scope.SelectedFileForUpload = file[0];

        var templateName = "";
        $scope.SelectedFileForUploadnamemainproduct = file[0].name;
        $rootScope.SelectedFileForUploadnamemain = file[0].name;
        var templateArray = $rootScope.SelectedFileForUploadnamemain.split('.');
        templateName = templateArray[0];
        $scope.txt_ContentTemplate = templateName;
        if ($rootScope.SelectedFileForUploadnamemain.length <= 40) {
            $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain;
        }
        else {
            $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain.substring(0, 40) + "." + $rootScope.SelectedFileForUploadnamemain.split('.')[1];
        }
        $scope.IsFormSubmitted = true;
        $scope.fileChoose = true;
        var fileExtension = $scope.SelectedFileForUpload.name.split('.')[1];
        $scope.ImportFormat = fileExtension;
        $scope.UploadFile($scope.SelectedFileForUpload);
    };

    //delete selected files//
    $scope.Cancelfile = function () {
        $scope.SelectedFileForUpload = null;
        $scope.TemplateNameImport = "";
        $scope.txt_ContentTemplate = "";
        $scope.backbutton = false;
        $scope.enableImportClick = false;
        $rootScope.SelectedFileForUploadnamemain = "";
        $scope.SelectedFileForUploadnamemainsam = "";
        if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
            $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        //  var $el = $('#importfileContentLibrary');
        //   $el.wrap('<form>').closest('form').get(0).reset();
        //   $el.unwrap();
    };

    $scope.UploadFile = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        $scope.file = file;
        //We can send more data to server using append         
        $rootScope.SelectedFileForUploadnamemain = file.name;
        $http.post("/Import/SaveFilesFullImport?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                //$scope.file = d.split(',')[0];
                $scope.excelPath = d.split(',')[0];
                if (d.split(',')[1].toUpperCase() == "TRUE") {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Invalid sheet.',
                        type: "error"
                    });
                    if ($scope.ImportcatalogIdAttr != undefined && $scope.ImportcatalogIdAttr != '')
                        $scope.IsFormSubmitted = false;
                    else
                        $scope.IsFormSubmitted = false;
                    $scope.Message = "";
                    var $el = $('#importfile1');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    $rootScope.SelectedFileForUploadnamemain = "";
                    blockUI.stop();
                    return;
                }
                else {
                    if ($scope.ImportFormat.toString().toUpperCase().includes('PDF')) {
                        $scope.excelPath = d.split(',')[0];
                        $scope.selectedFilesToMerge.push({ FILE_NAME: $rootScope.SelectedFileForUploadnamemain, FILE_PATH: $scope.excelPath });
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'File upload completed successfully.',
                            type: "error"
                        });
                    }
                }

            }).error(function () {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
            });
        return defer.promise;
    };
    $scope.alphabetData = [
        { text: "All", value: "All" },
                    { text: "A", value: "A" },
                    { text: "B", value: "B" },
                    { text: "C", value: "C" },
                    { text: "D", value: "D" },
                    { text: "E", value: "E" },
                    { text: "F", value: "F" },
                    { text: "G", value: "G" },
                    { text: "H", value: "H" },
                    { text: "I", value: "I" },
                    { text: "J", value: "J" },
                    { text: "K", value: "K" },
                    { text: "L", value: "L" },
                    { text: "M", value: "M" },
                    { text: "N", value: "N" },
                    { text: "O", value: "O" },
                    { text: "P", value: "P" },
                    { text: "Q", value: "Q" },
                    { text: "R", value: "R" },
                    { text: "S", value: "S" },
                    { text: "T", value: "T" },
                    { text: "U", value: "U" },
                    { text: "V", value: "V" },
                    { text: "W", value: "W" },
                    { text: "X", value: "X" },
                    { text: "Y", value: "Y" },
                    { text: "Z", value: "Z" }
    ];
    //$scope.alphebetDataSource = new kendo.data.ObservableArray(alphabetData);
    $scope.init = function () {
        $timeout(function () {
            $('[id^=content_]').removeClass("activemenu1");
            $("#content_All").addClass("activemenu1");
            $('[id^=exempted_]').removeClass("activemenu1");
            $("#exempted_All").addClass("activemenu1");
            $("#searchOption").data("kendoDropDownList").select(0);
            $scope.selecttedSearchOption = "1";
            //$scope.selectedSheetOption = "1";
            $scope.selecttedSearchOptionExempted = "1";
            $scope.txt_keyword = "";
            $scope.keywordSearch = "";
            $scope.txt_ContentTemplate = "";
        }, 200);
    }

    $scope.init();

    $scope.keywordSectionChange = function (e) {
        if (e != "All") {
            $scope.keywordListSelected = [];
            var selectedKey = e.toUpperCase();
            $('[id^=content_]').removeClass("activemenu1");
            $("#content_" + e).addClass("activemenu1");
            angular.forEach($scope.keywordsList, function (value) {
                if (value.KEYWORD_NAME.toUpperCase().startsWith(selectedKey))
                    $scope.keywordListSelected.push(value);
            });
        }
        else {
            $scope.keywordListSelected = $scope.keywordsList;
            $('[id^=content_]').removeClass("activemenu1");
            $("#content_All").addClass("activemenu1");
        }
    }

    $scope.filterFunc = function () {
        //$scope.keywordListSelected = $filter('filter')($scope.keywordsList, { $: $scope.keywordSearch });
        if ($scope.keywordSearch == undefined || $scope.keywordSearch == '') {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Please Enter a Search Value.",
                type: "info"
            });
        }
        if ($scope.selecttedSearchOption != "") {
            var selectedKey = $scope.keywordSearch.toUpperCase();
            var filterList = [];
            //$('[id^=content_]').removeClass("activemenu1");
            //$("#content_All").addClass("activemenu1");
            if ($scope.selecttedSearchOption == "1") {
                angular.forEach($scope.keywordsList, function (value) {
                    if (value.KEYWORD_NAME.toUpperCase().includes(selectedKey))
                        filterList.push(value);
                });
            }
            else if ($scope.selecttedSearchOption == "2") {
                angular.forEach($scope.keywordsList, function (value) {
                    if (value.KEYWORD_NAME.toUpperCase().startsWith(selectedKey))
                        filterList.push(value);
                });
            }
            else if ($scope.selecttedSearchOption == "3") {
                angular.forEach($scope.keywordsList, function (value) {
                    if (value.KEYWORD_NAME.toUpperCase().endsWith(selectedKey))
                        filterList.push(value);
                });
            }
            $scope.keywordListSelected = filterList;
        }
        else {
            $scope.keywordListSelected = $scope.keywordsList;
        }
    }

    $scope.filterFuncKey  = function () {
        //$scope.keywordListSelected = $filter('filter')($scope.keywordsList, { $: $scope.keywordSearch });
        if (event.which === 13 && ($scope.keywordSearch == "" || $scope.keywordSearch == undefined))
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Please Enter a Search Value.",
                type: "info"
            });

        if ($scope.selecttedSearchOption != "") {
            var selectedKey = $scope.keywordSearch.toUpperCase();
            var filterList = [];
            //$('[id^=content_]').removeClass("activemenu1");
            //$("#content_All").addClass("activemenu1");
            if ($scope.selecttedSearchOption == "1") {
                angular.forEach($scope.keywordsList, function (value) {
                    if (value.KEYWORD_NAME.toUpperCase().includes(selectedKey))
                        filterList.push(value);
                });
            }
            else if ($scope.selecttedSearchOption == "2") {
                angular.forEach($scope.keywordsList, function (value) {
                    if (value.KEYWORD_NAME.toUpperCase().startsWith(selectedKey))
                        filterList.push(value);
                });
            }
            else if ($scope.selecttedSearchOption == "3") {
                angular.forEach($scope.keywordsList, function (value) {
                    if (value.KEYWORD_NAME.toUpperCase().endsWith(selectedKey))
                        filterList.push(value);
                });
            }
            $scope.keywordListSelected = filterList;
        }
        else {
            $scope.keywordListSelected = $scope.keywordsList;
        }
    }

    $scope.checkContent = function (data) {
        if (data == 'PDF') {
            if ($scope.keywordSearch.length === 0) {
                $scope.getKeywordsList($scope.groupTypes)
            }
        }
        else if (data == 'Exempt') {
            if ($scope.exempkeywordSearch.length == 0) {
                $scope.getExemptedwordsList();
            }
        }
    }

    $scope.backCntntLibrary = function () {
        $scope.Importvalidation = false;
        $scope.getContentData($scope.templateName);
        $scope.getKeywordsList($scope.templateName);
        $scope.init();
        $scope.Cancelfile();
    }

    $scope.deleteIndexPDFClick = function () {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Do you want to delete the Template details?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }

                if (bool == true) {
                    dataFactory.deletePDFIndexValues($scope.templateName).success(function (tempDeleteRes) {
                        if (tempDeleteRes != null) {
                            $scope.contentTemplateDatasource.read();
                            $scope.keywordsList = [];
                            $scope.deletePdfTempEnable = true;
                            $scope.txt_ContentTemplate = "";
                            $scope.SelectedContentTemplate = "";
                            $rootScope.SelectedFileForUploadnamemain = "";
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Template details deleted successfully.",
                                type: "error"
                            });
                        }
                    });
                }
            }
        });
    }

    //------------------------CONTENT LIBRARY---------------------------------------------//

    $scope.addExemptedWord = function (word) {


        if (word.trim().length == 1) {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Single Letter is not allowed.",
                type: "error"
            });

            $scope.txt_exemptword = '';

            return;
        }



        dataFactory.addExemptedword(word).success(function (saveResponse) {
            if (saveResponse != null) {
                $scope.txt_exemptword = "";
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: saveResponse,
                    type: "error"
                });
                $scope.getExemptedwordsList();
            }
        });
    }

    $scope.getExemptedwordsList = function () {
        dataFactory.getExemptedwordsList().success(function (exemplistResponse) {
            if (exemplistResponse != null) {
                $scope.exemptedwordsList = exemplistResponse;
                $scope.exemptedListSelected = $scope.exemptedwordsList;
            }
        });
    }

    $scope.getExemptedwordsList();

    $scope.exemptedDeleteClick = function (data) {
        dataFactory.exemptedDelete(data.ID).success(function (deleteResponse) {
            if (deleteResponse != null) {
                $scope.getExemptedwordsList();
            }
        });
    };

    $scope.keywordSectionChangeExempted = function (e) {
        if (e != "All") {
            $scope.exemptedListSelected = [];
            var selectedKey = e.toUpperCase();
            $('[id^=exempted_]').removeClass("activemenu1");
            $("#exempted_" + e).addClass("activemenu1");
            angular.forEach($scope.exemptedwordsList, function (value) {
                if (value.WORD.toUpperCase().startsWith(selectedKey))
                    $scope.exemptedListSelected.push(value);
            });
        }
        else {
            $scope.exemptedListSelected = $scope.exemptedwordsList;
            $('[id^=exempted_]').removeClass("activemenu1");
            $("#exempted_All").addClass("activemenu1");
        }
    }

    $scope.filterFuncExempted = function () {
        if ($scope.exempkeywordSearch == undefined || $scope.exempkeywordSearch == '') {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Please Enter a Search Value.",
                type: "info"
            });
        }
        if ($scope.selecttedSearchOptionExempted != "") {
            var selectedKey = $scope.exempkeywordSearch.toUpperCase();
            var filterListExemp = [];
            if ($scope.selecttedSearchOptionExempted == "1") {
                angular.forEach($scope.exemptedwordsList, function (value) {
                    if (value.WORD.toUpperCase().includes(selectedKey))
                        filterListExemp.push(value);
                });
            }
            else if ($scope.selecttedSearchOptionExempted == "2") {
                angular.forEach($scope.exemptedwordsList, function (value) {
                    if (value.WORD.toUpperCase().startsWith(selectedKey))
                        filterListExemp.push(value);
                });
            }
            else if ($scope.selecttedSearchOptionExempted == "3") {
                angular.forEach($scope.exemptedwordsList, function (value) {
                    if (value.WORD.toUpperCase().endsWith(selectedKey))
                        filterListExemp.push(value);
                });
            }
            $scope.exemptedListSelected = filterListExemp;
        }
        else {
            $scope.exemptedListSelected = $scope.exemptedwordsList;
        }
    }


    $scope.filterFuncExemptedKey = function () {

        if (event.which === 13 && ($scope.exempkeywordSearch == "" || $scope.exempkeywordSearch == undefined)) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Please Enter a Search Value.",
                type: "info"
            });
        }

        if ($scope.selecttedSearchOptionExempted != "") {
            var selectedKey = $scope.exempkeywordSearch.toUpperCase();
            var filterListExemp = [];
            if ($scope.selecttedSearchOptionExempted == "1") {
                angular.forEach($scope.exemptedwordsList, function (value) {
                    if (value.WORD.toUpperCase().includes(selectedKey))
                        filterListExemp.push(value);
                });
            }
            else if ($scope.selecttedSearchOptionExempted == "2") {
                angular.forEach($scope.exemptedwordsList, function (value) {
                    if (value.WORD.toUpperCase().startsWith(selectedKey))
                        filterListExemp.push(value);
                });
            }
            else if ($scope.selecttedSearchOptionExempted == "3") {
                angular.forEach($scope.exemptedwordsList, function (value) {
                    if (value.WORD.toUpperCase().endsWith(selectedKey))
                        filterListExemp.push(value);
                });
            }
            $scope.exemptedListSelected = filterListExemp;
        }
        else {
            $scope.exemptedListSelected = $scope.exemptedwordsList;
        }
    }


    $scope.contentViewClick = function (data) {
        $scope.contentSearchWord = "";
        $scope.contentDataVal = data;
        $("#mediumModal1").removeClass("popupclose");
        $("#mediumModal1").addClass("popupActive");

    };

    $scope.contentSearchClick = function (searchKeyword, data) {
        //data.replace('<span class="isolate">', '');
        data = data.replace(new RegExp('<span class="isolate">', 'gi'), '');

        //data.replace('</span>', '');
        data = data.replace(new RegExp('</span>', 'gi'), '');
        var wordWithSmile = '<span class="isolate">' + searchKeyword + '</span>';
        //var toBereplaced = searchKeyword;
        var newVal = data.replace(new RegExp(searchKeyword, 'gi'), wordWithSmile);
        //var newVal = data.replace(toBereplaced, wordWithSmile);
        $scope.contentDataVal = newVal;
    }

    $scope.popupCancelClick = function () {
        $("#mediumModal1").removeClass("popupActive");
        $("#mediumModal1").addClass("popupclose");
    };

    $scope.keywordDeleteClick = function (data) {
        dataFactory.keywordDeleteClick(data.ID).success(function (deleteKeywordresponse) {
            if (deleteKeywordresponse != null) {
                $scope.getKeywordsList($scope.SelectedContentTemplate);
            }
        });
    }

    $scope.refreshKeyword = function () {
        $scope.keywordListSelected = $scope.keywordsList;
        $('[id^=content_]').removeClass("activemenu1");
        $("#content_All").addClass("activemenu1");
        $scope.keywordSearch = "";
        $scope.txt_keyword = "";
        $scope.init();
    }

    $scope.refreshExemptedword = function () {
        $scope.exemptedListSelected = $scope.exemptedwordsList;
        $('[id^=exempted_]').removeClass("activemenu1");
        $("#exempted_All").addClass("activemenu1");
        $scope.exempkeywordSearch = "";
        $scope.txt_exemptword = "";
        $scope.init();
    }
    var modifiedList = [];
    $scope.saveKeywordsList = function () {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure to save all keywords?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    var selectedList = $scope.keywordsList[0].KEYWORD_LIST;
                    var oldList = $scope.keywordsList;
                    angular.forEach(oldList, function (value) {
                        if (document.getElementById("" + value.ID) != null) {
                            var word = document.getElementById("" + value.ID).innerHTML.toString();
                            value.KEYWORD_NAME = word.replace('&nbsp;', '');
                        }
                        modifiedList.push(value);
                    });
                    if (modifiedList.length > 0) {
                        dataFactory.updateKeywords(modifiedList).success(function (updateResponse) {
                            if (updateResponse != null) {
                                modifiedList = [];
                                $scope.getKeywordsList(selectedList);
                                $scope.init();

                            };
                        });
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Keywords Saved Successfully."
                        });
                    };
                }
            }
        });
    }

    //------------------------INDEX GENERATION---------------------------------------------//
    $("#mergePart").hide();
    $scope.indexGridSource = [];
    $scope.indexGenDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getIndexGenTemplateDetails().success(function (responseIndexGenContent) {
                    options.success(responseIndexGenContent);
                });
            }
        }
    });

    var layoutData = [
                        { text: "Layout 1", value: "1" },
                        { text: "Layout 2", value: "2" },
                        { text: "Layout 3", value: "3" },
    ];
    $scope.layoutDatasource = new kendo.data.ObservableArray(layoutData);

    $scope.cancelIndexTemplateDetails = function () {
        $scope.SelectedContentTemplateGen = "";
        $scope.SelectedKeywordListGeneration = "";
        $scope.selectedLayoutGen = "";
        $scope.txt_indexTemplate = "";
        $scope.indexGridSource = [];
    }

    $scope.indexGeneration = function () {
        if ($scope.SelectedIndexGenTemplate != "" && $scope.selectedLayoutGen != "" && $scope.SelectedIndexGenTemplate != undefined && $scope.selectedLayoutGen != undefined) {
            dataFactory.indexGenerationProcess($scope.SelectedIndexGenTemplate, $scope.selectedLayoutGen).success(function (indexGenResponse) {
                $scope.indexGridValueLength = indexGenResponse.m_Item1.length;
                $scope.indexGridValueLength1 = indexGenResponse.m_Item1.length;
                if (indexGenResponse != null) {
                    $scope.indexGridSource = indexGenResponse.m_Item1;
                    $scope.indexFilePath = indexGenResponse.m_Item2;
                    $("#mergePart").show();
                    $("#indexGenPart").hide();
                    $("#mytable").show();
                    $("#testButton").hide();
                }
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Kindly select template and layout to generate index.',
                type: "error"
            });
        }
    }

    $scope.mergeClick = function () {
        var indexFilePath = $scope.indexFilePath;
        var sourceFilePath = $scope.excelPath;
        var insertType = $scope.radioCheck;
        if (insertType != undefined && insertType != "") {
            if ($scope.selectedFilesToMerge.length == 1) {
                if (indexFilePath != undefined && sourceFilePath != undefined) {
                    dataFactory.mergeProcess($scope.selectedLayoutGen, sourceFilePath, insertType, $scope.indexGridSource).success(function (mergeResponse) {
                        if (mergeResponse != null) {
                            $scope.selectedFilesToMerge = [];
                            $scope.cancelIndexTemplateDetails();
                            $scope.Cancelfile();
                            $scope.mergeCancel();
                            window.open("DownloadPDFIndexFiles.ashx?SessionId=" + mergeResponse);
                            $('#indexGenerationTemplate').data('kendoDropDownList').value(-1);
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Merge process completed successfully.',
                                type: "info"
                            })
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Merge process failed.',
                                type: "info"
                            })
                        }
                    });
                }
            }
            else if ($scope.selectedFilesToMerge.length > 1) {
                dataFactory.sourcePDFmergeProcess($scope.selectedFilesToMerge).success(function (sourceMergeResponse) {
                    if (sourceMergeResponse != null) {
                        sourceFilePath = sourceMergeResponse;
                        dataFactory.mergeProcess($scope.selectedLayoutGen, sourceFilePath, insertType, $scope.indexGridSource).success(function (mergeResponse) {
                            if (mergeResponse != null) {
                                $scope.selectedFilesToMerge = [];
                                $scope.cancelIndexTemplateDetails();
                                $scope.Cancelfile();
                                $scope.mergeCancel();
                                window.open("DownloadPDFIndexFiles.ashx?SessionId=" + mergeResponse);
                                $('#indexGenerationTemplate').data('kendoDropDownList').value(-1);
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Merge process completed successfully.',
                                    type: "info"
                                })
                            }
                            else {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Merge process failed.',
                                    type: "info"
                                })
                            }
                        });
                    }
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Kindly select a source PDF to merge.',
                    type: "error"
                });
            }
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Kindly select a place to merge.',
                type: "error"
            });
        }
    };

    $scope.mergeCancel = function () {
        $scope.SelectedFileForUploadnamemain = "";
        $scope.indexGridSource = [];
        $("#mergePart").hide();
        $("#mytable").hide();
        $("#indexGenPart").show();
        $scope.selectedFilesToMerge = [];
    };

    $scope.rowDeleteClick = function (data) {
        //$scope.indexGridSource.remove(data);
        if ($scope.indexGridSource.length > 1) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure to delete keyword from the list?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        bool = true;
                    }
                    if (bool === true) {
                        var newGridVal = [];
                        //angular.forEach($scope.indexGridSource, function (value) {
                        //    if (value.KEYWORD_NAME != data.KEYWORD_NAME) {
                        //        newGridVal.push(value);
                        //    }
                        //});
                        newGridVal = jQuery.grep($scope.indexGridSource, function (value) {
                            return value.KEYWORD_NAME != data.KEYWORD_NAME;
                        });
                        $timeout(function () {
                            $scope.indexGridSource = newGridVal;
                        }, 250);
                    }
                }
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Keywords list cannot be empty.',
                type: "info"
            });
        }
        $("#mytable").load("~/Views/UserAdmin/UserAdministration.cshtml #mytable");
    }

    $scope.sourceRowDeleteClick = function (data) {
        //$scope.indexGridSource.remove(data);
        $scope.SelectedFileForUploadnamemain = "";
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure to delete source PDF?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    var newSourceGridVal = [];
                    //angular.forEach($scope.selectedFilesToMerge, function (value) {
                    //    if (value.FILE_NAME != data.FILE_NAME) {
                    //        newSourceGridVal.push(value);
                    //    }
                    //});
                    newSourceGridVal = jQuery.grep($scope.selectedFilesToMerge, function (value) {
                        return value.FILE_NAME != data.FILE_NAME;
                    });
                    $timeout(function () {
                        $scope.selectedFilesToMerge = newSourceGridVal;
                    }, 250);
                }
            }
        });
    }
    //------------------------INDEX GENERATION---------------------------------------------//

    //-----------------------START INDEX EXPORT GENERATION---------------------------------------//
    $scope.exportFormatType = ".XLS";
    $scope.ExportKeywordsList = function (selectedTempateName) {

        dataFactory.exportIndex(selectedTempateName, $scope.exportFormatType).success(function (response) {
            if (response != null) {
                window.open("DownloadFullExport.ashx?Path=" + response);
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Error occurred during export.',
                });
            }



        });
    }
    $scope.exportFormatType = ".XLS";
    $scope.ExportExempte = function () {

        dataFactory.ExportExempte($scope.exportFormatType).success(function (response) {

            window.open("DownloadFullExport.ashx?Path=" + response);


        });
    }


    $scope.ImportKeyWordList = function () {

        $scope.Importvalidation = true;
        $scope.SelectedFileForUploadnamemainproduct = null;
        document.getElementById("ShowKeyWordLIst").style.display = "none";
    }
    $scope.cancelImport = function () {
        document.getElementById("ShowKeyWordLIst").style.display = "block";
        $scope.Importvalidation = false;
    }



    $scope.ImportIndesignvalues = function () {
        $scope.UploadFile($scope.SelectedFileForUpload, "Import");
    }



    //-----------------------END INDEX EXPORT GENERATION---------------------------------------//
    //------------------------Index PDF import starts here----------------

    //var sheetOptions = [
    //                    { text: "PDFCONTENT", value: "1" },
    //                    { text: "KEYWORDS", value: "2" },
    //];
    //$scope.sheetOptionDatasource = new kendo.data.ObservableArray(sheetOptions);

    $scope.Resetsheet = function () {
        $scope.file = null;
        $scope.SelectedFileForUpload = null;
        $rootScope.SelectedFileForUploadnamemain = null;
        $scope.txt_ContentTemplate = null;
        $scope.SelectedFileForUploadnamemainproduct = null;
        var $el = $('#importfileContentLibrary');
        if ($el.val() != "") {
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
        }

        var $el1 = $('#importfileIndesign');
        if ($el1.val() != "") {
            $el1.wrap('<form>').closest('form').get(0).reset();
            $el1.unwrap();
        }
    }

    $scope.Next = function () {
        if ($scope.file != undefined && $scope.file != null && $scope.file != "") {
            $scope.ImportFormat = $('#uploadFileName1').text();
            var fileExtension = $scope.file.name.split('.')[1];
            if (fileExtension.contains("xls") || fileExtension.contains("xlsx")) {
                $scope.selectSheet = true;
                $scope.Importvalidation = false;
                $scope.Missing = false;
                $scope.Duplicate = false;
                $scope.validationPassed = false;
                $('#Validation').show();
                $('#ValidationIndexPDFResult').hide();
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose a valid file.',
                });
                blockUI.stop();
                return true;
            }
        }

        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Select a file to import.',
                type: "info"
            });
        }
    }

    $scope.BackFromValidation = function () {
        $scope.selectSheet = false;
        $scope.Importvalidation = true;
        $scope.validateresult = true;
        $scope.importEnable = false;
        $scope.validatendexpdfresult = true;
    }

    $scope.Validatesheet = function (data1, data) {

        if (data == "Validate") {
            //$scope.value = data1;
            //if ($scope.value == 1) {
            //    $scope.sheetName = "PDFCONTENT";
            //}
            //else if ($scope.value == 2) {
            $scope.sheetName = "KEYWORDS";
            //}
            //var formData = new FormData();
            //var file = $scope.file;
            //formData.append("file", file);

            dataFactory.ValidateProcess($scope.excelPath, $scope.sheetName).success(function (response) {
                if (response != null) {
                    $scope.countError = response.length;
                    $scope.validationPassed = true;

                    for (i = 0; i < response.length; i++) {
                        $scope.result = response[i];

                        if ($scope.result == "Missing Column")
                            $scope.Missing = true;
                        $scope.missingValidation = true;
                        if ($scope.result == "Duplicate")
                            $scope.Duplicate = true;
                        if ($scope.result == "NewColumn")
                            $scope.newColumn = true;
                    }
                }
                if ($scope.countError == 0) {
                    $scope.importEnable = true;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Validation Successful.',
                        type: "error"
                    });
                }
                else {
                    $scope.importEnable = false;
                    $scope.validatendexpdfresult = false;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Validation Unsuccessful.',
                        type: "error"
                    });
                }

            });

        }
        else if (data != "Validate") {
            //var FormData1 = new FormData();
            //var file = $scope.file;
            //FormData1.append("file", file);
            $scope.sheetName = "KEYWORDS";
            //FormData1.append("tableName", $scope.sheetName);

            //$http.post("/PDFIndex/ImportIndexPdf?XMLName=" + $scope.sheetName + "", FormData1,
            //{
            //    withCredentials: true,
            //    headers: { 'Content-Type': undefined },
            //    transformRequest: angular.identity
            //})
            dataFactory.indexImportPDFKeywords($scope.excelPath, $scope.sheetName)
            .success(function (response) {
                if (response != null) {
                    var resArray = response.split('~');
                    var message = resArray[0];
                    $scope.importSessionID = resArray[1];
                    if (resArray.length > 2) {
                        $scope.insertedCount = resArray[2];
                        $scope.updatedCount = resArray[3] - resArray[4];
                        $scope.deletedCount = resArray[4];
                        $('#Validation').hide();
                        $('#keywordImportSuccessWindow').show();
                        $scope.txt_ContentTemplate = "";
                        $rootScope.SelectedFileForUploadnamemain = "";
                    }
                    else {
                        $("#keywordImportErrorWindow").show();
                        $scope.keywordImportResultDatasource.read();
                    }
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: message,
                        type: "info"
                    });
                    $scope.ImportvalidationResult = false;
                    $scope.showImport = true;
                }

            });

        }
    }


    $scope.BackFromValidationLog = function () {
        $('#Validation').show();
        $('#ValidationIndexPDFResult').hide();
    }



    $scope.BackFromexemptValidationLog = function () {
        $('#ValidationExemptResult').hide();
        $('#ValidationExempt').show();
    }

    $scope.backKeywordImportClick = function () {
        $('#keywordImportSuccessWindow').hide();
        $('#Validation').show();
    }

    //----------------------------------------End Index PDF import 

    //=======================================Import log display process==================================//

    $scope.keywordImportResultDatasource = new kendo.data.DataSource({
        type: "json", autobind: false,
        //serverFiltering: true, pageSize: 5, serverPaging: true,
        //serverSorting: true,
        sort: { field: "ErrorMessage", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ErrorMessage",
                fields: {
                    ErrorMessage: { type: "string" },
                    ErrorProcedure: { type: "string" },
                    ErrorSeverity: { type: "string" },
                    ErrorState: { type: "string" },
                    ErrorNumber: { type: "string" },
                    ErrorLine: { type: "string" }
                }
            }
        }

    });

    $scope.keywordImportResultGridOptions = {
        dataSource: $scope.keywordImportResultDatasource,
        //sortable: true,
        scrollable: true, resizable: true,
        //filterable: true,
        //pageable: true,
        autoBind: false,
        columns: [
        { field: "ErrorMessage", title: "Error Message", width: "200px" },
        { field: "ErrorProcedure", title: "Error Procedure", width: "200px" },
        { field: "ErrorSeverity", title: "Error Severity", width: "200px" },
        { field: "ErrorState", title: "Error State", width: "200px" },
        { field: "ErrorNumber", title: "Error Number", width: "200px" },
        { field: "ErrorLine", title: "Error Line" }
        ]
    };
    //=======================================Import log display process==================================//

    //---------Start Excempt list import process

    $scope.ImportExempte = function () {
        $scope.ImportExempteValidation = true;
        $scope.Importvalidation = true;
        $scope.hideKeyword = false;
    }

    $scope.backImportExempt = function () {
        $scope.ImportExempteValidation = false;
        $scope.Importvalidation = false;
        $scope.hideKeyword = true;
    }

    $scope.ResetsheetExempt = function () {
        $scope.SelectedFileForUploadnamemainproduct = null;
        $scope.file = null;
        var $el = $('#importfile');
        if ($el.val() != "") {
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
        }
    }

    $scope.NextExempt = function () {
        if ($scope.file != undefined && $scope.file != null && $scope.file != "") {
            $scope.ImportFormat = $('#uploadFileName1').text();
            var fileExtension = $scope.file.name.split('.')[1];
            if (fileExtension.contains("xls") || fileExtension.contains("xlsx")) {
                $scope.selectSheet = true;
                $scope.validation = true;
                $scope.Importvalidation = false;
                $scope.Missing = false;
                $scope.Duplicate = false;
                $scope.importEnable = false;
                $scope.validationPassed = false;
                $scope.ImportExempteValidation = true;
                $('#ValidationExemptResult').hide();
                $('#ValidationExempt').show();
            }

            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose a valid file.',
                });
                blockUI.stop();
                return true;
            }
        }

        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Select a file to import.',
                type: "info"
            });
        }
    }
    $scope.ValidateExemptsheet = function (data) {

        var file = $scope.file;
        var formData = new FormData();

        formData.append('file', file);
        if (data == 'Validate') {
            $http.post("/PDFIndex/ValidateExemptsheet?XMLName=" + $scope.newxmlname1 + "", formData,
                       {
                           withCredentials: true,
                           headers: { 'Content-Type': undefined },
                           transformRequest: angular.identity
                       })
                        .success(function (response) {
                            if (response != null) {
                                $scope.countError = response.length;
                                $scope.validationPassed = true;

                                for (i = 0; i < response.length; i++) {
                                    $scope.result = response[i];

                                    if ($scope.result == "Missing Column")
                                        $scope.Missing = true;
                                    $scope.missingValidation = true;
                                    if ($scope.result == "Duplicate")
                                        $scope.Duplicate = true;
                                    if ($scope.result == "NewColumn")
                                        $scope.newColumn = true;
                                }
                            }
                            if ($scope.countError == 0) {
                                $scope.importEnable = true;
                                $scope.validation = false;
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Validation Successful.',
                                    type: "error"
                                });
                            }
                            else {
                                $scope.importEnable = false;
                                $scope.validation = false;
                                $scope.validateresult = false;
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Validation Unsuccessful.',
                                    type: "error"
                                });
                            }
                        })
        }
        else if (data == 'Import') {
            $http.post("/PDFIndex/ImportExemptsheet?XMLName=" + $scope.newxmlname1 + "", formData,
                      {
                          withCredentials: true,
                          headers: { 'Content-Type': undefined },
                          transformRequest: angular.identity
                      })
                       .success(function (response) {
                           if (response != null) {
                               var resArrayExempt = response.split('~');
                               var messageVal = resArrayExempt[0];
                               $scope.exemptImportSessionId = resArrayExempt[1];
                               if (resArrayExempt.length > 2) {
                                   $scope.exemptInsertRecords = resArrayExempt[2];
                                   $scope.exemptUpdateRecords = resArrayExempt[3];
                                   $scope.exemptDeleteRecords = resArrayExempt[4];
                                   $('#ValidationExempt').hide();
                                   $('#exemptImportSuccessWindow').show();
                               }
                               else {
                                   $("#exemptImportErrorWindow").show();
                                   $scope.exemptImportResultDatasource.read();
                               }
                               $scope.importEnable = false;
                               $scope.validation = false;
                               $.msgBox({
                                   title: $localStorage.ProdcutTitle,
                                   content: messageVal,
                                   type: "info"
                               });
                               $scope.getExemptedwordsList();
                               //$scope.hideKeyword = true;
                               //$scope.ImportExempteValidation = false
                           }
                       });

        }
    }



    $scope.ViewLogIndexpdfsheet = function () {
        dataFactory.IndexpdfvalidationViewLog().success(function (response) {
            $scope.ProcessName = "View Log";
            if (response != null) {
                //   var obj = jQuery.parseJSON(response.Data);                          
                //      $scope.ValidationExemptLogcolumn = response.Data;
                //  $scope.ValidationExemptLogcolumn = response.Columns;
                var obj = jQuery.parseJSON(response.Data);
                $scope.validateLogprod = obj;
                $scope.ValidationExemptLogcolumn = response.Columns;

                $('#Validation').hide();
                $('#ValidationIndexPDFResult').show();

            }
        });
    }


    $scope.ViewLogExemptsheet = function () {
        dataFactory.ExemptViewLog().success(function (response) {
            $scope.ProcessName = "View Log";
            if (response != null) {
                //   var obj = jQuery.parseJSON(response.Data);                          
                //      $scope.ValidationExemptLogcolumn = response.Data;
                //  $scope.ValidationExemptLogcolumn = response.Columns;
                var obj = jQuery.parseJSON(response.Data);
                $scope.validateLogprod = obj;
                $scope.ValidationExemptLogcolumn = response.Columns;

                $('#ValidationExempt').hide();
                $('#ValidationExemptResult').show();

            }
        });
    }


    $scope.backExemptImportClick = function () {
        $('#exemptImportSuccessWindow').hide();
        $('#ValidationExempt').show();
    }

    //=======================================Import log display process==================================//

    $scope.exemptImportResultDatasource = new kendo.data.DataSource({
        type: "json", autobind: false,
        //serverFiltering: true, pageSize: 5, serverPaging: true,
        //serverSorting: true,
        sort: { field: "ErrorMessage", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportResults($scope.exemptImportSessionId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ErrorMessage",
                fields: {
                    ErrorMessage: { type: "string" },
                    ErrorProcedure: { type: "string" },
                    ErrorSeverity: { type: "string" },
                    ErrorState: { type: "string" },
                    ErrorNumber: { type: "string" },
                    ErrorLine: { type: "string" }
                }
            }
        }

    });

    $scope.exemptImportResultGridOptions = {
        dataSource: $scope.exemptImportResultDatasource,
        //sortable: true,
        scrollable: true, resizable: true,
        //filterable: true,
        //pageable: true,
        autoBind: false,
        columns: [
        { field: "ErrorMessage", title: "Error Message", width: "200px" },
        { field: "ErrorProcedure", title: "Error Procedure", width: "200px" },
        { field: "ErrorSeverity", title: "Error Severity", width: "200px" },
        { field: "ErrorState", title: "Error State", width: "200px" },
        { field: "ErrorNumber", title: "Error Number", width: "200px" },
        { field: "ErrorLine", title: "Error Line" }
        ]
    };
    //=======================================Import log display process==================================//


    //------------------------------------------check all functionality--------------------------------------//
    $scope.selectAllKeyWords = function () {
        var checkAllFlag = $('#key_SelectAll').is(":checked");
        if (checkAllFlag) {
            angular.forEach($scope.indexGridSource, function (dataRow) {
                //$('#key_' + dataRow.KEYWORD_NAME).is(":checked") = true;
                document.getElementById('key_' + dataRow.KEYWORD_NAME).checked = true;

            });
            $scope.selectedKeywords = $scope.indexGridSource;
        }
        else {
            angular.forEach($scope.indexGridSource, function (dataRow) {
                //$('#key_' + dataRow.KEYWORD_NAME).is(":checked") = false;
                document.getElementById('key_' + dataRow.KEYWORD_NAME).checked = false;
            });
            $scope.selectedKeywords = [];
        }
    }

    $scope.selectKeyordForDelete = function (e) {
        var checkFlag = $('#key_' + e.KEYWORD_NAME).is(":checked");
        if (checkFlag) {
            $scope.selectedKeywords.push(e);
            $scope.indexGridValueLength1 = $scope.indexGridValueLength1 + 1;
            if ($scope.indexGridValueLength1 == $scope.indexGridValueLength) {
                document.getElementById('key_SelectAll').checked = true;
            }
        }

        else {
            $scope.selectedKeywords = jQuery.grep($scope.selectedKeywords, function (value) {
                return value.KEYWORD_NAME != e.KEYWORD_NAME;
            });
            $scope.indexGridValueLength1 = $scope.indexGridValueLength1 - 1;
            document.getElementById('key_SelectAll').checked = false;


        }
    }

    $scope.deleteMultipleClick = function () {
        if ($scope.selectedKeywords != undefined && $scope.selectedKeywords != null && $scope.selectedKeywords.length > 0) {
            var selectedMultipleKeywords = $scope.indexGridSource;
            if ($scope.selectedKeywords.length != $scope.indexGridSource.length) {
                angular.forEach($scope.selectedKeywords, function (selectedRow) {
                    selectedMultipleKeywords = jQuery.grep(selectedMultipleKeywords, function (value) {
                        return value.KEYWORD_NAME != selectedRow.KEYWORD_NAME;
                    });
                });
                $timeout(function () {
                    $scope.indexGridSource = selectedMultipleKeywords;
                    $scope.selectedKeywords = [];
                }, 250);
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Keywords list cannot be empty.',
                    type: "info"
                });
            }
        }
    }
    //------------------------------------------check all functionality--------------------------------------//
}]);