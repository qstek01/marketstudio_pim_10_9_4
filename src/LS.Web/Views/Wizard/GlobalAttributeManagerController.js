﻿
LSApp.controller('GlobalAttributeManagerController', ['$scope', '$rootScope', 'dataFactory', '$http', '$compile', '$localStorage', '$window', function ($scope, $rootScope, dataFactory, $http, $compile, $localStorage, $window) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;


    $scope.Sort = "";
    $scope.attributePattern = "";
    $scope.textPattern = "";
    $scope.attributecheck = false;
    $scope.valueForSelectedFamily = "";
    //$scope.GroupeType = 2;
    $scope.isAttributePackage = false;

    var packageTypefulldata = [{
        packagetype: 2,
        packagetypename: 'Product'
    }, {
        packagetype: 1,
        packagetypename: 'Family'
    }];


    if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
        $("#chkid1").prop("checked", true);
        $scope.IsCheck = true;
    }
    else {
        $("#chkid1").prop("checked", false);
        $scope.IsCheck = false;
    }

    $("#checkboxpadding").removeClass("disableBox");

    $scope.AttrtypeChange = function (e) {
        if (e.sender.value() != "") {
            $scope.selectcatalogattr(e.sender.value());
            if (e.sender.value() == 14 || e.sender.value() == 15) {
                $scope.isAttributePackage = true;
                var packType;
                if (e.sender.value() == 14) {
                    $scope.GroupeTypeName = "Product";
                }
                if (e.sender.value() == 15) {
                    $scope.GroupeTypeName = "Family";
                }
                //$timeout(function () {
                $scope.groupNameDataSource.read();
             //   $scope.unselectallDisplay = false;
               // $("#chkselectall").removeAttr('disabled');
              //  $("#addGroup").text("Add selected attributes");
              //  $("#removeGroup").text("Remove selected attributes");
                $(".isAttributePack").show();
                $(".isNotAttributePack").hide();
                $scope.unselectallDisplay = true;
                $("#chkselectall").attr('disabled', 'disabled');
                $("#addGroup").text("Add Group");
                $("#removeGroup").text("Remove Group");


                //}, 200);
            }
            else if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
                $("#chkid1").prop("checked", true);
                $scope.isAttributePackage = false;
                $scope.IsCheck = true;
                $("#checkboxpadding").removeClass("disableBox");
                $scope.GroupeTypeName = "";
                $scope.unselectallDisplay = false;
                $("#chkselectall").removeAttr('disabled');
                $("#addGroup").text("Add selected attributes");
                $("#removeGroup").text("Remove selected attributes");
                $(".isAttributePack").hide();
                $(".isNotAttributePack").show();
            }
            else {
                $("#chkid1").prop("checked", false);
                $scope.isAttributePackage = false;
                $scope.IsCheck = false;
                $("#checkboxpadding").removeClass("disableBox");
                $scope.GroupeTypeName = "";
                $scope.unselectallDisplay = false;
                $("#chkselectall").removeAttr('disabled');
                $("#addGroup").text("Add selected attributes");
                $("#removeGroup").text("Remove selected attributes");
                $(".isAttributePack").hide();
                $(".isNotAttributePack").show();
            }
            if (e.sender.value() != 14 && e.sender.value() != 15) {
                $(".isAttributePack").hide();
                $(".isNotAttributePack").show();
            }
        } else {
            $scope.entities = [];
            if (e.sender.text() == "--- Select Attribute Type ---") {
                $scope.isAttributePackage = false;
            } else {

            }
        }
    };

    $(".isAttributePack").hide();

    $scope.SelecteAttrType = '';
    $scope.AttrtypeChangetab = function (e) {
        if (e.sender.value() != "") {
            $scope.selectattributes(e.sender.value());
            $scope.SelecteAttrType = e.sender.value();
        } else {
            $scope.Attrtypeselected = 0;
            $scope.selectattributes(0);
            $scope.SelecteAttrType = 0;
        }
    };
    $scope.AttrtypeChangeForSelectedFamily = function (e) {

        if (e.sender.value() != "") {
            $scope.selectattributesForSelectedFamily(e.sender.value());
            $scope.SelecteAttrTypeForSelectedFamily = e.sender.value();

        } else {
            $scope.AttrtypeselectedForFamilySelected = 0;
            $scope.selectattributesForSelectedFamily(0);
            $scope.SelecteAttrTypeForSelectedFamily = 0;
        }
    };


    $scope.Attrtypeselected = 0;
    $scope.AttrtypeselectedForFamilySelected = 0;
    $scope.selectattributes = function (attributetype) {
        $scope.Attrtypeselected = attributetype;

        $scope.selectedAttribute.read();
    };
    $scope.selectattributesForSelectedFamily = function (attributetypeFSF) {
        //if (attributetypeFSF == "") {
        //    $scope.SelecteAttrTypeForSelectedFamily = e.sender.value();
        //    //return;
        //}
        $scope.AttrtypeselectedForFamilySelected = attributetypeFSF;
        $scope.selectedAttributeForSelectedFamily.read();

    };

    $scope.selectedAttribute = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetAttributeDetailsForGlobal($scope.Attrtypeselected, $scope.seelectCatalogID).success(function (response) {

                    if ($scope.Attrtypeselected == '1') {
                        response[0]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.updatevalueForSelectedFamily = function () {


        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var sorting = $scope.Sort;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        //alert($scope.valueForSelectedFamily);
        var selectedFamilytreeView = $("#treeviewKendo4").data("kendoTreeView");
        selectedFamilytreeData = selectedFamilytreeView.dataSource._data;
        if ($scope.Attrselected_val_FSF !== '') {
            if ($scope.checkeditems !== '') {

                dataFactory.updateGlobalAttrValueForSelectedFamily($scope.seelectCatalogID, $scope.AttrtypeselectedForFamilySelected, $scope.Attrselected_val_FSF, $scope.valueForSelectedFamily, $scope.checkeditems).success(function (response) {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Update completed.',
                        type: "info"
                    });

                }).error(function (error) {
                    //   options.error(error);
                });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Family.',
                    //type: "info"
                });
            }
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select an Attribute.',
                //type: "info"
            });
        }


    };

    $scope.selectedAttributeForSelectedFamily = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {

                //if ($scope.AttrtypeselectedForFamilySelected == "") {

                //    return;
                //}

                dataFactory.GetAttributeDetailsForGlobal($scope.AttrtypeselectedForFamilySelected, $scope.seelectCatalogID).success(function (response) {
                    if ($scope.AttrtypeselectedForFamilySelected == '1') {
                        response[0]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                    }
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }
        }
    });

    $scope.seelectCatalogID = 0;
    $scope.selectedcatalog = function (catalogId) {
        $scope.seelectCatalogID = catalogId;
        $scope.selectedcatalog1.read();
    };
    $scope.selectedcatalog_sort = function (catalogId) {
        $scope.seelectCatalogID = catalogId;
        $scope.selectedcatalog1_sort.read();
    };
    $scope.selectedcalculatedcatalog = function (catalogId) {
        $scope.seelectCatalogID = catalogId;
        $scope.selectedCalculatedcatalog1.read();
    };
    $scope.Attrselected_val = '';
    $scope.Attrselected_val_FSF = '';
    $scope.Attrselect = function (e) {
        if (e.sender.value() != "") {
            $scope.Attrselected_val = e.sender.value();
        }
    };
    $scope.UIMask = function (decimal, number) {
        var uimask = "";
        var uimask1 = "";
        for (var i = 0; i < decimal; i++) {
            uimask += "x";
        }
        for (var i1 = 0; i1 < number; i1++) {
            uimask1 += "x";
        }
        if (number != 0) {
            return uimask + "." + uimask1;
        } else {
            return uimask;
        }
    };

    $scope.AttrselectForSelectedFamily = function (e) {

        $scope.attributePattern = "";
        $scope.textPattern = "";
        $scope.Attrselected_val_FSF = e.sender.value();
        if ($scope.Attrselected_val_FSF == "") {
            return;
        }
        dataFactory.Attributedatatype(e.sender.value()).success(function (response) {
            if (response.length != 0) {
                if (response.contains('Num')) {
                    $scope.attributecheck = true;
                    var tempattrvals = response.match(/\((.*)\)/);
                    var tempval = tempattrvals[1].split(',');
                    var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                    pattern = pattern.replace("numeric", tempval[0]);
                    pattern = pattern.replace("decimal", tempval[1]);
                    var reg = new RegExp(pattern);
                    $scope.uimask = $scope.UIMask(tempval[0], tempval[1]);
                    $scope.attributePattern = reg;
                    $("#Atrributeval").hide();
                    $("#Atrributeval2").show();
                    $("#errorspan").show();
                    // tempattrvals[0].attributePattern = reg;
                }
                else {

                    if (response.contains('Text(')) {
                        var tempattrvals = response.match(/\((.*)\)/);


                        var pattern = "^.{0,decimal}$";
                        pattern = pattern.replace("decimal", tempattrvals[1]);
                        var reg = new RegExp(pattern);
                        $scope.uimask = tempattrvals[1];
                        $scope.textPattern = reg;
                        $("#Atrributeval").show();
                        $("#Atrributeval2").hide();
                        $("#errorspan").show();

                    }
                    else {
                        //$scope.uimask = 524288;
                        // $scope.textPattern = 524288;
                        var pattern = "^.{0,524288}$";
                        var reg = new RegExp(pattern);

                        $scope.textPattern = reg;
                        $("#Atrributeval").show();
                        $("#Atrributeval2").hide();
                        $("#errorspan").show();

                    }

                }
            }
        }).error(function (response) {
            options.success(response);
        });
    };



    $scope.selectedcatalog1 = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {

                dataFactory.getCategories($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);
                    //$("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
                    //$("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });
    $scope.selectedcatalog1_sort = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {

                dataFactory.getCategories($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);
                    //$("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
                    //$("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });

    $scope.selectedCalculatedcatalog1 = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {

                dataFactory.getCategories($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        }
    });

    $scope.selectedcatalog_ForSelectedFamily = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {

                dataFactory.getCategories($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);
                    //$("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
                    //$("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });


    $scope.selectedAttributetype = 0;
    $scope.selectcatalogattr = function (Attributeid) {
        $scope.selectedAttributetype = Attributeid;
        if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
            $("#chkid1").prop("checked", true);
            $scope.IsCheck = true;
        }
        else {
            $("#chkid1").prop("checked", false);
            $scope.IsCheck = false;
        }

        if (Attributeid == "") {
            Attributeid = 0;
        }
        $http.get("../Wizard/GetAllattributes?attributeid=" + Attributeid + "&catalogId=" + $scope.SelectedCatalogId).
            then(function (attributes) {
                $scope.entities = attributes.data;
                // $scope.selected = [];
                // $scope.removed = [];
                //$scope.newselected = [];
                //$scope.selectedvalue();


            });

    };

    $scope.selectAll = function ($event) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        for (var i = 0; i < $scope.entities.length; i++) {
            var entity = $scope.entities[i];
            updateSelected(action, entity.ATTRIBUTE_ID);
        }
    };
    $scope.selectall = function (name) {

        $(".chk1").each(function () {
            $(this).prop("checked", true);
        });
        if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
            $("#chkid1").prop("checked", true);

            $scope.IsCheck = true;

        }
        else {
            $("#chkid1").prop("checked", false);

            $scope.IsCheck = false;

        }
    };
    $scope.check = function (selct) {
        if (document.getElementById('selattr').checked === true) {
            $("#remattr").prop("checked", false);
            document.getElementById("remattr").disabled = true;
            $("#rempub").prop("checked", false);
            document.getElementById("rempub").disabled = true;
            $("#addpub").prop("checked", false);
            document.getElementById("addpub").disabled = false;
        } else {
            document.getElementById("remattr").disabled = false;
            document.getElementById("rempub").disabled = false;
        }
        $("#chkid1").prop("checked", true);

        $scope.IsCheck = true;

    };

    $scope.remattrcheck = function (selct) {

        if (document.getElementById('remattr').checked === true) {
            $("#rempub").prop("checked", false);
            document.getElementById("rempub").disabled = true;
            $("#addpub").prop("checked", false);
            document.getElementById("addpub").disabled = true;
            $("#selattr").prop("checked", false);
        } else {
            document.getElementById("rempub").disabled = false;
            document.getElementById("addpub").disabled = false;
        }
        $("#chkid1").prop("checked", false);
    };

    $scope.rempubcheck = function (select) {

        if (document.getElementById('rempub').checked === true) {
            $("#addpub").prop("checked", false);
        }
        $("#chkid1").prop("checked", false);
    };

    $scope.addpubcheck = function (select) {
        if (document.getElementById('addpub').checked === true) {
            $("#rempub").prop("checked", false);
        }
        $("#chkid1").prop("checked", true);

        $scope.IsCheck = true;

    };

    $scope.unselectall = function (name) {

        $(".chk1").each(function () {

            $(this).prop("checked", false);
        });
        if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
            $("#chkid1").prop("checked", true);

            $scope.IsCheck = true;

        }
        else {
            $("#chkid1").prop("checked", false);

            $scope.IsCheck = false;

        }
    };
    $("#tabselect li").click(function () {
        $scope.checkeditems = '';
    });

    $scope.runsortscript = function (totest) {
        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrid = new String;

        objstrid = $scope.Attrselected_val;
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var sorting = $scope.Sort;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        var objoption = new String;
        $(".chkoption").each(function () {
            if ($(this).is(':checked')) {
                objoption = objoption + ',' + $(this).val();
            }
        });
        objoption = objoption.substring(1);
        if ($scope.SelecteAttrType != '' && $scope.Attrselected_val != '' && $scope.checkeditems != '' && $scope.Sort != '') {
            //$http.get("../Wizard/globalattribute?Attrid=" + objstrid + "&&familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&Attributetype=" + $scope.SelecteAttrType + "&&option=sort &&sort=" + sorting)
            //$http.post("../Wizard/globalattribute?Attrid=" + objstrid + "&&familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&Attributetype=" + $scope.SelecteAttrType + "&&option=sort &&sort=" + sorting, JSON.stringify(objstrcatid))
            //    .success(function (response) {
            //        if (response != null) {
            //            alert(response);
            //        };
            //    });

            dataFactory.globalattribute(objstrid, $scope.checkeditems, $scope.seelectCatalogID, $scope.SelecteAttrType, "sort", sorting).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Saved successfully.',
                    type: "info"
                });

            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Save action not completed.',
                    type: "error"
                });
            });
        }
        else {
            if ($scope.SelecteAttrType == '') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Attribute Type.',
                    type: "error"
                });
            }

            else if ($scope.Attrselected_val == '') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select attribute.',
                    //type: "error"
                });
            }
            else if ($scope.Sort == '' || $scope.Sort == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a Sort Order.',
                    type: "error"
                });
            }
            else if (objstrcatid == '') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Family.',
                    type: "error"
                });
            }

        }
    };

    $scope.runscript = function (totest) {
        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrid = new String;
        $(".chk1").each(function () {
            if ($(this).is(':checked')) {
                objstrid = objstrid + $(this).val() + ',';
                cnt = cnt + 1;
                flg = 1;
            }
        });
      
        //var checkedValue = null;
        //var objstrid = new String;
        //var inputElements = document.getElementsByClassName('chk1');
        //$(".chk").each(function () {
        //    for (var i = 0; inputElements[i]; ++i) {
        //        if (inputElements[i].checked) {
        //            objstrid = inputElements[i].value;
        //            cnt = cnt + 1;
        //            flg = 1;
                   
        //        }
        //    }
        //});



        // $scope.chkattrlist = objstrid;
        if ($scope.GroupeTypeName == "Family" || $scope.GroupeTypeName == "Product") {
            $scope.chkattrlist = '~' + $scope.SelectedGroupId;
            objstrid = '~' + $scope.SelectedGroupId;
        }
        else {
            $scope.chkattrlist = objstrid;
        }
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        var objoption = new String;
        $(".chkoption").each(function () {
            if ($(this).is(':checked')) {
                objoption = objoption + ',' + $(this).val();
            }
        });
        objoption = objoption.substring(1);
        $scope.attroption = objoption;
        var sort = 0;
        if ($scope.chkattrlist != '' && $scope.chkattrlist != undefined && $scope.attroption != '' && $scope.checkeditems != '') {
            //$http.get("../Wizard/globalattribute?Attrid=" + objstrid + "&&familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&Attributetype=" + $scope.selectedAttributetype + "&&option=" + objoption + "&&sort=" + sort)
            //    .success(function (response) {
            //        if (response != null) {
            //            alert(response);
            //        };
            //    });

            dataFactory.globalattribute(objstrid, $scope.checkeditems, $scope.seelectCatalogID, $scope.selectedAttributetype, objoption, sort).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Saved successfully.',
                    type: "info"
                });

            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Save action not completed.',
                    type: "error"
                });
            });
        }
        else {
            if ($scope.selectedAttributetype == '' || $scope.selectedAttributetype == '--- Select Attribute type ---') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Attribute Type.',
                    type: "error"
                });
            }
            else if ($scope.chkattrlist == '' || $scope.chkattrlist == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one attribute.',
                    type: "error"
                });
            }

            else if ($scope.attroption == '' || $scope.attroption == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Option and continue.',
                    type: "error"
                });
            }
            else if ($scope.attrfamilyIds == '' || $scope.attrfamilyIds == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Product Family.',
                    type: "error"
                });
            }

        }
    };

    $scope.runscriptsubproducts = function (totest1) {

        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrid = new String;
        $(".chk").each(function () {
            if ($(this).is(':checked')) {
                objstrid = objstrid + $(this).val() + ',';
                cnt = cnt + 1;
                flg = 1;
            }
        });
        //$scope.chkattrlist = objstrid;
        if ($scope.GroupeTypeName == "Family" || $scope.GroupeTypeName == "Product") {
            $scope.chkattrlist = '~' + $scope.SelectedGroupId;
            objstrid = '~' + $scope.SelectedGroupId;
        }
        else {
            $scope.chkattrlist = objstrid;
        }
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        var objoption = new String;
        $(".chkoption").each(function () {
            if ($(this).is(':checked')) {
                objoption = objoption + ',' + $(this).val();
            }
        });
        objoption = objoption.substring(1);
        $scope.attroption = objoption;
        var sort = 0;

        // alert($scope.subproductcheckeditems);

        if ($scope.chkattrlist != '' && $scope.chkattrlist != undefined && $scope.attroption != '' && $scope.subproductcheckeditems != '') {

            if ($scope.selectedAttributetype != 7 && $scope.selectedAttributetype != 9 && $scope.selectedAttributetype != 11 && $scope.selectedAttributetype != 13) {

                dataFactory.globalattributesub(objstrid, $scope.subproductcheckeditems, $scope.seelectCatalogID, $scope.selectedAttributetype, objoption, sort).success(function (response) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Saved successfully.",
                        type: "info",
                        //autoClose: true
                    });

                }).error(function (error) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Save action not completed.",
                        type: "error",
                        ///autoClose: true
                    });
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Family Level attributes cannot be selected.",
                    type: "info",
                    // autoClose:true
                });
            }

        }
        else {
            if ($scope.selectedAttributetype == '' || $scope.selectedAttributetype == '--- Select Attribute type ---') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Attribute Type.',
                    type: "error"
                });
            }
            else if ($scope.chkattrlist == '' || $scope.chkattrlist == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one attribute.',
                    type: "error"
                });
            }
            else if ($scope.attroption == '' || $scope.attroption == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Option and continue.',
                    type: "error"
                });
            }
            else if ($scope.attrfamilyIds == '' || $scope.attrfamilyIds == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Product Family.',
                    type: "error"
                });
            }

        }
    };


    $scope.runcalculatedscript = function (CalculatedAttributes) {
        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        if ($scope.checkeditems != '') {
            //$http.get("../Wizard/globalCalculatedAttribute?familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&option=CALC")
            //    .success(function (response) {
            //        if (response != null) {
            //            alert(response);
            //        };
            //    });

            dataFactory.globalCalculatedAttribute($scope.checkeditems, $scope.seelectCatalogID, "CALC").success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Saved successfully.',
                    type: "info"
                });

            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Save action not completed.',
                    type: "error"
                });
            });
        }
        else {
            if ($scope.checkeditems == '' || $scope.checkeditems == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Product Family.',
                    type: "error"
                });
            }
        }
    };
    $scope.pagereload = function (name) {

        location.reload();
    };
    //Referesh global attribute value functionality
    $scope.Valuerefresh = function () {

        //$scope.AttributeDataSource.read();
        $scope.attributetype = '';
        $scope.attributetype1 = '';
        $scope.selectattribute = '';
        $scope.selectedAttributeForSelectedFamily.read();
        $scope.valueForSelectedFamily = '';
        $scope.CheckOrUncheckBox4 = '';
        $scope.ExpandOrCollapseBox4 = '';
        $scope.selectedcatalog_ForSelectedFamily.read();
        $scope.CheckOrUncheckBoxsubvalue = '';
        $scope.ExpandOrCollapseBox2value = '';
        $scope.subproductdatasourceAttrvalue.read();

    }
    //Refresh calculated attribute functionality
    $scope.Calrefresh = function () {

        $scope.applytorefresh = '';
        $scope.applytorefresh1 = '';
        $scope.CheckOrUncheckBox3 = '';
        $scope.ExpandOrCollapseBox3 = '';
        $scope.selectedCalculatedcatalog1.read();
    }
    //Refresh sort functionality
    $scope.sortrefresh = function () {

        $scope.attributetype = '';
        $scope.attributetype1 = '';
        $scope.attributesort = '';
        $scope.Sort = '';
        $scope.CheckOrUncheckBox2 = '';
        $scope.ExpandOrCollapseBox2 = '';
        $scope.selectedcatalog1_sort.read();
        $scope.CheckOrUncheckBox2sub = '';
        $scope.ExpandOrCollapseBox2sub = '';
        $scope.subproductdatasourceColumn.read();
    }
    $scope.getSelectedClass = function (entity) {
        return $scope.isSelected(entity.id) ? 'selected' : '';
    };
    $scope.isSelected = function (id) {

        return $scope.selected.indexOf(id) >= 0;
    };

    $scope.CatalogtreeChange = new kendo.data.HierarchicalDataSource({
        type: "json",
        transport: {
            read: function (options) {

                dataFactory.getCategories(options.data.id).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        }
    });

    //Static Content to display in global attribute page all dropdown.
    $scope.AttributeDataSource = [
    { id: 14, DATA_TYPE: "Item Group" },
    { id: 15, DATA_TYPE: "Product Group" },
    {
        id: 1, DATA_TYPE: "Item Specifications"
    },
    { id: 3, DATA_TYPE: "Item Image / Attachment" },
    { id: 4, DATA_TYPE: "Item Price" },
    { id: 6, DATA_TYPE: "Item key" },
    { id: 11, DATA_TYPE: "Product Specifications" },
    { id: 9, DATA_TYPE: "Product Image / Attachment" },
    { id: 12, DATA_TYPE: "Product Price" },
    { id: 13, DATA_TYPE: "Product Key" },
    { id: 7, DATA_TYPE: "Product Description" },
    { id: 21, DATA_TYPE: "Category Specifications" },
    { id: 23, DATA_TYPE: "Category Image / Attachment" },
    { id: 25, DATA_TYPE: "Category Description" }

    ];
    // --------------------------------------------------- End Sort Tab--------------------------------------------------------------------------------
    $scope.init = function () {

    };
    $scope.CheckAttribute = function (id) {
    };
    $scope.selectedtab = "";
    $scope.treeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            //console.log(e);
            $scope.checkeditems = '';
            if (!e.node) {
                $scope.selectedtab = "AttributeAssociation";
                $scope.attachChangeEvent();
            }
        }
    };

    $scope.treeOptionsCatalog_Sub = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            //console.log(e);
            $scope.checkeditems = '';
            if (!e.node) {
                $scope.selectedtab = "AttributeAssociationsub";
                $scope.attachChangeEvent();
            }
        }
    };

    $scope.treeOptionsCatalog_sort = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.checkeditems = '';
            //console.log(e);
            if (!e.node) {
                $scope.selectedtab = "ColumnSorter";
                $scope.attachChangeEvent();
            }
        }
    };
    $scope.treeOptionsCalculatedCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.checkeditems = '';
            //console.log(e);
            if (!e.node) {
                $scope.selectedtab = "CalculatedAttribute";
                $scope.attachChangeEvent();
            }
        }
    };
    $scope.treeOptionsCatalog_ForSelectedFamily = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {

            $scope.checkeditems = '';
            //console.log(e);
            if (!e.node) {
                $scope.selectedtab = "SelectedFamilyValue";
                $scope.attachChangeEvent();
            }
        }
    };
    $scope.attachChangeEvent = function () {
        if ($scope.selectedtab != "" && $scope.selectedtab == "AttributeAssociation") {
            var dataSource = $scope.selectedcatalog1;

        }
        else if ($scope.selectedtab != "" && $scope.selectedtab == "ColumnSorter") {
            var dataSource = $scope.selectedcatalog1_sort;
        }
        else if ($scope.selectedtab != "" && $scope.selectedtab == "SelectedFamilyValue") {
            var dataSource = $scope.selectedcatalog_ForSelectedFamily;
        }
        else {
            var dataSource = $scope.selectedCalculatedcatalog1;
        }

        $scope.checkeditems = '';
        dataSource.bind("change", function (e) {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.checkedNodeIds(dataSource.view(), checkedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            //console.log(selectedNodes);

            $scope.checkeditems = checkedNodes.join(",");
        });



    };
    $scope.checkedNodeIds = function (nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].CATEGORY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    };

    $scope.selectAllCat = function () {
        $('#treeviewKendo1 input[type="checkbox"]').prop('checked', true);
    };
    $scope.unselectallcat = function () {
        $('#treeviewKendo1 input[type="checkbox"]').prop('checked', false);
    };

    $scope.selectallcat1 = function () {
        $('#treeviewKendo1 input[type="checkbox"]').prop('checked', true);
    };
    $scope.unselectallcat1 = function () {
        $('#treeviewKendo2 input[type="checkbox"]').prop('checked', false);
    };

    // Start of Check/ UnCheck and Expand / Collapse
    $scope.CheckOrUncheckBox1 = false;

    $scope.CheckOrUncheck1 = function () {

        if (chkEnabled1.checked == true) {
            $scope.CheckOrUncheckBox1 = true;
        } else {
            $scope.CheckOrUncheckBox1 = false;
        }
        if ($scope.CheckOrUncheckBox1) {
            $('#treeviewKendo1 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox1 = true;
        } else {
            $('#treeviewKendo1 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox1 = false;
        }
    };

    $scope.CheckOrUncheckBox1sub = false;
    $scope.CheckOrUncheck1sub = function () {

        if (chkEnabledsub1.checked == true) {
            $scope.CheckOrUncheckBox1sub = true;
        } else {
            $scope.CheckOrUncheckBox1sub = false;
        }
        if ($scope.CheckOrUncheckBox1sub) {
            $('#treeviewKendo1sub input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox1sub = true;
        } else {
            $('#treeviewKendo1sub input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox1sub = false;
        }
    };




    $scope.ExpandOrCollapseBox1 = false;
    $scope.ExpandOrCollapse1 = function () {

        if (chkEnabled2.checked == true) {
            $scope.ExpandOrCollapseBox1 = true;
        } else {
            $scope.ExpandOrCollapseBox1 = false;
        }
        if ($scope.ExpandOrCollapseBox1) {
            $("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox1 = true;
        } else {
            $("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox1 = false;
        }
    };
    $scope.ExpandOrCollapseBox1sub = false;
    $scope.ExpandOrCollapse1sub = function () {

        if (chkEnabledsub2.checked == true) {
            $scope.ExpandOrCollapseBox1sub = true;
        } else {
            $scope.ExpandOrCollapseBox1sub = false;
        }
        if ($scope.ExpandOrCollapseBox1sub) {
            $("#treeviewKendo1sub").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox1sub = true;
        } else {
            $("#treeviewKendo1sub").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox1sub = false;
        }
    };
    $scope.ExpandOrCollapseBox2 = false;
    $scope.ExpandOrCollapseBox21 = function () {

        if (chkEnabled4.checked == true) {
            $scope.ExpandOrCollapseBox2 = true;
        } else {
            $scope.ExpandOrCollapseBox2 = false;
        }
        if ($scope.ExpandOrCollapseBox2) {
            $("#treeviewKendo2").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox2 = true;
        } else {
            $("#treeviewKendo2").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox2 = false;
        }
    };

    $scope.CheckOrUncheckBox2 = false;
    $scope.CheckOrUncheck2 = function () {
        if (chkEnabled3.checked == true) {
            $scope.CheckOrUncheckBox2 = true;
        } else {
            $scope.CheckOrUncheckBox2 = false;
        }
        if ($scope.CheckOrUncheckBox2) {
            $('#treeviewKendo2 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox2 = true;
        } else {
            $('#treeviewKendo2 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox2 = false;
        }
    };

    $scope.ExpandOrCollapseBox2 = false;
    $scope.ExpandOrCollapseBox2 = function () {

        if (chkEnabled4.checked == true) {
            $scope.ExpandOrCollapseBox2 = true;
        } else {
            $scope.ExpandOrCollapseBox2 = false;
        }
        if ($scope.ExpandOrCollapseBox2) {
            $("#treeviewKendo2").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox2 = true;
        } else {
            $("#treeviewKendo2").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox2 = false;
        }
    };
    $scope.CheckOrUncheckBox3 = false;
    $scope.CheckOrUncheck3 = function () {
        if (chkEnabled5.checked == true) {
            $scope.CheckOrUncheckBox3 = true;
        } else {
            $scope.CheckOrUncheckBox3 = false;
        }
        if ($scope.CheckOrUncheckBox3) {
            $('#treeviewKendo3 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox3 = true;
        } else {
            $('#treeviewKendo3 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox3 = false;
        }
    };
    $scope.ExpandOrCollapseBox3 = false;
    $scope.ExpandOrCollapse3 = function () {
        if (chkEnabled6.checked == true) {
            $scope.ExpandOrCollapseBox3 = true;
        } else {
            $scope.ExpandOrCollapseBox3 = false;
        }
        if ($scope.ExpandOrCollapseBox3) {
            $("#treeviewKendo3").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox3 = true;
        } else {
            $("#treeviewKendo3").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox3 = false;
        }
    };
    $scope.CheckOrUncheckBox4 = false;
    $scope.CheckOrUncheck4 = function () {
        if (chkEnabled7.checked == true) {
            $scope.CheckOrUncheckBox4 = true;
        } else {
            $scope.CheckOrUncheckBox4 = false;
        }
        if ($scope.CheckOrUncheckBox4) {

            $('#treeviewKendo4 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox4 = true;
        } else {
            $('#treeviewKendo4 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox4 = false;
        }
    };
    $scope.ExpandOrCollapseBox4 = false;
    $scope.ExpandOrCollapseBox41 = function () {
        if (chkEnabled8.checked == true) {
            $scope.ExpandOrCollapseBox4 = true;
        } else {
            $scope.ExpandOrCollapseBox4 = false;
        }
        if ($scope.ExpandOrCollapseBox4) {
            $("#treeviewKendo4").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox4 = true;
        } else {
            $("#treeviewKendo4").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox4 = false;
        }
    };
    // End of Check/ UnCheck and Expand / Collapse

    //--------------------------------------------------------------------------------------------------Strat Sub Products ------------------------------------------------------
    $scope.subproductcheckeditems = "";
    $scope.subproductdatasource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {


                if ($localStorage.EnableSubProductLocal == true) {
                    dataFactory.getCategoriessubProducts($scope.SelectedCatalogId, options.data.id).success(function (response) {
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                }
                else {
                    $('#subproductfamily').hide();
                }
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });

    $scope.subproducttreeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.subproductcheckeditems = '';
            if (!e.node) {
                $scope.subproductschanges($scope.subproductdatasource);
            }
        }
    };

    $scope.subproductdatasourceAttrvalue = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                if ($localStorage.EnableSubProductLocal == true) {
                    dataFactory.getCategoriessubProducts($scope.SelectedCatalogId, options.data.id).success(function (response) {
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                }
                else {
                    $('#subproductattr').hide();
                }
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });

    $scope.subproducttreeOptionsCatalogAttrvalue = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.subproductcheckeditems = '';
            if (!e.node) {
                $scope.subproductschanges($scope.subproductdatasourceAttrvalue);
            }
        }
    };

    $scope.subproductdatasourceColumn = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                if ($localStorage.EnableSubProductLocal == true) {
                    dataFactory.getCategoriessubProducts($scope.SelectedCatalogId, options.data.id).success(function (response) {
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                }
                else {
                    $('#subproductcolumn').hide();
                }
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });

    $scope.treeOptionsCatalog_sortsub = {

        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.subproductcheckeditems = '';
            if (!e.node) {
                $scope.selectedtab = "ColumnSorter";
                $scope.subproductschanges($scope.subproductdatasourceColumn);
            }
        }
    };

    $scope.subproductschanges = function (dataSource) {
        $scope.subproductcheckeditems = '';
        // var dataSource = $scope.subproductdatasource;
        dataSource.bind("change", function (e) {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.subproductcheckedNodeIds(dataSource.view(), checkedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }

            $scope.subproductcheckeditems = checkedNodes.join(",");
        });

    };
    $scope.subproductcheckedNodeIds = function (nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].CATEGORY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.subproductcheckedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    };



    $scope.updatevalueForSelectedFamilysub = function () {

        // alert($scope.subproductcheckeditems);
        var objstrcatid = new String;
        var items = $scope.subproductcheckeditems;
        var sorting = $scope.Sort;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        var selectedFamilytreeView = $("#treeviewKendo4").data("kendoTreeView");
        selectedFamilytreeData = selectedFamilytreeView.dataSource._data;
        if ($scope.Attrselected_val_FSF !== '') {
            if ($scope.subproductcheckeditems !== '') {
                dataFactory.updateGlobalAttrValueForSelectedFamilysub($scope.seelectCatalogID, $scope.AttrtypeselectedForFamilySelected, $scope.Attrselected_val_FSF, $scope.valueForSelectedFamily, $scope.subproductcheckeditems).success(function (response) {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Update completed.',
                        type: "info"
                    });
                }).error(function (error) {
                    //   options.error(error);
                });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Family.',
                    // type: "info"
                });
            }
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select an attribute.',
                // type: "info"
            });

        }
    };


    $scope.runsortscriptsub = function (totest) {
        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrid = new String;

        objstrid = $scope.Attrselected_val;
        var objstrcatid = new String;

        var items = $scope.subproductcheckeditems;
        var sorting = $scope.Sort;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
            else if (temp[i].indexOf('#') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('#', '');
            } else {
                objstrcatid = objstrcatid + ',' + temp[i];
            }
        }
        objstrcatid = objstrcatid.substring(1);
        var objoption = new String;
        $(".chkoption").each(function () {
            if ($(this).is(':checked')) {
                objoption = objoption + ',' + $(this).val();
            }
        });
        objoption = objoption.substring(1);
        if ($scope.SelecteAttrType != '' && $scope.Attrselected_val != '' && $scope.subproductcheckeditems != '' && $scope.Sort != '') {

            dataFactory.globalattributesub(objstrid, $scope.subproductcheckeditems, $scope.seelectCatalogID, $scope.SelecteAttrType, "sortSub", sorting).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Saved successfully.',
                    type: "info"
                });

            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Save action not completed.',
                    type: "error"
                });
            });
        }
        else {
            if ($scope.SelecteAttrType == '') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Attribute Type.',
                    type: "error"
                });
            }

            else if ($scope.Attrselected_val == '') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an attribute. ',
                    type: "error"
                });
            }
            else if ($scope.Sort == '' || $scope.Sort == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a Sort Order.',
                    type: "error"
                });
            }
            else if (objstrcatid == '') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Family.',
                    type: "error"
                });
            }

        }
    };

    $scope.CheckOrUncheckBox2sub = false;
    $scope.CheckOrUncheck2sub = function () {
        if (chkEnabledsubcol3.checked == true) {
            $scope.CheckOrUncheckBox2sub = true;
        } else {
            $scope.CheckOrUncheckBox2sub = false;
        }
        if ($scope.CheckOrUncheckBox2sub) {
            $('#treeviewKendo2sub input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox2sub = true;
        } else {
            $('#treeviewKendo2sub input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox2sub = false;
        }
    };
    $scope.ExpandOrCollapseBox2sub = false;
    $scope.ExpandOrCollapse2sub = function () {
        if (chkEnabledsubcol4.checked == true) {
            $scope.ExpandOrCollapseBox2sub = true;
        } else {
            $scope.ExpandOrCollapseBox2sub = false;
        }
        if ($scope.ExpandOrCollapseBox2sub) {
            $("#treeviewKendo2sub").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox2sub = true;
        } else {
            $("#treeviewKendo2sub").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox2sub = false;
        }
    };

    $scope.ExpandOrCollapseBox2value = false;
    $scope.ExpandOrCollapse1subvalue = function () {
        if (chkEnabledsub6.checked == true) {
            $scope.ExpandOrCollapseBox2value = true;
        } else {
            $scope.ExpandOrCollapseBox2value = false;
        }
        if ($scope.ExpandOrCollapseBox2value) {
            $("#treeviewKendoattrvalue").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox2value = true;
        } else {
            $("#treeviewKendoattrvalue").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox2value = false;
        }
    };
    $scope.CheckOrUncheckBoxsubvalue = false;
    $scope.CheckOrUncheck1subvalue = function () {
        if (chkEnabledsub5.checked == true) {
            $scope.CheckOrUncheckBoxsubvalue = true;
        } else {
            $scope.CheckOrUncheckBoxsubvalue = false;
        }
        if ($scope.CheckOrUncheckBoxsubvalue) {
            $('#treeviewKendoattrvalue input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBoxsubvalue = true;
        } else {
            $('#treeviewKendoattrvalue input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBoxsubvalue = false;
        }
    };
    //-------------------------------------------------------------------------------------------------- End Sub Products ------------------------------------------------------
    //--------To get EnableSubProduct value from preference page-------
    $rootScope.getEnableSubProduct = function () {
        //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
        dataFactory.getEnableSubProduct().success(function (response) {

            $rootScope.EnableSubProduct = response;
            $localStorage.EnableSubProductLocal = response;

        })
    };

    $scope.init = function () {

        if ($localStorage.getCatalogID === undefined) {
            $scope.getCatalogId = 0;
            $scope.SelectedCatalogId = 0;
        }
        else {
            $scope.getCatalogId = $localStorage.getCatalogID;
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
            $scope.selectedcatalog($scope.SelectedCatalogId);
            $scope.selectedcatalog_sort($scope.SelectedCatalogId);
            $scope.selectedcalculatedcatalog($scope.SelectedCatalogId);

        }
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerID = 0;

        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
        }
        $rootScope.getEnableSubProduct();

    };


    $scope.Packagetypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: packageTypefulldata
        })
    };


    $scope.groupNameDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                //if ($scope.GroupeType == 1) {
                //    $scope.GroupeTypeName = "Family";
                //} else if ($scope.GroupeType == 2) {
                //    $scope.GroupeTypeName = "Product";
                //}
                dataFactory.GetdropdownGroupName($scope.SelectedCatalogId, $scope.GroupeTypeName).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.unselectallDisplay = false;
    $("#chkselectall").removeAttr('disabled');
    $("#addGroup").text("Add selected attributes");
    $("#removeGroup").text("Remove selected attributes");

    $scope.groupNameChange = function (e) {
        if (e.sender.text() == "--- Select Group ---") {
            $scope.SelectedGroupId = 0;
        } else {
            $scope.SelectedGroupId = e.sender.value();
        }
        $scope.selectedAttributetype = 14;
        if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
            $("#chkid1").prop("checked", true);
            $scope.IsCheck = true;
        }
        else {
            $("#chkid1").prop("checked", false);
            $scope.IsCheck = false;
        }
        $http.get("../Wizard/GetallAttributesByGroupId?groupId=" + $scope.SelectedGroupId + "&catalogId=" + $scope.SelectedCatalogId + "&packageType=" + $scope.GroupeTypeName).then(function (attributes) {
            if (attributes != null && attributes != undefined && attributes.data != null && attributes.data != undefined && attributes.data.length > 0) {
                $scope.entities = attributes.data;
                $("#checkboxpadding").addClass("disableBox");
                $(".isAttributePack").show();
                $(".isNotAttributePack").hide();
                $scope.unselectallDisplay = true;
                $("#chkselectall").attr('disabled', 'disabled');
                $("#addGroup").text("Add Group");
                $("#removeGroup").text("Remove Group");
            }
            else
                $scope.entities = [];
        });
    };

    $scope.checkAllfunc = function () {
        $scope.selectall('a');
    };

    $scope.ChangeGroupType = function (e) {
        $scope.SelectedGroupId = 0;
        $http.get("../Wizard/GetallAttributesByGroupId?groupId=" + $scope.SelectedGroupId + "&catalogId=" + $scope.SelectedCatalogId + "&packageType=" + $scope.GroupeTypeName).then(function (attributes) {
            if (attributes != null) {
                $scope.entities = attributes.data;
                $scope.groupNameDataSource.read();
            }
            else {
                $scope.entities = [];
                $scope.groupNameDataSource.read();
            }
        });
    }


    $scope.init();


}]);
