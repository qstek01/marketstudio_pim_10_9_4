﻿
LSApp.controller('ProductTableManager', ['$scope', 'dataFactory', '$http', '$compile', '$localStorage', '$rootScope', function ($scope, dataFactory, $http, $compile, $localStorage, $rootScope) {

   $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;


    $('#PTMAfterTab').hide();
    $scope.ProductTableHeader = "1";
    $scope.HorizontalTable = "0";
    $scope.VerticalTable = "0";
    $scope.SuperTable = "0";
    $scope.table = "TurnOnOff";
    $('#horizontalid').hide();
    $('#verticalid').hide();
    $('#Familydet').hide();
    $scope.TurnHeaderOnOff = "true";

    $scope.refFid = 0;
    $scope.selectedfamilyid = 0;
    //$scope.TurnHeaderOff = "0";
    $scope.NextToPTM = function () {

        $('#PTMBeforeTab').hide();
        $('#PTMAfterTab').show();
    };
    
    $scope.TurnHeaderOn = function () {
        $scope.TurnHeaderOnOff = "true";
    };
    $scope.TurnHeaderOff = function () {
        $scope.TurnHeaderOnOff = "false";
    };
    $scope.TurnOnOffProductTableHeader = function () {
        $scope.table = "TurnOnOff";
        $scope.ProductTableHeader = "1";
        $scope.HorizontalTable = "0";
        $scope.VerticalTable = "0";
        $scope.SuperTable = "0";
        $('#horizontalid').hide();
        $('#turnonoffid').show();
        $('#verticalid').hide();
        $('#Familydet').hide();

    };
    $scope.CreateConfigureVerticalTable = function () {
        $scope.table = "Vertical";
        $scope.ProductTableHeader = "0";
        $scope.HorizontalTable = "0";
        $scope.VerticalTable = "1";
        $scope.SuperTable = "0";
        $('#turnonoffid').hide();
        $('#horizontalid').hide();
        $('#verticalid').show();
        $('#Familydet').hide();
    };
    $scope.CreateConfigureHorizontalTable = function () {
        $scope.table = "Horizontal";
        $scope.ProductTableHeader = "0";
        $scope.HorizontalTable = "1";
        $scope.VerticalTable = "0";
        $scope.SuperTable = "0";
        $('#turnonoffid').hide();
        $('#verticalid').hide();
        $('#horizontalid').show();
        $('#Familydet').hide();
    };
    $scope.CreateConfigureSuperTable = function () {
        $scope.table = "Pivot";
        $scope.ProductTableHeader = "0";
        $scope.HorizontalTable = "0";
        $scope.VerticalTable = "0";
        $scope.SuperTable = "1";
        $('#turnonoffid').hide();
        $('#Familydet').show();
        $('#verticalid').hide();

    };

    //$scope.catalogDataSourcePTM = new kendo.data.DataSource({
    //    type: "json",
    //    serverFiltering: true,
    //    transport: {
    //        read: function (options) {
    //            dataFactory.GetCatalogDetails($scope.SelectedItem, $scope.getCustomerID).success(function (response) {
    //                options.success(response);
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }
    //});

    $scope.familyDataSourcePTM = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetFamilyDetailsForProductconfig($scope.catalogId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    //$scope.catalogtypeChangePTM = function (e) {
    //    $scope.selectedcatalog(e.sender.value());

    //};
    $scope.familytypeChangePTM = function (e) {

        $scope.selectedfamilyid = e.sender.value();
        $scope.table = "TurnOnOffOn"

    };
    // $scope.catalogId = 0;
    $scope.selectedcatalog = function (catalogId) {
        // $scope.catalogId = catalogId;
        $scope.selectedcatalog1.read();
        $scope.familyDataSourcePTM.read();
    };
    $scope.selectedcatalog1 = new kendo.data.HierarchicalDataSource({
        type: "json", loadOnDemand: false,
        transport: {
            read: function (options) {
                dataFactory.getCategories($scope.catalogId, options.data.id).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        }
    });
    $scope.CheckOrUncheckBox1 = false;

    $scope.CheckOrUncheck1 = function () {

        if ($scope.CheckOrUncheckBox1) {
            $('#treeviewKendo1 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox1 = true;
        } else {
            $('#treeviewKendo1 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox1 = false;
        }

    };


    $scope.CheckOrUncheckBoxApplyFTS = false;
    $scope.CheckOrUncheckApplyFTS = function () {

        if ($scope.CheckOrUncheckBoxApplyFTS) {
            $scope.CheckOrUncheckBoxApplyFTS = true;
        } else {
            $scope.CheckOrUncheckBoxApplyFTS = false;
        }
    };

    $scope.ExpandOrCollapseBox1 = false;
    $scope.ExpandOrCollapse1 = function () {

        if ($scope.ExpandOrCollapseBox1) {
            $("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox1 = true;

        } else {
            $("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox1 = false;
        }
    };
    $scope.pageBack = function () {
        $('#PTMBeforeTab').show();
        $('#PTMAfterTab').hide();
    };
    $scope.runfinishscript = function () {
        $scope.attachChangeEvent();
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        //var temp = new Array();
        //temp = items.split(',');
        //for (var i = 0; i < temp.length; i++) {
        //    if (temp[i].indexOf('~') == 0) {
        //        objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
        //    }
        //}
        //objstrcatid = objstrcatid.substring(1);
        if (objstrcatid.trim() === "") {
            objstrcatid = items;
        }
        if ($scope.table == "TurnOnOff") {

            $http.get("../Wizard/UpdatePtHeader?catalogId=" + $scope.catalogId + "&familyIds=" + objstrcatid + "&header=" + $scope.TurnHeaderOnOff + "&items=" + items)
               .success(function (response) {
                   if (response != null) {                  
                       $.msgBox({
                           title: $localStorage.ProdcutTitle,
                           content: '' + response + '.',
                           type: "info"
                       });
                   };
               });
        }
        else {
            if ($scope.selectedfamilyid === 0) {

                if (($scope.table === 'Vertical' && $scope.CheckOrUncheckBox2 === true) || ($scope.table === 'Pivot' && $scope.CheckOrUncheckBoxApplyFTS === true) || ($scope.table === 'Horizontal' && $scope.CheckOrUncheckBox2 === true)) {
                    $http.get("../Wizard/UpdatePtstructure?familyId=" + objstrcatid + "&ttype=" + $scope.table + "&refFid=" + $scope.refFid + "&items=" + items + "&catalogId=" + $scope.catalogId)
                       .success(function (response) {
                           if (response != null) {  
                               $.msgBox({
                                   title: $localStorage.ProdcutTitle,
                                   content: '' + response + '.',
                                   type: "info"
                               });
                           };
                       });
                }
            }
            else {

                $http.get("../Wizard/UpdatePtstructurepivot?familyId=" + $scope.selectedfamilyid + "&ttype=" + $scope.table + "&refFid=" + $scope.refFid + "&items=" + items + "&catalogId=" + $scope.catalogId)
                   .success(function (response) {
                       if (response != null) {   
                           $.msgBox({
                               title: $localStorage.ProdcutTitle,
                               content: '' + response + '.',
                               type: "info"
                           });
                       };
                   });
            }
        }
    };
    //----------
    $scope.treeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            //console.log(e);
            if (!e.node) {
                $scope.selectedtab = "AttributeAssociation";
                $scope.attachChangeEvent();
            }
        }
    };

    $scope.attachChangeEvent = function () {
        var dataSource = $scope.selectedcatalog1;
        dataSource.bind("change", function (e) {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.checkedNodeIds(dataSource.view(), checkedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {

                    selectedNodes++;
                }
            }
            //console.log(selectedNodes);
            $scope.checkeditems = checkedNodes.join(",");

        });
    };
    $scope.checkedNodeIds = function (nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].CATEGORY_ID);

            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    };

    $scope.PreviewTextPTM = '';
    $scope.previewFamilySpecsPTM = function () {

        dataFactory.GetCsFamilyPreviewListPTM($scope.selectedfamilyid, "0", $scope.catalogId, $rootScope.FamilyLevelMultipletablePreview).success(function (response) {
            if (response != null) {

                $scope.PreviewTextPTM = response;
            }
        }).error(function (error) {
            options.error(error);
        });



    };


    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $scope.catalogId = 0;
        }
        else {
            $scope.catalogId = $localStorage.getCatalogID;
            $scope.selectedcatalog($scope.catalogId);
        }
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerID = 0;

        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
        }
    };
    $scope.init();
}]);
