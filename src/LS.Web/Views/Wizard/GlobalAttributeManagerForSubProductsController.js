﻿
LSApp.controller('GlobalAttributeManagerForSubProductsController', ['$scope', 'dataFactory', '$http', '$compile', '$localStorage', function ($scope, dataFactory, $http, $compile, $localStorage) {

    $scope.AttrtypeChange = function (e) {
        $scope.selectcatalogattr(e.sender.value());
    };
    $scope.SelecteAttrType = '';
    $scope.AttrtypeChangetab = function (e) {
        $scope.selectattributes(e.sender.value());
        $scope.SelecteAttrType = e.sender.value();
    };
    $scope.AttrtypeChangeForSelectedFamily = function (e) {

        $scope.selectattributesForSelectedFamily(e.sender.value());
        $scope.SelecteAttrTypeForSelectedFamily = e.sender.value();
    };


    $scope.Attrtypeselected = 0;
    $scope.AttrtypeselectedForFamilySelected = 0;
    $scope.selectattributes = function (attributetype) {

        $scope.Attrtypeselected = attributetype;
        $scope.selectedAttribute.read();
    };
    $scope.selectattributesForSelectedFamily = function (attributetypeFSF) {

        $scope.AttrtypeselectedForFamilySelected = attributetypeFSF;
        $scope.selectedAttributeForSelectedFamily.read();
    };
    $scope.selectedAttribute = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetAttributeDetailsForGlobal($scope.Attrtypeselected).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }
        }
    });
    $scope.updatevalueForSelectedFamily = function () {

        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var sorting = $scope.Sort;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        var selectedFamilytreeView = $("#treeviewKendo4").data("kendoTreeView");
        selectedFamilytreeData = selectedFamilytreeView.dataSource._data;
        if ($scope.Attrselected_val_FSF !== '') {
            if ($scope.checkeditems !== '') {
                dataFactory.updateGlobalAttrValueForSelectedFamily($scope.seelectCatalogID, $scope.AttrtypeselectedForFamilySelected, $scope.Attrselected_val_FSF, $scope.valueForSelectedFamily, $scope.checkeditems).success(function (response) {
                    //alert('Updated successfully');
                    $.msgBox({
                        title: "CatalogStudio10",
                        content: 'Updated successfully',
                        type: "info"
                    });

                }).error(function (error) {
                    //   options.error(error);
                });
            } else {
               // alert("Please select the Family");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select the Family',
                       // type: "info"
                    });
            }
        } else {
            //alert("Please select the attribute.");
            $.msgBox({
                title: "CatalogStudio10",
                content: 'Please select the attribute.',
                       // type: "info"
                    });
        }
    };

    $scope.selectedAttributeForSelectedFamily = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {

                dataFactory.GetAttributeDetailsForGlobal($scope.AttrtypeselectedForFamilySelected).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
   

    $scope.seelectCatalogID = 0;
    $scope.selectedcatalog = function (catalogId) {
        $scope.seelectCatalogID = catalogId;
        $scope.selectedcatalog1.read();
    };
    $scope.selectedcatalog_sort = function (catalogId) {
        $scope.seelectCatalogID = catalogId;
        $scope.selectedcatalog1_sort.read();
    };
    $scope.selectedcalculatedcatalog = function (catalogId) {
        $scope.seelectCatalogID = catalogId;
        $scope.selectedCalculatedcatalog1.read();
    };
    $scope.Attrselected_val = '';
    $scope.Attrselected_val_FSF = '';
    $scope.Attrselect = function (e) {
        $scope.Attrselected_val = e.sender.value();
    };
    $scope.AttrselectForSelectedFamily = function (e) {
        $scope.Attrselected_val_FSF = e.sender.value();
    };
    $scope.selectedcatalog1 = new kendo.data.HierarchicalDataSource({        
        type: "json",
        loadOnDemand: false,
              transport: {
            
                  read: function (options) {
                    
                dataFactory.getCategories($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);
                    //$("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
                    //$("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });

    $scope.globalsubprod = new kendo.data.HierarchicalDataSource({
       
        type: "json",
        loadOnDemand: true, autoBind: false,
        transport: {
            read: function (options) {
               
                //if ($scope.Family.FAMILY_ID == "") {
                //    $scope.familyids = 0;

                //} else {
                //    $scope.familyids = $scope.Family.FAMILY_ID;
                //}
                //alert($scope.familyids)
                dataFactory.GetAllglobslsubproducts($localStorage.CategoryID, $localStorage.getCatalogID, options.data.id).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });

            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        }
    });

    $scope.selectedcatalog1_sort = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                dataFactory.getCategories($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);
                    //$("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
                    //$("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });

    $scope.selectedCalculatedcatalog1 = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                dataFactory.getCategories($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        }
    });

    $scope.selectedcatalog_ForSelectedFamily = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                dataFactory.getCategories($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);
                    //$("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
                    //$("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });


    $scope.selectedAttributetype = 0;
    $scope.selectcatalogattr = function (Attributeid) {
        $scope.selectedAttributetype = Attributeid;
        $http.get("../Wizard/GetAllattributes?Attributeid=" + Attributeid).
            then(function (attributes) {
                $scope.entities = attributes.data;
                // $scope.selected = [];
                // $scope.removed = [];
                //$scope.newselected = [];
                //$scope.selectedvalue();

            });
    };

    $scope.selectAll = function ($event) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        for (var i = 0; i < $scope.entities.length; i++) {
            var entity = $scope.entities[i];
            updateSelected(action, entity.ATTRIBUTE_ID);
        }
    };
    $scope.selectall = function (name) {
        $(".chk").each(function () {
            $(this).prop("checked", true);
        });
    };
    $scope.check = function (selct) {
        if (document.getElementById('selattr').checked === true) {
            $("#remattr").prop("checked", false);
            document.getElementById("remattr").disabled = true;
            $("#rempub").prop("checked", false);
            document.getElementById("rempub").disabled = true;
            $("#addpub").prop("checked", false);
            document.getElementById("addpub").disabled = false;
        } else {
            document.getElementById("remattr").disabled = false;
            document.getElementById("rempub").disabled = false;
        }
    };

    $scope.remattrcheck = function (selct) {
        if (document.getElementById('remattr').checked === true) {
            $("#rempub").prop("checked", false);
            document.getElementById("rempub").disabled = true;
            $("#addpub").prop("checked", false);
            document.getElementById("addpub").disabled = true;
            $("#selattr").prop("checked", false);
        } else {
            document.getElementById("rempub").disabled = false;
            document.getElementById("addpub").disabled = false;
        }
    };

    $scope.rempubcheck = function (select) {
        if (document.getElementById('rempub').checked === true) {
            $("#addpub").prop("checked", false);
        }
    };

    $scope.addpubcheck = function (select) {
        if (document.getElementById('addpub').checked === true) {
            $("#rempub").prop("checked", false);
        }
    };

    $scope.unselectall = function (name) {
        $(".chk").each(function () {
            $(this).prop("checked", false);
        });
    };
    $("#tabselect li").click(function () {
        $scope.checkeditems = '';
    });
    $scope.Sort = "";
    $scope.valueForSelectedFamily = "";
    $scope.runsortscript = function (totest) {
        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrid = new String;

        objstrid = $scope.Attrselected_val;
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var sorting = $scope.Sort;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        var objoption = new String;
        $(".chkoption").each(function () {
            if ($(this).is(':checked')) {
                objoption = objoption + ',' + $(this).val();
            }
        });
        objoption = objoption.substring(1);
        if ($scope.SelecteAttrType != '' && $scope.Attrselected_val != '' && $scope.checkeditems != ''&& $scope.Sort != '') {
            //$http.get("../Wizard/globalattribute?Attrid=" + objstrid + "&&familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&Attributetype=" + $scope.SelecteAttrType + "&&option=sort &&sort=" + sorting)
            //$http.post("../Wizard/globalattribute?Attrid=" + objstrid + "&&familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&Attributetype=" + $scope.SelecteAttrType + "&&option=sort &&sort=" + sorting, JSON.stringify(objstrcatid))
            //    .success(function (response) {
            //        if (response != null) {
            //            alert(response);
            //        };
            //    });
            dataFactory.globalattribute(objstrid, $scope.checkeditems, $scope.seelectCatalogID, $scope.SelecteAttrType, "sort", sorting).success(function (response) {
               // alert('Saved successfully');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Saved successfully',
                    type: "info"
                });

            }).error(function (error) {
               // alert('Not Saved');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Not Saved.',
                    type: "error"
                });
            });
        }
        else {
            if ($scope.SelecteAttrType == '') {
                //alert('Please select attribute type');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select attribute type',
                    type: "error"
                });
            }

            else if ($scope.Attrselected_val == '') {
               // alert('Please select attribute ');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select attribute',
                    type: "error"
                });
            }
            else if ($scope.Sort == '' || $scope.Sort == undefined) {
               // alert("Please enter sort order");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please enter sort order',
                    type: "error"
                });
            }
            else if (objstrcatid == '') {
               // alert('Please select family ');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select family',
                    type: "error"
                });
            }
            
        }
    };

    $scope.runscript = function (totest) {
        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrid = new String;

        $(".chk").each(function () {
            if ($(this).is(':checked')) {
                objstrid = objstrid + $(this).val() + ',';
                cnt = cnt + 1;
                flg = 1;
            }
        });
        $scope.chkattrlist = objstrid;
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        var objoption = new String;
        $(".chkoption").each(function () {
            if ($(this).is(':checked')) {
                objoption = objoption + ',' + $(this).val();
            }
        });
        objoption = objoption.substring(1);
        $scope.attroption = objoption;
        var sort = 0;
        if ($scope.chkattrlist != '' && $scope.chkattrlist != undefined && $scope.attroption != '' && $scope.checkeditems != '') {
            //$http.get("../Wizard/globalattribute?Attrid=" + objstrid + "&&familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&Attributetype=" + $scope.selectedAttributetype + "&&option=" + objoption + "&&sort=" + sort)
            //    .success(function (response) {
            //        if (response != null) {
            //            alert(response);
            //        };
            //    });
            dataFactory.globalattribute(objstrid, $scope.checkeditems, $scope.seelectCatalogID, $scope.selectedAttributetype, objoption, sort).success(function (response) {
              //  alert('Saved successfully');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Saved successfully.',
                    type: "info"
                });

            }).error(function (error) {
               // alert('Not Saved');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Not Saved.',
                    type: "error"
                });
            });
        }
        else {
            if ($scope.selectedAttributetype == '' || $scope.selectedAttributetype == '--- Select Attribute type ---') {
                //alert("Please select attribute type");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select attribute type.',
                    type: "error"
                });
            }
            else if ($scope.chkattrlist == '' || $scope.chkattrlist == undefined) {
               // alert("Please select atleast one attribute");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select atleast one attribute.',
                    type: "error"
                });
            }
            else if ($scope.attroption == '' || $scope.attroption == undefined) {
               // alert("Please select Option");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select Option.',
                    type: "error"
                });
            }
            else if ($scope.attrfamilyIds == '' || $scope.attrfamilyIds == undefined) {
               // alert("Please select Product Family");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select Product Family',
                    type: "error"
                });
            }
            
        }
    };


    $scope.runscriptsubprod = function (totest) {
        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrid = new String;

        $(".chk").each(function () {
            if ($(this).is(':checked')) {
                objstrid = objstrid + $(this).val() + ',';
                cnt = cnt + 1;
                flg = 1;
            }
        });
        $scope.chkattrlist = objstrid;
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        var objoption = new String;
        $(".chkoption").each(function () {
            if ($(this).is(':checked')) {
                objoption = objoption + ',' + $(this).val();
            }
        });
        objoption = objoption.substring(1);
        $scope.attroption = objoption;
        var sort = 0;
        if ($scope.chkattrlist != '' && $scope.chkattrlist != undefined && $scope.attroption != '' && $scope.checkeditems != '') {
            //$http.get("../Wizard/globalattribute?Attrid=" + objstrid + "&&familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&Attributetype=" + $scope.selectedAttributetype + "&&option=" + objoption + "&&sort=" + sort)
            //    .success(function (response) {
            //        if (response != null) {
            //            alert(response);
            //        };
            //    });
            dataFactory.globalattributesub(objstrid, $scope.checkeditems, $scope.seelectCatalogID, $scope.selectedAttributetype, objoption, sort).success(function (response) {
               // alert('Saved successfully');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Saved successfully.',
                    type: "info"
                });

            }).error(function (error) {
               // alert('Not Saved');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Not Saved.',
                    type: "error"
                });
            });
        }
        else {
            if ($scope.selectedAttributetype == '' || $scope.selectedAttributetype == '--- Select Attribute type ---') {
                //alert("Please select attribute type");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select attribute type.',
                    type: "error"
                });
            }
            else if ($scope.chkattrlist == '' || $scope.chkattrlist == undefined) {
              //  alert("Please select atleast one attribute");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select atleast one attribute.',
                    type: "error"
                });
            }
            else if ($scope.attroption == '' || $scope.attroption == undefined) {
               // alert("Please select Option");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select Option.',
                    type: "error"
                });
            }
            else if ($scope.attrfamilyIds == '' || $scope.attrfamilyIds == undefined) {
               // alert("Please select Product Family");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select Product Family',
                    type: "error"
                });
            }

        }
    };


    $scope.runcalculatedscript = function (CalculatedAttributes) {
        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        if ($scope.checkeditems != '') {
            //$http.get("../Wizard/globalCalculatedAttribute?familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&option=CALC")
            //    .success(function (response) {
            //        if (response != null) {
            //            alert(response);
            //        };
            //    });
            dataFactory.globalCalculatedAttribute($scope.checkeditems, $scope.seelectCatalogID, "CALC").success(function (response) {
                //alert('Saved successfully');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Saved successfully.',
                    type: "info"
                });

            }).error(function (error) {
                //alert('Not Saved');
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Not Saved.',
                    type: "error"
                });
            });
        }
        else {
            if ($scope.checkeditems == '' || $scope.checkeditems == undefined) {
               // alert("Please select Product Family");
                $.msgBox({
                    title: "CatalogStudio10",
                    content: 'Please select Product Family',
                    type: "error"
                });
            }
        }
    };
    $scope.pagereload = function (name) {
        location.reload();
    };
    $scope.getSelectedClass = function (entity) {
        return $scope.isSelected(entity.id) ? 'selected' : '';
    };
    $scope.isSelected = function (id) {

        return $scope.selected.indexOf(id) >= 0;
    };

    $scope.CatalogtreeChange = new kendo.data.HierarchicalDataSource({
        type: "json",
        transport: {
            read: function (options) {
                dataFactory.getCategories(options.data.id).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        }
    });

    $scope.AttributeDataSourcesub = [{id: 1, DATA_TYPE: "Product Specifications"},{ id: 3, DATA_TYPE: "Product Image / Attachment" },{ id: 4, DATA_TYPE: "Product Price" },
  { id: 6, DATA_TYPE: "Product key" }];

    // --------------------------------------------------- End Sort Tab--------------------------------------------------------------------------------
    $scope.init = function () {

    };
    $scope.CheckAttribute = function (id) {
    };
    $scope.selectedtab = "";
    $scope.treeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            console.log(e);
            $scope.checkeditems = '';
            if (!e.node) {
                $scope.selectedtab = "AttributeAssociation";
                $scope.attachChangeEvent();
            }
        }
    };
    $scope.treeOptionsCatalog_sort = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.checkeditems = '';
            console.log(e);
            if (!e.node) {
                $scope.selectedtab = "ColumnSorter";
                $scope.attachChangeEvent();
            }
        }
    };
    $scope.treeOptionsCalculatedCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.checkeditems = '';
            console.log(e);
            if (!e.node) {
                $scope.selectedtab = "CalculatedAttribute";
                $scope.attachChangeEvent();
            }
        }
    };
    $scope.treeOptionsCatalog_ForSelectedFamily = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.checkeditems = '';
            console.log(e);
            if (!e.node) {
                $scope.selectedtab = "SelectedFamilyValue";
                $scope.attachChangeEvent();
            }
        }
    };
    $scope.attachChangeEvent = function () {
        if ($scope.selectedtab != "" && $scope.selectedtab == "AttributeAssociation") {
            var dataSource = $scope.selectedcatalog1;
        }
        else if ($scope.selectedtab != "" && $scope.selectedtab == "ColumnSorter") {
            var dataSource = $scope.selectedcatalog1_sort;
        }
        else if ($scope.selectedtab != "" && $scope.selectedtab == "SelectedFamilyValue") {
            var dataSource = $scope.selectedcatalog_ForSelectedFamily;
        }
        else {
            var dataSource = $scope.selectedCalculatedcatalog1;
        }
        $scope.checkeditems = '';
        dataSource.bind("change", function (e) {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.checkedNodeIds(dataSource.view(), checkedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            console.log(selectedNodes);
            $scope.checkeditems = checkedNodes.join(",");
        });
    };
    $scope.checkedNodeIds = function (nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].CATEGORY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    };

    $scope.selectAllCat = function () {
        $('#treeviewKendo1 input[type="checkbox"]').prop('checked', true);
    };
    $scope.unselectallcat = function () {
        $('#treeviewKendo1 input[type="checkbox"]').prop('checked', false);
    };

    $scope.selectallcat1 = function () {
        $('#treeviewKendo1 input[type="checkbox"]').prop('checked', true);
    };
    $scope.unselectallcat1 = function () {
        $('#treeviewKendo2 input[type="checkbox"]').prop('checked', false);
    };

    // Start of Check/ UnCheck and Expand / Collapse
    $scope.CheckOrUncheckBox1 = false;

    $scope.CheckOrUncheck1 = function () {
        if ($scope.CheckOrUncheckBox1) {
            $('#treeviewKendo1 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox1 = true;
        } else {
            $('#treeviewKendo1 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox1 = false;
        }
    };

    $scope.ExpandOrCollapseBox1 = false;
    $scope.ExpandOrCollapse1 = function () {
        if ($scope.ExpandOrCollapseBox1) {
            $("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox1 = true;
        } else {
            $("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox1 = false;
        }
    };
    $scope.CheckOrUncheckBox2 = false;
    $scope.CheckOrUncheck2 = function () {
        if ($scope.CheckOrUncheckBox2) {
            $('#treeviewKendo2 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox2 = true;
        } else {
            $('#treeviewKendo2 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox2 = false;
        }
    };
    $scope.ExpandOrCollapseBox2 = false;
    $scope.ExpandOrCollapseBox2 = function () {
        if ($scope.ExpandOrCollapseBox2) {
            $("#treeviewKendo2").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox2 = true;
        } else {
            $("#treeviewKendo2").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox2 = false;
        }
    };
    $scope.CheckOrUncheckBox3 = false;
    $scope.CheckOrUncheck3 = function () {
        if ($scope.CheckOrUncheckBox3) {
            $('#treeviewKendo3 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox3 = true;
        } else {
            $('#treeviewKendo3 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox3 = false;
        }
    };
    $scope.ExpandOrCollapseBox3 = false;
    $scope.ExpandOrCollapse3 = function () {
        if ($scope.ExpandOrCollapseBox3) {
            $("#treeviewKendo3").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox3 = true;
        } else {
            $("#treeviewKendo3").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox3 = false;
        }
    };
    $scope.CheckOrUncheckBox4 = false;
    $scope.CheckOrUncheck4 = function () {
        if ($scope.CheckOrUncheckBox4) {
            $('#treeviewKendo4 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox4 = true;
        } else {
            $('#treeviewKendo4 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox4 = false;
        }
    };
    $scope.ExpandOrCollapseBox4 = false;
    $scope.ExpandOrCollapseBox4 = function () {
        if ($scope.ExpandOrCollapseBox4) {
            $("#treeviewKendo4").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox4 = true;
        } else {
            $("#treeviewKendo4").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox4 = false;
        }
    };
    // End of Check/ UnCheck and Expand / Collapse

    $scope.init = function () {

        if ($localStorage.getCatalogID === undefined) {
            $scope.getCatalogId = 0;
            $scope.SelectedCatalogId = 0;
        }
        else {
            $scope.getCatalogId = $localStorage.getCatalogID;
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
            $scope.selectedcatalog($scope.SelectedCatalogId);
            $scope.selectedcatalog_sort($scope.SelectedCatalogId);
            $scope.selectedcalculatedcatalog($scope.SelectedCatalogId);

        }
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerID = 0;

        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
        }

    };

    $scope.init();
}]);
