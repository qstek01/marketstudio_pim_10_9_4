﻿
LSApp.controller("ImageManagementController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$rootScope', '$localStorage', '$http', '$filter', '$q','$sce','$timeout',
function ($scope, $window, $location, $routeParams, dataFactory, $rootScope, $localStorage, $http, $filter, $q, $sce, $timeout) {
    // --------------------------------------------------- Start Common Function --------------------------------------------------------------------------------
    // $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    $scope.ImagePreview = "";
    $scope.IsCreate = "";
    $scope.IsFileType = "";
    $scope.IsEnabled = false;
    $scope.managenew = false;
    $scope.displaymanagepaste = false;
    $scope.IsDisplay = true;
    $scope.IsEmpty = false;
    $scope.IsThumbnail = true;
    $("#newFolderPopUp").hide();
    $("#renameFolderPopUp").hide();
    $("#backbtn").hide();
    $("#copymanagebtn").hide();
    $("#cutmangebtn").hide();
    $("#compressmanagebtn").hide();
    $("#deletemanagebtn").hide();
    $("#renamemanagebtn").hide();
    $("#exportmanagebtn").hide();
    $("#downloadmanagebtn").hide();
    $scope.selectedRows = [];
    $rootScope.selectedRows = [];
    $scope.bradcrumbList = [];

    $scope.linkValues = []; //youtube link values for normal
    $scope.searchlinkValues = []; //youTube link values for search

    $scope.getIsSearchResult = false;
    //$scope.toggleGrid = function () {
    //    $scope.IsDisplay = true;
    //    $('[id^=mainGrid]').css('width', '31%');
    //    $('[id^=mainGrid]').css('height', '125px');
    //    $('[id^=foldername]').css('width', '92%');
    //}
    //$scope.toggleList = function () {
    //    $scope.IsDisplay = false;
    //}
    $scope.toggleGrid = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $('[id^=mainGrid]').css('width', '31%');
        $('[id^=mainGrid]').css('height', '125px');
        $('[id^=foldername]').css('width', '92%');
    }
    $scope.toggleList = function () {
        $scope.IsThumbnail = true;
        $scope.IsDisplay = false;

    }
    if ($localStorage.getCatalogID === undefined) {
        $scope.catalogId = 0;
        $scope.getCustomerIDs = 0;
    } else {

        $scope.catalogId = $localStorage.getCatalogID;
        $scope.getCustomerIDs = $localStorage.getCustomerID;

        dataFactory.GetImageFiles($scope.catalogId, $scope.getCustomerIDs, "", $scope.getCustomerCompanyName).success(function (response) {
            if (response != null && response.length > 0) {
                $scope.getImageData = response;
                //--------------------------------Youtube seperation loop-----------------------------------//x
                angular.forEach($scope.getImageData, function (file) {
                    if (file.FolderName.startsWith("YouTube_")) {
                        var fileDetail = file.FolderName.split('_');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.linkValues.push({ FolderName: file.FolderName, Link: convertedLink, Extention: "YouTube Link...", ModifiedDate: file.ModifiedDate, Size: "-", Path: file.Path, Caption: captionVal, CustomerFolder: file.CustomerFolder, Customer: file.Customer, ExternalDriveFlag: file.ExternalDriveFlag });
                    }
                });
                $scope.getImageData = jQuery.grep($scope.getImageData, function (value) {
                    return !(value.FolderName.startsWith("YouTube_"));
                });
                //$scope.getImageData = selectedList;
                //--------------------------------Youtube seperation loop-----------------------------------//
                $rootScope.LoadImageData = $scope.getImageData;
                if ($scope.getImageData.length > 0) {
                    $scope.getCustomerFolder = $scope.getImageData[0].CustomerFolder;
                    $scope.cusFolder = $scope.getImageData[0].Customer;
                    $scope.extDriveFlag = $scope.getImageData[0].ExternalDriveFlag;
                }
                else {
                    $scope.getCustomerFolder = $scope.linkValues[0].CustomerFolder;
                    $scope.cusFolder = $scope.linkValues[0].Customer;
                    $scope.extDriveFlag = $scope.linkValues[0].ExternalDriveFlag;
                }
                $scope.newFolderPath = $scope.getCustomerFolder;
                $scope.previosPath = $scope.getCustomerFolder;
                $scope.IsEmpty = false;
                $scope.bradcrumbList.push({ FolderName: $scope.cusFolder, Path: $scope.getCustomerFolder });
                //-----Sorting
                $scope.propertyName = 'FolderName';
                $scope.reverse = true;
                $scope.getImageData = $filter('orderBy')($rootScope.LoadImageData, $scope.propertyName, $scope.reverse);
                //-----End
            }
            else {
                $scope.IsEmpty = true;
            }
        });
    }
    $scope.sortBy = function (propertyName) {
      
        $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
            ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
        $scope.getImageData = $filter('orderBy')($rootScope.LoadImageData, $scope.propertyName, $scope.reverse);

        if($scope.getSearchResult.length > 0)
        {
            $scope.getSearchResult = $filter('orderBy')($scope.getSearchResult, $scope.propertyName, $scope.reverse);
        }

    };
    $("#managefile1").kendoUpload({
        async: {
            saveUrl: '/ImageManagement/SaveImageManagementFile',
            // removeUrl: '/ImageManagement/RemoveImageManagementFile',
            autoUpload: false,
            multiple: true,
            allowmultiple: true,
            batch: false
        },
        upload: function (e) {
            e.data = { customerFolder: $scope.getCustomerFolder };
            $scope.allFilesLoad();
        },
        //remove: function (e) {
        //    e.data = { customerFolder: $scope.getCustomerFolder };
        //    $scope.allFilesLoad();
        //}
    });

    $scope.breadCrumbClick = function (e) {
        $scope.getIsSearchResult = false;
        //var fileName = e.replace(/\\$/, '').split('\\').pop();
        var fileName = '';
        if ($scope.extDriveFlag == 'false')
            fileName = e.replace(/\\$/, '').split('\\').pop();
        else {
            fileName = e.replace(/\\$/, '').split('//').pop();
            fileName = fileName.replace(/\\$/, '').split(/\\/).pop();
        }
        if ($scope.bradcrumbList.length != 1) {
            for (var i = $scope.bradcrumbList.length - 1; i >= 0; i--) {
                if ($scope.bradcrumbList[i].Path == e && $scope.bradcrumbList[i].FolderName == fileName) {
                    $scope.bradcrumbList.splice(i, $scope.bradcrumbList.length);

                }
                else if ($scope.bradcrumbList[i].Path == e && $scope.bradcrumbList[i].FolderName == "File") {
                    $scope.bradcrumbList.splice(i, $scope.bradcrumbList.length);
                }
            }
        }
        else if ($scope.bradcrumbList.length == 1 && fileName == $scope.cusFolder) {
            $scope.bradcrumbList = [];
            $scope.getIsSearchResult = false;
        }
        $scope.fileClick(e);
    }
    $scope.fileClick = function (e) {
        //alert(e);
        $("#backbtn").show();
        $scope.getCustomerFolder = e;
        //var fileName = e.replace(/\\$/, '').split('\\').pop();
        var fileName = '';
        if ($scope.extDriveFlag == 'false')
            fileName = e.replace(/\\$/, '').split('\\').pop();
        else {
            fileName = e.replace(/\\$/, '').split('//').pop();
            fileName = fileName.replace(/\\$/, '').split(/\\/).pop();
        }
        $scope.bradcrumbList.push(
               {
                   FolderName: fileName,
                   Path: e
               }
               );
        //if ($scope.bradcrumbList != null && $scope.bradcrumbList.length > 0) {
        //    if ($scope.bradcrumbList.length == 1) {



        //    }

        //}
        //else {
        //    $scope.bradcrumbList.push({ FolderName: "File", Path: $scope.getCustomerFolder });
        //}

        $scope.allFilesLoad();

        $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").removeAttr("style")
        $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").removeAttr("style")
        if ($scope.selectedRows.length > 1) {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:block !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:block !important");
        }
        else {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:none !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:none !important");
            $("#copymanagebtn").hide();
            $("#cutmangebtn").hide();
            $("#compressmanagebtn").hide();
            $("#deletemanagebtn").hide();
            $("#renamemanagebtn").hide();
            $("#exportmanagebtn").hide();
            $("#downloadmanagebtn").hide();
        }
    }

    $scope.allFilesLoad = function () {
        $scope.linkValues = [];
        dataFactory.GetImageFiles($scope.catalogId, $scope.getCustomerIDs, $scope.getCustomerFolder).success(function (response) {
            if (response != null && response.length > 0) {

                $scope.getImageData = response;
                //--------------------------------Youtube seperation loop-----------------------------------//
                angular.forEach($scope.getImageData, function (file) {
                    if (file.FolderName.startsWith("YouTube_")) {
                        var fileDetail = file.FolderName.split('_');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.linkValues.push({ FolderName: file.FolderName, Link: convertedLink, Extention: "YouTube Link...", ModifiedDate: file.ModifiedDate, Size: "-", Path: file.Path, Caption: captionVal, CustomerFolder: file.CustomerFolder, Customer: file.Customer, ExternalDriveFlag: file.ExternalDriveFlag });
                    }
                });
                $scope.getImageData = jQuery.grep($scope.getImageData, function (value) {
                    return !(value.FolderName.startsWith("YouTube_"));
                });                
                //--------------------------------Youtube seperation loop-----------------------------------//
                $scope.IsEmpty = false;
                $rootScope.LoadImageData = $scope.getImageData;
                if ($scope.getImageData.length > 0) {
                    $scope.getCustomerFolder = $scope.getImageData[0].CustomerFolder;
                    $scope.cusFolder = $scope.getImageData[0].Customer;
                    $scope.extDriveFlag = $scope.getImageData[0].ExternalDriveFlag;
                }
                else {
                    $scope.getCustomerFolder = $scope.linkValues[0].CustomerFolder;
                    $scope.cusFolder = $scope.linkValues[0].Customer;
                    $scope.extDriveFlag = $scope.linkValues[0].ExternalDriveFlag;
                }
                $scope.newFolderPath = $scope.getCustomerFolder;
                $scope.previosPath = $scope.getCustomerFolder;                
                $scope.newFolderPath = $scope.getCustomerFolder;                
                //$scope.previosPath = $scope.getCustomerFolder;
                var previousPath = $scope.getCustomerFolder.substring(0, $scope.getCustomerFolder.lastIndexOf("\\"))
                var chkpreviousPath = previousPath + "\\"
                if (chkpreviousPath.contains("\\" + $scope.cusFolder + "\\") == true) {

                    $("#backbtn").show();
                    $scope.previosPath = previousPath;
                }
                else {
                    $("#backbtn").hide();
                }
                //-----Sorting

                $scope.propertyName = 'FolderName';
                $scope.reverse = true;
                $scope.getImageData = $filter('orderBy')($rootScope.LoadImageData, $scope.propertyName, $scope.reverse);
            }
            else {
                $scope.IsEmpty = true;
                var previousPath = $scope.getCustomerFolder.substring(0, $scope.getCustomerFolder.lastIndexOf("\\"))
                var chkpreviousPath = previousPath + "\\"
                if (chkpreviousPath.contains("\\" + $scope.cusFolder + "\\") == true) {

                    $("#backbtn").show();
                    $scope.previosPath = previousPath;
                }
                else {
                    $("#backbtn").hide();
                }
            }
        });
    }

    $("#custom-menu1").hide();
    $("#Newdiv").hide();
    $("#MainFolder").hide();
    $("#SubFolder").hide();
    $scope.ContextMenu = function (dataItem, event, fileType) {
        $scope.newFolderPath = dataItem.CustomerFolder;
        $scope.newText = dataItem.FolderName.replace(dataItem.Extention, "");
        //$("#MainFolder").hide();
        //$("#SubFolder").show();
        //$("#custom-menu1").show();
        //$("#Newdiv").hide();
        //     $("#mainGrid").addClass("context-menu-one btn btn-neutral");
        $scope.IsFileType = fileType;
        event.stopPropagation();
        return false;
    }
    $scope.ContextMainMenu = function (dataItem) {
        $scope.newFolderPath = dataItem;
        $("#MainFolder").show();
        $("#SubFolder").hide();
        $("#custom-menu1").show();
        $("#Newdiv").hide();
    }

    $scope.Create = function () {

        $("#custom-menu1").hide();
        $("#Newdiv").hide();
        $scope.newText = "";
        $scope.IsCreate = "Create";
        $("#newFolderPopUp").show();

        // var val = $scope.newText;
    }
    $scope.Rename = function () {

        if ($rootScope.userRoleAssetModify == true) {
            $("#custom-menu1").hide();
            $("#Newdiv").hide();
            $scope.IsCreate = "Rename";
            $("#renameFolderPopUp").show();
            $("#copymanagebtn").hide();
            $("#cutmangebtn").hide();
            $("#pastemanagebtn").hide();
            $("#compressmanagebtn").hide();
            $("#deletemanagebtn").hide();
            $("#renamemanagebtn").hide();
            $("#exportmanagebtn").hide();
            $("#downloadmanagebtn").hide();
            $scope.selectedRows = [];
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            // alert('You do not have rights to modify files.');
        }

    }
    $scope.Compress = function () {

        if ($rootScope.userRoleAssetModify == true) {
            $("#custom-menu1").hide();
            $("#Newdiv").hide();
            $scope.getAllSelectedRows();
            if ($scope.selectedRows.length == 0) {
                $scope.getAllSelectedRecentRows();
            }

            dataFactory.CompressImageManagementDetails($scope.newFolderPath, $localStorage.getCustomerID, $scope.IsFileType, $scope.selectedRows).success(function (response) {
                if (response != "") {
                    // alert(response);
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '',
                        type: "info"
                    });
                    $scope.allFilesLoad();
                }
                else {
                    // $scope.seletedAttribute = "";

                }
                $("#copymanagebtn").hide();
                $("#cutmangebtn").hide();
                $("#pastemanagebtn").hide();
                $("#compressmanagebtn").hide();
                $("#deletemanagebtn").hide();
                $("#renamemanagebtn").hide();
                $("#exportmanagebtn").hide();
                $("#downloadmanagebtn").hide();
                $scope.selectedRows = [];
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            // alert('You do not have rights to modify files.');
        }

    }
    $scope.Copy = function () {
        if ($rootScope.userRoleAssetModify == true) {
            $scope.getAllSelectedRows();
            $scope.IsEnabled = true;
            $scope.displaymanagepaste = true;
            $scope.PasteType = 0;
            $scope.IsCopy = true;
            $("#copymanagebtn").show();
            $("#pastemanagebtn").show();
            $("li.context-menu-item.context-menu-icon.context-menu-icon-paste").removeAttr("style")
            $("li.context-menu-item.context-menu-icon.context-menu-icon-paste").attr("style", "display:block !important");
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            // alert('You do not have rights to modify files.');
        }
    }
    $scope.Cut = function () {
        if ($rootScope.userRoleAssetModify == true) {
            $scope.getAllSelectedRows();
            $scope.displaymanagepaste = true;
            $scope.IsEnabled = true;
            $scope.PasteType = 1;
            $scope.IsCopy = true;
            $("#cutmangebtn").show();
            $("#pastemanagebtn").show();
            $("li.context-menu-item.context-menu-icon.context-menu-icon-paste").removeAttr("style")
            $("li.context-menu-item.context-menu-icon.context-menu-icon-paste").attr("style", "display:block !important");
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            // alert('You do not have rights to modify files.');
        }
    }

    $scope.Paste = function (getCustomerFolder) {
        if ($rootScope.userRoleAssetModify == true) {
           
            if ($scope.PasteType == 0 || $scope.PasteType == 1) {
                $scope.displaymanagepaste = false;
                //$scope.selectedRows;
                //$scope.PasteType;
                //$scope.newFolderPath;
                //$scope.IsFileType;
                dataFactory.CutCopyPasteDetails($scope.selectedRows, getCustomerFolder, $localStorage.getCustomerID, $scope.PasteType, $scope.IsFileType).success(function (response) {
                    if (response != "") {
                        //  alert(response);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '',
                            type: "info"
                        });
                        $scope.allFilesLoad();
                    }
                });
                $("#copymanagebtn").hide();
                $("#cutmangebtn").hide();
                $("#pastemanagebtn").hide();
                $("#compressmanagebtn").hide();
                $("#deletemanagebtn").hide();
                $("#renamemanagebtn").hide();
                $("#exportmanagebtn").hide();
                $("#downloadmanagebtn").hide();
                $scope.selectedRows = [];
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Cut/Copy a file before paste.',
                    type: "info"
                });
            }
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            // alert('You do not have rights to modify files.');
        }
    }
    $scope.IsCopy = false;
    $scope.Cancel = function () {
        $("#newFolderPopUp").hide();
        $("#renameFolderPopUp").hide();
        $scope.newText = "";
    }
    
    $scope.selectFileforUpload = function (file) {

        // blockUI.start();
        $q.all([$scope.selectFileforUpload1(file), $scope.SaveFileSelection()]).then(function (response) {
            return;
        });
    }


    $scope.selectFileforUpload1 = function (file) {
      
        this.value = null;
        $scope.isSuccessFlag = false;
        for (var i = 0; i < file.length; i++) {
            var formDataImage = new FormData();
            formDataImage.append("file", file[i]);
            $http.post("/ImageManagement/SaveFilesManageDrive?customerFolder=" + $scope.getCustomerFolder + "", formDataImage,
                {

                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                })
                .success(function (d) {
                    if (d == "") {
                        $scope.allFilesLoad();
                    }
                    else {
                        $scope.allFilesLoad();
                    }

                }).error(function () {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'File upload failed, please try again',
                        type: "error"
                    });
                });
            //  $scope.allFilesLoad();
            $scope.isSuccessFlag = true;
        }
        if ($scope.isSuccessFlag == true) {
            
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'File uploaded successfully',
                type: "Info"
            });
        }
        //  });

    };
    $scope.Delete = function () {
        if ($rootScope.userRoleAssetRemove == true) {
            $("#custom-menu1").hide();
            $("#Newdiv").hide();
            var bool = false;
            $scope.getAllSelectedRows();
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Do you like to delete this files?", // move to Recycle Bin
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result1) {

                    if (result1 === "Yes") {
                        bool = true;
                    }
                    if (bool == true) {
                        dataFactory.DeleteImageManagementDetails($scope.newFolderPath, $localStorage.getCustomerID, $scope.IsFileType, $scope.selectedRows).success(function (response) {
                            if (response != "") {

                                //alert(response);
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '',
                                    type: "info"
                                });
                                $("#pastemanagebtn").hide();
                                $("#copymanagebtn").hide();
                                $("#cutmangebtn").hide();
                                $("#compressmanagebtn").hide();
                                $("#deletemanagebtn").hide();
                                $("#renamemanagebtn").hide();
                                $("#exportmanagebtn").hide();
                                $("#downloadmanagebtn").hide();
                                $scope.allFilesLoad();
                            }
                            else {
                                // $scope.seletedAttribute = "";
                            }
                        });
                    }

                }
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to remove files.',
                type: "info"
            });
            //alert('You do not have rights to remove files.');
        }
    }
    $scope.Download = function () {
        if ($rootScope.userRoleAssetModify == true) {
            $scope.getAllSelectedRows();          
            if ($scope.selectedRows.length > 0) {

                var encryptedPath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.newFolderPath), key, {
                    keySize: 128 / 8,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });
                dataFactory.ExportCompressImageManagementDetails(encryptedPath, $localStorage.getCustomerID, $scope.IsFileType, $scope.selectedRows).success(function (response) {
                    if (response != "") {
                        // alert(response);
                        $scope.exportCompressPath = response;
                        var windowlocation = window.location.origin;

                        filename = $scope.exportCompressPath;
                        windowlocation = $scope.exportCompressPath;
                        window.open("DowloadImageManagementFiles.ashx?Path=" + $scope.exportCompressPath + "&Type=.zip");

                    }
                });

                //alert('You do not have rights to modify files.');
            }
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
          
        }
    }
    $scope.Extract = function () {
        if ($rootScope.userRoleAssetModify == true) {
            $scope.getAllSelectedRows();
            dataFactory.ExtractDetails($scope.selectedRows, $scope.getCustomerFolder, $localStorage.getCustomerID).success(function (response) {
                //if (response != "") {
                //    alert(response);
                if (response != "") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '',
                        type: "Info"
                    });
                    $("#pastemanagebtn").hide();
                    $("#copymanagebtn").hide();
                    $("#cutmangebtn").hide();
                    $("#compressmanagebtn").hide();
                    $("#deletemanagebtn").hide();
                    $("#renamemanagebtn").hide();
                    $("#exportmanagebtn").hide();
                    $("#downloadmanagebtn").hide();
                    $scope.allFilesLoad();
                }
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            //alert('You do not have rights to modify files.');
        }
    }
    $scope.BackFolder = function () {


        $scope.getCustomerFolder = $scope.previosPath;
        $scope.allFilesLoad();
    }
    // $scope.selectedRows = [];

    //For control key select on main asset page
    $scope.getSelectedFiles = function () {
        var selectedFiles = $filter("filter")($scope.getImageData, {
            selected: true
        }, true);
        var selectedLinkFiles = $filter("filter")($scope.linkValues, {
            selected: true
        }, true);
        var selectedFilesFinal = selectedFiles.concat(selectedLinkFiles);
        $scope.selectedFiles = selectedFilesFinal;
        $scope.IsCopy = true;
    }

    $scope.select = function (item, $event) {
        // var val = document.getElementById("filesdiv").querySelectorAll(".selected");

        var getSelectedClass = [];
        if ($scope.IsDisplay == true && $scope.IsThumbnail == true) {
            getSelectedClass = document.getElementById("filesdiv").querySelectorAll(".selected");
        }
        else if ($scope.IsDisplay == false) {
            getSelectedClass = document.getElementById("griddiv").querySelectorAll(".selected");
        }
        else if ($scope.IsThumbnail == false) {
            getSelectedClass = document.getElementById("Thumbnail").querySelectorAll(".selected");
        }
        $scope.getSelectedFiles();
        if ($scope.selectedFiles.length > 0) {
            var a = 0;
            var selectRows = $scope.selectedFiles;
            for (var i = 0; i < selectRows.length; i++) {
                selectRows[i].selected = false;
                if (selectRows[i] == item && selectRows.length > 1) {
                    item.selected = true;
                }
                if (selectRows[i] != item) {

                    a++;
                    if (a == selectRows.length) {
                        item.selected ? item.selected = false : item.selected = true;
                    }
                }
            }
        }
        else {
            item.selected ? item.selected = false : item.selected = true;
        }
        $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").removeAttr("style")
        $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").removeAttr("style")
        if ($scope.selectedRows.length > 1 || getSelectedClass.length > 1 || item.selected == true) {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:block !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:block !important");
            $("#copymanagebtn").show();
            $("#cutmangebtn").show();
            $("#compressmanagebtn").show();
            $("#deletemanagebtn").show();
            $("#renamemanagebtn").show();
            $("#exportmanagebtn").show();
            $("#downloadmanagebtn").show();
        }
        else {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:none !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:none !important");
            $("#copymanagebtn").hide();
            $("#cutmangebtn").hide();
            $("#compressmanagebtn").hide();
            $("#deletemanagebtn").hide();
            $("#renamemanagebtn").hide();
            $("#exportmanagebtn").hide();
            $("#downloadmanagebtn").hide();
        }
        //Rename folder
        $scope.RenameFolder = item.Path.toString();
        $event.stopPropagation();
        $event.preventDefault();
    }

    //For control key select on search result
    $scope.getSelectedSearchFiles = function () {
        var selectedRows = $filter("filter")($scope.getSearchResult, {
            selected: true
        }, true);
        var selectedRowsLinks = $filter("filter")($scope.searchlinkValues, {
            selected: true
        }, true);
        var selectedRowsFinal = selectedRows.concat(selectedRowsLinks);
        $scope.selectedRows = selectedRowsFinal;
        $rootScope.selectedRows = selectedRowsFinal;
        $scope.IsCopy = true;
    }

    $scope.selectSearch = function (item,$event) {

        var getSelectedClass = [];        
        if ($scope.IsDisplay == true && $scope.IsThumbnail == true) {
            getSelectedClass = document.getElementById("searchdivgrid").querySelectorAll(".selected");
        }
        else if ($scope.IsDisplay == false) {
            getSelectedClass = document.getElementById("searchdiv").querySelectorAll(".selected");
        }
        else if ($scope.IsThumbnail == false) {
            getSelectedClass = document.getElementById("searchThumbnail").querySelectorAll(".selected");
        }
        $scope.getSelectedSearchFiles();
        if ($scope.selectedRows.length > 0) {
            var a = 0;
            var selectRows = $scope.selectedRows;
            for (var i = 0; i < selectRows.length; i++) {
                selectRows[i].selected = false;
                if (selectRows[i] == item && selectRows.length > 0) {
                    item.selected = true;
                }
                if (selectRows[i] != item) {

                    a++;
                    if (a == selectRows.length) {
                        item.selected ? item.selected = false : item.selected = true;
                    }
                }
            }
        }
        else {
            item.selected ? item.selected = false : item.selected = true;
        }
        $event.stopPropagation();
        $event.preventDefault();
    }

    $scope.RenameFolder = '';

    $scope.selectright = function (item) {
        $scope.RenameFolder = item.Path.toString();
        //item.selected = true;
        $("#copymanagebtn").show();
        $("#cutmangebtn").show();
        $("#compressmanagebtn").show();
        $("#deletemanagebtn").show();
        $("#exportmanagebtn").show();
        $("#downloadmanagebtn").show();
    }
    $scope.getAllSelectedRows = function () {
        //var selectedRows = $filter("filter")($scope.getSearchResult, {
        //    selected: true
        //}, true);

        var selectedRows = $filter("filter")($scope.getImageData, {
            selected: true
        }, true);

        $scope.selectedRows = selectedRows;
        $rootScope.selectedRows = selectedRows;
        $scope.IsCopy = true;

    }
    $scope.SaveFileSelection = function (e) {
        if ($scope.checkfile == "search") {
            $scope.getSelectedSearchFiles();
        }
        else{
            $scope.getAllSelectedRows();
        }
        if ($rootScope.selectedRows.length > 0) {
            {
                if ($scope.extDriveFlag == 'false') {
                    var pathString = $rootScope.selectedRows[0].Path.split("\\" + $scope.cusFolder + "\\");
                    var path = "";
                    if (pathString.length > 0) {
                        path = "\\" + pathString[1];
                    }
                    else {
                        path = "\\" + fileName;
                    }
                    var fileName = $rootScope.selectedRows[0].Path.match(/\\([^\\]+)$/)[1];
                }
                else if ($scope.extDriveFlag == 'true')
                {
                    var pathVal = $rootScope.selectedRows[0].PreviewPath.split("//" + $scope.cusFolder);
                    var fileName = $rootScope.selectedRows[0].FolderName;
                    var path = pathVal[1];
                }
                if ($rootScope.selectedRows.length == 1) {
                    if ($rootScope.paramValue.toLowerCase().indexOf("category") != -1) {
                        $rootScope.SaveFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("family") != -1) {
                        $rootScope.SaveFamilyFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("supplier") != -1) {
                        $rootScope.SaveSupplierFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("reference") != -1) {
                        $rootScope.SaveReferenceTableFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("newproduct") != -1) {
                        $rootScope.SaveNewProdFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("newsub") != -1) {
                        $rootScope.SaveNewSubFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("commonsub") != -1) {
                        $rootScope.saveSubProductUpdation(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("commonmain") != -1) {
                        $rootScope.saveUpdation(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("backuprestore") != -1) {
                        $rootScope.saveBackupRestoreFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.length > 0) {
                        $rootScope.SaveProductFileSelection(fileName, path);
                    }
                    

                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select maximum 1 file',
                        type: "info"
                    });
                }
            }

        }
    }
    $scope.SaveDoubleClickFileSelection = function (selectedFile) {
        if (selectedFile != "" && selectedFile != undefined && selectedFile != null) {
            {
                if ($scope.extDriveFlag == 'false') {
                    var data = selectedFile.Path;
                    var pathString = data.split("\\" + $scope.cusFolder + "\\");
                    var path = "";
                    if (pathString.length > 0) {
                        path = "\\" + pathString[1];
                    }
                    else {
                        path = "\\" + fileName;
                    }
                    var fileName = data.match(/\\([^\\]+)$/)[1];
                    if ($rootScope.paramValue.toLowerCase().indexOf("category") != -1) {
                        $rootScope.SaveFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("family") != -1) {
                        $rootScope.SaveFamilyFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("supplier") != -1) {
                        $rootScope.SaveSupplierFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("reference") != -1) {
                        $rootScope.SaveReferenceTableFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("newproduct") != -1) {
                        $rootScope.SaveNewProdFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("newsub") != -1) {
                        $rootScope.SaveNewSubFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("commonmain") != -1) {
                        $rootScope.saveUpdation(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("commonsub") != -1) {
                        $rootScope.saveSubProductUpdation(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("backuprestore") != -1) {
                        $rootScope.saveBackupRestoreFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.length > 0) {

                        $rootScope.SaveProductFileSelection(fileName, path);
                    }                    
                }
                else if($scope.extDriveFlag == 'true')
                {
                    var pathVal = selectedFile.PreviewPath.split("//" + $scope.cusFolder);
                    var fileName = selectedFile.FolderName;
                    var path = pathVal[1];
                    if ($rootScope.paramValue.toLowerCase().indexOf("category") != -1) {
                        $rootScope.SaveFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("family") != -1) {
                        $rootScope.SaveFamilyFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("supplier") != -1) {
                        $rootScope.SaveSupplierFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("reference") != -1) {
                        $rootScope.SaveReferenceTableFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("newproduct") != -1) {
                        $rootScope.SaveNewProdFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("newsub") != -1) {
                        $rootScope.SaveNewSubFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("commonmain") != -1) {
                        $rootScope.saveUpdation(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("commonsub") != -1) {
                        $rootScope.saveSubProductUpdation(fileName, path);
                    }
                    else if ($rootScope.paramValue.toLowerCase().indexOf("backuprestore") != -1) {
                        $rootScope.saveBackupRestoreFileSelection(fileName, path);
                    }
                    else if ($rootScope.paramValue.length > 0) {

                        $rootScope.SaveProductFileSelection(fileName, path);
                    }                    
                }
            }

        }
    }

    $scope.SaveFolder = function () {

        if ($scope.IsCreate == "Rename" && $scope.newText == "") {
            $("#custom-menu1").show();
            $("#Newdiv").show();
            alert("Please enter the file name")
        }
        else {

            var Path = "";
            if ($scope.IsCreate == "Create") {
                Path = $scope.getCustomerFolder;
            }
            if ($scope.IsCreate == "Rename") {

                $scope.newSearchPath = $scope.RenameFolder;
                Path = $scope.RenameFolder;
            }
            else {
                Path = $scope.newFolderPath;
            }
            dataFactory.SaveImageManagementDetails(Path, $scope.newText, $localStorage.getCustomerID, $scope.IsCreate, $scope.IsFileType).success(function (response) {
                //dataFactory.SaveImageManagementDetails($scope.newSearchPath, $scope.newText, $localStorage.getCustomerID, $scope.IsCreate, $scope.IsFileType).success(function (response) {

                if (response != "") {

                    // alert(response);
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '',
                        type: "info"
                    });

                    $("#renameFolderPopUp").hide();
                    $("#newFolderPopUp").hide();
                    $scope.newText = "";
                }
                else {
                    // $scope.seletedAttribute = "";
                }
            });
            $scope.allFilesLoad();
        }

    }
    $scope.CancelAssetPopUp = function () {
        $scope.winManageDrive.center().close();
    }
    $scope.fileOpenClick = function (e) {
        // alert(e);

    }
    $scope.onSuccess1 = function (e) {
        if (e.operation === "remove") {
            $scope.ImagePreview = "";
        }
        else {
            var message = $.map(e.files, function (file) { return file.name; }).join(", ");
            if (message.indexOf('&') == -1 && message.indexOf('#') == -1 && message.indexOf('\'') == -1) {
                $scope.$apply(function () {
                    //$scope.ImagePreview = '\\Images\\' + message;
                    $scope.ImagePreview = message;
                });

                $(".k-upload-files").remove();
                $(".k-upload-status").remove();

            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Image name contains invalid special characters( # & \' )',
                    type: "error"
                });

                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            }
            //$scope.Category.IMAGE_FILE2 = '\\' + $localStorage.getCustomerCompanyName + '\\Images\\' + message;
        }

        //  alert(someInfo);
    };
    $scope.onSelect = function (e) {
        var message = $.map(e.files, function (file) { return file.name; }).join(", ");
    };

    $scope.clearFileAttach = function (imageValue) {
        $scope.ImagePreview = "";
    };

    $scope.init = function () {
        if (!$localStorage.imgaepopupforinvert) {
            $scope.productData = [];
            $rootScope.SubProdInvert = false;
            $rootScope.btnSubProdInvert = false;
            $rootScope.invertedproductsshow = false;
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = false;
        }

    };


    //$scope.gridViewLarge = function () {
    //    $scope.IsDisplay = true;
    //    $('[id^=mainGrid]').css('width', '48%');
    //    $('[id^=mainGrid]').css('height', '180px');
    //    $('[id^=foldername]').css('width', '96%');
    //}

    //$scope.gridViewMedium = function () {
    //    $scope.IsDisplay = true;
    //    $('[id^=mainGrid]').css('width', '31%');
    //    $('[id^=mainGrid]').css('height', '125px');
    //    $('[id^=foldername]').css('width', '94%');
    //    //document.getElementById('^mainGrid').style.width = "32%";
    //}

    //$scope.gridViewSmall = function () {
    //    $scope.IsDisplay = true;
    //    $('[id^=mainGrid]').css('width', '23%');
    //    $('[id^=mainGrid]').css('height', '120px');
    //    $('[id^=foldername]').css('width', '91%');
    //    $('[id^=image]').css('width', '82%');
    //    //document.getElementById('^mainGrid').style.width = "32%";
    //}
    //$scope.thumbnail = function () {
    //    $scope.IsDisplay = true;
    //    $('[id^=mainGrid]').css('width', '18%');
    //    $('[id^=mainGrid]').css('height', '120px');
    //    $('[id^=foldername]').css('width', '90%');
    //    //document.getElementById('^mainGrid').style.width = "32%";
    //}

    $scope.gridViewLarge = function () {

        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '32%', '!important');
            $('[id^=mainGrid]').css('height', '180px');
            $('[id^=youtubevideos]').css('width', '75%', '!important');
            $('[id^=youtubevideos]').css('height', '180px');
            $('i.fa.fa-folder-open.folderimage.context-menu-one.btn.btn-neutral.ng-scope').css('padding', '50px 170px 50px 160px');
        }, 200);
        //document.getElementById('^mainGrid').style.width = "32%";
    }

    $scope.gridViewMedium = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '24%');
            $('[id^=mainGrid]').css('height', '125px');
            $('[id^=youtubevideos]').css('width', '75%');
            $('[id^=youtubevideos]').css('height', '125px');
        }, 200);
        //document.getElementById('^mainGrid').style.width = "32%";
    }

    $scope.gridViewSmall = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '19%');
            $('[id^=mainGrid]').css('height', '120px');
            $('[id^=youtubevideos]').css('width', '75%');
            $('[id^=youtubevideos]').css('height', '120px');
        }, 200);
        //document.getElementById('^mainGrid').style.width = "32%";
    }
    $scope.thumbnail = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '15%');
            $('[id^=mainGrid]').css('height', '120px');
            $('[id^=youtubevideos]').css('width', '75%');
            $('[id^=youtubevideos]').css('height', '120px');
            $('i.fa.fa-folder-open.folderimage.context-menu-one.btn.btn-neutral.ng-scope').css('padding', '22px 65px 23px 65px');
        }, 200);
        //document.getElementById('^mainGrid').style.width = "32%";
    }

    $scope.thumbnailnew = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = false;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '10%');
            $('[id^=mainGrid]').css('height', '60px');
            $('[id^=youtubevideos]').css('width', '30%');
            $('[id^=youtubevideos]').css('height', '60px');
        }, 200);
        //$('i.fa.fa-folder-open.folderimage.context-menu-one.btn.btn-neutral.ng-scope').css('padding', '22px 65px 23px 65px');

        //document.getElementById('^mainGrid').style.width = "32%";
    }

    $scope.getSearchResultLoad = function (data) {
        $scope.checkfile = data;
        $scope.getIsSearchResult = true;
        $scope.getIsRecentList = false;
        $scope.searchlinkValues = [];
        //var path = $scope.newFolderPath;
        //var searchtxt = $scope.searchtxt;
        dataFactory.getFileSearchResult($scope.newFolderPath, $scope.searchtxt, $scope.getCustomerIDs).success(function (response) {
            //if (response != "") {
            $scope.getSearchResult = response;
            //--------------------------------Youtube seperation loop-----------------------------------//
            angular.forEach($scope.getSearchResult, function (file) {
                if (file.FolderName.startsWith("YouTube_")) {
                    var fileDetail = file.FolderName.split('_');
                    var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                    var convertedLink = $sce.trustAsResourceUrl(linkVal);
                    var captionVal = fileDetail[2].replace('.rtf', '');
                    $scope.searchlinkValues.push({ FolderName: file.FolderName, Link: convertedLink, Extention: "YouTube Link...", ModifiedDate: file.ModifiedDate, Size: "-", Path: file.Path, Caption: captionVal });
                }
            });
            $scope.getSearchResult = jQuery.grep($scope.getSearchResult, function (value) {
                return !(value.FolderName.startsWith("YouTube_"));
            });            
            //--------------------------------Youtube seperation loop-----------------------------------//
            $scope.getIsSearchResult = true;
            $("#pastemanagebtn").hide();
            $("#copymanagebtn").hide();
            $("#cutmangebtn").hide();
            $("#compressmanagebtn").hide();
            $("#deletemanagebtn").hide();
            $("#renamemanagebtn").hide();
            $("#exportmanagebtn").hide();
            $("#downloadmanagebtn").hide();
            //   }
        });
    }

    $scope.fileSearchClick = function (e, $event) {

        var newPath = "";
        $scope.bradcrumbList = [];
        $scope.IsEmpty = false;
        var PathString = "";
        dataFactory.getLocation().success(function (response) {
            if (response != "") {

                $scope.getFileLocation = response;
                $scope.getFullPath = e.replace($scope.getFileLocation + "\\", "").split('\\');
                if ($scope.getFullPath.length > 0) {
                    for (var j = 0; j < $scope.getFullPath.length; j++) {

                        if ($scope.getFullPath[j] == $scope.cusFolder && j == 0) {

                            newPath = $scope.getFileLocation + "\\" + $scope.cusFolder + "\\";
                        }
                        else {

                            if (PathString == "") {

                                PathString = $scope.getFullPath[j];
                            }
                            else {
                                PathString = PathString + "\\" + $scope.getFullPath[j];

                            }
                            newPath = $scope.getFileLocation + "\\" + $scope.cusFolder + "\\" + PathString;
                        }

                        $scope.bradcrumbList.push(
                 {
                     FolderName: $scope.getFullPath[j],
                     Path: newPath
                 }
                 );
                    }
                }
            }
        });
        $scope.getIsSearchResult = false;
        $scope.getCustomerFolder = e;
        $scope.allFilesLoad();
        $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").removeAttr("style")
        $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").removeAttr("style")
        if ($scope.selectedRows.length > 1) {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:block !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:block !important");
        }
        else {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:none !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:none !important");
            $("#copymanagebtn").hide();
            $("#cutmangebtn").hide();
            $("#compressmanagebtn").hide();
            $("#deletemanagebtn").hide();
            $("#renamemanagebtn").hide();
            $("#exportmanagebtn").hide();
            $("#downloadmanagebtn").hide();
        }

    }

    $scope.init();

    $scope.mainPageOut = function () {
        angular.forEach($scope.getImageData, function (value) {
            value.selected = false;
        });
        $("#copymanagebtn").hide();
        $("#cutmangebtn").hide();
        $("#pastemanagebtn").hide();
        $("#compressmanagebtn").hide();
        $("#deletemanagebtn").hide();
        $("#renamemanagebtn").hide();
        $("#exportmanagebtn").hide();
        $("#downloadmanagebtn").hide();
    }

    $scope.searchPageOut = function () {
        angular.forEach($scope.getSearchResult, function (value) {
            value.selected = false;
        });
        $("#copymanagebtn").hide();
        $("#cutmangebtn").hide();
        $("#pastemanagebtn").hide();
        $("#compressmanagebtn").hide();
        $("#deletemanagebtn").hide();
        $("#renamemanagebtn").hide();
        $("#exportmanagebtn").hide();
        $("#downloadmanagebtn").hide();
    }

}]);
