﻿
LSApp.controller("ImageManagementController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$rootScope', '$localStorage', '$http', '$filter', '$q', '$sce', 'blockUI', '$timeout',
function ($scope, $window, $location, $routeParams, dataFactory, $rootScope, $localStorage, $http, $filter, $q, $sce, blockUI, $timeout) {
    // --------------------------------------------------- Start Common Function --------------------------------------------------------------------------------
    // $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
    //----------------------------------------------------Extention Array values-----------------------------------------------------------------//
    var imageExtensionArray = [".jpg", ".png", ".gif", ".bmp", ".jpeg"]; // *** Speicify the extension in lower case only... ***  ## Add it in else part also. 

    var pdfExtensionArray = [".pdf"]; // *** Speicify the extension in lower case only... ***  ## Add it in else part also.  

    var videoExtensionArray = [".avi", ".flv", ".mov", ".mp4", ".wmv"]; // *** Speicify the extension in lower case only... ***  ## Add it in else part also.  

    var otherThanMentionedArray = [".jpg", ".png", ".gif", ".bmp", ".jpeg", ".pdf", ".avi", ".flv", ".mov", ".mp4", ".wmv", ".rtf"];


    //----------------------------------------------------Extention Array values-----------------------------------------------------------------//

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    var arrayImages = [];
    $scope.ImagePreview = "";
    $scope.IsCreate = "";
    $scope.IsFileType = "";
    $scope.newFolderPath = "";
    $scope.IsEnabled = false;
    $scope.managenew = false;
    $scope.getIsSearchResult = false;
    $scope.displaymanagepaste = false;
    $scope.IsDisplay = true;
    $scope.IsEmpty = false;
    $scope.assetSizeRange = "Greater than";
    $scope.assetFileSize = "KB";
    $("#newFolderPopUp").hide();
    $("#renameFolderPopUp").hide();
    $("#renameFolderPopUp").hide();
    $("#addYoutubeLink").hide();
    $("#backbtn").hide();
    $("#copymanagebtn").hide();
    $("#cutmangebtn").hide();
    $("#HyperLinkmanagebtn").hide();
    $("#compressmanagebtn").hide();
    $("#deletemanagebtn").hide();
    $("#renamemanagebtn").hide();
    $("#exportmanagebtn").hide();
    $("#downloadmanagebtn").hide();
    $scope.IsThumbnail = true;
    $scope.selectedRows = [];
    $scope.bradcrumbList = [];



    $("#HyperlinkModelpopup").hide();
    $("#recievermailID").hide();
    $("#gmail").hide();
    $("#validationmessage").hide();
    $("#linkcopiedmessage").hide();


    $scope.linkValues = []; //youtube link values for normal
    $scope.searchlinkValues = []; //youTube link values for search
    $scope.recentTodayLinkValues = []; //youTube link values for recent today
    $scope.recentYesterdayLinkValues = []; //youTube link values for recent yesterday
    $scope.recentPreviousLinkValues = []; //youTube link values for recent previous

    $scope.getIsSearchResult = false;
    $scope.getIsRecentList = false;
    $scope.recentFlag = false;
    $scope.starredFlag = false;
    $scope.getSearchResultLoad = function () {
        $scope.getIsSearchResult = true;
        $scope.getIsRecentList = false;
        $scope.searchlinkValues = [];


        //var path = $scope.newFolderPath;
        //var searchtxt = $scope.searchtxt;
        if ($scope.searchtxt != undefined && $scope.searchtxt != "") {
            dataFactory.getFileSearchResult($scope.newSearchPath, $scope.searchtxt, $scope.getCustomerIDs).success(function (response) {
                //if (response != "") {

                $scope.getSearchResult = response;
                //--------------------------------Youtube seperation loop-----------------------------------//
                angular.forEach($scope.getSearchResult, function (file) {
                    if (file.FolderName.startsWith("YouTube~")) {
                        var fileDetail = file.FolderName.split('~');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.searchlinkValues.push({ FolderName: file.FolderName, Extention: "YouTube Link...", Path: file.Path, Link: convertedLink, ModifiedDate: file.ModifiedDate, Size: "-", Caption: captionVal });
                        //$scope.youTubeIndex.push($scope.getImageData.indexOf(file));                        
                        //$scope.getImageData.delete(value);
                    }
                });
                $scope.getSearchResult = jQuery.grep($scope.getSearchResult, function (value) {
                    return !(value.FolderName.startsWith("YouTube~"));
                });
                //$scope.getImageData = selectedList;
                //--------------------------------Youtube seperation loop-----------------------------------//
                $scope.getIsSearchResult = true;
                $("#copymanagebtn").hide();
                $("#cutmangebtn").hide();
                $("#pastemanagebtn").hide();
                $("#HyperLinkmanagebtn").hide();
                $("#compressmanagebtn").hide();
                $("#deletemanagebtn").hide();
                $("#renamemanagebtn").hide();
                $("#exportmanagebtn").hide();
                $("#downloadmanagebtn").hide();

                //   }
            });
        }
        else {
            $scope.breadCrumbClick($scope.newSearchPath);
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a value in the Search text.',
                type: "info"
            });
        }
    }

    $scope.toggleGrid = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $('[id^=mainGrid]').css('width', '24%');
        $('[id^=mainGrid]').css('height', '125px');
        $('[id^=sizeBasedMainGrid]').css('width', '24%');
        $('[id^=sizeBasedMainGrid]').css('height', '125px');
        $('[id^=starredMainGrid]').css('width', '24%');
		$('[id^=starredMainGrid]').css('height', '125px');
    }
    $scope.toggleList = function () {
        $scope.IsThumbnail = true;
        $scope.IsDisplay = false;

    }
    if ($localStorage.getCatalogID === undefined) {
        $scope.catalogId = 0;
        $scope.getCustomerIDs = 0;
    } else {

        $scope.catalogId = $localStorage.getCatalogID;
        $scope.getCustomerIDs = $localStorage.getCustomerID;

        dataFactory.GetImageFiles($scope.catalogId, $scope.getCustomerIDs, "", $scope.getCustomerCompanyName).success(function (response) {

            if (response != null && response.length > 0) {
                $scope.getImageData = response;
                //--------------------------------Youtube seperation loop-----------------------------------//
                angular.forEach($scope.getImageData, function (file) {
                    if (file.FolderName.startsWith("YouTube~")) {
                        var fileDetail = file.FolderName.split('~');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.linkValues.push({ FolderName: file.FolderName, Extention: "YouTube Link...", Path: file.Path, Link: convertedLink, ModifiedDate: file.ModifiedDate, Size: "-", Caption: captionVal, CustomerFolder: file.CustomerFolder, Customer: file.Customer, ExternalDriveFlag: file.ExternalDriveFlag });
                        //$scope.youTubeIndex.push($scope.getImageData.indexOf(file));                        
                        //$scope.getImageData.delete(value);
                    }
                });
                $scope.getImageData = jQuery.grep($scope.getImageData, function (value) {
                    return !(value.FolderName.startsWith("YouTube~"));
                });
                //$scope.getImageData = selectedList;
                //--------------------------------Youtube seperation loop-----------------------------------//

                $rootScope.LoadImageData = $scope.getImageData;
                if ($scope.getImageData.length > 0) {
                    $scope.getCustomerFolder = $scope.getImageData[0].CustomerFolder;
                    $scope.cusFolder = $scope.getImageData[0].Customer;
                    $scope.extDriveFlag = $scope.getImageData[0].ExternalDriveFlag;
                }
                else {
                    $scope.getCustomerFolder = $scope.linkValues[0].CustomerFolder;
                    $scope.cusFolder = $scope.linkValues[0].Customer;
                    $scope.extDriveFlag = $scope.linkValues[0].ExternalDriveFlag;
                }
                $scope.newFolderPath = $scope.getCustomerFolder;
                $scope.newSearchPath = $scope.getCustomerFolder;
                $scope.recentFilesPath = $scope.getCustomerFolder;
                $scope.previosPath = $scope.getCustomerFolder;

                $scope.IsEmpty = false;
                $scope.bradcrumbList.push({ FolderName: $scope.cusFolder, Path: $scope.getCustomerFolder });
                //-----Sorting
                $scope.propertyName = 'FolderName';
                $scope.reverse = true;
                $scope.getImageData = $filter('orderBy')($rootScope.LoadImageData, $scope.propertyName, $scope.reverse);
                //-----End
            }
            else {
                $scope.IsEmpty = true;
            }
        });
    }

    //for sorting values in the list view page
    $scope.sortBy = function (propertyName) {

        $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
            ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
        $scope.getImageData = $filter('orderBy')($rootScope.LoadImageData, $scope.propertyName, $scope.reverse);
        if ($scope.recentFileDetailsPrevious != undefined && $scope.recentFileDetailsPrevious.length > 0) {
            $scope.recentFileDetailsPrevious = $filter('orderBy')($scope.recentFileDetailsPrevious, $scope.propertyName, $scope.reverse);
        }
        if ($scope.recentFileDetailsToday != undefined && $scope.recentFileDetailsToday.length > 0) {
            $scope.recentFileDetailsToday = $filter('orderBy')($scope.recentFileDetailsToday, $scope.propertyName, $scope.reverse);
        }
        if ($scope.recentFileDetailsYesterday != undefined && $scope.recentFileDetailsYesterday.length > 0) {
            $scope.recentFileDetailsYesterday = $filter('orderBy')($scope.recentFileDetailsYesterday, $scope.propertyName, $scope.reverse);
        }
        if ($scope.getSearchResult != undefined && $scope.getSearchResult.length > 0) {
            $scope.getSearchResult = $filter('orderBy')($scope.getSearchResult, $scope.propertyName, $scope.reverse);
        }

        if ($scope.getImageBasedOnSize != undefined && $scope.getImageBasedOnSize.length > 0) {
            $scope.getImageBasedOnSize = $filter('orderBy')($scope.getImageBasedOnSize, $scope.propertyName, $scope.reverse);
        }
        if ($scope.getImageBasedOnDate != undefined && $scope.getImageBasedOnDate.length > 0) {
            $scope.getImageBasedOnDate = $filter('orderBy')($scope.getImageBasedOnDate, $scope.propertyName, $scope.reverse);
        }

        if ($scope.starredFiles != undefined && $scope.starredFiles.length > 0) {
            $scope.starredFiles = $filter('orderBy')($scope.starredFiles, $scope.propertyName, $scope.reverse);
        }

        $scope.active_Sort($scope.propertyName);//this is used to call from sort in left navigator. this function is used to active the dropdown list selection.
    };

    $scope.active_Sort = function (propertyName) {
        if ($scope.propertyName == 'FolderName')
            $("#Name").addClass("active");
        else
            $("#Name").removeClass("active");

        if ($scope.propertyName == 'Size')
            $("#Size").addClass("active");
        else
            $("#Size").removeClass("active");

        if ($scope.propertyName == 'Extention')
            $("#Type").addClass("active");
        else
            $("#Type").removeClass("active");

        if ($scope.propertyName == 'ModifiedDate')
            $("#Modifieddate").addClass("active");
        else
            $("#Modifieddate").removeClass("active");
    }

    //For New UI in asset management 2022 Dec Size based filter
    ///on change date
    $scope.assetDateOnChange = function (e) {
        debugger;
        if ($scope.assetDateBasedSearch == "Custom Date") {
            $("#fromDate").show();
            $("#toDate").show();
            $("#from").show();
            $("#to").show();
        } else {
            $("#fromDate").hide();
            $("#toDate").hide();
            $("#from").hide();
            $("#to").hide();        
        }
    }


    //On change event
    $scope.assetSizeRangeOnChange = function (e) {
        if ($scope.assetSizeRange == "Greater than") {
            $scope.assetFileSize = $scope.assetFileSize;
            $scope.isDisabled = false;
        }
        else if ($scope.assetSizeRange == "Less than") {
            $scope.assetFileSize = $scope.assetFileSize;
            $scope.isDisabled = false;
        }
        else {
            $scope.assetFileSize = "Size";
            $scope.assetSizeValue = 0;
            $scope.isDisabled = true;
        }
    };


    //Kendo Source
    $scope.assetDateRangeSource = new kendo.data.DataSource({        
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getAssetDateRange().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });


    //Kendo Source
    $scope.assetSizeRangeSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getAssetSizeRange().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    //Kendo Source
    $scope.assetFileSizeSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getAssetFileSize().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.sizeBasedFilter = function () {

        $scope.getSizeBasedFilterResult = true;
        $scope.getIsRecentList = false;
        $scope.displayStarredAssets = false;
        $scope.getDateBasedFilterResult = false;

        if ($scope.assetSizeRange != "" && $scope.assetSizeValue != undefined) {
            if ($scope.assetSizeRange == "Greater than") {
                $scope.assetFileSize = $scope.assetFileSize;
            }
            else if ($scope.assetSizeRange == "Less than") {
                $scope.assetFileSize = $scope.assetFileSize;
            }
            else {
                $scope.assetFileSize = "Size";
                $scope.assetSizeValue = 0;
            }
            dataFactory.getFilesBasedOnGivenSize($scope.getCustomerFolder, $scope.assetSizeRange, $scope.assetSizeValue, $scope.assetFileSize).success(function (response) {
                if (response != null && response.length > 0) {
                    $scope.getImageBasedOnSize = response;
                    $scope.IsEmpty = false;
                    $("#copymanagebtn").hide();
                    $("#cutmangebtn").hide();
                    $("#pastemanagebtn").hide();
                    $("#HyperLinkmanagebtn").hide();
                    $("#compressmanagebtn").hide();
                    $("#deletemanagebtn").hide();
                    $("#renamemanagebtn").hide();
                    $("#exportmanagebtn").hide();
                    $("#downloadmanagebtn").hide();
                }
                else {
                    $scope.IsEmpty = true;
                }
            });

        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a value',
                type: "info"
            })
            $scope.resetFilter();
        }

    }

    // File filter by Date range
    $scope.filterDateRange = function () {      
        $scope.getDateBasedFilterResult = true;
        $scope.getSizeBasedFilterResult = false;
        $scope.getIsRecentList = false;
        $scope.displayStarredAssets = false;
        $scope.SelectedDay = "";
        $scope.selectedFrom = "";
        $scope.selectedTo = "";
        $scope.IsEmpty = false;
        $scope.fromRange = "";
        $scope.toRange = "";
        $scope.selectedFrom = "";
        $scope.selectedTo = "";
        if (($scope.assetDateBasedSearch != "") || ($scope.assetDateBasedSearch != "undefined")) {
            if($scope.assetDateBasedSearch=="Any Time")
            {
                $scope.SelectedDay = "Any Time";
            }
            else if ($scope.assetDateBasedSearch == "") {
                $scope.SelectedDay = "Any Time";
            }
            else if ($scope.assetDateBasedSearch == "Custom Date") {
                $scope.fromRange =  document.getElementById('fromDate').value;
                $scope.toRange = document.getElementById('toDate').value;
                $scope.SelectedDay = $scope.assetDateBasedSearch;
            }
            else
            {
                $scope.SelectedDay = $scope.assetDateBasedSearch;
            }
            if ($scope.fromRange > $scope.toRange) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please selet proper date range',
                    type: "info"
                });
                $scope.init();
            }
            dataFactory.getFilesByDate($scope.SelectedDay, $scope.getCustomerFolder, $scope.fromRange, $scope.toRange ).success(function (response) {
                if (response != null && response.length > 0) {
                    $scope.getImageBasedOnDate = response;
                }
                else {
                    $scope.IsEmpty = true;
                }
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please selet the date range',
                type: "info"
            })
            $scope.clearFilter();
        }
        
       
    }
    $scope.clearFilter = function () {
        $scope.getIsSearchResult = false;
        $scope.getIsRecentList = false;
        $scope.getSizeBasedFilterResult = false;
        $scope.displayStarredAssets = false;
        $scope.getDateBasedFilterResult = false;
        $scope.IsEmpty = false;
        $scope.assetDateBasedSearch = "Any Time";
        $scope.init();
    }

    $scope.resetFilter = function () {
        $scope.getIsSearchResult = false;
        $scope.getIsRecentList = false;
        $scope.getSizeBasedFilterResult = false;
        $scope.displayStarredAssets = false;
        $scope.getDateBasedFilterResult = false;
        $scope.IsEmpty = false;
        $scope.assetSizeValue = "";
        $scope.assetSizeRange = "Greater than";
        $scope.assetFileSize = "KB";

        $scope.init();
    }

    //For New UI in asset management 2022 Dec Size based filter End

    //For New UI in asset management 2022 Dec Display starred Items

    $scope.starredFilesClick = function () {

        $scope.displayStarredAssets = true;
        $scope.getSizeBasedFilterResult = false;
        $scope.getDateBasedFilterResult = false;
        $scope.getIsRecentList = false;
        $scope.recentFlag = false;

        $scope.starredFlag = true;

        $scope.UserName = $localStorage.getUserName;

        dataFactory.getStarredFiles($scope.UserName).success(function (response) {
            if (response != null) {
                $scope.starredFiles = response;
                // $scope.extDriveFlag = $scope.starredFiles.EXTERNAL_DRIVE_FLAG;
                $("#copymanagebtn").hide();
                $("#cutmangebtn").hide();
                $("#pastemanagebtn").hide();
                $("#compressmanagebtn").hide();
                $("#deletemanagebtn").hide();
                $("#renamemanagebtn").hide();
                $("#exportmanagebtn").hide();
                $("#downloadmanagebtn").hide();
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'No Files Found',
                    type: "info"
                })
            }
        });
    }
        
    //For New UI in asset management 2022 Dec Display starred Items End

    $("#managefile1").kendoUpload({
        async: {
            saveUrl: '/ImageManagement/SaveImageManagementFile',
            // removeUrl: '/ImageManagement/RemoveImageManagementFile',
            autoUpload: false,
            multiple: true,
            allowmultiple: true,
            batch: false
        },
        upload: function (e) {
            e.data = { customerFolder: $scope.getCustomerFolder };
            $scope.allFilesLoad();
        },
        //remove: function (e) {
        //    e.data = { customerFolder: $scope.getCustomerFolder };
        //    $scope.allFilesLoad();
        //}
    });

    $scope.breadCrumbClick = function (e) {

        $scope.getIsSearchResult = false;
        $scope.getIsRecentList = false;
        $scope.getSizeBasedFilterResult = false;
        $scope.getDateBasedFilterResult = false;
        $scope.displayStarredAssets = false;

        $scope.recentFlag = false;
        $scope.starredFlag = false;
        var fileName = '';
        if ($scope.extDriveFlag == 'false')
            fileName = e.replace(/\\$/, '').split('\\').pop();
        else {
            fileName = e.replace(/\\$/, '').split('//').pop();
            fileName = fileName.replace(/\\$/, '').split(/\\/).pop();
        }
        if ($scope.bradcrumbList.length != 1) {
            for (var i = $scope.bradcrumbList.length - 1; i >= 0; i--) {
                if ($scope.bradcrumbList[i].Path == e && $scope.bradcrumbList[i].FolderName == fileName) {
                    $scope.bradcrumbList.splice(i, $scope.bradcrumbList.length);

                }
                else if ($scope.bradcrumbList[i].Path == e && $scope.bradcrumbList[i].FolderName == "File") {
                    $scope.bradcrumbList.splice(i, $scope.bradcrumbList.length);
                }
            }
        }
        else if ($scope.bradcrumbList.length == 1 && fileName == $scope.cusFolder) {
            $scope.bradcrumbList = [];
        }
        $scope.fileClick(e);
    }
    $scope.fileClick = function (e) {
        //alert(e);
        $scope.newSearchPath = e;
        $("#backbtn").show();
        $scope.getIsSearchResult = false;

        $scope.getCustomerFolder = e;
        var fileName = '';
        if ($scope.extDriveFlag == 'false')
            fileName = e.replace(/\\$/, '').split('\\').pop();
        else {
            fileName = e.replace(/\\$/, '').split('//').pop();
            fileName = fileName.replace(/\\$/, '').split(/\\/).pop();
        }

        $scope.bradcrumbList.push(
               {
                   FolderName: fileName,
                   Path: e
               }
               );
        //if ($scope.bradcrumbList != null && $scope.bradcrumbList.length > 0) {
        //    if ($scope.bradcrumbList.length == 1) {



        //    }

        //}
        //else {
        //    $scope.bradcrumbList.push({ FolderName: "File", Path: $scope.getCustomerFolder });
        //}

        $scope.allFilesLoad();
        $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").removeAttr("style")
        $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").removeAttr("style")
        $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:none !important");
        $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:none !important");
        $("#copymanagebtn").hide();
        $("#cutmangebtn").hide();
        $("#compressmanagebtn").hide();
        $("#HyperLinkmanagebtn").hide();
        $("#deletemanagebtn").hide();
        $("#renamemanagebtn").hide();
        $("#exportmanagebtn").hide();
        $("#downloadmanagebtn").hide();

    }

    $scope.allFilesLoad = function () {
        $scope.starredFlag = false;
        $scope.linkValues = [];
        dataFactory.GetImageFiles($scope.catalogId, $scope.getCustomerIDs, $scope.getCustomerFolder).success(function (response) {
            if (response != null && response.length > 0) {
                $scope.getImageData = response;
                //--------------------------------Youtube seperation loop-----------------------------------//
                angular.forEach($scope.getImageData, function (file) {
                    if (file.FolderName.startsWith("YouTube~")) {
                        var fileDetail = file.FolderName.split('~');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.linkValues.push({ FolderName: file.FolderName, Extention: "YouTube Link...", Path: file.Path, Link: convertedLink, ModifiedDate: file.ModifiedDate, Size: "-", Caption: captionVal, CustomerFolder: file.CustomerFolder, Customer: file.Customer, ExternalDriveFlag: file.ExternalDriveFlag });
                        //$scope.youTubeIndex.push($scope.getImageData.indexOf(file));                        
                        //$scope.getImageData.delete(value);
                    }
                });
                $scope.getImageData = jQuery.grep($scope.getImageData, function (value) {
                    return !(value.FolderName.startsWith("YouTube~"));
                });
                //$scope.getImageData = selectedList;
                //--------------------------------Youtube seperation loop-----------------------------------//
                //$scope.getImageData = response;
                $scope.IsEmpty = false;
                $rootScope.LoadImageData = $scope.getImageData;
                if ($scope.getImageData.length > 0) {
                    $scope.getCustomerFolder = $scope.getImageData[0].CustomerFolder;
                    $scope.cusFolder = $scope.getImageData[0].Customer;
                    $scope.extDriveFlag = $scope.getImageData[0].ExternalDriveFlag;
                }
                else {
                    $scope.getCustomerFolder = $scope.linkValues[0].CustomerFolder;
                    $scope.cusFolder = $scope.linkValues[0].Customer;
                    $scope.extDriveFlag = $scope.linkValues[0].ExternalDriveFlag;
                }
                //$scope.getCustomerFolder = $scope.getImageData[0].CustomerFolder;
                //var cusFolder = $scope.getImageData[0].Customer;
                //$scope.cusFolder = $scope.getImageData[0].Customer;
                $scope.newFolderPath = $scope.getCustomerFolder;
                $scope.newSearchPath = $scope.getCustomerFolder;
                //$scope.previosPath = $scope.getCustomerFolder;
                var previousPath = $scope.getCustomerFolder.substring(0, $scope.getCustomerFolder.lastIndexOf("\\"))
                var chkpreviousPath = previousPath + "\\"
                if (chkpreviousPath.contains("\\" + $scope.cusFolder + "\\") == true) {

                    $("#backbtn").show();
                    $scope.previosPath = previousPath;
                }
                else {
                    $("#backbtn").hide();
                }
                //-----Sorting

                $scope.propertyName = 'FolderName';
                $scope.reverse = true;
                $scope.getImageData = $filter('orderBy')($rootScope.LoadImageData, $scope.propertyName, $scope.reverse);
            }
            else {

                $scope.IsEmpty = true;
                var previousPath = $scope.getCustomerFolder.substring(0, $scope.getCustomerFolder.lastIndexOf("\\"))
                var chkpreviousPath = previousPath + "\\"
                if (chkpreviousPath.contains("\\" + $scope.cusFolder + "\\") == true) {

                    $("#backbtn").show();
                    $scope.previosPath = previousPath;
                }
                else {
                    $("#backbtn").hide();
                }
            }
        });
        $scope.recentTodayLinkValues = []; //youTube link values for recent today
        $scope.recentYesterdayLinkValues = []; //youTube link values for recent yesterday
        $scope.recentPreviousLinkValues = []; //youTube link values for recent previous
        dataFactory.recentFilesClick($scope.recentFilesPath).success(function (response) {

            if (response != null) {
                $scope.recentFileDetailsToday = response.FilesModifiedToday;
                $scope.recentFileDetailsYesterday = response.FilesModifiedyesterday;
                $scope.recentFileDetailsPrevious = response.OtherFiles;
                //--------------------------------Youtube seperation loop-----------------------------------//
                angular.forEach($scope.recentFileDetailsToday, function (file) {
                    if (file.FolderName.startsWith("YouTube~")) {
                        var fileDetail = file.FolderName.split('~');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.recentTodayLinkValues.push({ FolderName: file.FolderName, Extention: "YouTube Link...", Path: file.Path, Link: convertedLink, ModifiedDate: file.ModifiedDate, Size: "-", Caption: captionVal });
                    }
                });
                $scope.recentFileDetailsToday = jQuery.grep($scope.recentFileDetailsToday, function (value) {
                    return !(value.FolderName.startsWith("YouTube~"));
                });

                angular.forEach($scope.recentFileDetailsYesterday, function (file) {
                    if (file.FolderName.startsWith("YouTube~")) {
                        var fileDetail = file.FolderName.split('~');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.recentYesterdayLinkValues.push({ FolderName: file.FolderName, Extention: "YouTube Link...", Path: file.Path, Link: convertedLink, ModifiedDate: file.ModifiedDate, Size: "-", Caption: captionVal });
                    }
                });
                $scope.recentFileDetailsYesterday = jQuery.grep($scope.recentFileDetailsYesterday, function (value) {
                    return !(value.FolderName.startsWith("YouTube~"));
                });

                angular.forEach($scope.recentFileDetailsPrevious, function (file) {
                    if (file.FolderName.startsWith("YouTube~")) {
                        var fileDetail = file.FolderName.split('~');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.recentPreviousLinkValues.push({ FolderName: file.FolderName, Extention: "YouTube Link...", Path: file.Path, Link: convertedLink, ModifiedDate: file.ModifiedDate, Size: "-", Caption: captionVal });
                    }
                });
                $scope.recentFileDetailsPrevious = jQuery.grep($scope.recentFileDetailsPrevious, function (value) {
                    return !(value.FolderName.startsWith("YouTube~"));
                });
                //--------------------------------Youtube seperation loop-----------------------------------//
            }

        });
    }

    $("#custom-menu1").hide();
    $("#Newdiv").hide();
    $("#MainFolder").hide();
    $("#SubFolder").hide();
    $scope.ContextMenu = function (dataItem, event, fileType) {
        //
        $scope.PreviewPath = dataItem.PreviewPath;
        $scope.newFolderPath = dataItem.Path;
        $scope.newText = dataItem.FolderName.replace(dataItem.Extention, "");

        //$("#MainFolder").hide();
        //$("#SubFolder").show();
        //$("#custom-menu1").show();
        //$("#Newdiv").hide();
        //     $("#mainGrid").addClass("context-menu-one btn btn-neutral");
        $scope.IsFileType = fileType;
        event.stopPropagation();
        return false;
    }
    $scope.RecentContextMenu = function (dataItem, event, fileType) {
        //
        $scope.newFolderPath = dataItem.FullName;
        $scope.newText = dataItem.Name.replace(dataItem.Extention, "");
        $scope.IsFileType = fileType;
        event.stopPropagation();
        return false;
    }

    $scope.YoutubeContextMenu = function (dataItem, event, fileType) {
        //
        $scope.newFolderPath = dataItem.FullName;
        $scope.newText = dataItem.Caption.replace(".rtf", "");
        $scope.IsFileType = fileType;
        event.stopPropagation();
        return false;
    }

    $scope.ContextMainMenu = function (dataItem) {

        $scope.newFolderPath = dataItem;
        $("#MainFolder").show();
        $("#SubFolder").hide();
        $("#custom-menu1").show();
        $("#Newdiv").hide();
    }

    $scope.Create = function () {
        $("#custom-menu1").hide();
        $("#Newdiv").hide();
        $scope.newText = "";
        $scope.IsCreate = "Create";
        $("#newFolderPopUp").show();
        $("#Foldertext").focus();
        // var val = $scope.newText;
    }
    $scope.Rename = function () {

        if ($rootScope.userRoleAssetModify == true) {
            $("#custom-menu1").hide();
            $("#Newdiv").hide();
            $scope.IsCreate = "Rename";
            $("#renameFolderPopUp").show();
            $("#copymanagebtn").show();
            $("#cutmangebtn").show();
            $("#compressmanagebtn").show();
            $("#deletemanagebtn").show();
            $("#exportmanagebtn").show();
            $("#downloadmanagebtn").show();
            $scope.selectedRows = [];
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            //alert('You do not have rights to modify files.')
        }
    }

    $scope.Compress = function () {
        if ($rootScope.userRoleAssetModify == true) {
            $("#custom-menu1").hide();
            $("#Newdiv").hide();
            $scope.getAllSelectedRows();
            if ($scope.linkValues.length > 0) {
                $scope.getAllSelectedYoutubeRows();
            }
            if ($scope.selectedRows.length == 0) {
                $scope.getAllSelectedRecentRows();
            }
            dataFactory.CompressImageManagementDetails($scope.getCustomerFolder, $localStorage.getCustomerID, $scope.IsFileType, $scope.selectedRows).success(function (response) {
                if (response != "") {
                    //
                    // alert(response);
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                    $scope.allFilesLoad();
                }
                else {
                    // $scope.seletedAttribute = "";
                }
                $("#copymanagebtn").hide();
                $("#cutmangebtn").hide();
                $("#pastemanagebtn").hide();
                $("#HyperLinkmanagebtn").hide();
                $("#compressmanagebtn").hide();
                $("#deletemanagebtn").hide();
                $("#renamemanagebtn").hide();
                $("#exportmanagebtn").hide();
                $("#downloadmanagebtn").hide();
                $scope.selectedRows = [];
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            //alert('You do not have rights to modify files.');
        }

    }

    $scope.Copy = function () {
        //
        if ($rootScope.userRoleAssetModify == true) {
            $scope.getAllSelectedRows();
            if ($scope.linkValues.length > 0) {
                $scope.getAllSelectedYoutubeRows();
            }
            if ($scope.selectedRows.length == 0) {
                $scope.getAllSelectedRecentRows();
            }
            //
            $scope.IsEnabled = true;
            $scope.displaymanagepaste = true;
            $scope.PasteType = 0;
            $scope.IsCopy = true;
            $("#copymanagebtn").show();
            $("#pastemanagebtn").show();
            $("li.context-menu-item.context-menu-icon.context-menu-icon-paste").removeAttr("style")
            $("li.context-menu-item.context-menu-icon.context-menu-icon-paste").attr("style", "display:block !important");
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            //alert('You do not have rights to modify files.');
        }
    }

    $scope.Cut = function () {
        if ($rootScope.userRoleAssetModify == true) {

            $scope.getAllSelectedRows();
            if ($scope.linkValues.length > 0) {
                $scope.getAllSelectedYoutubeRows();
            }
            if ($scope.selectedRows.length == 0) {
                $scope.getAllSelectedRecentRows();
            }

            $scope.displaymanagepaste = true;
            $scope.IsEnabled = true;
            $scope.PasteType = 1;
            $scope.IsCopy = true;
            $("#cutmangebtn").show();
            $("#pastemanagebtn").show();
            $("li.context-menu-item.context-menu-icon.context-menu-icon-paste").removeAttr("style")
            $("li.context-menu-item.context-menu-icon.context-menu-icon-paste").attr("style", "display:block !important");
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            //alert('You do not have rights to modify files.');
        }
    }

    $scope.Paste = function (getCustomerFolder) {
        if ($rootScope.userRoleAssetModify == true) {
            if ($scope.PasteType == 0 || $scope.PasteType == 1) {
                $scope.displaymanagepaste = false;

                dataFactory.CutCopyPasteDetails($scope.selectedRows, getCustomerFolder, $localStorage.getCustomerID, $scope.PasteType, $scope.IsFileType).success(function (response) {
                    if (response != "") {
                        //  alert(response);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });
                        $scope.allFilesLoad();
                    }
                });
                $("#copymanagebtn").hide();
                $("#cutmangebtn").hide();
                $("#pastemanagebtn").hide();
                $("#HyperLinkmanagebtn").hide();
                $("#compressmanagebtn").hide();
                $("#deletemanagebtn").hide();
                $("#renamemanagebtn").hide();
                $("#exportmanagebtn").hide();
                $("#downloadmanagebtn").hide();
                $scope.selectedRows = [];
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Cut/Copy a file before paste.',
                    type: "info"
                });
            }
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            // alert('You do not have rights to modify files.');
        }
    }

    $scope.IsCopy = false;

    $scope.Cancel = function () {
        if ($rootScope.userRoleAssetModify == true || $rootScope.userRoleAssetRemove == true) {
            $("#newFolderPopUp").hide();
            $("#renameFolderPopUp").hide();
            $scope.newText = "";
        }
    }


    $scope.selectright = function (item) {
        if ($scope.recentFlag == true && $scope.starredFlag == false) {
            $scope.rightMenuRecent();
            $("#copymanagebtn").show();
            $("#cutmangebtn").show();
            $("#deletemanagebtn").show();
            $("#downloadmanagebtn").show();
        }
        else if ($scope.recentFlag == false && $scope.starredFlag == false) {

            $scope.rightMenu();
            $("#copymanagebtn").show();
            $("#cutmangebtn").show();
            $("#compressmanagebtn").show();
            $("#deletemanagebtn").show();
            $("#exportmanagebtn").show();
            $("#downloadmanagebtn").show();
            $scope.getSelectedFiles();
            if ($scope.selectedFiles.length != 0) {
                var a = 0;
                var selectRows = $scope.selectedFiles;


                for (var i = 0; i < selectRows.length; i++) {
                    selectRows[i].selected = true;
                    //    if (selectRows[i] == item && selectRows.length > 1) {
                    //        item.selected = true;
                    //    }
                    //    if (selectRows[i] != item) {

                    //        a++;
                    //        if (a == selectRows.length) {
                    //            item.selected ? item.selected = false : item.selected = true;
                    //        }
                    //    }
                }
            }

        }
        //else if ($scope.recentFlag == false && $scope.starredFlag == true) {
        //    $scope.starredRightMenu(item)
        //}

        $scope.RenameFolder = item.Path.toString();
        item.selected = true;

    }

    $scope.rightMenu = function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {

                if (key == "rename") {
                    $scope.Rename();
                }
                else if (key == "compress") {
                    $scope.Compress();
                }
                else if (key == "copy") {
                    $scope.Copy();
                }
                else if (key == "cut") {
                    $scope.Cut();
                }
                else if (key == "paste") {
                    $scope.Paste($scope.getCustomerFolder);
                }
                else if (key == "HyperLink") {
                    $scope.HyperLink();
                }
                else if (key == "delete") {
                    $scope.Delete();
                }
                else if (key == "download") {
                    $scope.Download();
                }
                else if (key == "addToStarred") {
                    $scope.AddToStarred();
                }
                else if (key == "extract") {
                    $scope.Extract();
                }
                else if (key == "find") {
                    $scope.FindAssociateImages();
                }
                else if (key == "assign") {
                    $scope.AssignImages();
                }
                else if (key == "default") {
                    $scope.Default();
                }
                else {
                    return 'context-menu-icon context-menu-icon-quit';
                }
                //var m = "clicked: " + key;
                //window.console && console.log(m) || alert(m);
            },

            items: {
                "rename": { name: "Rename", icon: "edit" },
                "cut": { name: "Cut", icon: "fa-cut" },
                "copy": { name: "Copy", icon: "fa-copy" },
                "paste": { name: "Paste", icon: "fa-paste" },
                "HyperLink": { name: "Get-HyperLink", icon: "fa-link" },
                "compress": { name: "Compress", icon: "fa-compress", style: "padding-top: 7px" },
                "extract": { name: "Extract", icon: "fa-file-archive-o" },
                "download": { name: "Download", icon: "fa-cloud-download" },
                //Newly add for Starred
                "addToStarred": { name: "Add to Starred", icon: "fa-star" },
                "find": { name: "Find", icon: "fa-search" },
                "assign": { name: "Assign", icon: "fa-plus" },
                "delete": { name: "Delete", icon: "delete" },
                //"default": { name: "Set Default Path", icon: "fa-arrow-right" },
                "sep1": "---------",
                "quit": {
                    name: "Cancel", icon: function () {
                        return 'context-menu-icon context-menu-icon-quit';
                    }
                }
            }

        });

        $('.context-menu-one').on('click', function (e) {
            console.log('clicked', this);
        })
    };

    $scope.selectStarredRight = function (item) {

        

        if ($scope.starredFlag == true)
         {
            $scope.starredRightMenu(item)
         }
        //else {
        //    $scope.selectright
        //}


    }

    $scope.starredRightMenu = function (item) {

        $scope.selectedRemovalFile = item.FILE_NAME;
        $.contextMenu({
            selector: '.context-menu-one2',
            callback: function (key, options) {
                if (key == "removeFromStarred") {
                    $scope.RemovefromStarred($scope.selectedRemovalFile);
                }
                else {
                    return 'context-menu-icon context-menu-icon-quit';
                }
            },

            items: {
                //Newly add for Starred
                "removeFromStarred": { name: "Remove from Starred", icon: "fa-star-o" },
                "sep1": "---------",
                "quit": {
                    name: "Cancel", icon: function () {
                        return 'context-menu-icon context-menu-icon-quit';
                    }
                }
            }

        });

        $('.context-menu-one').on('click', function (e) {
            console.log('clicked', this);
        })
    };

    $scope.rightMenuRecent = function () {

        $.contextMenu({
            selector: '.context-menu-one1',
            callback: function (key, options) {

                if (key == "rename") {
                    $scope.Rename();
                }
                else if (key == "copy") {
                    $scope.Copy();
                }
                else if (key == "cut") {
                    $scope.Cut();
                }
                else if (key == "paste") {
                    $scope.Paste($scope.getCustomerFolder);
                }
                else if (key == "addToStarred") {
                    $scope.AddToStarred();
                }
                else if (key == "delete") {
                    $scope.Delete();
                }
                else if (key == "download") {
                    $scope.Download();
                }
                else if (key == "default") {
                    $scope.Default();
                }
                else {
                    return 'context-menu-icon context-menu-icon-quit';
                }
                //var m = "clicked: " + key;
                //window.console && console.log(m) || alert(m);
            },

            items: {
                "rename": { name: "Rename", icon: "edit" },
                "cut": { name: "Cut", icon: "fa-cut" },
                "copy": { name: "Copy", icon: "fa-copy" },
                "paste": { name: "Paste", icon: "fa-paste" },
                //Newly add for Starred
                "addToStarred": { name: "Add to Starred", icon: "fa-star" },
                "download": { name: "Download", icon: "fa-cloud-download" },
                "delete": { name: "Delete", icon: "delete" },
                "sep1": "---------",
                "quit": {
                    name: "Cancel", icon: function () {
                        return 'context-menu-icon context-menu-icon-quit';
                    }
                }
            }

        });

        $('.context-menu-one').on('click', function (e) {
            console.log('clicked', this);
        })
    };

    $scope.selectFileforUpload = function (file) {
        // blockUI.start();
        debugger;
        $q.all([$scope.selectFileforUpload1(file)]).then(function (response) {
            return;
        });
    }

    $scope.selectFileforUpload1 = function (file) {
        this.value = null;
        $scope.isSuccessFlag = false;
        var filesCount = 0;
        if (file[0].size > 97136867) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'File size is large to upload.',
                type: "Info"
            });
        }

        else {
            for (var i = 0; i < file.length; i++) {
                var formDataImage = new FormData();
                formDataImage.append("file", file[i]);



                $http.post("/ImageManagement/SaveFilesManageDrive?customerFolder=" + $scope.getCustomerFolder + "", formDataImage,
                  {
                      withCredentials: true,
                      headers: { 'Content-Type': undefined },
                      transformRequest: angular.identity
                  })
                  .success(function (d) {

                      if (d == "") {

                          $scope.allFilesLoad();
                      }
                      else {
                          $scope.allFilesLoad();
                      }

                  }).error(function () {

                      $.msgBox({
                          title: $localStorage.ProdcutTitle,
                          content: 'File upload failed, please try again',
                          type: "error"
                      });
                  });
                $scope.isSuccessFlag = true;
                filesCount++;
                //  $scope.allFilesLoad();                
            }
            if ($scope.isSuccessFlag == true && filesCount == file.length) {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File uploaded successfully',
                    type: "Info"
                });
            }
        }

       

    };

    //Newly added for Starred
    $scope.AddToStarred = function () {
        if ($scope.IsFileType != "folder") {
            angular.element("input[type='file']").val(null);
            $("#custom-menu1").hide();
            $("#Newdiv").hide();
            $scope.getAllSelectedRows();
            if ($scope.linkValues.length > 0) {
                $scope.getAllSelectedYoutubeRows();
            }
            if ($scope.selectedRows.length == 0) {
                $scope.getAllSelectedRecentRows();
            }

            $scope.UserName = $localStorage.getUserName;
            $scope.CatalogId = $localStorage.getCatalogID;

            $scope.selectedFileCount = $scope.selectedRows.length;

            dataFactory.addtoStarred($scope.newFolderPath, $scope.UserName, $scope.CatalogId, $scope.IsFileType, $scope.selectedFileCount, $scope.selectedRows).success(function (response) {
                if (response != "") {
                    //
                    //alert(response);
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '',
                        type: "info"
                    });
                    $("#copymanagebtn").hide();
                    $("#cutmangebtn").hide();
                    $("#pastemanagebtn").hide();
                    $("#compressmanagebtn").hide();
                    $("#deletemanagebtn").hide();
                    $("#renamemanagebtn").hide();
                    $("#exportmanagebtn").hide();
                    $("#downloadmanagebtn").hide();
                    $scope.allFilesLoad();
                }
            });

        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Starred, folders cannot be added',
                type: "info"
            });
        }
    }


    $scope.RemovefromStarred = function (selectedRemovalFile) {
            
            
        $scope.UserName = $localStorage.getUserName;
        //$scope.FileName = item.FILE_NAME;
        $scope.FileName = selectedRemovalFile

        dataFactory.deleteFromStarred($scope.UserName, $scope.FileName).success(function (response) {
                if (response != "") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '',
                        type: "info"
                    });
                    $scope.starredFilesClick();
                }
            });
    }


    //Newly added for Starred End

    $scope.Delete = function () {
        //
        if ($rootScope.userRoleAssetRemove == true) {
            angular.element("input[type='file']").val(null);
            $("#custom-menu1").hide();
            $("#Newdiv").hide();
            var bool = false;
            $scope.getAllSelectedRows();
            if ($scope.linkValues.length > 0) {
                $scope.getAllSelectedYoutubeRows();
            }
            if ($scope.selectedRows.length == 0) {
                $scope.getAllSelectedRecentRows();
            }

            $scope.selectedFileCount = $scope.selectedRows.length;
            //
            if ($scope.selectedRows.length == 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Do you like to delete this file?", // move to Recycle Bin
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result1) {

                        if (result1 === "Yes") {
                            bool = true;
                        }
                        if (bool == true) {
                            dataFactory.DeleteImageManagementDetails($scope.newFolderPath, $localStorage.getCustomerID, $scope.IsFileType, $scope.selectedFileCount, $scope.selectedRows).success(function (response) {
                                if (response != "") {
                                    //
                                    //alert(response);
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response + '',
                                        type: "info"
                                    });
                                    $("#copymanagebtn").hide();
                                    $("#cutmangebtn").hide();
                                    $("#pastemanagebtn").hide();
                                    $("#compressmanagebtn").hide();
                                    $("#deletemanagebtn").hide();
                                    $("#renamemanagebtn").hide();
                                    $("#exportmanagebtn").hide();
                                    $("#downloadmanagebtn").hide();
                                    $scope.allFilesLoad();
                                }
                                else {
                                    // $scope.seletedAttribute = "";
                                }
                            });
                        }

                    }
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Do you like to delete this files?", // move to Recycle Bin
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result1) {

                        if (result1 === "Yes") {
                            bool = true;
                        }
                        if (bool == true) {
                            dataFactory.DeleteImageManagementDetails($scope.newFolderPath, $localStorage.getCustomerID, $scope.IsFileType, $scope.selectedFileCount, $scope.selectedRows).success(function (response) {
                                if (response != "") {
                                    //
                                    //alert(response);
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response + '',
                                        type: "info"
                                    });
                                    $("#copymanagebtn").hide();
                                    $("#cutmangebtn").hide();
                                    $("#pastemanagebtn").hide();
                                    $("#compressmanagebtn").hide();
                                    $("#deletemanagebtn").hide();
                                    $("#renamemanagebtn").hide();
                                    $("#exportmanagebtn").hide();
                                    $("#downloadmanagebtn").hide();
                                    $scope.allFilesLoad();
                                }
                                else {
                                    // $scope.seletedAttribute = "";
                                }
                            });
                        }

                    }
                });
            }

            //$.msgBox({
            //    title: $localStorage.ProdcutTitle,
            //    content: "Do you like to delete this files?", // move to Recycle Bin
            //    type: "confirm",
            //    buttons: [{ value: "Yes" }, { value: "No" }],
            //    success: function (result1) {

            //        if (result1 === "Yes") {
            //            bool = true;
            //        }
            //        if (bool == true) {
            //            dataFactory.DeleteImageManagementDetails($scope.newFolderPath, $localStorage.getCustomerID, $scope.IsFileType, $scope.selectedRows).success(function (response) {
            //                if (response != "") {
            //                    //
            //                    //alert(response);
            //                    $.msgBox({
            //                        title: $localStorage.ProdcutTitle,
            //                        content: '' + response + '',
            //                        type: "info"
            //                    });
            //                    $("#copymanagebtn").hide();
            //                    $("#cutmangebtn").hide();
            //                    $("#pastemanagebtn").hide();
            //                    $("#compressmanagebtn").hide();
            //                    $("#deletemanagebtn").hide();
            //                    $("#renamemanagebtn").hide();
            //                    $("#exportmanagebtn").hide();
            //                    $("#downloadmanagebtn").hide();
            //                    $scope.allFilesLoad();
            //                }
            //                else {
            //                    // $scope.seletedAttribute = "";
            //                }
            //            });
            //        }

            //    }
            //});
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to remove files.',
                type: "info"
            });
            //alert('You do not have rights to remove files.');
        }
    }

    $scope.Download = function () {
        //

        if ($rootScope.userRoleAssetModify == true) {

            $scope.getAllSelectedRows();
            if ($scope.selectedRows.length == 0) {
                $scope.getAllSelectedRecentRows();
            }
            //
            //var filename;
            //for (var i = 0; i < $scope.selectedRows.length; i++) {s
            //    var windowlocation = window.location.origin;

            //    filename = $scope.selectedRows[i].FolderName;
            //    windowlocation = $scope.selectedRows[i].Path;
            //    window.open("DowloadImageManagementFiles.ashx?Path=" + $scope.selectedRows[i].Path + "&Type=" + $scope.selectedRows[i].Extention);

            //}
            if ($scope.selectedRows.length > 0) {
                var encryptedPath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.newFolderPath), key, {
                    keySize: 128 / 8,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });
                dataFactory.ExportCompressImageManagementDetails(encryptedPath, $localStorage.getCustomerID, $scope.IsFileType, $scope.selectedRows).success(function (response) {
                    if (response != "") {
                        //
                        // alert(response);
                        $scope.exportCompressPath = response;
                        var windowlocation = window.location.origin;

                        filename = $scope.exportCompressPath;
                        windowlocation = $scope.exportCompressPath;
                        window.open("DowloadImageManagementFiles.ashx?Path=" + $scope.exportCompressPath + "&Type=.zip");

                    }
                });
            }
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            //alert('You do not have rights to modify files.');
        }
    }

    $scope.Extract = function () {
        if ($rootScope.userRoleAssetModify == true) {
            $scope.getAllSelectedRows();
            if ($scope.selectedRows.length == 0) {
                $scope.getAllSelectedRecentRows();
            }
            dataFactory.ExtractDetails($scope.selectedRows, $scope.getCustomerFolder, $localStorage.getCustomerID).success(function (response) {
                //if (response != "") {
                if (response != "") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '',
                        type: "Info"
                    });
                    $("#copymanagebtn").hide();
                    $("#cutmangebtn").hide();
                    $("#pastemanagebtn").hide();
                    $("#compressmanagebtn").hide();
                    $("#deletemanagebtn").hide();
                    $("#renamemanagebtn").hide();
                    $("#exportmanagebtn").hide();
                    $("#downloadmanagebtn").hide();

                }



                //    alert(response);
                $scope.allFilesLoad();

            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'You do not have rights to modify files.',
                type: "info"
            });
            //alert('You do not have rights to modify files.');
        }
    }

    $scope.BackFolder = function () {


        $scope.getCustomerFolder = $scope.previosPath;
        $scope.allFilesLoad();
    }

    //For control key select on main asset page
    $scope.getSelectedFiles = function () {
        var selectedFiles = $filter("filter")($scope.getImageData, {
            selected: true
        }, true);
        var selectedYoutubeFiles = $filter("filter")($scope.linkValues, {
            selected: true
        }, true);
        var selectedFilesFinal = selectedFiles.concat(selectedYoutubeFiles);
        $scope.selectedFiles = selectedFilesFinal;
        $scope.IsCopy = true;
    }

    $scope.select = function (item, $event) {

        var getSelectedClass = [];
        if ($scope.IsDisplay == true && $scope.IsThumbnail == true) {
            getSelectedClass = document.getElementById("filesdiv").querySelectorAll(".selected");
          //  getSelectedClass = document.getElementById("StarredFilesdiv").querySelectorAll(".selected");
        }
        else if ($scope.IsDisplay == false) {
            getSelectedClass = document.getElementById("griddiv").querySelectorAll(".selected");
           // getSelectedClass = document.getElementById("StarredGriddiv").querySelectorAll(".selected");
        }
        else if ($scope.IsThumbnail == false) {
            getSelectedClass = document.getElementById("Thumbnail").querySelectorAll(".selected");
           // getSelectedClass = document.getElementById("StarredThumbnail").querySelectorAll(".selected");
        }
        var control = $event.ctrlKey;
        var shift = $event.shiftKey;
        if (control == false && shift == false) {
            $scope.getSelectedFiles();
            if ($scope.selectedFiles.length != 0) {
                var a = 0;
                var selectRows = $scope.selectedFiles;
                for (var i = 0; i < selectRows.length; i++) {
                    selectRows[i].selected = false;
                    if (selectRows[i] == item && selectRows.length > 1) {
                        item.selected = true;
                    }
                    if (selectRows[i] != item) {

                        a++;
                        if (a == selectRows.length) {
                            item.selected ? item.selected = false : item.selected = true;
                        }
                    }
                }
            }
        }
        else if (control == false && shift == true) {
            $scope.getSelectedFiles();
            if ($scope.selectedFiles.length > 0) {
                var firtSelection = [];
                if ($scope.selectedFiles.length == 1) {
                    $scope.firtSelection = $scope.selectedFiles[0];
                }
                var initialCount = 0;
                var finalCount = 0;
                for (var k = 0; k < $scope.getImageData.length; k++) {
                    if ($scope.firtSelection == $scope.getImageData[k]) {
                        initialCount = k;
                    }
                    if (item == $scope.getImageData[k]) {
                        finalCount = k;
                    }
                }
                if (initialCount > finalCount) {
                    for (var l = finalCount; l <= initialCount; l++) {
                        $scope.getImageData[l].selected = true;
                    }
                    if (finalCount != 0) {
                        for (var l = 0; l < finalCount; l++) {
                            $scope.getImageData[l].selected = false;
                        }
                    }
                    if (initialCount != $scope.getImageData.length - 1) {
                        for (var l = initialCount + 1; l <= $scope.getImageData.length - 1; l++) {
                            $scope.getImageData[l].selected = false;
                        }
                    }

                }
                else {
                    for (var l = initialCount; l <= finalCount; l++) {
                        $scope.getImageData[l].selected = true;
                    }
                    if (initialCount != 0) {
                        for (var l = 0; l < initialCount; l++) {
                            $scope.getImageData[l].selected = false;
                        }
                    }
                    if (finalCount != $scope.getImageData.length - 1) {
                        for (var l = finalCount + 1; l <= $scope.getImageData.length - 1; l++) {
                            $scope.getImageData[l].selected = false;
                        }
                    }
                }

            }
        }
        if ($scope.selectedFiles.length == 0 || control == true) {
            item.selected ? item.selected = false : item.selected = true;
        }
        $("li.context-menu-item context-menu-icon context-menu-icon--fa fa fa-cut").removeAttr("style")
        $("li.context-menu-item context-menu-icon context-menu-icon--fa fa fa-copy").removeAttr("style")
        if ($scope.selectedRows.length > 1 || getSelectedClass.length > 1 || item.selected == true) {
            $("li.context-menu-item context-menu-icon context-menu-icon--fa fa fa-cut").attr("style", "display:block !important");
            $("li.context-menu-item context-menu-icon context-menu-icon--fa fa fa-copy").attr("style", "display:block !important");
            $("#copymanagebtn").show();
            $("#cutmangebtn").show();
            $("#compressmanagebtn").show();
            $("#deletemanagebtn").show();
            $("#renamemanagebtn").show();
            $("#exportmanagebtn").show();
            $("#downloadmanagebtn").show();
        }
        else {
            $("li.context-menu-item context-menu-icon context-menu-icon--fa fa fa-cut").attr("style", "display:none !important");
            $("li.context-menu-item context-menu-icon context-menu-icon--fa fa fa-copy").attr("style", "display:none !important");
            $("#copymanagebtn").hide();
            $("#cutmangebtn").hide();
            $("#HyperLinkmanagebtn").hide();
            $("#compressmanagebtn").hide();
            $("#deletemanagebtn").hide();
            $("#renamemanagebtn").hide();
            $("#exportmanagebtn").hide();
            $("#downloadmanagebtn").hide();
        }

        //Rename folder
        $scope.RenameFolder = item.Path.toString();
        $event.stopPropagation();
        $event.preventDefault();
    }

    //For control key select on search result
    $scope.getSelectedSearchFiles = function () {
        var selectedFilesAll = $filter("filter")($scope.getSearchResult, {
            selected: true
        }, true);
        var selectedSearchFilesYouLink = $filter("filter")($scope.searchlinkValues, {
            selected: true
        }, true);
        var selectedFiles = selectedFilesAll.concat(selectedSearchFilesYouLink);
        $scope.selectedFiles = selectedFiles;
        $scope.IsCopy = true;
    }

    $scope.selectSearch = function (item, $event) {

        var getSelectedClass = [];
        if ($scope.IsDisplay == true && $scope.IsThumbnail == true) {
            getSelectedClass = document.getElementById("searchdivgrid").querySelectorAll(".selected");
        }
        else if ($scope.IsDisplay == false) {
            getSelectedClass = document.getElementById("searchdiv").querySelectorAll(".selected");
        }
        else if ($scope.IsThumbnail == false) {
            getSelectedClass = document.getElementById("searchThumbnail").querySelectorAll(".selected");
        }
        var control = $event.ctrlKey;
        if (control == false) {
            $scope.getSelectedSearchFiles();
            if ($scope.selectedFiles.length != 0) {
                var a = 0;
                var selectRows = $scope.selectedFiles;
                for (var i = 0; i < selectRows.length; i++) {
                    selectRows[i].selected = false;
                    if (selectRows[i] == item && selectRows.length > 1) {
                        item.selected = true;
                    }
                    if (selectRows[i] != item) {

                        a++;
                        if (a == selectRows.length) {
                            item.selected ? item.selected = false : item.selected = true;
                        }
                    }
                }
            }
        }
        if ($scope.selectedFiles.length == 0 || control == true) {
            item.selected ? item.selected = false : item.selected = true;
        }
        $event.stopPropagation();
        $event.preventDefault();
        //item.selected ? item.selected = false : item.selected = true;
    }



    //For loading the recent files tab
    $scope.recentFilesClick = function (e) {

        $scope.starredFlag = false;
        $scope.getIsRecentList = true;
        $scope.getSizeBasedFilterResult = false;
        $scope.getDateBasedFilterResult = false;
        $scope.displayStarredAssets = false;
        $scope.recentFlag = true;
        $scope.starredFlag = false;
        $scope.assetSizeValue = "";
        $scope.recentTodayLinkValues = []; //youTube link values for recent today
        $scope.recentYesterdayLinkValues = []; //youTube link values for recent yesterday
        $scope.recentPreviousLinkValues = []; //youTube link values for recent previous
        dataFactory.recentFilesClick($scope.recentFilesPath).success(function (response) {
            if (response != null) {
                $scope.recentFileDetailsToday = response.FilesModifiedToday;
                $scope.recentFileDetailsYesterday = response.FilesModifiedyesterday;
                $scope.recentFileDetailsPrevious = response.OtherFiles;
                //--------------------------------Youtube seperation loop-----------------------------------//
                angular.forEach($scope.recentFileDetailsToday, function (file) {
                    if (file.FolderName.startsWith("YouTube_")) {
                        var fileDetail = file.FolderName.split('_');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.recentTodayLinkValues.push({ FolderName: file.FolderName, Link: convertedLink, Extention: "YouTube Link...", ModifiedDate: file.ModifiedDate, Size: "-", Path: file.Path, Caption: captionVal });
                    }
                });
                $scope.recentFileDetailsToday = jQuery.grep($scope.recentFileDetailsToday, function (value) {
                    return !(value.FolderName.startsWith("YouTube_"));
                });

                angular.forEach($scope.recentFileDetailsYesterday, function (file) {
                    if (file.FolderName.startsWith("YouTube_")) {
                        var fileDetail = file.FolderName.split('_');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.recentYesterdayLinkValues.push({ FolderName: file.FolderName, Link: convertedLink, Extention: "YouTube Link...", ModifiedDate: file.ModifiedDate, Size: "-", Path: file.Path, Caption: captionVal });
                    }
                });
                $scope.recentFileDetailsYesterday = jQuery.grep($scope.recentFileDetailsYesterday, function (value) {
                    return !(value.FolderName.startsWith("YouTube_"));
                });

                angular.forEach($scope.recentFileDetailsPrevious, function (file) {
                    if (file.FolderName.startsWith("YouTube_")) {
                        var fileDetail = file.FolderName.split('_');
                        var linkVal = 'https://www.youtube.com/embed/' + fileDetail[1];
                        var convertedLink = $sce.trustAsResourceUrl(linkVal);
                        var captionVal = fileDetail[2].replace('.rtf', '');
                        $scope.recentPreviousLinkValues.push({ FolderName: file.FolderName, Link: convertedLink, Extention: "YouTube Link...", ModifiedDate: file.ModifiedDate, Size: "-", Path: file.Path, Caption: captionVal });
                    }
                });
                $scope.recentFileDetailsPrevious = jQuery.grep($scope.recentFileDetailsPrevious, function (value) {
                    return !(value.FolderName.startsWith("YouTube_"));
                });
                //--------------------------------Youtube seperation loop-----------------------------------//
                $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").removeAttr("style")
                $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").removeAttr("style")
                $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:none !important");
                $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:none !important");
                $("#copymanagebtn").hide();
                $("#cutmangebtn").hide();
                $("#compressmanagebtn").hide();
                $("#HyperLinkmanagebtn").hide();
                $("#deletemanagebtn").hide();
                $("#renamemanagebtn").hide();
                $("#exportmanagebtn").hide();
                $("#downloadmanagebtn").hide();
                //$("#SizeBasedFilterFilesdiv").hide();
                //$("#SizeBasedFilterGriddiv").hide();
                //$("#SizeBasedFilterThumbnail").hide();
            }
        });
    };

    //For the cut/copy/paste/download/compress fuctionalities in recent files page
    $scope.getAllSelectedRecentRows = function () {
        $scope.starredFlag = false;
        $scope.selectedRows = [];
        var selectedRowsToday = $filter("filter")($scope.recentFileDetailsToday, {
            selected: true
        }, true);
        var selectedRowsYesterday = $filter("filter")($scope.recentFileDetailsYesterday, {
            selected: true
        }, true);
        var selectedRowsPrevious = $filter("filter")($scope.recentFileDetailsPrevious, {
            selected: true
        }, true);
        var selectedRowsTodayLinks = $filter("filter")($scope.recentTodayLinkValues, {
            selected: true
        }, true);
        var selectedRowsYesterdayLinks = $filter("filter")($scope.recentYesterdayLinkValues, {
            selected: true
        }, true);
        var selectedRowsPreviousLinks = $filter("filter")($scope.recentPreviousLinkValues, {
            selected: true
        }, true);
        if (selectedRowsToday.length > 0) {
            for (var i = 0; i < selectedRowsToday.length; i++) {
                $scope.selectedRows.push(selectedRowsToday[i]);
            }
        }
        if (selectedRowsYesterday.length > 0) {
            for (var j = 0; j < selectedRowsYesterday.length; j++) {
                $scope.selectedRows.push(selectedRowsYesterday[j]);
            }
        }
        if (selectedRowsPrevious.length > 0) {
            for (var k = 0; k < selectedRowsPrevious.length; k++) {
                $scope.selectedRows.push(selectedRowsPrevious[k]);
            }
        }
        $scope.selectedRows = $scope.selectedRows.concat(selectedRowsTodayLinks);
        $scope.selectedRows = $scope.selectedRows.concat(selectedRowsYesterdayLinks);
        $scope.selectedRows = $scope.selectedRows.concat(selectedRowsPreviousLinks);
        $scope.IsCopy = true;
    }

    $scope.getSelectedRecentFiles = function () {
        $scope.starredFlag = false;
        $scope.selectedFiles = [];
        var selectedRowsToday = $filter("filter")($scope.recentFileDetailsToday, {
            selected: true
        }, true);
        var selectedRowsYesterday = $filter("filter")($scope.recentFileDetailsYesterday, {
            selected: true
        }, true);
        var selectedRowsPrevious = $filter("filter")($scope.recentFileDetailsPrevious, {
            selected: true
        }, true);
        var selectedRowsTodayLinks = $filter("filter")($scope.recentTodayLinkValues, {
            selected: true
        }, true);
        var selectedRowsYesterdayLinks = $filter("filter")($scope.recentYesterdayLinkValues, {
            selected: true
        }, true);
        var selectedRowsPreviousLinks = $filter("filter")($scope.recentPreviousLinkValues, {
            selected: true
        }, true);
        if (selectedRowsToday.length > 0) {
            for (var i = 0; i < selectedRowsToday.length; i++) {
                $scope.selectedFiles.push(selectedRowsToday[i]);
            }
        }
        if (selectedRowsYesterday.length > 0) {
            for (var j = 0; j < selectedRowsYesterday.length; j++) {
                $scope.selectedFiles.push(selectedRowsYesterday[j]);
            }
        }
        if (selectedRowsPrevious.length > 0) {
            for (var k = 0; k < selectedRowsPrevious.length; k++) {
                $scope.selectedFiles.push(selectedRowsPrevious[k]);
            }
        }
        $scope.selectedFiles = $scope.selectedFiles.concat(selectedRowsTodayLinks);
        $scope.selectedFiles = $scope.selectedFiles.concat(selectedRowsYesterdayLinks);
        $scope.selectedFiles = $scope.selectedFiles.concat(selectedRowsPreviousLinks);
        $scope.IsCopy = true;
    }

    $scope.selectRecent = function (item, $event) {
        $scope.starredFlag = false;
        var getSelectedClass = [];
        if ($scope.IsDisplay == true && $scope.IsThumbnail == true) {
            getSelectedClass = document.getElementById("recentdivGrid").querySelectorAll(".selected");
        }
        else if ($scope.IsDisplay == false) {
            getSelectedClass = document.getElementById("recentdiv").querySelectorAll(".selected");
        }
        else if ($scope.IsThumbnail == false) {
            getSelectedClass = document.getElementById("recentThumbnail").querySelectorAll(".selected");
        }
        var control = $event.ctrlKey;
        var shift = $event.shiftKey;
        if (control == false && shift == false) {
            $scope.getSelectedRecentFiles();
            if ($scope.selectedFiles.length != 0) {
                var a = 0;
                var selectRows = $scope.selectedFiles;
                for (var i = 0; i < selectRows.length; i++) {
                    selectRows[i].selected = false;
                    if (selectRows[i] == item && selectRows.length > 1) {
                        item.selected = true;
                    }
                    if (selectRows[i] != item) {

                        a++;
                        if (a == selectRows.length) {
                            item.selected ? item.selected = false : item.selected = true;
                        }
                    }
                }
            }
        }
        else if (control == false && shift == true) {
            $scope.getSelectedRecentFiles();
            var AllRecentFiles = []
            for (var i = 0; i < $scope.recentFileDetailsToday.length; i++) {
                AllRecentFiles.push($scope.recentFileDetailsToday[i]);
            }
            for (var i = 0; i < $scope.recentFileDetailsYesterday.length; i++) {
                AllRecentFiles.push($scope.recentFileDetailsYesterday[i]);
            }
            for (var i = 0; i < $scope.recentFileDetailsPrevious.length; i++) {
                AllRecentFiles.push($scope.recentFileDetailsPrevious[i]);
            }
            if ($scope.selectedFiles.length > 0) {
                var firtSelection = [];
                if ($scope.selectedFiles.length == 1) {
                    $scope.firtSelection = $scope.selectedFiles[0];
                }
                var initialCount = 0;
                var finalCount = 0;
                for (var k = 0; k < AllRecentFiles.length; k++) {
                    if ($scope.firtSelection == AllRecentFiles[k]) {
                        initialCount = k;
                    }
                    if (item == AllRecentFiles[k]) {
                        finalCount = k;
                    }
                }
                if (initialCount > finalCount) {
                    for (var l = finalCount; l <= initialCount; l++) {
                        AllRecentFiles[l].selected = true;
                    }
                    if (finalCount != 0) {
                        for (var l = 0; l < finalCount; l++) {
                            AllRecentFiles[l].selected = false;
                        }
                    }
                    if (initialCount != AllRecentFiles.length - 1) {
                        for (var l = initialCount + 1; l <= AllRecentFiles.length - 1; l++) {
                            AllRecentFiles[l].selected = false;
                        }
                    }

                }
                else {
                    for (var l = initialCount; l <= finalCount; l++) {
                        AllRecentFiles[l].selected = true;
                    }
                    if (initialCount != 0) {
                        for (var l = 0; l < initialCount; l++) {
                            AllRecentFiles[l].selected = false;
                        }
                    }
                    if (finalCount != AllRecentFiles.length - 1) {
                        for (var l = finalCount + 1; l <= AllRecentFiles - 1; l++) {
                            AllRecentFiles[l].selected = false;
                        }
                    }
                }

            }
        }
        if ($scope.selectedFiles.length == 0 || control == true) {
            item.selected ? item.selected = false : item.selected = true;
        }
        $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").removeAttr("style")
        $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").removeAttr("style")
        if ($scope.selectedRows.length > 1 || item.selected == true || getSelectedClass.length > 1) {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:block !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:block !important");
            $("#copymanagebtn").show();
            $("#cutmangebtn").show();
            $("#compressmanagebtn").show();
            $("#deletemanagebtn").show();
            $("#renamemanagebtn").show();
            $("#exportmanagebtn").show();
            $("#downloadmanagebtn").show();
        }
        else {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:none !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:none !important");
            $("#copymanagebtn").hide();
            $("#cutmangebtn").hide();
            $("#compressmanagebtn").hide();
            $("#HyperLinkmanagebtn").hide();
            $("#deletemanagebtn").hide();
            $("#renamemanagebtn").hide();
            $("#exportmanagebtn").hide();
            $("#downloadmanagebtn").hide();
        }
        $event.stopPropagation();
        $event.preventDefault();
    }

    $scope.RenameFolder = '';

    //For the cut/copy/paste/download/compress/Add to Starred fuctionalities in main grid 
    $scope.getAllSelectedRows = function () {
        var selectedRows = $filter("filter")($scope.getImageData, {
            selected: true
        }, true);
        $scope.selectedRows = selectedRows;
        $rootScope.selectedRows = selectedRows;
        $scope.IsCopy = true;
    }
    $scope.SaveFileSelection = function () {
        $rootScope.SaveFileSelection();
    }

    $scope.SaveFolder = function () {

        if ($scope.IsCreate == "Rename" && $scope.newText == "") {

            $("#custom-menu1").show();
            $("#Newdiv").show();
            alert("Please enter the file name")
        }
        else {

            var Path = "";
            if ($scope.IsCreate == "Create") {
                Path = $scope.getCustomerFolder;
            }
            else if ($scope.IsCreate == "Rename") {

                $scope.newSearchPath = $scope.RenameFolder;
                Path = $scope.RenameFolder;
            }
            else {
                Path = $scope.newFolderPath;
            }
            dataFactory.SaveImageManagementDetails(Path, $scope.newText, $localStorage.getCustomerID, $scope.IsCreate, $scope.IsFileType).success(function (response) {
                if (response != "") {

                    // alert(response);
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '',
                        type: "info"
                    });

                    $scope.allFilesLoad();
                    $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").removeAttr("style")
                    $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").removeAttr("style")
                    $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:none !important");
                    $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:none !important");
                    $("#copymanagebtn").hide();
                    $("#cutmangebtn").hide();
                    $("#compressmanagebtn").hide();
                    $("#HyperLinkmanagebtn").hide();
                    $("#deletemanagebtn").hide();
                    $("#renamemanagebtn").hide();
                    $("#exportmanagebtn").hide();
                    $("#downloadmanagebtn").hide();


                    $("#renameFolderPopUp").hide();
                    $("#newFolderPopUp").hide();
                    $scope.newText = "";
                    //$scope.allFilesLoad();
                    //$("#copymanagebtn").hide();
                    //$("#cutmangebtn").hide();
                    //$("#pastemanagebtn").hide();
                    //$("#compressmanagebtn").hide();
                    //$("#deletemanagebtn").hide();
                    //$("#renamemanagebtn").hide();
                    //$("#exportmanagebtn").hide();
                    //$("#downloadmanagebtn").hide();
                }
                else {
                    // $scope.seletedAttribute = "";
                }
            });

        }

    }
    //file preview option//
    $('#imageQuickViewClose').click(function () {
        // alert('success');

        $('#imageQuickView').css('display', 'none');

    });

    $scope.close = function () {
        $('#imageQuickView').css('display', 'none');
    }

    //------------------document preview------------------------//
    $('#documentQuickViewClose').click(function () {
        // alert('success');
        $('#documentQuickView').css('display', 'none');
    });
    //------------------document preview------------------------//
    $scope.fileOpenClick = function (e) {
        $('#documentView').attr('src', '');
        $('#imgZoom').attr('src', '');
        if ($scope.extDriveFlag == "false") {
            var filePath = e.Path;
            var Path = filePath.split('Content\\ProductImages');
            var imagePath = Path[1];
            imagePath = 'Content\\ProductImages' + imagePath;
            var format = imagePath.split('.');

            if (format[1].toLowerCase() == "jpg" || format[1].toLowerCase() == "png" || format[1].toLowerCase() == "gif" || format[1].toLowerCase() == "bmp" || format[1].toLowerCase() == "jpeg") {
                $('#imageQuickView').css('display', 'block');
                $('#imgZoom').attr('src', imagePath);
            }
            else if (format[format.length - 1].toLowerCase() == "pdf" || format[format.length - 1].toLowerCase() == "txt") {
                //if (format[format.length - 1].toLowerCase() == "doc" || format[format.length - 1].toLowerCase() == "xls" || format[format.length - 1].toLowerCase() == "xlsx") {
                //    window.open(imagePath,'_blank');
                //}
                //else {
                $('#documentQuickView').css('display', 'block');
                $('#documentView').attr('src', imagePath);
                //}
            }
            else {
                var ConstPath = "/Content/images/Preview/NoPreviewAvailable.jpg";
                $('#imageQuickView').css('display', 'block');
                $('#imgZoom').attr('src', ConstPath);
            }
        }
        else if ($scope.extDriveFlag == "true") {
            var filePath = e.PreviewPath;
            var pathFields = filePath.split('//');
            var format = pathFields[pathFields.length - 1].split('.');
            if (format[1].toLowerCase() == "jpg" || format[1].toLowerCase() == "png" || format[1].toLowerCase() == "gif" || format[1].toLowerCase() == "bmp" || format[1].toLowerCase() == "jpeg") {
                $('#imageQuickView').css('display', 'block');
                $('#imgZoom').attr('src', filePath);
            }
            else if (format[format.length - 1].toLowerCase() == "pdf" || format[format.length - 1].toLowerCase() == "txt") {
                //if (format[format.length - 1].toLowerCase() == "doc" || format[format.length - 1].toLowerCase() == "xls" || format[format.length - 1].toLowerCase() == "xlsx") {
                //    window.open(imagePath,'_blank');
                //}
                //else {
                $('#documentQuickView').css('display', 'block');
                $('#documentView').attr('src', filePath);
                //}
            }
            else {
                var ConstPath = "/Content/images/Preview/NoPreviewAvailable.jpg";
                $('#imageQuickView').css('display', 'block');
                $('#imgZoom').attr('src', ConstPath);
            }
        }
    }
    //------------------Starred document preview------------------------//
    $scope.starredFileOpenClick = function (e) {
        $('#documentView').attr('src', '');
        $('#imgZoom').attr('src', '');
        if ($scope.extDriveFlag == "false") {
            var filePath = e.FILE_PATH;
            var Path = filePath.split('Content\\ProductImages');
            var imagePath = Path[1];
            imagePath = 'Content\\ProductImages' + imagePath;
            var format = imagePath.split('.');

            if (format[1].toLowerCase() == "jpg" || format[1].toLowerCase() == "png" || format[1].toLowerCase() == "gif" || format[1].toLowerCase() == "bmp" || format[1].toLowerCase() == "jpeg") {
                $('#imageQuickView').css('display', 'block');
                $('#imgZoom').attr('src', imagePath);
            }
            else if (format[format.length - 1].toLowerCase() == "pdf" || format[format.length - 1].toLowerCase() == "txt") {
                //if (format[format.length - 1].toLowerCase() == "doc" || format[format.length - 1].toLowerCase() == "xls" || format[format.length - 1].toLowerCase() == "xlsx") {
                //    window.open(imagePath,'_blank');
                //}
                //else {
                $('#documentQuickView').css('display', 'block');
                $('#documentView').attr('src', imagePath);
                //}
            }
            else {
                var ConstPath = "/Content/images/Preview/NoPreviewAvailable.jpg";
                $('#imageQuickView').css('display', 'block');
                $('#imgZoom').attr('src', ConstPath);
            }
        }
        else if ($scope.extDriveFlag == "true") {
            var filePath = e.PREVIEW_PATH;
            var pathFields = filePath.split('//');
            var format = pathFields[pathFields.length - 1].split('.');
            if (format[1].toLowerCase() == "jpg" || format[1].toLowerCase() == "png" || format[1].toLowerCase() == "gif" || format[1].toLowerCase() == "bmp" || format[1].toLowerCase() == "jpeg") {
                $('#imageQuickView').css('display', 'block');
                $('#imgZoom').attr('src', filePath);
            }
            else if (format[format.length - 1].toLowerCase() == "pdf" || format[format.length - 1].toLowerCase() == "txt") {
                //if (format[format.length - 1].toLowerCase() == "doc" || format[format.length - 1].toLowerCase() == "xls" || format[format.length - 1].toLowerCase() == "xlsx") {
                //    window.open(imagePath,'_blank');
                //}
                //else {
                $('#documentQuickView').css('display', 'block');
                $('#documentView').attr('src', filePath);
                //}
            }
            else {
                var ConstPath = "/Content/images/Preview/NoPreviewAvailable.jpg";
                $('#imageQuickView').css('display', 'block');
                $('#imgZoom').attr('src', ConstPath);
            }
        }
    }

  

    //thumbnail various size variation//
    $scope.gridViewLarge = function () {

        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '32%', '!important');
            $('[id^=mainGrid]').css('height', '180px');
            $('[id^=sizeBasedMainGrid]').css('width', '32%', '!important');
            $('[id^=sizeBasedMainGrid]').css('height', '180px');
            $('[id^=starredMainGrid]').css('width', '32%', '!important');
            $('[id^=starredMainGrid]').css('height', '180px');
            $('[id^=dateBasedMainGrid]').css('width', '32%', '!important');
            $('[id^=dateBasedMainGrid]').css('height', '180px');
            $('[id^=youtubevideos]').css('width', '75%', '!important');
            $('[id^=youtubevideos]').css('height', '156px');
            $('i.fa.fa-folder-open.folderimage.context-menu-one.btn.btn-neutral.ng-scope').css('padding', '50px 170px 50px 160px');
        }, 200);
        //document.getElementById('^mainGrid').style.width = "32%";
    }

    $scope.gridViewMedium = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '24%');
            $('[id^=mainGrid]').css('height', '125px');
            $('[id^=sizeBasedMainGrid]').css('width', '24%');
            $('[id^=sizeBasedMainGrid]').css('height', '125px');
            $('[id^=starredMainGrid]').css('width', '24%');
            $('[id^=starredMainGrid]').css('height', '125px');
            $('[id^=dateBasedMainGrid]').css('width', '24%');
            $('[id^=dateBasedMainGrid]').css('height', '125px');
            $('[id^=youtubevideos]').css('width', '75%');
            $('[id^=youtubevideos]').css('height', '101px');
        }, 200);
        //document.getElementById('^mainGrid').style.width = "32%";
    }

    $scope.gridViewSmall = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '19%');
            $('[id^=mainGrid]').css('height', '120px');
            $('[id^=sizeBasedMainGrid]').css('width', '19%');
            $('[id^=sizeBasedMainGrid]').css('height', '120px');
            $('[id^=starredMainGrid]').css('width', '19%');
            $('[id^=starredMainGrid]').css('height', '120px');
            $('[id^=dateBasedMainGrid]').css('width', '19%');
            $('[id^=dateBasedMainGrid]').css('height', '120px');
            $('[id^=youtubevideos]').css('width', '75%');
            $('[id^=youtubevideos]').css('height', '96px');
        }, 200);
        //document.getElementById('^mainGrid').style.width = "32%";
    }
    $scope.thumbnail = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = true;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '15%');
            $('[id^=mainGrid]').css('height', '120px');
            $('[id^=sizeBasedMainGrid]').css('width', '15%');
            $('[id^=sizeBasedMainGrid]').css('height', '120px');
            $('[id^=starredMainGrid]').css('width', '15%');
            $('[id^=starredMainGrid]').css('height', '120px');
            $('[id^=dateBasedMainGrid]').css('width', '15%');
            $('[id^=dateBasedMainGrid]').css('height', '120px');
            $('[id^=youtubevideos]').css('width', '75%');
            $('[id^=youtubevideos]').css('height', '96px');
            $('i.fa.fa-folder-open.folderimage.context-menu-one.btn.btn-neutral.ng-scope').css('padding', '22px 56px 23px 56px');
        }, 200);
        //document.getElementById('^mainGrid').style.width = "32%";
    }

    $scope.thumbnailnew = function () {
        $scope.IsDisplay = true;
        $scope.IsThumbnail = false;
        $timeout(function () {
            $('[id^=mainGrid]').css('width', '10%');
            $('[id^=mainGrid]').css('height', '60px');
            $('[id^=sizeBasedMainGrid]').css('width', '10%');
            $('[id^=sizeBasedMainGrid]').css('height', '60px');
            $('[id^=starredMainGrid]').css('width', '10%');
            $('[id^=starredMainGrid]').css('height', '60px')
            $('[id^=dateBasedMainGrid]').css('width', '10%');
            $('[id^=dateBasedMainGrid]').css('height', '60px');;
            $('[id^=youtubevideos]').css('width', '30%');
            $('[id^=youtubevideos]').css('height', '60px');
        }, 200);
        //$('i.fa.fa-folder-open.folderimage.context-menu-one.btn.btn-neutral.ng-scope').css('padding', '22px 65px 23px 65px');

        //document.getElementById('^mainGrid').style.width = "32%";
    }


    $scope.onSelect = function (e) {
        var message = $.map(e.files, function (file) { return file.name; }).join(", ");
    };

    $scope.clearFileAttach = function (imageValue) {
        $scope.ImagePreview = "";
    };
    $scope.SplitFunction = function (val) {
        $scope.getFullPath(val);
    }



    $scope.fileSearchClick = function (e, $event) {

        var newPath = "";
        $scope.bradcrumbList = [];
        $scope.IsEmpty = false;
        var PathString = "";
        dataFactory.getLocation().success(function (response) {
            if (response != "") {

                $scope.getFileLocation = response;
                $scope.getFullPath = e.replace($scope.getFileLocation + "\\", "").split('\\');
                if ($scope.getFullPath.length > 0) {
                    for (var j = 0; j < $scope.getFullPath.length; j++) {

                        if ($scope.getFullPath[j] == $scope.cusFolder && j == 0) {

                            newPath = $scope.getFileLocation + "\\" + $scope.cusFolder + "\\";
                        }
                        else {

                            if (PathString == "") {

                                PathString = $scope.getFullPath[j];
                            }
                            else {
                                PathString = PathString + "\\" + $scope.getFullPath[j];

                            }
                            newPath = $scope.getFileLocation + "\\" + $scope.cusFolder + "\\" + PathString;
                        }

                        $scope.bradcrumbList.push(
                 {
                     FolderName: $scope.getFullPath[j],
                     Path: newPath
                 }
                 );
                    }
                }
            }
        });
        $scope.getIsSearchResult = false;
        $scope.getCustomerFolder = e;
        $scope.allFilesLoad();
        $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").removeAttr("style")
        $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").removeAttr("style")
        if ($scope.selectedRows.length > 1) {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:block !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:block !important");
        }
        else {
            $("li.context-menu-item.context-menu-icon.context-menu-icon-cut").attr("style", "display:none !important");
            $("li.context-menu-item.context-menu-icon.context-menu-icon-copy").attr("style", "display:none !important");
            $("#copymanagebtn").hide();
            $("#cutmangebtn").hide();
            $("#compressmanagebtn").hide();
            $("#HyperLinkmanagebtn").hide();
            $("#deletemanagebtn").hide();
            $("#renamemanagebtn").hide();
            $("#exportmanagebtn").hide();
            $("#downloadmanagebtn").hide();
        }

    }

    $scope.validateFolderName = function (e) {

        if (e.keyCode == 124 || e.keyCode == 42 || e.keyCode == 47 || e.keyCode == 92 || e.keyCode == 58 || e.keyCode == 63 || e.keyCode == 34 || e.keyCode == 60 || e.keyCode == 62) {
            $scope.isValidate = true;
            return false;
        }
        else {
            $scope.isValidate = false;
        }
    }


    $scope.init = function () {
        $scope.productData = [];
        $rootScope.SubProdInvert = false;
        $rootScope.btnSubProdInvert = false;
        $rootScope.invertedproductsshow = false;
        $rootScope.invertedproductsbutton = false;
        $rootScope.invertedproductsbutton1 = false;
        $scope.isDisabled = false;
        $scope.assetSizeRange = "Greater than";
        $scope.assetFileSize = "KB";
        $scope.assetDateBasedSearch ="Any Time";
        $scope.starredFlag = false;
        $("#fromDate").hide();
        $("#toDate").hide();
        $("#from").hide();
        $("#to").hide();

    };

    $scope.init();

    $scope.mainPageOut = function () {
        angular.forEach($scope.getImageData, function (value) {
            value.selected = false;
        });
        angular.forEach($scope.linkValues, function (value) {
            value.selected = false;
        });
        $("#copymanagebtn").hide();
        $("#cutmangebtn").hide();
        $("#pastemanagebtn").show();
        $("#compressmanagebtn").hide();
        $("#deletemanagebtn").hide();
        $("#renamemanagebtn").hide();
        $("#exportmanagebtn").hide();
        $("#downloadmanagebtn").hide();
    }

    $scope.searchPageOut = function () {
        angular.forEach($scope.getSearchResult, function (value) {
            value.selected = false;
        });
        angular.forEach($scope.searchlinkValues, function (value) {
            value.selected = false;
        });
        $("#copymanagebtn").hide();
        $("#cutmangebtn").hide();
        $("#HyperLinkmanagebtn").hide();
        $("#pastemanagebtn").hide();
        $("#compressmanagebtn").hide();
        $("#deletemanagebtn").hide();
        $("#renamemanagebtn").hide();
        $("#exportmanagebtn").hide();
        $("#downloadmanagebtn").hide();
    }

    $scope.recentPageOut = function () {
        angular.forEach($scope.recentFileDetailsToday, function (value) {
            value.selected = false;
        });
        angular.forEach($scope.recentFileDetailsYesterday, function (value) {
            value.selected = false;
        });
        angular.forEach($scope.recentFileDetailsPrevious, function (value) {
            value.selected = false;
        });
        angular.forEach($scope.recentTodayLinkValues, function (value) {
            value.selected = false;
        });
        angular.forEach($scope.recentYesterdayLinkValues, function (value) {
            value.selected = false;
        });
        angular.forEach($scope.recentPreviousLinkValues, function (value) {
            value.selected = false;
        });
        $("#copymanagebtn").hide();
        $("#cutmangebtn").hide();
        $("#HyperLinkmanagebtn").hide();
        $("#pastemanagebtn").hide();
        $("#compressmanagebtn").hide();
        $("#deletemanagebtn").hide();
        $("#renamemanagebtn").hide();
        $("#exportmanagebtn").hide();
        $("#downloadmanagebtn").hide();
    }

    //------------------------------------Find associate Images starts here----------------

    //Find associate images

    $scope.FindAssociateImages = function () {
        var selectedRows = $filter("filter")($scope.getImageData, {
            selected: true
        }, true);
        if (selectedRows[0].Extention != "folder") {
            $scope.FindWindow.center().open();
            $scope.selectedRows = selectedRows;
            $rootScope.selectedRows = selectedRows;
            if ($scope.extDriveFlag == "false") {
                $scope.SelectedImagePath = $scope.selectedRows[0].PreviewPath.split("///").pop();
                $("#grid").data("kendoGrid").dataSource.read();
            }
            else if ($scope.extDriveFlag == "true") {
                var spilitValue = 'Content//ProductImages//' + $scope.cusFolder;
                $scope.SelectedImagePath = $scope.selectedRows[0].PreviewPath.split(spilitValue).pop();
                $("#grid").data("kendoGrid").dataSource.read();
            }
        }

    }
    //Grid
    $("#grid").kendoGrid({
        columns: [
        { title: "<input id='chkAll' class='checkAllCls' type='checkbox'/>", width: "35px", template: "<input type='checkbox' id ='innercheck' class='check-box-inner' />", filterable: false },
           { field: "TYPE", title: "Type", hidden: true },
           { field: "STRING_VALUE", title: "Item" },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", template: "<a title='#=ATTRIBUTE_NAME#'>#=ATTRIBUTE_NAME#</a>" },
       { field: "PRODUCT_ID", title: "Id", hidden: true },
         {
             command: [
             { name: "destroy", text: "", width: "10px", template: '<a class=\'k-grid-detach k-item girdicons\'><div title=\'Detach\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\' ></div></a>' },
             ],
             field: "", title: "Action"
         }], toolbar: kendo.template($("#createtemplate").html()),
        autoBind: false, filterable: true,
        dataSource: {
            type: "json", scrollable: true,
            transport: {
                read: function (options) {

                    dataFactory.findAssociateImage($scope.SelectedImagePath).success(function (response) {
                        options.success(response);
                    })
                        .error(function (error) {
                            options.error(error);
                        });

                },
            }, schema: {
                model: {
                    id: "PRODUCT_ID",
                    fields: {
                        ISAvailable: { type: "boolean" },
                        ATTRIBUTE_NAME: { editable: false },
                        PRODUCT_ID: { editable: false },
                        TYPE: { editable: false },
                        STRING_VALUE: { editable: false }
                    }
                }
            },
            group: { field: "TYPE", text: "", dir: "desc" },
        },

    });
    //To detech checkall  from grid

    $("#grid").find(".k-grid-toolbar").on("click", " .k-button", function (e) {
        $scope.RemoveImageAssociation(arrayImages, "All");
        e.preventDefault();
        $('#grid').data('kendoGrid').dataSource.read();
        $('#grid').data('kendoGrid').refresh();
    });

    //To detach single row from grid
    $("#grid tbody").on("click", "td", function (e) {

        var cell = $(e.currentTarget);
        var cellIndex = cell[0].cellIndex;
        var rowElement = this;
        var row = $(rowElement);
        var grid = $("#grid").getKendoGrid();
        if (event.target.checked == true || cellIndex == 6) {
            var grid = $("#grid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
            if (cellIndex == 6)//cellindex of detach column
            {
                $scope.RemoveImageAssociation(dataItem, "single");
            }
            arrayImages.push(dataItem);
            e.stopPropagation();
        } else {
            $('#chkAll').prop('checked', false);
            var grid = $("#grid").data("kendoGrid"),
            dataItem = grid.dataItem($(e.target).closest("tr"));
            arrayImages.pop(dataItem);

        }

    });

    //To checkall option used to check box to delete from grid
    $(".checkAllCls").on("click", function () {
        var ele = this;
        var state = $(ele).is(':checked');
        var grid = $('#grid').data('kendoGrid');
        if (state == true) {
            arrayImages = grid._data;
            $('.check-box-inner').prop('checked', true).closest('tr');

        }
        else {
            arrayImages = [];
            $('.check-box-inner').prop('checked', false).closest('tr').removeClass('k-state-selected');
        }
    });

    //call to detach
    $scope.RemoveImageAssociation = function (data, option) {

        if ((option == "single" && data != undefined && data != null && data != '') || (option == "All" && data.length > 0)) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to remove this image from association?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        bool = true;
                    }
                    if (bool === true) {

                        dataFactory.removeAssociation(arrayImages).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Delete successful.',
                                type: "error"
                            });


                            $('#grid').data('kendoGrid').dataSource.read();
                            $('#grid').data('kendoGrid').refresh();
                        }).error(function (error) {
                            options.error(error);
                        });
                    };
                }
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Select atleast one image association to delete.',
                type: "info"
            });
        }
    }
    //----------------------Find association and Detach process ends here---------------------------------- 

    //-------------------------------------------------------Filter By---------------------------------------------------------------//
    var filterCount = 0;
    $scope.filterBy = function (filterType) {
        filterCount++;
        if (filterCount > 1) {
            $scope.clearFilterBy();
        }
        $scope.filterType = filterType;
        var conditionArray = [];

        if (filterType.toLowerCase() == "images") {
            conditionArray = imageExtensionArray;
        }
        else if (filterType.toLowerCase() == "pdf") {
            conditionArray = pdfExtensionArray;
        }
        else if (filterType.toLowerCase() == "videos") {
            conditionArray = videoExtensionArray;
        }
        else {
            conditionArray = otherThanMentionedArray;
        }
        //if (filterType.toLowerCase() != "Others") {
        var selectedTypeFiles = [];
        if ($scope.getImageDataOld == undefined && $scope.linkValuesOld == undefined) {
            $scope.getImageDataOld = $scope.getImageData; // Used to retrive all the assets ** Not IMAGES
            $scope.linkValuesOld = $scope.linkValues;
        }
        if ($scope.linkValuesOld != undefined) {
            if (filterType.toLowerCase() != "youtube")
                $scope.linkValues = [];
            else if (filterType.toLowerCase() == "youtube")
                $scope.linkValues = $scope.linkValuesOld;
        }
        angular.forEach($rootScope.LoadImageData, function (value) {
            if (conditionArray.indexOf(value.Extention.toLowerCase()) > -1 && filterType.toLowerCase() != "others" && filterType.toLowerCase() != "youtube") {
                selectedTypeFiles.push(value);
            }
            else if (!(conditionArray.indexOf(value.Extention.toLowerCase()) > -1) && filterType.toLowerCase() == "others" && filterType.toLowerCase() != "youtube") {
                selectedTypeFiles.push(value);
            }
        });
        $scope.getImageData = selectedTypeFiles;
        if ($scope.recentFileDetailsPrevious != undefined) {
            if ($scope.recentFileDetailsPreviousOld == undefined && $scope.recentPreviousLinkValuesOld == undefined) {
                $scope.recentFileDetailsPreviousOld = $scope.recentFileDetailsPrevious;
                $scope.recentPreviousLinkValuesOld = $scope.recentPreviousLinkValues;
            }
            if ($scope.recentPreviousLinkValuesOld != undefined) {
                if (filterType.toLowerCase() != "youtube")
                    $scope.recentPreviousLinkValues = [];
                else if (filterType.toLowerCase() == "youtube")
                    $scope.recentPreviousLinkValues = $scope.recentPreviousLinkValuesOld;
            }
            var selectedTypeFilesPre = [];
            angular.forEach($scope.recentFileDetailsPreviousOld, function (value) {
                if (conditionArray.indexOf(value.Extention.toLowerCase()) > -1 && filterType.toLowerCase() != "others" && filterType.toLowerCase() != "youtube") {
                    selectedTypeFilesPre.push(value);
                }
                else if (!(conditionArray.indexOf(value.Extention.toLowerCase()) > -1) && filterType.toLowerCase() == "others" && filterType.toLowerCase() != "youtube") {
                    selectedTypeFilesPre.push(value);
                }
            });
            $scope.recentFileDetailsPrevious = selectedTypeFilesPre;
        }
        if ($scope.recentFileDetailsToday != undefined) {
            if ($scope.recentFileDetailsTodayOld == undefined && $scope.recentTodayLinkValuesOld == undefined) {
                $scope.recentFileDetailsTodayOld = $scope.recentFileDetailsToday;
                $scope.recentTodayLinkValuesOld = $scope.recentTodayLinkValues;
            }
            if ($scope.recentTodayLinkValuesOld != undefined) {
                if (filterType.toLowerCase() != "youtube")
                    $scope.recentTodayLinkValues = [];
                else if (filterType.toLowerCase() == "youtube")
                    $scope.recentTodayLinkValues = $scope.recentTodayLinkValuesOld;
            }
            var selectedTypeFilesToday = [];
            angular.forEach($scope.recentFileDetailsTodayOld, function (value) {
                if (conditionArray.indexOf(value.Extention.toLowerCase()) > -1 && filterType.toLowerCase() != "others" && filterType.toLowerCase() != "youtube") {
                    selectedTypeFilesToday.push(value);
                }
                else if (!(conditionArray.indexOf(value.Extention.toLowerCase()) > -1) && filterType.toLowerCase() == "others" && filterType.toLowerCase() != "youtube") {
                    selectedTypeFilesToday.push(value);
                }
            });
            $scope.recentFileDetailsToday = selectedTypeFilesToday;
        }
        if ($scope.recentFileDetailsYesterday != undefined) {
            if ($scope.recentFileDetailsYesterdayOld == undefined && $scope.recentYesterdayLinkValuesOld == undefined) {
                $scope.recentFileDetailsYesterdayOld = $scope.recentFileDetailsYesterday;
                $scope.recentYesterdayLinkValuesOld = $scope.recentYesterdayLinkValues;
            }
            if ($scope.recentYesterdayLinkValuesOld != undefined) {
                if (filterType.toLowerCase() != "youtube")
                    $scope.recentYesterdayLinkValues = [];
                else if (filterType.toLowerCase() == "youtube")
                    $scope.recentYesterdayLinkValues = $scope.recentYesterdayLinkValuesOld;
            }
            var selectedTypeFilesYes = [];
            angular.forEach($scope.recentFileDetailsYesterdayOld, function (value) {
                if (conditionArray.indexOf(value.Extention.toLowerCase()) > -1 && filterType.toLowerCase() != "others" && filterType.toLowerCase() != "youtube") {
                    selectedTypeFilesYes.push(value);
                }
                else if (!(conditionArray.indexOf(value.Extention.toLowerCase()) > -1) && filterType.toLowerCase() == "others" && filterType.toLowerCase() != "youtube") {
                    selectedTypeFilesYes.push(value);
                }
            });
            $scope.recentFileDetailsYesterday = selectedTypeFilesYes;
        }
        if ($scope.getSearchResult != undefined) {
            if ($scope.getSearchResultOld == undefined && $scope.searchlinkValuesOld == undefined) {
                $scope.getSearchResultOld = $scope.getSearchResult;
                $scope.searchlinkValuesOld = $scope.searchlinkValues;
            }
            if ($scope.searchlinkValuesOld != undefined) {
                if (filterType.toLowerCase() != "youtube")
                    $scope.searchlinkValues = [];
                else if (filterType.toLowerCase() == "youtube")
                    $scope.searchlinkValues = $scope.searchlinkValuesOld;
            }
            var selectedTypeFilesSearch = [];
            angular.forEach($scope.getSearchResultOld, function (value) {
                if (conditionArray.indexOf(value.Extention.toLowerCase()) > -1 && filterType.toLowerCase() != "others" && filterType.toLowerCase() != "youtube") {
                    selectedTypeFilesSearch.push(value);
                }
                else if (!(conditionArray.indexOf(value.Extention.toLowerCase()) > -1) && filterType.toLowerCase() == "others" && filterType.toLowerCase() != "youtube") {
                    selectedTypeFilesSearch.push(value);
                }
            });
            $scope.getSearchResult = selectedTypeFilesSearch;
        }
    }

    $scope.clearFilterBy = function () {
        if ($scope.getImageDataOld != undefined) {
            $scope.getImageData = $scope.getImageDataOld;
            $scope.linkValues = $scope.linkValuesOld;
            $scope.getImageDataOld = undefined;
            $scope.linkValuesOld = undefined;
        }
        if ($scope.recentFileDetailsPrevious != undefined) {
            $scope.recentFileDetailsPrevious = $scope.recentFileDetailsPreviousOld;
            $scope.recentPreviousLinkValues = $scope.recentPreviousLinkValuesOld;
            $scope.recentFileDetailsPreviousOld = undefined;
            $scope.recentPreviousLinkValuesOld = undefined;
        }
        if ($scope.recentFileDetailsToday != undefined) {
            $scope.recentFileDetailsToday = $scope.recentFileDetailsTodayOld;
            $scope.recentTodayLinkValues = $scope.recentTodayLinkValuesOld;
            $scope.recentFileDetailsTodayOld = undefined;
            $scope.recentTodayLinkValuesOld = undefined;
        }
        if ($scope.recentFileDetailsYesterday != undefined) {
            $scope.recentFileDetailsYesterday = $scope.recentFileDetailsYesterdayOld;
            $scope.recentYesterdayLinkValues = $scope.recentYesterdayLinkValuesOld;
            $scope.recentFileDetailsYesterdayOld = undefined;
            $scope.recentYesterdayLinkValuesOld = undefined;
        }
        if ($scope.getSearchResult != undefined && $scope.getSearchResult.length > 0) {
            $scope.getSearchResult = $scope.getSearchResultOld;
            $scope.searchlinkValues = $scope.searchlinkValuesOld;
            $scope.getSearchResultOld = undefined;
            $scope.searchlinkValuesOld = undefined;
        }
        filterCount = 0;
    }
    //-------------------------------------------------------Filter By---------------------------------------------------------------//

    //----------------------------------------------------Adding Youtube Videos------------------------------------------------------//
    $scope.addYoutubeLinkClick = function () {
        $("#custom-menu1").hide();
        $("#Newdiv").hide();
        $scope.newLink = "";
        $scope.newCaption = "";
        $("#addYoutubeLink").show();
        $("#linkText").focus();
    }

    $scope.cancelLink = function () {
        if ($rootScope.userRoleAssetModify == true || $rootScope.userRoleAssetRemove == true) {
            $("#addYoutubeLink").hide();
            $scope.newLink = "";
            $scope.newCaption = "";
        }
    }

    $scope.linkValues = [];

    $scope.addYoutubeLink = function () {
        var link = $scope.newLink.trim();
        var caption = $scope.newCaption.trim();
        if (link != "") {
            if (link.includes('https://www.youtube.com/')) {
                link = link.replace('watch?v=', 'embed/');
                //https://www.youtube.com/watch?v=xcJtL7QggTI        
            }
            else if (link.includes('https://youtu.be/')) {
                link = link.replace('youtu.be', 'www.youtube.com/embed/');
                //https://youtu.be/1MxSsx9h45A
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Invalid You Tube link.',
                    type: "info"
                });
                return;
            }
            if (caption != "") {
                dataFactory.saveYoutubeLink(link, caption, $scope.getCustomerFolder).success(function (youTubeRes) {
                    if (youTubeRes != null) {
                        $("#addYoutubeLink").hide();
                        $scope.newLink = "";
                        $scope.newCaption = "";
                        $scope.allFilesLoad();
                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'File upload failed.',
                            type: "info"
                        });
                    }
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Enter a Caption to proceed.',
                    type: "info"
                });
            }
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Enter a URL to proceed.',
                type: "info"
            });
        }
    }

    $scope.youTubeOpenClick = function (data) {
        var linkVal = data.FolderName.split('_');
        var URL = 'https://www.youtube.com/embed/' + linkVal[1];
        window.open(URL);
    }
    //----------------------------------------------------Adding Youtube Videos------------------------------------------------------//

    // ----------------------------------  Assign Images ---------------------------------------------
    // $scope.AttributeType = 2;
    // $scope.AttributeTypeName = "Product";
    $("#productAttributes").hide();
    $("#familyAttributes").hide();
    $scope.matchWith = 1;
    $("#Assign").hide();

    var attributeTypefulldata = [{
        attributetype: 2,
        attributetypename: 'Product'
    }, {
        attributetype: 1,
        attributetypename: 'Family'
    }];

    $scope.Attributetypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: attributeTypefulldata
        })
    };

    $scope.treeDataAssign = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                dataFactory.CategoriesForAssignOption($scope.catalogId, options.data.id).success(function (response) {
                    options.success(response);
                    //blockUI.stop();
                }).error(function (response) {
                    options.success(response);
                });
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
    });


    // function that gathers IDs of checked nodes
    function checkedNodeIds(nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                if (nodes[i].CATEGORY_ID.contains("~")) {
                    checkedNodes.push(nodes[i].CATEGORY_ID);
                } else if (nodes[i].CATEGORY_ID && !nodes[i].check) {
                    checkedNodes.push(nodes[i].CATEGORY_ID);
                }
                else {
                }
            }

            if (nodes[i].hasChildren) {
                checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    }
    var checkedNodes = [], message;
    function onCheck() {
        checkedNodes = [];
        checkedNodeIds($scope.treeDataAssign._data, checkedNodes);
        if (checkedNodes.length > 0) {
            message = "IDs of checked nodes: " + checkedNodes.join(",");
            // alert(message);

        } else {
            $scope.Category_ids = undefined;
            message = "No nodes checked.";
        }
        $scope.AttributeNameDataSource.read();
        $scope.SelectedAttributeId = "--- Select ---";
        $scope.AttributeType = "--- Select ---";
    }


    $scope.treeDataAssignOption = {
        checkboxes: {
            checkChildren: true,
        }, check: onCheck,
        dataSource: $scope.treeDataAssign
    };

    $scope.AssignImages = function () {
        $("#Assign").show();
        $scope.AssignWindow.center().open();
        $scope.treeDataAssign.read();
        var selectedRows = $filter("filter")($scope.getImageData, {
            selected: true
        }, true);
        $timeout(function () {
            if (selectedRows.length > 1) {
                $("#multipleImage").show();
                $("#singleImage").hide();
                $(".singleImageS").hide();
            } else {
                if (selectedRows[0].Extention == "folder") {
                    $("#multipleImage").show();
                    $("#singleImage").hide();
                    $(".singleImageS").hide();
                } else {
                    $("#multipleImage").hide();
                    $("#singleImage").show();
                    $(".singleImageS").show();
                }
            }
        }, 200);
        $scope.AttributeNameDataSource.read();
    };

    $scope.Count = 2;
    $scope.ChangeAttributeType = function (e) {
        var match_With_Value = 0;
        if ($scope.matchWith == 2 && e.sender.text() == "Family") {
            match_With_Value = 1;
        } else if ($scope.matchWith == 3 && e.sender.text() == "Product") {
            match_With_Value = 1;
        } else {

        }
        $scope.AttributeTypeName = e.sender.text();
        if ($scope.AttributeTypeName != undefined && $scope.AttributeTypeName != "" && $scope.AttributeTypeName != '') {
            if ($scope.AttributeTypeName == "Product") {
                $("#match_With").data("kendoDropDownList").dataSource.read();
                var ddl = $("#match_With").data("kendoDropDownList");
                var oldData = ddl.dataSource.data();
                ddl.dataSource.remove(oldData[2]);
                if (match_With_Value == 1)
                    $scope.matchWith = 1;
                else {
                }
            } else if ($scope.AttributeTypeName == "Family") {
                $("#match_With").data("kendoDropDownList").dataSource.read();
                var ddl = $("#match_With").data("kendoDropDownList");
                var oldData = ddl.dataSource.data();
                ddl.dataSource.remove(oldData[1]);
                if (match_With_Value == 1)
                    $scope.matchWith = 1;
                else {
                }
            } else if ($scope.AttributeTypeName == "--- Select ---") {
                $("#match_With").data("kendoDropDownList").dataSource.read();
            }
            var selectedRows = $filter("filter")($scope.getImageData, {
                selected: true
            }, true);
            if ((selectedRows.length == 1 && selectedRows[0].Extention != "folder") || selectedRows.length == 0) {
                $scope.AttributeNameDataSource.read();
            } else {
                $scope.GetMultipleAttributes();
                if ($scope.AttributeTypeName == "--- Select ---") {
                    $("#familyAttributes").hide();
                    $("#productAttributes").hide();
                } else {

                }
            }
        } else {

        }
    }

    $scope.ChangeMatchWith = function (e) {
        if ($scope.matchWithName == undefined || $scope.matchWithName != e.sender.text()) {
            $scope.matchWithName = e.sender.text();
            if ($scope.AttributeTypeName != undefined && $scope.AttributeTypeName != "" && $scope.AttributeTypeName != '') {
                if ($scope.AttributeTypeName == "Product") {
                    $("#match_With").data("kendoDropDownList").dataSource.read();
                    var ddl = $("#match_With").data("kendoDropDownList");
                    var oldData = ddl.dataSource.data();
                    ddl.dataSource.remove(oldData[2]);
                } else if ($scope.AttributeTypeName == "Family") {
                    $("#match_With").data("kendoDropDownList").dataSource.read();
                    var ddl = $("#match_With").data("kendoDropDownList");
                    var oldData = ddl.dataSource.data();
                    ddl.dataSource.remove(oldData[1]);
                } else if ($scope.AttributeTypeName == "--- Select ---") {
                    $("#match_With").data("kendoDropDownList").dataSource.read();
                }
            }
            var selectedRows = $filter("filter")($scope.getImageData, {
                selected: true
            }, true);
            if ((selectedRows.length == 1 && selectedRows[0].Extention != "folder") || selectedRows.length == 0) {
                $scope.AttributeNameDataSource.read();
            } else {
                $scope.GetMultipleAttributes();
                if ($scope.AttributeTypeName == "--- Select ---") {
                    $("#familyAttributes").hide();
                    $("#productAttributes").hide();
                } else {

                }
            }
        }
        else {

        }
    }


    $scope.AttributeNameDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                if (checkedNodes.length > 0) {
                    $scope.Category_ids = checkedNodes.toString();
                    $scope.Category_ids = $scope.Category_ids.replace(/#/g, '*');
                }
                var selectedRows = $filter("filter")($scope.getImageData, {
                    selected: true
                }, true);
                if (selectedRows != undefined) {
                    if (selectedRows.length == 0)
                        selectedRows = undefined;
                }
                if (selectedRows == undefined || selectedRows.length == 0 || selectedRows.length == 1 && selectedRows[0].Extention != "folder") {
                    dataFactory.GetImageAttributes($scope.catalogId, $scope.Category_ids, $scope.AttributeTypeName, selectedRows).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                } else {
                    dataFactory.GetMultipleImageAttributes($scope.catalogId, $scope.Category_ids, $scope.AttributeTypeName, $scope.matchWith, selectedRows).success(function (response) {
                        $scope.MultipleAttributeName = response.m_Item2;
                        $scope.getMultipleImageData = response.m_Item1;
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        }
    });

    var MultipleAttributeNameData = [];
    //$scope.MultipleAttributeNameDataSource = {
    //    dataSource: new kendo.data.DataSource({
    //        data: $scope.MultipleAttributeName
    //    })
    //};


    $scope.MultipleAttributeNameDataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                options.success($scope.MultipleAttributeName);
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
    });

    var attrCount = 0;
    var imgCount = 0;

    $scope.GetMultipleAttributes = function () {
        var selectedRows = $filter("filter")($scope.getImageData, {
            selected: true
        }, true);
        if (checkedNodes.length > 0) {
            $scope.Category_ids = checkedNodes.toString();
            $scope.Category_ids = $scope.Category_ids.replace(/#/g, '*');
        }
        dataFactory.GetMultipleImageAttributes($scope.catalogId, $scope.Category_ids, $scope.AttributeTypeName, $scope.matchWith, selectedRows).success(function (response) {
            $scope.MultipleAttributeName = response.m_Item2;
            $scope.getMultipleImageData = response.m_Item1;
            attrCount = $scope.MultipleAttributeName.length;
            imgCount = $scope.getMultipleImageData.length;
            if (imgCount > attrCount) {
                if ($scope.AttributeTypeName.toLowerCase() == "product" && checkedNodes != undefined && checkedNodes.length != 0 && $scope.Category_ids != null && $scope.Category_ids != "" && $scope.Category_ids != '' && $scope.Category_ids != undefined) {
                    $("#productAttributes").show();
                    $("#familyAttributes").hide();
                } else if ($scope.AttributeTypeName.toLowerCase() == "family" && checkedNodes != undefined && checkedNodes.length != 0 && $scope.Category_ids != null && $scope.Category_ids != "" && $scope.Category_ids != '' && $scope.Category_ids != undefined) {
                    $("#familyAttributes").show();
                    $("#productAttributes").hide();
                } else {

                }
            } else {
                $("#familyAttributes").hide();
                $("#productAttributes").hide();
            }
            if ($scope.AttributeTypeName == "--- Select ---") {
                $("#familyAttributes").hide();
                $("#productAttributes").hide();
            } else {

            }
            $timeout(function () {
                $scope.MultipleAttributeNameDataSource.read();
            }, 200);
            if (imgCount == 0) {
                var result = "";
                if ($scope.AttributeTypeName == "Product")
                    result = "Item#";
                else if ($scope.AttributeTypeName == "Family")
                    result = "Family Name";
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: result + " not match with selected items.",
                    type: "error"
                });
            }

        }).error(function (error) {
            options.error(error);
        });
    };

    var attributeswithImages = [];
    $scope.AssociateImages = function () {
        var selectedRows = $filter("filter")($scope.getImageData, {
            selected: true
        }, true);
        $scope.selectedRows = selectedRows;
        $rootScope.selectedRows = selectedRows;
        if (checkedNodes.length > 0) {
            $scope.Category_ids = checkedNodes.toString();
            $scope.Category_ids = $scope.Category_ids.replace(/#/g, '*');
        }
        if (selectedRows.length == 1 && selectedRows[0].Extention != "folder") {
            if (checkedNodes != undefined && checkedNodes.length != 0 && $scope.Category_ids != null && $scope.Category_ids != "" && $scope.Category_ids != '' && $scope.Category_ids != undefined) {
                if ($scope.SelectedAttributeId != null && $scope.SelectedAttributeId > 0) {
                    dataFactory.AssociateImages($scope.catalogId, $scope.matchWith, $scope.AttributeType, $scope.SelectedAttributeId, $scope.Category_ids, selectedRows).success(function (response) {
                        $scope.Category_ids = "";
                        // $scope.AttributeType = 2;
                        // $scope.matchWith = 1;
                        // $scope.SelectedAttributeId = "--- Select ---"
                        //  $scope.AttributeType = "--- Select ---"
                        //  $scope.AttributeNameDataSource.read();
                        //  $scope.treeDataAssign.read();
                        if (response !== "") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select any attribute.',
                        type: "error"
                    });
                }
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Category / Family / Product.',
                    type: "error"
                });
            }
        } else {
            var attr_Count = $scope.MultipleAttributeName.length;
            var img_Count = $scope.getMultipleImageData.length;
            for (var i = 0 ; i < img_Count; i++) {
                var imageName = $("#fileName" + i).val();
                var attrId = $("#object" + i).val();
                if (attrId != undefined && attrId != "" && attrId != '' && attrId > 0) {
                    attributeswithImages.push(attrId + "*" + imageName);
                } else {

                }
            }
            if (checkedNodes != undefined && checkedNodes.length != 0 && $scope.Category_ids != null && $scope.Category_ids != "" && $scope.Category_ids != '' && $scope.Category_ids != undefined) {
                if (attributeswithImages != null && attributeswithImages.length > 0) {
                    dataFactory.AssociateMultipleImages($scope.catalogId, $scope.matchWith, $scope.AttributeType, $scope.SelectedAttributeId, $scope.Category_ids, attributeswithImages).success(function (response) {
                        $scope.Category_ids = "";
                        //   $scope.AttributeType = 2;
                        //$scope.SelectedAttributeId = "--- Select ---"
                        // $scope.AttributeType = "--- Select ---"
                        //  $scope.matchWith = 1;
                        //  $scope.GetMultipleAttributes();
                        //  $scope.treeDataAssign.read();
                        if (response !== "") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select any attribute.',
                        type: "error"
                    });
                }
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Category / Family / Product.',
                    type: "error"
                });
            }
        }


    };

    $scope.CancelBtn = function () {
        $scope.treeDataAssign.read();
        $scope.SelectedAttributeId = "--- Select ---"
        $scope.AttributeType = "--- Select ---"
        checkedNodes = [];
        $scope.Category_ids = "";
        $timeout(function () {
            $scope.AssignWindow.close();
        }, 200);
        $("#familyAttributes").hide();
        $("#productAttributes").hide();
        //location.reload();
    };

    $scope.productNewAttributeSetup = function () {
        $scope.ProductNewAttributeSetup.refresh({ url: "../Views/App/Partials/ProductImageAttribute.html" });
        $scope.ProductNewAttributeSetup.center().open();
    };

    $scope.familyNewAttributeSetup = function () {
        $scope.FamilyNewAttributeSetup.refresh({ url: "../Views/App/Partials/FamilyImageAttribute.html" });
        $scope.FamilyNewAttributeSetup.center().open();
    };

    $scope.FamImgAttrSave = function (e) {
        if (e != null && e != undefined && e.ATTRIBUTE_NAME != "" && e.ATTRIBUTE_NAME != '' && e.ATTRIBUTE_NAME != undefined) {
            if (checkedNodes.length > 0) {
                $scope.Category_ids = checkedNodes.toString();
                $scope.Category_ids = $scope.Category_ids.replace(/#/g, '*');
            }
            dataFactory.SaveImageAttribute($scope.AttributeType, $scope.Category_ids, $scope.catalogId, e).success(function (response) {
                $scope.GetMultipleAttributes();
                $timeout(function () {
                    if (imgCount > attrCount) {
                        if ($scope.AttributeTypeName.toLowerCase() == "product" && checkedNodes != undefined && checkedNodes.length != 0 && $scope.Category_ids != null && $scope.Category_ids != "" && $scope.Category_ids != '' && $scope.Category_ids != undefined) {
                            $("#productAttributes").show();
                            $("#familyAttributes").hide();
                        } else if ($scope.AttributeTypeName.toLowerCase() == "family" && checkedNodes != undefined && checkedNodes.length != 0 && $scope.Category_ids != null && $scope.Category_ids != "" && $scope.Category_ids != '' && $scope.Category_ids != undefined) {
                            $("#familyAttributes").show();
                            $("#productAttributes").hide();
                        } else {

                        }
                    } else {
                        $("#familyAttributes").hide();
                        $("#productAttributes").hide();
                    }
                }, 300);
                $scope.FamilyNewAttributeSetup.close();

                if (response !== "") {
                    $scope.famAttribute.STYLE_NAME = "";
                    $scope.famAttribute.ATTRIBUTE_NAME = "";
                    $scope.famAttribute.ATTRIBUTE_SIZE = "";
                    $scope.famAttribute.ATTRIBUTE_DECIMAL = 0;
                    $scope.famAttribute.Prefix = "";
                    $scope.famAttribute.Suffix = "";
                    $scope.famAttribute.Condition = "";
                    $scope.famAttribute.CustomValue = "";
                    $scope.famAttribute.ApplyTo = "All";
                    $scope.famAttribute.ApplyForNumericOnly = true;
                    $scope.famAttribute.PUBLISH2PRINT = true;
                    $scope.famAttribute.PUBLISH2WEB = true;
                    $scope.famAttribute.PUBLISH2PDF = true;
                    $scope.famAttribute.PUBLISH2EXPORT = true;
                    $scope.famAttribute.PUBLISH2PORTAL = true;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                }
            }).error(function (error) {
                options.error(error);
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please attribute name.',
                type: "error"
            });
        }

    }

    $scope.ProdImgAttrSave = function (e) {
        if (e != null && e != undefined && e.ATTRIBUTE_NAME != "" && e.ATTRIBUTE_NAME != '' && e.ATTRIBUTE_NAME != undefined) {
            if (checkedNodes.length > 0) {
                $scope.Category_ids = checkedNodes.toString();
                $scope.Category_ids = $scope.Category_ids.replace(/#/g, '*');
            }
            dataFactory.SaveImageAttribute($scope.AttributeType, $scope.Category_ids, $scope.catalogId, e).success(function (response) {
                $scope.GetMultipleAttributes();
                $timeout(function () {
                    if (imgCount > attrCount) {
                        if ($scope.AttributeTypeName.toLowerCase() == "product" && checkedNodes != undefined && checkedNodes.length != 0 && $scope.Category_ids != null && $scope.Category_ids != "" && $scope.Category_ids != '' && $scope.Category_ids != undefined) {
                            $("#productAttributes").show();
                            $("#familyAttributes").hide();
                        } else if ($scope.AttributeTypeName.toLowerCase() == "family" && checkedNodes != undefined && checkedNodes.length != 0 && $scope.Category_ids != null && $scope.Category_ids != "" && $scope.Category_ids != '' && $scope.Category_ids != undefined) {
                            $("#familyAttributes").show();
                            $("#productAttributes").hide();
                        } else {

                        }
                    } else {
                        $("#familyAttributes").hide();
                        $("#productAttributes").hide();
                    }
                }, 300);
                $scope.ProductNewAttributeSetup.close();

                if (response !== "") {
                    $scope.ImgAttribute.STYLE_NAME = "";
                    $scope.ImgAttribute.ATTRIBUTE_NAME = "";
                    $scope.ImgAttribute.ATTRIBUTE_SIZE = "";
                    $scope.ImgAttribute.ATTRIBUTE_DECIMAL = 0;
                    $scope.ImgAttribute.Prefix = "";
                    $scope.ImgAttribute.Suffix = "";
                    $scope.ImgAttribute.Condition = "";
                    $scope.ImgAttribute.CustomValue = "";
                    $scope.ImgAttribute.ApplyTo = "All";
                    $scope.ImgAttribute.ApplyForNumericOnly = true;
                    $scope.ImgAttribute.PUBLISH2PRINT = true;
                    $scope.ImgAttribute.PUBLISH2WEB = true;
                    $scope.ImgAttribute.PUBLISH2PDF = true;
                    $scope.ImgAttribute.PUBLISH2EXPORT = true;
                    $scope.ImgAttribute.PUBLISH2PORTAL = true;

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                }
            }).error(function (error) {
                options.error(error);
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please attribute name.',
                type: "error"
            });
        }
    }

    $scope.CancelProdAttribute = function () {
        $scope.ProductNewAttributeSetup.close();
        $scope.ImgAttribute.STYLE_NAME = "";
        $scope.ImgAttribute.ATTRIBUTE_NAME = "";
        $scope.ImgAttribute.ATTRIBUTE_SIZE = "";
        $scope.ImgAttribute.ATTRIBUTE_DECIMAL = 0;
        $scope.ImgAttribute.Prefix = "";
        $scope.ImgAttribute.Suffix = "";
        $scope.ImgAttribute.Condition = "";
        $scope.ImgAttribute.CustomValue = "";
        $scope.ImgAttribute.ApplyTo = "All";
        $scope.ImgAttribute.ApplyForNumericOnly = true;
        $scope.ImgAttribute.PUBLISH2PRINT = true;
        $scope.ImgAttribute.PUBLISH2WEB = true;
        $scope.ImgAttribute.PUBLISH2PDF = true;
        $scope.ImgAttribute.PUBLISH2EXPORT = true;
        $scope.ImgAttribute.PUBLISH2PORTAL = true;
    }

    $scope.CancelFamAttribute = function () {
        $scope.FamilyNewAttributeSetup.close();
        $scope.famAttribute.STYLE_NAME = "";
        $scope.famAttribute.ATTRIBUTE_NAME = "";
        $scope.famAttribute.ATTRIBUTE_SIZE = "";
        $scope.famAttribute.ATTRIBUTE_DECIMAL = 0;
        $scope.famAttribute.Prefix = "";
        $scope.famAttribute.Suffix = "";
        $scope.famAttribute.Condition = "";
        $scope.famAttribute.CustomValue = "";
        $scope.famAttribute.ApplyTo = "All";
        $scope.famAttribute.ApplyForNumericOnly = true;
        $scope.famAttribute.PUBLISH2PRINT = true;
        $scope.famAttribute.PUBLISH2WEB = true;
        $scope.famAttribute.PUBLISH2PDF = true;
        $scope.famAttribute.PUBLISH2EXPORT = true;
        $scope.famAttribute.PUBLISH2PORTAL = true;
    }

    $scope.omit_special_char = function (e, attribute_Name) {
        if (attribute_Name == "" || attribute_Name == '') {
            var valid = (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122);
            if (!valid) {
                e.preventDefault();
            }
        }
        else {
            var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 45) || (e.which == 46) || (e.which == 95) || (e.which == 32);
            if (!valid) {
                e.preventDefault();
            }
        }
    }

    $scope.getAllSelectedYoutubeRows = function () {
        var selectedRows = $filter("filter")($scope.linkValues, {
            selected: true
        }, true);

        $scope.selectedRows = $scope.selectedRows.concat(selectedRows);
        $rootScope.selectedRows = $rootScope.selectedRows.concat(selectedRows);
        $scope.IsCopy = true;
    }

    $rootScope.gridAssestManagementResolution = function () {
        var getClassName = $('#managefile').attr('class');
        if (getClassName == "selectbulkfile uploadbtnstyle") {
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#managefile").css("cssText", "margin-right: 0px ;height: 38px ;margin-top: 67px;padding-bottom: 0 ;!important;");
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#managefile").css("cssText", "margin-right: 0px ;height: 38px ;margin-top: 67px;padding-bottom: 0 ;!important;");
            }
            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#managefile").css("cssText", "margin-right: 0px ;height: 38px ;margin-top: 67px;padding-bottom: 0 ;!important;");
            }
            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#managefile").css("cssText", "margin-right: 0px ;height: 38px ;margin-top: 67px;padding-bottom: 0 ;!important;");
            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#managefile").css("cssText", "margin-right: 0px ;height: 38px ;margin-top: 67px;padding-bottom: 0 ;!important;");
            }
            if (window.screen.availWidth >= 1280 && window.screen.availWidth < 1360) {
                $("#managefile").css("cssText", "margin-right: 0px ;height: 38px ;margin-top: 67px;padding-bottom: 0 ;!important;");
            }
            if (window.screen.availWidth >= 1024 && window.screen.availWidth < 1280) {
                $("#managefile").css("cssText", "margin-right: 600px ;height: 38px ;margin-top: 67px;padding-bottom: 0 ;!important;");
            }
        }
    }
    $rootScope.gridAssestManagementResolution();

    $scope.HyperLink = function () {
        if ($rootScope.userRoleAssetModify == true) {
            // $scope.PreviewPath;

            var windowlocation = window.location.origin;
            var fileLink = windowlocation + "/" + $scope.PreviewPath;

            var key = CryptoJS.enc.Utf8.parse('8080808080808080');
            var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

            var encryptedImagePath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(fileLink), key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                //padding: CryptoJS.pad.Pkcs7
            });
            var fileUrl = windowlocation + "/" + "App" + "/" + "CopyImageManagement?url=" + encryptedImagePath;


            ////copy paste
            //var copyElement = document.createElement("Copy");
            //copyElement.appendChild(document.createTextNode(fileUrl));
            //copyElement.id = 'Copy';
            //angular.element(document.body.append(copyElement));

            //// select the text
            //var range = document.createRange();
            //range.selectNode(copyElement);
            //window.getSelection().removeAllRanges();
            //window.getSelection().addRange(range);

            //document.execCommand('copy');



            $("#HyperlinkModelpopup").show();
            $("#validationmessage").hide();
            $("#linkcopiedmessage").hide();
            $("#encryptedfileUrl").val(fileUrl);
            $("#recievermailID").hide();
            $("#gmail").hide();

            $("#encryptedfileUrl").click(function () {
                $(this).select();
            });
            $scope.Cancelmsgbox = function () {
                $("#HyperlinkModelpopup").hide();
                $("#validationmessage").hide();
                $("#linkcopiedmessage").hide();
            }
            $scope.Preview = function () {
                var Fullpath = windowlocation + "/" + $scope.PreviewPath;
                $('#imageQuickView').css('display', 'block');
                $('#imgZoom').attr('src', Fullpath);
                $("#HyperlinkModelpopup").hide();
                $("#validationmessage").hide();
                $("#linkcopiedmessage").hide();
            }
            $scope.Copylink = function () {
                var CopyLink = document.getElementById("encryptedfileUrl");
                CopyLink.select();
                document.execCommand("copy");
                $("#linkcopiedmessage").show();
            }
            $scope.sendmailtoclient = function () {
                $("#recievermailID").val('');
                $("#recievermailID").show();
                $("#gmail").show();
                $("#validationmessage").hide();
                $("#linkcopiedmessage").hide();
            }
            $scope.sendgmail = function () {
                var vaildMail = 0;
                var emails = $scope.recievermailID.split(",");
                for (var i = 0; i < emails.length; i++) {
                    const expression = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if (!expression.test(emails[i])) {
                        $("#validationmessage").show();
                        $("#HyperlinkModelpopup").show();
                        vaildMail = 1;
                    }
                }
                if (vaildMail == 0) {

                    $http.post("/App/Sendgmail?recievermailID=" + $scope.recievermailID + "&fileUrl=" + fileUrl,
                                {
                                    withCredentials: true,
                                    headers: { 'Content-Type': undefined },
                                    transformRequest: angular.identity
                                })

                              .success(function (response) {
                                  if (response == "okey") {
                                      $.msgBox({
                                          title: $localStorage.ProdcutTitle,
                                          content: 'Mail Send Successfully',
                                          type: "info"
                                      });
                                  }
                                  else {
                                      $.msgBox({
                                          title: $localStorage.ProdcutTitle,
                                          content: 'Cannot send mail',
                                          type: "error"
                                      });
                                  }
                              });

                    $("#HyperlinkModelpopup").hide();
                }
            }
            $scope.recievermailID = "";
        }
    }

}]);
