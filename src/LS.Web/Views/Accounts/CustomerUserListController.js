﻿LSApp.controller('CustomerUserListCtrl', function ($scope, dataFactory) {
    $scope.totalPages = 0;
    $scope.totalItems = 0;
    $scope.dataList = [];
    $scope.userList = [];
    $scope.selectedRow = {};
    $scope.dpOptions = { format: "dd.MM.yyyy" };

    //default criteria that will be sent to the server
    $scope.pageSortCriteria = {
        pageNumber: 1,
        pageSize: 5,
        sortDir: 'asc',
        sortedBy: 'DateUpdated'
    };

    //default criteria that will be sent to the server
    $scope.filterCriteria = {};
    $scope.userList = [];
    //The function that is responsible of fetching the result from the server and setting the grid to the new result
    $scope.fetchResult = function () {
        var request = {};
        request.PageSearchCriteria = $scope.pageSortCriteria;
        request.FilterCriteria = $scope.filterCriteria;

        return dataFactory.getCustomerUsers(request)
                .success(function (data) {
                    $scope.dataList = data.Data.DataList;
                    $scope.totalPages = data.Data.TotalPages;
                    $scope.totalItems = data.Data.TotalItems;
                })
                .error(function (error) {
                    $scope.dataList = [];
                    $scope.totalPages = 0;
                    $scope.totalItems = 0;
                    $scope.status = 'Unable to load user data: ' + error.message;
                });
    };

    if ($scope.dataList.length < 6) {

        $("#editable-sample_new").css({ "position": "relative", "top": "1px !important" });
    } else {
        $("#editable-sample_new").css({ "position": "relative", "top": "25px !important" });
    }


    //called when navigate to another page in the pagination
    $scope.selectPage = function () {
        $scope.fetchResult();
    };

    //Will be called when filtering the grid, will reset the page number to one
    $scope.filterResult = function () {
        $scope.pageSortCriteria.pageNumber = 1;
        $scope.fetchResult().then(function () {
            //The request fires correctly but sometimes the ui doesn't update, that's a fix
            $scope.pageSortCriteria.pageNumber = 1;
        });
    };

    //call back function that we passed to our custom directive sortBy, will be called when clicking on any field to sort
    $scope.onSort = function (sortedBy, sortDir) {
        var asd = sortedBy;
        $scope.pageSortCriteria.sortDir = sortDir;
        $scope.pageSortCriteria.sortedBy = sortedBy;
        $scope.pageSortCriteria.pageNumber = 1;
        $scope.fetchResult().then(function () {
            //The request fires correctly but sometimes the ui doesn't update, that's a fix
            $scope.pageSortCriteria.pageNumber = 1;
        });
    };

    $scope.initialize = function () {
        $scope.pageSortCriteria.pageNumber = 1;
        $scope.selectPage(1);
    };

    $scope.initialize();
});