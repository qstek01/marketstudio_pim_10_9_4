﻿LSApp.controller('WebSyncController', ['$scope', '$location', 'dataFactory', '$http', '$rootScope', '$localStorage', 'blockUI', '$timeout', '$window', '$filter', function ($scope, $location, dataFactory, $http, $rootScope, $localStorage, blockUI, $timeout, $window, $filter) {

    $scope.ids = {
        categoryId: '',
        familyId: '',
        productId: '',
        subProductId: '',
        urlBase: '',
        sessionId: '',
        send: '',
        updateType: '',
        jobType: '',
        user: '',
        jobName: '',
        fromDate: '',
        toDate: '',
        Catalog_Id: '',
        endTime: '',
        WebSyncOthers: '',
        IndesignTemp: '',
    }

    $scope.progressComplete = false;
    $scope.getSelectedNode = "";
    //$scope.showOkButton = false;
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    //**Bind the Catalog Dropdown Start**//
    $scope.WebSyncatalogDataSources = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetCurretUserInfo().success(function (response) {
                    $rootScope.currentUser = response;
                    $localStorage.getCustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
                    $localStorage.getUserName = $rootScope.currentUser.Username;
                    $localStorage.getCustomerCompanyName = $rootScope.currentUser.CustomerDetails.Comments;
                    dataFactory.GetCatalogDetailsForCurrentCustomer($scope.SelectedItem, $rootScope.currentUser.CustomerDetails.CustomerId).success(function (cResponse) {
                        options.success(cResponse);
                        if ($localStorage.getCatalogID === undefined) {
                            $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                            $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                            $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                            $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                            $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                        }
                        if ($scope.SelectedCatalogId == '') {
                            $scope.SelectedCatalogId = cResponse[0].CATALOG_ID;
                            $localStorage.getCatalogName = cResponse[0].CATALOG_NAME;
                            $localStorage.getCatalogID = cResponse[0].CATALOG_ID;
                            $rootScope.selecetedCatalogId = $scope.SelectedCatalogId;
                            $scope.SelectedDataItem.CATALOG_NAME = cResponse[0].CATALOG_NAME;
                        }
                        var catalogchk = 0;
                        angular.forEach(cResponse, function (value) {
                            if (value.CATALOG_ID.toString() === $rootScope.selecetedCatalogId.toString()) {
                                catalogchk = 1;
                            }

                        });
                        if (catalogchk === 0) {
                            $rootScope.selecetedCatalogId = 1;
                            $localStorage.getCatalogName = "Master Catalog";
                            $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
                            $scope.SelectedCatalogId = 1;
                            $scope.catalogDataSource.read();
                            $rootScope.treeData._data = [];
                            i = 0;
                            $rootScope.treeData.read();
                        } else {
                            $rootScope.treeData.read();
                        }
                        blockUI.stop();
                    }).error(function (error) {
                        options.error(error);
                    });
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    //**Bind the Catalog Dropdown END**//

    //*Catalog Dropdown changes Start*//
    $scope.WebSyncatalogChanges = function (e) {
        if (e.sender.value() != "") {
            $scope.$broadcast("clearids");
            $rootScope.getSelectedNode = "";
            $rootScope.selecetedCatalogId = e.sender.value();
            $scope.selecetedCatalogId = e.sender.value();
            $localStorage.getCatalogName = e.sender.text();
            $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
            $rootScope.treeData._data = [];
            i = 0;
            $rootScope.treeData.read();

        } else {
            $rootScope.treeData = [];
            $rootScope.selecetedCatalogId = 0;
        }
        $scope.GetUserRoleRightsAll();
        $('#RightWebSyncTree').hide();
    };
    /**End Catalog change function**/

    /**Check the WebApiConnection Start**/
    $scope.checkWebApiConnection = function (webApiUrl) {
        var destinationUrl = webApiUrl + '\CheckWebApiConnection';
        $.get(destinationUrl, function (responseText) {
            if (responseText == true) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Connection Successfully',
                    type: "info"
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Connection unsuccessfull',
                    type: "info"
                });
            }
        }).error(function (error) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Connection unsuccessfull',
                type: "info"
            });
        });
    }
    /**End Connection**/

    //**Start DateTime Picker **//

    /* Reset Datetime picker */
    //$scope.DateTimePicker = function () {
    //    // var today = kendo.date.today("dd/MM/yyyy");
    //    $("#start").data('kendoDatePicker').options._current = new Date();
    //    var today = kendo.toString(kendo.parseDate(kendo.date.today("dd/MM/yyyy")), 'M/d/yyyy');
    //    $("#start").data('kendoDatePicker')._value = new Date();
    //    $("#start").data('kendoDatePicker').dateView.calendar.options.value = new Date();
    //    $("#start").val(today);

    //    $("#end").data('kendoDatePicker').options._current = new Date();
    //    $("#end").data('kendoDatePicker')._value = new Date();
    //    $("#end").data('kendoDatePicker').dateView.calendar.options.value = new Date();
    //    $("#end").val(today);
    //}
    //**End With DateTimePicker **//

    /* tree for WebSync */
    /* tree expand function */
    $scope.expandTree = function (e) {
       
        var catList = [];
        var arrayCategory = [];
        arrayCategory.push(e.sender.dataSource._data)
        var famId = 0;
        var prodId = 0;
        var catId = 0;
        var subProdId = 0;
        $scope.expendTree = "Expand";
        var flagCatColor = false;
        var flagFamColor = false;
        var flagProdColor = false;
        var flagSubProdColor = false;

        //angular.forEach($rootScope.dataFromSrc, function (srcValue) {
        //    angular.forEach($rootScope.dataFromDes, function (desValue) {
        //        if (desValue.CATEGORY_ID == srcValue.CATEGORY_ID) {
        //            flagCatColor = true;
        //        }
        //        if (desValue.FAMILY_ID == srcValue.FAMILY_ID && srcValue.FAMILY_ID != 0) {
        //            flagFamColor = true;
        //        }
        //        if (desValue.PRODUCT_ID == srcValue.PRODUCT_ID && srcValue.PRODUCT_ID != 0) {
        //            flagProdColor = true;
        //        }
        //        if (desValue.SUBPRODUCT_ID == srcValue.SUBPRODUCTID && srcValue.SUBPRODUCTID != 0) {
        //            flagSubProdColor = true;
        //        }

        //        if ($rootScope.completedIds != undefined && $rootScope.completedIds.includes(desValue.CATEGORY_ID)) {
        //            if (desValue.CATEGORY_ID == srcValue.CATEGORY_ID && new Date(desValue.MODIFIED_DATE).toString() != new Date(srcValue.MODIFIED_DATE).toString() && !catList.includes(desValue.CATEGORY_ID) && $('#W' + desValue.CATEGORY_ID).length != 0) {
        //                $('#W' + srcValue.CATEGORY_ID)[0].className = $('#W' + srcValue.CATEGORY_ID)[0].className + " pending";
        //                catList.push(srcValue.CATEGORY_ID);
        //            }

        //            if (desValue.FAMILY_ID == srcValue.FAMILY_ID && new Date(desValue.FMODIFIED_DATE).toString() != new Date(srcValue.FMODIFIED_DATE).toString() && !catList.includes(desValue.FAMILY_ID) && $('#W' + desValue.FAMILY_ID).length != 0) {
        //                $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " pending";
        //                catList.push(srcValue.FAMILY_ID);
        //            }
        //            if (desValue.PRODUCT_ID == srcValue.PRODUCT_ID && new Date(desValue.PMODIFIED_DATE).toString() != new Date(srcValue.PMODIFIED_DATE).toString() && !catList.includes(desValue.PRODUCT_ID) && $('#W' + desValue.PRODUCT_ID).length != 0) {
        //                $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " pending";
        //                if ($('#W' + desValue.FAMILY_ID).length != 0)
        //                    $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " pending";
        //                catList.push(srcValue.PRODUCT_ID);

        //            }
        //            if ((desValue.SUBPRODUCTID == srcValue.SUBPRODUCTID || desValue.SUBPRODUCTID == 0) && new Date(desValue.SPMODIFIED_DATE).toString() != new Date(srcValue.SPMODIFIED_DATE).toString() && !catList.includes(desValue.SUBPRODUCTID) && $('#W' + desValue.SUBPRODUCTID).length != 0) {
        //                $('#W' + srcValue.SUBPRODUCTID)[0].className = $('#W' + srcValue.SUBPRODUCTID)[0].className + " pending";
        //                catList.push(srcValue.SUBPRODUCTID);
        //                if ($('#W' + desValue.FAMILY_ID).length != 0)
        //                    $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " pending";
        //                if ($('#W' + desValue.PRODUCT_ID).length != 0)
        //                    $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " pending";
        //            }
        //        }
        //        else {

        //            if (desValue.CATEGORY_ID == srcValue.CATEGORY_ID && new Date(desValue.MODIFIED_DATE).toString() != new Date(srcValue.MODIFIED_DATE).toString() && !catList.includes(desValue.CATEGORY_ID) && $('#W' + desValue.CATEGORY_ID).length != 0) {
        //                $('#W' + srcValue.CATEGORY_ID)[0].className = $('#W' + srcValue.CATEGORY_ID)[0].className + " modified";
        //                catList.push(srcValue.CATEGORY_ID);
        //            }

        //            if (desValue.FAMILY_ID == srcValue.FAMILY_ID && new Date(desValue.FMODIFIED_DATE).toString() != new Date(srcValue.FMODIFIED_DATE).toString() && !catList.includes(desValue.FAMILY_ID) && $('#W' + desValue.FAMILY_ID).length != 0) {
        //                $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
        //                catList.push(srcValue.FAMILY_ID);
        //            }
        //            if (desValue.PRODUCT_ID == srcValue.PRODUCT_ID && new Date(desValue.PMODIFIED_DATE).toString() != new Date(srcValue.PMODIFIED_DATE).toString() && !catList.includes(desValue.PRODUCT_ID) && $('#W' + desValue.PRODUCT_ID).length != 0) {
        //                $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " modified";
        //                catList.push(srcValue.PRODUCT_ID);
        //                if ($('#W' + desValue.FAMILY_ID).length != 0)
        //                    $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
        //            }
        //            if ((desValue.SUBPRODUCT_ID == srcValue.SUBPRODUCTID || desValue.SUBPRODUCT_ID == 0) && new Date(desValue.SPMODIFIED_DATE).toString() != new Date(srcValue.SPMODIFIED_DATE).toString() && !catList.includes(desValue.SUBPRODUCTID) && $('#W' + desValue.SUBPRODUCTID).length != 0) {
        //                $('#W' + srcValue.SUBPRODUCTID)[0].className = $('#W' + srcValue.SUBPRODUCTID)[0].className + " modified";
        //                catList.push(srcValue.SUBPRODUCTID);
        //                if ($('#W' + desValue.FAMILY_ID).length != 0)
        //                    $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
        //                if ($('#W' + desValue.PRODUCT_ID).length != 0)
        //                    $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " modified";
        //            }
        //        }
        //    });


        //    if ($rootScope.completedIds != undefined && $rootScope.completedIds.includes(srcValue.CATEGORY_ID)) {

        //        if (!catList.includes(srcValue.CATEGORY_ID) && $('#W' + srcValue.CATEGORY_ID).length != 0) {
        //            $('#W' + srcValue.CATEGORY_ID)[0].className = $('#W' + srcValue.CATEGORY_ID)[0].className + " pending";
        //            catList.push(srcValue.CATEGORY_ID);
        //        }
        //        if (!catList.includes(srcValue.FAMILY_ID) && $('#W' + srcValue.FAMILY_ID).length != 0) {
        //            $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " pending";
        //            catList.push(srcValue.FAMILY_ID);
        //        }
        //        if (!catList.includes(srcValue.PRODUCT_ID) && $('#W' + srcValue.PRODUCT_ID).length != 0) {
        //            $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " pending";
        //            catList.push(srcValue.PRODUCT_ID);
        //            if ($('#W' + srcValue.FAMILY_ID).length != 0)
        //                $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " pending";
        //        }
        //        if (!catList.includes(srcValue.SUBPRODUCTID) && $('#W' + srcValue.SUBPRODUCTID).length != 0) {
        //            $('#W' + srcValue.SUBPRODUCTID)[0].className = $('#W' + srcValue.SUBPRODUCTID)[0].className + " pending";
        //            catList.push(srcValue.SUBPRODUCTID);
        //            if ($('#W' + srcValue.FAMILY_ID).length != 0)
        //                $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " pending";
        //            if ($('#W' + srcValue.PRODUCT_ID).length != 0)
        //                $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " pending";
        //        }
        //    }
        //    else {

        //        if (catList.length != 0) {

        //            if (!catList.includes(srcValue.FAMILY_ID) && $('#W' + srcValue.FAMILY_ID).length != 0) {
        //                $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
        //                catList.push(srcValue.FAMILY_ID);
        //            }
        //            if (!catList.includes(srcValue.PRODUCT_ID) && $('#W' + srcValue.PRODUCT_ID).length != 0) {
        //                $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " modified";
        //                catList.push(srcValue.PRODUCT_ID);
        //                if ($('#W' + srcValue.FAMILY_ID).length != 0)
        //                    $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
        //            }
        //            if (!catList.includes(srcValue.SUBPRODUCTID) && $('#W' + srcValue.SUBPRODUCTID).length != 0) {
        //                $('#W' + srcValue.SUBPRODUCTID)[0].className = $('#W' + srcValue.SUBPRODUCTID)[0].className + " modified";
        //                catList.push(srcValue.SUBPRODUCTID);
        //                if ($('#W' + srcValue.FAMILY_ID).length != 0)
        //                    $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
        //                if ($('#W' + srcValue.PRODUCT_ID).length != 0)
        //                    $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " modified";
        //            }
        //        }
        //        else {

        //            if (!flagFamColor && srcValue.FAMILY_ID != 0 && $('#W' + srcValue.FAMILY_ID).length != 0) {
        //                $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
        //            }
        //            else {
        //                flagFamColor = false;
        //            }
        //            if (!flagProdColor && srcValue.PRODUCT_ID != 0 && $('#W' + srcValue.PRODUCT_ID).length != 0) {
        //                $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " modified";
        //                if ($('#W' + srcValue.FAMILY_ID).length != 0)
        //                    $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
        //            }
        //            else {
        //                flagProdColor = false;
        //            }
        //            if (!flagSubProdColor && srcValue.SUBPRODUCTID != 0 && $('#W' + srcValue.SUBPRODUCTID).length != 0) {
        //                $('#W' + srcValue.SUBPRODUCTID)[0].className = $('#W' + srcValue.SUBPRODUCTID)[0].className + " modified";
        //                if ($('#W' + srcValue.FAMILY_ID).length != 0)
        //                    $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
        //                if ($('#W' + srcValue.PRODUCT_ID).length != 0)
        //                    $('#W' + srcValue.PRODUCT_ID)[0].className = $('#W' + srcValue.PRODUCT_ID)[0].className + " modified";
        //            }
        //            else {
        //                flagSubProdColor = false;
        //            }
        //        }
        //    }
        //});

       
    }
    /* End tree expand function */

    /*WebSync tree bind*/
    $rootScope.Categorydetails = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: true,
        autoBind: false,
        transport: {
            read: function (options) {

                var date = new Date();
                dataFactory.GetWebSynCategoryDetails($scope.webSyncFromDate == undefined ? (date.getMonth() + 1) + '/' + date.getDate() + "/" + date.getFullYear() + " 12:00 AM" : $scope.webSyncFromDate, $scope.webSyncToDate == undefined ? (date.getMonth() + 1) + '/' + date.getDate() + "/" + date.getFullYear() + " 12:00 AM" : $scope.webSyncToDate, $rootScope.selecetedCatalogId, $scope.selectedUser == undefined ? "" : $scope.selectedUser, options.data.id).success(function (response) {
                  
                    if ($rootScope.getCategoryid == undefined) {

                        $rootScope.getCategoryid = response;
                    }
                    if ($scope.selectedUser == undefined) {
                        options.success(response);
                        blockUI.stop();
                        return;
                    }
                    if (response == null) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "No records found.",
                            type: "info"
                        });
                        blockUI.stop();
                        return;
                    }
                   
                    if (response.length != 0 && response[0].CATEGORY_NAME == "More Products...") {
                        $scope.RemoveCheckBox = "Remove";
                        //var treeviewName = $("#webSyncTree").data("kendoTreeView");
                        //var id = '#' + $("#W999999").closest('.k-group').find('input[type="checkbox"]').first()[0].id;
                        ////$(id).attr("disabled", true);
                        //$(id).remove();
                    }
                    else {
                        $scope.RemoveCheckBox = "not remove";
                    }
                    options.success(response);
                    if (response.length > 0) {
                        dataFactory.GetWebSyncUrl().success(function (websyncUrl) {
                          
                            $scope.webApiUrl = websyncUrl;
                            var categoryIds = "";
                            var arrayCategory = [];
                            var webApiUrl = $scope.webApiUrl.toString().split('\\')[0] + "\GetSyncDataQueue?catalogId=" + $rootScope.selecetedCatalogId;
                            var arrayCount = 0;
                            $.get(webApiUrl, function (pendingData) {
                              
                                angular.forEach($rootScope.getCategoryid, function (value) {
                                    if (categoryIds.length >= 4000) {
                                        arrayCount += 1;
                                        arrayCategory.push(arrayCount);
                                    }
                                    if (arrayCategory == undefined || arrayCategory.length == 0) {
                                        arrayCategory.push(arrayCount);
                                        if (!value.CATEGORY_ID.includes('ATTR'))
                                            arrayCategory[arrayCount] = "'" + value.CATEGORY_ID + "',";
                                    }
                                    else {
                                        if (!value.CATEGORY_ID.includes('ATTR'))
                                            arrayCategory[arrayCount] = arrayCategory[arrayCount] + "'" + value.CATEGORY_ID + "',";
                                    }
                                });
                                angular.forEach(arrayCategory, function (value) {
                                    if (arrayCategory != undefined && arrayCategory != "" && arrayCategory != null) {
                                        categoryIds = categoryIds + value.toString().trimRight(',');
                                    }
                                });
                                if (categoryIds.length == categoryIds.lastIndexOf(',') + 1) {
                                    categoryIds = categoryIds.substring(0, categoryIds.lastIndexOf(','));
                                }
                                if (categoryIds != "0")
                                    $scope.categoryIds = categoryIds;
                                webApiUrl = $scope.webApiUrl.toString().split('\\')[0] + "/SyncUpdateData?catalogId=" + $rootScope.selecetedCatalogId + "&category_id=" + arrayCategory;
                                $.post(webApiUrl, function (updatedData) {
                                    debugger
                                    $scope.destinationTable = updatedData.Data[0].Table;
                                    angular.forEach(response, function (srcValue) {
                                        if (!srcValue.CATEGORY_ID.includes('~') && !srcValue.CATEGORY_NAME.includes('Products')) {
                                            $('#W' + srcValue.CATEGORY_ID.replace('~', '')).css({ "background-color": "orange" });
                                        }
                                        if (srcValue.CATEGORY_ID.includes('~') && srcValue.CATEGORY_NAME!='Products') {
                                            $('#W' + srcValue.CATEGORY_ID.replace('~', '')).css({ "background-color": "orange" });
                                        }
                                        if (srcValue.CATEGORY_ID.includes('~~'))
                                        {
                                            $('#W' + srcValue.CATEGORY_ID.replace('~~', '')).css({ "background-color": "orange" });
                                        }
                                        angular.forEach($scope.destinationTable, function (desValue) {
                                            var CategoryDate = new Date(srcValue.Modify_Date.replace('T', ' '));
                                            
                                           
                                            if (desValue.CATEGORY_ID == srcValue.CATEGORY_ID && new Date(desValue.MODIFIED_DATE)>= new Date(CategoryDate)) {
                                                $('#W' + srcValue.CATEGORY_ID.replace('~', '')).css({ "background-color": "white" });
                                            }
                                            else
                                            {
                                                if (srcValue.CATEGORY_ID.includes('ATTR'))
                                                {
                                                    if (desValue.CATEGORY_ID == srcValue.CATEGORY_ID.replace('ATTR', '') && new Date(desValue.MODIFIED_DATE).toLocaleString() >= new Date(CategoryDate).toLocaleString()) {
                                                        $('#W' + srcValue.CATEGORY_ID.replace('~', '')).css({ "background-color": "white" });
                                                    }
                                                }
                                            }
                                            //else {
                                            //    $('#W' + srcValue.CATEGORY_ID.replace('~', '')).css({ "background-color": "limegreen" });
                                            //}
                                            if (srcValue.CATEGORY_ID.includes('~') && !srcValue.CATEGORY_ID.includes('~~')) {
                                                if (desValue.FAMILY_ID == srcValue.CATEGORY_ID.replace('~', '') && new Date(desValue.FMODIFIED_DATE).toLocaleString() >= new Date(CategoryDate).toLocaleString()) {
                                                    $('#W' + srcValue.CATEGORY_ID.replace('~', '')).css({ "background-color": "white" });
                                                }                                  
                                            }
                                            else if (srcValue.CATEGORY_ID.includes('~~')) {
                                                if (desValue.PRODUCT_ID == srcValue.CATEGORY_ID.replace('~~', '') && new Date(desValue.PMODIFIED_DATE).toLocaleString() >= new Date(CategoryDate).toLocaleString()) {
                                                    $('#W' + srcValue.CATEGORY_ID.replace('~~', '')).css({ "background-color": "white" });
                                                }
                                               
                                            }
                                        });

                                        //if ($scope.destinationTable.length == 0) {

                                        //    if (srcValue.CATEGORY_ID.includes('~') && !srcValue.CATEGORY_ID.includes('~~')) {

                                        //        $('#W' + srcValue.CATEGORY_ID.replace('~', '')).css({ "background-color": "limegreen" });
                                        //    }
                                        //    else if (srcValue.CATEGORY_ID.includes('~~')) {
                                        //        $('#W' + srcValue.CATEGORY_ID.replace('~~', '')).css({ "background-color": "limegreen" });
                                        //    }
                                        //    else {
                                        //        $('#W' + srcValue.CATEGORY_ID).css({ "background-color": "limegreen" });
                                        //    }
                                        //}




                                        //  $('#W' + srcValue.CATEGORY_ID).className = $('#W' + srcValue.CATEGORY_ID).className + " modified";
                                    });
                                    $scope.destinationPendingTable = pendingData.Data;

                                    angular.forEach(response, function (srcsValue) {
                                        angular.forEach($scope.destinationPendingTable, function (dessValue) {
                                            var CategoryDate = new Date(srcsValue.Modify_Date.replace('T', ' '));
                                            var DescDate = new Date(dessValue.Modified_Date.replace('T', ' '));
                                            if (dessValue.Type_Id == srcsValue.CATEGORY_ID) {
                                                $('#W' + srcsValue.CATEGORY_ID.replace('~', '')).css({ "background-color": "Darkgreen" });
                                            }
                                           
                                            if (srcsValue.CATEGORY_ID.includes('~') && !srcsValue.CATEGORY_ID.includes('~~')) {
                                                if (dessValue.Type_Id == srcsValue.CATEGORY_ID.replace('~', '')) {
                                                    $('#W' + srcsValue.CATEGORY_ID.replace('~', '')).css({ "background-color": "Darkgreen" });
                                                }
                                            }
                                            else if (srcsValue.CATEGORY_ID.includes('~~')) {
                                                if (dessValue.Type_Id == srcsValue.CATEGORY_ID.replace('~~', '')) {
                                                    $('#W' + srcsValue.CATEGORY_ID.replace('~~', '')).css({ "background-color": "Darkgreen" });
                                                }
                                            }
                                        });

                                    });

                                });




                             








                             
                            })
                        })

                    }
                    //if ($rootScope.dataFromDes != 0 && $rootScope.dataFromDes != undefined && $rootScope.dataFromSrc != undefined && options.data.id != undefined) {
                    //    return;
                    //    blockUI.stop();
                    //}
                    //else {
                    //    if (response.length > 0) {
                    //        dataFactory.GetWebSyncUrl().success(function (websyncUrl) {
                    //            $scope.webApiUrl = websyncUrl;
                    //            var categoryIds = "";
                    //            var arrayCategory = [];
                    //            var webApiUrl = $scope.webApiUrl.toString().split('\\')[0] + "\GetSyncDataQueue?catalogId=" + $rootScope.selecetedCatalogId;
                    //            var arrayCount = 0;
                    //            $.get(webApiUrl, function (pendingData) {
                    //                angular.forEach(response, function (value) {
                    //                    if (categoryIds.length >= 4000) {
                    //                        arrayCount += 1;
                    //                        arrayCategory.push(arrayCount);
                    //                    }
                    //                    if (arrayCategory == undefined || arrayCategory.length == 0) {
                    //                        arrayCategory.push(arrayCount);
                    //                        if (!value.CATEGORY_ID.includes('ATTR'))
                    //                            arrayCategory[arrayCount] = "'" + value.CATEGORY_ID + "',";
                    //                    }
                    //                    else {
                    //                        if (!value.CATEGORY_ID.includes('ATTR'))
                    //                            arrayCategory[arrayCount] = arrayCategory[arrayCount] + "'" + value.CATEGORY_ID + "',";
                    //                    }
                    //                });
                    //                angular.forEach(arrayCategory, function (value) {
                    //                    if (arrayCategory != undefined && arrayCategory != "" && arrayCategory != null) {
                    //                        categoryIds = categoryIds + value.toString().trimRight(',');
                    //                    }
                    //                });
                    //                if (categoryIds.length == categoryIds.lastIndexOf(',') + 1) {
                    //                    categoryIds = categoryIds.substring(0, categoryIds.lastIndexOf(','));
                    //                }
                    //                if (categoryIds != "0")
                    //                    $scope.categoryIds = categoryIds;
                    //                webApiUrl = $scope.webApiUrl.toString().split('\\')[0] + "/SyncUpdateData?catalogId=" + $rootScope.selecetedCatalogId + "&category_id=" + arrayCategory;
                    //                $.post(webApiUrl, function (updatedData) {
                    //                    if (($rootScope.dataFromDes == undefined || $rootScope.dataFromDes.length == 0) && updatedData != null) {
                    //                        $rootScope.dataFromDes = updatedData.Data[0].Table;
                    //                    }
                    //                    blockUI.start();
                    //                    dataFactory.GetCategoryFamilyProductData($scope.webSyncFromDate, $scope.webSyncToDate, $rootScope.selecetedCatalogId, $scope.categoryIds, $scope.selectedUser).success(function (srcData) {
                    //                    

                    //                            $rootScope.dataFromSrc = srcData;
                    //                            var flagColor = false;
                    //                                angular.forEach(srcData, function (srcValue) {

                    //                                    angular.forEach($rootScope.dataFromDes, function (desValue) {

                    //                                        if (desValue.CATEGORY_ID == srcValue.CATEGORY_ID) {
                    //                                            flagColor = true;
                    //                                        }
                    //                                        if (desValue.CATEGORY_ID == srcValue.CATEGORY_ID && new Date(desValue.MODIFIED_DATE).toString() <= new Date(srcValue.MODIFIED_DATE.replace('T',' ')).toString()) {
                    //                                            if ($rootScope.completedIds != undefined && $rootScope.completedIds.includes(desValue.CATEGORY_ID)) {
                    //                                                $('#W' + desValue.CATEGORY_ID)[0].className = $('#W' + desValue.CATEGORY_ID)[0].className + " pending";
                    //                                            }
                    //                                            else {
                    //                                                $('#W' + desValue.CATEGORY_ID)[0].className = $('#W' + desValue.CATEGORY_ID)[0].className + " modified";
                    //                                            }
                    //                                        }
                    //                                        else if (desValue.CATEGORY_ID == srcValue.CATEGORY_ID && (desValue.SUBPRODUCT_ID == srcValue.SUBPRODUCTID || desValue.SUBPRODUCT_ID == 0) && srcValue.SUBPRODUCTID != 0 && new Date(desValue.PMODIFIED_DATE).toString() <= new Date(srcValue.PMODIFIED_DATE.replace('T', ' ')).toString()) {
                    //                                            if ($rootScope.completedIds != undefined && $rootScope.completedIds.includes(desValue.CATEGORY_ID)) {
                    //                                                $('#W' + desValue.CATEGORY_ID)[0].className = $('#W' + desValue.CATEGORY_ID)[0].className + " pending";
                    //                                            }
                    //                                            else {
                    //                                                $('#W' + desValue.CATEGORY_ID)[0].className = $('#W' + desValue.CATEGORY_ID)[0].className + " modified";
                    //                                            }
                    //                                        }
                    //                                        else
                    //                                        {
                    //                                            flagColor = true;
                    //                                        }

                    //                                    });

                    //                                        if (!flagColor && $('#W' + srcValue.CATEGORY_ID).length != 0) {
                    //                                            $('#W' + srcValue.CATEGORY_ID)[0].className = $('#W' + srcValue.CATEGORY_ID)[0].className + " modified";
                    //                                        }
                    //                                        else {
                    //                                            flagColor = false;
                    //                                        }

                    //                                        if ($rootScope.dataFromDes.length == 0) {
                    //                                        if ( $scope.expendTree == "Expand") {
                    //                                                $('#W' + srcValue.FAMILY_ID)[0].className = $('#W' + srcValue.FAMILY_ID)[0].className + " modified";
                    //                                            }
                    //                                        }


                    //                                });

                    //                            blockUI.stop();


                    //                    });
                    //                    blockUI.stop();
                    //                });
                    //            });
                    //        }).error(function (error) {
                    //            blockUI.stop();
                    //        });
                    //    }
                    //    else {
                    //        blockUI.stop();
                    //    }
                    //}
                }).error(function (error) {
                    blockUI.stop();
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Server disconnected',
                        type: "info"
                    });
                });

            }

        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },

    });
    $scope.treeOptionsCatalog1 = {
        loadOnDemand: true,
    };

   

    $scope.treeOptionsCatalog = {

        checkboxes: {
            checkChildren: true,
            
        },
        
       
        loadOnDemand: true,
         
        
        dataBound: function (e) {
            if (e.sender.dataSource._data.length > 0) {
                //root[0].dataset.uid
                if ($rootScope.getSelectedNode == "") {
                    var treeview = e.sender;
                    var root = $('.k-item:first');

                    treeview.trigger('select', { node: root });


                } else {

                }
                // $rootScope.previoustate();
            }
            if (!e.node) {

                $scope.attachChangeEvent();
            }
        }
    };
    $scope.attachChangeEvent = function () {
        var dataSource = $rootScope.Categorydetails;
        $scope.checkeditems = '';
        $scope.uncheckeditems = '';
        dataSource.bind("change", function (e) {

            blockUI.start();
            var selectedNodes = 0;
            var UnselectedNodes = 0;
            var checkedNodes = [];
            var UncheckedNodes = [];
            $scope.checkedNodeIds(dataSource.view(), checkedNodes);
            $scope.UncheckedNodeIds(dataSource.view(), UncheckedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            $scope.checkeditems = checkedNodes.join(",");

            for (var i = 0; i < UncheckedNodes.length; i++) {
                var nd = UncheckedNodes[i];
                if (!nd.checked) {
                    UnselectedNodes++;
                }
            }

            $scope.uncheckeditems = UncheckedNodes.join(",");
            blockUI.stop();
            if ($scope.RemoveCheckBox == "Remove") {
                var treeviewName = $("#webSyncTree").data("kendoTreeView");
                var id = '#' + $("#W999999").closest('.k-group').find('input[type="checkbox"]').first()[0].id;
                //$(id).attr("disabled", true);
                $(id).remove();
            }
        });
    };

    $scope.checkedNodeIds = function (nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked == undefined || (nodes[i].checked && !nodes[i].CATEGORY_ID.includes('ATTR'))) {
                checkedNodes.push(nodes[i].CATEGORY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    };
    $scope.UncheckedNodeIds = function (nodes, UncheckedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (!nodes[i].checked && !nodes[i].CATEGORY_ID.includes('ATTR')) {
                UncheckedNodes.push(nodes[i].CATEGORY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.UncheckedNodeIds(nodes[i].children.view(), UncheckedNodes);
            }
        }
    };
    $scope.rightClickEvent = function (data, e) {
        $scope.rightClickItem = e.target.id.toString().replace('W', '');
        $("#custom-menu1").css({ top: (e.pageY - 250) + "px", left: (e.pageX) + "px" }).show();
    }
    /*end Tree Data */

    /**Filter the Result**/
    $scope.FilterResult = function () {
        dataFactory.GetWebSyncUrl().success(function (response) {
            if (response != "") {
                $('#RightWebSyncTree').show();

                $scope.catalogName = $('#drpCatalog').data('kendo-drop-down-list').text();
                var Catalog_Id = $rootScope.selecetedCatalogId;

                $scope.EventRunning = true;
                var url = $location.$$absUrl.split('WebSync') + '';
                $scope.webSyncFromDate = $('#datetimepickerFrom').val();//angular.element(document.querySelector("#start")).val();
                $scope.webSyncToDate = $('#datetimepickerTo').val();//angular.element(document.querySelector("#end")).val();
                var startDate = $scope.webSyncFromDate;
                var endDate = $scope.webSyncToDate;
                $scope.validateDate(startDate, endDate);//calling validate date picker function
                var key = CryptoJS.enc.Utf8.parse('8080808080808080');
                if (Catalog_Id == 1) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'You Cannot Sync From Master Catalog! Please Change Catalog and Proceed Further',
                        type: "info"
                    });
                }
                else {
                    blockUI.start();
                    if ($scope.selectedUser == "" || $scope.selectedUser == undefined || $scope.selectedUser == null) {
                        $scope.selectedUser = "ALL";
                    }
                    $rootScope.dataFromDes = undefined;
                    $rootScope.dataFromSrc = undefined;
                    $rootScope.getCategoryid = undefined;
                    $rootScope.Categorydetails.read();
                    blockUI.stop();
                }
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Connection failed.Please verify connection url.',
                    type: "info"
                });
            }
        }).error(function (error) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Connection failed.Please verify connection url.',
                type: "info"
            });
        });
    }
    /**Filter Result END **/
    $scope.webSyncProgressbarCount = function (percentage, rowCount) {
        $timeout(function () {
            // $scope.webSyncCompletedStatus = true;
            //$scope.showOkButton = false;
            if (percentage <= 6) {
                if ($scope.progressComplete != true) {
                    var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                        type: "chunk",
                        chunkCount: 10,
                        min: 0,
                        max: 10,
                        value: 1
                    }).data("kendoProgressBar");

                    liveSyncProgress.value((percentage));
                    $("#completed").text(((percentage) * 10) + "%");
                    $scope.webSyncProgressbarCount(percentage + 1, rowCount);
                    //$scope.showOkButton = false;
                }
            }
            else {
                if ($scope.progressComplete != true) {
                    var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                        type: "chunk",
                        chunkCount: 10,
                        min: 0,
                        max: 10,
                        value: 1
                    }).data("kendoProgressBar");

                    liveSyncProgress.value(6);
                    $("#completed").text("60%");
                    //$scope.showOkButton = false;
                }
                $timeout(function () {
                    if ($scope.progressComplete != true) {
                        var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");

                        liveSyncProgress.value(7);
                        $("#completed").text("70%");
                        // $scope.showOkButton = false;
                    }
                }, 5000);
                $timeout(function () {
                    if ($scope.progressComplete != true) {
                        var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");

                        liveSyncProgress.value(8);
                        $("#completed").text("80%");
                        // $scope.showOkButton = false;
                    }
                }, 10000);
                $timeout(function () {
                    if ($scope.progressComplete != true) {
                        var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");
                        liveSyncProgress.value(9);
                        $("#completed").text("90%");
                        // $scope.showOkButton = false;
                    }
                }, 15000);
            }
        }, 6000);
    };

    $scope.UpdateLiveDataImage = function () {
      
        $rootScope.completedIds = "";
        $rootScope.updateStates = "";
        senderValue = null;
        var Catalog_Id = $rootScope.selecetedCatalogId;
        $scope.webSyncFromDate = $('#datetimepickerFrom').val();//angular.element(document.querySelector("#start")).val();
        $scope.webSyncToDate = $('#datetimepickerTo').val();
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Do you want to Start Sync?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                if (result == "Yes") {
                    $('#myModal').show();
                    var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                        type: "chunk",
                        chunkCount: 10,
                        min: 0,
                        max: 10,
                        value: 1
                    }).data("kendoProgressBar");
                    liveSyncProgress.value(1);
                    $("#completed").text("10%");
                    var modal = document.getElementById('myModal');
                    modal.style.display = "block";
                    var progCount = 0;
                    dataFactory.GetDeletedHistory($scope.webSyncFromDate, $scope.webSyncToDate,Catalog_Id).success(function (response) {
                        var error = "";
                        var syncDataTypeFormat = "";
                        var updateType = "BOTH";
                        if (senderValue == null) {
                            var jobType = "Live";
                            var isLiveUpdate = true;
                        }
                        else {
                            var jobType = "Scheduled";
                            var isLiveUpdate = false;
                        }
                        jobName = "DELETE"
                        var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");
                        liveSyncProgress.value(2);
                        $("#completed").text("20%");
                        // $scope.showOkButton = false;
                        var percentage = 2;
                        if ($rootScope.dataFromSrc != null || $rootScope.dataFromSrc != undefined) {
                            if ($rootScope.dataFromSrc.length != 0)
                                var rowCount = $rootScope.dataFromSrc.length;
                            else
                                var rowCount = 2;
                        }
                        $scope.progressComplete = false;
                        $scope.webSyncProgressbarCount(2, rowCount);
                        var webApiUrl = $scope.webApiUrl.toString().split('\\')[0] + "/SessionId";
                        $.get(webApiUrl, function (sessionID) {
                            if (sessionID.length > 0) {
                                var endDate = new Date();
                                $scope.endDateValue = endDate.getFullYear() + '-' + ('0' + (endDate.getMonth() + 1)).slice(-2) + '-' + ('0' + endDate.getDate()).slice(-2);
                                var send = "send";
                                $scope.ids.urlBase = $scope.webApiUrl.toString().split('\\')[0];
                                $scope.ids.sessionId = sessionID;
                                $scope.ids.endDate = $scope.endDateValue;
                                $scope.ids.send = send;
                                $scope.ids.updateType = updateType;
                                $scope.ids.jobType = jobType;
                                $scope.ids.user = $scope.selectedUser;
                                $scope.ids.jobName = jobName;
                                $scope.ids.Catalog_Id = Catalog_Id;
                                $scope.ids.fromDate = $scope.webSyncFromDate;
                                $scope.ids.toDate = $scope.webSyncToDate;
                                $scope.ids.WebSyncOthers = $scope.WebSyncOthers;
                                $scope.ids.IndesignTemp = $scope.IndesignTemp;
                                dataFactory.InsertSession($scope.ids).success(function (response) {
                                    if (response == null) {
                                        var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                                            type: "chunk",
                                            chunkCount: 10,
                                            min: 0,
                                            max: 10,
                                            value: 1
                                        }).data("kendoProgressBar");
                                        liveSyncProgress.value(10);
                                        $scope.webSyncCompletedStatus = false;
                                        $("#completed").text("Live sync failed");
                                        $scope.progressComplete = true;
                                        $scope.showOkButton = true;
                                        blockUI.stop();
                                        //$.msgBox({
                                        //    title: $localStorage.ProdcutTitle,
                                        //    content: 'Live sync is failed',
                                        //    type: "info"
                                        //});

                                        if ($scope.checkeditems.includes(',')) {
                                            $rootScope.completedIds = $scope.ids.categoryId + $scope.ids.familyId + $scope.ids.productId;
                                        }
                                        $scope.FilterResult();
                                    }
                                    else if (response.toString().toUpperCase().includes("INSERT SUCCESSFUL")) {
                                       
                                        webApiUrl = $scope.webApiUrl.toString().split('\\')[0] + "/CallSyncLiveUpdate?sessionId=" + $scope.ids.sessionId;
                                        $.post(webApiUrl, function (UpdateStatus) {
                                            if (UpdateStatus.toString().toUpperCase() == "UPDATED IN DESTINATION DB") {
                                                var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                                                    type: "chunk",
                                                    chunkCount: 10,
                                                    min: 0,
                                                    max: 10,
                                                    value: 1
                                                }).data("kendoProgressBar");
                                                liveSyncProgress.value(10);
                                                $scope.webSyncCompletedStatus = false;
                                                $("#completed").text("Live Sync Successful");
                                                $scope.progressComplete = true;
                                                $scope.showOkButton = true;
                                                $rootScope.updateStates = "UPDATED IN DESTINATION DB"
                                                blockUI.stop();

                                            }
                                            else {
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: 'Synchronization is failed.Please contact Questudio.',
                                                    type: "info"
                                                });
                                                var liveSyncProgress = $("#webSyncCompleteness").kendoProgressBar({
                                                    type: "chunk",
                                                    chunkCount: 10,
                                                    min: 0,
                                                    max: 10,
                                                    value: 1
                                                }).data("kendoProgressBar");
                                                liveSyncProgress.value(10);
                                                $scope.webSyncCompletedStatus = false;
                                                $("#completed").text("Live Sync failed");
                                                $scope.progressComplete = true;
                                                $scope.showOkButton = true;
                                                blockUI.stop();

                                                if ($scope.checkeditems.includes(',')) {
                                                    $rootScope.completedIds = $scope.ids.categoryId + $scope.ids.familyId + $scope.ids.productId;
                                                }
                                            }
                                            //if ($scope.checkeditems.includes(',')) {
                                            //    $rootScope.completedIds = $scope.ids.categoryId + $scope.ids.familyId + $scope.ids.productId;
                                            //}
                                            $scope.FilterResult();
                                        });
                                    }
                                    else {
                                        blockUI.stop();
                                    }
                                });
                            }
                            blockUI.stop();
                        });
                    });
                }
                else {
                    blockUI.stop();
                    return false;
                }
            }
        });
    }
    //progress bar popup close function once live sync completed successfully
    $scope.closePopup = function () {

        $scope.showOkButton = false;
        $("#myModal").hide();
        $("#webSyncCompleteness").kendoProgressBar({
            type: "chunk",
            chunkCount: 10,
            min: 0,
            max: 10,
            value: 1
        }).data("kendoProgressBar");
        $("#completed").text("10 %");
        blockUI.stop();
    };

    $scope.LiveSync = function () {
        blockUI.start();
        $scope.webSyncCompletedStatus = true;
        var multipleIds = $scope.checkeditems;
        var categoryId = "";
        var familyId = "";
        var productId = "";
        var subProductId = "";
        var webSyncTree = $('#webSyncTree').data('kendoTreeView');
        var expand = 0;
        if ($scope.checkeditems == "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "There is no item(s) selected / deleted?",
                type: "info"
            });
            //return;
            blockUI.stop();
        }
        angular.forEach($scope.checkeditems.split(','), function (Ids) {
            if (Ids.includes('CAT') && !Ids.includes('ATTR') && !categoryId.includes(Ids.replace('~', ''))) {
                categoryId = categoryId + Ids + ",";
            }
            else if (Ids.includes('~') && !Ids.includes('~~') && !Ids.includes('~~~') && !familyId.includes(Ids.replace('~', '')) && !Ids.includes('CAT') && !Ids.includes('ATTR')) {
                familyId = familyId + Ids + ",";
            }
            else if (Ids.includes('~~') && !Ids.includes('~~~') && !productId.includes(Ids.replace('~', '')) && !Ids.includes('CAT') && !Ids.includes('ATTR')) {
                productId = productId + Ids + ",";
            }
            else if (Ids.includes('~~~') && !subProductId.includes(Ids.replace('~', '')) && !Ids.includes('CAT') && !Ids.includes('ATTR')) {
                subProductId = subProductId + Ids + ",";
            }
        });
        if (categoryId == "" && familyId != "") {
            angular.forEach(familyId.split(','), function (Ids) {
                angular.forEach($rootScope.dataFromSrc, function (srcValue) {
                    if (Ids.replace('~', '').includes(srcValue.FAMILY_ID) && !categoryId.includes(Ids.replace('~', ''))) {
                        categoryId = categoryId + srcValue.CATEGORY_ID + ",";
                    }
                });
            });
        }
        else if (categoryId == "" && familyId == "" && productId != "") {
            angular.forEach(familyId.split(','), function (Ids) {
                angular.forEach($rootScope.dataFromSrc, function (srcValue) {
                    if (Ids.replace('~', '').replace('~~', '').includes(srcValue.PRODUCT_ID) && !categoryId.includes(Ids.replace('~', ''))) {
                        categoryId = categoryId + srcValue.CATEGORY_ID + ",";
                    }
                    if (Ids.replace('~', '').replace('~~', '').includes(srcValue.PRODUCT_ID) && !familyId.includes(Ids.replace('~', ''))) {
                        familyId = familyId + srcValue.FAMILY_ID + ",";
                    }
                });
            });
        }
        else if (categoryId == "" && familyId == "" && productId == "" && subProductId != "") {
            angular.forEach(familyId.split(','), function (Ids) {
                angular.forEach($rootScope.dataFromSrc, function (srcValue) {
                    if (Ids.replace('~', '').replace('~', '').includes(srcValue.SUBPRODUCT_ID) && !categoryId.includes(Ids.replace('~', ''))) {
                        categoryId = categoryId + srcValue.CATEGORY_ID + ",";
                    }
                    if (Ids.replace('~', '').replace('~', '').includes(srcValue.SUBPRODUCT_ID) && !familyId.includes(Ids.replace('~', ''))) {
                        familyId = familyId + srcValue.FAMILY_ID + ",";
                    }
                    if (Ids.replace('~', '').replace('~', '').includes(srcValue.SUBPRODUCT_ID) && !productId.includes(Ids.replace('~', '').replace('~', ''))) {
                        productId = productId + srcValue.PRODUCT_ID + ",";
                    }
                });
            });
        }
        $scope.ids.categoryId = categoryId;
        $scope.ids.familyId = familyId;
        $scope.ids.productId = productId;
        $scope.ids.subProductId = subProductId;
        $scope.UpdateLiveDataImage();
    }
    //Live Update END

    // Get Web api url
    $scope.GetWebSyncUrl = function () {
        dataFactory.GetWebSyncUrl().success(function (response) {
            if (response != null) {
                $scope.webApiUrl = response;
            }
            else {
                $scope.webApiUrl = undefined;
            }
        })
    }
    // Get Web api url End
    /*Reset Page*/
    $scope.ResetPage = function () {
        $scope.webSyncStatus = "ALL";
        $("#Status").data("kendoDropDownList").dataSource.read();
        $('#RightWebSyncTree').hide();
        $rootScope.treeData.read();//Left side naviagtion tree
        $scope.GetUserDetails.read();
        $scope.GetWebSyncUrl();
        $scope.webSyncFromDate = $('#datetimepickerFrom').val();
        $scope.webSyncToDate = $('#datetimepickerTo').val();
        //$scope.DateTimePicker();
    }
    /*Reset Page End */

    /* Load UserList */
    $scope.GetUserDetails = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetUserList().success(function (response) {
                    options.success(response);
                    if (response.length > 1) {
                        $scope.selectedUser = response[0].User_Name;
                    }
                    else {
                        $scope.selectedUser = response[0].User_Name;
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    /*Filter function*/
    $scope.ChangeFilterType = function () {

    }
    $scope.Filter = function (query, dataSource) {
        var hasVisibleChildren = false;
        var data = dataSource instanceof kendo.data.HierarchicalDataSource && dataSource.data();
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var text = item.CATEGORY_NAME.toLowerCase();
            var itemVisible =
                query === true // parent already matches
                || query === "" // query is empty
                || text.indexOf(query) >= 0; // item text matches query

            var anyVisibleChildren = $scope.Filter(itemVisible || query, item.children); // pass true if parent matches

            hasVisibleChildren = hasVisibleChildren || anyVisibleChildren || itemVisible;
            angular.forEach($scope.filterValues, function (data) {
                if (!data.text.trim().toLowerCase().includes(text)) {
                    item.hidden = !itemVisible && !anyVisibleChildren;
                }
            });
        }
        if (data) {
            // re-apply filter on children
            dataSource.filter({ field: "hidden", operator: "neq", value: true });
        }
        return hasVisibleChildren;
    }


    /*Filter function End*/

    // initial function
    $scope.Init = function () {
        $scope.GetWebSyncUrl();
        $scope.EventRunning = false;
        $scope.syncDataSource.read();

    }
    // initial function End

    //SyncLog Start
    $scope.ViewSyncLogs = function () {
        $scope.syncDataSource.read();
    }
    $scope.syncDataSource = new kendo.data.DataSource({
        type: "json",
        pageSize: 10,
        transport: {
            read: function (options) {
                if ($scope.webApiUrl == undefined) {
                    dataFactory.GetWebSyncUrl().success(function (response) {

                        $scope.Task = 'All';
                        var webApiUrl = response.toString().split('\\')[0] + "\syncDataLog?Task=" + $scope.Task;
                        $.post(webApiUrl, function (data) {
                            if (data.Data.length == 0)
                                options.success(data.Data)
                            else
                                options.success(data.Data[0]);
                        }).error(function (error) {
                            options.error(error);
                        });
                    });
                }
                else {

                    $scope.option = '';
                    var webApiUrl = $scope.webApiUrl.toString().split('\\')[0] + "\syncDataLog?TaskId=" + $scope.Task;
                    $.post(webApiUrl, function (data) {
                        options.success(data.Data[0])
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        },
        schema: {
            model: {
                id: "Id",
                fields: {
                    Id: { editable: false, nullable: true }
                }
            }
        },
    });
    $scope.SyncErrorDetails = function (dataItem) {

        return {
            type: "json",
            transport: {
                read: function (options) {
                    if ($scope.webApiUrl == undefined) {
                        dataFactory.GetWebSyncUrl().success(function (response) {

                            var array = dataItem.Id.split('-');
                            $scope.Task = array[1];
                            var webApiUrl = response.toString().split('\\')[0] + "\syncDataLog?Task=" + dataItem.Id;
                            $.post(webApiUrl, function (getData) {

                                options.success(getData.Data[0]);
                            }).error(function (error) {
                                options.error(error);
                            });
                        });
                    }
                    else {

                        var array = dataItem.Id.split('-');
                        $scope.Task = array[1];
                        var webApiUrl = $scope.webApiUrl.toString().split('\\')[0] + "\syncDataLog?Task=" + dataItem.Id;
                        $.post(webApiUrl, function (getData) {
                            options.success(getData.Data[0]);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            },
            schema: {
                model: {
                    id: "Id",
                }
            },
        };
    };
    $scope.WebSyncMainLogGridOptions = {
        dataSource: $scope.syncDataSource,
        sortable: true,
        scrollable: true, filterable: true,
        pageable: { buttonCount: 5 },
        columns: [
                { field: "Id", title: "Job", width: "160px" },
                { field: "JOB_TYPE", title: "Job Type", width: "160px" },
                { field: "UPDATED_DATE", title: "Created Date", width: "160px" },
                { field: "UPDATED_DATE", title: "Updated Date", width: "160px" },
        ],
        detailTemplate: kendo.template($("#newgrouptemplate").html()),
    }
    $scope.WebSyncErrorLog = function (dataItem) {
        return {
            dataSource: $scope.SyncErrorDetails(dataItem),
            columns: [
                { field: "Id", title: "Id", width: "250px" },
                { field: "Status", title: "Status", width: "250px" },
                { field: "Message", title: "Message", width: "250px", template: "<a title=\'Download File\' style=\'padding-right: 10px;\' class=\'glyphicon glyphicon-download-alt blue btn-xs-icon\' ng-click=\'DownloadMessage($event,this)\'  ng-href='{{ fileUrl }}' Message='file.txt'></a>" },//<a title=\'View File\' style=\'padding-right: 10px;\' ng-click=\'ViewMessage($event,this)\' class=\'glyphicon fa fa-eye blue btn-xs-icon\'></a>" },
                //{ template: "<a class=\'k-item girdicons\' ng-click=\'DownloadMessage($event,this)\' ng-show=\'userRoleDeleteAttributeManagement\'></a>" },
                //{ command: [{ field: "Message", title: "Message", width: "250px", template: "<a  ng-click=\'DownloadMessage($event,this)\'</a>" }] },
                { field: "UPDATED_DATE", title: "Updated Date", width: "250px" },

                { command: [{ name: "delete", title: "Action", text: "", width: "110px", template: "<a class=\'k-item girdicons\' ng-click=\'DeleteSyncLog($event,this)\' ng-show=\'userRoleDeleteAttributeManagement\'><div title=\'Delete\' style=\'padding-right: 10px;\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" }], title: "ACTIONS" }//<div title=\'Download File\' style=\'padding-right: 10px;\' class=\'glyphicon glyphicon-download-alt blue btn-xs-icon\'ng-click=\'DownloadMessage($event,this)\'  ng-href='{{ fileUrl }}' Message='file.txt'></div><div title=\'View File\' style=\'padding-right: 10px;\' ng-click=\'ViewMessage($event,this)\' class=\'glyphicon fa fa-eye blue btn-xs-icon\'></div></a>" }], title: "ACTIONS" }
            ],
            detailTemplate: false,
        };
    }
    //var wnd, detailsTemplate;
    //wnd = $("#details")
    //     .kendoWindow({
    //         title: "Errorlog Details",
    //     modal: true,
    //     visible: false,
    //     resizable: true,
    //     width: 500,
    //     height: 500,
    //     }).data("kendoWindow");
    //detailsTemplate = kendo.template($("#newgrouptemplate").html());

    $scope.DownloadMessage = function (event, ids) {
        var xmlData = "<root>" + ids.dataItem.Message + "</root>";
        var fileToDownload = new Blob([xmlData], { type: "text/xml" });
        var a = document.createElement("a");
        a.href = URL.createObjectURL(fileToDownload);
        a.target = "_blank";
        a.download = "export_" + "Test" + ".xml";
        a.click();
    }

    $scope.ViewMessage = function (event, data) {

        event.preventDefault();
        var dataItem = data.dataItem.Message;
        wnd.content(detailsTemplate(dataItem));
        wnd.center().open();
    }

    $scope.DeleteSyncLog = function (e, id) {
        if ($scope.webApiUrl == undefined) {
            dataFactory.GetWebSyncUrl().success(function (response) {
                $scope.Choice = "DELETE";
                $scope.PacketId = id.dataItem.Id;
                var webApiUrl = response.toString().split('\\')[0] + "\DeleteSyncLog?PacketIds=" + $scope.PacketId + "&choice=" + $scope.Choice;
                $.post(webApiUrl, function (response) {
                    $scope.Task = "All"
                    $scope.webApiUrl = undefined;
                    $scope.syncDataSource.read();
                }).error(function (error) {
                    options.error(error);
                });
            });
        }
        else {
            $scope.Choice = "DELETE";
            $scope.PacketId = id.dataItem.Id;
            var webApiUrl = $scope.webApiUrl.toString().split('\\')[0] + "\DeleteSyncLog?PacketIds=" + $scope.PacketId + "&choice=" + $scope.Choice;
            $.post(webApiUrl, function (response) {
                $scope.Task = "All"
                $scope.webApiUrl = undefined;
                $scope.syncDataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        }
    }
    //SyncLog End

    //Status dropdown

    $(document).ready(function () {
        function onSelect(e) {
            var dataItem = this.dataItem(e.item);
            $rootScope.selectedStatus = dataItem.text;
            var filterValue = "";
            if ($rootScope.selectedStatus == "Not Updated")
                filterValue = "modified";
            else if ($rootScope.selectedStatus == "Pending")
                filterValue = "pending";
            $scope.ColorWiseFilter(filterValue);
        };
        var data = [
            { text: "All", value: "1" },
            { text: "Not Updated", value: "2" },
            { text: "Pending", value: "3" }
        ];

        $("#Status").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: data,
            selectedStatus: "All",
            select: onSelect,
        });
    });

    /*Color Wise Filter*/
    $scope.ColorWiseFilter = function (filterText) {
        if (filterText !== "") {
            $(".selectAll").css("visibility", "hidden");

            $("#webSyncTree  .k-group .k-group .k-in").closest("li").hide();
            $("#webSyncTree  .k-group").closest("li").hide();
            $("#webSyncTree  .k-in ." + filterText).each(function () {
                $(this).parents("ul, li").each(function () {
                    var treeView = $("#webSyncTree ").data("kendoTreeView");
                    treeView.expand($(this).parents("li"));
                    $(this).show();
                });
            });
            $("#webSyncTree  .k-group .k-in " + filterText).each(function () {
                $(this).parents("ul, li").each(function () {
                    $(this).show();
                });
            });
        }
        else {
            $("#webSyncTree  .k-group").find("li").show();
            var nodes = $("#webSyncTree  > .k-group > li");

            $.each(nodes, function (i, val) {
                if (nodes[i].getAttribute("data-expanded") == null) {
                    $(nodes[i]).find("li").hide();
                }
            });

            $(".selectAll").css("visibility", "visible");
        }
    }


    //var SyncStatus = [{ 'StatusType': 'All', 'value': 'All' },
    //        { 'StatusType': 'Update', 'value': 'Update' },
    //        { 'StatusType': 'Pending', 'value': 'Pending' }];

    //var viewModel = kendo.observable({
    //    SyncStatus: SyncStatus,
    //    selectedStatus: "All",
    //    change: StatusDropDownChanged
    //});
    ////$scope.StatusSelected = SyncStatus.selectedStatus;
    //kendo.bind(document.body, viewModel);

    //$scope.StatusDropDownChanged = function (value) {
    //    $scope.SyncDropDownStatus = this.dataSource.get(this.value()).id
    //    $scope.SelectedSyncStatus = value;
    //    $scope.SyncDropDownStatus = $scope.SelectedSyncStatus;
    //};

    //$("#dropdownlist").kendoDropDownList({
    //    dataSource: [
    //      { id: 1, name: "All" },
    //      { id: 2, name: "Update" },
    //    { id: 3, name: "Pending"}
    //    ],
    //    dataTextField: "name",
    //    dataValueField: "id",
    //    //index: 1,
    //    change: onChange
    //});
    //function onChange(e) {
    //    //console.log(this.dataSource.get(this.value()).id);
    //    $scope.changes = e.sender.dataSource._data[0].name;
    //}
    //$scope.tempdata = [
    //    { Text: "ALL" },
    //       { Text: "Update" },
    //        { Text: "Pending" }
    //];
    //$("#Status").kendoDropDownList({
    //    dataTextField: "Text",
    //    optionLabel: "Select Status",
    //    dataSource: $scope.tempdata,
    //    animation: false,
    //    onchange:changeStatus
    //});
    //$scope.tempdatadatasource = new kendo.data.DataSource({
    //    type: "json",
    //    transport: {
    //        read:
    //            function (options) {

    //                $scope.tempdata
    //               // $scope.status = $scope.tempdata;
    //            }
    //    }
    //});


    // Start of Check/ UnCheck
    $scope.CheckOrUncheckBoxWebsync = false;

    $scope.CheckOrUncheckWebsync = function () {

        if (chkEnabledWebsync1.checked == true) {
            $scope.CheckOrUncheckBoxWebsync = true;
        } else {
            $scope.CheckOrUncheckBoxWebsync = false;
        }
        if ($scope.CheckOrUncheckBoxWebsync) {
            $('#webSyncTree input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBoxWebsync = true;
        } else {
            $('#webSyncTree input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBoxWebsync = false;
        }
    };

    // Expand / Collapse
    $scope.ExpandOrCollapseBoxWebsync = false;
    $scope.ExpandOrCollapseWebsync = function () {
        if (chkEnabledWebsync2.checked == true) {
            $scope.ExpandOrCollapseBoxWebsync = true;
        } else {
            $scope.ExpandOrCollapseBoxWebsync = false;
        }
        if ($scope.ExpandOrCollapseBoxWebsync) {
            $("#webSyncTree").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBoxWebsync = true;
        } else {
            $("#webSyncTree").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBoxWebsync = false;
        }
    };
    //Validate date picker
    $scope.validateDate = function (startDate, endDate) {
        $scope.errMessage = '';
        var curDate = new Date();
        if (new Date(startDate) > new Date(endDate)) {
            $('#RightWebSyncTree').hide();
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter valid start and end date ',
                type: "info"
            });

            return false;
        }
    };
    //SyncDBErrorLog End
    $scope.Init();

}]);