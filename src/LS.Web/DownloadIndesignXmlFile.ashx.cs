﻿using LS.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;


namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadIndesignXmlFile
    /// </summary>
    public class DownloadIndesignXmlFile : IHttpHandler
    {
        readonly CSEntities _dbcontext = new CSEntities();
       
        public void ProcessRequest(HttpContext context)
        {
            
            string _xmlpath1=context.Server.MapPath("~/Content/ProductImages");
            //context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;
            string folderPath= HttpContext.Current.Request.QueryString["Path"];
            string filename = System.IO.Path.GetFileName(folderPath);
            _xmlpath1 = string.Format("{0}{1}", _xmlpath1, folderPath);
            if (File.Exists(_xmlpath1))
            {
                //var path = context.Session["Xmlpath"] + filename;
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + filename + ";");
               // response.TransmitFile((context.Session["Xmlpath"] + filename));
                response.TransmitFile(_xmlpath1);
                response.Flush();
                response.End();
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("File not found!");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}


  