﻿using Infragistics.Documents.Excel;
using LS.Data;
using LS.Web.Controllers;
using LS.Web.Models;
using System;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Web;
using System.Web.SessionState;
using DataTable = System.Data.DataTable;
using Workbook = Infragistics.Documents.Excel.Workbook;

namespace LS.Web
{

    public class DownloadTableDesignerErrorLog : IHttpHandler
    {
        private readonly CSEntities _dbcontext = new CSEntities();
        QueryValues queryValues = new QueryValues();
        HomeApiController homeObj = new HomeApiController();

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                HttpResponse response = context.Response;
                string filename = HttpContext.Current.Request.QueryString["Path"];
                bool EnableSubProduct = Convert.ToBoolean(HttpContext.Current.Request.QueryString["EnableSubProduct"]);
                string importType = "TableDesigner";
                string importTypetabledesigner = HttpContext.Current.Request.QueryString["importType"];
                if (context.Session["ValidateTableDesigner"] != null && filename.ToLower().Contains(".xls") && importType == importTypetabledesigner)
                {
                    var workbook = new Workbook();
                    if (context.Session["ValidateTableDesigner"] != null && filename.ToLower().Contains(".xls"))
                    {
                        if (context.Session["ValidateTableDesigner"] != null)
                        {
                            var tableSP = (DataTable)context.Session["ValidateTableDesigner"];
                            string tableNameSp = "TableDesigner";
                            const string tableNameSp1 = "TableDesigner";
                            int rowcntTempSP = tableSP.Rows.Count;
                            int jSP = 0;
                            int runningcntSP = 0;
                            do
                            {
                                int rowcntSP;
                                if (rowcntTempSP <= 65000)
                                {
                                    rowcntSP = rowcntTempSP;
                                    rowcntTempSP = rowcntSP - 65000;
                                }
                                else
                                {
                                    rowcntSP = 65000;
                                    rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                                }
                                jSP++;

                                if (jSP != 1)
                                {
                                    tableNameSp = tableNameSp1 + (jSP - 1);
                                }
                                var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                                workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                                for (int jj = 1; jj <= rowcntSP; jj++)
                                    for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                                    {
                                        if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                        }
                                        else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                        }
                                        else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);
                                        }
                                    }
                                int rowIndex = 2;
                                int temprunningcntSP = runningcntSP;
                                for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                                {
                                    var row = worksheetSp.Rows[rowIndex++];
                                    runningcntSP++;
                                    for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                                    {
                                        row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                    }
                                }
                            } while (rowcntTempSP > 0);

                        }
                    }
                    if (context.Session["tableDesignerFamilyExportTable"] != null && filename.ToLower().Contains(".xls"))
                    {
                        var tableDesignerFamilyExport = (DataTable)System.Web.HttpContext.Current.Session["tableDesignerFamilyExportTable"];

                        System.Web.HttpContext.Current.Session["tableDesignerFamilyExportTable"] = null;
                        if (tableDesignerFamilyExport.Rows.Count > 0)
                        {
                            string tableName = "TableDesigner";
                            int rowcntTemp = tableDesignerFamilyExport.Rows.Count;
                            int j = 0;
                            int runningcnt = 0;
                            do
                            {
                                int rowcnt;
                                if (rowcntTemp <= 65000)
                                {
                                    rowcnt = rowcntTemp;
                                    rowcntTemp = rowcnt - 65000;
                                }
                                else
                                {
                                    rowcnt = 65000;
                                    rowcntTemp = tableDesignerFamilyExport.Rows.Count - (runningcnt + 65000);
                                }
                                j++;
                                if (j != 1)
                                {
                                    tableName = tableName + (j - 1);
                                }
                                var worksheet = workbook.Worksheets.Add(tableName);
                                workbook.WindowOptions.SelectedWorksheet = worksheet;
                                for (int columnIndex = 0; columnIndex < tableDesignerFamilyExport.Columns.Count; columnIndex++)
                                {
                                    if (HttpUtility.HtmlDecode(tableDesignerFamilyExport.Columns[columnIndex].ColumnName) != "ID" && HttpUtility.HtmlDecode(tableDesignerFamilyExport.Columns[columnIndex].ColumnName) != "ROLE_ID" && HttpUtility.HtmlDecode(tableDesignerFamilyExport.Columns[columnIndex].ColumnName) != "CATALOGID")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableDesignerFamilyExport.Columns[columnIndex].ColumnName);
                                    }
                                }
                                int rowIndex = 2;
                                int temprunningcnt = runningcnt;
                                for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                                {
                                    var row = worksheet.Rows[rowIndex++];
                                    runningcnt++;
                                    for (int columnIndex = 0; columnIndex < tableDesignerFamilyExport.Rows[k].ItemArray.Length; columnIndex++)
                                    {
                                        row.Cells[columnIndex].Value = tableDesignerFamilyExport.Rows[k].ItemArray[columnIndex];
                                    }
                                }

                            }
                            while (rowcntTemp > 0);
                        }
                    }
                    if (workbook.Worksheets[0] != null)
                    {
                        var worksheet1 = workbook.Worksheets[0];
                        workbook.WindowOptions.SelectedWorksheet = worksheet1;
                    }
                    if (filename.ToLower().Contains(".xlsx"))
                    {
                        workbook.SetCurrentFormat(WorkbookFormat.Excel2007);
                    }
                    workbook.Save(context.Server.MapPath("~/Content/" + filename));

                    if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                    {
                        response.ClearContent();
                        response.Clear();
                        response.ContentType = "application/vnd.ms-excel";
                        response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", filename));
                        response.TransmitFile(context.Server.MapPath("~/Content/" + filename));
                        response.Flush();
                        response.End();
                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("File not found!");
                    }


                }

                else if (context.Session["MappingAttributeErrorLog"] != null && filename.ToLower().Contains(".xls") && importType != importTypetabledesigner)
                {
                    var workbook = new Workbook();
                    if (context.Session["MappingAttributeErrorLog"] != null && filename.ToLower().Contains(".xls"))
                    {
                        if (context.Session["MappingAttributeErrorLog"] != null)
                        {
                            var tableSP = (DataTable)context.Session["MappingAttributeErrorLog"];
                            string tableNameSp = "MappingAttribute";
                            const string tableNameSp1 = "MappingAttribute";
                            int rowcntTempSP = tableSP.Rows.Count;
                            int jSP = 0;
                            int runningcntSP = 0;
                            do
                            {
                                int rowcntSP;
                                if (rowcntTempSP <= 65000)
                                {
                                    rowcntSP = rowcntTempSP;
                                    rowcntTempSP = rowcntSP - 65000;
                                }
                                else
                                {
                                    rowcntSP = 65000;
                                    rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                                }
                                jSP++;

                                if (jSP != 1)
                                {
                                    tableNameSp = tableNameSp1 + (jSP - 1);
                                }
                                var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                                workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                                for (int jj = 1; jj <= rowcntSP; jj++)
                                    for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                                    {
                                        if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                        }
                                        else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                        }
                                        else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);
                                        }
                                    }
                                int rowIndex = 2;
                                int temprunningcntSP = runningcntSP;
                                for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                                {
                                    var row = worksheetSp.Rows[rowIndex++];
                                    runningcntSP++;
                                    for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                                    {
                                        row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                    }
                                }
                            } while (rowcntTempSP > 0);

                        }
                    }

                    if (workbook.Worksheets[0] != null)
                    {
                        var worksheet1 = workbook.Worksheets[0];
                        workbook.WindowOptions.SelectedWorksheet = worksheet1;
                    }
                    if (filename.ToLower().Contains(".xlsx"))
                    {
                        workbook.SetCurrentFormat(WorkbookFormat.Excel2007);
                    }
                    workbook.Save(context.Server.MapPath("~/Content/" + filename));

                    if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                    {
                        response.ClearContent();
                        response.Clear();
                        response.ContentType = "application/vnd.ms-excel";
                        response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", filename));
                        response.TransmitFile(context.Server.MapPath("~/Content/" + filename));
                        response.Flush();
                        response.End();
                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("File not found!");
                    }


                }

                ////************************* Duplicate Attribute names in mapping table error log  **************/////
                else if (context.Session["DuplicateMappingAttributes"] != null && filename.ToLower().Contains(".xls") && importType != importTypetabledesigner)
                {
                    var workbook = new Workbook();
                    if (context.Session["DuplicateMappingAttributes"] != null && filename.ToLower().Contains(".xls"))
                    {
                        if (context.Session["DuplicateMappingAttributes"] != null)
                        {
                            var tableSP = ((DataSet)context.Session["DuplicateMappingAttributes"]).Tables[0];
                            string tableNameSp = "DuplicateAttributes";
                            const string tableNameSp1 = "DuplicateAttributes";
                            int rowcntTempSP = tableSP.Rows.Count;
                            int jSP = 0;
                            int runningcntSP = 0;
                            do
                            {
                                int rowcntSP;
                                if (rowcntTempSP <= 65000)
                                {
                                    rowcntSP = rowcntTempSP;
                                    rowcntTempSP = rowcntSP - 65000;
                                }
                                else
                                {
                                    rowcntSP = 65000;
                                    rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                                }
                                jSP++;

                                if (jSP != 1)
                                {
                                    tableNameSp = tableNameSp1 + (jSP - 1);
                                }
                                var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                                workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                                for (int jj = 1; jj <= rowcntSP; jj++)
                                    for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                                    {
                                        if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                        }
                                        else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                        }
                                        else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);
                                        }
                                    }
                                int rowIndex = 2;
                                int temprunningcntSP = runningcntSP;
                                for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                                {
                                    var row = worksheetSp.Rows[rowIndex++];
                                    runningcntSP++;
                                    for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                                    {
                                        row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                    }
                                }
                            } while (rowcntTempSP > 0);

                        }
                    }

                    if (workbook.Worksheets[0] != null)
                    {
                        var worksheet1 = workbook.Worksheets[0];
                        workbook.WindowOptions.SelectedWorksheet = worksheet1;
                    }
                    if (filename.ToLower().Contains(".xlsx"))
                    {
                        workbook.SetCurrentFormat(WorkbookFormat.Excel2007);
                    }
                    workbook.Save(context.Server.MapPath("~/Content/" + filename));

                    if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                    {
                        response.ClearContent();
                        response.Clear();
                        response.ContentType = "application/vnd.ms-excel";
                        response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", filename));
                        response.TransmitFile(context.Server.MapPath("~/Content/" + filename));
                        response.Flush();
                        response.End();
                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("File not found!");
                    }


                }
            }
            catch (Exception ex)
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}