﻿using System;
using System.Drawing;
using System.Web;
using Stimulsoft.Report;
using Stimulsoft.Report.Components;
using System.IO;
using System.Drawing.Imaging;
using System.Xml;
using System.Text.RegularExpressions;
using LS.Data;
using LS.Web.Models;
using LS.Web.Controllers;// For Extracting values from ATTRIBUTE_DATARULE


namespace LS.Models
{
    public class Reports : Stimulsoft.Report.StiReport
    {
        public Reports(string converttype, string tempPath, string imagePath, string unSupportedImagePath, string imagemagickPath, int catalogId, string categoryId, string connectionString)
        {
            this.ConnectionString = connectionString;
            this.InitializeComponent();
            this.content = tempPath;
            this.ImagePath = imagePath;
            this.UnSupportedImagePath = unSupportedImagePath;
            this.CatalogId = catalogId;
            this.CategoryId = categoryId;
            this.imagemagickpath = imagemagickPath;
            this.converttype = converttype;

        }

        #region Family Attribute Alteration
        public string Attribute_Alteration(string code)
        {
            //if (code == "FamilyAttributeName")
            //{                
            //    string attrvalue = FamilySpecs.STRING_VALUE;
            //    if (attrvalue != "" && attrvalue != null)
            //    {
            //        return FamilySpecs.ATTRIBUTE_NAME;
            //    }
            //    else
            //    {
            //        return "";
            //    }
            //}
            if (code == "CategoryDecimal")
            {
                string attrdatatype = CategorySpecs.ATTRIBUTE_DATATYPE;
                string attrvalue = CategorySpecs.STRING_VALUE;

                if (attrvalue.ToString() != "" && attrvalue.ToString() != null)
                {
                    if (attrdatatype.StartsWith("Num"))
                    {
                        attrdatatype = attrdatatype.Remove(0, attrdatatype.IndexOf(',') + 1);
                        int noofdecimalplace = ((int)(StiReport.ChangeType((attrdatatype.TrimEnd(')')), typeof(int), true)));
                        if (attrvalue.Contains(".") && (noofdecimalplace != ((attrvalue.Length) - (attrvalue.IndexOf(".") + 1))))
                        {
                            if (noofdecimalplace != 6)
                            {
                                attrvalue = attrvalue.Remove(attrvalue.IndexOf('.') + 1 + noofdecimalplace);
                            }
                            if (noofdecimalplace == 0)
                            {
                                attrvalue = attrvalue.TrimEnd('.');
                            }
                        }
                    }


                }

                // Extracting Pr/Sf/Cond/Cust txt and applying 
                if (CategorySpecs.ATTRIBUTE_DATARULE != "" && CategorySpecs.ATTRIBUTE_DATARULE != null)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    string dataruleXML = CategorySpecs.ATTRIBUTE_DATARULE.ToString();
                    xmldoc.LoadXml(dataruleXML);
                    XmlNode DRootNode = xmldoc.DocumentElement;
                    if (DRootNode != null)
                    {
                        XmlNodeList DChdnode = DRootNode.ChildNodes;
                        string sValue = attrvalue;
                        string _prefix = "", _suffix = "";
                        string _replaceText = "", _emptyCondition = "", _fornumeric = "", _headeroptions = "";
                        int rowCount = 0;
                        //if (attrvalue != null && attrvalue != "")
                        _prefix = DChdnode[0]["Prefix"].InnerText.ToString();
                        _suffix = DChdnode[0]["Suffix"].InnerText.ToString();
                        _replaceText = DChdnode[0]["CustomValue"].InnerText.ToString();
                        _emptyCondition = DChdnode[0]["Condition"].InnerText.ToString();
                        _fornumeric = DChdnode[0]["ApplyForNumericOnly"].InnerText.ToString();
                        _headeroptions = DChdnode[0]["ApplyTo"].InnerText.ToString();


                        #region "From DataRule"
                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                if (sValue == "0")
                                {
                                    if (_emptyCondition == "0.000000")
                                    {
                                        sValue = _emptyCondition;
                                    }
                                }
                                if (sValue == "Null")
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                }
                                else
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                }
                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                if (_replaceText.ToString() != string.Empty)
                                {
                                    if (_replaceText.ToString() != sValue)
                                    {
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText))
                                            {
                                                if (_headeroptions == "All")
                                                {
                                                    attrvalue = _prefix + _replaceText + _suffix;
                                                    rowCount++;
                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        attrvalue = _prefix + _replaceText + _suffix;
                                                        rowCount++;
                                                    }
                                                    else
                                                    {
                                                        attrvalue = _replaceText;
                                                        rowCount++;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                attrvalue = _replaceText;
                                                rowCount++;

                                            }
                                        }
                                        else
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                attrvalue = _prefix + _replaceText + _suffix;
                                                rowCount++;

                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    attrvalue = _prefix + _replaceText + _suffix;
                                                    rowCount++;

                                                }
                                                else
                                                {
                                                    attrvalue = _replaceText;
                                                    rowCount++;

                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            attrvalue = _prefix + _replaceText + _suffix;
                                            rowCount++;

                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                attrvalue = _prefix + _replaceText + _suffix;
                                                rowCount++;

                                            }
                                            else
                                            {
                                                attrvalue = _replaceText;
                                                rowCount++;

                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _replaceText = sValue;
                            }
                        }
                        #endregion

                    }
                }
                return attrvalue;
            }
            if (code == "FamilyDecimal")
            {
                string attrdatatype = FamilySpecs.ATTRIBUTE_DATATYPE;
                string attrvalue = FamilySpecs.STRING_VALUE;

                if (attrvalue.ToString() != "" && attrvalue.ToString() != null)
                {
                    if (attrdatatype.StartsWith("Num"))
                    {
                        attrdatatype = attrdatatype.Remove(0, attrdatatype.IndexOf(',') + 1);
                        int noofdecimalplace = ((int)(StiReport.ChangeType((attrdatatype.TrimEnd(')')), typeof(int), true)));
                        if (attrvalue.Contains(".") && (noofdecimalplace != ((attrvalue.Length) - (attrvalue.IndexOf(".") + 1))))
                        {
                            if (noofdecimalplace != 6)
                            {
                                attrvalue = attrvalue.Remove(attrvalue.IndexOf('.') + 1 + noofdecimalplace);
                            }
                            if (noofdecimalplace == 0)
                            {
                                attrvalue = attrvalue.TrimEnd('.');
                            }
                        }
                    }


                }

                // Extracting Pr/Sf/Cond/Cust txt and applying 
                if (FamilySpecs.ATTRIBUTE_DATARULE != "" && FamilySpecs.ATTRIBUTE_DATARULE != null)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    string dataruleXML = FamilySpecs.ATTRIBUTE_DATARULE.ToString();
                    xmldoc.LoadXml(dataruleXML);
                    XmlNode DRootNode = xmldoc.DocumentElement;
                    if (DRootNode != null)
                    {
                        XmlNodeList DChdnode = DRootNode.ChildNodes;
                        string sValue = attrvalue;
                        string _prefix = "", _suffix = "";
                        string _replaceText = "", _emptyCondition = "", _fornumeric = "", _headeroptions = "";
                        int rowCount = 0;
                        //if (attrvalue != null && attrvalue != "")
                        _prefix = DChdnode[0]["Prefix"].InnerText.ToString();
                        _suffix = DChdnode[0]["Suffix"].InnerText.ToString();
                        _replaceText = DChdnode[0]["CustomValue"].InnerText.ToString();
                        _emptyCondition = DChdnode[0]["Condition"].InnerText.ToString();
                        _fornumeric = DChdnode[0]["ApplyForNumericOnly"].InnerText.ToString();
                        _headeroptions = DChdnode[0]["ApplyTo"].InnerText.ToString();


                        #region "From DataRule"
                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                if (sValue == "0")
                                {
                                    if (_emptyCondition == "0.000000")
                                    {
                                        sValue = _emptyCondition;
                                    }
                                }
                                if (sValue == "Null")
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                }
                                else
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                }
                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                if (_replaceText.ToString() != string.Empty)
                                {
                                    if (_replaceText.ToString() != sValue)
                                    {
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText))
                                            {
                                                if (_headeroptions == "All")
                                                {
                                                    attrvalue = _prefix + _replaceText + _suffix;
                                                    rowCount++;
                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        attrvalue = _prefix + _replaceText + _suffix;
                                                        rowCount++;
                                                    }
                                                    else
                                                    {
                                                        attrvalue = _replaceText;
                                                        rowCount++;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                attrvalue = _replaceText;
                                                rowCount++;

                                            }
                                        }
                                        else
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                attrvalue = _prefix + _replaceText + _suffix;
                                                rowCount++;

                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    attrvalue = _prefix + _replaceText + _suffix;
                                                    rowCount++;

                                                }
                                                else
                                                {
                                                    attrvalue = _replaceText;
                                                    rowCount++;

                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            attrvalue = _prefix + _replaceText + _suffix;
                                            rowCount++;

                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                attrvalue = _prefix + _replaceText + _suffix;
                                                rowCount++;

                                            }
                                            else
                                            {
                                                attrvalue = _replaceText;
                                                rowCount++;

                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _replaceText = sValue;
                            }
                        }
                        #endregion

                    }
                }
                return attrvalue;
            }
            else if (code == "ProductDecimal")
            {
                string attrdatatype = TBL3.ATTRIBUTE_DATATYPE;
                string attrvalue = TBL3.STRING_VALUE.Replace("&amp;", "&");

                if (attrdatatype.StartsWith("Num") && attrvalue.ToString() != "" && attrvalue.ToString() != null)
                {
                    attrdatatype = attrdatatype.Remove(0, attrdatatype.IndexOf(',') + 1);
                    int noofdecimalplace = ((int)(StiReport.ChangeType((attrdatatype.TrimEnd(')')), typeof(int), true)));
                    if (attrvalue.Contains("."))
                    {
                        if (noofdecimalplace != 6)
                        {
                            attrvalue = attrvalue.Remove(attrvalue.IndexOf('.') + 1 + noofdecimalplace);
                        }
                        if (noofdecimalplace == 0)
                        {
                            attrvalue = attrvalue.TrimEnd('.');
                        }
                    }
                }

                // Extracting Pr/Sf/Cond/Cust txt and applying 
                if (TBL3.ATTRIBUTE_DATARULE != "" && TBL3.ATTRIBUTE_DATARULE != null)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    string dataruleXML = TBL3.ATTRIBUTE_DATARULE.ToString();
                    xmldoc.LoadXml(dataruleXML);
                    XmlNode DRootNode = xmldoc.DocumentElement;
                    if (DRootNode != null)
                    {
                        XmlNodeList DChdnode = DRootNode.ChildNodes;
                        string sValue = attrvalue;
                        string _prefix = "", _suffix = "";
                        string _replaceText = "", _emptyCondition = "", _fornumeric = "", _headeroptions = "";
                        int rowCount = 0;
                        //if (attrvalue != null && attrvalue != "")
                        _prefix = DChdnode[0]["Prefix"].InnerText.ToString();
                        _suffix = DChdnode[0]["Suffix"].InnerText.ToString();
                        _replaceText = DChdnode[0]["CustomValue"].InnerText.ToString();
                        _emptyCondition = DChdnode[0]["Condition"].InnerText.ToString();
                        _fornumeric = DChdnode[0]["ApplyForNumericOnly"].InnerText.ToString();
                        _headeroptions = DChdnode[0]["ApplyTo"].InnerText.ToString();


                        #region "From DataRule"
                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                if (sValue == "0")
                                {
                                    if (_emptyCondition == "0.000000")
                                    {
                                        sValue = _emptyCondition;
                                    }
                                }
                                if (sValue == "Null")
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                }
                                else
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                }
                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                //if (_replaceText != "")
                                //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[6].ToString().Trim() + ": </b><br>");
                                //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                if (_replaceText.ToString() != string.Empty)
                                {
                                    if (_replaceText.ToString() != sValue)
                                    {
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText))
                                            {
                                                if (_headeroptions == "All")
                                                {
                                                    attrvalue = _prefix + _replaceText + _suffix;
                                                    rowCount++;

                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        attrvalue = _prefix + _replaceText + _suffix;
                                                        rowCount++;
                                                    }
                                                    else
                                                    {
                                                        attrvalue = _replaceText;
                                                        rowCount++;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                attrvalue = _replaceText;
                                                rowCount++;
                                            }
                                        }
                                        else
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                attrvalue = _prefix + _replaceText + _suffix;
                                                rowCount++;
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    attrvalue = _prefix + _replaceText + _suffix;
                                                    rowCount++;
                                                }
                                                else
                                                {
                                                    attrvalue = _replaceText;
                                                    rowCount++;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            attrvalue = _prefix + _replaceText + _suffix;
                                            rowCount++;
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                attrvalue = _prefix + _replaceText + _suffix;
                                                rowCount++;
                                            }
                                            else
                                            {
                                                attrvalue = _replaceText;
                                                rowCount++;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _replaceText = sValue;
                            }
                        }
                        #endregion
                        attrvalue = _prefix + attrvalue + _suffix;
                    }
                }
                return attrvalue;
            }
            else if (code == "MultipleTable")
            {
                string attrdatatype = MultipleTables.ATTRIBUTE_DATATYPE;
                string attrvalue = MultipleTables.STRING_VALUE.Replace("&amp;", "&"); ;

                if (attrdatatype.StartsWith("Num") && attrvalue.ToString() != "" && attrvalue.ToString() != null)
                {
                    attrdatatype = attrdatatype.Remove(0, attrdatatype.IndexOf(',') + 1);
                    int noofdecimalplace = ((int)(StiReport.ChangeType((attrdatatype.TrimEnd(')')), typeof(int), true)));
                    if (attrvalue.Contains("."))
                    {
                        if (noofdecimalplace != 6)
                        {
                            attrvalue = attrvalue.Remove(attrvalue.IndexOf('.') + 1 + noofdecimalplace);
                        }
                        if (noofdecimalplace == 0)
                        {
                            attrvalue = attrvalue.TrimEnd('.');
                        }
                    }
                }

                // Extracting Pr/Sf/Cond/Cust txt and applying 
                if (MultipleTables.ATTRIBUTE_DATARULE != "" && MultipleTables.ATTRIBUTE_DATARULE != null)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    string dataruleXML = MultipleTables.ATTRIBUTE_DATARULE.ToString();
                    xmldoc.LoadXml(dataruleXML);
                    XmlNode DRootNode = xmldoc.DocumentElement;
                    if (DRootNode != null)
                    {
                        XmlNodeList DChdnode = DRootNode.ChildNodes;
                        string sValue = attrvalue;
                        string _prefix = "", _suffix = "";
                        string _replaceText = "", _emptyCondition = "", _fornumeric = "", _headeroptions = "";
                        int rowCount = 0;
                        //if (attrvalue != null && attrvalue != "")
                        _prefix = DChdnode[0]["Prefix"].InnerText.ToString();
                        _suffix = DChdnode[0]["Suffix"].InnerText.ToString();
                        _replaceText = DChdnode[0]["CustomValue"].InnerText.ToString();
                        _emptyCondition = DChdnode[0]["Condition"].InnerText.ToString();
                        _fornumeric = DChdnode[0]["ApplyForNumericOnly"].InnerText.ToString();
                        _headeroptions = DChdnode[0]["ApplyTo"].InnerText.ToString();


                        #region "From DataRule"
                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                if (sValue == "0")
                                {
                                    if (_emptyCondition == "0.000000")
                                    {
                                        sValue = _emptyCondition;
                                    }
                                }
                                if (sValue == "Null")
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                }
                                else
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                }
                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                if (_replaceText.ToString() != string.Empty)
                                {
                                    if (_replaceText.ToString() != sValue)
                                    {
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText))
                                            {
                                                if (_headeroptions == "All")
                                                {
                                                    attrvalue = _prefix + _replaceText + _suffix;
                                                    rowCount++;

                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        attrvalue = _prefix + _replaceText + _suffix;
                                                        rowCount++;
                                                    }
                                                    else
                                                    {
                                                        attrvalue = _replaceText;
                                                        rowCount++;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                attrvalue = _replaceText;
                                                rowCount++;
                                            }
                                        }
                                        else
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                attrvalue = _prefix + _replaceText + _suffix;
                                                rowCount++;
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    attrvalue = _prefix + _replaceText + _suffix;
                                                    rowCount++;
                                                }
                                                else
                                                {
                                                    attrvalue = _replaceText;
                                                    rowCount++;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            attrvalue = _prefix + _replaceText + _suffix;
                                            rowCount++;
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                attrvalue = _prefix + _replaceText + _suffix;
                                                rowCount++;
                                            }
                                            else
                                            {
                                                attrvalue = _replaceText;
                                                rowCount++;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _replaceText = sValue;
                            }
                        }
                        #endregion
                        attrvalue = _prefix + attrvalue + _suffix;
                    }
                }
                return attrvalue;
            }
            else
            {
                return "0";
            }

        }
        #endregion
        public bool Isnumber(string refValue)
        {
            if (refValue == "")
            {
                return false;
            }
            //const string strReg =
            //    @"^((\d?)|(([-+]?\d+\.?\d*)|([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d+\.?\d*))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d+\.?\d*)))$";
            var retval = false;
            //var res = new Regex(strReg);
            //if (res.IsMatch(refValue))
            //{
            //    retval = true;
            //}
            //return retval;
            const string strRegx = @"^[0-9]*(\.)?[0-9]+$";
            var re = new Regex(strRegx);
            if (re.IsMatch(refValue))
            {
                retval = true;
            }
            return retval;
        }
        #region "MANUAL METHODS"
        public static Bitmap Convert_Text_to_Image(string txt, string fontname, int fontsize)
        {
            //creating bitmap image
            Bitmap bmp = new Bitmap(1, 1);

            //FromImage method creates a new Graphics from the specified Image.
            Graphics graphics = Graphics.FromImage(bmp);
            // Create the Font object for the image text drawing.
            Font font = new Font(fontname, fontsize);
            // Instantiating object of Bitmap image again with the correct size for the text and font.
            SizeF stringSize = graphics.MeasureString(txt, font);
            bmp = new Bitmap(bmp, (int)stringSize.Width, (int)stringSize.Height);
            graphics = Graphics.FromImage(bmp);

            /* It can also be a way
           bmp = new Bitmap(bmp, new Size((int)graphics.MeasureString(txt, font).Width, (int)graphics.MeasureString(txt, font).Height));*/

            //Draw Specified text with specified format 
            graphics.DrawString(txt, font, Brushes.Black, 0, 0);
            font.Dispose();
            graphics.Flush();
            graphics.Dispose();
            return bmp;     //return Bitmap Image 

        }

        ImageFormat getImageFormat(String path)
        {
            switch (Path.GetExtension(path))
            {
                case ".bmp": return ImageFormat.Bmp;
                case ".gif": return ImageFormat.Gif;
                case ".jpg": return ImageFormat.Jpeg;
                case ".png": return ImageFormat.Png;
                default: break;
            }
            return ImageFormat.Jpeg;
        }

        byte[] getResizedImage(String path, int width, int height)
        {
            Bitmap imgIn = new Bitmap(path);
            double y = imgIn.Height;
            double x = imgIn.Width;

            double factor = 1;
            if (width > 0)
            {
                factor = width / x;
            }
            else if (height > 0)
            {
                factor = height / y;
            }
            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            Bitmap imgOut = new Bitmap((int)(x * factor), (int)(y * factor));

            // Set DPI of image (xDpi, yDpi)
            imgOut.SetResolution(72, 72);

            Graphics g = Graphics.FromImage(imgOut);
            g.Clear(Color.White);
            g.DrawImage(imgIn, new Rectangle(0, 0, (int)(factor * x), (int)(factor * y)),
              new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

            imgOut.Save(outStream, getImageFormat(path));
            return outStream.ToArray();
        }
        #endregion

        #region StiReport Designer generated code - do not modify
        public Stimulsoft.Report.Dictionary.StiDataRelation ParentName11;
        public Stimulsoft.Report.Dictionary.StiDataRelation ParentName6;
        public Stimulsoft.Report.Dictionary.StiDataRelation ParentName5;
        public Stimulsoft.Report.Dictionary.StiDataRelation ParentName4;
        public Stimulsoft.Report.Dictionary.StiDataRelation ParentName3;
        public Stimulsoft.Report.Dictionary.StiDataRelation ParentName2;
        public Stimulsoft.Report.Dictionary.StiDataRelation ParentName1;
        public Stimulsoft.Report.Dictionary.StiDataRelation ParentName;
        public string ImagePath;
        public string ConnectionString;
        public string UnSupportedImagePath;
        public int CatalogId;
        public string CategoryId;
        public string imagemagickpath;
        public string converttype = string.Empty;
        public string Exefile = string.Empty;
        //public string prodImages = HttpContext.Current.Server.MapPath("~/Content/ProdImages");
        public string content;
        public Stimulsoft.Report.Components.StiPage Cover;
        public Stimulsoft.Report.Components.StiText Text2;
        public Stimulsoft.Report.Components.StiWatermark Cover_Watermark;
        public Stimulsoft.Report.Components.StiPage TOC;
        public Stimulsoft.Report.Components.StiPageHeaderBand PageHeaderBand1;
        public Stimulsoft.Report.Components.StiText Text8;
        public Stimulsoft.Report.Components.StiGroupHeaderBand GroupHeaderBand1;
        public Stimulsoft.Report.Components.StiText Text4;
        public Stimulsoft.Report.Components.StiText Text6;
        public Stimulsoft.Report.Components.StiDataBand DataBand4;
        public Stimulsoft.Report.Components.StiGroupHeaderBand GroupHeaderBand2;
        public Stimulsoft.Report.Components.StiText Text5;
        public Stimulsoft.Report.Components.StiText Text7;
        public Stimulsoft.Report.Components.StiDataBand DataBand5;
        public Stimulsoft.Report.Components.StiWatermark TOC_Watermark;
        public Stimulsoft.Report.Components.StiPage Main;
        public Stimulsoft.Report.Components.StiPageFooterBand PageFooterBand1;
        public Stimulsoft.Report.Components.StiText Text9;
        public Stimulsoft.Report.Components.StiText Text10;
        public Stimulsoft.Report.Components.StiText Text12;
        public Stimulsoft.Report.Components.StiHorizontalLinePrimitive HorizontalLinePrimitive1;
        public Stimulsoft.Report.Components.StiDataBand DataBand1;
        public Stimulsoft.Report.Components.StiText Text1;
        public Stimulsoft.Report.Components.StiImage Image1;
        //public Stimulsoft.Report.Components.StiImage Image2;
        public Stimulsoft.Report.Components.StiText Text17;
        public Stimulsoft.Report.Components.StiText Text18;
        //public Stimulsoft.Report.Components.StiText Text17;
        //public Stimulsoft.Report.Components.StiText Text18;
        public Stimulsoft.Report.Components.StiText Text19;
        //public Stimulsoft.Report.Components.StiText Text20;
        //public Stimulsoft.Report.Components.StiText Text21;
        //public Stimulsoft.Report.Components.StiText Text22;
        //public Stimulsoft.Report.Components.StiText Text23;
        //public Stimulsoft.Report.Components.StiText Text24;
        //public Stimulsoft.Report.Components.StiText Text25;
        //public Stimulsoft.Report.Components.StiText Text26;
        //public Stimulsoft.Report.Components.StiText Text27;
        //public Stimulsoft.Report.Components.StiText Text28;
        public Stimulsoft.Report.Components.StiDataBand DataBand2;
        public Stimulsoft.Report.Components.StiText Text3;
        public Stimulsoft.Report.Components.StiDataBand DataBand20;
        public Stimulsoft.Report.Components.StiDataBand DataBand21;
        //public Stimulsoft.Report.Components.StiText Text17;
        //public Stimulsoft.Report.Components.StiText Text302;
        public Stimulsoft.Report.Components.StiDataBand DataBand13;
        public Stimulsoft.Report.Components.StiText Text29;
        public Stimulsoft.Report.Components.StiText Text30;
        public Stimulsoft.Report.Components.StiDataBand DataBand14;
        public Stimulsoft.Report.Components.StiText Text34;
        public Stimulsoft.Report.Components.StiImage Image3;
        public Stimulsoft.Report.Components.StiDataBand DataBand3;
        public Stimulsoft.Report.CrossTab.StiCrossTab CrossTab1;
        public Stimulsoft.Report.CrossTab.StiCrossColumnTotal CrossTab1_ColTotal1;
        public Stimulsoft.Report.CrossTab.StiCrossRowTotal CrossTab1_RowTotal1;
        public Stimulsoft.Report.CrossTab.StiCrossTitle CrossTab1_Row1_Title;
        public Stimulsoft.Report.CrossTab.StiCrossTitle CrossTab1_LeftTitle;
        public Stimulsoft.Report.CrossTab.StiCrossTitle CrossTab1_RightTitle;
        public Stimulsoft.Report.CrossTab.StiCrossRow CrossTab1_Row1;
        public Stimulsoft.Report.CrossTab.StiCrossColumn CrossTab1_Column1;
        public Stimulsoft.Report.CrossTab.StiCrossSummary CrossTab1_Sum1;
        public Stimulsoft.Report.Components.StiGroupHeaderBand GroupHeaderBand6;
        public Stimulsoft.Report.Components.StiText Text31;
        public Stimulsoft.Report.Components.StiDataBand DataBand10;
        public Stimulsoft.Report.Components.StiText Text15;
        public Stimulsoft.Report.Components.StiDataBand DataBand9;
        public Stimulsoft.Report.CrossTab.StiCrossTab CrossTab2;
        public Stimulsoft.Report.CrossTab.StiCrossColumnTotal CrossTab2_ColTotal1;
        public Stimulsoft.Report.CrossTab.StiCrossRowTotal CrossTab2_RowTotal1;
        public Stimulsoft.Report.CrossTab.StiCrossTitle CrossTab2_Row1_Title;
        public Stimulsoft.Report.CrossTab.StiCrossTitle CrossTab2_LeftTitle;
        public Stimulsoft.Report.CrossTab.StiCrossTitle CrossTab2_RightTitle;
        public Stimulsoft.Report.CrossTab.StiCrossRow CrossTab2_Row1;
        public Stimulsoft.Report.CrossTab.StiCrossColumn CrossTab2_Column1;
        public Stimulsoft.Report.CrossTab.StiCrossSummary CrossTab2_Sum1;
        public Stimulsoft.Report.Components.StiGroupHeaderBand GroupHeaderBand7;
        public Stimulsoft.Report.Components.StiText Text32;
        public Stimulsoft.Report.Components.StiDataBand DataBand12;
        public Stimulsoft.Report.Components.StiText Text16;
        public Stimulsoft.Report.Components.StiDataBand DataBand11;
        public Stimulsoft.Report.CrossTab.StiCrossTab CrossTab3;
        public Stimulsoft.Report.CrossTab.StiCrossColumnTotal CrossTab3_ColTotal1;
        public Stimulsoft.Report.CrossTab.StiCrossRowTotal CrossTab3_RowTotal1;
        public Stimulsoft.Report.CrossTab.StiCrossTitle CrossTab3_Row1_Title;
        public Stimulsoft.Report.CrossTab.StiCrossTitle CrossTab3_LeftTitle;
        public Stimulsoft.Report.CrossTab.StiCrossTitle CrossTab3_RightTitle;
        public Stimulsoft.Report.CrossTab.StiCrossRow CrossTab3_Row1;
        public Stimulsoft.Report.CrossTab.StiCrossColumn CrossTab3_Column1;
        public Stimulsoft.Report.CrossTab.StiCrossSummary CrossTab3_Sum1;
        public Stimulsoft.Report.Components.StiWatermark Main_Watermark;
        public Stimulsoft.Report.Components.StiPage ItemIndex;
        public Stimulsoft.Report.Components.StiPageHeaderBand PageHeaderBand2;
        public Stimulsoft.Report.Components.StiText Text11;
        public Stimulsoft.Report.Components.StiGroupHeaderBand GroupHeaderBand4;
        public Stimulsoft.Report.Components.StiDataBand DataBand7;
        public Stimulsoft.Report.Components.StiGroupHeaderBand GroupHeaderBand5;
        public Stimulsoft.Report.Components.StiDataBand DataBand8;
        public Stimulsoft.Report.Components.StiGroupHeaderBand GroupHeaderBand3;
        public Stimulsoft.Report.Components.StiText Text13;
        public Stimulsoft.Report.Components.StiText Text14;
        public Stimulsoft.Report.Components.StiDataBand DataBand6;
        public Stimulsoft.Report.Components.StiWatermark ItemIndex_Watermark;
        public Stimulsoft.Report.Print.StiPrinterSettings Report_PrinterSettings;
        public TBL1DataSource TBL1;
        public TBL1DataSource TBL1Category;
        public TBL2DataSource TBL2;
        public TBL3DataSource TBL3;
        public MultipleTablesDataSource MultipleTables;
        public MultipleTableSoruceDataSource MultipleTableSoruce;
        public ReferenceTablesDataSource ReferenceTables;
        public ReferenceTableSourceDataSource ReferenceTableSource;
        public FamilySpecsDataSource FamilySpecs;
        public CategorySpecsDataSource CategorySpecs;
        public TBL4DataSource TBL4;

        public void Text2__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Cover Page for " + ToString(sender, TBL1.CATALOG_NAME, true);
        }

        public void Text2_BeforePrint(object sender, System.EventArgs e)
        {
            Text2.Top = 2;
        }

        public void Text8__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Contents";
        }

        public void GroupHeaderBand1__GetValue(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = TBL1.CATEGORY_ID;
        }

        public void Text4__GetHyperlink(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = "#" + TBL1.CATEGORY_ID + TBL1.CATEGORY_NAME;
        }

        public void Text4__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, TBL1.CATEGORY_NAME, true);
        }

        public void Text6__GetHyperlink(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = "#" + TBL1.CATEGORY_ID + TBL1.CATEGORY_NAME;
        }

        public void Text6__GetTag(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = ToString(sender, TBL1.CATEGORY_ID, false) + ToString(sender, TBL1.CATEGORY_NAME, false);
        }

        public void Text6__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "#%#{GetAnchorPageNumber(sender.TagValue)}";
            e.StoreToPrinted = true;
        }

        public System.String Text6_GetValue_End(Stimulsoft.Report.Components.StiComponent sender)
        {
            if (sender.TagValue != null)
            {
                return ToString(sender, GetAnchorPageNumber(sender.TagValue), true);
            }
            else
            {
                return ToString(sender, GetAnchorPageNumber("Sample"), true);
            }
        }

        public void GroupHeaderBand2__GetValue(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = TBL2.FAMILY_ID;
        }

        public void Text5__GetHyperlink(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = "#" + TBL1.CATEGORY_ID + TBL1.CATEGORY_NAME + TBL2.FAMILY_ID + TBL2.FAMILY_NAME;
        }

        public void Text5__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, TBL2.FAMILY_NAME, true);
        }

        public void Text7__GetHyperlink(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = "#" + TBL1.CATEGORY_ID + TBL1.CATEGORY_NAME + TBL2.FAMILY_ID + TBL2.FAMILY_NAME;
        }

        public void Text7__GetTag(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = ToString(sender, TBL1.CATEGORY_ID, false) + ToString(sender, TBL1.CATEGORY_NAME, false) + ToString(sender, TBL2.FAMILY_ID, false) + ToString(sender, TBL2.FAMILY_NAME, false);
        }

        public void Text7__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "#%#{GetAnchorPageNumber(sender.TagValue)}";
            e.StoreToPrinted = true;
        }

        public System.String Text7_GetValue_End(Stimulsoft.Report.Components.StiComponent sender)
        {
            if (sender.TagValue != null)
            {
                return ToString(sender, GetAnchorPageNumber(sender.TagValue), true);
            }
            else
            {
                return ToString(sender, GetAnchorPageNumber("Sample"), true);
            }
            // return ToString(sender, GetAnchorPageNumber(sender.TagValue), true);
        }

        public void Text9__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, TBL1.CATALOG_NAME, true);
        }

        public void Text10__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "#%#{PageNofM}";
            e.StoreToPrinted = true;
        }

        public System.String Text10_GetValue_End(Stimulsoft.Report.Components.StiComponent sender)
        {
            return ToString(sender, PageNofM, true);
        }

        public void Text12__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, Time, true);
        }

        public void Text1__GetBookmark(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = ToString(sender, TBL1.CATEGORY_ID + TBL1.CATEGORY_NAME, false);
        }

        public void Text1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, TBL1.CATEGORY_NAME, true);
        }

        public void Text1_BeforePrint(object sender, System.EventArgs e)
        {
            AddAnchor(TBL1.CATEGORY_ID + TBL1.CATEGORY_NAME);
        }

        public void Image1_BeforePrint(object sender, System.EventArgs e)
        {
            Image1.File = CategorySpecs.STRING_VALUE == "" ? UnSupportedImagePath : File.Exists(ImagePath.ToString() + CategorySpecs.STRING_VALUE.ToString()) == true ? ImagePath.ToString() + CategorySpecs.STRING_VALUE.ToString() : UnSupportedImagePath; ;
        }

        //public void Image2_BeforePrint(object sender, System.EventArgs e)
        //{
        //    string ImageLocation1 = ImageConversion_category(ImagePath.ToString() + TBL1.IMAGE_FILE2.ToString());
        //    Image2.File = TBL1.IMAGE_FILE2 == "" ? UnSupportedImagePath : ImageLocation1;
        //}

        //public void Text17__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = ToString(sender, TBL1.SHORT_DESC, true);
        //}

        //public void Text18__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = "Short Description";
        //}

        public void Text19__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, CategorySpecs.ATTRIBUTE_NAME, true);
        }

        //public void Text20__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = "Custom Text";
        //}

        //public void Text21__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = ToString(sender, TBL1.CUSTOM_TEXT_FIELD2, true);
        //}

        //public void Text22__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = ToString(sender, TBL1.CUSTOM_TEXT_FIELD3, true);
        //}

        //public void Text23__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = ToString(sender, TBL1.CUSTOM_NUM_FIELD1, true);
        //}

        //public void Text24__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = "Custom Number";
        //}

        //public void Text25__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = ToString(sender, TBL1.CUSTOM_NUM_FIELD2, true);
        //}

        //public void Text26__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = ToString(sender, TBL1.CUSTOM_NUM_FIELD3, true);
        //}

        //public void Text27__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = ToString(sender, TBL1.IMAGE_NAME, true);
        //}

        //public void Text28__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        //{
        //    e.Value = ToString(sender, TBL1.IMAGE_NAME2, true);
        //}
        public void DataBand20__GetFilter(object sender, Stimulsoft.Report.Events.StiFilterEventArgs e)
        {
            e.Value = (this.CategorySpecs.ATTRIBUTE_TYPE.ToString().ToLower() != this.ToString("23").ToLower());
        }
        public void DataBand21__GetFilter(object sender, Stimulsoft.Report.Events.StiFilterEventArgs e)
        {
            e.Value = (this.CategorySpecs.ATTRIBUTE_TYPE.ToString().ToLower() == this.ToString("23").ToLower());
        }
        public void Text17__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, CategorySpecs.STRING_VALUE, true);
        }

        public void Text18__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, CategorySpecs.ATTRIBUTE_NAME, true);
        }
        public void Text3__GetBookmark(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = ToString(sender, TBL1.CATEGORY_ID + TBL1.CATEGORY_NAME + TBL2.FAMILY_ID + TBL2.FAMILY_NAME, false);
        }

        public void Text3__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, TBL2.FAMILY_NAME, true);
        }

        public void Text3_BeforePrint(object sender, System.EventArgs e)
        {
            AddAnchor(TBL1.CATEGORY_ID + TBL1.CATEGORY_NAME + TBL2.FAMILY_ID + TBL2.FAMILY_NAME);
        }

        public void DataBand13__GetFilter(object sender, Stimulsoft.Report.Events.StiFilterEventArgs e)
        {
            e.Value = (this.FamilySpecs.ATTRIBUTE_TYPE.ToString().ToLower() != this.ToString("9").ToLower());
        }


        public void Text29__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, Attribute_Alteration("FamilyDecimal"), true);
        }

        public void Text30__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, FamilySpecs.ATTRIBUTE_NAME, true);
        }

        public void DataBand14__GetFilter(object sender, Stimulsoft.Report.Events.StiFilterEventArgs e)
        {
            e.Value = (this.FamilySpecs.ATTRIBUTE_TYPE.ToString().ToLower() == this.ToString("9").ToLower());
        }

        public void Text34__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, FamilySpecs.ATTRIBUTE_NAME, true);
        }

        public void Image3_BeforePrint(object sender, System.EventArgs e)
        {
            Image3.File = FamilySpecs.STRING_VALUE == "" ? UnSupportedImagePath : File.Exists(ImagePath.ToString() + FamilySpecs.STRING_VALUE.ToString()) == true ? ImagePath.ToString() + FamilySpecs.STRING_VALUE.ToString() : UnSupportedImagePath; ;
        }

        public void CrossTab1_ColTotal1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Total";
        }

        public void CrossTab1_RowTotal1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Total";
        }

        public void CrossTab1_Row1_Title__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "PRODUCT_ID";
        }

        public void CrossTab1_LeftTitle__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "TBL3";
        }

        public void CrossTab1_RightTitle__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "ATTRIBUTE_NAME";
        }

        private void CrossTab1_Row1_Conditions(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            decimal value = 0;
            if (e.Value is decimal)
            {
                value = ((decimal)(e.Value));
            }
            string tag = ((StiComponent)sender).TagValue as string;
            string tooltip = ((StiComponent)sender).ToolTipValue as string;
            string hyperlink = ((StiComponent)sender).HyperlinkValue as string;
            if ((this.TBL3.PRODUCT_ID.ToString().ToLower() != this.ToString("").ToLower()))
            {
                ((Stimulsoft.Report.Components.IStiTextBrush)(sender)).TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Red);
                ((Stimulsoft.Report.Components.IStiBrush)(sender)).Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                Stimulsoft.Report.Components.StiConditionHelper.ApplyFont(sender, new System.Drawing.Font("Arial", 8F), Stimulsoft.Report.Components.StiConditionPermissions.All);
                ((Stimulsoft.Report.Components.IStiBorder)(sender)).Border = ((Stimulsoft.Base.Drawing.StiBorder)(((Stimulsoft.Report.Components.IStiBorder)(sender)).Border.Clone()));
                ((Stimulsoft.Report.Components.IStiBorder)(sender)).Border.Side = Stimulsoft.Base.Drawing.StiBorderSides.None;
                ((Stimulsoft.Report.Components.StiComponent)(sender)).Enabled = false;
                return;
            }
        }

        public void CrossTab1_Row1__GetDisplayCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = TBL3.PRODUCT_ID;
        }

        public void CrossTab1_Row1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "PRODUCT_ID";
        }

        public void CrossTab1_Row1__GetCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = TBL3.PRODUCT_ID;
        }

        public void CrossTab1_Column1__GetDisplayCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {

            CSEntities _dbcontext = new CSEntities();
            QueryValues queryValues = new QueryValues();
            HomeApiController homeObj = new HomeApiController();

            //CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {
                System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
            }



            e.Value = TBL3.ATTRIBUTE_NAME == "ITEM#" ? System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString() : TBL3.ATTRIBUTE_NAME == "SUBITEM#" ? System.Web.HttpContext.Current.Session["CustomerSubItemNo"].ToString() : TBL3.ATTRIBUTE_NAME;
        }

        public void CrossTab1_Column1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "ATTRIBUTE_NAME";
        }

        public void CrossTab1_Column1__GetCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {

            CSEntities _dbcontext = new CSEntities();
            QueryValues queryValues = new QueryValues();
            HomeApiController homeObj = new HomeApiController();

            //CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {
                System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
            }


            e.Value = TBL3.ATTRIBUTE_NAME == "ITEM#" ? System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString() : TBL3.ATTRIBUTE_NAME == "SUBITEM#" ? System.Web.HttpContext.Current.Session["CustomerSubItemNo"].ToString() : TBL3.ATTRIBUTE_NAME;
            //e.Value = TBL3.ATTRIBUTE_NAME;
        }

        public void CrossTab1_Sum1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            //e.Value = TBL3.ATTRIBUTE_NAME;
            e.Value = "0";
            //if (e.Value != null)
            //{
            //    e.Value = e.Value.Replace("&amp;", "&");
            //}
        }

        public void CrossTab1_Sum1__GetCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            try
            {

                FileInfo chkFile = null;
                string imageFullPath = "";
                try
                {
                    chkFile = new FileInfo(Attribute_Alteration("ProductDecimal"));
                }
                catch (Exception exception)
                {
                    if (!string.IsNullOrEmpty(Attribute_Alteration("ProductDecimal")))
                    {
                        e.Value = Convert_Text_to_Image(Attribute_Alteration("ProductDecimal"), "MS Sans Serif", 7);
                    }
                    else
                    {
                        e.Value = Attribute_Alteration("ProductDecimal");
                    }
                    return;
                }

                if (chkFile.Extension.ToUpper() == ".BMP" || chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".SVG" || chkFile.Extension.ToUpper() == ".JPEG" || chkFile.Extension.ToUpper() == ".GIF" || chkFile.Extension.ToUpper() == ".EPS" || chkFile.Extension.ToUpper() == ".PDF" || chkFile.Extension.ToUpper() == ".PSD" || chkFile.Extension.ToUpper() == ".PNG" || chkFile.Extension.ToUpper() == ".TIF" || chkFile.Extension.ToUpper() == ".RTF" || chkFile.Extension.ToUpper() == ".TIFF" || chkFile.Extension.ToUpper() == ".DOC")
                {
                    string file_name = FamilySpecs.FAMILY_ID.ToString() + "_" + FamilySpecs.ATTRIBUTE_ID.ToString() + "_" + chkFile.Name;
                    if (chkFile.Extension.ToLower().EndsWith(".tif") || chkFile.Extension.ToLower().EndsWith(".tiff") || chkFile.Extension.ToLower().EndsWith(".tga") ||
                                      chkFile.Extension.ToLower().EndsWith(".pcx") || chkFile.Extension.ToLower().EndsWith(".png") || chkFile.Extension.ToLower().EndsWith(".jpg") ||
                                      chkFile.Extension.ToLower().EndsWith(".bmp") || chkFile.Extension.ToLower().EndsWith(".gif") || chkFile.Extension.ToLower().EndsWith(".jpeg"))
                    {
                        if (Directory.Exists(imagemagickpath))
                        {

                            // string[] imgargs = { ImagePath + chkFile, content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };


                            string[] imgargs = { ImagePath + chkFile, content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg" };
                            //string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_Id).Select(a => a.ConverterType).SingleOrDefault();
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        //imageFullPath = content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                        imageFullPath = content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg";
                        imageFullPath = imageFullPath.Replace("&", "");
                    }
                    else if (chkFile.Extension.ToLower().EndsWith(".psd"))
                    {
                        if (Directory.Exists(imagemagickpath))
                        {
                            //string[] imgargs = { ImagePath + chkFile + "[0]", content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };
                            string[] imgargs = { ImagePath + chkFile + "[0]", content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        //imageFullPath = content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                        imageFullPath = content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg";
                        imageFullPath = imageFullPath.Replace("&", "");
                    }
                    else if (chkFile.Extension.ToLower().EndsWith(".eps"))
                    {
                        if (Directory.Exists(imagemagickpath))
                        {
                            //string[] imgargs = { ImagePath + chkFile, content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + "pr.png" };
                            string[] imgargs = { ImagePath + chkFile, content + "\\temp\\ImageandAttFile\\" + file_name + "pr.png" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        //imageFullPath = content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + "pr.png";
                        imageFullPath = content + "\\temp\\ImageandAttFile\\" + file_name + "pr.png";
                        imageFullPath = imageFullPath.Replace("&", "");
                    }
                    else
                    {
                        imageFullPath = ImagePath + Attribute_Alteration("ProductDecimal");
                    }
                    FileInfo filInfo = new FileInfo(imageFullPath);
                    if (filInfo.Exists)
                        e.Value = Image.FromFile(imageFullPath);
                    else
                    {
                        if (System.IO.Directory.Exists(imagemagickpath))
                        {
                            chkFile = new FileInfo(UnSupportedImagePath);
                            string[] imgargs = { chkFile.ToString(), content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        imageFullPath = content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                        imageFullPath = imageFullPath.Replace("&", "");
                        e.Value = Image.FromFile(imageFullPath);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Attribute_Alteration("ProductDecimal")))
                    {
                        e.Value = Convert_Text_to_Image(Attribute_Alteration("ProductDecimal"), "MS Sans Serif", 7);
                    }
                    else
                    {
                        e.Value = Attribute_Alteration("ProductDecimal");
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public void CrossTab1_Sum1_BeforePrint(object sender, System.EventArgs e)
        {
            AddAnchor(TBL1.CATEGORY_ID + TBL1.CATEGORY_NAME + TBL2.FAMILY_ID + TBL2.FAMILY_NAME + TBL3.PRODUCT_ID);
        }

        public void GroupHeaderBand6__GetValue(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = MultipleTableSoruce.FAMILY_ID;
        }

        public void Text31__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Multiple / Additional Tables : ";
        }

        public void Text15__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, MultipleTableSoruce.GROUP_NAME, true) + " : ";
        }

        public void CrossTab2_ColTotal1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Total";
        }

        public void CrossTab2_RowTotal1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Total";
        }

        public void CrossTab2_Row1_Title__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "PRODUCT_ID";
        }

        public void CrossTab2_LeftTitle__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "MultipleTables";
        }

        public void CrossTab2_RightTitle__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "ATTRIBUTE_NAME";
        }

        private void CrossTab2_Row1_Conditions(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            decimal value = 0;
            if (e.Value is decimal)
            {
                value = ((decimal)(e.Value));
            }
            string tag = ((StiComponent)sender).TagValue as string;
            string tooltip = ((StiComponent)sender).ToolTipValue as string;
            string hyperlink = ((StiComponent)sender).HyperlinkValue as string;
            if ((this.MultipleTables.PRODUCT_ID.ToString().ToLower() != this.ToString("").ToLower()))
            {
                ((Stimulsoft.Report.Components.IStiTextBrush)(sender)).TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Red);
                ((Stimulsoft.Report.Components.IStiBrush)(sender)).Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                Stimulsoft.Report.Components.StiConditionHelper.ApplyFont(sender, new System.Drawing.Font("Arial", 8F), Stimulsoft.Report.Components.StiConditionPermissions.All);
                ((Stimulsoft.Report.Components.IStiBorder)(sender)).Border = ((Stimulsoft.Base.Drawing.StiBorder)(((Stimulsoft.Report.Components.IStiBorder)(sender)).Border.Clone()));
                ((Stimulsoft.Report.Components.IStiBorder)(sender)).Border.Side = Stimulsoft.Base.Drawing.StiBorderSides.None;
                ((Stimulsoft.Report.Components.StiComponent)(sender)).Enabled = false;
                return;
            }
        }

        public void CrossTab2_Row1__GetDisplayCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = MultipleTables.PRODUCT_ID;
        }

        public void CrossTab2_Row1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "PRODUCT_ID";
        }

        public void CrossTab2_Row1__GetCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = MultipleTables.PRODUCT_ID;
        }

        public void CrossTab2_Column1__GetDisplayCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = MultipleTables.ATTRIBUTE_NAME;
        }

        public void CrossTab2_Column1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "ATTRIBUTE_NAME";
        }

        public void CrossTab2_Column1__GetCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = MultipleTables.ATTRIBUTE_NAME;
        }

        public void CrossTab2_Sum1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "0";
        }

        public void CrossTab2_Sum1__GetCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            try
            {
                FileInfo chkFile = null;
                string imageFullPath = "";
                try
                {
                    chkFile = new FileInfo(Attribute_Alteration("MultipleTable"));
                }
                catch (Exception exception)
                {
                    e.Value = Convert_Text_to_Image(Attribute_Alteration("MultipleTable"), "Bookman Old Style", 7);

                    return;
                }

                if (chkFile.Extension.ToUpper() == ".BMP" || chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".SVG" || chkFile.Extension.ToUpper() == ".GIF" || chkFile.Extension.ToUpper() == ".EPS" || chkFile.Extension.ToUpper() == ".PDF" || chkFile.Extension.ToUpper() == ".PSD" || chkFile.Extension.ToUpper() == ".PNG" || chkFile.Extension.ToUpper() == ".TIF" || chkFile.Extension.ToUpper() == ".RTF" || chkFile.Extension.ToUpper() == ".TIFF" || chkFile.Extension.ToUpper() == ".DOC")
                {
                    string file_name = MultipleTables.FAMILY_ID.ToString() + "_" + MultipleTables.PRODUCT_ID.ToString() + "_" + chkFile.Name;
                    if (chkFile.Extension.ToLower().EndsWith(".tif") ||
                                      chkFile.Extension.ToLower().EndsWith(".tiff") ||
                                      chkFile.Extension.ToLower().EndsWith(".tga") ||
                                      chkFile.Extension.ToLower().EndsWith(".pcx") ||
                                      chkFile.Extension.ToLower().EndsWith(".png") ||
                                      chkFile.Extension.ToLower().EndsWith(".jpg") ||
                                      chkFile.Extension.ToLower().EndsWith(".bmp") ||
                                      chkFile.Extension.ToLower().EndsWith(".gif"))
                    {
                        if (System.IO.Directory.Exists(imagemagickpath))
                        {
                            // string[] imgargs = { ImagePath + chkFile.ToString(), content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };
                            string[] imgargs = { ImagePath + chkFile.ToString(), content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        //imageFullPath = content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                        imageFullPath = content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg";
                        //imageFullPath = content + "\\temp\\ImageandAttFile" + file_name + ".jpg";                        
                        imageFullPath = imageFullPath.Replace("&", "");
                    }
                    else if (chkFile.Extension.ToLower().EndsWith(".psd"))
                    {
                        if (Directory.Exists(imagemagickpath))
                        {
                            //string[] imgargs = { ImagePath + chkFile.ToString() + "[0]", content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };
                            string[] imgargs = { ImagePath + chkFile.ToString() + "[0]", content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        //imageFullPath = content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                        imageFullPath = content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg";
                        imageFullPath = imageFullPath.Replace("&", "");
                    }
                    else if (chkFile.Extension.ToLower().EndsWith(".eps"))
                    {
                        if (Directory.Exists(imagemagickpath))
                        {
                            //string[] imgargs = { ImagePath + chkFile.ToString(), content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + "pr.png" };
                            string[] imgargs = { ImagePath + chkFile.ToString(), content + "\\temp\\ImageandAttFile\\" + file_name + "pr.png" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        //imageFullPath = content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + "pr.png";
                        imageFullPath = content + "\\temp\\ImageandAttFile\\" + file_name + "pr.png";
                        imageFullPath = imageFullPath.Replace("&", "");
                    }
                    else
                    {
                        imageFullPath = ImagePath + Attribute_Alteration("MultipleTable");
                    }
                    FileInfo filInfo = new FileInfo(imageFullPath);
                    if (filInfo.Exists)
                        e.Value = Image.FromFile(imageFullPath);
                    else
                    {
                        if (System.IO.Directory.Exists(imagemagickpath))
                        {
                            chkFile = new FileInfo(UnSupportedImagePath);
                            string[] imgargs = { chkFile.ToString(), content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        imageFullPath = content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                        imageFullPath = imageFullPath.Replace("&", "");
                        e.Value = Image.FromFile(imageFullPath);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Attribute_Alteration("MultipleTable")))
                    {
                        e.Value = Convert_Text_to_Image(Attribute_Alteration("MultipleTable"), "Bookman Old Style", 7);
                    }
                    else
                    {
                        e.Value = Attribute_Alteration("MultipleTable");
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public void GroupHeaderBand7__GetValue(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = ReferenceTableSource.FAMILY_ID;
        }

        public void Text32__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Reference Tables : ";
        }

        public void Text16__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, ReferenceTableSource.TABLE_NAME, true);
        }

        public void CrossTab3_ColTotal1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Total";
        }

        public void CrossTab3_RowTotal1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Total";
        }

        public void CrossTab3_Row1_Title__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "ROW_INDEX";
        }

        public void CrossTab3_LeftTitle__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "ReferenceTables";
        }

        public void CrossTab3_RightTitle__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "COLUMN_INDEX";
        }

        private void CrossTab3_Row1_Conditions(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            decimal value = 0;
            if (e.Value is decimal)
            {
                value = ((decimal)(e.Value));
            }
            string tag = ((StiComponent)sender).TagValue as string;
            string tooltip = ((StiComponent)sender).ToolTipValue as string;
            string hyperlink = ((StiComponent)sender).HyperlinkValue as string;
            if ((this.ReferenceTables.TABLE_ID.ToString().ToLower() != this.ToString("").ToLower()))
            {
                ((Stimulsoft.Report.Components.IStiTextBrush)(sender)).TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Red);
                ((Stimulsoft.Report.Components.IStiBrush)(sender)).Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                Stimulsoft.Report.Components.StiConditionHelper.ApplyFont(sender, new System.Drawing.Font("Arial", 8F), Stimulsoft.Report.Components.StiConditionPermissions.All);
                ((Stimulsoft.Report.Components.IStiBorder)(sender)).Border = ((Stimulsoft.Base.Drawing.StiBorder)(((Stimulsoft.Report.Components.IStiBorder)(sender)).Border.Clone()));
                ((Stimulsoft.Report.Components.IStiBorder)(sender)).Border.Side = Stimulsoft.Base.Drawing.StiBorderSides.None;
                ((Stimulsoft.Report.Components.StiComponent)(sender)).Enabled = false;
                return;
            }
        }

        public void CrossTab3_Row1__GetDisplayCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = ReferenceTables.ROW_INDEX;
        }

        public void CrossTab3_Row1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "ROW_INDEX";
        }

        public void CrossTab3_Row1__GetCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = ReferenceTables.ROW_INDEX;
        }

        private void CrossTab3_Column1_Conditions(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            decimal value = 0;
            if (e.Value is decimal)
            {
                value = ((decimal)(e.Value));
            }
            string tag = ((StiComponent)sender).TagValue as string;
            string tooltip = ((StiComponent)sender).ToolTipValue as string;
            string hyperlink = ((StiComponent)sender).HyperlinkValue as string;
            if ((this.ReferenceTables.TABLE_ID.ToString().ToLower() != this.ToString("").ToLower()))
            {
                ((Stimulsoft.Report.Components.IStiTextBrush)(sender)).TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Red);
                ((Stimulsoft.Report.Components.IStiBrush)(sender)).Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                Stimulsoft.Report.Components.StiConditionHelper.ApplyFont(sender, new System.Drawing.Font("Arial", 8F), Stimulsoft.Report.Components.StiConditionPermissions.All);
                ((Stimulsoft.Report.Components.IStiBorder)(sender)).Border = ((Stimulsoft.Base.Drawing.StiBorder)(((Stimulsoft.Report.Components.IStiBorder)(sender)).Border.Clone()));
                ((Stimulsoft.Report.Components.IStiBorder)(sender)).Border.Side = Stimulsoft.Base.Drawing.StiBorderSides.None;
                ((Stimulsoft.Report.Components.StiComponent)(sender)).Enabled = false;
                return;
            }
        }

        public void CrossTab3_Column1__GetDisplayCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = ReferenceTables.COLUMN_INDEX;
        }

        public void CrossTab3_Column1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "COLUMN_INDEX";
        }

        public void CrossTab3_Column1__GetCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            e.Value = ReferenceTables.COLUMN_INDEX;
        }

        public void CrossTab3_Sum1__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "0";
        }

        public void CrossTab3_Sum1__GetCrossValue(object sender, Stimulsoft.Report.CrossTab.StiGetCrossValueEventArgs e)
        {
            try
            {
                FileInfo chkFile = null;
                string imageFullPath = "";
                try
                {
                    chkFile = new FileInfo(ReferenceTables.ROW_VALUE);
                }
                catch (Exception exception)
                {
                    e.Value = ReferenceTables.ROW_VALUE;
                    return;
                }

                if (chkFile.Extension.ToUpper() == ".BMP" || chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".SVG" || chkFile.Extension.ToUpper() == ".GIF" || chkFile.Extension.ToUpper() == ".EPS" || chkFile.Extension.ToUpper() == ".PDF" || chkFile.Extension.ToUpper() == ".PSD" || chkFile.Extension.ToUpper() == ".PNG" || chkFile.Extension.ToUpper() == ".TIF" || chkFile.Extension.ToUpper() == ".RTF" || chkFile.Extension.ToUpper() == ".TIFF" || chkFile.Extension.ToUpper() == ".DOC")
                {
                    string oldFileName = Path.GetFileNameWithoutExtension(chkFile.ToString());
                    string file_name = ReferenceTables.FAMILY_ID.ToString() + "_" + ReferenceTables.TABLE_ID.ToString() + "_" + oldFileName;
                    if (chkFile.Extension.ToLower().EndsWith(".tif") ||
                                      chkFile.Extension.ToLower().EndsWith(".tiff") ||
                                      chkFile.Extension.ToLower().EndsWith(".tga") ||
                                      chkFile.Extension.ToLower().EndsWith(".pcx") ||
                                      chkFile.Extension.ToLower().EndsWith(".png") ||
                                      chkFile.Extension.ToLower().EndsWith(".jpg") ||
                                      chkFile.Extension.ToLower().EndsWith(".bmp") ||
                                      chkFile.Extension.ToLower().EndsWith(".gif"))
                    {
                        if (System.IO.Directory.Exists(imagemagickpath))
                        {

                            string[] imgargs = { ImagePath + chkFile.ToString(), content + "\\temp\\ImageandAttFile\\" + file_name.Replace(".jpg", "") + "pr.jpg" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        imageFullPath = content + "\\temp\\ImageandAttFile\\" + file_name + "pr.jpg";
                        imageFullPath = imageFullPath.Replace("&", "");
                    }
                    else if (chkFile.Extension.ToLower().EndsWith(".psd"))
                    {
                        if (Directory.Exists(imagemagickpath))
                        {
                            string[] imgargs = { ImagePath + chkFile.ToString() + "[0]", content + "\\temp\\ImageandAttFile\\" + file_name + "pr.jpg" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        imageFullPath = content + "\\temp\\ImageandAttFile\\" + file_name + "pr.jpg";
                        imageFullPath = imageFullPath.Replace("&", "");
                    }
                    else if (chkFile.Extension.ToLower().EndsWith(".eps"))
                    {
                        if (Directory.Exists(imagemagickpath))
                        {
                            string[] imgargs = { ImagePath + chkFile.ToString(), content + "\\temp\\ImageandAttFile\\" + file_name + "pr.png" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        imageFullPath = content + "\\temp\\ImageandAttFile\\" + file_name + "pr.png";
                        imageFullPath = imageFullPath.Replace("&", "");
                    }
                    else
                    {
                        imageFullPath = ImagePath + ReferenceTables.ROW_VALUE;
                    }
                    FileInfo filInfo = new FileInfo(imageFullPath);
                    if (filInfo.Exists)
                        e.Value = Image.FromFile(imageFullPath);
                    else
                    {
                        if (System.IO.Directory.Exists(imagemagickpath))
                        {
                            chkFile = new FileInfo(UnSupportedImagePath);
                            string[] imgargs = { chkFile.ToString(), content + "\\temp\\ImageandAttFile\\" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + "pr.jpg" };
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");

                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 50x20");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    e.Value = Image.FromFile(imgargs[1].Replace("&", ""));
                                    return;
                                    //File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        imageFullPath = content + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + "pr.jpg";
                        imageFullPath = imageFullPath.Replace("&", "");
                        e.Value = Image.FromFile(imageFullPath);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ReferenceTables.ROW_VALUE))
                    {
                        e.Value = Convert_Text_to_Image(ReferenceTables.ROW_VALUE, "Bookman Old Style", 7);
                    }
                    else
                    {
                        e.Value = ReferenceTables.ROW_VALUE;
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public void CrossTab3_Sum1_BeforePrint(object sender, System.EventArgs e)
        {
            CrossTab3_Sum1.Summary = Stimulsoft.Report.CrossTab.Core.StiSummaryType.Image;
        }

        public void Main_BeforePrint(object sender, System.EventArgs e)
        {
            Main.Height = 11.69; ;
        }

        public void Text11__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "Index";
        }

        public void GroupHeaderBand4__GetValue(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = TBL1.CATEGORY_ID;
        }

        public void GroupHeaderBand5__GetValue(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = TBL2.FAMILY_ID;
        }

        public void GroupHeaderBand3__GetValue(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = TBL3.PRODUCT_ID;
        }

        public void Text13__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = ToString(sender, TBL3.STRING_VALUE, true);
        }

        public void Text14__GetTag(object sender, Stimulsoft.Report.Events.StiValueEventArgs e)
        {
            e.Value = ToString(sender, TBL1.CATEGORY_ID, false) + ToString(sender, TBL1.CATEGORY_NAME, false) + ToString(sender, TBL2.FAMILY_ID, false) + ToString(sender, TBL2.FAMILY_NAME, false) + ToString(sender, TBL3.PRODUCT_ID, false);
        }

        public void Text14__GetValue(object sender, Stimulsoft.Report.Events.StiGetValueEventArgs e)
        {
            e.Value = "#%#{GetAnchorPageNumber(sender.TagValue)}";
            e.StoreToPrinted = true;
        }

        public System.String Text14_GetValue_End(Stimulsoft.Report.Components.StiComponent sender)
        {
            return ToString(sender, GetAnchorPageNumber(sender.TagValue), true);
        }

        public void DataBand6__GetFilter(object sender, Stimulsoft.Report.Events.StiFilterEventArgs e)
        {
            e.Value = (this.TBL3.ATTRIBUTE_ID.ToString().ToLower() == this.ToString("1").ToLower());
        }

        public void ReportWordsToEnd__EndRender(object sender, System.EventArgs e)
        {
            this.Text6.SetText(new Stimulsoft.Report.Components.StiGetValue(this.Text6_GetValue_End));
            this.Text7.SetText(new Stimulsoft.Report.Components.StiGetValue(this.Text7_GetValue_End));
            this.Text10.SetText(new Stimulsoft.Report.Components.StiGetValue(this.Text10_GetValue_End));
            this.Text14.SetText(new Stimulsoft.Report.Components.StiGetValue(this.Text14_GetValue_End));
        }

        private void InitializeComponent()
        {
            this.TBL4 = new TBL4DataSource();
            this.FamilySpecs = new FamilySpecsDataSource();
            this.CategorySpecs = new CategorySpecsDataSource();
            this.ReferenceTableSource = new ReferenceTableSourceDataSource();
            this.ReferenceTables = new ReferenceTablesDataSource();
            this.MultipleTableSoruce = new MultipleTableSoruceDataSource();
            this.MultipleTables = new MultipleTablesDataSource();
            this.TBL3 = new TBL3DataSource();
            this.TBL2 = new TBL2DataSource();
            this.TBL1 = new TBL1DataSource();
            this.ParentName11 = new Stimulsoft.Report.Dictionary.StiDataRelation("Relation8", "Name1", "Name1", this.TBL4, this.TBL3, new System.String[] {
                        "ATTRIBUTE_ID"}, new System.String[] {
                        "ATTRIBUTE_ID"});
            this.ParentName6 = new Stimulsoft.Report.Dictionary.StiDataRelation("Relation7", "Name", "Name", this.TBL2, this.FamilySpecs, new System.String[] {
                        "FAMILY_ID",
                        "CATEGORY_ID"}, new System.String[] {
                        "FAMILY_ID",
                        "CATEGORY_ID"});
            this.ParentName5 = new Stimulsoft.Report.Dictionary.StiDataRelation("Relation6", "Name", "Name", this.TBL2, this.ReferenceTableSource, new System.String[] {
                        "FAMILY_ID"}, new System.String[] {
                        "FAMILY_ID"});
            this.ParentName4 = new Stimulsoft.Report.Dictionary.StiDataRelation("Relation5", "Name", "Name", this.ReferenceTableSource, this.ReferenceTables, new System.String[] {
                        "TABLE_ID"}, new System.String[] {
                        "TABLE_ID"});
            this.ParentName3 = new Stimulsoft.Report.Dictionary.StiDataRelation("Relation3", "Name", "Name", this.MultipleTableSoruce, this.MultipleTables, new System.String[] {
                        "GROUP_ID"}, new System.String[] {
                        "GROUP_ID"});
            this.ParentName2 = new Stimulsoft.Report.Dictionary.StiDataRelation("Relation4", "Name", "Name", this.TBL2, this.MultipleTableSoruce, new System.String[] {
                        "FAMILY_ID"}, new System.String[] {
                        "FAMILY_ID"});
            this.ParentName1 = new Stimulsoft.Report.Dictionary.StiDataRelation("Relation2", "Name", "Name", this.TBL2, this.TBL3, new System.String[] {
                        "FAMILY_ID"}, new System.String[] {
                        "FAMILY_ID"});
            this.ParentName = new Stimulsoft.Report.Dictionary.StiDataRelation("Relation", "Name", "Name", this.TBL1, this.TBL2, new System.String[] {
                        "CATEGORY_ID"}, new System.String[] {
                        "CATEGORY_ID"});
            this.Dictionary.Variables.Add(new Stimulsoft.Report.Dictionary.StiVariable("", "ImagePath", "ImagePath", "", typeof(string), "D:", false, Stimulsoft.Report.Dictionary.StiVariableInitBy.Value, false));
            this.Dictionary.Variables.Add(new Stimulsoft.Report.Dictionary.StiVariable("", "UnSupportedImagePath", "UnSupportedImagePath", "", typeof(string), "", false, Stimulsoft.Report.Dictionary.StiVariableInitBy.Value, false));
            this.Dictionary.Variables.Add(new Stimulsoft.Report.Dictionary.StiVariable("", "CatalogId", "CatalogId", "", typeof(int), "10", false, Stimulsoft.Report.Dictionary.StiVariableInitBy.Value, false));
            this.Dictionary.Variables.Add(new Stimulsoft.Report.Dictionary.StiVariable("", "CategoryId", "CategoryId", "", typeof(string), "07102013-[C1]-[C2]", false, Stimulsoft.Report.Dictionary.StiVariableInitBy.Value, false));
            this.NeedsCompiling = false;
            // 
            // Variables init
            // 
            this.ImagePath = "D:";
            this.UnSupportedImagePath = "";
            this.CatalogId = 10;
            this.CategoryId = "07102013-[C1]-[C2]";
            this.EngineVersion = Stimulsoft.Report.Engine.StiEngineVersion.EngineV2;
            this.ReferencedAssemblies = new System.String[] {
                    "System.Dll",
                    "System.Drawing.Dll",
                    "System.Windows.Forms.Dll",
                    "System.Data.Dll",
                    "System.Xml.Dll",
                    "Stimulsoft.Controls.Dll",
                    "Stimulsoft.Base.Dll",
                    "Stimulsoft.Report.Dll"};
            this.ReportAlias = "Report";
            // 
            // ReportChanged
            // 
            this.ReportChanged = new DateTime(2014, 7, 23, 14, 36, 43, 801);
            // 
            // ReportCreated
            // 
            this.ReportCreated = new DateTime(2013, 12, 23, 16, 39, 20, 0);
            this.ReportFile = "D:\\Vijay\\cATEGORYpREVIEW.mrt";
            this.ReportGuid = "964b4489781d436398d2df81b14df026";
            this.ReportName = "Report";
            this.ReportUnit = Stimulsoft.Report.StiReportUnitType.Inches;
            this.ReportVersion = "2013.1.1600";
            this.ScriptLanguage = Stimulsoft.Report.StiReportLanguageType.CSharp;
            // 
            // Cover
            // 
            this.Cover = new Stimulsoft.Report.Components.StiPage();
            this.Cover.Guid = "0b5fdca0f367467fa9baf8b1c4d03601";
            this.Cover.Name = "Cover";
            this.Cover.PageHeight = 11.69;
            this.Cover.PageWidth = 8.27;
            this.Cover.PaperSize = System.Drawing.Printing.PaperKind.Letter;
            this.Cover.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 2, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Cover.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // Text2
            // 
            this.Text2 = new Stimulsoft.Report.Components.StiText();
            this.Text2.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 1, 7.5, 0.2);
            this.Text2.Guid = "2a9fa596a0c64106abb126d739b48b64";
            this.Text2.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Center;
            this.Text2.Name = "Text2";
            this.Text2.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text2__GetValue);
            this.Text2.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text2.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text2.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.Text2.Indicator = null;
            this.Text2.Interaction = null;
            this.Text2.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text2.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text2.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.Text2.BeforePrint += new System.EventHandler(this.Text2_BeforePrint);
            this.Cover.ExcelSheetValue = null;
            this.Cover.Interaction = null;
            this.Cover.Margins = new Stimulsoft.Report.Components.StiMargins(0.39, 0.39, 0.39, 0.39);
            this.Cover_Watermark = new Stimulsoft.Report.Components.StiWatermark();
            this.Cover_Watermark.Font = new System.Drawing.Font("Arial", 100F);
            this.Cover_Watermark.Image = null;
            this.Cover_Watermark.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(50, 0, 0, 0));
            // 
            // TOC
            // 
            this.TOC = new Stimulsoft.Report.Components.StiPage();
            this.TOC.Guid = "8d3785873fb74ab4ac34aa1bc2c10a38";
            this.TOC.Name = "TOC";
            this.TOC.PageHeight = 11.69;
            this.TOC.PageWidth = 8.27;
            this.TOC.PaperSize = System.Drawing.Printing.PaperKind.Letter;
            this.TOC.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 2, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.TOC.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // PageHeaderBand1
            // 
            this.PageHeaderBand1 = new Stimulsoft.Report.Components.StiPageHeaderBand();
            this.PageHeaderBand1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.2, 7.49, 0.6);
            this.PageHeaderBand1.Name = "PageHeaderBand1";
            this.PageHeaderBand1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.PageHeaderBand1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // Text8
            // 
            this.Text8 = new Stimulsoft.Report.Components.StiText();
            this.Text8.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.1, 7.5, 0.3);
            this.Text8.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Center;
            this.Text8.Name = "Text8";
            this.Text8.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text8__GetValue);
            this.Text8.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text8.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text8.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text8.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold);
            this.Text8.Guid = null;
            this.Text8.Indicator = null;
            this.Text8.Interaction = null;
            this.Text8.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text8.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text8.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.PageHeaderBand1.Guid = null;
            this.PageHeaderBand1.Interaction = null;
            // 
            // GroupHeaderBand1
            // 
            this.GroupHeaderBand1 = new Stimulsoft.Report.Components.StiGroupHeaderBand();
            this.GroupHeaderBand1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 1.2, 7.49, 0.3);
            this.GroupHeaderBand1.GetValue += new Stimulsoft.Report.Events.StiValueEventHandler(this.GroupHeaderBand1__GetValue);
            this.GroupHeaderBand1.Name = "GroupHeaderBand1";
            this.GroupHeaderBand1.SortDirection = Stimulsoft.Report.Components.StiGroupSortDirection.None;
            this.GroupHeaderBand1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.GroupHeaderBand1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // Text4
            // 
            this.Text4 = new Stimulsoft.Report.Components.StiText();
            this.Text4.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, 6.9, 0.3);
            this.Text4.Guid = "c0f42a8ab2a74226b3ace98ebd684ac5";
            this.Text4.GetHyperlink += new Stimulsoft.Report.Events.StiValueEventHandler(this.Text4__GetHyperlink);
            this.Text4.Name = "Text4";
            this.Text4.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text4__GetValue);
            this.Text4.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text4.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text4.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text4.Font = new System.Drawing.Font("Arial", 16F);
            this.Text4.Indicator = null;
            this.Text4.Interaction = null;
            this.Text4.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text4.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text4.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // Text6
            // 
            this.Text6 = new Stimulsoft.Report.Components.StiText();
            this.Text6.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(7.1, 0, 0.3, 0.3);
            this.Text6.GetHyperlink += new Stimulsoft.Report.Events.StiValueEventHandler(this.Text6__GetHyperlink);
            this.Text6.Name = "Text6";
            this.Text6.GetTag += new Stimulsoft.Report.Events.StiValueEventHandler(this.Text6__GetTag);
            this.Text6.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text6__GetValue);
            this.Text6.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text6.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text6.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text6.Font = new System.Drawing.Font("Arial", 8F);
            this.Text6.Guid = null;
            this.Text6.Indicator = null;
            this.Text6.Interaction = null;
            this.Text6.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text6.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text6.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.GroupHeaderBand1.Guid = null;
            this.GroupHeaderBand1.Interaction = null;
            // 
            // DataBand4
            // 
            this.DataBand4 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand4.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 1.9, 7.49, 0.1);
            this.DataBand4.DataSourceName = "TBL1";
            this.DataBand4.Name = "DataBand4";
            this.DataBand4.Sort = new System.String[0];
            this.DataBand4.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand4.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand4.BusinessObjectGuid = null;
            this.DataBand4.DataRelationName = null;
            this.DataBand4.Guid = null;
            this.DataBand4.Interaction = null;
            this.DataBand4.MasterComponent = null;
            // 
            // GroupHeaderBand2
            // 
            this.GroupHeaderBand2 = new Stimulsoft.Report.Components.StiGroupHeaderBand();
            this.GroupHeaderBand2.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 2.4, 7.49, 0.3);
            this.GroupHeaderBand2.GetValue += new Stimulsoft.Report.Events.StiValueEventHandler(this.GroupHeaderBand2__GetValue);
            this.GroupHeaderBand2.Name = "GroupHeaderBand2";
            this.GroupHeaderBand2.SortDirection = Stimulsoft.Report.Components.StiGroupSortDirection.None;
            this.GroupHeaderBand2.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.GroupHeaderBand2.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // Text5
            // 
            this.Text5 = new Stimulsoft.Report.Components.StiText();
            this.Text5.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.2, 0, 6.7, 0.2);
            this.Text5.Guid = "20a544dee1b0448198e42a7f55396410";
            this.Text5.GetHyperlink += new Stimulsoft.Report.Events.StiValueEventHandler(this.Text5__GetHyperlink);
            this.Text5.Name = "Text5";
            this.Text5.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text5__GetValue);
            this.Text5.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text5.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text5.Font = new System.Drawing.Font("Arial", 12F);
            this.Text5.Indicator = null;
            this.Text5.Interaction = null;
            this.Text5.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text5.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Blue);
            this.Text5.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // Text7
            // 
            this.Text7 = new Stimulsoft.Report.Components.StiText();
            this.Text7.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(7.1, 0, 0.3, 0.3);
            this.Text7.Guid = "a12ae8f02bfd452298c0352aa3fdcbb5";
            this.Text7.GetHyperlink += new Stimulsoft.Report.Events.StiValueEventHandler(this.Text7__GetHyperlink);
            this.Text7.Name = "Text7";
            this.Text7.GetTag += new Stimulsoft.Report.Events.StiValueEventHandler(this.Text7__GetTag);
            this.Text7.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text7__GetValue);
            this.Text7.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text7.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text7.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text7.Font = new System.Drawing.Font("Arial", 8F);
            this.Text7.Indicator = null;
            this.Text7.Interaction = null;
            this.Text7.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text7.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text7.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.GroupHeaderBand2.Guid = null;
            this.GroupHeaderBand2.Interaction = null;
            // 
            // DataBand5
            // 
            this.DataBand5 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand5.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 3.1, 7.49, 0.1);
            this.DataBand5.DataRelationName = "Relation";
            this.DataBand5.DataSourceName = "TBL2";
            this.DataBand5.Name = "DataBand5";
            this.DataBand5.Sort = new System.String[0];
            this.DataBand5.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand5.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand5.BusinessObjectGuid = null;
            this.DataBand5.Guid = null;
            this.DataBand5.Interaction = null;
            this.TOC.ExcelSheetValue = null;
            this.TOC.Interaction = null;
            this.TOC.Margins = new Stimulsoft.Report.Components.StiMargins(0.39, 0.39, 0.39, 0.39);
            this.TOC_Watermark = new Stimulsoft.Report.Components.StiWatermark();
            this.TOC_Watermark.Font = new System.Drawing.Font("Arial", 100F);
            this.TOC_Watermark.Image = null;
            this.TOC_Watermark.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(50, 0, 0, 0));
            // 
            // Main
            // 
            this.Main = new Stimulsoft.Report.Components.StiPage();
            this.Main.Guid = "d3b95dfb34bc4da18301916c56c219b6";
            this.Main.Name = "Main";
            this.Main.PageHeight = 19;
            this.Main.PageWidth = 8.27;
            this.Main.PaperSize = System.Drawing.Printing.PaperKind.Letter;
            this.Main.RightToLeft = true;
            this.Main.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 2, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Main.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // PageFooterBand1
            // 
            this.PageFooterBand1 = new Stimulsoft.Report.Components.StiPageFooterBand();
            this.PageFooterBand1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 17.82, 7.49, 0.4);
            this.PageFooterBand1.Name = "PageFooterBand1";
            this.PageFooterBand1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.PageFooterBand1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // Text9
            // 
            this.Text9 = new Stimulsoft.Report.Components.StiText();
            this.Text9.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.18, 1.7, 0.2);
            this.Text9.Name = "Text9";
            this.Text9.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text9__GetValue);
            this.Text9.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text9.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text9.Font = new System.Drawing.Font("Arial", 8F);
            this.Text9.Guid = null;
            this.Text9.Indicator = null;
            this.Text9.Interaction = null;
            this.Text9.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text9.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text9.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // Text10
            // 
            this.Text10 = new Stimulsoft.Report.Components.StiText();
            this.Text10.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(6.7, 0.18, 0.7, 0.2);
            this.Text10.Name = "Text10";
            this.Text10.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text10__GetValue);
            this.Text10.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text10.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text10.Font = new System.Drawing.Font("Arial", 8F);
            this.Text10.Guid = null;
            this.Text10.Indicator = null;
            this.Text10.Interaction = null;
            this.Text10.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text10.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text10.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // Text12
            // 
            this.Text12 = new Stimulsoft.Report.Components.StiText();
            this.Text12.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(5, 0.18, 1.4, 0.2);
            this.Text12.Name = "Text12";
            this.Text12.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text12__GetValue);
            this.Text12.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text12.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text12.Font = new System.Drawing.Font("Arial", 8F);
            this.Text12.Guid = null;
            this.Text12.Indicator = null;
            this.Text12.Interaction = null;
            this.Text12.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text12.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text12.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // HorizontalLinePrimitive1
            // 
            this.HorizontalLinePrimitive1 = new Stimulsoft.Report.Components.StiHorizontalLinePrimitive();
            this.HorizontalLinePrimitive1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.08, 7.5, 0.01);
            this.HorizontalLinePrimitive1.Color = System.Drawing.Color.Black;
            this.HorizontalLinePrimitive1.Name = "HorizontalLinePrimitive1";
            this.HorizontalLinePrimitive1.EndCap = new Stimulsoft.Base.Drawing.StiCap(10, Stimulsoft.Base.Drawing.StiCapStyle.None, 10, true, System.Drawing.Color.Black);
            this.HorizontalLinePrimitive1.Guid = null;
            this.HorizontalLinePrimitive1.Interaction = null;
            this.HorizontalLinePrimitive1.StartCap = new Stimulsoft.Base.Drawing.StiCap(10, Stimulsoft.Base.Drawing.StiCapStyle.None, 10, true, System.Drawing.Color.Black);
            this.PageFooterBand1.Guid = null;
            this.PageFooterBand1.Interaction = null;
            // 
            // DataBand1
            // 
            this.DataBand1 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.2, 0.5, 0.3);
            this.DataBand1.DataSourceName = "TBL1";
            this.DataBand1.Name = "DataBand1";
            this.DataBand1.PrintIfDetailEmpty = true;
            this.DataBand1.Sort = new System.String[0];
            this.DataBand1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand1.BusinessObjectGuid = null;
            // 
            // Text1
            // 
            this.Text1 = new Stimulsoft.Report.Components.StiText();
            this.Text1.GetBookmark += new Stimulsoft.Report.Events.StiValueEventHandler(this.Text1__GetBookmark);
            this.Text1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.1, 7.5, 0.3);
            this.Text1.Name = "Text1";
            this.Text1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text1__GetValue);
            this.Text1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Coral);
            this.Text1.Font = new System.Drawing.Font("Arial", 16F);
            this.Text1.Guid = null;
            this.Text1.Indicator = null;
            this.Text1.Interaction = null;
            this.Text1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.White);
            this.Text1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.Text1.BeforePrint += new System.EventHandler(this.Text1_BeforePrint);
            // 
            // Image1
            // 
            this.Image1 = new Stimulsoft.Report.Components.StiImage();
            this.Image1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.4, 0.7, 1.03, 0.69);
            this.Image1.Guid = "4e1e0ef89f7c4049a6700c3c4e958765";
            this.Image1.Name = "Image1";
            this.Image1.Stretch = true;
            this.Image1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Image1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Image1.Guid = null;
            this.Image1.Image = null;
            this.Image1.Interaction = null;
            this.Image1.BeforePrint += new System.EventHandler(this.Image1_BeforePrint);
            // 
            // Image2
            // 
            //this.Image2 = new Stimulsoft.Report.Components.StiImage();
            //this.Image2.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(2.2, 3.5, 1.03, 1.09);
            //this.Image2.Guid = "356f8cf02817481cad8c9e5437024bfe";
            //this.Image2.Name = "Image2";
            //this.Image2.Stretch = true;
            //this.Image2.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Image2.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Image2.Image = null;
            //this.Image2.Interaction = null;
            //this.Image2.BeforePrint += new System.EventHandler(this.Image2_BeforePrint);
            //// 
            //// Text17
            //// 
            this.Text17 = new Stimulsoft.Report.Components.StiText();
            this.Text17.CanGrow = true;
            this.Text17.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.3, 0.8, 7.1, 0.2);
            this.Text17.Name = "Text17";
            this.Text17.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text17__GetValue);
            this.Text17.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text17.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text17.Font = new System.Drawing.Font("Arial", 8F);
            this.Text17.Guid = null;
            this.Text17.Indicator = null;
            this.Text17.Interaction = null;
            this.Text17.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text17.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text17.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.Text17.AllowHtmlTags = true;
            //// 
            //// Text18
            //// 
            this.Text18 = new Stimulsoft.Report.Components.StiText();
            this.Text18.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 0.5, 1.9, 0.2);
            this.Text18.Name = "Text18";
            this.Text18.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text18__GetValue);
            this.Text18.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text18.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text18.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text18.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.Text18.Guid = null;
            this.Text18.Indicator = null;
            this.Text18.Interaction = null;
            this.Text18.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text18.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text18.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.Text18.AllowHtmlTags = true;
            //// 
            //// 
            //// Text19
            //// 
            this.Text19 = new Stimulsoft.Report.Components.StiText();
            this.Text19.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 0.5, 1.9, 0.2);
            this.Text19.Name = "Text19";
            this.Text19.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text19__GetValue);
            this.Text19.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text19.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text19.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text19.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.Text19.Guid = null;
            this.Text19.Indicator = null;
            this.Text19.Interaction = null;
            this.Text19.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text19.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text19.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.Text19.AllowHtmlTags = true;
            //// 
            //// Text19
            //// 
            //this.Text19 = new Stimulsoft.Report.Components.StiText();
            //this.Text19.CanGrow = true;
            //this.Text19.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.3, 1.4, 7.1, 0.2);
            //this.Text19.Guid = "83501ee586734d1c9b34655ae758aaf8";
            //this.Text19.Name = "Text19";
            //this.Text19.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text19__GetValue);
            //this.Text19.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text19.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text19.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text19.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text19.Indicator = null;
            //this.Text19.Interaction = null;
            //this.Text19.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text19.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text19.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //// 
            //// Text20
            //// 
            //this.Text20 = new Stimulsoft.Report.Components.StiText();
            //this.Text20.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 1.1, 1.9, 0.2);
            //this.Text20.Guid = "1c0759ab5c364272b676afcd8157e80c";
            //this.Text20.Name = "Text20";
            //this.Text20.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text20__GetValue);
            //this.Text20.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            //this.Text20.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text20.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text20.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            //this.Text20.Indicator = null;
            //this.Text20.Interaction = null;
            //this.Text20.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text20.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text20.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //// 
            //// Text21
            //// 
            //this.Text21 = new Stimulsoft.Report.Components.StiText();
            //this.Text21.CanGrow = true;
            //this.Text21.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.3, 1.7, 7.1, 0.2);
            //this.Text21.Guid = "9b7c7ee8a4c84c8aac28f34a2b86e2d0";
            //this.Text21.Name = "Text21";
            //this.Text21.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text21__GetValue);
            //this.Text21.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text21.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text21.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text21.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text21.Indicator = null;
            //this.Text21.Interaction = null;
            //this.Text21.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text21.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text21.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //// 
            //// Text22
            //// 
            //this.Text22 = new Stimulsoft.Report.Components.StiText();
            //this.Text22.CanGrow = true;
            //this.Text22.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.3, 2, 7.1, 0.2);
            //this.Text22.Guid = "8ae765427fd14e56ac408046690da581";
            //this.Text22.Name = "Text22";
            //this.Text22.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text22__GetValue);
            //this.Text22.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text22.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text22.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text22.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text22.Indicator = null;
            //this.Text22.Interaction = null;
            //this.Text22.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text22.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text22.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //// 
            //// Text23
            //// 
            //this.Text23 = new Stimulsoft.Report.Components.StiText();
            //this.Text23.CanGrow = true;
            //this.Text23.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.3, 2.6, 7.1, 0.2);
            //this.Text23.Guid = "100dc773df804edf9680e83e95d3a1be";
            //this.Text23.Name = "Text23";
            //this.Text23.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text23__GetValue);
            //this.Text23.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text23.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text23.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text23.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text23.Indicator = null;
            //this.Text23.Interaction = null;
            //this.Text23.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text23.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text23.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //// 
            //// Text24
            //// 
            //this.Text24 = new Stimulsoft.Report.Components.StiText();
            //this.Text24.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 2.3, 1.9, 0.2);
            //this.Text24.Guid = "8610fe4811dd405699a1848d6dd7a9c4";
            //this.Text24.Name = "Text24";
            //this.Text24.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text24__GetValue);
            //this.Text24.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            //this.Text24.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text24.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text24.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            //this.Text24.Indicator = null;
            //this.Text24.Interaction = null;
            //this.Text24.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text24.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text24.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //// 
            //// Text25
            //// 
            //this.Text25 = new Stimulsoft.Report.Components.StiText();
            //this.Text25.CanGrow = true;
            //this.Text25.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.3, 2.9, 7.1, 0.2);
            //this.Text25.Guid = "c7d8615a8ca040e8a5a262990da73530";
            //this.Text25.Name = "Text25";
            //this.Text25.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text25__GetValue);
            //this.Text25.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text25.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text25.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text25.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text25.Indicator = null;
            //this.Text25.Interaction = null;
            //this.Text25.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text25.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text25.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //// 
            //// Text26
            //// 
            //this.Text26 = new Stimulsoft.Report.Components.StiText();
            //this.Text26.CanGrow = true;
            //this.Text26.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.3, 3.2, 7.1, 0.2);
            //this.Text26.Guid = "904bae4166714b7e9665660ec7235697";
            //this.Text26.Name = "Text26";
            //this.Text26.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text26__GetValue);
            //this.Text26.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text26.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text26.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text26.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text26.Indicator = null;
            //this.Text26.Interaction = null;
            //this.Text26.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text26.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text26.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //// 
            //// Text27
            //// 
            //this.Text27 = new Stimulsoft.Report.Components.StiText();
            //this.Text27.CanGrow = true;
            //this.Text27.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.2, 4.7, 1.9, 0.2);
            //this.Text27.Guid = "5d4201b3063d40628df454282178c55f";
            //this.Text27.Name = "Text27";
            //this.Text27.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text27__GetValue);
            //this.Text27.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text27.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text27.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text27.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text27.Indicator = null;
            //this.Text27.Interaction = null;
            //this.Text27.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text27.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text27.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //// 
            //// Text28
            //// 
            //this.Text28 = new Stimulsoft.Report.Components.StiText();
            //this.Text28.CanGrow = true;
            //this.Text28.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(2.2, 4.7, 1.9, 0.2);
            //this.Text28.Guid = "050f5e4716ab44b3807d4ea809d27990";
            //this.Text28.Name = "Text28";
            //this.Text28.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text28__GetValue);
            //this.Text28.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text28.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text28.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text28.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text28.Indicator = null;
            //this.Text28.Interaction = null;
            //this.Text28.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text28.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text28.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //this.DataBand1.DataRelationName = null;
            //this.DataBand1.Guid = null;
            //this.DataBand1.Interaction = null;
            //this.DataBand1.MasterComponent = null;
            // 
            // DataBand2
            // 
            this.DataBand2 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand2.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 1.1, 7.49, 0.3);
            this.DataBand2.DataRelationName = "Relation";
            this.DataBand2.DataSourceName = "TBL2";
            this.DataBand2.Name = "DataBand2";
            this.DataBand2.PrintIfDetailEmpty = true;
            this.DataBand2.Sort = new System.String[0];
            this.DataBand2.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand2.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand2.BusinessObjectGuid = null;
            // 

            //// DataBand20
            //// 
            this.DataBand20 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand20.CanBreak = true;
            this.DataBand20.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 6, 9.1, 0.6);
            // this.DataBand20.DataRelationName = "Relation7";
            this.DataBand20.DataSourceName = "CategorySpecs";
            this.DataBand20.Name = "DataBand20";
            this.DataBand20.Sort = new System.String[0];
            this.DataBand20.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand20.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand20.BusinessObjectGuid = null;
            //// 
            //// DataBand21
            //// 
            this.DataBand21 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand21.CanBreak = true;
            this.DataBand21.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 6, 8, 0.6);
            // this.DataBand20.DataRelationName = "Relation7";
            this.DataBand21.DataSourceName = "CategorySpecs";
            this.DataBand21.Name = "DataBand21";
            this.DataBand21.Sort = new System.String[0];
            this.DataBand21.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand21.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand21.BusinessObjectGuid = null;
            //// 
            // Text17
            // 
            //this.Text17 = new Stimulsoft.Report.Components.StiText();
            //this.Text17.CanGrow = true;
            //this.Text17.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.3, 0.8, 7.1, 0.2);
            //this.Text17.GrowToHeight = true;
            //this.Text17.Guid = "5d4201b3063d40628df454282178c55f";
            //this.Text17.Name = "Text17";
            //this.Text17.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text17__GetValue);
            //this.Text17.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text17.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text17.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text17.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text17.Indicator = null;
            //this.Text17.Interaction = null;
            //this.Text17.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text17.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text17.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // Text302
            // 
            //this.Text18 = new Stimulsoft.Report.Components.StiText();
            //this.Text18.CanGrow = true;
            //this.Text18.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.2, 0, 1.9, 0.2);
            //this.Text18.GrowToHeight = true;
            //this.Text18.Guid = "050f5e4716ab44b3807d4ea809d27990";
            //this.Text18.Name = "Text18";
            //this.Text18.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text18__GetValue);
            //this.Text18.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            //this.Text18.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            //this.Text18.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            //this.Text18.Font = new System.Drawing.Font("Arial", 8F);
            //this.Text18.Indicator = null;
            //this.Text18.Interaction = null;
            //this.Text18.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            //this.Text18.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            //this.Text18.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 

            // Text3
            // 
            this.Text3 = new Stimulsoft.Report.Components.StiText();
            this.Text3.GetBookmark += new Stimulsoft.Report.Events.StiValueEventHandler(this.Text3__GetBookmark);
            this.Text3.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, 7.5, 0.2);
            this.Text3.Name = "Text3";
            this.Text3.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text3__GetValue);
            this.Text3.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text3.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DimGray);
            this.Text3.Font = new System.Drawing.Font("Arial", 12F);
            this.Text3.Guid = null;
            this.Text3.Indicator = null;
            this.Text3.Interaction = null;
            this.Text3.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text3.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.White);
            this.Text3.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.Text3.BeforePrint += new System.EventHandler(this.Text3_BeforePrint);
            this.DataBand2.Guid = null;
            this.DataBand2.Interaction = null;
            // 

            // DataBand13
            // 
            this.DataBand13 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand13.CanBreak = true;
            this.DataBand13.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 7, 7.49, 0.6);
            this.DataBand13.DataRelationName = "Relation7";
            this.DataBand13.DataSourceName = "FamilySpecs";
            this.DataBand13.Name = "DataBand13";
            this.DataBand13.Sort = new System.String[0];
            this.DataBand13.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand13.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand13.BusinessObjectGuid = null;
            // 

            // Text29
            // 
            this.Text29 = new Stimulsoft.Report.Components.StiText();
            this.Text29.CanGrow = true;
            this.Text29.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.4, 0.3, 7.1, 0.2);
            this.Text29.GrowToHeight = true;
            this.Text29.Guid = "78a2d734b61248c3aa8c514e23301518";
            this.Text29.Name = "Text29";
            this.Text29.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text29__GetValue);
            this.Text29.Type = Stimulsoft.Report.Components.StiSystemTextType.DataColumn;
            this.Text29.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text29.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text29.Font = new System.Drawing.Font("Arial", 8F);
            this.Text29.Indicator = null;
            this.Text29.Interaction = null;
            this.Text29.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text29.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text29.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // Text30
            // 
            this.Text30 = new Stimulsoft.Report.Components.StiText();
            this.Text30.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.2, 0, 1.9, 0.2);
            this.Text30.Guid = "024a69ce86d7461fa4f10f04f8c86928";
            this.Text30.Name = "Text30";
            this.Text30.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text30__GetValue);
            this.Text30.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text30.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text30.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text30.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.Text30.Indicator = null;
            this.Text30.Interaction = null;
            this.Text30.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text30.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text30.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.DataBand13.Guid = null;
            this.DataBand13.Interaction = null;
            // 
            // DataBand14
            // 
            this.DataBand14 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand14.CanBreak = true;
            this.DataBand14.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 8.3, 7.49, 1.5);
            this.DataBand14.DataRelationName = "Relation7";
            this.DataBand14.DataSourceName = "FamilySpecs";
            this.DataBand14.Guid = "68bd756af6b2441db30f83ac56ebd99a";
            this.DataBand14.Name = "DataBand14";
            this.DataBand14.Sort = new System.String[0];
            this.DataBand14.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand14.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand14.BusinessObjectGuid = null;
            // 
            // Text34
            // 
            this.Text34 = new Stimulsoft.Report.Components.StiText();
            this.Text34.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.2, 0, 1.9, 0.2);
            this.Text34.Guid = "5b4acc6c452a48b8bec0ae0c93877e11";
            this.Text34.Name = "Text34";
            this.Text34.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text34__GetValue);
            this.Text34.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text34.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text34.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text34.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.Text34.Indicator = null;
            this.Text34.Interaction = null;
            this.Text34.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text34.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text34.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // Image3
            // 
            this.Image3 = new Stimulsoft.Report.Components.StiImage();
            this.Image3.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.4, 0.3, 1.03, 1.09);
            this.Image3.Guid = "492fe8b2919b40e1956f0a7f9031a59b";
            this.Image3.Name = "Image3";
            this.Image3.Stretch = true;
            this.Image3.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Image3.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Image3.Image = null;
            this.Image3.Interaction = null;
            this.Image3.BeforePrint += new System.EventHandler(this.Image3_BeforePrint);
            this.DataBand14.Interaction = null;
            // 
            // DataBand3
            // 
            this.DataBand3 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand3.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 10.2, 7.49, 0.7);
            this.DataBand3.DataRelationName = "Relation2";
            this.DataBand3.DataSourceName = "TBL3";
            this.DataBand3.KeepChildTogether = true;
            this.DataBand3.KeepDetails = Stimulsoft.Report.Components.StiKeepDetails.KeepDetailsTogether;
            this.DataBand3.Name = "DataBand3";
            this.DataBand3.Sort = new System.String[0];
            this.DataBand3.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand3.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand3.BusinessObjectGuid = null;
            // 
            // CrossTab1
            // 
            this.CrossTab1 = new Stimulsoft.Report.CrossTab.StiCrossTab();
            this.CrossTab1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, 7.49, 0.7);
            this.CrossTab1.CrossTabStyleIndex = 1;
            this.CrossTab1.DataRelationName = "Relation2";
            this.CrossTab1.DataSourceName = "TBL3";
            this.CrossTab1.DockStyle = Stimulsoft.Report.Components.StiDockStyle.Fill;
            this.CrossTab1.EmptyValue = "";
            this.CrossTab1.HorAlignment = Stimulsoft.Report.CrossTab.StiCrossHorAlignment.Left;
            this.CrossTab1.Name = "CrossTab1";
            this.CrossTab1.PrintIfEmpty = false;
            this.CrossTab1.Sort = new System.String[0];
            this.CrossTab1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // CrossTab1_ColTotal1
            // 
            this.CrossTab1_ColTotal1 = new Stimulsoft.Report.CrossTab.StiCrossColumnTotal();
            this.CrossTab1_ColTotal1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(1.92, 0.22, 0.3, 0.2);
            this.CrossTab1_ColTotal1.Enabled = false;
            this.CrossTab1_ColTotal1.Guid = "0d11b439c8f14fef87d4fabeb82ee72f";
            this.CrossTab1_ColTotal1.Name = "CrossTab1_ColTotal1";
            this.CrossTab1_ColTotal1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab1_ColTotal1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab1_ColTotal1__GetValue);
            this.CrossTab1_ColTotal1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 69, 69, 69), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab1_ColTotal1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 219, 219, 219));
            this.CrossTab1_ColTotal1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab1_ColTotal1.Indicator = null;
            this.CrossTab1_ColTotal1.Interaction = null;
            this.CrossTab1_ColTotal1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab1_ColTotal1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.CrossTab1_ColTotal1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //this.CrossTab1_ColTotal1.AllowHtmlTags = true;
            // 
            // CrossTab1_RowTotal1
            // 
            this.CrossTab1_RowTotal1 = new Stimulsoft.Report.CrossTab.StiCrossRowTotal();
            this.CrossTab1_RowTotal1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.64, 0.8, 0);
            this.CrossTab1_RowTotal1.Guid = "081a107ac6d34e37807d7122f7264220";
            this.CrossTab1_RowTotal1.Name = "CrossTab1_RowTotal1";
            this.CrossTab1_RowTotal1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab1_RowTotal1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab1_RowTotal1__GetValue);
            this.CrossTab1_RowTotal1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 69, 69, 69), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab1_RowTotal1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 219, 219, 219));
            this.CrossTab1_RowTotal1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab1_RowTotal1.Indicator = null;
            this.CrossTab1_RowTotal1.Interaction = null;
            this.CrossTab1_RowTotal1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab1_RowTotal1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.CrossTab1_RowTotal1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            //this.CrossTab1_RowTotal1.AllowHtmlTags = true;
            // 
            // CrossTab1_Row1_Title
            // 
            this.CrossTab1_Row1_Title = new Stimulsoft.Report.CrossTab.StiCrossTitle();
            this.CrossTab1_Row1_Title.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.22, 0.8, 0.2);
            this.CrossTab1_Row1_Title.Enabled = false;
            this.CrossTab1_Row1_Title.Guid = "cda8967619f44503ab3ea763dd2d109c";
            this.CrossTab1_Row1_Title.Name = "CrossTab1_Row1_Title";
            this.CrossTab1_Row1_Title.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab1_Row1_Title.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab1_Row1_Title__GetValue);
            this.CrossTab1_Row1_Title.TypeOfComponent = "Row:CrossTab1_Row1";
            this.CrossTab1_Row1_Title.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 69, 69, 69), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab1_Row1_Title.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DarkGray);
            this.CrossTab1_Row1_Title.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab1_Row1_Title.Indicator = null;
            this.CrossTab1_Row1_Title.Interaction = null;
            this.CrossTab1_Row1_Title.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab1_Row1_Title.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 19, 19, 19));
            this.CrossTab1_Row1_Title.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab1_LeftTitle
            // 
            this.CrossTab1_LeftTitle = new Stimulsoft.Report.CrossTab.StiCrossTitle();
            this.CrossTab1_LeftTitle.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, 0.8, 0.2);
            this.CrossTab1_LeftTitle.Enabled = false;
            this.CrossTab1_LeftTitle.Guid = "8a776ff4572843a0b19c536d503ebcc3";
            this.CrossTab1_LeftTitle.Name = "CrossTab1_LeftTitle";
            this.CrossTab1_LeftTitle.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab1_LeftTitle.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab1_LeftTitle__GetValue);
            this.CrossTab1_LeftTitle.TypeOfComponent = "LeftTitle";
            this.CrossTab1_LeftTitle.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 69, 69, 69), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab1_LeftTitle.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DarkGray);
            this.CrossTab1_LeftTitle.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab1_LeftTitle.Indicator = null;
            this.CrossTab1_LeftTitle.Interaction = null;
            this.CrossTab1_LeftTitle.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab1_LeftTitle.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 19, 19, 19));
            this.CrossTab1_LeftTitle.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab1_RightTitle
            // 
            this.CrossTab1_RightTitle = new Stimulsoft.Report.CrossTab.StiCrossTitle();
            this.CrossTab1_RightTitle.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.82, 0, 1.4, 0.2);
            this.CrossTab1_RightTitle.Enabled = false;
            this.CrossTab1_RightTitle.Guid = "37e1c44babf248ee9e0a7fdce55c3d37";
            this.CrossTab1_RightTitle.Name = "CrossTab1_RightTitle";
            this.CrossTab1_RightTitle.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab1_RightTitle.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab1_RightTitle__GetValue);
            this.CrossTab1_RightTitle.TypeOfComponent = "RightTitle";
            this.CrossTab1_RightTitle.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 69, 69, 69), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab1_RightTitle.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DarkGray);
            this.CrossTab1_RightTitle.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab1_RightTitle.Indicator = null;
            this.CrossTab1_RightTitle.Interaction = null;
            this.CrossTab1_RightTitle.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab1_RightTitle.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 19, 19, 19));
            this.CrossTab1_RightTitle.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab1_Row1
            // 
            this.CrossTab1_Row1 = new Stimulsoft.Report.CrossTab.StiCrossRow();
            this.CrossTab1_Row1.Alias = "PRODUCT_ID";
            this.CrossTab1_Row1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.44, 0.8, 0.2);
            this.CrossTab1_Row1.TextProcess += new Stimulsoft.Report.Events.StiValueEventHandler(this.CrossTab1_Row1_Conditions);
            this.CrossTab1_Row1.GetDisplayCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab1_Row1__GetDisplayCrossValue);
            this.CrossTab1_Row1.Guid = "78c12d80b29e41dab69058a5f5c84947";
            this.CrossTab1_Row1.Name = "CrossTab1_Row1";
            this.CrossTab1_Row1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab1_Row1.ShowTotal = false;
            this.CrossTab1_Row1.SortDirection = Stimulsoft.Report.CrossTab.Core.StiSortDirection.None;
            this.CrossTab1_Row1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab1_Row1__GetValue);
            this.CrossTab1_Row1.TotalGuid = "081a107ac6d34e37807d7122f7264220";
            this.CrossTab1_Row1.GetCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab1_Row1__GetCrossValue);
            this.CrossTab1_Row1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 69, 69, 69), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab1_Row1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DarkGray);
            this.CrossTab1_Row1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab1_Row1.Indicator = null;
            this.CrossTab1_Row1.Interaction = null;
            this.CrossTab1_Row1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab1_Row1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 19, 19, 19));
            this.CrossTab1_Row1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // this.CrossTab1_Row1.AllowHtmlTags = true;
            // 
            // CrossTab1_Column1
            // 
            this.CrossTab1_Column1 = new Stimulsoft.Report.CrossTab.StiCrossColumn();
            this.CrossTab1_Column1.Alias = "ATTRIBUTE_NAME";
            this.CrossTab1_Column1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.82, 0.22, 1.1, 0.2);
            this.CrossTab1_Column1.GetDisplayCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab1_Column1__GetDisplayCrossValue);
            this.CrossTab1_Column1.Guid = "69649042884d40a6a722c0e959aebd0e";
            this.CrossTab1_Column1.MinSize = new Stimulsoft.Base.Drawing.SizeD(0.9, 0);
            this.CrossTab1_Column1.Name = "CrossTab1_Column1";
            this.CrossTab1_Column1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab1_Column1.SortDirection = Stimulsoft.Report.CrossTab.Core.StiSortDirection.None;
            this.CrossTab1_Column1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab1_Column1__GetValue);
            this.CrossTab1_Column1.TotalGuid = "0d11b439c8f14fef87d4fabeb82ee72f";
            this.CrossTab1_Column1.GetCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab1_Column1__GetCrossValue);
            this.CrossTab1_Column1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 69, 69, 69), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab1_Column1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DarkGray);
            this.CrossTab1_Column1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab1_Column1.Indicator = null;
            this.CrossTab1_Column1.Interaction = null;
            this.CrossTab1_Column1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab1_Column1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 19, 19, 19));
            this.CrossTab1_Column1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // this.CrossTab1_Column1.AllowHtmlTags = true;
            // 
            // CrossTab1_Sum1
            // 
            this.CrossTab1_Sum1 = new Stimulsoft.Report.CrossTab.StiCrossSummary();
            this.CrossTab1_Sum1.Alias = "STRING_VALUE";
            this.CrossTab1_Sum1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.82, 0.44, 1.1, 0.2);
            this.CrossTab1_Sum1.Guid = "6444dbde635041fdbfbf1bee86584a2c";
            this.CrossTab1_Sum1.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Left;
            this.CrossTab1_Sum1.Name = "CrossTab1_Sum1";
            this.CrossTab1_Sum1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab1_Sum1.Stretch = false;
            this.CrossTab1_Sum1.Summary = Stimulsoft.Report.CrossTab.Core.StiSummaryType.Image;
            this.CrossTab1_Sum1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab1_Sum1__GetValue);
            this.CrossTab1_Sum1.GetCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab1_Sum1__GetCrossValue);
            this.CrossTab1_Sum1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 69, 69, 69), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab1_Sum1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 255, 255, 255));
            this.CrossTab1_Sum1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab1_Sum1.Indicator = null;
            this.CrossTab1_Sum1.Interaction = null;
            this.CrossTab1_Sum1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab1_Sum1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.CrossTab1_Sum1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.CrossTab1_Sum1.BeforePrint += new System.EventHandler(this.CrossTab1_Sum1_BeforePrint);
            //this.CrossTab1_Sum1.AllowHtmlTags = true;
            // this.CrossTab1_Sum1.Value = "";
            this.CrossTab1.Guid = null;
            this.CrossTab1.Interaction = null;
            this.DataBand3.Guid = null;
            this.DataBand3.Interaction = null;
            // 
            // GroupHeaderBand6
            // 
            this.GroupHeaderBand6 = new Stimulsoft.Report.Components.StiGroupHeaderBand();
            this.GroupHeaderBand6.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 10.3, 7.49, 0.2);
            this.GroupHeaderBand6.GetValue += new Stimulsoft.Report.Events.StiValueEventHandler(this.GroupHeaderBand6__GetValue);
            this.GroupHeaderBand6.Name = "GroupHeaderBand6";
            this.GroupHeaderBand6.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.GroupHeaderBand6.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // Text31
            // 
            this.Text31 = new Stimulsoft.Report.Components.StiText();
            this.Text31.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.2, 0, 3.1, 0.2);
            this.Text31.Guid = "b2c6c02f7325496f904d84e3911ff494";
            this.Text31.Name = "Text31";
            this.Text31.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text31__GetValue);
            this.Text31.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text31.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text31.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text31.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.Text31.Indicator = null;
            this.Text31.Interaction = null;
            this.Text31.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text31.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text31.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.GroupHeaderBand6.Guid = null;
            this.GroupHeaderBand6.Interaction = null;
            // 
            // DataBand10
            // 
            this.DataBand10 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand10.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 10.9, 7.49, 0.2);
            this.DataBand10.DataRelationName = "Relation4";
            this.DataBand10.DataSourceName = "MultipleTableSoruce";
            this.DataBand10.KeepChildTogether = true;
            this.DataBand10.KeepDetails = Stimulsoft.Report.Components.StiKeepDetails.KeepDetailsTogether;
            this.DataBand10.Name = "DataBand10";
            this.DataBand10.Sort = new System.String[0];
            this.DataBand10.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand10.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand10.BusinessObjectGuid = null;
            // 
            // Text15
            // 
            this.Text15 = new Stimulsoft.Report.Components.StiText();
            this.Text15.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.8, 0, 3.1, 0.2);
            this.Text15.Name = "Text15";
            this.Text15.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text15__GetValue);
            this.Text15.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text15.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text15.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text15.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.Text15.Guid = null;
            this.Text15.Indicator = null;
            this.Text15.Interaction = null;
            this.Text15.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text15.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text15.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.DataBand10.Guid = null;
            this.DataBand10.Interaction = null;
            // 
            // DataBand9
            // 
            this.DataBand9 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand9.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 11.5, 7.49, 0.9);
            this.DataBand9.DataRelationName = "Relation3";
            this.InitializeComponent2();
        }

        public void InitializeComponent2()
        {
            this.DataBand9.DataSourceName = "MultipleTables";
            this.DataBand9.KeepChildTogether = true;
            this.DataBand9.KeepDetails = Stimulsoft.Report.Components.StiKeepDetails.KeepDetailsTogether;
            this.DataBand9.Name = "DataBand9";
            this.DataBand9.Sort = new System.String[0];
            this.DataBand9.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand9.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand9.BusinessObjectGuid = null;
            // 
            // CrossTab2
            // 
            this.CrossTab2 = new Stimulsoft.Report.CrossTab.StiCrossTab();
            this.CrossTab2.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, 7.49, 0.9);
            this.CrossTab2.CrossTabStyleIndex = 9;
            this.CrossTab2.DataRelationName = "Relation3";
            this.CrossTab2.DataSourceName = "MultipleTables";
            this.CrossTab2.DockStyle = Stimulsoft.Report.Components.StiDockStyle.Fill;
            this.CrossTab2.EmptyValue = "";
            this.CrossTab2.Name = "CrossTab2";
            this.CrossTab2.PrintIfEmpty = false;
            this.CrossTab2.Sort = new System.String[0];
            this.CrossTab2.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab2.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // CrossTab2_ColTotal1
            // 
            this.CrossTab2_ColTotal1 = new Stimulsoft.Report.CrossTab.StiCrossColumnTotal();
            this.CrossTab2_ColTotal1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(1.92, 0.22, 0, 0.2);
            this.CrossTab2_ColTotal1.Guid = "bc743bdfd7ab48d6ba0beced4ae97fe6";
            this.CrossTab2_ColTotal1.Name = "CrossTab2_ColTotal1";
            this.CrossTab2_ColTotal1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab2_ColTotal1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab2_ColTotal1__GetValue);
            this.CrossTab2_ColTotal1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 145, 122, 79), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab2_ColTotal1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 255, 255, 229));
            this.CrossTab2_ColTotal1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab2_ColTotal1.Indicator = null;
            this.CrossTab2_ColTotal1.Interaction = null;
            this.CrossTab2_ColTotal1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab2_ColTotal1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.CrossTab2_ColTotal1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab2_RowTotal1
            // 
            this.CrossTab2_RowTotal1 = new Stimulsoft.Report.CrossTab.StiCrossRowTotal();
            this.CrossTab2_RowTotal1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.8, 0.64, 0, 0);
            this.CrossTab2_RowTotal1.Guid = "304f02b84dd54f0db2df2ead18e026a4";
            this.CrossTab2_RowTotal1.Name = "CrossTab2_RowTotal1";
            this.CrossTab2_RowTotal1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab2_RowTotal1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab2_RowTotal1__GetValue);
            this.CrossTab2_RowTotal1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 145, 122, 79), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab2_RowTotal1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 255, 255, 229));
            this.CrossTab2_RowTotal1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab2_RowTotal1.Indicator = null;
            this.CrossTab2_RowTotal1.Interaction = null;
            this.CrossTab2_RowTotal1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab2_RowTotal1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.CrossTab2_RowTotal1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab2_Row1_Title
            // 
            this.CrossTab2_Row1_Title = new Stimulsoft.Report.CrossTab.StiCrossTitle();
            this.CrossTab2_Row1_Title.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.22, 0.8, 0.2);
            this.CrossTab2_Row1_Title.Enabled = false;
            this.CrossTab2_Row1_Title.Guid = "91c7e63139804d619dabc7af74ab9c0e";
            this.CrossTab2_Row1_Title.Name = "CrossTab2_Row1_Title";
            this.CrossTab2_Row1_Title.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab2_Row1_Title.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab2_Row1_Title__GetValue);
            this.CrossTab2_Row1_Title.TypeOfComponent = "Row:CrossTab2_Row1";
            this.CrossTab2_Row1_Title.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 145, 122, 79), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab2_Row1_Title.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Wheat);
            this.CrossTab2_Row1_Title.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab2_Row1_Title.Indicator = null;
            this.CrossTab2_Row1_Title.Interaction = null;

            this.CrossTab2_Row1_Title.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab2_Row1_Title.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 95, 72, 29));
            this.CrossTab2_Row1_Title.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab2_LeftTitle
            // 
            this.CrossTab2_LeftTitle = new Stimulsoft.Report.CrossTab.StiCrossTitle();
            this.CrossTab2_LeftTitle.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, 0.8, 0.2);
            this.CrossTab2_LeftTitle.Enabled = false;
            this.CrossTab2_LeftTitle.Guid = "3bc82442b07644a1b6b1ab5a6e948258";
            this.CrossTab2_LeftTitle.Name = "CrossTab2_LeftTitle";
            this.CrossTab2_LeftTitle.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab2_LeftTitle.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab2_LeftTitle__GetValue);
            this.CrossTab2_LeftTitle.TypeOfComponent = "LeftTitle";
            this.CrossTab2_LeftTitle.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 145, 122, 79), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab2_LeftTitle.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Wheat);
            this.CrossTab2_LeftTitle.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab2_LeftTitle.Indicator = null;
            this.CrossTab2_LeftTitle.Interaction = null;
            this.CrossTab2_LeftTitle.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab2_LeftTitle.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 95, 72, 29));
            this.CrossTab2_LeftTitle.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab2_RightTitle
            // 
            this.CrossTab2_RightTitle = new Stimulsoft.Report.CrossTab.StiCrossTitle();
            this.CrossTab2_RightTitle.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.82, 0, 1.1, 0.2);
            this.CrossTab2_RightTitle.Guid = "2c1f6acb363b4598806f936897ba1829";
            this.CrossTab2_RightTitle.Name = "CrossTab2_RightTitle";
            this.CrossTab2_RightTitle.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab2_RightTitle.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab2_RightTitle__GetValue);
            this.CrossTab2_RightTitle.TypeOfComponent = "RightTitle";
            this.CrossTab2_RightTitle.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 145, 122, 79), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab2_RightTitle.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Wheat);
            this.CrossTab2_RightTitle.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab2_RightTitle.Indicator = null;
            this.CrossTab2_RightTitle.Interaction = null;
            this.CrossTab2_RightTitle.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab2_RightTitle.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 95, 72, 29));
            this.CrossTab2_RightTitle.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab2_Row1
            // 
            this.CrossTab2_Row1 = new Stimulsoft.Report.CrossTab.StiCrossRow();
            this.CrossTab2_Row1.Alias = "PRODUCT_ID";
            this.CrossTab2_Row1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.44, 0.8, 0.2);
            this.CrossTab2_Row1.TextProcess += new Stimulsoft.Report.Events.StiValueEventHandler(this.CrossTab2_Row1_Conditions);
            this.CrossTab2_Row1.GetDisplayCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab2_Row1__GetDisplayCrossValue);
            this.CrossTab2_Row1.Guid = "08a7531fef0b41cda80752965d215181";
            this.CrossTab2_Row1.Name = "CrossTab2_Row1";
            this.CrossTab2_Row1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab2_Row1.ShowTotal = false;
            this.CrossTab2_Row1.SortDirection = Stimulsoft.Report.CrossTab.Core.StiSortDirection.None;
            this.CrossTab2_Row1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab2_Row1__GetValue);
            this.CrossTab2_Row1.TotalGuid = "304f02b84dd54f0db2df2ead18e026a4";
            this.CrossTab2_Row1.GetCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab2_Row1__GetCrossValue);
            this.CrossTab2_Row1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 145, 122, 79), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab2_Row1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Wheat);
            this.CrossTab2_Row1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab2_Row1.Indicator = null;
            this.CrossTab2_Row1.Interaction = null;
            this.CrossTab2_Row1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab2_Row1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 95, 72, 29));
            this.CrossTab2_Row1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab2_Column1
            // 
            this.CrossTab2_Column1 = new Stimulsoft.Report.CrossTab.StiCrossColumn();
            this.CrossTab2_Column1.Alias = "ATTRIBUTE_NAME";
            this.CrossTab2_Column1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.82, 0.22, 1.1, 0.2);
            this.CrossTab2_Column1.GetDisplayCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab2_Column1__GetDisplayCrossValue);
            this.CrossTab2_Column1.Guid = "b8a454c171074f0d89136cedf1a898a0";
            this.CrossTab2_Column1.Name = "CrossTab2_Column1";
            this.CrossTab2_Column1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab2_Column1.ShowTotal = false;
            this.CrossTab2_Column1.SortDirection = Stimulsoft.Report.CrossTab.Core.StiSortDirection.None;
            this.CrossTab2_Column1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab2_Column1__GetValue);
            this.CrossTab2_Column1.TotalGuid = "bc743bdfd7ab48d6ba0beced4ae97fe6";
            this.CrossTab2_Column1.GetCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab2_Column1__GetCrossValue);
            this.CrossTab2_Column1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 145, 122, 79), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab2_Column1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Wheat);
            this.CrossTab2_Column1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab2_Column1.Indicator = null;
            this.CrossTab2_Column1.Interaction = null;
            this.CrossTab2_Column1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab2_Column1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 95, 72, 29));
            this.CrossTab2_Column1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab2_Sum1
            // 
            this.CrossTab2_Sum1 = new Stimulsoft.Report.CrossTab.StiCrossSummary();
            this.CrossTab2_Sum1.Alias = "STRING_VALUE";
            this.CrossTab2_Sum1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.82, 0.44, 1.1, 0.2);
            this.CrossTab2_Sum1.Guid = "497740fbaad4481fa49a49831fa2988a";
            this.CrossTab2_Sum1.Name = "CrossTab2_Sum1";
            this.CrossTab2_Sum1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab2_Sum1.Stretch = false;
            this.CrossTab2_Sum1.Summary = Stimulsoft.Report.CrossTab.Core.StiSummaryType.Image;
            this.CrossTab2_Sum1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab2_Sum1__GetValue);
            this.CrossTab2_Sum1.GetCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab2_Sum1__GetCrossValue);
            this.CrossTab2_Sum1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 145, 122, 79), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab2_Sum1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 255, 255, 255));
            this.CrossTab2_Sum1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab2_Sum1.Indicator = null;
            this.CrossTab2_Sum1.Interaction = null;
            this.CrossTab2_Sum1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab2_Sum1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.CrossTab2_Sum1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.CrossTab2.Guid = null;
            this.CrossTab2.Interaction = null;
            this.DataBand9.Guid = null;
            this.DataBand9.Interaction = null;
            // 
            // GroupHeaderBand7
            // 
            this.GroupHeaderBand7 = new Stimulsoft.Report.Components.StiGroupHeaderBand();
            this.GroupHeaderBand7.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 12.8, 7.49, 0.2);
            this.GroupHeaderBand7.GetValue += new Stimulsoft.Report.Events.StiValueEventHandler(this.GroupHeaderBand7__GetValue);
            this.GroupHeaderBand7.Guid = "a25c12ad35874e6e89e1b16c5c68df82";
            this.GroupHeaderBand7.Name = "GroupHeaderBand7";
            this.GroupHeaderBand7.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.GroupHeaderBand7.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // Text32
            // 
            this.Text32 = new Stimulsoft.Report.Components.StiText();
            this.Text32.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.2, 0, 3.1, 0.2);
            this.Text32.Guid = "6e745a5ce9644942a2eeecebc794fcbe";
            this.Text32.Name = "Text32";
            this.Text32.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text32__GetValue);
            this.Text32.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text32.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text32.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text32.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.Text32.Indicator = null;
            this.Text32.Interaction = null;
            this.Text32.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text32.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text32.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.GroupHeaderBand7.Interaction = null;
            // 
            // DataBand12
            // 
            this.DataBand12 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand12.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 13.4, 7.49, 0.3);
            this.DataBand12.DataRelationName = "Relation6";
            this.DataBand12.DataSourceName = "ReferenceTableSource";
            this.DataBand12.KeepChildTogether = true;
            this.DataBand12.KeepDetails = Stimulsoft.Report.Components.StiKeepDetails.KeepDetailsTogether;
            this.DataBand12.Name = "DataBand12";
            this.DataBand12.Sort = new System.String[0];
            this.DataBand12.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand12.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand12.BusinessObjectGuid = null;
            // 
            // Text16
            // 
            this.Text16 = new Stimulsoft.Report.Components.StiText();
            this.Text16.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.8, 0.1, 3.1, 0.15);
            this.Text16.Guid = "4d0e8cddbf3b4ec5a88bcf56645507b6";
            this.Text16.Name = "Text16";
            this.Text16.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text16__GetValue);
            this.Text16.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text16.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text16.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text16.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.Text16.Indicator = null;
            this.Text16.Interaction = null;
            this.Text16.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text16.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text16.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.DataBand12.Guid = null;
            this.DataBand12.Interaction = null;
            // 
            // DataBand11
            // 
            this.DataBand11 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand11.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 14.1, 7.49, 0.8);
            this.DataBand11.DataRelationName = "Relation5";
            this.DataBand11.DataSourceName = "ReferenceTables";
            this.DataBand11.KeepChildTogether = true;
            this.DataBand11.KeepDetails = Stimulsoft.Report.Components.StiKeepDetails.KeepDetailsTogether;
            this.DataBand11.Linked = true;
            this.DataBand11.Locked = true;
            this.DataBand11.Name = "DataBand11";
            this.DataBand11.Sort = new System.String[0];
            this.DataBand11.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand11.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand11.BusinessObjectGuid = null;
            // 
            // CrossTab3
            // 
            this.CrossTab3 = new Stimulsoft.Report.CrossTab.StiCrossTab();
            this.CrossTab3.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, 7.49, 0.7);
            this.CrossTab3.CrossTabStyleIndex = 10;
            this.CrossTab3.DataRelationName = "Relation5";
            this.CrossTab3.DataSourceName = "ReferenceTables";
            this.CrossTab3.EmptyValue = "";
            this.CrossTab3.KeepCrossTabTogether = true;
            this.CrossTab3.Name = "CrossTab3";
            this.CrossTab3.PrintIfEmpty = false;
            this.CrossTab3.Sort = new System.String[0];
            this.CrossTab3.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab3.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // CrossTab3_ColTotal1
            // 
            this.CrossTab3_ColTotal1 = new Stimulsoft.Report.CrossTab.StiCrossColumnTotal();
            this.CrossTab3_ColTotal1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(1.92, 0.22, 0, 0.2);
            this.CrossTab3_ColTotal1.Guid = "67f520107bc3440593af0315cd5cc7d8";
            this.CrossTab3_ColTotal1.Name = "CrossTab3_ColTotal1";
            this.CrossTab3_ColTotal1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab3_ColTotal1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab3_ColTotal1__GetValue);
            this.CrossTab3_ColTotal1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 140, 130, 40), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab3_ColTotal1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 255, 255, 190));
            this.CrossTab3_ColTotal1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab3_ColTotal1.Indicator = null;
            this.CrossTab3_ColTotal1.Interaction = null;
            this.CrossTab3_ColTotal1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab3_ColTotal1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.CrossTab3_ColTotal1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab3_RowTotal1
            // 
            this.CrossTab3_RowTotal1 = new Stimulsoft.Report.CrossTab.StiCrossRowTotal();
            this.CrossTab3_RowTotal1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.8, 0.64, 0, 0);
            this.CrossTab3_RowTotal1.Guid = "97435ddcf70d4a8abe5b3e1e65f04597";
            this.CrossTab3_RowTotal1.Name = "CrossTab3_RowTotal1";
            this.CrossTab3_RowTotal1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab3_RowTotal1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab3_RowTotal1__GetValue);
            this.CrossTab3_RowTotal1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 140, 130, 40), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab3_RowTotal1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 255, 255, 190));
            this.CrossTab3_RowTotal1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab3_RowTotal1.Indicator = null;
            this.CrossTab3_RowTotal1.Interaction = null;
            this.CrossTab3_RowTotal1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab3_RowTotal1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.CrossTab3_RowTotal1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab3_Row1_Title
            // 
            this.CrossTab3_Row1_Title = new Stimulsoft.Report.CrossTab.StiCrossTitle();
            this.CrossTab3_Row1_Title.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.22, 0.9, 0.2);
            this.CrossTab3_Row1_Title.Enabled = false;
            this.CrossTab3_Row1_Title.Guid = "327e4cdd6a1e4a57bda776613f76cef4";
            this.CrossTab3_Row1_Title.Name = "CrossTab3_Row1_Title";
            this.CrossTab3_Row1_Title.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab3_Row1_Title.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab3_Row1_Title__GetValue);
            this.CrossTab3_Row1_Title.TypeOfComponent = "Row:CrossTab3_Row1";
            this.CrossTab3_Row1_Title.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 140, 130, 40), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab3_Row1_Title.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Khaki);
            this.CrossTab3_Row1_Title.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab3_Row1_Title.Indicator = null;
            this.CrossTab3_Row1_Title.Interaction = null;
            this.CrossTab3_Row1_Title.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab3_Row1_Title.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 90, 80, 0));
            this.CrossTab3_Row1_Title.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab3_LeftTitle
            // 
            this.CrossTab3_LeftTitle = new Stimulsoft.Report.CrossTab.StiCrossTitle();
            this.CrossTab3_LeftTitle.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, 0.9, 0.2);
            this.CrossTab3_LeftTitle.Enabled = false;
            this.CrossTab3_LeftTitle.Guid = "41bba4918a4e4284a4e52c38894b91fd";
            this.CrossTab3_LeftTitle.Name = "CrossTab3_LeftTitle";
            this.CrossTab3_LeftTitle.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab3_LeftTitle.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab3_LeftTitle__GetValue);
            this.CrossTab3_LeftTitle.TypeOfComponent = "LeftTitle";
            this.CrossTab3_LeftTitle.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 140, 130, 40), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab3_LeftTitle.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Khaki);
            this.CrossTab3_LeftTitle.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab3_LeftTitle.Indicator = null;
            this.CrossTab3_LeftTitle.Interaction = null;
            this.CrossTab3_LeftTitle.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab3_LeftTitle.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 90, 80, 0));
            this.CrossTab3_LeftTitle.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab3_RightTitle
            // 
            this.CrossTab3_RightTitle = new Stimulsoft.Report.CrossTab.StiCrossTitle();
            this.CrossTab3_RightTitle.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.92, 0, 1, 0.2);
            this.CrossTab3_RightTitle.Guid = "2ef9dd425273419ea2a0e33c5ccea09d";
            this.CrossTab3_RightTitle.Name = "CrossTab3_RightTitle";
            this.CrossTab3_RightTitle.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab3_RightTitle.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab3_RightTitle__GetValue);
            this.CrossTab3_RightTitle.TypeOfComponent = "RightTitle";
            this.CrossTab3_RightTitle.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 140, 130, 40), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab3_RightTitle.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Khaki);
            this.CrossTab3_RightTitle.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab3_RightTitle.Indicator = null;
            this.CrossTab3_RightTitle.Interaction = null;
            this.CrossTab3_RightTitle.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab3_RightTitle.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 90, 80, 0));
            this.CrossTab3_RightTitle.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab3_Row1
            // 
            this.CrossTab3_Row1 = new Stimulsoft.Report.CrossTab.StiCrossRow();
            this.CrossTab3_Row1.Alias = "ROW_INDEX";
            this.CrossTab3_Row1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.44, 0.9, 0.2);
            this.CrossTab3_Row1.TextProcess += new Stimulsoft.Report.Events.StiValueEventHandler(this.CrossTab3_Row1_Conditions);
            this.CrossTab3_Row1.GetDisplayCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab3_Row1__GetDisplayCrossValue);
            this.CrossTab3_Row1.Guid = "33088611179c4357aa55d88374113dca";
            this.CrossTab3_Row1.Name = "CrossTab3_Row1";
            this.CrossTab3_Row1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab3_Row1.ShowTotal = false;
            this.CrossTab3_Row1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab3_Row1__GetValue);
            this.CrossTab3_Row1.TotalGuid = "97435ddcf70d4a8abe5b3e1e65f04597";
            this.CrossTab3_Row1.GetCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab3_Row1__GetCrossValue);
            this.CrossTab3_Row1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 140, 130, 40), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab3_Row1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Khaki);
            this.CrossTab3_Row1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab3_Row1.Indicator = null;
            this.CrossTab3_Row1.Interaction = null;
            this.CrossTab3_Row1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab3_Row1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 90, 80, 0));
            this.CrossTab3_Row1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab3_Column1
            // 
            this.CrossTab3_Column1 = new Stimulsoft.Report.CrossTab.StiCrossColumn();
            this.CrossTab3_Column1.Alias = "COLUMN_INDEX";
            this.CrossTab3_Column1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.92, 0.22, 1, 0.2);
            this.CrossTab3_Column1.TextProcess += new Stimulsoft.Report.Events.StiValueEventHandler(this.CrossTab3_Column1_Conditions);
            this.CrossTab3_Column1.GetDisplayCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab3_Column1__GetDisplayCrossValue);
            this.CrossTab3_Column1.Guid = "dd49fda6d1b54b95b93dbd72342e5d80";
            this.CrossTab3_Column1.Name = "CrossTab3_Column1";
            this.CrossTab3_Column1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab3_Column1.ShowTotal = false;
            this.CrossTab3_Column1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab3_Column1__GetValue);
            this.CrossTab3_Column1.TotalGuid = "67f520107bc3440593af0315cd5cc7d8";
            this.CrossTab3_Column1.GetCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab3_Column1__GetCrossValue);
            this.CrossTab3_Column1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 140, 130, 40), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab3_Column1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Khaki);
            this.CrossTab3_Column1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab3_Column1.Indicator = null;
            this.CrossTab3_Column1.Interaction = null;
            this.CrossTab3_Column1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab3_Column1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 90, 80, 0));
            this.CrossTab3_Column1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // CrossTab3_Sum1
            // 
            this.CrossTab3_Sum1 = new Stimulsoft.Report.CrossTab.StiCrossSummary();
            this.CrossTab3_Sum1.Alias = "ROW_VALUE";
            this.CrossTab3_Sum1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.92, 0.44, 1, 0.2);
            this.CrossTab3_Sum1.Guid = "c7eda163fe154d70a7efb7cc5961e043";
            this.CrossTab3_Sum1.Name = "CrossTab3_Sum1";
            this.CrossTab3_Sum1.Restrictions = ((((Stimulsoft.Report.Components.StiRestrictions.None | Stimulsoft.Report.Components.StiRestrictions.AllowMove)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowResize)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowSelect)
                        | Stimulsoft.Report.Components.StiRestrictions.AllowChange);
            this.CrossTab3_Sum1.Stretch = false;
            this.CrossTab3_Sum1.Summary = Stimulsoft.Report.CrossTab.Core.StiSummaryType.Image;
            this.CrossTab3_Sum1.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.CrossTab3_Sum1__GetValue);
            this.CrossTab3_Sum1.GetCrossValue += new Stimulsoft.Report.CrossTab.StiGetCrossValueEventHandler(this.CrossTab3_Sum1__GetCrossValue);
            this.CrossTab3_Sum1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.FromArgb(255, 140, 130, 40), 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.CrossTab3_Sum1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(255, 255, 255, 240));
            this.CrossTab3_Sum1.Font = new System.Drawing.Font("Arial", 8F);
            this.CrossTab3_Sum1.Indicator = null;
            this.CrossTab3_Sum1.Interaction = null;
            this.CrossTab3_Sum1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.CrossTab3_Sum1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.CrossTab3_Sum1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.CrossTab3_Sum1.BeforePrint += new System.EventHandler(this.CrossTab3_Sum1_BeforePrint);
            this.CrossTab3.Guid = null;
            this.CrossTab3.Interaction = null;
            this.DataBand11.Guid = null;
            this.DataBand11.Interaction = null;
            this.Main.ExcelSheetValue = null;
            this.Main.Interaction = null;
            this.Main.Margins = new Stimulsoft.Report.Components.StiMargins(0.39, 0.39, 0.39, 0.39);
            this.Main_Watermark = new Stimulsoft.Report.Components.StiWatermark();
            this.Main_Watermark.Font = new System.Drawing.Font("Arial", 100F);
            this.Main_Watermark.Image = null;
            this.Main_Watermark.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(50, 0, 0, 0));
            this.Main.BeforePrint += new System.EventHandler(this.Main_BeforePrint);
            // 
            // ItemIndex
            // 
            this.ItemIndex = new Stimulsoft.Report.Components.StiPage();
            this.ItemIndex.ColumnGaps = 0.1;
            this.ItemIndex.Columns = 2;
            this.ItemIndex.ColumnWidth = 3.7;
            this.ItemIndex.Guid = "973ab7375f744d219f304a0705a8908c";
            this.ItemIndex.Name = "ItemIndex";
            this.ItemIndex.PageHeight = 11.69;
            this.ItemIndex.PageWidth = 8.27;
            this.ItemIndex.PaperSize = System.Drawing.Printing.PaperKind.Letter;
            this.ItemIndex.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 2, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.ItemIndex.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // PageHeaderBand2
            // 
            this.PageHeaderBand2 = new Stimulsoft.Report.Components.StiPageHeaderBand();
            this.PageHeaderBand2.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.2, 7.49, 0.5);
            this.PageHeaderBand2.Name = "PageHeaderBand2";
            this.PageHeaderBand2.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.PageHeaderBand2.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // Text11
            // 
            this.Text11 = new Stimulsoft.Report.Components.StiText();
            this.Text11.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.1, 7.5, 0.3);
            this.Text11.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Center;
            this.Text11.Name = "Text11";
            this.Text11.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text11__GetValue);
            this.Text11.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text11.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text11.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text11.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.Text11.Guid = null;
            this.Text11.Indicator = null;
            this.Text11.Interaction = null;
            this.Text11.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text11.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text11.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.PageHeaderBand2.Guid = null;
            this.PageHeaderBand2.Interaction = null;
            // 
            // GroupHeaderBand4
            // 
            this.GroupHeaderBand4 = new Stimulsoft.Report.Components.StiGroupHeaderBand();
            this.GroupHeaderBand4.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 1.1, 3.7, 0);
            this.GroupHeaderBand4.GetValue += new Stimulsoft.Report.Events.StiValueEventHandler(this.GroupHeaderBand4__GetValue);
            this.GroupHeaderBand4.Guid = "2d3347ec60d740209af8febf1c070b42";
            this.GroupHeaderBand4.Name = "GroupHeaderBand4";
            this.GroupHeaderBand4.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.GroupHeaderBand4.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.GroupHeaderBand4.Interaction = null;
            // 
            // DataBand7
            // 
            this.DataBand7 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand7.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 1.5, 3.7, 0);
            this.DataBand7.DataSourceName = "TBL1";
            this.DataBand7.Guid = "ed3ffaa1661148959256e9d408b621ab";
            this.DataBand7.Name = "DataBand7";
            this.DataBand7.Sort = new System.String[0];
            this.DataBand7.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand7.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand7.BusinessObjectGuid = null;
            this.DataBand7.DataRelationName = null;
            this.DataBand7.Interaction = null;
            this.DataBand7.MasterComponent = null;
            // 
            // GroupHeaderBand5
            // 
            this.GroupHeaderBand5 = new Stimulsoft.Report.Components.StiGroupHeaderBand();
            this.GroupHeaderBand5.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 1.9, 3.7, 0);
            this.GroupHeaderBand5.GetValue += new Stimulsoft.Report.Events.StiValueEventHandler(this.GroupHeaderBand5__GetValue);
            this.GroupHeaderBand5.Guid = "4f6ad01be4a8426287b87c510d39848a";
            this.GroupHeaderBand5.Name = "GroupHeaderBand5";
            this.GroupHeaderBand5.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.GroupHeaderBand5.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.GroupHeaderBand5.Interaction = null;
            // 
            // DataBand8
            // 
            this.DataBand8 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand8.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 2.3, 3.7, 0);
            this.DataBand8.DataRelationName = "Relation";
            this.DataBand8.DataSourceName = "TBL2";
            this.DataBand8.Guid = "9245075ec1a74147b66454cd7ebf6da0";
            this.DataBand8.Name = "DataBand8";
            this.DataBand8.Sort = new System.String[0];
            this.DataBand8.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand8.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand8.BusinessObjectGuid = null;
            this.DataBand8.Interaction = null;
            // 
            // GroupHeaderBand3
            // 
            this.GroupHeaderBand3 = new Stimulsoft.Report.Components.StiGroupHeaderBand();
            this.GroupHeaderBand3.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 2.7, 3.7, 0.2);
            this.GroupHeaderBand3.GetValue += new Stimulsoft.Report.Events.StiValueEventHandler(this.GroupHeaderBand3__GetValue);
            this.GroupHeaderBand3.Name = "GroupHeaderBand3";
            this.GroupHeaderBand3.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.GroupHeaderBand3.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            // 
            // Text13
            // 
            this.Text13 = new Stimulsoft.Report.Components.StiText();
            this.Text13.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, 1.2, 0.2);
            this.Text13.Guid = "40ab4d853b924e00b13a4500dc213edf";
            this.Text13.Name = "Text13";
            this.Text13.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text13__GetValue);
            this.Text13.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text13.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text13.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text13.Font = new System.Drawing.Font("Arial", 8F);
            this.Text13.Indicator = null;
            this.Text13.Interaction = null;
            this.Text13.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text13.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text13.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            // 
            // Text14
            // 
            this.Text14 = new Stimulsoft.Report.Components.StiText();
            this.Text14.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(1.3, 0, 0.3, 0.2);
            this.Text14.Guid = "b392c43548e84ba6ad45bb750686d564";
            this.Text14.Name = "Text14";
            this.Text14.GetTag += new Stimulsoft.Report.Events.StiValueEventHandler(this.Text14__GetTag);
            this.Text14.GetValue += new Stimulsoft.Report.Events.StiGetValueEventHandler(this.Text14__GetValue);
            this.Text14.Type = Stimulsoft.Report.Components.StiSystemTextType.Expression;
            this.Text14.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.Text14.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.Text14.Font = new System.Drawing.Font("Arial", 8F);
            this.Text14.Indicator = null;
            this.Text14.Interaction = null;
            this.Text14.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
            this.Text14.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
            this.Text14.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
            this.GroupHeaderBand3.Guid = null;
            this.GroupHeaderBand3.Interaction = null;
            // 
            // DataBand6
            // 
            this.DataBand6 = new Stimulsoft.Report.Components.StiDataBand();
            this.DataBand6.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 3.3, 3.7, 0);
            this.DataBand6.DataRelationName = "Relation2";
            this.DataBand6.DataSourceName = "TBL3";
            this.DataBand6.Name = "DataBand6";
            this.DataBand6.Sort = new System.String[0];
            this.DataBand6.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black), false);
            this.DataBand6.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
            this.DataBand6.BusinessObjectGuid = null;
            this.DataBand6.Guid = null;
            this.DataBand6.Interaction = null;
            this.ItemIndex.ExcelSheetValue = null;
            this.ItemIndex.Interaction = null;
            this.ItemIndex.Margins = new Stimulsoft.Report.Components.StiMargins(0.39, 0.39, 0.39, 0.39);
            this.ItemIndex_Watermark = new Stimulsoft.Report.Components.StiWatermark();
            this.ItemIndex_Watermark.Font = new System.Drawing.Font("Arial", 100F);
            this.ItemIndex_Watermark.Image = null;
            this.ItemIndex_Watermark.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.FromArgb(50, 0, 0, 0));
            this.Report_PrinterSettings = new Stimulsoft.Report.Print.StiPrinterSettings();
            this.PrinterSettings = this.Report_PrinterSettings;
            this.Cover.Report = this;
            this.Cover.Watermark = this.Cover_Watermark;
            this.Text2.Page = this.Cover;
            this.Text2.Parent = this.Cover;
            this.TOC.Report = this;
            this.TOC.Watermark = this.TOC_Watermark;
            this.PageHeaderBand1.Page = this.TOC;
            this.PageHeaderBand1.Parent = this.TOC;
            this.Text8.Page = this.TOC;
            this.Text8.Parent = this.PageHeaderBand1;
            this.GroupHeaderBand1.Page = this.TOC;
            this.GroupHeaderBand1.Parent = this.TOC;
            this.Text4.Page = this.TOC;
            this.Text4.Parent = this.GroupHeaderBand1;
            this.Text6.Page = this.TOC;
            this.Text6.Parent = this.GroupHeaderBand1;
            this.DataBand4.Page = this.TOC;
            this.DataBand4.Parent = this.TOC;
            this.GroupHeaderBand2.Page = this.TOC;
            this.GroupHeaderBand2.Parent = this.TOC;
            this.Text5.Page = this.TOC;
            this.Text5.Parent = this.GroupHeaderBand2;
            this.Text7.Page = this.TOC;
            this.Text7.Parent = this.GroupHeaderBand2;
            this.DataBand5.MasterComponent = this.DataBand4;
            this.DataBand5.Page = this.TOC;
            this.DataBand5.Parent = this.TOC;
            this.Main.Report = this;
            this.Main.Watermark = this.Main_Watermark;
            this.PageFooterBand1.Page = this.Main;
            this.PageFooterBand1.Parent = this.Main;
            this.Text9.Page = this.Main;
            this.Text9.Parent = this.PageFooterBand1;
            this.Text10.Page = this.Main;
            this.Text10.Parent = this.PageFooterBand1;
            this.Text12.Page = this.Main;
            this.Text12.Parent = this.PageFooterBand1;
            this.HorizontalLinePrimitive1.Page = this.Main;
            this.HorizontalLinePrimitive1.Parent = this.PageFooterBand1;
            this.DataBand1.Page = this.Main;
            this.DataBand1.Parent = this.Main;
            this.Text1.Page = this.Main;
            this.Text1.Parent = this.DataBand1;
            this.Image1.Page = this.Main;
            this.Image1.Parent = this.DataBand21;
            //this.Image2.Page = this.Main;
            //this.Image2.Parent = this.DataBand1;
            this.Text17.Page = this.Main;
            this.Text17.Parent = this.DataBand20;
            this.Text18.Page = this.Main;
            this.Text18.Parent = this.DataBand20;
            //this.Text17.Page = this.Main;
            //this.Text17.Parent = this.DataBand1;
            //this.Text18.Page = this.Main;
            //this.Text18.Parent = this.DataBand1;
            this.Text19.Page = this.Main;
            this.Text19.Parent = this.DataBand21;
            //this.Text20.Page = this.Main;
            //this.Text20.Parent = this.DataBand1;
            //this.Text21.Page = this.Main;
            //this.Text21.Parent = this.DataBand1;
            //this.Text22.Page = this.Main;
            //this.Text22.Parent = this.DataBand1;
            //this.Text23.Page = this.Main;
            //this.Text23.Parent = this.DataBand1;
            //this.Text24.Page = this.Main;
            //this.Text24.Parent = this.DataBand1;
            //this.Text25.Page = this.Main;
            //this.Text25.Parent = this.DataBand1;
            //this.Text26.Page = this.Main;
            //this.Text26.Parent = this.DataBand1;
            //this.Text27.Page = this.Main;
            //this.Text27.Parent = this.DataBand1;
            //this.Text28.Page = this.Main;
            //this.Text28.Parent = this.DataBand1;
            this.DataBand20.MasterComponent = this.DataBand1;
            this.DataBand20.Page = this.Main;
            this.DataBand20.Parent = this.Main;
            this.DataBand21.MasterComponent = this.DataBand1;
            this.DataBand21.Page = this.Main;
            this.DataBand21.Parent = this.Main;
            this.Text17.Page = this.Main;
            this.Text17.Parent = this.DataBand20;
            this.Text18.Page = this.Main;
            this.Text18.Parent = this.DataBand20;
            this.Text19.Page = this.Main;
            this.Text19.Parent = this.DataBand21;
            this.Image1.Page = this.Main;
            this.Image1.Parent = this.DataBand21;
            this.DataBand2.MasterComponent = this.DataBand1;
            this.DataBand2.Page = this.Main;
            this.DataBand2.Parent = this.Main;
            this.Text3.Page = this.Main;
            this.Text3.Parent = this.DataBand2;

            this.DataBand13.MasterComponent = this.DataBand2;
            this.DataBand13.Page = this.Main;
            this.DataBand13.Parent = this.Main;
            this.Text29.Page = this.Main;
            this.Text29.Parent = this.DataBand13;
            this.Text30.Page = this.Main;
            this.Text30.Parent = this.DataBand13;
            this.DataBand14.MasterComponent = this.DataBand2;
            this.DataBand14.Page = this.Main;
            this.DataBand14.Parent = this.Main;
            this.Text34.Page = this.Main;
            this.Text34.Parent = this.DataBand14;
            this.Image3.Page = this.Main;
            this.Image3.Parent = this.DataBand14;
            this.DataBand3.MasterComponent = this.DataBand2;
            this.DataBand3.Page = this.Main;
            this.DataBand3.Parent = this.Main;
            this.CrossTab1.Page = this.Main;
            this.CrossTab1.Parent = this.DataBand3;
            this.CrossTab1_ColTotal1.Page = this.Main;
            this.CrossTab1_ColTotal1.Parent = this.CrossTab1;
            this.CrossTab1_RowTotal1.Page = this.Main;
            this.CrossTab1_RowTotal1.Parent = this.CrossTab1;
            this.CrossTab1_Row1_Title.Page = this.Main;
            this.CrossTab1_Row1_Title.Parent = this.CrossTab1;
            this.CrossTab1_LeftTitle.Page = this.Main;
            this.CrossTab1_LeftTitle.Parent = this.CrossTab1;
            this.CrossTab1_RightTitle.Page = this.Main;
            this.CrossTab1_RightTitle.Parent = this.CrossTab1;
            this.CrossTab1_Row1.Page = this.Main;
            this.CrossTab1_Row1.Parent = this.CrossTab1;
            this.CrossTab1_Column1.Page = this.Main;
            this.CrossTab1_Column1.Parent = this.CrossTab1;
            this.CrossTab1_Sum1.Page = this.Main;
            this.CrossTab1_Sum1.Parent = this.CrossTab1;
            this.GroupHeaderBand6.Page = this.Main;
            this.GroupHeaderBand6.Parent = this.Main;
            this.Text31.Page = this.Main;
            this.Text31.Parent = this.GroupHeaderBand6;
            this.DataBand10.MasterComponent = this.DataBand2;
            this.DataBand10.Page = this.Main;
            this.DataBand10.Parent = this.Main;
            this.Text15.Page = this.Main;
            this.Text15.Parent = this.DataBand10;
            this.DataBand9.MasterComponent = this.DataBand10;
            this.DataBand9.Page = this.Main;
            this.DataBand9.Parent = this.Main;
            this.CrossTab2.Page = this.Main;
            this.CrossTab2.Parent = this.DataBand9;
            this.CrossTab2_ColTotal1.Page = this.Main;
            this.CrossTab2_ColTotal1.Parent = this.CrossTab2;
            this.CrossTab2_RowTotal1.Page = this.Main;
            this.CrossTab2_RowTotal1.Parent = this.CrossTab2;
            this.CrossTab2_Row1_Title.Page = this.Main;
            this.CrossTab2_Row1_Title.Parent = this.CrossTab2;
            this.CrossTab2_LeftTitle.Page = this.Main;
            this.CrossTab2_LeftTitle.Parent = this.CrossTab2;
            this.CrossTab2_RightTitle.Page = this.Main;
            this.CrossTab2_RightTitle.Parent = this.CrossTab2;
            this.CrossTab2_Row1.Page = this.Main;
            this.CrossTab2_Row1.Parent = this.CrossTab2;
            this.CrossTab2_Column1.Page = this.Main;
            this.CrossTab2_Column1.Parent = this.CrossTab2;
            this.CrossTab2_Sum1.Page = this.Main;
            this.CrossTab2_Sum1.Parent = this.CrossTab2;
            this.GroupHeaderBand7.Page = this.Main;
            this.GroupHeaderBand7.Parent = this.Main;
            this.Text32.Page = this.Main;
            this.Text32.Parent = this.GroupHeaderBand7;
            this.DataBand12.MasterComponent = this.DataBand2;
            this.DataBand12.Page = this.Main;
            this.DataBand12.Parent = this.Main;
            this.Text16.Page = this.Main;
            this.Text16.Parent = this.DataBand12;
            this.DataBand11.MasterComponent = this.DataBand12;
            this.DataBand11.Page = this.Main;
            this.DataBand11.Parent = this.Main;
            this.CrossTab3.Page = this.Main;
            this.CrossTab3.Parent = this.DataBand11;
            this.CrossTab3_ColTotal1.Page = this.Main;
            this.CrossTab3_ColTotal1.Parent = this.CrossTab3;
            this.CrossTab3_RowTotal1.Page = this.Main;
            this.CrossTab3_RowTotal1.Parent = this.CrossTab3;
            this.CrossTab3_Row1_Title.Page = this.Main;
            this.CrossTab3_Row1_Title.Parent = this.CrossTab3;
            this.CrossTab3_LeftTitle.Page = this.Main;
            this.CrossTab3_LeftTitle.Parent = this.CrossTab3;
            this.CrossTab3_RightTitle.Page = this.Main;
            this.CrossTab3_RightTitle.Parent = this.CrossTab3;
            this.CrossTab3_Row1.Page = this.Main;
            this.CrossTab3_Row1.Parent = this.CrossTab3;
            this.CrossTab3_Column1.Page = this.Main;
            this.CrossTab3_Column1.Parent = this.CrossTab3;
            this.CrossTab3_Sum1.Page = this.Main;
            this.CrossTab3_Sum1.Parent = this.CrossTab3;
            this.ItemIndex.Report = this;
            this.ItemIndex.Watermark = this.ItemIndex_Watermark;
            this.PageHeaderBand2.Page = this.ItemIndex;
            this.PageHeaderBand2.Parent = this.ItemIndex;
            this.Text11.Page = this.ItemIndex;
            this.Text11.Parent = this.PageHeaderBand2;
            this.GroupHeaderBand4.Page = this.ItemIndex;
            this.GroupHeaderBand4.Parent = this.ItemIndex;
            this.DataBand7.Page = this.ItemIndex;
            this.DataBand7.Parent = this.ItemIndex;
            this.GroupHeaderBand5.Page = this.ItemIndex;
            this.GroupHeaderBand5.Parent = this.ItemIndex;
            this.DataBand8.MasterComponent = this.DataBand7;
            this.DataBand8.Page = this.ItemIndex;
            this.DataBand8.Parent = this.ItemIndex;
            this.GroupHeaderBand3.Page = this.ItemIndex;
            this.GroupHeaderBand3.Parent = this.ItemIndex;
            this.Text13.Page = this.ItemIndex;
            this.Text13.Parent = this.GroupHeaderBand3;
            this.Text14.Page = this.ItemIndex;
            this.Text14.Parent = this.GroupHeaderBand3;
            this.DataBand6.MasterComponent = this.DataBand8;
            this.DataBand6.Page = this.ItemIndex;
            this.DataBand6.Parent = this.ItemIndex;
            this.EndRender += new System.EventHandler(this.ReportWordsToEnd__EndRender);
            // 
            // Add to Cover.Components
            // 
            this.Cover.Components.Clear();
            this.Cover.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text2});
            // 
            // Add to PageHeaderBand1.Components
            // 
            this.PageHeaderBand1.Components.Clear();
            this.PageHeaderBand1.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text8});
            // 
            // Add to GroupHeaderBand1.Components
            // 
            this.GroupHeaderBand1.Components.Clear();
            this.GroupHeaderBand1.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text4,
                        this.Text6});
            // 
            // Add to GroupHeaderBand2.Components
            // 
            this.GroupHeaderBand2.Components.Clear();
            this.GroupHeaderBand2.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text5,
                        this.Text7});
            // 
            // Add to TOC.Components
            // 
            this.TOC.Components.Clear();
            this.TOC.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.PageHeaderBand1,
                        this.GroupHeaderBand1,
                        this.DataBand4,
                        this.GroupHeaderBand2,
                        this.DataBand5});
            // 
            // Add to PageFooterBand1.Components
            // 
            this.PageFooterBand1.Components.Clear();
            this.PageFooterBand1.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text9,
                        this.Text10,
                        this.Text12,
                        this.HorizontalLinePrimitive1});
            // 
            // Add to DataBand1.Components
            // 
            this.DataBand1.Components.Clear();
            this.DataBand1.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text1,
                        //this.Image1,
                        //this.Image2,
                        //this.Text17,
                        //this.Text302
                        //this.Text17,
                        //this.Text18
                        //this.Text19,
                        //this.Text20,
                        //this.Text21,
                        //this.Text22,
                        //this.Text23,
                        //this.Text24,
                        //this.Text25,
                        //this.Text26,
                        //this.Text27,
                        //this.Text28
            });
            // 
            // Add to DataBand2.Components
            // 
            this.DataBand2.Components.Clear();
            this.DataBand2.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                //this.Text17,
                //        this.Text302,
                        this.Text3});
            // 
            // Add to DataBand20.Components
            // 
            this.DataBand20.Components.Clear();
            this.DataBand20.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text17,
                        this.Text18});
            // 
            // Add to DataBand13.Components
            // 
            // Add to DataBand21.Components
            // 
            this.DataBand21.Components.Clear();
            this.DataBand21.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text19,
                        this.Image1});
            // 
            // Add to DataBand13.Components
            // 
            this.DataBand13.Components.Clear();
            this.DataBand13.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text29,
                        this.Text30});
            // 
            // Add to DataBand14.Components
            // 
            this.DataBand14.Components.Clear();
            this.DataBand14.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text34,
                        this.Image3});
            // 
            // Add to CrossTab1.Components
            // 
            this.CrossTab1.Components.Clear();
            this.CrossTab1.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.CrossTab1_ColTotal1,
                        this.CrossTab1_RowTotal1,
                        this.CrossTab1_Row1_Title,
                        this.CrossTab1_LeftTitle,
                        this.CrossTab1_RightTitle,
                        this.CrossTab1_Row1,
                        this.CrossTab1_Column1,
                        this.CrossTab1_Sum1});
            // 
            // Add to DataBand3.Components
            // 
            this.DataBand3.Components.Clear();
            this.DataBand3.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.CrossTab1});
            // 
            // Add to GroupHeaderBand6.Components
            // 
            this.GroupHeaderBand6.Components.Clear();
            this.GroupHeaderBand6.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text31});
            // 
            // Add to DataBand10.Components
            // 
            this.DataBand10.Components.Clear();
            this.DataBand10.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text15});
            // 
            // Add to CrossTab2.Components
            // 
            this.CrossTab2.Components.Clear();
            this.CrossTab2.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.CrossTab2_ColTotal1,
                        this.CrossTab2_RowTotal1,
                        this.CrossTab2_Row1_Title,
                        this.CrossTab2_LeftTitle,
                        this.CrossTab2_RightTitle,
                        this.CrossTab2_Row1,
                        this.CrossTab2_Column1,
                        this.CrossTab2_Sum1});
            // 
            // Add to DataBand9.Components
            // 
            this.DataBand9.Components.Clear();
            this.DataBand9.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.CrossTab2});
            // 
            // Add to GroupHeaderBand7.Components
            // 
            this.GroupHeaderBand7.Components.Clear();
            this.GroupHeaderBand7.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text32});
            // 
            // Add to DataBand12.Components
            // 
            this.DataBand12.Components.Clear();
            this.DataBand12.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text16});
            // 
            // Add to CrossTab3.Components
            // 
            this.CrossTab3.Components.Clear();
            this.CrossTab3.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.CrossTab3_ColTotal1,
                        this.CrossTab3_RowTotal1,
                        this.CrossTab3_Row1_Title,
                        this.CrossTab3_LeftTitle,
                        this.CrossTab3_RightTitle,
                        this.CrossTab3_Row1,
                        this.CrossTab3_Column1,
                        this.CrossTab3_Sum1});
            // 
            // Add to DataBand11.Components
            // 
            this.DataBand11.Components.Clear();
            this.DataBand11.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.CrossTab3});
            // 
            // Add to Main.Components
            // 
            this.Main.Components.Clear();
            this.Main.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.PageFooterBand1,
                        this.DataBand1,
                          this.DataBand20,
                           this.DataBand21,
                        this.DataBand2,

                        this.DataBand13,
                        this.DataBand14,
                        this.DataBand3,
                        this.GroupHeaderBand6,
                        this.DataBand10,
                        this.DataBand9,
                        this.GroupHeaderBand7,
                        this.DataBand12,
                        this.DataBand11});
            // 
            // Add to PageHeaderBand2.Components
            // 
            this.PageHeaderBand2.Components.Clear();
            this.PageHeaderBand2.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text11});
            // 
            // Add to GroupHeaderBand3.Components
            // 
            this.GroupHeaderBand3.Components.Clear();
            this.GroupHeaderBand3.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.Text13,
                        this.Text14});
            // 
            // Add to ItemIndex.Components
            // 
            this.ItemIndex.Components.Clear();
            this.ItemIndex.Components.AddRange(new Stimulsoft.Report.Components.StiComponent[] {
                        this.PageHeaderBand2,
                        this.GroupHeaderBand4,
                        this.DataBand7,
                        this.GroupHeaderBand5,
                        this.DataBand8,
                        this.GroupHeaderBand3,
                        this.DataBand6});
            // 
            // Add to Pages
            // 
            this.Pages.Clear();
            this.Pages.AddRange(new Stimulsoft.Report.Components.StiPage[] {
                        this.Cover,
                        this.TOC,
                        this.Main,
                        this.ItemIndex});
            this.Dictionary.Relations.Add(this.ParentName);
            this.Dictionary.Relations.Add(this.ParentName1);
            this.Dictionary.Relations.Add(this.ParentName2);
            this.Dictionary.Relations.Add(this.ParentName3);
            this.Dictionary.Relations.Add(this.ParentName4);
            this.Dictionary.Relations.Add(this.ParentName5);
            this.Dictionary.Relations.Add(this.ParentName6);
            this.Dictionary.Relations.Add(this.ParentName11);
            this.TBL1.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CATEGORY_ID", "CATEGORY_ID", "CATEGORY_ID", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CATEGORY_NAME", "CATEGORY_NAME", "CATEGORY_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("PARENT_CATEGORY", "PARENT_CATEGORY", "PARENT_CATEGORY", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("SHORT_DESC", "SHORT_DESC", "SHORT_DESC", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("IMAGE_FILE", "IMAGE_FILE", "IMAGE_FILE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("IMAGE_TYPE", "IMAGE_TYPE", "IMAGE_TYPE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("IMAGE_NAME", "IMAGE_NAME", "IMAGE_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("IMAGE_NAME2", "IMAGE_NAME2", "IMAGE_NAME2", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("IMAGE_FILE2", "IMAGE_FILE2", "IMAGE_FILE2", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("IMAGE_TYPE2", "IMAGE_TYPE2", "IMAGE_TYPE2", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CUSTOM_NUM_FIELD1", "CUSTOM_NUM_FIELD1", "CUSTOM_NUM_FIELD1", typeof(double)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CUSTOM_NUM_FIELD2", "CUSTOM_NUM_FIELD2", "CUSTOM_NUM_FIELD2", typeof(double)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CUSTOM_NUM_FIELD3", "CUSTOM_NUM_FIELD3", "CUSTOM_NUM_FIELD3", typeof(double)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CUSTOM_TEXT_FIELD1", "CUSTOM_TEXT_FIELD1", "CUSTOM_TEXT_FIELD1", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CUSTOM_TEXT_FIELD2", "CUSTOM_TEXT_FIELD2", "CUSTOM_TEXT_FIELD2", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CUSTOM_TEXT_FIELD3", "CUSTOM_TEXT_FIELD3", "CUSTOM_TEXT_FIELD3", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CREATED_USER", "CREATED_USER", "CREATED_USER", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CREATED_DATE", "CREATED_DATE", "CREATED_DATE", typeof(DateTime)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("MODIFIED_USER", "MODIFIED_USER", "MODIFIED_USER", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("MODIFIED_DATE", "MODIFIED_DATE", "MODIFIED_DATE", typeof(DateTime)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("PUBLISH", "PUBLISH", "PUBLISH", typeof(bool)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("PUBLISH2PRINT", "PUBLISH2PRINT", "PUBLISH2PRINT", typeof(bool)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("PUBLISH2CD", "PUBLISH2CD", "PUBLISH2CD", typeof(bool)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("WORKFLOW_STATUS", "WORKFLOW_STATUS", "WORKFLOW_STATUS", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("WORKFLOW_COMMENTS", "WORKFLOW_COMMENTS", "WORKFLOW_COMMENTS", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CLONE_LOCK", "CLONE_LOCK", "CLONE_LOCK", typeof(bool)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("SORT_ORDER", "SORT_ORDER", "SORT_ORDER", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CATALOG_ID", "CATALOG_ID", "CATALOG_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CATALOG_NAME", "CATALOG_NAME", "CATALOG_NAME", typeof(string))
            });
            this.TBL1.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CatalogId", 8, 0),
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CategoryId", 23, 0)});
            this.DataSources.Add(this.TBL1);
            this.TBL2.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                        new Stimulsoft.Report.Dictionary.StiDataColumn("FAMILY_ID", "FAMILY_ID", "FAMILY_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("FAMILY_NAME", "FAMILY_NAME", "FAMILY_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CATEGORY_ID", "CATEGORY_ID", "CATEGORY_ID", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("SORT_ORDER", "SORT_ORDER", "SORT_ORDER", typeof(int))});
            this.TBL2.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CatalogId", 8, 0),
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CategoryId", 23, 0)});
            this.DataSources.Add(this.TBL2);
            this.TBL3.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                        new Stimulsoft.Report.Dictionary.StiDataColumn("STRING_VALUE", "STRING_VALUE", "STRING_VALUE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("PRODUCT_ID", "PRODUCT_ID", "PRODUCT_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_ID", "ATTRIBUTE_ID", "ATTRIBUTE_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_NAME", "ATTRIBUTE_NAME", "ATTRIBUTE_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("FAMILY_ID", "FAMILY_ID", "FAMILY_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_DATATYPE", "ATTRIBUTE_DATATYPE", "ATTRIBUTE_DATATYPE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_DATARULE", "ATTRIBUTE_DATARULE", "ATTRIBUTE_DATARULE", typeof(string))});
            this.TBL3.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CatalogId", 8, 0),
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CategoryId", 23, 0)});
            this.DataSources.Add(this.TBL3);
            this.MultipleTables.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                        new Stimulsoft.Report.Dictionary.StiDataColumn("GROUP_ID", "GROUP_ID", "GROUP_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("GROUP_NAME", "GROUP_NAME", "GROUP_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("STRING_VALUE", "STRING_VALUE", "STRING_VALUE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("PRODUCT_ID", "PRODUCT_ID", "PRODUCT_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_NAME", "ATTRIBUTE_NAME", "ATTRIBUTE_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("SORT_ORDER", "SORT_ORDER", "SORT_ORDER", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("FAMILY_ID", "FAMILY_ID", "FAMILY_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_DATATYPE", "ATTRIBUTE_DATATYPE", "ATTRIBUTE_DATATYPE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_DATARULE", "ATTRIBUTE_DATARULE", "ATTRIBUTE_DATARULE", typeof(string))});
            this.MultipleTables.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CATALOG_ID", 8, 0)});
            this.DataSources.Add(this.MultipleTables);
            this.MultipleTableSoruce.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                        new Stimulsoft.Report.Dictionary.StiDataColumn("GROUP_ID", "GROUP_ID", "GROUP_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("GROUP_NAME", "GROUP_NAME", "GROUP_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("FAMILY_ID", "FAMILY_ID", "FAMILY_ID", typeof(int))});
            this.MultipleTableSoruce.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CATALOG_ID", 8, 0)});
            this.DataSources.Add(this.MultipleTableSoruce);
            this.ReferenceTables.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                        new Stimulsoft.Report.Dictionary.StiDataColumn("FAMILY_ID", "FAMILY_ID", "FAMILY_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("TABLE_ID", "TABLE_ID", "TABLE_ID", typeof(long)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("COLUMN_INDEX", "COLUMN_INDEX", "COLUMN_INDEX", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ROW_INDEX", "ROW_INDEX", "ROW_INDEX", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ROW_VALUE", "ROW_VALUE", "ROW_VALUE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("TABLE_NAME", "TABLE_NAME", "TABLE_NAME", typeof(string))});
            this.ReferenceTables.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CATALOG_ID", 8, 0),
                        new Stimulsoft.Report.Dictionary.StiDataParameter("PATH", 22, 100)});
            this.DataSources.Add(this.ReferenceTables);
            this.ReferenceTableSource.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                        new Stimulsoft.Report.Dictionary.StiDataColumn("FAMILY_ID", "FAMILY_ID", "FAMILY_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("TABLE_ID", "TABLE_ID", "TABLE_ID", typeof(long)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("TABLE_NAME", "TABLE_NAME", "TABLE_NAME", typeof(string))});
            this.ReferenceTableSource.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CATALOG_ID", 23, 0)});
            this.DataSources.Add(this.ReferenceTableSource);
            //--------------------------CategorySpecs-------------------//
            this.CategorySpecs.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                         new Stimulsoft.Report.Dictionary.StiDataColumn("CATEGORY_ID", "CATEGORY_ID", "CATEGORY_ID", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CATEGORY_NAME", "FAMILY_NAME", "FAMILY_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("SORT_ORDER", "SORT_ORDER", "SORT_ORDER", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_ID", "ATTRIBUTE_ID", "ATTRIBUTE_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_NAME", "ATTRIBUTE_NAME", "ATTRIBUTE_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("STRING_VALUE", "STRING_VALUE", "STRING_VALUE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_TYPE", "ATTRIBUTE_TYPE", "ATTRIBUTE_TYPE", typeof(byte)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_DATATYPE", "ATTRIBUTE_DATATYPE", "ATTRIBUTE_DATATYPE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_DATARULE", "ATTRIBUTE_DATARULE", "ATTRIBUTE_DATARULE", typeof(string))});
            this.CategorySpecs.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CatalogId", 23, 0),
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CategoryId", 23, 0)});
            this.DataSources.Add(this.CategorySpecs);
            //--------------------------End------------------------------//
            this.FamilySpecs.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                        new Stimulsoft.Report.Dictionary.StiDataColumn("FAMILY_ID", "FAMILY_ID", "FAMILY_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("FAMILY_NAME", "FAMILY_NAME", "FAMILY_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("CATEGORY_ID", "CATEGORY_ID", "CATEGORY_ID", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("SORT_ORDER", "SORT_ORDER", "SORT_ORDER", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_ID", "ATTRIBUTE_ID", "ATTRIBUTE_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_NAME", "ATTRIBUTE_NAME", "ATTRIBUTE_NAME", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("STRING_VALUE", "STRING_VALUE", "STRING_VALUE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_TYPE", "ATTRIBUTE_TYPE", "ATTRIBUTE_TYPE", typeof(byte)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_DATATYPE", "ATTRIBUTE_DATATYPE", "ATTRIBUTE_DATATYPE", typeof(string)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_DATARULE", "ATTRIBUTE_DATARULE", "ATTRIBUTE_DATARULE", typeof(string))});
            this.FamilySpecs.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CatalogId", 23, 0),
                        new Stimulsoft.Report.Dictionary.StiDataParameter("CategoryId", 23, 0)});
            this.DataSources.Add(this.FamilySpecs);
            this.TBL4.Columns.AddRange(new Stimulsoft.Report.Dictionary.StiDataColumn[] {
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_ID", "ATTRIBUTE_ID", "ATTRIBUTE_ID", typeof(int)),
                        new Stimulsoft.Report.Dictionary.StiDataColumn("ATTRIBUTE_NAME", "ATTRIBUTE_NAME", "ATTRIBUTE_NAME", typeof(string))});
            this.TBL4.Parameters.AddRange(new Stimulsoft.Report.Dictionary.StiDataParameter[] {
                        new Stimulsoft.Report.Dictionary.StiDataParameter("@CatalogId", 8, 0),
                        new Stimulsoft.Report.Dictionary.StiDataParameter("@CategoryId", 23, 0)});
            this.DataSources.Add(this.TBL4);
            this.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("Connection", "Connection", ConnectionString, false));
            this.TBL1.Connecting += new System.EventHandler(this.GetTBL1_SqlCommand);
            this.TBL2.Connecting += new System.EventHandler(this.GetTBL2_SqlCommand);
            this.TBL3.Connecting += new System.EventHandler(this.GetTBL3_SqlCommand);
            this.MultipleTables.Connecting += new System.EventHandler(this.GetMultipleTables_SqlCommand);
            this.MultipleTableSoruce.Connecting += new System.EventHandler(this.GetMultipleTableSoruce_SqlCommand);
            this.ReferenceTables.Connecting += new System.EventHandler(this.GetReferenceTables_SqlCommand);
            this.ReferenceTableSource.Connecting += new System.EventHandler(this.GetReferenceTableSource_SqlCommand);
            this.CategorySpecs.Connecting += new System.EventHandler(this.GetCategorySpecs_SqlCommand);
            this.FamilySpecs.Connecting += new System.EventHandler(this.GetFamilySpecs_SqlCommand);
            this.TBL4.Connecting += new System.EventHandler(this.GetTBL4_SqlCommand);
        }

        public void GetTBL1_SqlCommand(object sender, System.EventArgs e)
        {
            this.TBL1.SqlCommand = "SELECT * FROM category_function(@CatalogId,CONVERT(NVARCHAR(MAX),@CategoryId)) order by HIERARCHY_LEVEL , SORT_ORDER";
            this.TBL1.CommandTimeout = 0;
            this.TBL1.Parameters["CatalogId"].ParameterValue = CatalogId;
            this.TBL1.Parameters["CategoryId"].ParameterValue = CategoryId;
        }

        public void GetTBL2_SqlCommand(object sender, System.EventArgs e)
        {
            this.TBL2.SqlCommand = ";WITH AA AS \r\n(\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\n\tJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId AND A.CATEGORY_ID = @CategoryId\r\n\tJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\n\tUNION ALL\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\n\tJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId\r\n\tJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\n\tJOIN AA ON AA.CATEGORY_ID = A.PARENT_CATEGORY\r\n)\r\n,FAM\r\nAS\r\n(\r\nSELECT A.FAMILY_ID,A.FAMILY_NAME,AA.CATEGORY_ID,B.SORT_ORDER FROM AA JOIN TB_CATALOG_FAMILY B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId \r\nJOIN TB_FAMILY A ON A.FAMILY_ID = B.FAMILY_ID AND PARENT_FAMILY_ID = 0\r\nUNION\r\nSELECT A.FAMILY_ID,A.FAMILY_NAME,AA.CATEGORY_ID,B.SORT_ORDER FROM AA JOIN TB_CATALOG_FAMILY B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId\r\nJOIN TB_FAMILY A ON A.PARENT_FAMILY_ID = B.FAMILY_ID \r\n)\r\nSELECT" +
" A.FAMILY_ID,A.FAMILY_NAME,A.CATEGORY_ID,A.SORT_ORDER  FROM FAM  A INNER JOIN TB_CATALOG_FAMILY CF ON CF.FAMILY_ID=A.FAMILY_ID AND CF.CATEGORY_ID=@CategoryId AND CF.CATALOG_ID=@CatalogId  ORDER BY A.SORT_ORDER ";
            this.TBL2.CommandTimeout = 0;
            this.TBL2.Parameters["CatalogId"].ParameterValue = CatalogId;
            this.TBL2.Parameters["CategoryId"].ParameterValue = CategoryId;
        }

        public void GetTBL3_SqlCommand(object sender, System.EventArgs e)
        {
            this.TBL3.SqlCommand = ";WITH AA AS \r\n(\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\n\tJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId AND A.CATEGORY_ID = @CategoryId\r\n\tJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\n\tUNION ALL\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\n\tJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId\r\n\tJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\n\tJOIN AA ON AA.CATEGORY_ID = A.PARENT_CATEGORY\r\n)\r\n,BB\r\nAS\r\n(\r\nSELECT A.FAMILY_ID,A.FAMILY_NAME,B.CATEGORY_ID,B.SORT_ORDER FROM AA JOIN TB_CATALOG_FAMILY B ON AA.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId\r\n\tJOIN TB_FAMILY A ON A.FAMILY_ID = B.FAMILY_ID\r\n)\r\nSELECT CASE \r\n\t\t\t\t\t\t\tWHEN F.ATTRIBUTE_DATATYPE LIKE \'%Tex%\'\r\n\t\t\t\t\t\t\tTHEN E.STRING_VALUE \r\n\t\t\t\t\t\t\tWHEN F.ATTRIBUTE_DATATYPE LIKE \'%Num%\'\r\n\t\t\t\t\t\t\tTHEN CONVERT(VARCHAR,E.NUMERIC_VALUE)\r\n\t\t\t\t\t\t\tWHEN F.ATTRIBUTE_DATATYPE LIKE \'%Date and Time%\'\r\n\t\t\t\t\t\t\tTHEN" +
" CONVERT(VARCHAR,E.STRING_VALUE)\r\n\t\t\t\t\t\t\tWHEN F.ATTRIBUTE_DATATYPE LIKE \'%Hyperlink%\'\r\n\t\t\t\t\t\t\tTHEN CONVERT(VARCHAR,E.STRING_VALUE)\r\n\t\t\t\t\t\t\tEND AS STRING_VALUE,E.PRODUCT_ID,E.ATTRIBUTE_ID\r\n\t\t\t\t\t\t\t,F.ATTRIBUTE_NAME,BB.FAMILY_ID,F.ATTRIBUTE_DATATYPE, F.ATTRIBUTE_DATARULE, C.SORT_ORDER FROM BB\r\nJOIN TB_PROD_FAMILY C ON C.FAMILY_ID = BB.FAMILY_ID  AND C.PUBLISH = 1 AND C.PUBLISH2PRINT = 1\r\nJOIN TB_PRODUCT D ON D.PRODUCT_ID = C.PRODUCT_ID\r\nJOIN TB_PROD_SPECS E ON E.PRODUCT_ID = D.PRODUCT_ID\r\nJOIN TB_PROD_FAMILY_ATTR_LIST PE ON  PE.ATTRIBUTE_ID = E.ATTRIBUTE_ID AND PE.FAMILY_ID = BB.FAMILY_ID\r\nJOIN TB_ATTRIBUTE F ON F.ATTRIBUTE_ID = E.ATTRIBUTE_ID \r\nUNION \r\nSELECT CASE \r\n\t\t\t\t\t\t\tWHEN F.ATTRIBUTE_DATATYPE LIKE \'%Tex%\'\r\n\t\t\t\t\t\t\tTHEN E.ATTRIBUTE_VALUE \r\n\t\t\t\t\t\t\tWHEN F.ATTRIBUTE_DATATYPE LIKE \'%Num%\'\r\n\t\t\t\t\t\t\tTHEN CONVERT(VARCHAR,E.ATTRIBUTE_VALUE)\r\n\t\t\t\t\t\t\tWHEN F.ATTRIBUTE_DATATYPE LIKE \'%Date and Time%\'\r\n\t\t\t\t\t\t\tTHEN CONVERT(VARCHAR,E.ATTRIBUTE_VALUE)\r\n\t\t\t\t\t\t\tWHEN F.ATTRIBUTE_DATATYPE LIKE " +
"\'%Hyperlink%\'\r\n\t\t\t\t\t\t\tTHEN CONVERT(VARCHAR,E.ATTRIBUTE_VALUE)\r\n\t\t\t\t\t\t\tEND AS STRING_VALUE,E.PRODUCT_ID,E.ATTRIBUTE_ID\r\n\t\t\t\t\t\t\t,F.ATTRIBUTE_NAME,BB.FAMILY_ID,F.ATTRIBUTE_DATATYPE, F.ATTRIBUTE_DATARULE, C.SORT_ORDER FROM BB\r\nJOIN TB_PROD_FAMILY C ON C.FAMILY_ID = BB.FAMILY_ID  AND C.PUBLISH = 1 AND C.PUBLISH2PRINT = 1\r\nJOIN TB_PRODUCT D ON D.PRODUCT_ID = C.PRODUCT_ID\r\nJOIN TB_PARTS_KEY E ON E.PRODUCT_ID = D.PRODUCT_ID AND E.CATALOG_ID = @CatalogId AND E.FAMILY_ID = C.FAMILY_ID\r\nJOIN TB_PROD_FAMILY_ATTR_LIST PE ON PE.ATTRIBUTE_ID = E.ATTRIBUTE_ID AND PE.FAMILY_ID = BB.FAMILY_ID\r\nJOIN TB_ATTRIBUTE F ON F.ATTRIBUTE_ID = E.ATTRIBUTE_ID \r\nORDER BY C.SORT_ORDER,ATTRIBUTE_ID";
            this.TBL3.CommandTimeout = 0;
            this.TBL3.Parameters["CatalogId"].ParameterValue = CatalogId;
            this.TBL3.Parameters["CategoryId"].ParameterValue = CategoryId;
        }

        public void GetMultipleTables_SqlCommand(object sender, System.EventArgs e)
        {
            this.MultipleTables.SqlCommand = ";WITH TEM\r\nAS\r\n(\r\nSELECT DISTINCT K.GROUP_ID,K.GROUP_NAME,\r\n\tCASE WHEN G.ATTRIBUTE_DATATYPE LIKE \'%TEXT%\' THEN F.STRING_VALUE \r\nWHEN G.ATTRIBUTE_DATATYPE LIKE \'%NUM%\'\r\nTHEN CONVERT(VARCHAR(1000),F.NUMERIC_VALUE)\r\nWHEN G.ATTRIBUTE_DATATYPE LIKE \'%Date and Time%\'\r\nTHEN CONVERT(VARCHAR(1000),F.STRING_VALUE)\r\nWHEN G.ATTRIBUTE_DATATYPE LIKE \'%Hyperlink%\' \r\nTHEN CONVERT(VARCHAR(1000),F.STRING_VALUE) END AS STRING_VALUE\r\n,F.PRODUCT_ID,G.ATTRIBUTE_NAME,C.SORT_ORDER,A.FAMILY_ID, G.ATTRIBUTE_DATATYPE, G.ATTRIBUTE_DATARULE\r\n\tFROM TB_FAMILY A \r\nJOIN TB_CATALOG_FAMILY B ON A.FAMILY_ID = B.FAMILY_ID AND B.CATALOG_ID = @CATALOG_ID\r\nJOIN TB_PROD_FAMILY C ON C.FAMILY_ID = A.FAMILY_ID AND C.PUBLISH2PRINT = 1\r\nJOIN TB_PRODUCT D ON D.PRODUCT_ID = C.PRODUCT_ID \r\nJOIN TB_CATALOG_PRODUCT E ON E.PRODUCT_ID = D.PRODUCT_ID\r\nJOIN TB_PROD_SPECS F ON F.PRODUCT_ID = D.PRODUCT_ID\r\nJOIN TB_ATTRIBUTE G ON G.ATTRIBUTE_ID = F.ATTRIBUTE_ID\r\nJOIN TB_CATALOG_ATTRIBUTES H ON H.ATTRIBUTE_ID = G.ATTRIBUTE_ID AND H.CATALOG_ID = @CATALOG_ID\r\nJOIN TB_ATTRIBUTE_G" +
"ROUP_SPECS I ON I.ATTRIBUTE_ID = G.ATTRIBUTE_ID\r\nJOIN TB_ATTRIBUTE_GROUP_SECTIONS J ON J.GROUP_ID = I.GROUP_ID AND J.CATALOG_ID = @CATALOG_ID\r\n\tAND J.FAMILY_ID = A.FAMILY_ID\r\nJOIN TB_ATTRIBUTE_GROUP K ON K.GROUP_ID = J.GROUP_ID\r\nUNION ALL\r\nSELECT K.GROUP_ID,K.GROUP_NAME,ATTRIBUTE_VALUE AS STRING_VALUE\r\n\t,F.PRODUCT_ID,G.ATTRIBUTE_NAME,C.SORT_ORDER,A.FAMILY_ID, G.ATTRIBUTE_DATATYPE, G.ATTRIBUTE_DATARULE\r\n\tFROM TB_FAMILY A \r\nJOIN TB_CATALOG_FAMILY B ON A.FAMILY_ID = B.FAMILY_ID AND B.CATALOG_ID = @CATALOG_ID\r\nJOIN TB_PROD_FAMILY C ON C.FAMILY_ID = A.FAMILY_ID  AND C.PUBLISH2PRINT = 1\r\nJOIN TB_PRODUCT D ON D.PRODUCT_ID = C.PRODUCT_ID \r\nJOIN TB_CATALOG_PRODUCT E ON E.PRODUCT_ID = D.PRODUCT_ID\r\nJOIN TB_PARTS_KEY F ON F.PRODUCT_ID = D.PRODUCT_ID AND F.FAMILY_ID = C.FAMILY_ID\r\nJOIN TB_ATTRIBUTE G ON G.ATTRIBUTE_ID = F.ATTRIBUTE_ID\r\nJOIN TB_CATALOG_ATTRIBUTES H ON H.ATTRIBUTE_ID = G.ATTRIBUTE_ID AND H.CATALOG_ID = @CATALOG_ID\r\nJOIN TB_ATTRIBUTE_GROUP_SPECS I ON I.ATTRIBUTE_ID = G.ATTRIBUTE_ID\r\nJOIN TB_ATTRIBUTE_GROUP_" +
"SECTIONS J ON J.GROUP_ID = I.GROUP_ID AND J.CATALOG_ID = @CATALOG_ID\r\n\tAND J.FAMILY_ID = A.FAMILY_ID\r\nJOIN TB_ATTRIBUTE_GROUP K ON K.GROUP_ID = J.GROUP_ID\r\n)\r\nSELECT * FROM TEM ORDER BY SORT_ORDER\r\n";
            this.MultipleTables.CommandTimeout = 0;
            this.MultipleTables.Parameters["CATALOG_ID"].ParameterValue = CatalogId;
        }

        public void GetMultipleTableSoruce_SqlCommand(object sender, System.EventArgs e)
        {
            this.MultipleTableSoruce.SqlCommand = ";WITH TEM\r\nAS\r\n(\r\nSELECT DISTINCT K.GROUP_ID,K.GROUP_NAME,\r\n\tCASE WHEN G.ATTRIBUTE_DATATYPE LIKE \'%TEXT%\' THEN F.STRING_VALUE \r\nWHEN G.ATTRIBUTE_DATATYPE LIKE \'%NUM%\'\r\nTHEN CONVERT(VARCHAR(1000),F.NUMERIC_VALUE)\r\nWHEN G.ATTRIBUTE_DATATYPE LIKE \'%Date and Time%\'\r\nTHEN CONVERT(VARCHAR(1000),F.STRING_VALUE)\r\nWHEN G.ATTRIBUTE_DATATYPE LIKE \'%Hyperlink%\' \r\nTHEN CONVERT(VARCHAR(1000),F.STRING_VALUE) END AS STRING_VALUE\r\n,F.PRODUCT_ID,G.ATTRIBUTE_NAME,C.SORT_ORDER,A.FAMILY_ID\r\n\tFROM TB_FAMILY A \r\nJOIN TB_CATALOG_FAMILY B ON A.FAMILY_ID = B.FAMILY_ID AND B.CATALOG_ID = @CATALOG_ID\r\nJOIN TB_PROD_FAMILY C ON C.FAMILY_ID = A.FAMILY_ID AND C.PUBLISH2PRINT = 1\r\nJOIN TB_PRODUCT D ON D.PRODUCT_ID = C.PRODUCT_ID \r\nJOIN TB_CATALOG_PRODUCT E ON E.PRODUCT_ID = D.PRODUCT_ID\r\nJOIN TB_PROD_SPECS F ON F.PRODUCT_ID = D.PRODUCT_ID\r\nJOIN TB_ATTRIBUTE G ON G.ATTRIBUTE_ID = F.ATTRIBUTE_ID\r\nJOIN TB_CATALOG_ATTRIBUTES H ON H.ATTRIBUTE_ID = G.ATTRIBUTE_ID AND H.CATALOG_ID = @CATALOG_ID\r\nJOIN TB_ATTRIBUTE_GROUP_SPECS I ON I.ATTRIBUTE_ID = G.ATTRIBUTE" +
"_ID\r\nJOIN TB_ATTRIBUTE_GROUP_SECTIONS J ON J.GROUP_ID = I.GROUP_ID AND J.CATALOG_ID = @CATALOG_ID\r\n\tAND J.FAMILY_ID = A.FAMILY_ID\r\nJOIN TB_ATTRIBUTE_GROUP K ON K.GROUP_ID = J.GROUP_ID\r\nUNION ALL\r\nSELECT K.GROUP_ID,K.GROUP_NAME,ATTRIBUTE_VALUE AS STRING_VALUE\r\n\t,F.PRODUCT_ID,G.ATTRIBUTE_NAME,C.SORT_ORDER,A.FAMILY_ID\r\n\tFROM TB_FAMILY A \r\nJOIN TB_CATALOG_FAMILY B ON A.FAMILY_ID = B.FAMILY_ID AND B.CATALOG_ID = @CATALOG_ID\r\nJOIN TB_PROD_FAMILY C ON C.FAMILY_ID = A.FAMILY_ID  AND C.PUBLISH2PRINT = 1\r\nJOIN TB_PRODUCT D ON D.PRODUCT_ID = C.PRODUCT_ID \r\nJOIN TB_CATALOG_PRODUCT E ON E.PRODUCT_ID = D.PRODUCT_ID\r\nJOIN TB_PARTS_KEY F ON F.PRODUCT_ID = D.PRODUCT_ID AND F.FAMILY_ID = C.FAMILY_ID\r\nJOIN TB_ATTRIBUTE G ON G.ATTRIBUTE_ID = F.ATTRIBUTE_ID\r\nJOIN TB_CATALOG_ATTRIBUTES H ON H.ATTRIBUTE_ID = G.ATTRIBUTE_ID AND H.CATALOG_ID = @CATALOG_ID\r\nJOIN TB_ATTRIBUTE_GROUP_SPECS I ON I.ATTRIBUTE_ID = G.ATTRIBUTE_ID\r\nJOIN TB_ATTRIBUTE_GROUP_SECTIONS J ON J.GROUP_ID = I.GROUP_ID AND J.CATALOG_ID = @CATALOG_ID\r\n\tAND J.FAMILY_ID =" +
" A.FAMILY_ID\r\nJOIN TB_ATTRIBUTE_GROUP K ON K.GROUP_ID = J.GROUP_ID\r\n)\r\nSELECT DISTINCT GROUP_ID,GROUP_NAME,FAMILY_ID FROM TEM\r\n";
            this.MultipleTableSoruce.CommandTimeout = 0;
            this.MultipleTableSoruce.Parameters["CATALOG_ID"].ParameterValue = CatalogId;
        }

        public void GetReferenceTables_SqlCommand(object sender, System.EventArgs e)
        {
            this.ReferenceTables.SqlCommand = "SELECT FAMILY_ID,A.TABLE_ID,B.COLUMN_INDEX,C.ROW_INDEX,CASE WHEN C.ROW_VALUE LIKE \'%.JPG%\' THEN @PATH + C.ROW_VALUE \r\nELSE C.ROW_VALUE END AS ROW_VALUE,A.TABLE_NAME\r\n\tFROM TB_REFERENCE_TABLE A \r\n\t\t\t\t\t\tJOIN TB_REFERENCE_TABLE_COLUMNS B ON A.TABLE_ID = B.TABLE_ID\r\n\t\t\t\t\t\tJOIN TB_REFERENCE_TABLE_ROWS C ON C.COLUMN_ID = B.COLUMN_ID\r\n\t\t\t\t\t\tJOIN TB_REFERENCE_SECTIONS D ON D.TABLE_ID = A.TABLE_ID AND CATALOG_ID = @CATALOG_ID";
            this.ReferenceTables.CommandTimeout = 0;
            this.ReferenceTables.Parameters["CATALOG_ID"].ParameterValue = CatalogId;
            //this.ReferenceTables.Parameters["PATH"].ParameterValue = ImagePath;
            this.ReferenceTables.Parameters["PATH"].ParameterValue = "";
        }

        public void GetReferenceTableSource_SqlCommand(object sender, System.EventArgs e)
        {
            this.ReferenceTableSource.SqlCommand = "SELECT DISTINCT FAMILY_ID,A.TABLE_ID,A.TABLE_NAME\r\n\tFROM TB_REFERENCE_TABLE A \r\n\t\t\t\t\t\tJOIN TB_REFERENCE_TABLE_COLUMNS B ON A.TABLE_ID = B.TABLE_ID\r\n\t\t\t\t\t\tJOIN TB_REFERENCE_TABLE_ROWS C ON C.COLUMN_ID = B.COLUMN_ID\r\n\t\t\t\t\t\tJOIN TB_REFERENCE_SECTIONS D ON D.TABLE_ID = A.TABLE_ID AND CATALOG_ID = @CATALOG_ID";
            this.ReferenceTableSource.CommandTimeout = 0;
            this.ReferenceTableSource.Parameters["CATALOG_ID"].ParameterValue = CatalogId;
        }
        //-------------------CategorySpecs-------------------//
        public void GetCategorySpecs_SqlCommand(object sender, System.EventArgs e)
        {
            this.CategorySpecs.SqlCommand = ";WITH AA AS \r\n(\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\nJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\nAND B.CATALOG_ID = @CatalogId AND A.CATEGORY_ID = @CategoryId\r\nJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\nUNION ALL\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\nJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\nAND B.CATALOG_ID = @CatalogId\r\nJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\nJOIN AA ON AA.CATEGORY_ID = A.PARENT_CATEGORY\r\nwhere A.PARENT_CATEGORY = '0')" +
            "SELECT AA.CATEGORY_NAME,B.CATEGORY_ID,B.SORT_ORDER,C.ATTRIBUTE_ID,D.ATTRIBUTE_NAME,C.STRING_VALUE,ATTRIBUTE_TYPE,\r\nD.ATTRIBUTE_DATATYPE, D.ATTRIBUTE_DATARULE\r\nFROM AA \r\nJOIN TB_CATALOG_SECTIONS B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId\r\nJOIN TB_CATEGORY_SPECS C ON C.CATEGORY_ID = AA.CATEGORY_ID\r\nJOIN TB_ATTRIBUTE D ON D.ATTRIBUTE_ID = C.ATTRIBUTE_ID AND (D.ATTRIBUTE_DATATYPE LIKE '%TEXT%' or D.ATTRIBUTE_DATATYPE LIKE '%DATE%' or D.ATTRIBUTE_DATATYPE LIKE '%HYPER%') \r\nUNION\r\nSELECT AA.CATEGORY_NAME,B.CATEGORY_ID,B.SORT_ORDER,C.ATTRIBUTE_ID,D.ATTRIBUTE_NAME,CONVERT(VARCHAR,C.NUMERIC_VALUE),ATTRIBUTE_TYPE, \r\nD.ATTRIBUTE_DATATYPE , D.ATTRIBUTE_DATARULE\r\nFROM AA \r\nJOIN TB_CATALOG_SECTIONS B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId\r\nJOIN TB_CATEGORY_SPECS C ON C.CATEGORY_ID = AA.CATEGORY_ID\r\nJOIN TB_ATTRIBUTE D ON D.ATTRIBUTE_ID = C.ATTRIBUTE_ID AND D.ATTRIBUTE_DATATYPE LIKE '%NUM%'\r\nUNION" +
"\r\nSELECT AA.CATEGORY_NAME,B.CATEGORY_ID,B.SORT_ORDER,C.ATTRIBUTE_ID,D.ATTRIBUTE_NAME,C.ATTRIBUTE_VALUE,ATTRIBUTE_TYPE,\r\nD.ATTRIBUTE_DATATYPE, D.ATTRIBUTE_DATARULE\r\nFROM AA \r\nJOIN TB_CATALOG_SECTIONS B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId\r\nJOIN TB_CATEGORY_KEY C ON C.CATEGORY_ID = AA.CATEGORY_ID AND C.CATALOG_ID = B.CATALOG_ID\r\nJOIN TB_ATTRIBUTE D ON D.ATTRIBUTE_ID = C.ATTRIBUTE_ID AND (D.ATTRIBUTE_DATATYPE LIKE '%TEXT%' or D.ATTRIBUTE_DATATYPE LIKE '%DATE%' or D.ATTRIBUTE_DATATYPE LIKE '%HYPER%') \r\nUNION\r\nSELECT AA.CATEGORY_NAME,B.CATEGORY_ID,B.SORT_ORDER,C.ATTRIBUTE_ID,D.ATTRIBUTE_NAME,CONVERT(VARCHAR,C.ATTRIBUTE_VALUE),ATTRIBUTE_TYPE,\r\nD.ATTRIBUTE_DATATYPE, D.ATTRIBUTE_DATARULE\r\nFROM AA \r\nJOIN TB_CATALOG_SECTIONS B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId\r\nJOIN TB_CATEGORY_KEY C ON C.CATEGORY_ID = AA.CATEGORY_ID AND C.CATALOG_ID = B.CATALOG_ID\r\n--JOIN TB_CATEGORY_FAMILY_ATTR_LIST CA ON CA.FAMILY_ID = A.FAMILY_ID AND CA.ATTRIBUTE_ID = C.ATTRIBUTE_ID\r\nJOIN TB_ATTRIBUTE D ON D.ATTRIBUTE_ID = C.ATTRIBUTE_ID AND D.ATTRIBUTE_DATATYPE LIKE '%NUM%'\r\nORDER BY D.ATTRIBUTE_TYPE";
            this.CategorySpecs.CommandTimeout = 0;
            this.CategorySpecs.Parameters["CatalogId"].ParameterValue = CatalogId;
            this.CategorySpecs.Parameters["CategoryId"].ParameterValue = CategoryId;
        }

        public void GetFamilySpecs_SqlCommand(object sender, System.EventArgs e)
        {
            this.FamilySpecs.SqlCommand = ";WITH AA AS \r\n(\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\n\tJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId AND A.CATEGORY_ID = @CategoryId\r\n\tJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\n\tUNION ALL\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\n\tJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId\r\n\tJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\n\tJOIN AA ON AA.CATEGORY_ID = A.PARENT_CATEGORY\r\n)\r\nSELECT A.FAMILY_ID,A.FAMILY_NAME,B.CATEGORY_ID,B.SORT_ORDER,C.ATTRIBUTE_ID,D.ATTRIBUTE_NAME,C.STRING_VALUE,ATTRIBUTE_TYPE,\r\n\tD.ATTRIBUTE_DATATYPE, D.ATTRIBUTE_DATARULE\r\n\tFROM AA \r\nJOIN TB_CATALOG_FAMILY B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId\r\nJOIN TB_FAMILY A ON A.FAMILY_ID = B.FAMILY_ID\r\nJOIN TB_FAMILY_SPECS C ON C.FAMILY_ID = A.FAMILY_ID\r\nJOIN TB_CATEGORY_FAMILY_ATTR_LIST CA ON CA.FAMILY_ID = A.FAMILY_ID AND CA.ATTRIBUTE_ID = C.ATTRIBUTE_ID\r\n" +
"JOIN TB_ATTRIBUTE D ON D.ATTRIBUTE_ID = C.ATTRIBUTE_ID AND (D.ATTRIBUTE_DATATYPE LIKE \'%TEXT%\' or D.ATTRIBUTE_DATATYPE LIKE \'%DATE%\' or D.ATTRIBUTE_DATATYPE LIKE \'%HYPER%\') \r\nUNION\r\nSELECT A.FAMILY_ID,A.FAMILY_NAME,B.CATEGORY_ID,B.SORT_ORDER,C.ATTRIBUTE_ID,D.ATTRIBUTE_NAME,CONVERT(VARCHAR,C.NUMERIC_VALUE),ATTRIBUTE_TYPE, \r\n\tD.ATTRIBUTE_DATATYPE , D.ATTRIBUTE_DATARULE\r\n\tFROM AA \r\nJOIN TB_CATALOG_FAMILY B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId\r\nJOIN TB_FAMILY A ON A.FAMILY_ID = B.FAMILY_ID\r\nJOIN TB_FAMILY_SPECS C ON C.FAMILY_ID = A.FAMILY_ID\r\nJOIN TB_CATEGORY_FAMILY_ATTR_LIST CA ON CA.FAMILY_ID = A.FAMILY_ID AND CA.ATTRIBUTE_ID = C.ATTRIBUTE_ID\r\nJOIN TB_ATTRIBUTE D ON D.ATTRIBUTE_ID = C.ATTRIBUTE_ID AND D.ATTRIBUTE_DATATYPE LIKE \'%NUM%\'\r\nUNION\r\nSELECT A.FAMILY_ID,A.FAMILY_NAME,B.CATEGORY_ID,B.SORT_ORDER,C.ATTRIBUTE_ID,D.ATTRIBUTE_NAME,C.ATTRIBUTE_VALUE,ATTRIBUTE_TYPE,\r\n\tD.ATTRIBUTE_DATATYPE, D.ATTRIBUTE_DATARULE\r\n\tFROM AA \r\nJOIN TB_CATALOG_FAMILY B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId\r\nJOIN TB_FAMILY A ON A.FAMILY_ID = B.FAMILY_ID\r\nJOIN T" +
"B_FAMILY_KEY C ON C.FAMILY_ID = A.FAMILY_ID AND C.CATALOG_ID = B.CATALOG_ID\r\nJOIN TB_CATEGORY_FAMILY_ATTR_LIST CA ON CA.FAMILY_ID = A.FAMILY_ID AND CA.ATTRIBUTE_ID = C.ATTRIBUTE_ID\r\nJOIN TB_ATTRIBUTE D ON D.ATTRIBUTE_ID = C.ATTRIBUTE_ID AND (D.ATTRIBUTE_DATATYPE LIKE \'%TEXT%\' or D.ATTRIBUTE_DATATYPE LIKE \'%DATE%\' or D.ATTRIBUTE_DATATYPE LIKE \'%HYPER%\') \r\nUNION\r\nSELECT A.FAMILY_ID,A.FAMILY_NAME,B.CATEGORY_ID,B.SORT_ORDER,C.ATTRIBUTE_ID,D.ATTRIBUTE_NAME,CONVERT(VARCHAR,C.ATTRIBUTE_VALUE),ATTRIBUTE_TYPE,\r\n\tD.ATTRIBUTE_DATATYPE, D.ATTRIBUTE_DATARULE\r\n\tFROM AA \r\nJOIN TB_CATALOG_FAMILY B ON AA.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CatalogId\r\nJOIN TB_FAMILY A ON A.FAMILY_ID = B.FAMILY_ID\r\nJOIN TB_FAMILY_KEY C ON C.FAMILY_ID = A.FAMILY_ID AND C.CATALOG_ID = B.CATALOG_ID\r\nJOIN TB_CATEGORY_FAMILY_ATTR_LIST CA ON CA.FAMILY_ID = A.FAMILY_ID AND CA.ATTRIBUTE_ID = C.ATTRIBUTE_ID\r\nJOIN TB_ATTRIBUTE D ON D.ATTRIBUTE_ID = C.ATTRIBUTE_ID AND D.ATTRIBUTE_DATATYPE LIKE \'%NUM%\'\r\nORDER BY D.ATTRIBUTE_TYPE\r\n";
            this.FamilySpecs.CommandTimeout = 0;
            this.FamilySpecs.Parameters["CatalogId"].ParameterValue = CatalogId;
            this.FamilySpecs.Parameters["CategoryId"].ParameterValue = CategoryId;
        }

        public void GetTBL4_SqlCommand(object sender, System.EventArgs e)
        {
            this.TBL4.SqlCommand = ";WITH AA AS \r\n(\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\n\tJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId AND A.CATEGORY_ID = @CategoryId\r\n\tJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\n\tUNION ALL\r\nSELECT A.*,B.SORT_ORDER,C.CATALOG_ID,C.CATALOG_NAME FROM TB_CATEGORY A \r\n\tJOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId\r\n\tJOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID\r\n\tJOIN AA ON AA.CATEGORY_ID = A.PARENT_CATEGORY\r\n)\r\n,BB\r\nAS\r\n(\r\nSELECT A.FAMILY_ID,A.FAMILY_NAME,B.CATEGORY_ID,B.SORT_ORDER FROM AA JOIN TB_CATALOG_FAMILY B ON AA.CATEGORY_ID = B.CATEGORY_ID \r\n\tAND B.CATALOG_ID = @CatalogId\r\n\tJOIN TB_FAMILY A ON A.FAMILY_ID = B.FAMILY_ID\r\n)\r\nSELECT DISTINCT E.ATTRIBUTE_ID,F.ATTRIBUTE_NAME FROM BB\r\nJOIN TB_PROD_FAMILY C ON C.FAMILY_ID = BB.FAMILY_ID\r\nJOIN TB_PRODUCT D ON D.PRODUCT_ID = C.PRODUCT_ID\r\nJOIN TB_PROD_SPECS E ON E.PRODUCT_ID = D.PRODUCT_ID\r\nJOIN TB_ATTRIBUTE F ON F.ATTRIBUTE_ID " +
"= E.ATTRIBUTE_ID";
            this.TBL4.CommandTimeout = 0;
            this.TBL4.Parameters["@CatalogId"].ParameterValue = CatalogId;
            this.TBL4.Parameters["@CategoryId"].ParameterValue = CategoryId;
        }

        #region Relation ParentName
        public class ParentNameRelation : Stimulsoft.Report.Dictionary.StiDataRow
        {

            public ParentNameRelation(Stimulsoft.Report.Dictionary.StiDataRow dataRow) :
                base(dataRow)
            {
            }

            public virtual string CATEGORY_ID
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_ID"], typeof(string), true)));
                }
            }

            public virtual string CATEGORY_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_NAME"], typeof(string), true)));
                }
            }

            public virtual string PARENT_CATEGORY
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["PARENT_CATEGORY"], typeof(string), true)));
                }
            }

            public virtual string SHORT_DESC
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["SHORT_DESC"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_FILE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_FILE"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_TYPE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_TYPE"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_NAME"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_NAME2
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_NAME2"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_FILE2
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_FILE2"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_TYPE2
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_TYPE2"], typeof(string), true)));
                }
            }

            public virtual double CUSTOM_NUM_FIELD1
            {
                get
                {
                    return ((double)(StiReport.ChangeType(this["CUSTOM_NUM_FIELD1"], typeof(double), true)));
                }
            }

            public virtual double CUSTOM_NUM_FIELD2
            {
                get
                {
                    return ((double)(StiReport.ChangeType(this["CUSTOM_NUM_FIELD2"], typeof(double), true)));
                }
            }

            public virtual double CUSTOM_NUM_FIELD3
            {
                get
                {
                    return ((double)(StiReport.ChangeType(this["CUSTOM_NUM_FIELD3"], typeof(double), true)));
                }
            }

            public virtual string CUSTOM_TEXT_FIELD1
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CUSTOM_TEXT_FIELD1"], typeof(string), true)));
                }
            }

            public virtual string CUSTOM_TEXT_FIELD2
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CUSTOM_TEXT_FIELD2"], typeof(string), true)));
                }
            }

            public virtual string CUSTOM_TEXT_FIELD3
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CUSTOM_TEXT_FIELD3"], typeof(string), true)));
                }
            }

            public virtual string CREATED_USER
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CREATED_USER"], typeof(string), true)));
                }
            }

            public virtual DateTime CREATED_DATE
            {
                get
                {
                    return ((DateTime)(StiReport.ChangeType(this["CREATED_DATE"], typeof(DateTime), true)));
                }
            }

            public virtual string MODIFIED_USER
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["MODIFIED_USER"], typeof(string), true)));
                }
            }

            public virtual DateTime MODIFIED_DATE
            {
                get
                {
                    return ((DateTime)(StiReport.ChangeType(this["MODIFIED_DATE"], typeof(DateTime), true)));
                }
            }

            public virtual bool PUBLISH
            {
                get
                {
                    return ((bool)(StiReport.ChangeType(this["PUBLISH"], typeof(bool), true)));
                }
            }

            public virtual bool PUBLISH2PRINT
            {
                get
                {
                    return ((bool)(StiReport.ChangeType(this["PUBLISH2PRINT"], typeof(bool), true)));
                }
            }

            public virtual bool PUBLISH2CD
            {
                get
                {
                    return ((bool)(StiReport.ChangeType(this["PUBLISH2CD"], typeof(bool), true)));
                }
            }

            public virtual int WORKFLOW_STATUS
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["WORKFLOW_STATUS"], typeof(int), true)));
                }
            }

            public virtual string WORKFLOW_COMMENTS
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["WORKFLOW_COMMENTS"], typeof(string), true)));
                }
            }

            public virtual bool CLONE_LOCK
            {
                get
                {
                    return ((bool)(StiReport.ChangeType(this["CLONE_LOCK"], typeof(bool), true)));
                }
            }

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public virtual int CATALOG_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["CATALOG_ID"], typeof(int), true)));
                }
            }

            public virtual string CATALOG_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATALOG_NAME"], typeof(string), true)));
                }
            }
        }
        #endregion Relation ParentName

        #region Relation ParentName1
        public class ParentName1Relation : Stimulsoft.Report.Dictionary.StiDataRow
        {

            public ParentName1Relation(Stimulsoft.Report.Dictionary.StiDataRow dataRow) :
                base(dataRow)
            {
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual string FAMILY_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["FAMILY_NAME"], typeof(string), true)));
                }
            }

            public virtual string CATEGORY_ID
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_ID"], typeof(string), true)));
                }
            }

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation"));
                }
            }
        }
        #endregion Relation ParentName1

        #region Relation ParentName2
        public class ParentName2Relation : Stimulsoft.Report.Dictionary.StiDataRow
        {

            public ParentName2Relation(Stimulsoft.Report.Dictionary.StiDataRow dataRow) :
                base(dataRow)
            {
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual string FAMILY_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["FAMILY_NAME"], typeof(string), true)));
                }
            }

            public virtual string CATEGORY_ID
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_ID"], typeof(string), true)));
                }
            }

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation"));
                }
            }
        }
        #endregion Relation ParentName2

        #region Relation ParentName3
        public class ParentName3Relation : Stimulsoft.Report.Dictionary.StiDataRow
        {

            public ParentName3Relation(Stimulsoft.Report.Dictionary.StiDataRow dataRow) :
                base(dataRow)
            {
            }

            public virtual int GROUP_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["GROUP_ID"], typeof(int), true)));
                }
            }

            public virtual string GROUP_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["GROUP_NAME"], typeof(string), true)));
                }
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation4"));
                }
            }
        }
        #endregion Relation ParentName3

        #region Relation ParentName4
        public class ParentName4Relation : Stimulsoft.Report.Dictionary.StiDataRow
        {

            public ParentName4Relation(Stimulsoft.Report.Dictionary.StiDataRow dataRow) :
                base(dataRow)
            {
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual long TABLE_ID
            {
                get
                {
                    return ((long)(StiReport.ChangeType(this["TABLE_ID"], typeof(long), true)));
                }
            }

            public virtual string TABLE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["TABLE_NAME"], typeof(string), true)));
                }
            }

            public virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation6"));
                }
            }
        }
        #endregion Relation ParentName4

        #region Relation ParentName5
        public class ParentName5Relation : Stimulsoft.Report.Dictionary.StiDataRow
        {

            public ParentName5Relation(Stimulsoft.Report.Dictionary.StiDataRow dataRow) :
                base(dataRow)
            {
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual string FAMILY_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["FAMILY_NAME"], typeof(string), true)));
                }
            }

            public virtual string CATEGORY_ID
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_ID"], typeof(string), true)));
                }
            }

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation"));
                }
            }
        }
        #endregion Relation ParentName5

        #region Relation ParentName6
        public class ParentName6Relation : Stimulsoft.Report.Dictionary.StiDataRow
        {

            public ParentName6Relation(Stimulsoft.Report.Dictionary.StiDataRow dataRow) :
                base(dataRow)
            {
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual string FAMILY_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["FAMILY_NAME"], typeof(string), true)));
                }
            }

            public virtual string CATEGORY_ID
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_ID"], typeof(string), true)));
                }
            }

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation"));
                }
            }
        }
        #endregion Relation ParentName6

        #region Relation ParentName11
        public class ParentName11Relation : Stimulsoft.Report.Dictionary.StiDataRow
        {

            public ParentName11Relation(Stimulsoft.Report.Dictionary.StiDataRow dataRow) :
                base(dataRow)
            {
            }

            public virtual int ATTRIBUTE_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["ATTRIBUTE_ID"], typeof(int), true)));
                }
            }

            public virtual string ATTRIBUTE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_NAME"], typeof(string), true)));
                }
            }
        }
        #endregion Relation ParentName11

        #region DataSource TBL1
        public class TBL1DataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public TBL1DataSource() :
                base("Connection", "TBL1", "TBL1", "", true, false, 30)
            {
            }

            public virtual string CATEGORY_ID
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_ID"], typeof(string), true)));
                }
            }

            public virtual string CATEGORY_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_NAME"], typeof(string), true)));
                }
            }

            public virtual string PARENT_CATEGORY
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["PARENT_CATEGORY"], typeof(string), true)));
                }
            }

            public virtual string SHORT_DESC
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["SHORT_DESC"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_FILE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_FILE"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_TYPE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_TYPE"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_NAME"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_NAME2
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_NAME2"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_FILE2
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_FILE2"], typeof(string), true)));
                }
            }

            public virtual string IMAGE_TYPE2
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["IMAGE_TYPE2"], typeof(string), true)));
                }
            }

            public virtual double CUSTOM_NUM_FIELD1
            {
                get
                {
                    return ((double)(StiReport.ChangeType(this["CUSTOM_NUM_FIELD1"], typeof(double), true)));
                }
            }

            public virtual double CUSTOM_NUM_FIELD2
            {
                get
                {
                    return ((double)(StiReport.ChangeType(this["CUSTOM_NUM_FIELD2"], typeof(double), true)));
                }
            }

            public virtual double CUSTOM_NUM_FIELD3
            {
                get
                {
                    return ((double)(StiReport.ChangeType(this["CUSTOM_NUM_FIELD3"], typeof(double), true)));
                }
            }

            public virtual string CUSTOM_TEXT_FIELD1
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CUSTOM_TEXT_FIELD1"], typeof(string), true)));
                }
            }

            public virtual string CUSTOM_TEXT_FIELD2
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CUSTOM_TEXT_FIELD2"], typeof(string), true)));
                }
            }

            public virtual string CUSTOM_TEXT_FIELD3
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CUSTOM_TEXT_FIELD3"], typeof(string), true)));
                }
            }

            public virtual string CREATED_USER
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CREATED_USER"], typeof(string), true)));
                }
            }

            public virtual DateTime CREATED_DATE
            {
                get
                {
                    return ((DateTime)(StiReport.ChangeType(this["CREATED_DATE"], typeof(DateTime), true)));
                }
            }

            public virtual string MODIFIED_USER
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["MODIFIED_USER"], typeof(string), true)));
                }
            }

            public virtual DateTime MODIFIED_DATE
            {
                get
                {
                    return ((DateTime)(StiReport.ChangeType(this["MODIFIED_DATE"], typeof(DateTime), true)));
                }
            }

            public virtual bool PUBLISH
            {
                get
                {
                    return ((bool)(StiReport.ChangeType(this["PUBLISH"], typeof(bool), true)));
                }
            }

            public virtual bool PUBLISH2PRINT
            {
                get
                {
                    return ((bool)(StiReport.ChangeType(this["PUBLISH2PRINT"], typeof(bool), true)));
                }
            }

            public virtual bool PUBLISH2CD
            {
                get
                {
                    return ((bool)(StiReport.ChangeType(this["PUBLISH2CD"], typeof(bool), true)));
                }
            }

            public virtual int WORKFLOW_STATUS
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["WORKFLOW_STATUS"], typeof(int), true)));
                }
            }

            public virtual string WORKFLOW_COMMENTS
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["WORKFLOW_COMMENTS"], typeof(string), true)));
                }
            }

            public virtual bool CLONE_LOCK
            {
                get
                {
                    return ((bool)(StiReport.ChangeType(this["CLONE_LOCK"], typeof(bool), true)));
                }
            }

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public virtual int CATALOG_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["CATALOG_ID"], typeof(int), true)));
                }
            }

            public virtual string CATALOG_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATALOG_NAME"], typeof(string), true)));
                }
            }
        }
        #endregion DataSource TBL1

        #region DataSource TBL2
        public class TBL2DataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public TBL2DataSource() :
                base("Connection", "TBL2", "TBL2", "", true, false, 30)
            {
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual string FAMILY_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["FAMILY_NAME"], typeof(string), true)));
                }
            }

            public virtual string CATEGORY_ID
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_ID"], typeof(string), true)));
                }
            }

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public new virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation"));
                }
            }
        }
        #endregion DataSource TBL2

        #region DataSource TBL3
        public class TBL3DataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public TBL3DataSource() :
                base("Connection", "TBL3", "TBL3", "", true, false, 30)
            {
            }

            public virtual string STRING_VALUE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["STRING_VALUE"], typeof(string), true)));
                }
            }

            public virtual int PRODUCT_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["PRODUCT_ID"], typeof(int), true)));
                }
            }

            public virtual int ATTRIBUTE_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["ATTRIBUTE_ID"], typeof(int), true)));
                }
            }

            public virtual string ATTRIBUTE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_NAME"], typeof(string), true)));
                }
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual string ATTRIBUTE_DATATYPE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_DATATYPE"], typeof(string), true)));
                }
            }

            public virtual string ATTRIBUTE_DATARULE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_DATARULE"], typeof(string), true)));
                }
            }

            public new virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation2"));
                }
            }

            public virtual ParentName1Relation Name1
            {
                get
                {
                    return new ParentName1Relation(this.GetParentData("Relation8"));
                }
            }
        }
        #endregion DataSource TBL3

        #region DataSource MultipleTables
        public class MultipleTablesDataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public MultipleTablesDataSource() :
                base("Connection", "MultipleTables", "MultipleTables", "", true, false, 30)
            {
            }

            public virtual int GROUP_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["GROUP_ID"], typeof(int), true)));
                }
            }

            public virtual string GROUP_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["GROUP_NAME"], typeof(string), true)));
                }
            }

            public virtual string STRING_VALUE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["STRING_VALUE"], typeof(string), true)));
                }
            }

            public virtual int PRODUCT_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["PRODUCT_ID"], typeof(int), true)));
                }
            }

            public virtual string ATTRIBUTE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_NAME"], typeof(string), true)));
                }
            }

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual string ATTRIBUTE_DATATYPE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_DATATYPE"], typeof(string), true)));
                }
            }

            public virtual string ATTRIBUTE_DATARULE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_DATARULE"], typeof(string), true)));
                }
            }

            public new virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation3"));
                }
            }
        }
        #endregion DataSource MultipleTables

        #region DataSource MultipleTableSoruce
        public class MultipleTableSoruceDataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public MultipleTableSoruceDataSource() :
                base("Connection", "MultipleTableSoruce", "MultipleTableSoruce", "", true, false, 30)
            {
            }

            public virtual int GROUP_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["GROUP_ID"], typeof(int), true)));
                }
            }

            public virtual string GROUP_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["GROUP_NAME"], typeof(string), true)));
                }
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public new virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation4"));
                }
            }
        }
        #endregion DataSource MultipleTableSoruce

        #region DataSource ReferenceTables
        public class ReferenceTablesDataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public ReferenceTablesDataSource() :
                base("Connection", "ReferenceTables", "ReferenceTables", "", true, false, 30)
            {
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual long TABLE_ID
            {
                get
                {
                    return ((long)(StiReport.ChangeType(this["TABLE_ID"], typeof(long), true)));
                }
            }

            public virtual int COLUMN_INDEX
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["COLUMN_INDEX"], typeof(int), true)));
                }
            }

            public virtual int ROW_INDEX
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["ROW_INDEX"], typeof(int), true)));
                }
            }

            public virtual string ROW_VALUE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ROW_VALUE"], typeof(string), true)));
                }
            }

            public virtual string TABLE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["TABLE_NAME"], typeof(string), true)));
                }
            }

            public new virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation5"));
                }
            }
        }
        #endregion DataSource ReferenceTables

        #region DataSource ReferenceTableSource
        public class ReferenceTableSourceDataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public ReferenceTableSourceDataSource() :
                base("Connection", "ReferenceTableSource", "ReferenceTableSource", "", true, false, 30)
            {
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual long TABLE_ID
            {
                get
                {
                    return ((long)(StiReport.ChangeType(this["TABLE_ID"], typeof(long), true)));
                }
            }

            public virtual string TABLE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["TABLE_NAME"], typeof(string), true)));
                }
            }

            public new virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation6"));
                }
            }
        }
        #endregion DataSource ReferenceTableSource
        #region DataSource CategorySpecs
        public class CategorySpecsDataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public CategorySpecsDataSource() :
                base("Connection", "CategorySpecs", "CategorySpecs", "", true, false, 30)
            {
            }

            public virtual string CATEGORY_ID
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_ID"], typeof(string), true)));
                }
            }

            public virtual string CATEGORY_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_NAME"], typeof(string), true)));
                }
            }

            //public virtual string PARENT_CATEGORY
            //{
            //    get
            //    {
            //        return ((string)(StiReport.ChangeType(this["PARENT_CATEGORY"], typeof(string), true)));
            //    }
            //}

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public virtual int ATTRIBUTE_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["ATTRIBUTE_ID"], typeof(int), true)));
                }
            }

            public virtual string ATTRIBUTE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_NAME"], typeof(string), true)));
                }
            }

            public virtual string STRING_VALUE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["STRING_VALUE"], typeof(string), true)));
                }
            }

            public virtual byte ATTRIBUTE_TYPE
            {
                get
                {
                    return ((byte)(StiReport.ChangeType(this["ATTRIBUTE_TYPE"], typeof(byte), true)));
                }
            }

            public virtual string ATTRIBUTE_DATATYPE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_DATATYPE"], typeof(string), true)));
                }
            }

            public virtual string ATTRIBUTE_DATARULE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_DATARULE"], typeof(string), true)));
                }
            }

            //public new virtual ParentNameRelation Name
            //{
            //    get
            //    {
            //        return new ParentNameRelation(this.GetParentData("Relation7"));
            //    }
            //}
        }
        #endregion DataSource CategorySpecs


        #region DataSource FamilySpecs
        public class FamilySpecsDataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public FamilySpecsDataSource() :
                base("Connection", "FamilySpecs", "FamilySpecs", "", true, false, 30)
            {
            }

            public virtual int FAMILY_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["FAMILY_ID"], typeof(int), true)));
                }
            }

            public virtual string FAMILY_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["FAMILY_NAME"], typeof(string), true)));
                }
            }

            public virtual string CATEGORY_ID
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["CATEGORY_ID"], typeof(string), true)));
                }
            }

            public virtual int SORT_ORDER
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["SORT_ORDER"], typeof(int), true)));
                }
            }

            public virtual int ATTRIBUTE_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["ATTRIBUTE_ID"], typeof(int), true)));
                }
            }

            public virtual string ATTRIBUTE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_NAME"], typeof(string), true)));
                }
            }

            public virtual string STRING_VALUE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["STRING_VALUE"], typeof(string), true)));
                }
            }

            public virtual byte ATTRIBUTE_TYPE
            {
                get
                {
                    return ((byte)(StiReport.ChangeType(this["ATTRIBUTE_TYPE"], typeof(byte), true)));
                }
            }

            public virtual string ATTRIBUTE_DATATYPE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_DATATYPE"], typeof(string), true)));
                }
            }

            public virtual string ATTRIBUTE_DATARULE
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_DATARULE"], typeof(string), true)));
                }
            }

            public new virtual ParentNameRelation Name
            {
                get
                {
                    return new ParentNameRelation(this.GetParentData("Relation7"));
                }
            }
        }
        #endregion DataSource FamilySpecs

        #region DataSource TBL4
        public class TBL4DataSource : Stimulsoft.Report.Dictionary.StiSqlSource
        {

            public TBL4DataSource() :
                base("Connection", "TBL4", "TBL4", "", true, false, 30)
            {
            }

            public virtual int ATTRIBUTE_ID
            {
                get
                {
                    return ((int)(StiReport.ChangeType(this["ATTRIBUTE_ID"], typeof(int), true)));
                }
            }

            public virtual string ATTRIBUTE_NAME
            {
                get
                {
                    return ((string)(StiReport.ChangeType(this["ATTRIBUTE_NAME"], typeof(string), true)));
                }
            }
        }
        #endregion DataSource TBL4


        public string ImageConversion_category(string ChkFile)
        {
            if (ChkFile.Length > 3)
            {

                string ImagePath;
                string file_name = string.Empty;
                var file = ChkFile.Substring(ChkFile.ToString().LastIndexOf('\\')).Trim('\\').Split('.');
                if (file.Length > 0)
                    file_name = CategoryId + "_" + file[0].ToString();
                else
                    file_name = CategoryId + "_ImageandAttFile";
                if (System.IO.Directory.Exists(imagemagickpath))
                {

                    //string[] imgargs = { ChkFile.ToString(), content + "\\temp\\ImageandAttFile" + ChkFile.ToString().Substring(ChkFile.ToString().LastIndexOf('\\'), ChkFile.ToString().LastIndexOf('.') - ChkFile.ToString().LastIndexOf('\\')) + ".jpg" };


                    string[] imgargs = { ChkFile.ToString(), content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg" };


                    if (converttype == "Image Magic")
                    {
                        Exefile = "convert.exe";
                        var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, "\"" + imgargs[0] + "\" -resize 200x200 \"" + imgargs[1].Replace("&", "") + "\"");

                        pStart.CreateNoWindow = true;
                        pStart.UseShellExecute = false;
                        pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                        while (!cmdProcess.HasExited)
                        {
                        }
                    }
                    else
                    {
                        Exefile = "cons_rcp.exe";
                        // var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile," -s "+ "\"" + imgargs[0] + "\" -resize 50x20 \"" + imgargs[1].Replace("&", "") + "\"");
                        var pStart = new System.Diagnostics.ProcessStartInfo(imagemagickpath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"  -resize 200x200");
                        pStart.CreateNoWindow = true;
                        pStart.UseShellExecute = false;
                        pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                        while (!cmdProcess.HasExited)
                        {
                        }
                    }
                }
                //ImagePath = content + "\\temp\\ImageandAttFile" + ChkFile.ToString().Substring(ChkFile.ToString().LastIndexOf('\\'), ChkFile.ToString().LastIndexOf('.') - ChkFile.ToString().LastIndexOf('\\')) + ".jpg";
                ImagePath = content + "\\temp\\ImageandAttFile\\" + file_name + ".jpg";
                if (!string.IsNullOrEmpty(file.ToString()))
                {
                    if (!string.IsNullOrEmpty(file[0].ToString().Trim()) && !string.IsNullOrEmpty(file[1].ToString().Trim()) && (file[1].ToString().ToUpper() == "PSD"))
                    {
                        ImagePath = content + "\\temp\\ImageandAttFile\\" + file_name + "-0.jpg";
                    }
                }
                ImagePath = ImagePath.Replace("&", "");


                return ImagePath;
            }
            else
                return ChkFile;

        }
        #endregion StiReport Designer generated code - do not modify
    }
}