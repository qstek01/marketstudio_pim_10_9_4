﻿using LS.Data;
using LS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LS.Data.Model.WebSync;
using log4net;

namespace LS.Web.Models
{
    public class QueryValues
    {
        private string connectionString;
        private static ILog _logger;
        private SqlConnection con;
        private SqlCommand sqlCommand;
        private SqlDataAdapter sqlDataAdapter;
        private CSEntities csEntitied;
        public QueryValues()
        {
            _logger = LogManager.GetLogger(typeof(QueryValues));
            connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;

        }
        internal List<CategoryFamilyProduct> GetCategoryFamilyProduct(DateTime fromDate, DateTime toDate, int catalogId, string categoryIds, string userName)
        {
            try
            {
                List<CategoryFamilyProduct> categoryFamilyProduct = new List<CategoryFamilyProduct>();
                DataTable dt = new DataTable();
                string sqlQuery = string.Empty;
                
                    categoryIds = categoryIds.Replace("ATTR", "");

                    categoryIds = categoryIds == "0" ? "'0'" : categoryIds;
                    if (categoryIds.Contains("'~CAT") || categoryIds.Contains("'CAT"))
                    {
                        if (userName == "ALL")
                        {
                            sqlQuery = "SELECT Distinct C.CATEGORY_ID,Convert(nvarchar,C.MODIFIED_DATE,120) MODIFIED_DATE,ISNULL(F.FAMILY_ID,0) FAMILY_ID,Convert(nvarchar,isnull(F.MODIFIED_DATE,getDate()),120) FMODIFIED_DATE,isnull(PF.PRODUCT_ID,0) as PRODUCT_ID,Convert(nvarchar,isnull(P.MODIFIED_DATE,getDate()),120) PMODIFIED_DATE,isnull(TSP.SUBPRODUCT_ID,0) as SUBPRODUCT_ID,Convert(nvarchar,isnull(TSP.MODIFIED_DATE,getDate()),120) SPMODIFIED_DATE FROM TB_CATEGORY C" +
                                           " INNER JOIN TB_CATALOG_SECTIONS CS ON CS.CATEGORY_ID=C.CATEGORY_ID" +
                                           " LEFT OUTER JOIN TB_FAMILY F ON  CS.CATEGORY_ID=F.CATEGORY_ID " +
                                           " AND (((CONVERT(NVARCHAR(10), F.MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') )" +
                                           " or exists (Select 1 from TB_FAMILY where PARENT_FAMILY_ID=F.FAMILY_ID " +
                                           " and (CONVERT(NVARCHAR(10), MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "')) or exists (Select 1 from TB_PROD_SPECS where  (CONVERT(NVARCHAR(10), MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') ))" +
                                           " LEFT OUTER JOIN TB_CATALOG_FAMILY CF ON  CF.FAMILY_ID=F.FAMILY_ID And CF.CATALOG_ID=" + catalogId +
                                           " LEFT OUTER JOIN TB_PROD_FAMILY PF ON PF.FAMILY_ID=F.FAMILY_ID  AND PF.PRODUCT_ID in (select PRODUCT_ID from [PRODUCTAPPROVALFOR2WS](" + catalogId + ",PF.FAMILY_ID)) " +
                                           " LEFT OUTER JOIN TB_PRODUCT P ON PF.PRODUCT_ID=P.PRODUCT_ID and( (CONVERT(NVARCHAR(10), P.MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') or exists (Select 1 from TB_PROD_SPECS where  (CONVERT(NVARCHAR(10), MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') ))" +
                                           " LEFT OUTER JOIN TB_SUBPRODUCT TSP ON PF.PRODUCT_ID=TSP.PRODUCT_ID and ((CONVERT(NVARCHAR(10), TSP.MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') or exists (Select 1 from TB_PROD_SPECS where  (CONVERT(NVARCHAR(10), MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') ))" +
                                           " LEFT OUTER JOIN TB_CATALOG_PRODUCT CP ON CP.PRODUCT_ID=P.PRODUCT_ID And CP.CATALOG_ID=" + catalogId +
                                           " WHERE CS.CATALOG_ID=" + catalogId + " And C.CATEGORY_ID in (" + categoryIds + ")  or C.PARENT_CATEGORY in (" + categoryIds + ") AND C.WORKFLOW_STATUS=4";
                        }
                        else
                        {
                            sqlQuery = "SELECT Distinct C.CATEGORY_ID,Convert(nvarchar,C.MODIFIED_DATE,120) MODIFIED_DATE,ISNULL(F.FAMILY_ID,0) FAMILY_ID,Convert(nvarchar,isnull(F.MODIFIED_DATE,getDate()),120) FMODIFIED_DATE,isnull(PF.PRODUCT_ID,0) as PRODUCT_ID,Convert(nvarchar,isnull(P.MODIFIED_DATE,getDate()),120) PMODIFIED_DATE,isnull(TSP.SUBPRODUCT_ID,0) as SUBPRODUCT_ID,Convert(nvarchar,isnull(TSP.MODIFIED_DATE,getDate()),120) SPMODIFIED_DATE FROM TB_CATEGORY C" +
                                          " INNER JOIN TB_CATALOG_SECTIONS CS ON CS.CATEGORY_ID=C.CATEGORY_ID" +
                                          " LEFT OUTER JOIN TB_FAMILY F ON  CS.CATEGORY_ID=F.CATEGORY_ID " +
                                          " AND (((CONVERT(NVARCHAR(10), F.MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') AND F.MODIFIED_USER='" + userName + "')" +
                                          " or exists (Select 1 from TB_FAMILY where PARENT_FAMILY_ID=F.FAMILY_ID " +
                                          " and (CONVERT(NVARCHAR(10), MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') AND MODIFIED_USER='" + userName + "')or exists (Select 1 from TB_PROD_SPECS where  (CONVERT(NVARCHAR(10), MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') ))" +
                                          " LEFT OUTER JOIN TB_CATALOG_FAMILY CF ON  CF.FAMILY_ID=F.FAMILY_ID And CF.CATALOG_ID=" + catalogId +
                                          " LEFT OUTER JOIN TB_PROD_FAMILY PF ON PF.FAMILY_ID=F.FAMILY_ID  AND PF.PRODUCT_ID in (select PRODUCT_ID from [PRODUCTAPPROVALFOR2WS](" + catalogId + ",PF.FAMILY_ID))" +
                                          " LEFT OUTER JOIN TB_PRODUCT P ON PF.PRODUCT_ID=P.PRODUCT_ID and ((CONVERT(NVARCHAR(10), P.MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') or exists (Select 1 from TB_PROD_SPECS where  (CONVERT(NVARCHAR(10), MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') )) AND P.MODIFIED_USER='" + userName + "'" +
                                          " LEFT OUTER JOIN TB_SUBPRODUCT TSP ON PF.PRODUCT_ID=TSP.PRODUCT_ID and( (CONVERT(NVARCHAR(10), TSP.MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "')or exists (Select 1 from TB_PROD_SPECS where  (CONVERT(NVARCHAR(10), MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') )) AND TSP.MODIFIED_USER='" + userName + "'" +
                                          " LEFT OUTER JOIN TB_CATALOG_PRODUCT CP ON CP.PRODUCT_ID=P.PRODUCT_ID And CP.CATALOG_ID=" + catalogId +
                                          " WHERE CS.CATALOG_ID=" + catalogId + " And C.CATEGORY_ID in (" + categoryIds + ") or C.PARENT_CATEGORY in (" + categoryIds + ") AND C.WORKFLOW_STATUS=4 ";

                        }
                        using (con = new SqlConnection(connectionString))
                        {
                            con.Open();
                            sqlCommand = new SqlCommand(sqlQuery, con);
                            sqlCommand.CommandTimeout = 0;
                            sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                            sqlDataAdapter.Fill(dt);
                        }
                        List<DataRow> list = dt.AsEnumerable().ToList();

                        foreach (var item in list)
                        {
                            categoryFamilyProduct.Add(

                               new CategoryFamilyProduct
                               {
                                   FAMILY_ID = Convert.ToInt32(item["FAMILY_ID"]),
                                   CATEGORY_ID = Convert.ToString(item["CATEGORY_ID"]),
                                   PRODUCT_ID = Convert.ToInt32(item["PRODUCT_ID"]),
                                   SUBPRODUCTID = Convert.ToInt32(item["SUBPRODUCT_ID"]),
                                   MODIFIED_DATE = Convert.ToDateTime(item["MODIFIED_DATE"]),
                                   FMODIFIED_DATE = Convert.ToDateTime(item["FMODIFIED_DATE"]),
                                   PMODIFIED_DATE = Convert.ToDateTime(item["PMODIFIED_DATE"]),
                                   SPMODIFIED_DATE = Convert.ToDateTime(item["SPMODIFIED_DATE"])

                               });
                        }

                        return categoryFamilyProduct;
                    }
                    else
                        return null;
            }
                
            catch (Exception ex)
            {
                _logger.Error("Error at GetXmlData:QueryValues-", ex);
                return null;
            }
        }

        internal List<WebSyncCategoryDetails> GetCategoryAttributes(string categoryId, int catalogId)
        {
            try
            {
                var ListwebSyncCategoryDetails = new List<WebSyncCategoryDetails>();
                var webSyncCategoryDetails = new WebSyncCategoryDetails();
                DataTable dtCategoryAttr = new DataTable();
                    using (con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        sqlCommand = new SqlCommand("STP_LS_CATEGORYATTRIBUTES", con);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@cateory_Id", categoryId);
                        sqlCommand.Parameters.Add("@Catalog_Id", catalogId);
                        sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                        sqlDataAdapter.Fill(dtCategoryAttr);
                        con.Close();
                    }
                    int i = 0;
                    foreach (var item in dtCategoryAttr.AsEnumerable().ToList())
                    {
                        ListwebSyncCategoryDetails.Add(new WebSyncCategoryDetails
                        {
                            id = categoryId,
                            CATEGORY_ID = categoryId + "ATTR",
                            CATEGORY_NAME = item["ATTRIBUTENAME"].ToString(),
                            @checked = false,
                            hasChildren = false,
                            FamilyandRelatedFamily = false,
                            spriteCssClass = "category",
                            encoded = false,
                            CATEGORY_SHORT = categoryId,
                            Parent_Category = categoryId,
                            Modify_Date = DateTime.Now

                        });
                        i++;
                    }
                    return ListwebSyncCategoryDetails;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetXmlData:QueryValues-", ex);
                return null;
            }

        }

        internal DataTable GetXmlData(int catalogId, string categoryId, string rootCategoryId, int familyId, string allowDuplicate, string productId, string subProductId, string fromDate, string toDate)
        {
            try
            {
                DataTable xmlData = new DataTable();
                using (con = new SqlConnection(connectionString))
                {
                    sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_FLATXMLTREE", con);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    sqlCommand.Parameters.Add("@CATEGORYID", SqlDbType.VarChar).Value = categoryId;
                    sqlCommand.Parameters.Add("@ROOT_CATEGORYID", SqlDbType.VarChar).Value = rootCategoryId;
                    sqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                    sqlCommand.Parameters.Add("@ALLOW_DUPLICATES", SqlDbType.VarChar).Value = allowDuplicate;
                    sqlCommand.Parameters.Add("@PRODUCTIDS", SqlDbType.VarChar).Value = productId;
                    sqlCommand.Parameters.Add("@SUBPRODUCTIDS", SqlDbType.VarChar).Value = subProductId;
                    sqlCommand.Parameters.Add("@FROMDATE", String.Format("{0:yyyyMMdd}", fromDate));
                    sqlCommand.Parameters.Add("@TODATE", String.Format("{0:yyyyMMdd}", toDate));
                    sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(xmlData);
                }
                return xmlData;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetXmlData:QueryValues-", ex);
                return null;
            }

        }

        public string GetCatalogItemNo(int customerId)
        {
            try
            {
                csEntitied = new CSEntities();
                var catalogItemNo = csEntitied.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault();
                if (catalogItemNo == null)
                    catalogItemNo = "ITEM#";
                return catalogItemNo.ToString();
            }

            catch (Exception ex)
            {
                _logger.Error("Error at GetXmlData:QueryValues-", ex);
                return null;
            }
        }

        public string GetSubCatalogItemNo(int customerId)
        {
            try
            {
                csEntitied = new CSEntities();
                var catalogItemNo = csEntitied.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeSubItemNo).FirstOrDefault();
                if (catalogItemNo == null)
                    catalogItemNo = "SUBITEM#";
                return catalogItemNo.ToString();
             
            }

            catch (Exception ex)
            {
                _logger.Error("Error at GetXmlData:QueryValues-", ex);
                return null;
            }
        }

       
    }
}