﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LS.Web.Models
{
    public static class HtmlHelperExtensions 
    {
        public static MvcHtmlString RenderDropDownOptionsForGroups(List<string> options)
        {
            StringBuilder optionsBuilder = new StringBuilder();
            foreach (var option in options)
            {
                optionsBuilder.Append("<option >" + option + "</option>");
            }
            return new MvcHtmlString(optionsBuilder.ToString());
        }
    }
}