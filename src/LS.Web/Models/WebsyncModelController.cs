﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace LS.Web.Models
{
    public class WebsyncModelController : ApiController
    {
    }

    public class SyncDataModel
    {
        public string sessionId { get; set; }
        public string createdUser { get; set; }
        //public int createdId { get; set; }
        public string updateType { get; set; }
        public string jobType { get; set; }
        public string jobName { get; set; }
        // public string xmldata { get; set; }
        public XmlDocument xmldata { get; set; }
        //public int CustomerId { get; set; }

    }
}
