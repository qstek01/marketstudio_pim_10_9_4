﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using log4net;
using LS.Data.Model;
using Newtonsoft.Json;
using System.Xml.Serialization;

namespace LS.Web.Models
{
    public class AttributeMethods
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AttributeMethods));
        public string DataFormat(string dataformat)
        {
            //var items = e.sender._data;
            try
            {
                if (dataformat != null)
                {
                    if (dataformat == @"(^-?\d\d*$)")
                    {
                        return "Integer";
                    }
                    else if (dataformat.Contains("{0,6}$"))
                    {
                        return "Real Numbers";
                    }
                    else if (dataformat == @"^[ 0-9a-zA-Z\r\n\x20-\x7E\u0000-\uFFFF]*$")
                    {
                        return "All Characters";
                    }
                    else if (dataformat == "^[ 0-9a-zA-Z\\r\\n]*$")
                    {
                        return "Alpha Numeric";
                    }
                    else if (dataformat == "^[ a-zA-Z\\r\\n]*$")
                    {
                        return "Alphabets Only";
                    }
                    else if (dataformat ==
                             @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)")
                    {
                        return "Short Format (dd/mm/yyyy)";
                    }
                    else if (dataformat ==
                             @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4})(?=.{3}(?:0[1-9]|[12]\d|3[01]))(?=.{0}(?:0[1-9]|1[0-2]))(?!.{0}(?:0[2469]|11)-31)(?!.{0}02-30)(?!02(\-|\/|\.)29(\-|\/|\.)...[13579])(?!02(\-|\/|\.)29(\-|\/|\.)..[13579][048])(?!02(\-|\/|\.)29(\-|\/|\.)..[02468][26])(?!02(\-|\/|\.)29(\-|\/|\.).[13579]00)(?!02(\-|\/|\.)29(\-|\/|\.)[13579][048]00)(?!02(\-|\/|\.)29(\-|\/|\.)[02468][26]00)")
                    {
                        return "Short Format (mm/dd/yyyy)";
                    }
                    else if (dataformat ==
                             @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))")
                    {
                        return "DateTime";
                    }
                    else if (dataformat ==
                             @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2}\s\w{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))")
                    {
                        return "Long Format";
                    }
                    else if (dataformat.Contains("https?://"))
                    {
                        return "Hyperlink";
                    }
                    else
                    {
                        return "System default";
                    }
                }
                else
                {
                    return "System default";

                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at AttributeMethods :  GetAllattributes", objException);
                return "System default";
            }
        }


        public string OrgDataFormat(string dataformat, string type)
        {
            //var items = e.sender._data;
            try
            {
                if (type.ToLower().Contains("text"))
                {

                    if (dataformat == "All Characters")
                    {
                        return "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                    }
                    else if (dataformat == "Alphabets Only")
                    {
                        return "^[ a-zA-Z\\r\\n]*$";
                    }
                    else if (dataformat == "Alpha Numeric")
                    {
                        return "^[ 0-9a-zA-Z\\r\\n]*$";
                    }
                    else
                    {
                        return "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                    }


                }
                else if (type.ToLower().Contains("number"))
                {

                    if (dataformat == "Integer")
                    {
                        return @"(^-?\d\d*$)";
                    }
                    else
                    {
                        return @"^-{0,1}?\d*\.{0,1\}d{0,6}$";
                    }


                }
                else if (type.ToLower().Contains("date"))
                {

                    if (dataformat == "Short Format (dd/mm/yyyy)")
                    {
                        return @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)";
                    }
                    else if (dataformat == "Short Format (mm/dd/yyyy)")
                    {
                        return @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4})(?=.{3}(?:0[1-9]|[12]\d|3[01]))(?=.{0}(?:0[1-9]|1[0-2]))(?!.{0}(?:0[2469]|11)-31)(?!.{0}02-30)(?!02(\-|\/|\.)29(\-|\/|\.)...[13579])(?!02(\-|\/|\.)29(\-|\/|\.)..[13579][048])(?!02(\-|\/|\.)29(\-|\/|\.)..[02468][26])(?!02(\-|\/|\.)29(\-|\/|\.).[13579]00)(?!02(\-|\/|\.)29(\-|\/|\.)[13579][048]00)(?!02(\-|\/|\.)29(\-|\/|\.)[02468][26]00)";
                    }
                    else if (dataformat == "DateTime")
                    {
                        return @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))";
                    }
                    else if (dataformat == "Long Format")
                    {
                        return @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2}\s\w{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))";
                    }
                    else
                    {
                        return "System default";
                    }

                }
                else if (type.ToLower().Contains("hyperlink"))
                {
                    return @"^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$";
                }
                else
                {
                    return "System default";

                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at AttributeMethods :  GetAllattributes", objException);
                return "System default";
            }
        }

        public List<DataRule> XmlDeserializefunction(string attributeDatarule)
        {
            try
            {
                if (string.IsNullOrEmpty(attributeDatarule))
                {
                    var obj = new List<DataRule>();
                    var obj1 = new DataRule();
                    obj1.ApplyTo = "All";
                    obj1.ApplyForNumericOnly = "1";
                    obj.Add(obj1);
                    return obj;
                }
                else
                {


                    var deserializer = new XmlSerializer(typeof(NewDataSet));
                    TextReader reader = new StringReader(attributeDatarule);
                    object obj = deserializer.Deserialize(reader);
                    var xmlData = (NewDataSet)obj;
                    return xmlData.dataRuleList;
                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController :  XmlDeserializefunction", objException);
                return null;
            }
        }

        public string GetNumericValues(int length, string value, string opt)
        {
            if (opt == "Num" && value.Contains("Num"))
            {
                if (length == 12)
                {
                    return value.Substring(7, 2);
                }
                else
                {
                    return value.Substring(7, 1);
                }
            }
            else if (opt == "Dec" && value.Contains("Num"))
            {
                if (length == 12)
                {
                    return value.Substring(10, 1);
                }
                else
                {
                    return value.Substring(9, 1);
                }
            }
            else
            {
                return value;
            }
        }

        public string Xmlserializefunction(NewDataSet newDataSet)
        {
            try
            {
                if (newDataSet.dataRuleList.Count <= 0)
                {
                    return null;
                }
                else
                {
                    if (!string.IsNullOrEmpty(newDataSet.dataRuleList[0].Prefix) ||
                        !string.IsNullOrEmpty(newDataSet.dataRuleList[0].Suffix) ||
                        !string.IsNullOrEmpty(newDataSet.dataRuleList[0].CustomValue))
                    {
                        var serializer1 = new JavaScriptSerializer();
                        var json = serializer1.Serialize(newDataSet.dataRuleList);
                        XmlDocument doc = JsonConvert.DeserializeXmlNode("{\"DataRule\":" + json + "}", "NewDataSet");
                        return "<?xml version=\"1.0\" standalone=\"yes\"?>" + doc.InnerXml;
                    }
                    else
                    {
                        return null;

                    }
                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : Xmlserializefunction", objException);
                return null;
            }
        }

     
    }
}