﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LS.Web.Models
{
    public class ImportBatch
    {
        public string BatchId { get; set; }
        public string CatalogName { get; set; }
        public string FileName { get; set; }
        public string ImportType { get; set; }
        public int AllowDuplicate { get; set; }
        public int InsertRecords { get; set; }
        public int DeleteRecords { get; set; }
        public int SkipRecords { get; set; }
        public string Status { get; set; }
        public string ElapsedTime { get; set; }
        public string scheduleTime { get; set; }

        public string NoOfRecords { get; set; }
        public bool ISAvailable { get; set; }
        public string CustomStatus { get; set; }
        public int UPDATEDRECORDS { get; set; }


    }

}