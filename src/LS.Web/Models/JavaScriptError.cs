﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LS.Web.Models
{
    public class JavaScriptError
    {
        public string ErrorUrl { get; set; }
        public string ErrorMessage { get; set; }
        public List<string> StackTrace { get; set; }
        public string Cause { get; set; }
    }
    public class ProgressBar
    {
        public string progressVale { get; set; }
    }
}