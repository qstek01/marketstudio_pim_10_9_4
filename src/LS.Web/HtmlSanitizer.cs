﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace LS.Web.Helpers
{
    public static class HtmlSanitizer
    {
        public static DataSet Sanitize(DataSet dataset)
        {

            for (int i = 0; i < dataset.Tables.Count; i++)
            {
                foreach (DataColumn dc in dataset.Tables[i].Columns) // search whole table
                {
                    if (dc.DataType.Equals(typeof(System.String)))
                    {

                        foreach (DataRow datarow in dataset.Tables[i].Rows)
                        {
                            string replacedValue = datarow[dc].ToString();
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "<br />", Environment.NewLine);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "<br>", Environment.NewLine);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "<br", String.Empty);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "<\br", String.Empty);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "<br/>", String.Empty);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "</p>", String.Empty);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "<p>", String.Empty);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "<pre>", String.Empty);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "</pre>", String.Empty);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "<strong>", String.Empty);
                            replacedValue = ReplaceCaseInsensitive(replacedValue, "</strong>", String.Empty);
                            replacedValue = replacedValue.Replace(Environment.NewLine + Environment.NewLine, Environment.NewLine);
                            datarow[dc] = replacedValue;
                        }
                    }
                }
            }
            return dataset;
        }

        private static string ReplaceCaseInsensitive(string input, string search, string replacement)
        {
            string result = Regex.Replace(
                input,
                Regex.Escape(search),
                replacement.Replace("$", "$$"),
                RegexOptions.IgnoreCase
            );
            return result;
        }
    }
}