﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadImportTemplate
    /// </summary>
    public class DownloadImportTemplate : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;
            string filename = HttpContext.Current.Request.QueryString["Path"];
            if (File.Exists(context.Server.MapPath("~/Content/XMLTemplate/" + filename)))
            {
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + filename + ";");
                response.TransmitFile(context.Server.MapPath("~/Content/XMLTemplate/" + filename));
                response.Flush();
                response.End();
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("File not found!");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}