﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LS.Web
{
    public static partial class ExtensionMethods
    {
        public static string ToRFC822String(this DateTime timestamp)
        {
            return timestamp.ToString("yyyy-MM-dd HH':'mm':'ss")
                + " "
                + timestamp.ToString("zzzz").Replace(":", "");
        }
    }
}