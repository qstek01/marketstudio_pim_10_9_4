﻿using Infragistics.Documents.Excel;
using LS.Data;
using LS.Web.Controllers;
using LS.Web.Helpers;
using LS.Web.Models;
using System;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Web;
using System.Web.SessionState;
using DataTable = System.Data.DataTable;
using Workbook = Infragistics.Documents.Excel.Workbook;

namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadFile
    /// </summary>
    public class DownloadFile : IHttpHandler, IRequiresSessionState
    {

        //private readonly CSEntities 
        private readonly CSEntities _dbcontext = new CSEntities();

        QueryValues queryValues = new QueryValues();
        HomeApiController homeObj = new HomeApiController();

        public void ProcessRequest(HttpContext context)
        {


            //CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {

                System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
            }





            //context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;
            string filename = HttpContext.Current.Request.QueryString["Path"];
            bool EnableSubProduct = Convert.ToBoolean(HttpContext.Current.Request.QueryString["EnableSubProduct"]);

            if (context.Session["ExportTable"] != null && filename.ToLower().Contains(".xls"))
            {
                var distinctds = (DataTable)context.Session["ExportTable"];
                string referencetable = (string)context.Session["ExportTableFlag"];
                if (!(distinctds.Columns.Contains("Action") || distinctds.Columns.Contains("ACTION")))
                {
                    if (referencetable == null)
                    {
                        distinctds.Columns.Add("ACTION").SetOrdinal(0);
                        distinctds.AcceptChanges();
                    }
                }

                System.Web.HttpContext.Current.Session["ExportTableFlag"] = null;
                DataSet dataset1;
                dataset1 = HtmlSanitizer.Sanitize(distinctds.DataSet);
                var table = distinctds.DefaultView.ToTable(true);
                // table.Columns.Add("sdfsaf");
                // table.Columns.Add("sdfsaf1");
                // table.Columns.Add("sdfsaf2");
                //   context.Session["ExportTable"] = null;
                var workbook = new Workbook();

                //var objExport = new RKLib.ExportData.Export();
                if (table.Rows.Count > 0)
                {
                    const string tableName1 = "Products";
                    string tableName = "Products";

                    int rowcntTemp = table.Rows.Count;
                    int j = 0;
                    int runningcnt = 0;
                    do
                    {
                        int rowcnt;
                        if (rowcntTemp <= 65000)
                        {
                            rowcnt = rowcntTemp;
                            rowcntTemp = rowcnt - 65000;
                        }
                        else
                        {
                            rowcnt = 65000;
                            rowcntTemp = table.Rows.Count - (runningcnt + 65000);
                        }
                        j++;
                        if (j != 1)
                        {
                            tableName = tableName1 + (j - 1);
                        }
                        var worksheet = workbook.Worksheets.Add(tableName);
                        workbook.WindowOptions.SelectedWorksheet = worksheet;
                        for (int jj = 1; jj <= rowcnt; jj++)
                            // Create the worksheet to represent this data table
                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                            {
                                if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBCATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                                }
                            }
                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet
                        int rowIndex = 2;
                        int temprunningcnt = runningcnt;
                        for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                        {
                            var row = worksheet.Rows[rowIndex++];
                            runningcnt++;
                            for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                if (table.Rows[k].ItemArray[columnIndex].ToString().ToUpper() == "ITEM#" || table.Rows[k].ItemArray[columnIndex].ToString().ToUpper() == "SUBITEM#" || table.Rows[k].ItemArray[columnIndex].ToString().ToUpper().Contains("CATALOG_ITEM_NO"))
                                {
                                    if (table.Rows[k].ItemArray[columnIndex].ToString().ToUpper() == "ITEM#")
                                    {
                                        row.Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                    }
                                    else if (table.Rows[k].ItemArray[columnIndex].ToString().ToUpper() == "SUBITEM#")
                                    {
                                        row.Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (table.Rows[k].ItemArray[columnIndex].ToString().ToUpper().Contains("SUBCATALOG_ITEM_NO"))
                                    {
                                        row.Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (table.Rows[k].ItemArray[columnIndex].ToString().ToUpper().Contains("CATALOG_ITEM_NO"))
                                    {
                                        row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex].ToString().ToUpper().Replace("CATALOG_ITEM_NO", HttpContext.Current.Session["CustomerItemNo"].ToString());
                                    }
                                }
                                else
                                {
                                    row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                                }
                            }
                        }
                    } while (rowcntTemp > 0);
                }
                else
                {
                    string tableName = "Products";
                    var worksheet = workbook.Worksheets.Add(tableName);
                    workbook.WindowOptions.SelectedWorksheet = worksheet;
                    for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                    {
                        if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "ITEM#")
                        {
                            worksheet.Rows[0].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                        }
                        else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBCATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBITEM#")
                        {
                            worksheet.Rows[0].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                        }
                        else
                        {
                            worksheet.Rows[0].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                        }
                    }
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("No records found.");
                }

                if (EnableSubProduct == true)
                {
                    if (context.Session["ExportTableSubProduct"] != null)
                    {
                        var tableSP = (DataTable)context.Session["ExportTableSubProduct"];
                        string tableNameSp = "SubProducts";
                        const string tableNameSp1 = "SubProducts";
                        int rowcntTempSP = tableSP.Rows.Count;
                        int jSP = 0;
                        int runningcntSP = 0;
                        if (tableSP.Rows.Count > 0)
                        {
                            do
                            {
                                int rowcntSP;
                                if (rowcntTempSP <= 65000)
                                {
                                    rowcntSP = rowcntTempSP;
                                    rowcntTempSP = rowcntSP - 65000;
                                }
                                else
                                {
                                    rowcntSP = 65000;
                                    rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                                }
                                jSP++;

                                if (jSP != 1)
                                {
                                    tableNameSp = tableNameSp1 + (jSP - 1);
                                }
                                var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                                workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                                for (int jj = 1; jj <= rowcntSP; jj++)

                                    // Create the worksheet to represent this data table


                                    // Create column headers for each column
                                    for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                                    {
                                        if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                        {
                                            worksheetSp.Rows[0].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                        }
                                        else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                        {
                                            worksheetSp.Rows[0].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                        }
                                        else
                                        {
                                            worksheetSp.Rows[0].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);
                                        }


                                    }

                                // Starting at row index 1, copy all data rows in
                                // the data table to the worksheet

                                int rowIndex = 1;
                                int temprunningcntSP = runningcntSP;
                                for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                                {
                                    var row = worksheetSp.Rows[rowIndex++];
                                    runningcntSP++;
                                    for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                                    {

                                        if (tableSP.Rows[k].ItemArray[columnIndex].ToString().ToUpper() == "ITEM#" || tableSP.Rows[k].ItemArray[columnIndex].ToString().ToUpper() == "SUBITEM#")
                                        {

                                            if (tableSP.Rows[k].ItemArray[columnIndex].ToString().ToUpper() == "ITEM#")
                                            {
                                                row.Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                            }
                                            else if (tableSP.Rows[k].ItemArray[columnIndex].ToString().ToUpper() == "SUBITEM#")
                                            {
                                                row.Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];

                                            }
                                            else if (tableSP.Rows[k].ItemArray[columnIndex].ToString().ToUpper().Contains("SUBCATALOG_ITEM_NO"))
                                            {
                                                row.Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                            }
                                            else if (tableSP.Rows[k].ItemArray[columnIndex].ToString().ToUpper().Contains("CATALOG_ITEM_NO"))
                                            {
                                                row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex].ToString().ToUpper().Replace("CATALOG_ITEM_NO", HttpContext.Current.Session["CustomerItemNo"].ToString());
                                            }



                                        }
                                        else
                                        {
                                            row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                        }



                                        //row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                    }
                                }
                            } while (rowcntTempSP > 0);
                        }
                    }
                }
                if (context.Session["tableDesignerFamilyExportTable"] != null && filename.ToLower().Contains(".xls"))
                {
                    var tableDesignerFamilyExport = (DataTable)System.Web.HttpContext.Current.Session["tableDesignerFamilyExportTable"];
                    if (tableDesignerFamilyExport.Rows.Count > 0)
                    {
                        string tableName = "TableDesigner";
                        int rowcntTemp = tableDesignerFamilyExport.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = tableDesignerFamilyExport.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                tableName = tableName + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(tableName);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int columnIndex = 0; columnIndex < tableDesignerFamilyExport.Columns.Count; columnIndex++)
                            {
                                if (HttpUtility.HtmlDecode(tableDesignerFamilyExport.Columns[columnIndex].ColumnName) != "ID" && HttpUtility.HtmlDecode(tableDesignerFamilyExport.Columns[columnIndex].ColumnName) != "ROLE_ID" && HttpUtility.HtmlDecode(tableDesignerFamilyExport.Columns[columnIndex].ColumnName) != "CATALOGID")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableDesignerFamilyExport.Columns[columnIndex].ColumnName);
                                }
                            }
                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < tableDesignerFamilyExport.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    if (Convert.ToString(tableDesignerFamilyExport.Rows[k].ItemArray[columnIndex]).Length > 32700)
                                    {

                                        row.Cells[columnIndex].Value = "";

                                    }
                                    else
                                    {
                                        row.Cells[columnIndex].Value = tableDesignerFamilyExport.Rows[k].ItemArray[columnIndex];
                                    }
                                }
                            }

                        }
                        while (rowcntTemp > 0);
                    }
                }
                if (workbook.Worksheets[0] != null)
                {
                    var worksheet1 = workbook.Worksheets[0];
                    workbook.WindowOptions.SelectedWorksheet = worksheet1;
                }
                if (filename.ToLower().Contains(".xlsx"))
                {
                    workbook.SetCurrentFormat(WorkbookFormat.Excel2007);
                }
                workbook.Save(context.Server.MapPath("~/Content/" + filename));

                if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                {
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "application/vnd.ms-excel";
                    //response.AddHeader("Content-Disposition","attachment; filename=" + filename + ";");
                    response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", filename));
                    response.TransmitFile(context.Server.MapPath("~/Content/" + filename));
                    response.Flush();
                    response.End();
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not found!");
                }


            }
            else if (filename.ToLower().Contains(".txt") || filename.ToLower().Contains(".csv") || filename.ToLower().Contains(".xml"))
            {
                string subproductcheck = string.Empty;
                if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                {
                    System.IO.FileStream fs = null;
                    fs = System.IO.File.Open(HttpContext.Current.Server.MapPath("Content/" +
                             filename), System.IO.FileMode.Open);
                    byte[] btFile = new byte[fs.Length];
                    fs.Read(btFile, 0, Convert.ToInt32(fs.Length));
                    fs.Close();
                    response.AddHeader("Content-disposition", "attachment; filename=" +
                                       filename);
                    response.ContentType = "application/octet-stream";
                    response.BinaryWrite(btFile);
                    response.End();
                    // string extension = Path.GetExtension(filename);
                    // subproductcheck = filename.Replace(".txt", "").Replace(".csv", "").Replace(".xml", "") + "_subproduct" + extension;

                    // if (File.Exists(context.Server.MapPath("~/Content/" + subproductcheck)))
                    // {
                    //     System.IO.FileStream fs1 = null;
                    //     fs1 = System.IO.File.Open(HttpContext.Current.Server.MapPath("Content/" +
                    //              subproductcheck), System.IO.FileMode.Open);
                    //     byte[] btFile1 = new byte[fs1.Length];
                    //     fs1.Read(btFile1, 0, Convert.ToInt32(fs1.Length));
                    //     fs1.Close();
                    //     response.AddHeader("Content-disposition", "attachment; filename=" +
                    //                        subproductcheck);
                    //     response.ContentType = "application/octet-stream";
                    //     response.BinaryWrite(btFile1);
                    // }

                    //// string startPath = @"c:\example\start";
                    //// string zipPath = @"C:\Users\Rakesh\Desktop\test.zip";
                    ////// string extractPath = @"c:\example\extract";

                    //// ZipFile.CreateFromDirectory(startPath, zipPath);





                    // response.End();


                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not found!");
                }
                if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                {
                    // format = filepath + "_subproduct" + ".txt";

                    System.IO.FileStream fs = null;
                    fs = System.IO.File.Open(HttpContext.Current.Server.MapPath("Content/" +
                             filename), System.IO.FileMode.Open);
                    byte[] btFile = new byte[fs.Length];
                    fs.Read(btFile, 0, Convert.ToInt32(fs.Length));
                    fs.Close();
                    response.AddHeader("Content-disposition", "attachment; filename=" +
                                       filename);
                    response.ContentType = "application/octet-stream";
                    response.BinaryWrite(btFile);
                    response.End();

                }
            }
            else if (context.Session["ExportErrorTable"] != null && filename.ToLower().Contains(".xls"))
            {
                var dtGetLogDetails = (DataTable)context.Session["ExportErrorTable"];
                if (dtGetLogDetails != null && dtGetLogDetails.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=errorlog.xls";

                    response = HttpContext.Current.Response;
                    response.ClearContent();
                    response.AddHeader("content-disposition", attachment);
                    response.ContentType = "application/vnd.ms-excel";
                    string tab = "";
                    foreach (DataColumn dc in dtGetLogDetails.Columns)
                    {
                        response.Write(tab + dc.ColumnName);
                        tab = "\t";
                    }
                    response.Write("\n");
                    int i;
                    foreach (DataRow dr in dtGetLogDetails.Rows)
                    {
                        tab = "";
                        for (i = 0; i < dtGetLogDetails.Columns.Count; i++)
                        {
                            response.Write(tab + dr[i].ToString());
                            tab = "\t";
                        }
                        response.Write("\n");
                    }

                    response.End();

                }
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("Error occurred during export!.");
            }


        }
        public void ProcessRequest1(HttpContext context)
        {


            //CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {

                System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
            }

            //context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;
            string filename = HttpContext.Current.Request.QueryString["Path"];
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/xls";
            response.AddHeader("Content-Disposition",
                               "attachment; filename=" + filename + ";");
            response.TransmitFile(context.Server.MapPath("~/Content/" + filename));
            response.Flush();
            response.End();



            //if (context.Session["ExportTable"] != null)
            //{
            //    var table = (DataTable)context.Session["ExportTable"];
            //    // table.Columns.Add("sdfsaf");
            //    // table.Columns.Add("sdfsaf1");
            //    // table.Columns.Add("sdfsaf2");
            //    context.Session["ExportTable"] = null;
            //   //  var objExport = new RKLib.ExportData.Export();
            //    if (table != null && table.Rows.Count > 0)
            //    {
            //      //  objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, filename);
            //        //context.Response.ClearContent();
            //        //context.Response.Buffer = true;
            //        //context.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));
            //        //context.Response.ContentType = "application/ms-excel";
            //        //DataTable dt = table;
            //        //string str = string.Empty;
            //        //foreach (DataColumn dtcol in dt.Columns)
            //        //{
            //        //    context.Response.Write(str + dtcol.ColumnName);
            //        //    str = "\t";
            //        //}
            //        //context.Response.Write("\n");
            //        //foreach (DataRow dr in dt.Rows)
            //        //{
            //        //    str = "";
            //        //    for (int j = 0; j < dt.Columns.Count; j++)
            //        //    {
            //        //        context.Response.Write(str + System.Convert.ToString(dr[j]));
            //        //        str = "\t";
            //        //    }
            //        //    context.Response.Write("\n");
            //        //}
            //        //context.Response.End();
            //    }

            //}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

}