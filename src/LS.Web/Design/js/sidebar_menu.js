$("#menu-toggle").click(function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
$("#menu-toggle-2").click(function (e) {

    e.preventDefault();
    var getClassName = $('#wrapper').attr('class');
    if (getClassName == "container-fluid") {
        $('#menu').css("display", "none");
        $('#sidebar-wrapper').css("border", "#fff solid 1px");
        $('#showName').css("color", "#091f3f");

    } else if (getClassName == "container-fluid toggled-2") {
        $('#menu').css("display", "block");
        $('#sidebar-wrapper').css("border", "#ddd solid 1px");
        $('#showName').css("color", "#fff");
    }
    if ($('#wrapper').find('#sidebar-wrapper1').length!=0 && $('#wrapper').find('#sidebar-wrapper1')[0].style.display == "none")
    {
        $('#wrapper').find('#sidebar-wrapper1').show();
    }
    else {
        $('#wrapper').find('#sidebar-wrapper1').hide();
        $('#menu-toggle-2').show();
    }
    $("#wrapper").toggleClass("toggled-2");
    //$('#menu ul').hide();
});

function initMenu() {
    $('#sidebar-wrapper').hover(function (event) {
        //fix_sidemenu();
        //event.preventDefault();
        //$("#wrapper").toggleClass("toggled-2");
        //$('#menu ul').hide();
    });


    $('#menu ul').hide();
    $('#menu ul').children('.current').parent().show();
    //$('#menu ul:first').show();
    $('#menu li a').click(
      function () {
          var checkElement = $(this).next();
          if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
              return false;
          }
          if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
              $('#menu ul:visible').slideUp('normal');
              checkElement.slideDown('normal');
              return false;
          }
      }
      );
}
$(document).ready(function () { initMenu(); });