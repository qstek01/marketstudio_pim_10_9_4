
$(document).ready(function () {
    $("#files").kendoUpload({
        async: {
            saveUrl: "save",
            removeUrl: "remove",
            autoUpload: false
        },
        multiple: false,
        select: function (e) {
            var fileInfo = e.files[0];
            var wrapper = this.wrapper;

            setTimeout(function () {
                addPreview(fileInfo, wrapper);
            });
        }
    });
});

function addPreview(file, wrapper) {
    var raw = file.rawFile;
    var reader = new FileReader();

    if (raw) {
        reader.onloadend = function () {
            var preview = $("<img class='image-preview'>").attr("src", this.result);

            wrapper.find(".k-file[data-uid='" + file.uid + "'] .k-file-extension-wrapper")
              .replaceWith(preview);
        };

        reader.readAsDataURL(raw);
    }
}

