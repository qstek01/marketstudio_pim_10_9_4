﻿LSApp.factory('DALdataFactory', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {
  
    var baseUrl = $("base").first().attr("href");
    var urlBase = baseUrl + 'api';

    var dataFactory = {};
    dataFactory.$rootScope = $rootScope;
    dataFactory.$q = $q;

    dataFactory.authenticateUser = function () {
        //$http.get(urlBase + '/AccountApi/GetCurretUserInfo').success(function (response) {
        //    $rootScope.currentUserInfo = response;
        //}).error(function (error) {
        //    //console.log(error);
        //});
    };
    dataFactory.getCategoryFamilyList = function (categoryId, option, resouceTypeId, familyId, userGroupId, userId) {
        return $http.get(urlBase + '/HomeApi/GetCategoryFamilyList?categoryId=' + categoryId + '&option=' + option + '&resouceTypeId=' + resouceTypeId + '&familyId=' + familyId + '&userGroupId=' + userGroupId + '&userId=' + userId);
    };

    dataFactory.getRegisteredUsersList = function (userType) {
        //
        return $http.post(urlBase + '/HomeApi/GetUsersList?userType=' + userType);
    };

    dataFactory.deleteUserRecord = function (userId) {
        //
        return $http.post(urlBase + '/HomeApi/DeleteUserRecord?userId=' + userId);
    };

    dataFactory.updateUserDetailsCall = function (updateDetails) {
        return $http.post(urlBase + '/HomeApi/UpdateUserDetails', JSON.stringify(updateDetails));
    };

    dataFactory.userGroupCreation = function (groupId, groupName) {
        return $http.post(urlBase + '/HomeApi/UserGroupCreation?groupName=' + groupName + '&groupId=' + groupId);
    };

    dataFactory.getUserGroupList = function () {
        return $http.post(urlBase + '/HomeApi/GetUserGroupList');
    };

    dataFactory.deleteUserGroup = function (groupId) {
        return $http.post(urlBase + '/HomeApi/DeleteUserGroup?groupId=' + groupId);
    };

    dataFactory.getAssociatedUsersList = function (groupId) {
        return $http.post(urlBase + '/HomeApi/GetAssociatedUsersList?groupId=' + groupId);
    };

    dataFactory.userGroupAssociationCall = function (groupId, selectionType, modifiedList) {
        return $http.post(urlBase + '/HomeApi/UserGroupAssociation?groupId=' + groupId + '&selectionType=' + selectionType, JSON.stringify(modifiedList));
    };

    dataFactory.getUserGroupRightsList = function (groupId) {
        return $http.post(urlBase + '/HomeApi/GetUsergroupRightsList?groupId=' + groupId);
    };

    dataFactory.userGroupRightsCall = function (groupId, attrId, changedVal, flag) {
        return $http.post(urlBase + '/HomeApi/UserGroupRightsUpdate?groupId=' + groupId + '&attrId=' + attrId + '&valueToUpdate=' + changedVal + '&flag=' + flag);
    };

    dataFactory.treeViewForGroupRights = function (CategoryId, userGroupId) {
        return $http.post(urlBase + '/HomeApi/TreeForGroupRights?categoryId=' + CategoryId + "&userGroupId=" + userGroupId);
    };

    dataFactory.saveHierarchyRightsCall = function (groupId, selectedIds) {
        return $http.post(urlBase + '/HomeApi/SaveHierarchyRights?userGroupId=' + groupId, JSON.stringify(selectedIds));
    };

    dataFactory.getBreadCrumbList = function (categoryId, familyId, productId, breadcrumbOption) {
        return $http.get(urlBase + '/HomeApi/GetBreadCrumbList?categoryId=' + categoryId + '&familyId=' + familyId + '&productId=' + productId + '&breadcrumbOption=' + breadcrumbOption);
    };
    dataFactory.getProductDetails = function (categoryId, familyId, productId, userId) {
        return $http.get(urlBase + '/HomeApi/GetProductDetails?categoryId=' + categoryId + '&familyId=' + familyId + '&productId=' + productId + '&userId=' + userId);
    };

    dataFactory.getLoggedUserInfo = function () {
        return $http.post(urlBase + '/HomeApi/GetLoggedUserInfo');
    };

    dataFactory.getImageDetails = function (image) {
        return $http.get(urlBase + '/HomeApi/GetImageDetails?image=' + image);
    };

    dataFactory.resourcePDFGen = function (familyId, productId, pdfType, pdfFor, attributeId, userId, cobrandFlag) {
        return $http.post(urlBase + '/HomeApi/GenerateResourcePDF?familyId=' + familyId + '&productId=' + productId + '&pdfType=' + pdfType + '&pdfFor=' + pdfFor + '&attributeId=' + attributeId + '&userId=' + userId + '&cobrandFlag=' + cobrandFlag);
    };

    dataFactory.addToCart = function (userId, attributeId, attributeName, familyId, productId, filePath) {
        return $http.post(urlBase + '/HomeApi/AddDetailsToCart?userId=' + userId + '&attributeId=' + attributeId + '&pricingClass=' + attributeName + '&familyId=' + familyId + '&productId=' + productId + '&filePath=' + filePath);
    };

    dataFactory.getShoppingCartData = function (userId) {
        return $http.post(urlBase + '/HomeApi/GetCartData?userId=' + userId);
    };

    dataFactory.getImageCompressDetails = function (itemNumber, downloadFormat, imageList) {
        return $http.post(urlBase + '/HomeApi/GetImageCompressDetails?itemNumber=' + itemNumber + '&downloadFormat=' + downloadFormat, JSON.stringify(imageList));
    };

    dataFactory.removeFromCart = function (userId, familyId, productId, attributeId) {
        return $http.post(urlBase + '/HomeApi/RemoveCartData?userId=' + userId + '&familyId=' + familyId + '&productId=' + productId + '&attributeId=' + attributeId);
    };

    dataFactory.updateCartData = function (userId, familyId, productId, attributeId, qty) {
        return $http.post(urlBase + '/HomeApi/UpdateCartData?userId=' + userId + '&familyId=' + familyId + '&productId=' + productId + '&attributeId=' + attributeId + '&qty=' + qty);
    };

    dataFactory.saveShippingAddress = function (userId, shippingAddDetails) {
        return $http.post(urlBase + '/HomeApi/SaveShippingAddress?userId=' + userId, JSON.stringify(shippingAddDetails));
    }

    dataFactory.orderFileGeneration = function (userId) {
        return $http.post(urlBase + '/HomeApi/OrderFileGeneration?userId=' + userId);
    };

    dataFactory.getSearchResults = function (searchText, userGroupId, userId, attributeId, attributeValue, attrFlag, pageNumber, prodPerPage, resourcePageNumber, prodResoucePerPage, selectedResourceFilterOption, filterAttrList) {
        return $http.post(urlBase + '/HomeApi/GetSearchResults?searchString=' + searchText + '&userGroupId=' + userGroupId + '&userId=' + userId + '&attributeId=' + attributeId + '&attributeValue=' + attributeValue + '&attrFlag=' + attrFlag + '&pageNumber=' + pageNumber + '&prodPerPage=' + prodPerPage + '&resourcePageNumber=' + resourcePageNumber + '&prodResoucePerPage=' + prodResoucePerPage + '&selectedResourceFilterOption=' + selectedResourceFilterOption, JSON.stringify(filterAttrList));
    };

    dataFactory.saveDefaultGroupDetails = function (groupId, checkVal, option) {
        return $http.post(urlBase + '/HomeApi/SaveDefaultGroupDetails?groupId=' + groupId + '&checkVal=' + checkVal + '&option=' + option);
    };
    ////dataFactory.updateLinkDetails = function (familyId, productId, option, userId, userGroupId, flagType, imageData) {
    ////    return $http.get(urlBase + '/HomeApi/UpdateLinkDetails?familyId=' + familyId + '&productId=' + productId + '&option=' + option + '&userId=' + userId + '&userGroupId=' + userGroupId, '&flagType=' + flagType, '&imageData=');
    ////};
    dataFactory.updateLinkDetails = function (objData) {
        return $http.post(urlBase + '/HomeApi/UpdateLinkDetails?', JSON.stringify(objData));
    };

    dataFactory.getLinkDetails = function (encryptLinkId) {
        return $http.get(urlBase + '/HomeApi/GetLinkDetails?encryptLinkId=' + encryptLinkId);
    };

    dataFactory.getGustProductList = function (categoryId, option, familyId, productId, userId) {
        return $http.get(urlBase + '/HomeApi/GetGustProductList?categoryId=' + categoryId + '&option=' + option + '&familyId=' + familyId + '&productId=' + productId + '&userId=' + userId);
    };

    dataFactory.userAgreementUpdate = function (userId) {
        return $http.post(urlBase + '/HomeApi/UserAgreementUpdate?userId=' + userId);
    };

    dataFactory.getCatalogAttribute = function (option, request) {
        return $http.post(urlBase + '/HomeApi/GetCatalogAttribute?option=' + option, JSON.stringify(request));
    };

    dataFactory.saveAttributes = function (SelectAttribute_Ids, request) {
        return $http.post(urlBase + '/HomeApi/SaveAttributes?SelectAttribute_Ids=' + SelectAttribute_Ids, JSON.stringify(request));
    };

    dataFactory.SaveFilterAttributes = function (filterOption, FilterAttribute_Ids) {
        return $http.post(urlBase + '/HomeApi/SaveFilterAttributes?option=' + filterOption + '&SelectAttribute_Ids=' + FilterAttribute_Ids);
    };

    dataFactory.removeSelectedAttributeGrid = function (SelectAttribute_Ids) {
        return $http.post(urlBase + '/HomeApi/RemoveSelectedAttributeGrid?SelectAttribute_Ids=' + SelectAttribute_Ids);
    };

    dataFactory.attributeSortUpDownward = function (filterOption, attributeId) {
        return $http.get(urlBase + '/HomeApi/AttributeSortUpDownward?option=' + filterOption + '&attributeId=' + attributeId);
    };

    dataFactory.sendFeedBackDetails = function (feedBackData) {
        return $http.post(urlBase + '/HomeApi/CustomerFeedBackProcess', JSON.stringify(feedBackData));
    };

    dataFactory.checkFileExits = function (filePath) {
        return $http.post(urlBase + '/HomeApi/CheckFileExits?filePath=' + filePath);
    };

    dataFactory.cartItemTypeCheck = function (categoryId, familyId, productId) {
        return $http.post(urlBase + '/HomeApi/CartItemTypeCheck?categoryId=' + categoryId + '&familyId=' + familyId + '&productId=' + productId);
    };
    dataFactory.sendResetEmailNotification = function (data) {
        return $http.post(urlBase + '/HomeApi/SendResetEmailNotification', JSON.stringify(data));
    };
    dataFactory.getTemplateDetails = function () {
        return $http.post(urlBase + '/HomeApi/GetTemplateDetails');
    };
    dataFactory.saveFileNameDetails = function (data) {
        return $http.post(urlBase + '/HomeApi/SaveFileNameDetails', JSON.stringify(data));
    };
    dataFactory.getFamilyNameDetailList = function () {
        return $http.post(urlBase + '/HomeApi/GetFamilyNameDetailList');
    };
    dataFactory.deleteFileNameDetails = function (fileId) {
        return $http.post(urlBase + '/HomeApi/DeleteFileNameDetails?fileId=' + fileId);
    };
    dataFactory.userStatusUpdateSave = function (userType, selectionType, userData) {
        return $http.post(urlBase + '/HomeApi/UserStatusUpdateSave?userType=' + userType + '&selectionType=' + selectionType, JSON.stringify(userData));
    };

    dataFactory.treeViewForCollections = function (CategoryId) {
        return $http.post(urlBase + '/HomeApi/TreeForCollections?categoryId=' + CategoryId);
    };

    dataFactory.groupEmailNotification = function (groupMailData) {
        return $http.post(urlBase + '/HomeApi/GroupMailNotification', JSON.stringify(groupMailData));

    };
    dataFactory.getUserGroupDetails = function (option, userId) {
        //
        return $http.post(urlBase + '/HomeApi/GetUserGroupDetails?option=' + option + '&userId=' + userId);
    };
    dataFactory.removeAttachmentFile = function (fileName) {
        return $http.post(urlBase + '/HomeApi/RemoveAttachmentFile?fileName=' + fileName);
    };
    dataFactory.createNewCollectionDetails = function (userId, collection, option) {
        return $http.post(urlBase + '/HomeApi/CreateNewCollectionDetails?userId=' + userId + '&collection=' + collection + '&option=' + option);
    };
    dataFactory.getCollectionList = function (userId) {
        return $http.post(urlBase + '/HomeApi/GetCollectionList?userId=' + userId);
    };
    dataFactory.deleteCollection = function (userId, collectionId) {
        return $http.post(urlBase + '/HomeApi/DeleteCollectionDetails?userId=' + userId + '&collectionId=' + collectionId);
    };
    dataFactory.importTemplateDetails = function () {
        return $http.post(urlBase + '/ImportApi/GetImportTemplateDetails');
    };
    dataFactory.updateSortOrder = function (userId, collections) {
        return $http.post(urlBase + '/HomeApi/UpdateSortOrder?userId=' + userId, JSON.stringify(collections));
    };
    dataFactory.getExcelFileDetails = function (userId, templateName, categoryId, selectedFamilyId) {
        return $http.post(urlBase + '/ImportApi/GetExcelFileDetails?userId=' + userId + '&templateName=' + templateName + '&categoryId=' + categoryId + '&selectedFamilyId=' + selectedFamilyId);
    };
    dataFactory.updateSortOrder = function (userId, collections) {
        return $http.post(urlBase + '/HomeApi/UpdateSortOrder?userId=' + userId, JSON.stringify(collections));
    };
    dataFactory.loadProductData = function (userId, productCnt, attributes) {
        return $http.post(urlBase + '/ImportApi/LoadProductDataDetails?userId=' + userId + '&productCnt=' + productCnt, JSON.stringify(attributes));
    };

    dataFactory.treeViewForInputPage = function (CategoryId) {
        return $http.post(urlBase + '/ImportApi/TreeForInputPage?categoryId=' + CategoryId);
    };
    dataFactory.getFullHierarchyDetails = function (catId) {
        return $http.post(urlBase + '/ImportApi/GetFullHierarchyDetails?categoryId=' + catId);
    };
    dataFactory.getSelectedTemplateDetails = function (templateId) {
        return $http.post(urlBase + '/ImportApi/GetSelectedTemplateDetails?templateId=' + templateId);
    };
    dataFactory.addNewRowProduct = function (templateNmae, selectionType,option, selectedRow, selectedFamilyId, prodData) {
        return $http.post(urlBase + '/ImportApi/AddNewRowProduct?templateNmae=' + templateNmae + '&selectionType=' + selectionType + '&option=' + option + '&selectedRow=' + selectedRow + '&selectedFamilyId=' + selectedFamilyId, JSON.stringify(prodData));
    };
    dataFactory.saveFamilyDetails = function (option, userId, familyName, selectedCategory, prodRow, selectedExcel, templateId, hierarchyDetails, prodData) {
        return $http.post(urlBase + '/ImportApi/SaveFamilyDetails?option=' + option + '&userId=' + userId + '&familyName=' + familyName + '&selectedCategory=' + selectedCategory + '&prodRow=' + prodRow + '&selectedExcel=' + selectedExcel + '&templateId=' + templateId + '&selectedHierarchy=' + hierarchyDetails, JSON.stringify(prodData));
    };
    dataFactory.checkFamilyExistsData = function (familyName, leastCatName) {
        return $http.post(urlBase + '/ImportApi/CheckFamilyExistsData?FamilyName=' + familyName + '&leastCatName=' + leastCatName);
    };
    dataFactory.getFamilyData = function (familyName) {
        return $http.post(urlBase + '/ImportApi/GetExistFamilyData?FamilyName=' + familyName);
    };
    dataFactory.loadProductDetails = function (templatePath, familyName, userId, productId, selectedFamilyId, existFlag) {
        return $http.post(urlBase + '/ImportApi/LoadProductData?templatePath=' + templatePath + '&familyName=' + familyName + '&userId=' + userId + '&productId=' + productId + '&selectedFamilyId=' + selectedFamilyId + '&existFlag=' + existFlag);
    };
    dataFactory.checkFamilyFile = function (templatePath, userId) {
        return $http.post(urlBase + '/ImportApi/checkFamilyFile?templatePath=' + templatePath + '&userId=' + userId);
    };
    dataFactory.getPickListData = function (picklistdata) {
        return $http.post(urlBase + '/ImportApi/GetPicklistDatabyAttribute?pkData=' + picklistdata);
    };
    dataFactory.GetImportSpecs = function (sheetName, excelPath) {
        return $http.get(urlBase + '/ImportApi/GetImportSpecs?SheetName=' + sheetName + '&ExcelPath=' + excelPath);
    };
    dataFactory.getImportSpecs = function (sheetName, excelPath, templateId, importType, selectAll) {
        if (selectAll == '' || selectAll == "" || selectAll == undefined) {
            selectAll = 0;
        }
        return $http.get(urlBase + '/AdvanceImportApi/GetImportSpecs?SheetName=' + sheetName + '&ExcelPath=' + excelPath + "&templateId=" + templateId + "&importType=" + importType + "&selectAll=" + selectAll);
    };
    dataFactory.GetImportStatus = function (excelPath, sheetName) {
        return $http.get(urlBase + '/AdvanceImportApi/GetImportStatus?excelPath=' + excelPath + '&sheetName=' + sheetName);
    };
    dataFactory.finishImport = function (sessionId, Sheet, allowduplicate, importType, catalogId, Path, batch, scheduleDate, data) {
        return $http.post(urlBase + '/AdvanceImportApi/finishImport?sessionId=' + sessionId + '&SheetName=' + Sheet + '&allowDuplicate=' + allowduplicate + '&catalogId=' + catalogId + '&importType=' + importType + '&excelPath=' + Path + '&batch=' + batch + '&scheduleDate=' + scheduleDate, JSON.stringify(data));
    };
    dataFactory.Validateimport = function (Sheet, allowduplicate, Path, importtype, catalogId, Errorlogoutputformat, validation, data) {
        var importData = [];
        importData.push(data);
        importData.push(validation);
        return $http.post(urlBase + '/CSImportApi/Validateimport?SheetName=' + Sheet + '&allowDuplicate=' + allowduplicate + '&excelPath=' + Path + '&importtype=' + importtype + '&catalogId=' + catalogId + '&Errorlogoutputformat=' + Errorlogoutputformat + '&Errorlogoutputformat=' + Errorlogoutputformat, JSON.stringify(importData));
    };
    dataFactory.updateImportValidationFlag = function (selectedTemplate, importTypeSelection, validationStatus, sheetName) {

        return $http.post(urlBase + '/CSImportApi/UpdateImportValidationFlag1?selectedTemplate=' + selectedTemplate + '&importTypeSelection=' + importTypeSelection + '&validationStatus=' + validationStatus + '&sheetName=' + sheetName);
    };
    dataFactory.getSpecSheets = function (selectedTemplate) {


        return $http.post(urlBase + '/ImportApi/getSpecSheets?filepath=' + selectedTemplate);
    };
    dataFactory.getCatTemplateDetails = function (catId) {
        return $http.post(urlBase + '/ImportApi/GetCatTemplateDetails?catergoryId=' + catId);
    };
    //dataFactory.saveExcelDetails = function (option, userId, familyName, selectedCategory, prodRow, selectedExcel, templateId, hierarchyDetails, selectedFamilyId, prodData) {
    //    return $http.post(urlBase + '/ImportApi/SaveExcelDetails?option=' + option + '&userId=' + userId + '&familyName=' + familyName + '&selectedCategory=' + selectedCategory + '&prodRow=' + prodRow + '&selectedExcel=' + selectedExcel + '&templateId=' + templateId + '&selectedHierarchy=' + hierarchyDetails + '&selectedFamilyId=' + selectedFamilyId, JSON.stringify(prodData));
    //};

    dataFactory.saveExcelDetails = function (option, userId, familyName, selectedCategory, prodRow, selectedExcel, templateId,hierarchyDetails, selectedFamilyId, prodData) {
        return $http.post(urlBase + '/ImportApi/SaveExcelDetails?option=' + option + '&userId=' + userId + '&familyName=' + familyName + '&selectedCategory=' + selectedCategory + '&prodRow=' + prodRow + '&selectedExcel=' + selectedExcel + '&templateId=' + templateId + '&selectedHierarchy=' + hierarchyDetails + '&selectedFamilyId=' + selectedFamilyId, JSON.stringify(prodData));
    };
    dataFactory.checkImportFile = function () {
        return $http.post(urlBase + '/ImportApi/CheckImportFile');
    };
    //dataFactory.InputAssetUpload = function (commonPath) {
    //    return $http.post(urlBase + '/ImportApi/InputAssetUpload?commonPath=' + commonPath);
    //};


    ///view log by Aswin kumar

    dataFactory.validateImportResults = function (SessionId, importType) {

        return $http.get(urlBase + '/CSImportApi/ValidateImportResults?SessionId=' + SessionId + '&importType=' + importType);
    };

    dataFactory.getFinishImportSuccessResults = function (SessionId, option) {
        return $http.get(urlBase + '/CSImportApi/getFinishImportSuccessResults?SessionId=' + SessionId + "&option=" + option);
    };
    dataFactory.getFinishImportResults = function (SessionId) {
        return $http.get(urlBase + '/ImportApi/getFinishImportResults?SessionId=' + SessionId);
    };
    dataFactory.GetItemImportErrorlist = function () {
        return $http.get(urlBase + '/AdvanceImportApi/GetItemImportErrorlist')
    }

    ///view log by Aswin kumar
    dataFactory.getSampleSpecSheet = function (categoryId) {
        return $http.get(urlBase + '/ImportApi/GetSampleSpecSheet?categoryId=' + categoryId)
    }
    //dataFactory.deleteFamilyUpdate = function (selectedFamilyId) {
    //    return $http.post(urlBase + '/ImportApi/DeletedFamily?selectedFamilyId=' + selectedFamilyId);
    //};
    dataFactory.fileClearProcess = function (excelPath) {
        return $http.post(urlBase + '/ImportApi/FileClearProcess?excelPath=' + excelPath);
    };
    dataFactory.itemNumberCheck = function (itemNo, familyId, templateId) {
        return $http.post(urlBase + '/ImportApi/ItemNoCheck?itemNo=' + itemNo + '&familyId=' + familyId + '&templateId=' + templateId);
    };

    ///view log by Aswin kumar

    dataFactory.validateImportResults = function (SessionId, importType) {

        return $http.get(urlBase + '/CSImportApi/ValidateImportResults?SessionId=' + SessionId + '&importType=' + importType);
    };

    dataFactory.getFinishImportSuccessResults = function (SessionId, option) {
        return $http.get(urlBase + '/CSImportApi/getFinishImportSuccessResults?SessionId=' + SessionId + "&option=" + option);
    };
    dataFactory.getFinishImportResults = function (SessionId) {
        return $http.get(urlBase + '/CSImportApi/getFinishImportResults?SessionId=' + SessionId);
    };
    dataFactory.GetItemImportErrorlist = function () {
        return $http.get(urlBase + '/AdvanceImportApi/GetItemImportErrorlist')
    };

     dataFactory.CustomerDisplayItemNumber = function () {
        return $http.get(urlBase + '/AdvanceImportApi/CustomerDisplayItemNumber')
    };

    ///view log by Aswin kumar
     dataFactory.checkUploadedFile = function (filePath, path, exitPath, familyName, fromSize, option, folderPath, clickingOption) {
         return $http.post(urlBase + '/ImportApi/checkUploadedFile?file=' + filePath + '&path=' + path + '&exitPath=' + exitPath + '&familyName=' + familyName + '&fromSize=' + fromSize + '&option=' + option + '&folderPath=' + folderPath + '&clickingOption=' + clickingOption);
    };
    dataFactory.getAvailableAssetList = function (folderPath) {
        return $http.post(urlBase + '/ImportApi/GetAvailableAssetList?folderPath=' + folderPath);
    };
    dataFactory.getAssetMapAttributes = function (mapType, templateId) {
        return $http.post(urlBase + '/ImportApi/GetAssetMapAttributes?mappingType=' + mapType + '&templateId=' + templateId);
    };
    dataFactory.productSheetDelete = function (templatePath, familyId) {
        return $http.post(urlBase + '/ImportApi/ProductSheetDelete?templatePath=' + templatePath + '&familyId=' + familyId);
    };
    dataFactory.getTabNames = function (templatePath) {
        return $http.post(urlBase + '/ImportApi/GetTabNames?templatePath=' + templatePath);
    };
    ///Excelfile  delete after import////
    dataFactory.excelFileDeleteAfterImport = function (excelPath) {
        return $http.get(urlBase + '/AdvanceImportApi/ExcelFileDeleteAfterImport?excelPath=' + excelPath)
    };
    ///Excelfile  delete after import////
    dataFactory.saveAssetMappingDetails = function (mappingType, mappingFor, mappingData) {
        return $http.post(urlBase + '/ImportApi/SaveAssetMappingDetails?MappingType=' + mappingType + '&MappingFor=' + mappingFor, JSON.stringify(mappingData));
    };
    dataFactory.getSharePathFolder = function (userId, templateId, folderPath) {
        return $http.post(urlBase + '/ImportApi/GetSharePathFolder?userId=' + userId + '&templateId=' + templateId + '&folderPath=' + folderPath);
    };
    dataFactory.getAvailableFolderDetails = function (newFolderpath) {
        return $http.post(urlBase + '/ImportApi/GetAvailableFolderDetails?NewFolderPath=' + newFolderpath);
    }
    dataFactory.createNewFolder = function (folderName, newFolderpath) {
        return $http.post(urlBase + '/ImportApi/CreateNewFolder?NewFolderPath=' + newFolderpath + '&folderName=' + folderName);
    }
    dataFactory.getSharePathFolder = function (userId, templateId, folderPath,defaultFolderOption) {
        return $http.post(urlBase + '/ImportApi/GetSharePathFolder?userId=' + userId + '&templateId=' + templateId + '&folderPath=' + folderPath + '&defaultFolderOption=' + defaultFolderOption);
    };
    dataFactory.getConvertName = function (folderName) {
        return $http.post(urlBase + '/ImportApi/GetConvertName?folderName=' + folderName);
    };
    dataFactory.getAttributeGroup = function (templateNmae, categoryId, option) {
        return $http.post(urlBase + '/ImportApi/GetAttributeGroup?templateNmae=' + templateNmae + '&categoryId=' + categoryId + '&option=' + option);
    };
    //--------------------Attribute Group Sort order Page-----------------------------Start------------------------------------//
    dataFactory.getAttrGroupSortDetails = function (templateName) {
        return $http.post(urlBase + '/HomeApi/GetAttrGroupSortDetails?templateName=' + templateName );
    };
    dataFactory.attrGroupSortChange = function (templateName, selectedGroupId, sortType) {
        return $http.post(urlBase + '/HomeApi/AttrGroupSortChange?templateName=' + templateName + '&selectedGroupId=' + selectedGroupId + '&sortType=' + sortType);
    };
    dataFactory.attributeSortTreeView = function (categoryId) {
        return $http.post(urlBase + '/HomeApi/AttributeSortTreeView?categoryId=' + categoryId);
    };
    dataFactory.getTemplateAttrGroupSortDetails = function (templateName) {
        return $http.post(urlBase + '/HomeApi/GetTemplateAttrGroupSortDetails?templateName=' + templateName);
    };
    dataFactory.attributeGroupManageProcess = function (templateName, manageGroupId, manageCommand) {
        return $http.post(urlBase + '/HomeApi/AttributeGroupManageProcess?templateName=' + templateName  + '&manageGroupId=' + manageGroupId + '&manageCommand=' + manageCommand);
    };
    dataFactory.updateGroupDisplayName = function (templateName, groupId, displayName) {
        return $http.post(urlBase + '/HomeApi/UpdateGroupDisplayName?templateName=' + templateName + '&groupId=' + groupId + '&displayName=' + displayName);
    };
    dataFactory.getPriceClassList = function () {
        return $http.post(urlBase + '/LineCardApi/GetPriceClassDetails');
    };
    //--------------------Attribute Group Sort order Page-----------------------------End------------------------------------//
    dataFactory.getMatixList = function (categoryId,option) {
        return $http.post(urlBase + '/LineCardApi/GetMatixList?categoryId=' + categoryId + '&option=' + option);
    };
    dataFactory.treeViewForLineCard = function (CategoryId) {
        return $http.post(urlBase + '/LineCardApi/TreeViewForLineCard?categoryId=' + CategoryId);
    };
    dataFactory.rightSideTreeViewForLineCard = function (CategoryId) {
        return $http.post(urlBase + '/LineCardApi/RightSideTreeViewForLineCard?categoryId=' + CategoryId);
    };
    //==============================================================Analytics function call===============================Start==========================================//
    dataFactory.pdfAnalyticsDetailsEntry = function (pdfPath, analyticType, userId, fileType) {
        return $http.post(urlBase + '/AnalyticsApi/PDFanalyticsEntryCall?pdfPath=' + pdfPath + '&analyticType=' + analyticType + '&userId=' + userId + '&fileType=' + fileType);
    };
    dataFactory.productAnalyticsDetailsEntry = function (itemNumber, userId) {
        return $http.post(urlBase + '/AnalyticsApi/ProductAnalyticsEntryCall?itemNumber=' + itemNumber + '&userId=' + userId);
    };
    dataFactory.getAnalyticsGridDetails = function (optionVal, filterFromDate, filterToDate) {
        return $http.post(urlBase + '/AnalyticsApi/GetAnalyticsGridDetails?optionVal=' + optionVal + '&filterFromDate=' + filterFromDate + '&filterToDate=' + filterToDate);
    };
      //==============================================================Analytics function call=================================End==========================================//
    //==============================================================LineCard function call=================================End==========================================//
    dataFactory.checkLineCardInputTemplate = function (familyId) {
        return $http.post(urlBase + '/LineCardApi/CheckLineCardInputTemplate?familyId=' + familyId);
    };
    dataFactory.lineCardPDFGeneration = function (generationType, lineCardData) {
        return $http.post(urlBase + '/LineCardApi/LineCardPDFGeneration?generationType=' + generationType, JSON.stringify(lineCardData));
    };
    //==============================================================LineCard function call=================================End==========================================//
    //==================================================================GetMRTFileList=======================================================================//
    dataFactory.getMRTFilesList = function () {
        return $http.post(urlBase + '/LineCardApi/GetMRTFilesList');
    };
    dataFactory.getAttrSortDetails = function (templateName, selectedGroupId, selectedGroupRef) {
        return $http.post(urlBase + '/HomeApi/GetAttrSortDetails?templateName=' + templateName + '&selectedGroupId=' + selectedGroupId + '&selectedGroupRef=' + selectedGroupRef);
    };
    dataFactory.attributeManageProcess = function (templateName, manageAttrId, manageCommand, selectedGroupRef) {
        return $http.post(urlBase + '/HomeApi/AttributeManageProcess?templateName=' + templateName + '&manageAttrId=' + manageAttrId + '&manageCommand=' + manageCommand + '&selectedGroupRef=' + selectedGroupRef);
    };
    dataFactory.getTemplategGroupAttrSortDetails = function (templateName, selectedGroupId, selectedGroupRef) {
        return $http.post(urlBase + '/HomeApi/GetTemplategGroupAttrSortDetails?templateName=' + templateName + '&selectedGroupId=' + selectedGroupId + '&selectedGroupRef=' + selectedGroupRef);
    };
    dataFactory.updateGroupAttrDisplayName = function (selectedGroupRef, selectedAttrId, displayName) {
        return $http.post(urlBase + '/HomeApi/UpdateGroupAttrDisplayName?selectedGroupRef=' + selectedGroupRef + '&selectedAttrId=' + selectedAttrId + '&displayName=' + displayName);
    };
    dataFactory.groupedAttrSortChange = function (selectedGroupRef, selectedAttrId, sortType) {
        return $http.post(urlBase + '/HomeApi/GroupedAttrSortChange?selectedGroupRef=' + selectedGroupRef + '&selectedAttrId=' + selectedAttrId + '&sortType=' + sortType);
    };
    dataFactory.savePriceClassDetails = function (templateName, selectedPriceClass) {
        return $http.post(urlBase + '/LineCardApi/SavePriceClassDetails?templateName=' + templateName + '&selectedPriceClass=' + selectedPriceClass);
    };
    return dataFactory;
}]);
