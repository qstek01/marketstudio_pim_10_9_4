﻿LSApp.factory('dataFactory', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {

    var baseUrl = $("base").first().attr("href");
    var urlBase = baseUrl + 'api';

    var base = '';
    var dataFactory = {};
    dataFactory.$rootScope = $rootScope;
    dataFactory.$q = $q;

    dataFactory.authenticateUser = function () {
      
        $http.get(urlBase + '/AccountApi/GetCurretUserInfo').success(function (response) {
            $rootScope.currentUserInfo = response;
        }).error(function (error) {
            //console.log(error);
        });
    };
    dataFactory.GetAllFamilyId = function (projectId, recordId, fileNo) {
        return $http.get(baseUrl + '/Family/GetAllFamilyId?projectId=' + projectId + '&recordId=' + recordId + '&fileNo=' + fileNo);
    };
    dataFactory.GetCurretUserInfo = function () {
        return $http.get(urlBase + '/AccountApi/GetCurretUserInfo');
    };

    dataFactory.getCustomerTableDetailsByCurrentUser = function () {
        return $http.get(urlBase + '/AccountApi/GetCustomerTableDetailsByCurrentUser');
    };

    dataFactory.changePassword = function (request) {
        return $http.post(urlBase + '/AccountApi/ChangePassword', JSON.stringify(request));
    };
    dataFactory.getCustomerById = function (customerId) {
        return $http.get(urlBase + '/AccountApi/GetCustomerById?customerId=' + customerId);
    };
    dataFactory.addAppUser = function (request) {
        return $http.post(urlBase + '/AccountApi/AddAppUser', JSON.stringify(request));
    };
    dataFactory.updateAppUser = function (request) {
        return $http.post(urlBase + '/AccountApi/UpdateAppUser', JSON.stringify(request));
    };
    dataFactory.getUsers = function (criteria) {
        return $http.post(urlBase + '/AccountApi/UserList', JSON.stringify(criteria));
    };
    dataFactory.getAllRoles = function () {
        return $http.get(urlBase + '/AccountApi/GetAllRoles');
    };
    dataFactory.getUsers = function (request) {
        return $http.post(urlBase + '/AccountApi/GetAppUsers', JSON.stringify(request));
    };
    dataFactory.getUserList = function (cusid,filter) {
        return $http.get(urlBase + '/FamilyApi/GetUsers?CustomerId=' + cusid + '&filter=' + filter);
    };

    dataFactory.getMyprofileList = function (cusid) {
        return $http.get(urlBase + '/FamilyApi/getMyprofileList?Customerid=' + cusid);
    };
    dataFactory.getUserWebList = function () {
        return $http.get(urlBase + '/UserAdminApi/getUserWebList');
    };
    dataFactory.selectrowevent = function (Email_id) {
        return $http.get(urlBase + '/UserAdminApi/selectrowevent?Email_id=' + Email_id);
    };
    dataFactory.dropdownchange = function (value) {
        return $http.get(urlBase + '/UserAdminApi/dropdownchange?value=' + value);
    };
    dataFactory.dropdownRoleList = function (value,emailId) {
        return $http.get(urlBase + '/UserAdminApi/dropdownRoleList?value='+value + '&emailId=' + emailId );
    };
    dataFactory.saveUserWebdetails = function (value, formdatas) {
        return $http.post(urlBase + '/UserAdminApi/saveUserWebdetails?value=' + value, JSON.stringify(formdatas));
    };
    dataFactory.getMyprofileListStatus = function (username) {
        return $http.get(urlBase + '/FamilyApi/getMyprofileListStatus?username=' + username);
    };


    dataFactory.GetUsersFordropdown = function () {
        return $http.get(urlBase + '/FamilyApi/GetUsersFordropdown');
    };


    dataFactory.getUserListForWorkFlow = function () {
        return $http.get(urlBase + '/FamilyApi/GetUsersForWorkFlow');
    };

    dataFactory.getUserRolesList = function (cusid) {
        return $http.get(urlBase + '/FamilyApi/GetUsersRolesList?Customerid=' + cusid);
    };
    dataFactory.getUsersFunctionAllowedData = function (username, request) {

        return $http.get(urlBase + '/UserAdminApi/GetUsersFunctionAllowedData?username=' + username, JSON.stringify(request));
    };

    dataFactory.GetCompanyList = function () {
        return $http.get(urlBase + '/UserAdminApi/GetCompanyList');
    };


    dataFactory.saveModifiedUserData = function (request) {
        return $http.post(urlBase + '/UserAdminApi/SaveModifiedUserData', JSON.stringify(request));
    };

    dataFactory.saveUserRoleList = function (funcid, roleid, option, value) {
        return $http.post(urlBase + '/UserAdminApi/saveUserRoleList?FuncId=' + funcid + '&RoleId=' + roleid + '&Option=' + option + '&Value=' + value);
    };

    dataFactory.updateReadonlyattributes = function (roleid, attrid, value) {
        return $http.post(urlBase + '/UserAdminApi/updateReadonlyattributes?RoleId=' + roleid + '&Attrid=' + attrid + '&Value=' + value);
    };

    dataFactory.deleteRole = function (roleid) {
        return $http.get(urlBase + '/UserAdminApi/deleteRole?RoleId=' + roleid);
    };

    dataFactory.getAllCatalogs = function (customerId) {
        return $http.get(urlBase + '/UserAdminApi/GetAllCatalogs?customerId=' + customerId);
    };

    dataFactory.getAllCatalogsByRoleId = function (roleId, customerId, request) {
        return $http.post(urlBase + '/UserAdminApi/GetAllCatalogsByRoleId?roleId=' + roleId + '&customerId=' + customerId, JSON.stringify(request));
    };

    dataFactory.updatecustomerrolecatalog = function (roleId, customerId, catalogid, activeflag, flag) {
        return $http.post(urlBase + '/UserAdminApi/Updatecustomerrolecatalog?roleId=' + roleId + '&customerId=' + customerId + '&catalogid=' + catalogid + '&activeflag=' + activeflag + '&flag=' + flag);
    };


    dataFactory.getPlanFunctionAllowedData = function (planId) {
        return $http.get(urlBase + '/UserAdminApi/GetPlanFunctionAllowedData?planId=' + planId);
    };

    dataFactory.getNewPlanFunctionAllowedData = function () {
        return $http.get(urlBase + '/UserAdminApi/GetNewPlanFunctionAllowedData');
    };

    dataFactory.getUsersRolesFunctionAllowedData = function (roleId) {
        return $http.get(urlBase + '/UserAdminApi/GetUsersRolesFunctionAllowedData?RoleId=' + roleId);
    };

    dataFactory.updateUserRolesAllowedFunction = function (request) {
        return $http.post(urlBase + '/UserAdminApi/UpdateUserRolesAllowedFunction', JSON.stringify(request));
    };

    dataFactory.GetUsersRolesFunctionAllowedDatamodified = function (roleId, request) {
        return $http.post(urlBase + '/UserAdminApi/GetUsersRolesFunctionAllowedDatamodified?RoleId=' + roleId, JSON.stringify(request));
    };

    dataFactory.GetUsersRolesFunctionAllowedDatamodifiedforAttribute = function (roleId, request) {
        return $http.post(urlBase + '/UserAdminApi/GetUsersRolesFunctionAllowedDatamodifiedforAttribute?RoleId=' + roleId, JSON.stringify(request));
    };

    dataFactory.saveFunctionAllowedForPlan = function (request, planModel) {
        return $http.post(urlBase + '/SuperAdminConfigApi/SaveFunctionAllowedForPlan?PlanModel=' + planModel, JSON.stringify(request));
    };

    dataFactory.saveUserFunctionAllowed = function (request) {
        return $http.post(urlBase + '/UserAdminApi/SaveUserFunctionAllowed', JSON.stringify(request));
    };
    dataFactory.saveUserRoleFunctions = function (request) {
        return $http.post(urlBase + '/UserAdminApi/SaveUserRoleFunctions', JSON.stringify(request));
    };

    dataFactory.GetUserInvite = function (userInvitecustomerID) {
        return $http.get(urlBase + '/UserAdminApi/GetUserInvite?userInvitecustomerID=' + userInvitecustomerID);
    };
    dataFactory.SaveUserInvite = function (userInvitecustomerID, userEmailInvitation) {
        return $http.post(urlBase + '/UserAdminApi/SaveUserInvite?userInvitecustomerID=' + userInvitecustomerID, JSON.stringify(userEmailInvitation))
    };
    dataFactory.SendInviteToUser = function (userinviteid, customerid, sendInviteEmail) {
        return $http.post(urlBase + '/UserAdminApi/SendInviteToUser?userinviteid=' + userinviteid + '&customerid=' + customerid, JSON.stringify(sendInviteEmail));
    };

    dataFactory.saveModifiedInviteUserData = function (request) {
        return $http.post(urlBase + '/UserAdminApi/SaveModifiedUserInviteData', JSON.stringify(request));
    };

    dataFactory.getUsersRole = function () {
        return $http.get(urlBase + '/FamilyApi/GetUsersRole');
    };

    dataFactory.getUsersReadOnlyAttributeList = function (username) {
        return $http.get(urlBase + '/FamilyApi/GetUsersReadOnlyAttributeList?username=' + username);
    };

    dataFactory.getUserWorkFlow = function (cusid) {
        return $http.get(urlBase + '/FamilyApi/GetUserWorkFlow?CustomerId=' + cusid);
    };

    dataFactory.saveUserWorkFlow = function (request) {
        return $http.post(urlBase + '/FamilyApi/SaveUserWorkFlow', JSON.stringify(request));
    };

    dataFactory.saveuserdetails = function (request, roleNames) {
        return $http.post(urlBase + '/FamilyApi/SaveUser?roleNames=' + roleNames, JSON.stringify(request));
    };

    dataFactory.Updateuserdetails = function (request, roleNames) {
        return $http.post(urlBase + '/FamilyApi/UpdateUser?roleNames=' + roleNames, JSON.stringify(request));
    };
    dataFactory.saveuserrole = function (roleId) {
        return $http.post(urlBase + '/FamilyApi/SaveUserRole?RoleId=' + roleId);
    };

    dataFactory.getRecycleBinDataSource = function (selectType, userId, fromDate, toDate) {
        return $http.get(urlBase + '/RecycleBinApi/GetRecycleBinDataSource?SelectType=' + selectType + '&UserId=' + userId + '&FromDate=' + fromDate + '&ToDate=' + toDate);
    };

    dataFactory.RestoreSelectedItemsdetails = function (request) {
        return $http.post(urlBase + '/RecycleBinApi/RecycleBinRestoreSelectedItemsdetails', JSON.stringify(request));
    };
    dataFactory.RestoreSelectedTrashItemsdetails = function (request) {
        return $http.post(urlBase + '/RecycleBinApi/RecycleBinRestoreSelectedTrashItemsdetails', JSON.stringify(request));
    };

    dataFactory.PermanentlyRemoveSelecteddetails = function (request) {
        return $http.post(urlBase + '/RecycleBinApi/RecycleBinPermanentlyRemoveSelecteddetails', JSON.stringify(request));
    };

    dataFactory.emptyRecycleBindetails = function (request) {
        return $http.post(urlBase + '/RecycleBinApi/EmptyRecycleBindetails', JSON.stringify(request));
    };

    dataFactory.GetUserTimeZone = function () {
        return $http.get(urlBase + '/HomeApi/GetUserTimeZone');
    };

    dataFactory.GetSelecedtimeZoneChange = function (timezone) {
        return $http.post(urlBase + '/HomeApi/GetSelectedTimeZone', JSON.stringify(timezone));
    };


    dataFactory.GetCatalogs = function () {
        return $http.post(urlBase + '/HomeApi/GetCatalogs');
    };

    dataFactory.GetCatalogCountsForCatalogID = function (catalogid, EnableSubProduct) {

        return $http.get(urlBase + '/HomeApi/GetCatalogCountsForCatalogID?catalogid=' + catalogid + '&EnableSubProduct=' + EnableSubProduct);
    };

    dataFactory.GetRecentlyModifiedCountDetails = function (catalogid) {

        return $http.get(urlBase + '/HomeApi/GetRecentlyModifiedCountDetails?catalogid=' + catalogid);
    };

    dataFactory.GetSkuCount = function () {

        return $http.get(urlBase + '/HomeApi/GetSkuCount');
    };

    dataFactory.GetCustomerDetails = function () {

        return $http.get(urlBase + '/HomeApi/GetCustomerDetails');
    };

    dataFactory.getAllCategories = function (catalogId, categoryId, workingCatalogId) {
        return $http.get(urlBase + '/HomeApi/Categories?categoryId=' + categoryId + '&catalogid=' + catalogId + '&workingCatalogId=' + workingCatalogId);
    };
    dataFactory.getAllCategoriesTD = function (catalogId, categoryId) {
        return $http.get(urlBase + '/TableDesignerApi/CategoriesTD?categoryId=' + categoryId + '&catalogid=' + catalogId);
    };
    dataFactory.getAllCategoriesForWorkFlow = function (catalogId, categoryId, fromDate, toDate, workflowStaus, workflowUser) {

        return $http.get(urlBase + '/HomeApi/CategoriesForWorkFlow?catalogid=' + catalogId + '&categoryId=' + categoryId + '&fromDate=' + fromDate + '&toDate=' + toDate + '&workflowStaus=' + workflowStaus + '&workflowUser=' + workflowUser); //+ '&workflowStaus=' + workflowStaus + '&workflowUser=' + workflowUser
    };

    dataFactory.getCategories = function (catalogId, categoryId) {
        return $http.get(urlBase + '/HomeApi/wizardCategories?catalogid=' + catalogId + '&categoryId=' + categoryId);
    };




    dataFactory.getprodspecs = function (catalogId, id, displayID, categoryId, pageno, perpagecount, prod_id, packId, type,
      AttrValues, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues, val) {
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }

        if (type === undefined || type === '' || type === "") {
            type = 'null';
        }

        if (AttrValues === undefined || AttrValues === '' || AttrValues === "") {
            AttrValues = 'null';
        }
        if (productGridSearchValue === undefined || productGridSearchValue === '' || productGridSearchValue === "") {
            productGridSearchValue = 'null';
        }
        if (searchType === undefined || searchType === '' || searchType === "") {
            searchType = "0";
        }

        // To change the values

        if (ProdSpecsValues === undefined || ProdSpecsValues === '' || ProdSpecsValues === "") {
            ProdSpecsValues = true;
        }

        if (ProdPriceValues === undefined || ProdPriceValues === '' || ProdPriceValues === "") {
            ProdPriceValues = true;
        }
        if (ProdKeyValues === undefined || ProdKeyValues === '' || ProdKeyValues === "") {
            ProdKeyValues = true;
        }
        if (ProdImageValues === undefined || ProdImageValues === '' || ProdImageValues === "") {
            ProdImageValues = true;
        }


        // End




        return $http.get(urlBase + '/HomeApi/GetProdSpecs?catalogid=' + catalogId + '&familyIdString=' + id +
            '&displayID=' + displayID + '&CategoryId=' + categoryId + '&Pageno=' + pageno + '&Perpagecount=' + perpagecount +
            '&prod_id=' + prod_id + '&packId=' + packId +
             '&type=' + type + '&AttrValues=' + AttrValues +
              '&productGridSearchValue=' + productGridSearchValue + '&searchType=' + searchType +
                           '&ProdSpecsValues=' + ProdSpecsValues + '&ProdPriceValues=' + ProdPriceValues +
              '&ProdKeyValues=' + ProdKeyValues + '&ProdImageValues=' + ProdImageValues + '&val=' + val);
    };
    dataFactory.getprodspecsattributes = function (catalogId, id, displayID, categoryId, pageno, perpagecount, prod_id, packId, type,
       AttrValues, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues, val) {
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }

        if (type === undefined || type === '' || type === "") {
            type = 'null';
        }

        if (AttrValues === undefined || AttrValues === '' || AttrValues === "") {
            AttrValues = 'null';
        }
        if (productGridSearchValue === undefined || productGridSearchValue === '' || productGridSearchValue === "") {
            productGridSearchValue = 'null';
        }
        if (searchType === undefined || searchType === '' || searchType === "") {
            searchType = "0";
        }

        // To change the values

        if (ProdSpecsValues === undefined || ProdSpecsValues === '' || ProdSpecsValues === "") {
            ProdSpecsValues = true;
        }

        if (ProdPriceValues === undefined || ProdPriceValues === '' || ProdPriceValues === "") {
            ProdPriceValues = true;
        }
        if (ProdKeyValues === undefined || ProdKeyValues === '' || ProdKeyValues === "") {
            ProdKeyValues = true;
        }
        if (ProdImageValues === undefined || ProdImageValues === '' || ProdImageValues === "") {
            ProdImageValues = true;
        }


        // End




        return $http.get(urlBase + '/HomeApi/GetProdSpecsAttributes?catalogid=' + catalogId + '&familyIdString=' + id +
            '&displayID=' + displayID + '&CategoryId=' + categoryId + '&Pageno=' + pageno + '&Perpagecount=' + perpagecount +
            '&prod_id=' + prod_id + '&packId=' + packId +
             '&type=' + type + '&AttrValues=' + AttrValues +
              '&productGridSearchValue=' + productGridSearchValue + '&searchType=' + searchType +
                           '&ProdSpecsValues=' + ProdSpecsValues + '&ProdPriceValues=' + ProdPriceValues +
              '&ProdKeyValues=' + ProdKeyValues + '&ProdImageValues=' + ProdImageValues + '&val=' + val);
    };









    dataFactory.getFamilyspecification = function (catalogId, familyid, categoryId, packId) {
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }
        return $http.get(urlBase + '/FamilyApi/GetFamilySpecfication?catalogid=' + catalogId + '&familyIdString=' + familyid + '&CategoryId=' + categoryId + '&packId=' + packId);
    };

    dataFactory.getprodspecsWithoutAttributes = function (catalogId, id, displayID, categoryId) {

        return $http.get(urlBase + '/HomeApi/GetprodspecsWithoutAttributes?catalogid=' + catalogId + '&familyIdString=' + id + '&displayID=' + displayID + '&CategoryId=' + categoryId);
    };
    dataFactory.getprodconfigdata = function (catalogId, familyId, categoryId) {
        return $http.get(urlBase + '/HomeApi/getprodconfigdata?catalogId=' + catalogId + '&familyId=' + familyId + '&categoryId=' + categoryId);
    };
    dataFactory.createprodconfigdata = function (catalogId, familyId, categoryId, prodIds) {
        return $http.get(urlBase + '/HomeApi/createprodconfigdata?catalogid=' + catalogId + '&familyId=' + familyId + '&categoryId=' + categoryId + '&prodIds=' + prodIds);
    };
    dataFactory.GetProdConfigDataSelect = function (catalogId, familyId, categoryId, prodIds) {
        return $http.get(urlBase + '/HomeApi/GetProdConfigDataSelect?catalogid=' + catalogId + '&familyId=' + familyId + '&categoryId=' + categoryId + '&prodIds=' + prodIds);
    };
    dataFactory.getdynamicprodspecs = function (id) {
        return $http.get(urlBase + '/HomeApi/GetDynamicProdSpecs?id=' + id);
    };

    dataFactory.Getprodspecs = function (id) {
        return $http.get(urlBase + '/FamilyApi/Getprodspecs?id=' + id);
    };

    dataFactory.getfamilyspecs = function (id, catalogId) {
        return $http.get(urlBase + '/FamilyApi/GetFamilySpecs?id=' + id + '&catalogId=' + catalogId);
    };

    dataFactory.getfamilyspecsImgAttr = function (id, catalogId) {
        return $http.get(urlBase + '/FamilyApi/GetfamilyspecsImgAttr?id=' + id + '&catalogId=' + catalogId);
    };

    dataFactory.getRelatedfamily = function (familyid, catalogid) {
        return $http.get(urlBase + '/FamilyApi/GetRelatedfamily?id=' + familyid + '&catalogid=' + catalogid);
    };
    dataFactory.GetRelatedfamilyDetails = function (familyid, catalogid, categoryid, request) {
        return $http.post(urlBase + '/FamilyApi/GetRelatedfamilyDetails?id=' + familyid + '&catalogid=' + catalogid + '&categoryid=' + categoryid, JSON.stringify(request));
    };

    dataFactory.savecatalogdetails = function (request) {
        return $http.post(urlBase + '/HomeApi/SaveCatalog', JSON.stringify(request));
    };
    dataFactory.saveMirrorCatalogDetails = function (request) {
        return $http.post(urlBase + '/HomeApi/saveMirrorCatalog', JSON.stringify(request));
    };

    dataFactory.CheckCatalogNameExist = function (request) {
        return $http.post(urlBase + '/HomeApi/CheckCatalogNameExist', JSON.stringify(request));
    };

    dataFactory.deletecatalogdetails = function (request) {
        return $http.post(urlBase + '/HomeApi/DeleteCatalogDetails', JSON.stringify(request));
    };
    dataFactory.GetCatalogDetails = function (request, customerId) {
        if (customerId === undefined) {
            customerId = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCatalogDetailsForCurrentCustomer?customerId=' + customerId); //GetCatalogDetails
    };

    dataFactory.GetConverterDetails = function (request, customerId) {
        if (customerId === undefined) {
            customerId = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetconvertDetailsForCurrentCustomer?customerId=' + customerId); //GetCatalogDetails
    };
    dataFactory.GetCatalogDetailsforSearchCustomers = function (request, customerId) {
        if (customerId === undefined) {
            customerId = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCatalogDetailsForSearchCustomer?customerId=' + customerId); //GetCatalogDetails
    };
    dataFactory.GetCatalogDetailsUserRole = function (request, customerId, roleid) {
        return $http.get(urlBase + '/HomeApi/GetCatalogDetailsForCurrentCustomerUserRole?customerId=' + customerId + '&roleid=' + roleid); //GetCatalogDetails
    };
    dataFactory.DeleteProject = function (catalogId, projectId) {
        return $http.get(urlBase + '/HomeApi/DeleteExportProject?catalogId=' + catalogId + '&projectId=' + projectId);
    };
    dataFactory.GettemplateDetails = function (catalogid, option) {
        return $http.get(urlBase + '/HomeApi/GettemplateDetails?CatalogId=' + catalogid + '&option=' + option);
    };
    dataFactory.DeleteTemplate = function (projectId, catalogId) {
        return $http.post(urlBase + '/HomeApi/DeleteTemplate?ProjectId=' + projectId + '&CatalogId=' + catalogId);
    };
    dataFactory.OpenTemplate = function (projectId, catalogId, option) {
        return $http.get(urlBase + '/HomeApi/OpenTemplate?ProjectId=' + projectId + '&CatalogId=' + catalogId + '&option=' + option);
    };

    dataFactory.GetCatalogFamily = function (request, catalogId, customerId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetCatalogFamily?catalogId=' + catalogId + '&customerId=' + customerId);
    };

    dataFactory.GetFamilyDetailsForProductconfig = function (catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetFamilyDetailsForProductconfig?catalog_id=' + catalogId);
    };


    dataFactory.GetICalSAttributeList = function (request) {
        return $http.get(urlBase + '/HomeApi/GetIsCalSAttributeList');
    };
    dataFactory.GetCatalogDetailsForCurrentCustomer = function (request, customerId) {
        if (customerId === undefined) {
            customerId = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCatalogDetailsForCurrentCustomer?customerId=' + customerId);
    };

    dataFactory.GetAttributeDetailsForGlobal = function (attributetype, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetAttributeDetailsForGlobal?attributetype=' + attributetype + '&catalogid=' + catalogid);
    };
    dataFactory.GetCategorySortDetails = function (request, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCategorySortDetails?catalogid=' + catalogid + '&categoryid=' + request);
    };

    dataFactory.GetCatalogCategorySortDetails = function (catalogid, request) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCatalogCategorySortDetails?catalogid=' + catalogid + '&categoryid=' + request);
    };

    dataFactory.GetCategoriesForSearch = function (catalogid, request) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCategoriesForSearch?catalogid=' + catalogid + '&id=' + request);
    };
    dataFactory.GetCatalogCategoryDetails = function (catalogid, request, categoryid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        if (categoryid === undefined || categoryid === '' || categoryid === "") {
            categoryid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCatalogCategoryDetails?catalogid=' + catalogid + '&categoryid=' + request + '&selectedcategoryid=' + categoryid);
    };

    dataFactory.GetCategoryfamilyDetails = function (request, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCategoryfamilyDetails?catalogid=' + catalogid + '&categoryid=' + request);
    };

    dataFactory.GetCategorysubfamilyDetails = function (request, categoryid, catalogid, parentfamilyid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        if (categoryid === undefined || categoryid === '' || categoryid === "") {
            categoryid = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetCategorysubfamilyDetails?catalogid=' + catalogid + '&categoryid=' + categoryid + '&familyid=' + request + '&parentfamilyid=' + parentfamilyid);
    };

    dataFactory.GetCatalogfamilyattrlistDetails = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCatalogfamilyattrlistDetails?catalogid=' + catalogid);
    };

    dataFactory.GetCatalogproductattrlistDetails = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCatalogproductattrlistDetails?catalogid=' + catalogid);
    };

    dataFactory.savecatalogattrdetails = function (newselected, removed, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        var isrequest = { newselectedattr: newselected, removedattr: removed };
        return $http.post(urlBase + '/HomeApi/SaveCatalogAttr?id=' + catalogid, JSON.stringify(isrequest));
    };

    dataFactory.GetDynamicFamilySpecs = function (familyId, categoryId, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        if (categoryId === undefined || categoryId === '' || categoryId === "") {
            categoryId = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetDynamicFamilySpecs?FamilyId=' + familyId + '&CategoryId=' + categoryId + '&CatalogId=' + catalogid);
    };

    dataFactory.SaveProductSpecs = function (request, category_id, Parentproduct_Id, blurCheck, SaveProductSpecs) {
        var data = [];
        data.push(request);
        data.push(SaveProductSpecs);

        return $http.post(urlBase + '/FamilyApi/SaveProductSpecs?category_id=' + category_id + '&Parentproduct_Id=' + Parentproduct_Id + '&blurCheck=' + blurCheck, JSON.stringify(data));
    };
    dataFactory.SaveproductconfigSpecs = function (request, familyId, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.post(urlBase + '/FamilyApi/SaveproductconfigSpecs?familyId=' + familyId + '&catalogid=' + catalogid, JSON.stringify(request));
    };


    dataFactory.SaveFamilySpecs = function (catalogId, Category_id, request, SaveFamilySpecOld) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        var data = [];
        data.push(request);
        data.push(SaveFamilySpecOld);


        return $http.post(urlBase + '/FamilyApi/SaveFamilySpecs?catalogId=' + catalogId + '&Category_id=' + Category_id, JSON.stringify(data));
    };

    dataFactory.familyattrvalues = function (request, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + '/HomeApi/Getfamilyattrvalues?Attributeid=' + request + '&catalogid=' + catalogid);
    };

    dataFactory.productattrvalues = function (request, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + '/HomeApi/Getproductattrvalues?Attributeid=' + request + '&catalogid=' + catalogid);
    };

    dataFactory.savecategorysort = function (request, catalogid, categoryId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.post(urlBase + '/HomeApi/SaveCategorySort?id=' + catalogid + '&pCatgoryID=' + categoryId, JSON.stringify(request));
    };

    dataFactory.savefamilysort = function (request, catalogid, categoryId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.post(urlBase + '/HomeApi/SavefamilySort?id=' + catalogid + '&parentfamilyID=0', JSON.stringify(request));
    };

    dataFactory.savesubfamilysort = function (request, catalogid, parentfamilyId, subfamid) {
        return $http.post(urlBase + '/HomeApi/Savesubfamilysorts?id=' + catalogid + '&parentfamilyID=' + parentfamilyId + '&subfamid=' + subfamid, JSON.stringify(request));
    };

    dataFactory.savecategorysortasc = function (request, catalogid, categoryidvalue) {
        return $http.post(urlBase + '/HomeApi/SaveCategorySortall?id=' + catalogid + '&sorttype=' + request + '&sortindex=' + categoryidvalue);
    };

    dataFactory.savefamilysortasc = function (request, catalogid, categoryidvalue, attributeid, sortindex) {
        return $http.post(urlBase + '/HomeApi/savefamilysortasc?catalogid=' + catalogid + '&sorttype=' + request + '&categoryid=' + categoryidvalue + '&attributeid=' + attributeid + '&sortindex=' + sortindex);
    };
    dataFactory.updateCustomer = function (request) {
        return $http.post(urlBase + '/AccountApi/UpdateOrSaveCustomer', JSON.stringify(request));
    };

    dataFactory.customerValidation = function (Comments, CustomerId) {
        return $http.get(urlBase + '/AccountApi/customerValidation?Comments=' + Comments + '&CustomerId=' + CustomerId);
    };
    dataFactory.updateCatalog = function (customerId, request) {
        return $http.post(urlBase + '/AccountApi/UpdateCatalog?customerId=' + customerId, JSON.stringify(request));
    };
    dataFactory.deletecustomer = function (customerId) {
        return $http.post(urlBase + '/AccountApi/Deletecustomer?customerId=' + customerId);
    };
    dataFactory.deletePlan = function (planId) {
        return $http.post(urlBase + '/AccountApi/DeletePlan?planId=' + planId);
    };
    dataFactory.updatePlan = function (request) {
        return $http.post(urlBase + '/AccountApi/UpdateOrSavePlan', JSON.stringify(request));
    };

    dataFactory.getCustomers = function (request) {
        return $http.post(urlBase + '/AccountApi/GetCustomerList', JSON.stringify(request));
    };
    dataFactory.getCustomerName = function (custId) {
        return $http.post(urlBase + '/AccountApi/GetCustomerName?customerId=' + custId);
    };

    dataFactory.getCountries = function () {
        return $http.get(urlBase + '/CodeTableAPI/GetCountries');
    };

    dataFactory.getPlans = function (request) {
        return $http.post(urlBase + '/SuperAdminConfigApi/GetPlanList', JSON.stringify(request));
    };

    dataFactory.getListofPlans = function (planMode) {
        return $http.get(urlBase + '/SuperAdminConfigApi/GetListOfPlans?planMode=' + planMode);
    };

    dataFactory.getListofCountry = function () {
        return $http.get(urlBase + '/SuperAdminConfigApi/GetListOfCountries');
    };

    dataFactory.GetListOfStatesForSupplier = function (COUNTRY_CODE) {
        return $http.get(urlBase + '/SuperAdminConfigApi/GetListOfStatesForSupplier?countryCode=' + COUNTRY_CODE);
    };
    dataFactory.getListofStates = function (COUNTRY_CODE) {
        return $http.get(urlBase + '/SuperAdminConfigApi/GetListOfStates?countryCode=' + COUNTRY_CODE);
    };

    dataFactory.GetCutomerPlan = function (customerId, planId) {
        return $http.get(urlBase + '/SuperAdminConfigApi/GetCutomerPlan?customerId=' + customerId + '&planId=' + planId);
    };

    dataFactory.getpicklistall = function (type, datatype) {
        return $http.get(urlBase + '/HomeApi/LoadPickList?type=' + type + '&datatype=' + datatype);
    };

    dataFactory.UpdateAllattributes = function (request) {
        return $http.post(urlBase + '/HomeApi/UpdateAttribute', JSON.stringify(request));
    };
    //-----------------------------------------------------------------------------Start Table Designer------------------------------------------------------------------------------

    dataFactory.GetAllattributes = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetAllattributes?catalogid=' + catalogid);
    };
    dataFactory.GetAllattributesManage = function (catalogid, request) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.post(urlBase + '/HomeApi/GetAllattributesManage?catalogid=' + catalogid, JSON.stringify(request));
    };

    dataFactory.GetAllattributesProdConfig = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetAllattributesProdConfig?catalogid=' + catalogid);
    };

    dataFactory.GetAllattributesCount = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetAllattributesCount?catalogid=' + catalogid);
    };

    dataFactory.createNewGroupFromMultipleTable = function (newGroupName, familyId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/FamilyApi/createNewGroupFromMultipleTable?newGroupName=' + newGroupName + '&familyId=' + familyId + '&catalogId=' + catalogId);
    };

    dataFactory.getGroupTableDetails = function (familyId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/FamilyApi/getGroupTableDetails?familyId=' + familyId + '&catalogId=' + catalogId);
    };
    dataFactory.GetGroupLayout = function () {
        return $http.get(urlBase + '/FamilyApi/GetGroupLayout');
    };
    dataFactory.getMultipleTableLeftTreeEntities = function (catalogId, familyId, attributeType, groupId) {
        return $http.get(urlBase + '/FamilyApi/getMultipleTableLeftTreeEntities?catalogId=' + catalogId + '&familyId=' + familyId + '&AttributeType=' + attributeType + '&groupId=' + groupId);
    };
    dataFactory.GetAllattributesMultipleTable = function (catalogId, familyId, groupId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/FamilyApi/GetAllattributesMultipleTable?groupId=' + groupId + '&familyId=' + familyId + '&catalogId=' + catalogId);
    };

    //dataFactory.getAllAttributeDataSource = function (catalogId, familyId, request, flag, groupId) {
    //    if (catalogId === undefined || catalogId === '' || catalogId === "") {
    //        catalogId = 0;
    //    }

    //    return $http.post(urlBase + '/TableDesignerApi/GetAllAttributeDataSource?catalogid=' + catalogId + '&familyId=' + familyId + '&flag=' + flag + '&groupId=' + groupId, JSON.stringify(request));
    //};


    dataFactory.getAllAttributeDataSource = function (structureName, catalogId, familyId, request, flag, groupId, packId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (structureName === undefined || structureName == '') {
            structureName = "";
        }
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }
        return $http.post(urlBase + '/TableDesignerApi/GetAllAttributeDataSource?structureName=' + structureName + '&catalogid=' + catalogId + '&familyId=' + familyId + '&flag=' + flag + '&groupId=' + groupId + '&packId=' + packId, JSON.stringify(request));
    };

    dataFactory.GettableGroupHeaderDataSource = function (catalogId, familyId, request, flag, groupId, packId, structureName) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }
        return $http.post(urlBase + '/TableDesignerApi/GettableGroupHeaderDataSource?catalogid=' + catalogId + '&familyId=' + familyId + '&flag=' + flag + '&groupId=' + groupId + '&packId=' + packId + '&structureName=' + structureName, JSON.stringify(request));
    };
    dataFactory.tableGroupSummaryDataSource = function (catalogId, familyId, request, flag, groupId, packId, structureName) {
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }
        return $http.post(urlBase + '/TableDesignerApi/GettableGroupSummaryDataSource?catalogid=' + catalogId + '&familyId=' + familyId + '&flag=' + flag + '&groupId=' + groupId + '&packId=' + packId + '&structureName=' + structureName, JSON.stringify(request));
    };
    dataFactory.GetTableStructure = function (familyId, catalogId, structureName, flag, groupId) {
        return $http.get(urlBase + "/HomeApi/GetTableStructure?catalogId=" + catalogId + "&familyId=" + familyId + "&structureName=" + structureName + '&flag=' + flag + '&groupId=' + groupId);
    };
    dataFactory.DecodeLayoutXml = function (structureName, xmlString, mCatalogId) {
        return $http.post(urlBase + "/TableDesignerApi/DecodeLayoutXml?structureName=" + structureName + "&mCatalogId=" + mCatalogId, JSON.stringify(xmlString));
    };
    dataFactory.DecodeLayoutXmls = function (structureName, xmlString, mCatalogId, familyId, flag, groupId, packId) {
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }
        return $http.post(urlBase + "/TableDesignerApi/DecodeLayoutXmls?structureName=" + structureName + "&mCatalogId=" + mCatalogId + '&familyId=' + familyId + '&flag=' + flag + '&groupId=' + groupId + '&packId=' + packId, JSON.stringify(xmlString));
    };
    dataFactory.DecodeLayoutXmlLeftTree = function (structureName, xmlString, mCatalogId, familyId, attrId, flag, groupId, packId) {
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }
        return $http.post(urlBase + "/TableDesignerApi/DecodeLayoutXmlLeftTree?structureName=" + structureName + "&mCatalogId=" + mCatalogId + "&familyId=" + familyId + "&attrId=" + attrId + '&flag=' + flag + '&groupId=' + groupId + '&packId=' + packId, JSON.stringify(xmlString));
    };
    dataFactory.DecodeLayoutXmlRightTree = function (structureName, xmlString, mCatalogId, familyId, attrId, flag, groupId, packId) {
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }
        return $http.post(urlBase + "/TableDesignerApi/DecodeLayoutXmlRightTree?structureName=" + structureName + "&mCatalogId=" + mCatalogId + "&familyId=" + familyId + "&attrId=" + attrId + '&flag=' + flag + '&groupId=' + groupId + '&packId=' + packId, JSON.stringify(xmlString));
    };
    dataFactory.DecodeLayoutXmlSummaryTree = function (structureName, xmlString, mCatalogId, familyId, attrId, flag, groupId, packId) {
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }
        return $http.post(urlBase + "/TableDesignerApi/DecodeLayoutXmlSummaryTree?structureName=" + structureName + "&mCatalogId=" + mCatalogId + "&familyId=" + familyId + "&attrId=" + attrId + '&flag=' + flag + '&groupId=' + groupId + '&packId=' + packId, JSON.stringify(xmlString));
    };
    dataFactory.DecodeLayoutXmlColumnTree = function (structureName, xmlString, mCatalogId, familyId, attrId, flag, groupId, packId) {
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }
        return $http.post(urlBase + "/TableDesignerApi/DecodeLayoutXmlColumnTree?structureName=" + structureName + "&mCatalogId=" + mCatalogId + "&familyId=" + familyId + "&attrId=" + attrId + '&flag=' + flag + '&groupId=' + groupId + '&packId=' + packId, JSON.stringify(xmlString));
    };
    dataFactory.CreateStructure = function (familyId, categoryId, catalogId, isDefault, structureName, tableType) {
        return $http.get(urlBase + "/HomeApi/CreateStructure?catalogId=" + catalogId + "&familyId=" + familyId + "&categoryId=" + categoryId + "&isDefault=" + isDefault + "&structureName=" + structureName + "&tableType=" + tableType);
    };
    dataFactory.GetStructureNameDataSourceDetails = function (familyId, catalogId, flag, groupId) {
        return $http.get(urlBase + "/HomeApi/GetStructureNameDataSourceDetails?catalogId=" + catalogId + "&familyId=" + familyId + '&flag=' + flag + '&groupId=' + groupId);
    };
    dataFactory.GetAttributeDataFormat = function (attribute_id) {
        return $http.get(urlBase + "/HomeApi/GetAttributeDataFormat?AttrId=" + attribute_id);
    };
    dataFactory.DeleteStructure = function (familyId, catalogId, structureName) {
        return $http.post(urlBase + "/HomeApi/DeleteStructure?catalogId=" + catalogId + "&familyId=" + familyId + "&structureName=" + structureName);
    };
    dataFactory.setDefaultLayout = function (familyId, catalogId, structureName) {
        return $http.post(urlBase + "/HomeApi/setDefaultLayout?catalogId=" + catalogId + "&familyId=" + familyId + "&structureName=" + structureName);
    };
    //-----------------------------------------------------------------------------End Table Designer------------------------------------------------------------------------------
    dataFactory.GetAllMissingNames = function (catalogId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/GetAllMissingNames?catalogId=' + catalogId, JSON.stringify(request));
    };


    //dataFactory.GetALLReports = function (catalogId, option) {
    //    return $http.get(urlBase + '/ReportsApi/GetALLReports?catalogId=' + catalogId + '&option=' + option);
    //};
    dataFactory.GetInvertedproductAttributeDetails = function (catalogId, familyId) {
        return $http.post(urlBase + '/InvertedProduct/GetInvertedproductAttributeDetails?catalogId=' + catalogId + '&familyId=' + familyId);
    };
    //dataFactory.GetAllReports = function (catalogId, startDateModify, endDateModify, startDatenew, endDatenew, option, ProdCurrentPage, perpagecount, fileNames, start_date, end_date, searchItemValue, searchValue) {
    //    if (searchItemValue == undefined || searchValue == undefined) {
    //        searchItemValue = '';
    //        searchValue = '';
    //    }
    //    return $http.get(urlBase + '/ReportsApi/GetAllReports?catalogId=' + catalogId + '&startDateModify=' + startDateModify + '&endDateModify=' + endDateModify + '&startDatenew=' + startDatenew + '&endDatenew=' + endDatenew + '&option=' + option + '&ProdCurrentPage=' + ProdCurrentPage + '&perpagecount=' + perpagecount + '&fileNames=' + fileNames + '&start_date=' + start_date + '&end_date=' + end_date + '&searchItemValue=' + searchItemValue + '&searchValue=' + searchValue);

    //};

    dataFactory.prodcount = function (catalogId, startDateModify, endDateModify) {
        return $http.get(urlBase + '/ReportsApi/ReportModifiedproductscountresults?catalogId=' + catalogId + '&startDateModify=' + startDateModify + '&endDateModify=' + endDateModify);

    };
    dataFactory.prodcountnew = function (catalogId, startDatenew, endDatenew) {

        return $http.get(urlBase + '/ReportsApi/Reportnewproductscountresults?catalogId=' + catalogId + '&startDatenew=' + startDatenew + '&endDatenew=' + endDatenew);

    };


    dataFactory.GetFamilyPreviewList = function (familyId, categoryId, catalogid) {
        return $http.get(baseUrl + '/Family/GetFamilyPreviewList?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogId=' + catalogid);
    };


    dataFactory.GetFamilyPreviewList_Print = function (familyId, categoryId, catalogid) {
        return $http.get(baseUrl + '/Family/GetFamilyPreviewList?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogId=' + catalogid);
    };

    //Get Catalog Family Filters
    dataFactory.GetCatalogFamilyFilter = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(baseUrl + '/Family/GetCatalogFamilyFilter?CatalogId=' + catalogid);
    };

    dataFactory.GetAllCatalogattributes = function (catalogid, familyId, categoryId) {
        return $http.get(urlBase + "/HomeApi/GetAllCatalogattributes?catalogId=" + catalogid + "&familyIdString=" + familyId + '&categoryId=' + categoryId);
    };
    dataFactory.GetprodfamilyAttributes = function (catalogid, familyId, categoryId) {
        return $http.get(urlBase + "/HomeApi/GetprodfamilyAttributes?catalogId=" + catalogid + "&familyIdString=" + familyId + '&categoryId=' + categoryId);
    };

    dataFactory.SavePublishAttributes = function (categoryId, request) {
        return $http.post(urlBase + "/HomeApi/SavePublishAttributes?categoryId=" + categoryId, JSON.stringify(request));
    };
    dataFactory.DeletePublishAttributes = function (request) {
        return $http.post(urlBase + "/HomeApi/DeletePublishAttributes", JSON.stringify(request));
    };
    dataFactory.BtnMoveUpClicksub = function (sortOrder, request) {
        return $http.post(urlBase + "/HomeApi/BtnMoveUpClicksub?sortOrder=" + sortOrder, JSON.stringify(request));
    };
    dataFactory.BtnMoveDownClicksub = function (sortOrder, request) {
        return $http.post(urlBase + "/HomeApi/BtnMoveDownClicksub?sortOrder=" + sortOrder, JSON.stringify(request));
    };

    dataFactory.BtnMoveUpClick = function (sortOrder, request) {
        return $http.post(urlBase + "/HomeApi/BtnMoveUpClick?sortOrder=" + sortOrder, JSON.stringify(request));
    };
    dataFactory.BtnMoveDownClick = function (sortOrder, request) {
        return $http.post(urlBase + "/HomeApi/BtnMoveDownClick?sortOrder=" + sortOrder, JSON.stringify(request));
    };
    dataFactory.BtnMoveUpAttrGroupClick = function (sortOrder, request, groupId) {
        return $http.post(urlBase + "/HomeApi/BtnMoveUpAttrGroupClick?sortOrder=" + sortOrder + "&groupId=" + groupId, JSON.stringify(request));
    };
    dataFactory.BtnMoveDownAttrGroupClick = function (sortOrder, request, groupId) {
        return $http.post(urlBase + "/HomeApi/BtnMoveDownAttrGroupClick?sortOrder=" + sortOrder + "&groupId=" + groupId, JSON.stringify(request));
    };
    dataFactory.UnPublishAttributes = function (categoryid, request) {
        return $http.post(urlBase + "/HomeApi/UnPublishAttributes?categoryid=" + categoryid, JSON.stringify(request));
    };


    dataFactory.SaveFamilyPublishAttributes = function (request, categoryId, FamilyKeyCatid) {
        return $http.post(urlBase + "/HomeApi/SaveFamilyPublishAttributes?categoryId=" + categoryId + "&FamilyKeyCatid=" + FamilyKeyCatid, JSON.stringify(request));
    };
    dataFactory.DeleteFamilyPublishAttributes = function (request, categoryId) {
        return $http.post(urlBase + "/HomeApi/DeleteFamilyPublishAttributes?categoryId=" + categoryId, JSON.stringify(request));
    };
    dataFactory.BtnFamilyMoveUpClick = function (sortOrder, request, categoryId) {
        return $http.post(urlBase + "/HomeApi/BtnFamilyMoveUpClick?sortOrder=" + sortOrder + "&categoryId=" + categoryId, JSON.stringify(request));
    };
    dataFactory.BtnFamilyMoveDownClick = function (sortOrder, request, categoryId) {
        return $http.post(urlBase + "/HomeApi/BtnFamilyMoveDownClick?sortOrder=" + sortOrder + "&categoryId=" + categoryId, JSON.stringify(request));
    };
    dataFactory.UnPublishFamilyAttributes = function (request, categoryId, request1) {

        var data = [];
        data.push(request);
        data.push(request1);

        return $http.post(urlBase + "/HomeApi/UnPublishFamilyAttributes?categoryId=" + categoryId, JSON.stringify(data));
    };

    dataFactory.SubSearchResults = function (searchtxt, usewildcards, catalogIdAttr, Categoryid) {
        return $http.get(urlBase + "/HomeApi/GetSubItemSearchResults?searchtxt=" + encodeURIComponent(searchtxt) + "&usewildcards=" + usewildcards + "&catalogIdAttr=" + catalogIdAttr + "&CategoryId=" + Categoryid);
    };
    dataFactory.SearchResults = function (searchtxt, usewildcards, catalogIdAttr, Categoryid) {
        if (catalogIdAttr == '') {
            catalogIdAttr = 0;
        }



        return $http.get(urlBase + "/HomeApi/GetItemSearchResults?searchtxt=" + encodeURIComponent(searchtxt) + "&usewildcards=" + usewildcards + "&catalogIdAttr=" + catalogIdAttr + "&CategoryId=" + Categoryid);
    };
    dataFactory.GetCategorySearchResults = function (searchtxt, usewildcards, catalogIdAttr, category_id, option, seletedAttribute) {
        if (catalogIdAttr == '')
            catalogIdAttr = 0;
        return $http.get(urlBase + "/HomeApi/GetCategorySearchResults?searchtxt=" + searchtxt + "&usewildcards=" + usewildcards + "&catalogIdAttr=" + catalogIdAttr + "&CategoryId=" + category_id + "&Option=" + option + "&Attribute=" + seletedAttribute);
    };
    dataFactory.GetFamilySearchResults = function (searchtxt, searchid, usewildcards, catalogIdAttr, seletedAttribute, categoryid, EndDate) {
        if (catalogIdAttr == '')
            catalogIdAttr = 0;
        return $http.get(urlBase + "/HomeApi/GetFamilySearchResults?searchtxt=" + searchtxt + "&searchid=" + searchid + "&usewildcards=" + usewildcards + "&catalogIdAttr=" + catalogIdAttr + "&seletedAttribute=" + seletedAttribute + "&CategoryId=" + categoryid + "&EndDate=" + EndDate);
    };

    dataFactory.GroupTableDataGrid = function (familyId, catalogId) {
        return $http.get(urlBase + "/FamilyApi/GroupTableDataGrid?familyId=" + familyId + "&catalogId=" + catalogId);
    };

    dataFactory.GetProductSearchResults = function (searchtxt, searchid, usewildcards, catalogIdAttr, seletedAttribute, categoryid, EndDate) {
        if (catalogIdAttr == '') {
            catalogIdAttr = 0;
        }

        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var encriptedsearchtxt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(searchtxt), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });



        return $http.get(urlBase + "/HomeApi/GetProductSearchResults?searchtxt=" + encriptedsearchtxt.toString() + "&searchid=" + searchid + "&usewildcards=" + usewildcards + "&catalogIdAttr=" + catalogIdAttr + "&seletedAttribute=" + seletedAttribute + "&CategoryId=" + categoryid + "&EndDate=" + EndDate);
    };
    dataFactory.GetSubProductSearchResults = function (searchtxt, searchid, usewildcards, catalogIdAttr, seletedAttribute, categoryid, EndDate) {
        if (catalogIdAttr == '')
            catalogIdAttr = 0;

        return $http.get(urlBase + "/HomeApi/GetSubProductSearchResults?searchtxt=" + searchtxt + "&searchid=" + searchid + "&usewildcards=" + usewildcards + "&catalogIdAttr=" + catalogIdAttr + "&seletedAttribute=" + seletedAttribute + "&CategoryId=" + categoryid + "&EndDate=" + EndDate);
    };
    //For Basic Import
    dataFactory.basicimportExcelSheetSelection = function (allowduplicate, sheetName, excelPath, records) {
        var data = [];
        data.push(records);
        return $http.post(urlBase + "/ImportApi/BasicImportSheetToDatatable?allowDuplicate=" + allowduplicate + "&sheetName=" + sheetName + "&excelPath=" + excelPath, JSON.stringify(records));
    };
    dataFactory.basicImport = function (records, allowduplicate, importdata) {
        var data = [];
        data.push(records);
        data.push(importdata);
        return $http.post(urlBase + "/ImportApi/BasicImport?allowDuplicate=" + allowduplicate, JSON.stringify(data));
    };
    //For Import
    dataFactory.importExcelSheetSelection = function (excelPath) {
        return $http.get(urlBase + "/ImportApi/importExcelSheetSelection?excelPath=" + excelPath);
    };

    dataFactory.importXmlSheetSelection = function (excelPath) {
        return $http.get(urlBase + "/ImportApi/importXmlSheetSelection?excelPath=" + excelPath);
    };

    dataFactory.GetBasicImportSpecs = function (SheetName, excelPath) {

        return $http.get(urlBase + '/ImportApi/GetBasicImportSpecs?SheetName=' + SheetName + '&ExcelPath=' + excelPath);
    };
    dataFactory.GetImportSpecs = function (sheetName, excelPath) {
        return $http.get(urlBase + '/AdvanceImportApi/GetImportSpecs?SheetName=' + sheetName + '&ExcelPath=' + excelPath);
    };
    dataFactory.GetImportSpecs = function (sheetName, excelPath, templateId, importType, selectAll) {
        if (selectAll == '' || selectAll == "" || selectAll == undefined) {
            selectAll = 0;
        }
        return $http.get(urlBase + '/AdvanceImportApi/GetImportSpecs?SheetName=' + sheetName + '&ExcelPath=' + excelPath + "&templateId=" + templateId + "&importType=" + importType + "&selectAll=" + selectAll);
    };
    dataFactory.generateTableHtml = function (lefttreeViewData, righttreeViewData, columntreeViewData, summarytreeViewData, comboSummaryGroup, comboGroupByColumn, emptyCellTextBox, dispalyRowHeaderCheckEditor, displayColumnHeaderCheckBox, dispalySummaryHeaderCheckEditor, verticalTableChkBox, pivotHeaderTextBox, catalogId, familyId, structureName, categoryId, flag, groupId, packId) {
        var data = [];
        if (packId === undefined || packId === '' || packId === "") {
            packId = 0;
        }

        data.push(lefttreeViewData);
        data.push(righttreeViewData);
        data.push(columntreeViewData);
        data.push(summarytreeViewData);
        return $http.post(urlBase + '/HomeApi/GenerateTableHtml?comboSummaryGroupValue=' + comboSummaryGroup + "&comboGroupByColumnValue=" + comboGroupByColumn + "&emptyCellTextBoxValue=" + emptyCellTextBox + "&dispalyRowHeaderCheckEditorValue=" + dispalyRowHeaderCheckEditor + "&displayColumnHeaderCheckBoxValue=" + displayColumnHeaderCheckBox + "&dispalySummaryHeaderCheckEditorValue=" + dispalySummaryHeaderCheckEditor + "&verticalTableChkBoxValue=" + verticalTableChkBox + "&pivotHeaderTextBoxValue=" + pivotHeaderTextBox + "&familyId=" + familyId + "&structureName=" + structureName + "&catalogId=" + catalogId + "&CategoryID=" + categoryId + '&flag=' + flag + '&groupId=' + groupId + '&packId=' + packId, JSON.stringify(data));
    };

    dataFactory.GetCategoryDetails = function (catalogId) {
        return $http.get(urlBase + "/HomeApi/GetCategoryDetails?catalogId=" + catalogId);
    };
    dataFactory.GetProjectDetails = function (catalogid, projectType) {

        return $http.get(urlBase + "/HomeApi/GetProjectDetails?catalogid=" + catalogid + "&projectType=" + projectType);
    };
    dataFactory.GetCatalogattributes = function (catalogid, projectId) {
        return $http.get(urlBase + '/HomeApi/GetCatalogattributes?catalogid=' + catalogid + "&projectId=" + projectId);
    };

    dataFactory.GetSelectedattributes = function (catalogid, projectId) {
        return $http.get(urlBase + '/HomeApi/GetSelectedattributes?catalogid=' + catalogid + "&projectId=" + projectId);
    };
    dataFactory.GetSelectedDisplayAttributes = function (catalogid, projectId, selectedAttributes) {
        var data = [];
        data.push(selectedAttributes);
        return $http.post(urlBase + '/HomeApi/GetSelectedDisplayAttributes?catalogid=' + catalogid + "&projectId=" + projectId, JSON.stringify(data));
    };
    dataFactory.Saveproject = function (request, catalogid, projecttype, workflowstatus) {
        if (workflowstatus == undefined) {
            workflowstatus = "APPROVE";
        }
        return $http.post(urlBase + "/HomeApi/Saveproject?catalogid=" + catalogid + "&workflowstatus=" + workflowstatus + "&projecttype=" + projecttype, JSON.stringify(request));
    };
    dataFactory.SaveTemplate = function (request, catalogid, projecttype) {
        return $http.post(urlBase + "/HomeApi/SaveTemplate?catalogid=" + catalogid + "&projecttype=" + projecttype, JSON.stringify(request));
    };
    dataFactory.SaveExportproject = function (projectmodel, catalogid, attrBy, filter, familyFilter, catalogname, categoryId, familyId, projectId, selcategory, selecteddata, seletedAttributes, famarray, exportTemplateDetails, startDate, endDate) {
        var data = [];
        data.push(projectmodel);
        data.push(selecteddata);
        data.push(seletedAttributes);
        data.push(exportTemplateDetails);
        //data.push(selfamily);
        data.push(JSON.stringify(famarray));
        return $http.post(urlBase + "/HomeApi/SaveExportproject?catalogid=" + catalogid + "&startDate=" + startDate + "&endDate=" + endDate + "&attrBy=" + attrBy + "&filter=" + filter + "&familyFilter=" + familyFilter + "&catalogname=" + catalogname + "&categoryId=" + categoryId + "&familyId=" + familyId + "&projectId=" + projectId + "&selcategory_id=" + selcategory, JSON.stringify(data));
    };
    //dataFactory.Exportproject = function (projectmodel, catalogid, attrBy, filter, familyFilter, catalogname, categoryId, familyId, projectId, selcategory, selecteddata, seletedAttributes, outputformat, Delimiters, fromdate, todate, exportids, hierarchy, attributeorder, famarray, schedule, scheduleDate, ftp, url, username, passwordencrpt, templateName, exporttypes) {
    //    var data = [];
    //    data.push(projectmodel);
    //    data.push(selecteddata);
    //    data.push(seletedAttributes);
    //    //data.push(selfamily);
    //    data.push(JSON.stringify(famarray));
    //    data.push(JSON.stringify(attributeorder));

    //    return $http.post(urlBase + "/HomeApi/Exportproject?catalogid=" + catalogid + "&attrBy=" + attrBy + "&filter=" + filter + "&familyFilter=" + familyFilter + "&catalogname=" + catalogname + "&categoryId=" + categoryId + "&familyId=" + familyId + "&projectId=" + projectId + "&selcategory_id=" + selcategory + "&outputformat=" + outputformat + "&Delimiters=" + Delimiters + "&fromdate=" + fromdate + "&todate=" + todate + "&schedule=" + schedule + "&scheduleDateTime=" + scheduleDate + "&exportids=" + exportids + "&hierarchy=" + hierarchy + "&ftp=" + ftp + "&url=" + url + "&username=" + username + "&Passwordencrpt=" + passwordencrpt + "&templateName=" + templateName + "&exporttypes=" + exporttypes, JSON.stringify(data));
    //};
    dataFactory.Exportproject = function (projectmodel, catalogid, attrBy, filter, familyFilter, catalogname, categoryId, familyId, projectId, selcategory, selecteddata, seletedAttributes, outputformat, Delimiters, fromdate, todate, exportids, hierarchy, attributeorder, famarray, schedule, scheduleDate, ftp, url, username, passwordencrpt, templateName, singlesheet, exporttypes) {
        var data = [];
        data.push(projectmodel);
        data.push(selecteddata);
        data.push(seletedAttributes);
        //data.push(selfamily);
        data.push(JSON.stringify(famarray));
        data.push(JSON.stringify(attributeorder));

        return $http.post(urlBase + "/HomeApi/Exportproject?catalogid=" + catalogid + "&attrBy=" + attrBy + "&filter=" + filter + "&familyFilter=" + familyFilter + "&catalogname=" + catalogname + "&categoryId=" + categoryId + "&familyId=" + familyId + "&projectId=" + projectId + "&selcategory_id=" + selcategory + "&outputformat=" + outputformat + "&Delimiters=" + Delimiters + "&fromdate=" + fromdate + "&todate=" + todate + "&schedule=" + schedule + "&scheduleDateTime=" + scheduleDate + "&exportids=" + exportids + "&hierarchy=" + hierarchy + "&ftp=" + ftp + "&url=" + url + "&username=" + username + "&Passwordencrpt=" + passwordencrpt + "&templateName=" + templateName + "&singlesheet=" + singlesheet + "&exporttypes=" + exporttypes, JSON.stringify(data));
    };
    dataFactory.validationresult = function (Errorlogoutputformat, fileName) {

        return $http.post(urlBase + '/ImportApi/importlogs?Errorlogoutputformat=' + Errorlogoutputformat + '&fileName=' + fileName);
    };
    dataFactory.GetPaperSize = function () {
        return $http.get(urlBase + '/XpressCatalogApi/GetPaperSize');
    };

    dataFactory.GetPaperWidth = function (sizename) {
        return $http.get(urlBase + '/XpressCatalogApi/GetPaperWidth?sizename=' + encodeURIComponent(sizename));
    };

    dataFactory.GetPDFCatalogattributes = function (catalogid, projectId, type) {
        return $http.get(urlBase + '/XpressCatalogApi/GetPDFCatalogattributes?catalogid=' + catalogid + "&projectId=" + projectId + "&type=" + type);
    };

    dataFactory.GetPDFSelectedattributes = function (catalogid, projectId, type) {
        return $http.get(urlBase + '/XpressCatalogApi/GetPDFSelectedattributes?catalogid=' + catalogid + "&projectId=" + projectId + "&type=" + type);
    };

    dataFactory.GetProdSpecsExport = function (catalogId, id) {
        return $http.get(urlBase + '/HomeApi/GetProdSpecsExport?catalogId=' + catalogId + '&familyId=' + id);
    };

    //For Product Copy/Paste
    dataFactory.cathashSearch = function (partno) {
        return $http.get(urlBase + '/HomeApi/CatHashSearch?partno=' + partno);
    };

    dataFactory.cathashSearchsub = function (partno) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var partnoencrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(partno), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return $http.post(urlBase + '/HomeApi/CatHashSearchsub?partnoencry=' + partnoencrypt.toString());
    };
    dataFactory.SaveProduct = function (familyId, categoryId, catalogId, selectedItem, copycat, customerid, subproductconfirm, sortOrderValue, request) {
        return $http.post(urlBase + '/HomeApi/SaveProduct?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogID=' + catalogId + '&copycat=' + copycat + '&customerid=' + customerid + '&subproductconfirm=' + subproductconfirm + '&sortOrderValue=' + sortOrderValue, "[" + JSON.stringify(request) + "," + JSON.stringify(selectedItem.split("\n")) + "]");
    };
    dataFactory.SaveNewProduct = function (customerid, sortOrderValue, request) {
        return $http.post(urlBase + '/HomeApi/SaveNewProduct?customerid=' + customerid + '&sortOrderValue=' + sortOrderValue, JSON.stringify(request));
    };
    dataFactory.CreateNewCategory = function (parentCategoryId, request, catalogId, statusName) {
        return $http.post(urlBase + '/HomeApi/CreateNewCategory?catalogId=' + catalogId + '&rootCat=' + parentCategoryId + '&statusName=' + statusName, JSON.stringify(request));
    };

    dataFactory.UpdateCategory = function (parentCategoryId, statusName, request) {
        return $http.post(urlBase + '/HomeApi/UpdateCategory?&rootCat=' + parentCategoryId + '&statusName=' + statusName, JSON.stringify(request));
    };




    dataFactory.PreferecesApplicationSettings = function (applicationStyle, displayIdColumns, filePath, temporaryFilePath, techSupportUrl, duplicateItem, webcatSyncUrl, spellCheckerMode, whiteboardWebserviceUrl, showCustomApps, enableWorkflow, familyPreview, productPreview, userid, displaySkuProductCountInAlert, skuAlertPercentage, customerMasterCatalog, validateCatalogItems, defaultImagePath, EnableSubProduct, CatalogItemNumber, autoWebSync, webSyncFromDate, allowImagePdf, ConvertionType) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var CatalogItemNumber = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(CatalogItemNumber), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });


        return $http.post(urlBase + '/HomeApi/PreferecesApplicationSettings?&applicationStyle=' + applicationStyle + '&displayIdColumns=' + displayIdColumns + '&filePath=' + filePath + '&temporaryFilePath=' + temporaryFilePath + '&techSupportUrl=' + techSupportUrl + '&duplicateItem=' + duplicateItem + '&webcatSyncUrl=' + webcatSyncUrl + '&spellCheckerMode=' + spellCheckerMode + '&whiteboardWebserviceUrl=' + whiteboardWebserviceUrl + '&showCustomApps=' + showCustomApps + '&enableWorkflow=' + enableWorkflow + '&familyPreview=' + familyPreview + '&productPreview=' + productPreview + '&userid=' + userid + '&DisplaySkuProductCountInAlert=' + displaySkuProductCountInAlert + '&SKUAlertPercentage=' + skuAlertPercentage + '&customerMasterCatalog=' + customerMasterCatalog + '&validateCatalogItems=' + validateCatalogItems + '&defaultImagePath=' + defaultImagePath + '&EnableSubProduct=' + EnableSubProduct + '&CatalogItemNumber=' + CatalogItemNumber.toString() + '&autoWebSync=' + autoWebSync + '&webSyncFromDate=' + webSyncFromDate + '&AllowImagePDf=' + allowImagePdf + '&ConvertionType=' + ConvertionType);//autoWebSync, webSyncFromDate    

    };
    dataFactory.saveGeneralSettings = function (userid, displaySkuProductCountInAlert, skuAlertPercentage) {
        return $http.post(urlBase + '/HomeApi/saveGeneralSettings?userid=' + userid + '&DisplaySkuProductCountInAlert=' + displaySkuProductCountInAlert + '&SKUAlertPercentage=' + skuAlertPercentage);
    };

    dataFactory.PreferecesCatalogSettings = function (filePath, templatePath, xmlPath, exportImportPath, userid) {
        return $http.post(urlBase + '/HomeApi/PreferecesCatalogSettings?FilePath=' + filePath + '&TemplatePath=' + templatePath + '&XMLPath=' + xmlPath + '&ExportImportPath=' + exportImportPath + '&userid=' + userid);
    };
    dataFactory.PreferecesImageSettings = function (filePath, conversionPath, webCatPath, referencePath, width1, height1, foldeName1, width2, height2, foldeName2, width3, height3, foldeName3, width4, height4, foldeName4, width5, height5, foldeName5, width6, height6, foldeName6, userid) {
        return $http.post(urlBase + '/HomeApi/PreferecesImageSettings?FilePath=' + filePath + '&ConversionPath=' + conversionPath + '&WebCatPath=' + webCatPath + '&ReferencePath=' + referencePath + '&width1=' + width1 + '&height1=' + height1 + '&foldeName1=' + foldeName1 + '&width2=' + width2 + '&height2=' + height2 + '&foldeName2=' + foldeName2 + '&width3=' + width3 + '&height3=' + height1 + '&foldeName3=' + foldeName3 + '&width4=' + width4 + '&height4=' + height4 + '&foldeName4=' + foldeName4 + '&width5=' + width5 + '&height5=' + height5 + '&foldeName5=' + foldeName5 + '&width6=' + width6 + '&height6=' + height6 + '&foldeName6=' + foldeName6 + '&userid=' + userid);
    };
    dataFactory.GetPreferencesDetails = function (customerId) {
        return $http.get(urlBase + '/HomeApi/GetPreferencesDetails?customerId=' + customerId);
    };
    dataFactory.PrferencesaveUsersettings = function (catNav, prdcntNav, uploadImage, createdXml, userid) {
        return $http.post(urlBase + '/HomeApi/PrferencesaveUsersettings?catNav=' + catNav + '&prdcntNav=' + prdcntNav + '&uploadImage=' + uploadImage + '&createdXml=' + createdXml + '&userid=' + userid);
    };
    dataFactory.PrferencesSavesettings = function (exConfig, imConfig) {
        return $http.post(urlBase + '/HomeApi/PrferencesSavesettings?exConfig=' + exConfig + '&imConfig=' + imConfig);
    };

    dataFactory.ProductPreview = function (catalogId, familyId, productId, ProductLevelMultipletablePreview, categoryId) {
        return $http.get(urlBase + '/HomeApi/ViewProductPreview?catalogId=' + catalogId + '&familyId=' + familyId + '&productId=' + productId + '&ProductLevelMultipletablePreview=' + ProductLevelMultipletablePreview + '&categoryId=' + categoryId);
    };

    dataFactory.GetCalculatedattrList = function (catalogid, attrType, attributeDataType, request) {
        return $http.post(urlBase + "/HomeApi/GetCatalogIsattributelist?catalogId=" + catalogid + "&attrType=" + attrType + "&attrDataType=" + attributeDataType, JSON.stringify(request));
    };

    dataFactory.Iscalculatedattr_Save_Click = function (strexpression, attributeId, cal_flag) {
        return $http.get(urlBase + '/HomeApi/Iscalculated_attr_Save_Click?strexpression=' + strexpression + '&attributeId=' + attributeId + '&cal_flag=' + cal_flag);
    };

    dataFactory.UpdateXpressProject = function (projectId, projectName, category_Ids, family_Ids, comment, catalogId, catalogName, type,
       pagesize, widthValue, heightValue, segwidth, segheight, leftmrgn, rightmrgn, topmrgn, bottommrgn, cols, colswidth, colsgap, pageformat,
       cvrpg, tocpg, cattoc, gindex, catindex, indexcols, selecteddata) {
        var data = [];
        data.push(selecteddata);
        return $http.post(urlBase + "/XpressCatalogApi/UpdateXpressProject?projectId=" + projectId + "&projectName=" + projectName + "&category_Ids=" + category_Ids + "&family_Ids=" + family_Ids + "&comment=" + comment + "&catalogId=" + catalogId + "&catalogName=" + catalogName + "&type=" + type +
            "&pagesize=" + pagesize + "&widthValue=" + widthValue + "&heightValue=" + heightValue + "&segwidth=" + segwidth +
            "&segheight=" + segheight + "&leftmrgn=" + leftmrgn + "&rightmrgn=" + rightmrgn + "&topmrgn=" + topmrgn + "&bottommrgn=" + bottommrgn +
            "&cols=" + cols + "&colswidth=" + colswidth + "&colsgap=" + colsgap +
            "&pageformat=" + pageformat + "&cvrpg=" + cvrpg + "&tocpg=" + tocpg + "&cattoc=" + cattoc +
            "&gindex=" + gindex + "&catindex=" + catindex + "&indexcols=" + indexcols + "&indexcols=", JSON.stringify(data));
    };

    dataFactory.SaveProductFilters = function (catalogId, selecteddata) {
        return $http.post(urlBase + "/HomeApi/SaveProductFilters?catalogId=" + catalogId, JSON.stringify(selecteddata));
    };

    dataFactory.SaveFamilyFilters = function (catalogId, selecteddata) {
        return $http.post(urlBase + "/HomeApi/SaveFamilyFilters?catalogId=" + catalogId, JSON.stringify(selecteddata));
    };


    dataFactory.UpdateGroupByFields = function (projectId, selecteddata) {
        var data = [];
        data.push(selecteddata);
        return $http.post(urlBase + "/XpressCatalogApi/UpdateGroupByFields?projectId=" + projectId + "&model=", JSON.stringify(data));
    };
    dataFactory.GetPDFCatalogobjects = function (catalogId) {
        return $http.get(urlBase + '/XpressCatalogApi/GetPDFCatalogobjects?catalogId=' + catalogId);
    };

    dataFactory.GetPDFSelectedobjects = function () {
        return $http.get(urlBase + '/XpressCatalogApi/GetPDFSelectedobjects');
    };

    dataFactory.GetPDFSummary = function (projectId) {
        return $http.get(urlBase + "/XpressCatalogApi/GetPDFSummary?projectId=" + projectId);
    };

    dataFactory.EpaymentClick = function (msg, key) {
        return $http.get(urlBase + '/HomeApi/EpaymentClick?msg=' + msg + '&key=' + key);
    };

    dataFactory.GetkendoProdSpecs = function (catalogId, id) {
        return $http.get(baseUrl + '/Family/GetkendoProdSpecs?catalogid=' + catalogId + '&familyIdString=' + id);
    };
    //PickListName
    dataFactory.GetPickListDetails = function () {
        return $http.get(urlBase + '/HomeApi/GetPickListDetails');
    };
    dataFactory.GetAllCatalogFamilyAttributes = function (catalogId, familyId, categoryId) {
        return $http.get(urlBase + '/HomeApi/GetAllCatalogFamilyAttributes?familyId=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);
    };
    dataFactory.getPublishedfamilyAttributes = function (catalogId, familyId, categoryId) {
        return $http.get(urlBase + '/HomeApi/GetPublishedfamilyAttributes?familyIdString=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);
    };

    dataFactory.savenewattribute = function (request, catalogId) {
        return $http.post(urlBase + '/HomeApi/SaveNewAttribute?catalogId=' + catalogId, JSON.stringify(request));
    };
    dataFactory.Deleteattribute = function (catalogId, request) {
        return $http.post(urlBase + '/HomeApi/Deleteattribute?catalogId=' + catalogId, JSON.stringify(request));
    };
    dataFactory.Deleteassociationattribute = function (catalogId, attributeId) {
        return $http.post(urlBase + '/HomeApi/Deleteassociationattribute?catalogId=' + catalogId + '&attributeId=' + attributeId);
    };
      dataFactory.GetPickListValues = function (picklistname, request) {
        return $http.post(urlBase + '/HomeApi/GetPicklistData1?picklistname=' + picklistname, JSON.stringify(request));
    };

    dataFactory.UpdateAllPickListValues = function (selectedpicklist, selectedpicklisttype, oldupdatedpiclistId, getRowIndex, option, request) {
        return $http.post(urlBase + '/HomeApi/SavePickList?name=' + selectedpicklist + '&selectedpicklisttype=' + selectedpicklisttype + '&oldID=' + oldupdatedpiclistId + '&rowIndex=' + getRowIndex + '&option=' + option, JSON.stringify(request));
    };


    dataFactory.ChkCategoryexists = function (parentCategoryId, categoryId, catalogId) {
        return $http.get(urlBase + '/HomeApi/ChkCategoryexists?parentCategoryId=' + parentCategoryId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);
    };
    dataFactory.DeletePickList = function (selectedpicklist) {
        return $http.post(urlBase + '/HomeApi/DeletePickListValue?picklistname=' + selectedpicklist);
    };
    dataFactory.DeleteCategory = function (parentCategoryId, categoryId, catalogId) {
        return $http.post(urlBase + '/HomeApi/DeleteCategory?parentCategoryId=' + parentCategoryId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);
    };
    dataFactory.DeleteCategoryFromMaster = function (parentCategoryId, categoryId, catalogId) {
        return $http.post(urlBase + '/HomeApi/DeleteCategoryFromMaster?parentCategoryId=' + parentCategoryId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);
    };
    dataFactory.trashCategoryFromMaster = function (parentCategoryId, categoryId, catalogId) {
        return $http.post(urlBase + '/HomeApi/trashCategoryFromMaster?parentCategoryId=' + parentCategoryId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);
    };
    dataFactory.DeleteFamilyPermanent = function (familyId, categoryId, catalogId) {
        return $http.post(urlBase + '/HomeApi/DeleteFamilyPermanent?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId);
    };
    dataFactory.DeleteFamilyPermanentFromMaster = function (familyId, categoryId, catalogId) {
        return $http.post(urlBase + '/HomeApi/DeleteFamilyPermanentFromMaster?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId);
    };
    dataFactory.trashFamilyPermanentFromMaster = function (familyId, categoryId, catalogId,checkassociate) {
        return $http.post(urlBase + '/HomeApi/trashFamilyPermanentFromMaster?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId + '&checkassociate=' + checkassociate);
    };
    //SaveFamilyImageAttachments
    dataFactory.SaveFamilyImgAttach = function (request, catalogId) {
        return $http.post(urlBase + '/FamilyApi/SaveFamilyImgAttach?catalogId=' + catalogId, JSON.stringify(request));
    };

    dataFactory.missingPath = function (imagePath) {
        return $http.post(urlBase + '/FamilyApi/MissingImagePath?imageFile=' + imagePath);
    };
    //Save Family Description
    dataFactory.saveFamilyDescription = function (request, catalogId) {
        return $http.post(urlBase + '/FamilyApi/SaveFamilyDescription?catalogId=' + catalogId, JSON.stringify(request));
    };
    dataFactory.DeleteProductPermanent = function (familyId, categoryId, catalogId, productId) {
        return $http.post(urlBase + '/HomeApi/DeleteProductPermanent?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId + '&productId=' + productId);
    };
    dataFactory.DetachProductPermanent = function (familyId, categoryId, catalogId, productId) {
        return $http.post(urlBase + '/HomeApi/DetachProductPermanent?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogId=' + catalogId, JSON.stringify(productId));
    };


    dataFactory.DeleteProductPermanentFromMaster = function (familyId, categoryId, catalogId, productId) {
        return $http.post(urlBase + '/HomeApi/DeleteProductPermanentFromMaster?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId + '&productId=' + productId);
    };
    dataFactory.DeleteSubProductPermanentFromMaster = function (familyId, categoryId, catalogId, productId) {
        return $http.post(urlBase + '/HomeApi/DeleteSubProductPermanentFromMaster?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId + '&productId=' + productId);
    };
    dataFactory.delete_ProductsPermanentFromMaster = function (familyId, categoryId, catalogId, productId) {
        return $http.post(urlBase + '/HomeApi/delete_ProductsPermanentFromMaster?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId, JSON.stringify(productId));
    };

    dataFactory.trashProductPermanentFromMaster = function (familyId, categoryId, catalogId, productId) {
        return $http.post(urlBase + '/HomeApi/trashProductPermanentFromMaster?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId + '&productId=' + productId);
    };
    dataFactory.trashSubProductPermanentFromMaster = function (familyId, categoryId, catalogId, productId) {
        return $http.post(urlBase + '/HomeApi/trashSubProductPermanentFromMaster?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId + '&productId=' + productId);
    };
    dataFactory.GetCsFamilyPreviewList = function (familyId, categoryId, catalogid, familyLevelMultipletablePreview, EnableSubProduct) {
        return $http.get(baseUrl + '/Family/GetCsFamilyPreviewList?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogId=' + catalogid + '&FamilyLevelMultipletablePreview=' + familyLevelMultipletablePreview + '&EnableSubProduct=' + EnableSubProduct);
    };
    dataFactory.GetCsFamilyPreviewListCheck = function (familyId, categoryId, catalogid, familyLevelMultipletablePreview) {
        return $http.get(baseUrl + '/Family/GetCsFamilyPreviewListCheck?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogId=' + catalogid + '&FamilyLevelMultipletablePreview=' + familyLevelMultipletablePreview);
    };

    dataFactory.GetCsFamilyPreviewListPTM = function (familyId, categoryId, catalogid, familyLevelMultipletablePreview) {
        return $http.get(baseUrl + '/Family/GetCsFamilyPreviewListPTM?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogId=' + catalogid + '&FamilyLevelMultipletablePreview=' + familyLevelMultipletablePreview);
    };
    dataFactory.GetCatalogAssociation = function (categoryId, request) {
        return $http.post(urlBase + '/HomeApi/GetCatalogAssociation?categoryId=' + categoryId, JSON.stringify(request));
    };

    dataFactory.GetRecentlyModifiedFamily = function (catalogId, startDate, endDate, option, request) {
        return $http.post(urlBase + '/DashboardAPI/GetRecentlyModifiedFamily?catalogId=' + catalogId + '&startDate=' + startDate + '&endDate=' + endDate + '&option=' + option, JSON.stringify(request));
    };
    dataFactory.GetRecentlyModifiedProducts = function (catalogId, startDate, endDate, option, pageload, request) {

        return $http.post(urlBase + '/DashboardAPI/GetRecentlyModifiedProducts?catalogId=' + catalogId + '&startDate=' + startDate + '&endDate=' + endDate + '&option=' + option + '&pageload=' + pageload, JSON.stringify(request));
    };
    dataFactory.GetRecentlyModifiedCategories = function (catalogId, startDate, endDate, option, request) {

        return $http.post(urlBase + '/DashboardAPI/GetRecentlyModifiedCategories?catalogId=' + catalogId + '&startDate=' + startDate + '&endDate=' + endDate + '&option=' + option, JSON.stringify(request));
    };

    //dataFactory.GetRecentlyModifiedFamilies = function (catalogId, startDate, endDate) {
    //    return $http.post(urlBase + '/DashboardAPI/GetRecentlyModifiedFamily?catalogId=' + catalogId + '&startDate=' + startDate + '&endDate=' + endDate );
    //};

    //dataFactory.GetRecentlyModifiedProducts = function (catalogId, startDate, endDate) {

    //    return $http.post(urlBase + '/DashboardAPI/GetRecentlyModifiedProducts?catalogId=' + catalogId + '&startDate=' + startDate + '&endDate=' + endDate );
    //};
    //dataFactory.GetRecentlyModifiedCategories = function (catalogId, startDate, endDate) {

    //    return $http.post(urlBase + '/DashboardAPI/GetRecentlyModifiedCategories?catalogId=' + catalogId + '&startDate=' + startDate + '&endDate=' + endDate );
    //};
    //dataFactory.GetCategoryCountDetails=function(catalogId)
    //{
    //    return $http.post(urlBase + '/DashboardAPI/GetCategoryCountDetails?catalogId=' + catalogId);
    //}
    //dataFactory.GetFamilyCountDetails = function (catalogId) {
    //    return $http.post(urlBase + '/DashboardAPI/GetFamilyCountDetails?catalogId=' + catalogId);
    //}
    //dataFactory.GetProductCountDetails = function (catalogId) {
    //    return $http.post(urlBase + '/DashboardAPI/GetProductCountDetails?catalogId=' + catalogId);
    //}
    //dataFactory.GetSubProductCountDetails = function (catalogId) {
    //    return $http.post(urlBase + '/DashboardAPI/GetSubProductCountDetails?catalogId=' + catalogId);
    //}

    dataFactory.GetTotalCustomerAndUserCount = function () {
        return $http.get(urlBase + '/HomeApi/GetTotalCustomerAndUserCount');
    };

    dataFactory.GetCustomerCountByMonthlyWise = function (activationDate) {

        return $http.get(urlBase + '/HomeApi/GetCustomerCountByMonthlyWise?CustomerActivationDate=' + activationDate);
    };

    dataFactory.GetCustomerCountByPlace = function () {
        return $http.get(urlBase + '/HomeApi/GetCustomerCountByPlace');
    };
    dataFactory.GetSoonToExpiredCustomer = function (option) {
        return $http.get(urlBase + '/HomeApi/GetSoonToExpiredCustomer?option=' + option);
    };
    dataFactory.getPickListData = function (picklistdata) {
        return $http.get(urlBase + '/HomeApi/GetPicklistDatabyAttribute?pkData=' + picklistdata);
    };
    dataFactory.GetprodconfigListData = function (attrname, productid) {
        return $http.get(urlBase + '/HomeApi/GetprodconfigListData?attrname=' + attrname + '&productid=' + productid);
    };
    dataFactory.Getfirstfamilydetails = function (catalogId, productId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/HomeApi/Getfirstfamilydetails?catalogId=' + catalogId + '&productId=' + productId);
    };
    dataFactory.GetImportResults = function (catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetImportResults?catalogId=' + catalogId);
    };
    dataFactory.validateImportResults = function (SessionId, importType) {

        return $http.get(urlBase + '/ImportApi/ValidateImportResults?SessionId=' + SessionId + '&importType=' + importType);
    };

    dataFactory.ValidateImportResultssub = function (SessionId) {

        return $http.get(urlBase + '/ImportApi/ValidateImportResultssub?SessionId=' + SessionId);
    };

    dataFactory.validatetabledesignerImportResults = function (SessionId, importType) {

        return $http.get(urlBase + '/ImportApi/validatetabledesignerImportResults?SessionId=' + SessionId + '&importType=' + importType);
    };
    dataFactory.Validatetabledesignerimport = function (Sheet, allowduplicate, Path, importtype, catalogId, Errorlogoutputformat, validation, data) {
        var importData = [];
        importData.push(data);
        importData.push(validation);
        return $http.post(urlBase + '/ImportApi/Validatetabledesignerimport?SheetName=' + Sheet + '&allowDuplicate=' + allowduplicate + '&excelPath=' + Path + '&importtype=' + importtype + '&catalogId=' + catalogId + '&Errorlogoutputformat=' + Errorlogoutputformat + '&Errorlogoutputformat=' + Errorlogoutputformat, JSON.stringify(importData));
    };

    dataFactory.enableitemvalidation = function () {
        return $http.get(urlBase + '/ImportApi/enableitemvalidation');
    };
    dataFactory.enableitemvalidationsub = function () {
        return $http.get(urlBase + '/ImportApi/enableitemvalidationsub');
    };
    dataFactory.getFinishImportResults = function (SessionId) {
        return $http.get(urlBase + '/ImportApi/getFinishImportResults?SessionId=' + SessionId);
    };

    dataFactory.getFinishImportSuccessResults = function (SessionId, option) {
        return $http.get(urlBase + '/ImportApi/getFinishImportSuccessResults?SessionId=' + SessionId + "&option=" + option);
    };

    dataFactory.getCatalogID = function (catalogid) {
        return $http.get(urlBase + '/XpressCatalogApi/getCatalogID?catalogid=' + catalogid);
    };
    dataFactory.GetInitialStructureName = function (familyId, catalogId) {
        return $http.get(urlBase + "/HomeApi/GetInitialStructureName?catalogId=" + catalogId + "&familyId=" + familyId);
    };

    dataFactory.SaveMultipleLayout = function (groupId, layoutname) {
        return $http.post(urlBase + "/HomeApi/SaveMultipleLayout?groupId=" + groupId + "&layoutname=" + layoutname);
    };

    dataFactory.MultipleSavePublishAttributes = function (request, groupId) {
        return $http.post(urlBase + "/HomeApi/MultipleSavePublishAttributes?groupId=" + groupId, JSON.stringify(request));
    };
    dataFactory.MultipleDeletePublishAttributes = function (request, groupId) {
        return $http.post(urlBase + "/HomeApi/MultipleDeletePublishAttributes?groupId=" + groupId, JSON.stringify(request));
    };
    dataFactory.GetIndesignProjectValues = function (projectId, request) {
        return $http.post(urlBase + '/HomeApi/GetIndesignProjectValues?projectId=' + projectId, JSON.stringify(request));
    };
    dataFactory.GetIndesignFileValues = function (projectId, fileNo) {
        return $http.get(urlBase + '/HomeApi/GetIndesignFileValues?projectId=' + projectId + '&fileNo=' + fileNo);
    };
    dataFactory.UpdateIndesignValues = function (projectId, request) {
        return $http.post(urlBase + '/HomeApi/UpdateIndesignValues?projectId=' + projectId, JSON.stringify(request));
    };
    dataFactory.CreateIndesignValue = function (projectId, request) {
        return $http.post(urlBase + '/HomeApi/SaveIndesignProjectValues?projectId=' + projectId, JSON.stringify(request));
    };
    dataFactory.CreateIndesignValues = function (projectId, autofile, catalogId, catalogXmlFileName) {
        return $http.post(urlBase + '/HomeApi/SaveIndesignProjectValue?projectId=' + projectId + '&autofile=' + autofile + '&catalogId=' + catalogId + '&catalogXmlFileName=' + catalogXmlFileName);
    };
    dataFactory.DeleteIndesignValues = function (projectId, request) {
        return $http.post(urlBase + '/HomeApi/DeleteIndesignValues?projectId=' + projectId, JSON.stringify(request));
    };
    dataFactory.saveTBAttribute = function (CatalogId, AttributeName, AttributeType, STYLE_NAME, PUBLISH2PRINT, PUBLISH2WEB, AttributeDataType, size, decimalsize, prefix, suffix, condition, customvalue, applyto, ApplyForNumericOnly, Attributedataformat) {
        return $http.post(urlBase + '/FamilyApi/SaveTbAttribute?CatalogId=' + CatalogId + "&AttributeName=" + AttributeName + "&AttributeType=" + AttributeType + "&STYLE_NAME=" + STYLE_NAME + "&PUBLISH2PRINT=" + PUBLISH2PRINT + "&PUBLISH2WEB=" + PUBLISH2WEB + "&AttributeDataType=" + AttributeDataType + "&Size=" + size + "&decimalsize=" + decimalsize + "&prefix=" + prefix + "&suffix=" + suffix + "&condition=" + condition + "&customvalue=" + customvalue + "&applyto=" + applyto + "&ApplyForNumericOnly=" + ApplyForNumericOnly + "&AttributeDataFormat=" + Attributedataformat);
    };
    dataFactory.GetIndesignCategoryProjectValues = function (recordId,catalogid, request) {
        return $http.post(urlBase + '/HomeApi/GetIndesignCategoryProjectValues?recordId=' + recordId + '&catalogid=' + catalogid, JSON.stringify(request));
    };

    dataFactory.GetIndesignFilterCategoryProjectValues = function (recordId, saveFilterAttributeValues) {
        return $http.post(urlBase + '/HomeApi/GetIndesignFilterCategoryProjectValues?recordId=' + recordId, JSON.stringify(saveFilterAttributeValues));
    };
    dataFactory.GetIndesignCategoryValues = function (recordId) {
        return $http.get(urlBase + '/HomeApi/GetIndesignCategoryValues?recordId=' + recordId);
    };
    dataFactory.DeleteIndesignCategoryValues = function (request) {
        return $http.post(urlBase + '/HomeApi/DeleteIndesignCategoryValues', JSON.stringify(request));
    };
    dataFactory.categorysortChange = function (recordId, selectedrecord, request) {
        var data = [];
        data.push(selectedrecord);
        data.push(request);
        return $http.post(urlBase + '/HomeApi/CategorysortChange?recordId=' + recordId, JSON.stringify(data));
    };
    dataFactory.RefreshProject = function (recordId) {
        return $http.post(urlBase + '/HomeApi/RefreshProject?recordId=' + recordId);
    };
    dataFactory.AddFamiliestoIndesign = function (recordId, catalogId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + "/HomeApi/AddFamiliestoIndesign?recordId=" + recordId + '&catalogId=' + catalogId, JSON.stringify(request));
    };

    dataFactory.ResetFamiliestoIndesign = function (recordId, request) {
        return $http.post(urlBase + "/HomeApi/ResetFamiliestoIndesign?recordId=" + recordId);
    };

    dataFactory.DeleteFamiliestoIndesign = function (recordId, request) {
        return $http.post(urlBase + "/HomeApi/DeleteFamiliestoIndesign?recordId=" + recordId, JSON.stringify(request));
    };

    dataFactory.CategoriesForIndesign = function (catalogId, categoryId, recordId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (recordId === undefined || recordId === '' || recordId === "") {
            recordId = 0;
        }
        return $http.get(urlBase + '/HomeApi/CategoriesForIndesign?categoryId=' + categoryId + '&catalogid=' + catalogId + '&recordId=' + recordId);
    };

    dataFactory.generateInDesignXML = function (catalogID, xmlDataPath, dataFileCount, XML_FILE_NAME, RECORD_ID, CATEGORY_ID, FILE_NO, PROJECT_ID) {
        return $http.post(urlBase + '/InDesignApi/XmlDataFile?catalogID=' + catalogID + '&xmlDataPath=' + xmlDataPath + '&dataFileCount=' + dataFileCount + '&XML_FILE_NAME=' + XML_FILE_NAME + '&RECORD_ID=' + RECORD_ID + '&CATEGORY_ID=' + CATEGORY_ID + '&FILE_NO=' + FILE_NO + '&PROJECT_ID=' + PROJECT_ID); //
    };
    dataFactory.GetAllInDesignAttributesfamily = function (catalogId, familyId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/InDesignApi/LoadAttributes?catalogId=' + catalogId + '&familyId=' + familyId);
    };
    dataFactory.GetIndesignFamilyValues = function (catalogId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/InDesignApi/GetIndesignFamilyValues?catalogId=' + catalogId, JSON.stringify(request));
    };
    dataFactory.GetAllInDesignAttributes = function (catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/InDesignApi/GetAllInDesignAttributes?catalogId=' + catalogId);
    };
    dataFactory.GetAllInDesignAttributes = function (catalogId, familyId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/InDesignApi/GetAllInDesignAttributes?catalogId=' + catalogId + '&familyId=' + familyId);
    };


    dataFactory.generateInDesignXMLs = function (catalogId, filerecord, selectedattrlist, flagallcreate, selectedprojectId, idColumnFlag) {

        var data = [];
        data.push(filerecord);
        data.push(selectedattrlist);
        return $http.post(urlBase + '/InDesignApi/XmlDataFiles?catalogId=' + catalogId + '&flagallcreate=' + flagallcreate + '&selectedprojectId=' + selectedprojectId + '&idColumnFlag=' + idColumnFlag, JSON.stringify(data));
    };

    dataFactory.XmlDataFileswithoutAttr = function (catalogId, filerecord, flagallcreate, selectedprojectId, idColumnFlag, FilterAttributeValues) {
        var data = [];
        data.push(filerecord);
        data.push(FilterAttributeValues);
        return $http.post(urlBase + '/InDesignApi/XmlDataFileswithoutAttr?catalogId=' + catalogId + '&flagallcreate=' + flagallcreate + '&selectedprojectId=' + selectedprojectId + '&idColumnFlag=' + idColumnFlag, JSON.stringify(data)); //
    };


    dataFactory.createAutoFileNameIndesign = function (projectId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/createAutoFileNameIndesign?projectId=' + projectId + '&catalogId=' + catalogId);
    };

    dataFactory.GetAllCatalogattributesForAttribute = function (catalogId, attributeType, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/HomeApi/GetAllCatalogattributesForAttribute?catalogId=' + catalogId + '&attributeType=' + attributeType, JSON.stringify(request));
    };

    dataFactory.savecatalogallattrdetails = function (catalogId, CheckBoxValues) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/HomeApi/SaveCatalogattrdetails?catalogId=' + catalogId, JSON.stringify(CheckBoxValues));
    };

    dataFactory.downloadXMLFilePath = function () {
        return $http.post(urlBase + '/InDesignApi/downloadXMLFilePath');
    };

    dataFactory.GetAllMultiplegroups = function (catalogId, familyId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + "/HomeApi/GetAllMultiplegroups?catalogId=" + catalogId + "&familyIdString=" + familyId);
    };

    dataFactory.GetAllMultiplegroupsattributes = function (groupId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetAllMultiplegroupsattributes?catalogId=" + catalogId + "&groupId=" + groupId);
    };
    dataFactory.RemoveGroupFromMultipleTable = function (groupId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/FamilyApi/RemoveGroupFromMultipleTable?groupId=' + groupId + '&catalogId=' + catalogId);
    };
    dataFactory.GetAttributeGroupings = function (groupId) {
        return $http.get(urlBase + "/HomeApi/GetAttributeGrouping?groupId=" + groupId);
    };
    dataFactory.UpdateMultiplegroupName = function (request) {
        return $http.post(urlBase + "/HomeApi/UpdateMultiplegroupName", JSON.stringify(request));
    };
    dataFactory.ClearFamilyAttach = function (request, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/FamilyApi/ClearFamilyAttach?catalogId=' + catalogId, JSON.stringify(request));
    };
    dataFactory.saveProductAttribute = function (CatalogId, AttributeName, AttributeType, STYLE_NAME, PUBLISH2PRINT, PUBLISH2WEB, AttributeDataType, size, decimalsize, prefix, suffix, condition, customvalue, applyto, ApplyForNumericOnly, Attributedataformat) {
        if (CatalogId === undefined || CatalogId === '' || CatalogId === "") {
            CatalogId = 0;
        }

        return $http.post(urlBase + '/FamilyApi/saveTBAttribute?CatalogId=' + CatalogId + "&AttributeName=" + AttributeName + "&AttributeType=" + AttributeType + "&STYLE_NAME=" + STYLE_NAME + "&PUBLISH2PRINT=" + PUBLISH2PRINT + "&PUBLISH2WEB=" + PUBLISH2WEB + "&AttributeDataType=" + AttributeDataType + "&Size=" + size + "&decimalsize=" + decimalsize + "&prefix=" + prefix + "&suffix=" + suffix + "&condition=" + condition + "&customvalue=" + customvalue + "&applyto=" + applyto + "&ApplyForNumericOnly=" + ApplyForNumericOnly + "&AttributeDataFormat=" + Attributedataformat);
    };

    dataFactory.updateGlobalAttrValueForSelectedFamily = function (catalogId, attrType, attrId, attValue, familyIds) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/FamilyApi/UpdateGlobalAttrValueForSelectedFamily?catalogId=' + catalogId + "&attrType=" + attrType + "&attrId=" + attrId + "&attrValue=" + attValue, JSON.stringify(familyIds));
    };
    dataFactory.updateGlobalAttrValueForSelectedFamilysub = function (catalogId, attrType, attrId, attValue, familyIds) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/FamilyApi/UpdateGlobalAttrValueForSelectedFamilysub?catalogId=' + catalogId + "&attrType=" + attrType + "&attrId=" + attrId + "&attrValue=" + attValue, JSON.stringify(familyIds));
    };
    //dataFactory.globalattribute = function (objstrid, objstrcatid, CatalogID, AttrType, option, sorting) {
    //    if (CatalogID === undefined || CatalogID === '' || CatalogID === "") {
    //        CatalogID = 0;
    //    }

    //    return $http.post(urlBase + '/FamilyApi/globalattribute?Attrid=' + objstrid + "&Catalogid=" + CatalogID + "&Attributetype=" + AttrType + "&option=" + option + "&sort=" + sorting, JSON.stringify(objstrcatid));
    //};
    //sathya//
    dataFactory.globalattribute = function (objstrid, objstrcatid, CatalogID, AttrType, option, sorting) {
        if (CatalogID === undefined || CatalogID === '' || CatalogID === "") {
            CatalogID = 0;
        }
        var ItemRow = [];
        var Item = new Object();
        Item["Attribute_id"] = objstrid;
        Item["Categoryid"] = objstrcatid;
        ItemRow.push(Item);

        return $http.post(urlBase + '/FamilyApi/globalattribute?Catalogid=' + CatalogID + "&Attributetype=" + AttrType + "&option=" + option + "&sort=" + sorting, JSON.stringify(ItemRow));
    };
    //sathya//
    dataFactory.globalattributesub = function (objstrid, objstrcatid, CatalogID, AttrType, option, sorting) {
        return $http.post(urlBase + '/FamilyApi/globalattributesub?Attrid=' + objstrid + "&Catalogid=" + CatalogID + "&Attributetype=" + AttrType + "&option=" + option + "&sort=" + sorting, JSON.stringify(objstrcatid));
    };

    dataFactory.globalCalculatedAttribute = function (objstrcatid, CatalogID, option) {
        return $http.post(urlBase + '/FamilyApi/globalCalculatedAttribute?Catalogid=' + CatalogID + "&option=CALC", JSON.stringify(objstrcatid));
    };
    dataFactory.clearCategoryImgAttach = function (parentCategoryId, request) {
        return $http.post(urlBase + '/HomeApi/clearCategoryImgAttach?&rootCat=' + parentCategoryId, JSON.stringify(request));
    };

    dataFactory.clearCategoryImgAttach2 = function (parentCategoryId, request) {
        return $http.post(urlBase + '/HomeApi/clearCategoryImgAttach2?&rootCat=' + parentCategoryId, JSON.stringify(request));
    };

    dataFactory.PasteProduct = function (familyId, product_ids, flag, newfamily_id, customer_id, catalog_id, oldcatalogid, passoldcategoryid, passcategoryid, sortOrderProduct) {
        return $http.post(urlBase + '/HomeApi/PasteProduct?familyId=' + familyId + '&product_ids=' + product_ids + '&falg=' + flag + '&newFamily_id=' + newfamily_id + '&customer_id=' + customer_id + '&catalog_id=' + catalog_id + '&oldcatalogid=' + oldcatalogid + '&passoldcategoryid=' + passoldcategoryid + '&passcategoryid=' + passcategoryid + '&sortOrderValue=' + sortOrderProduct);
    };

    //-------------------------------------------------------------------------------------------------------------------------------------------------------
    dataFactory.GetAllReferenceTable = function (catalogId, customerId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetAllReferenceTable?catalogId=" + catalogId + '&customerId=' + customerId);
    };
    dataFactory.GetAllReferenceTableGrid = function (catalogId, customerId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + "/HomeApi/GetAllReferenceTableGrid?catalogId=" + catalogId + '&customerId=' + customerId, JSON.stringify(request));
    };

    dataFactory.GetAllReferenceTableFamily = function (tableId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetAllReferenceTableFamily?catalogId=" + catalogId + "&tableId=" + tableId);
    };
    dataFactory.UpdateReferenceTableName = function (request) {
        return $http.post(urlBase + "/HomeApi/UpdateReferenceTableName", JSON.stringify(request));
    };
    dataFactory.UpdateReferenceTable = function (Table_ID,Table_Name) {
        return $http.post(urlBase + '/HomeApi/UpdateReferenceTable?Table_ID=' + Table_ID + "&Table_Name=" + Table_Name);
    };
    dataFactory.createNewReferencetable = function (tablename, customerId) {
        return $http.post(urlBase + '/HomeApi/CreateNewReferencetable?tablename=' + tablename + "&customerId=" + customerId);
    };
    dataFactory.RemoveReferencetable = function (tableId) {
        return $http.post(urlBase + '/HomeApi/RemoveReferencetable?tableId=' + tableId);
    };
    dataFactory.GetDynamicReferenceTable = function (tableId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetDynamicReferenceTable?tableId=' + tableId + '&CatalogId=' + catalogId);
    };
    //Reference table export
    dataFactory.ReferencetableExport = function (tableId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/HomeApi/ReferencetableExport?tableId=' + tableId + '&CatalogId=' + catalogId);
    };
    dataFactory.StpCall = function (tableId, rowValue, valueType, rowId, columnIndex, rowIndex, columnId, flag, operation) {
        return $http.post(urlBase + "/HomeApi/StpCall?tableId=" + tableId + "&rowValue=" + rowValue + "&valueType=" + valueType + "&rowId=" + rowId + "&columnIndex=" + columnIndex + "&columnId=" + columnId + "&rowIndex=" + rowIndex + "&flag=" + flag + "&operation=" + operation);
    };
    dataFactory.Savereferencetablevalues = function (tableId, request) {
        return $http.post(urlBase + "/HomeApi/Savereferencetablevalues?tableId=" + tableId, JSON.stringify(request));
    };

    dataFactory.GetAllReferenceTableAssociation = function (catalogId, familyId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetAllReferenceTableAssociation?catalogId=' + catalogId + "&familyId=" + familyId);
    };
    dataFactory.GetselectedReferenceTableAssociation = function (catalogId, familyId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetselectedReferenceTableAssociation?catalogId=" + catalogId + "&familyId=" + familyId);
    };

    dataFactory.SavePublishReferenceTable = function (request, catalogId, familyId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + "/HomeApi/SavePublishReferenceTable?catalogId=" + catalogId + "&familyId=" + familyId, JSON.stringify(request));
    };
    dataFactory.DeletePublishedReferenceTable = function (request, catalogId, familyId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + "/HomeApi/DeletePublishedReferenceTable?catalogId=" + catalogId + "&familyId=" + familyId, JSON.stringify(request));
    };

    dataFactory.GetAllReferenceTableAssociationCategory = function (catalogId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetAllReferenceTableAssociationCategory?catalogId=' + catalogId + "&categoryId=" + categoryId);
    };
    dataFactory.GetselectedReferenceTableAssociationCategory = function (catalogId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + "/HomeApi/GetselectedReferenceTableAssociationCategory?catalogId=" + catalogId + "&categoryId=" + categoryId);
    };

    dataFactory.SavePublishReferenceTableCategory = function (request, catalogId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + "/HomeApi/SavePublishReferenceTableCategory?catalogId=" + catalogId + "&categoryId=" + categoryId, JSON.stringify(request));
    };
    dataFactory.DeletePublishedReferenceTableCategory = function (request, catalogId, categoryId) {
        return $http.post(urlBase + "/HomeApi/DeletePublishedReferenceTableCategory?catalogId=" + catalogId + "&categoryId=" + categoryId, JSON.stringify(request));
    };

    dataFactory.getWorkflow = function () {
        return $http.get(urlBase + '/HomeApi/GetWorkflow');
    };
    dataFactory.getProductSortorder = function (familyid) {

        return $http.get(urlBase + '/HomeApi/GetProductSortorder?familyid=' + familyid);
    };
    dataFactory.getProductSortorderForItem = function (familyid) {

        return $http.get(urlBase + '/HomeApi/GetProductSortorderForItem?familyid=' + familyid);
    };
    dataFactory.getWorkFlowActiveStaus = function () {
        return $http.get(urlBase + '/HomeApi/GetWorkFlowActiveStaus');
    };

    dataFactory.UpdatechangeWorkFlowStatus = function (activeStaus, catalogID, categoryIds) {
        return $http.post(urlBase + "/HomeApi/UpdateChangeWorkFlowStatus?ActiveStatus=" + activeStaus + "&catalogID=" + catalogID + "&categoryIds=" + categoryIds);
    };


    dataFactory.Getimagevaluevalues = function (imageValue) {
        return $http.get(urlBase + '/HomeApi/Getimagevaluevalues?imageValue=' + imageValue);
    };

    dataFactory.GetAllattributesManager = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetAllattributesManager?catalogid=' + catalogid);
    };
    dataFactory.AddProductconfigurator = function (familyId, categoryId, catalogId, selectedItem, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/HomeApi/AddProductconfigurator?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogID=' + catalogId + '&selectedItem=' + selectedItem, JSON.stringify(request));
    };
    dataFactory.ProdconfigSpecs = function (familyId, categoryId, catalogId, configprodid, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/HomeApi/getProdconfigSpecs?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogID=' + catalogId + '&configprodid=' + configprodid, JSON.stringify(request));
    };
    dataFactory.RemoveProductconfigurator = function (ProductIds) {
        return $http.get(urlBase + '/HomeApi/RemoveProductconfigurator?ProductIds=' + ProductIds);
    };
    dataFactory.GetAllConfigTreeData = function (familyId) {
        return $http.get(urlBase + "/HomeApi/GetAllConfigTreeData?familyId=" + familyId);
    };
    dataFactory.GetAllTreeDataDetails = function (familyId, Config_Id) {
        return $http.get(urlBase + "/HomeApi/GetAllTreeDataDetails?familyId=" + familyId + "&Config_Id=" + Config_Id);
    };
    //-------------------------------------------------------------------------------------------------------------------------------------------------------
    dataFactory.moveProductDefaultFamily = function (familyId, categoryId, catalogId, productId, defaultFamily) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/HomeApi/moveProductDefaultFamily?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId + '&productId=' + productId + '&defaultFamily=' + defaultFamily);
    };

    dataFactory.move_ProductsDefaultFamily = function (familyId, categoryId, catalogId, productId, defaultFamily) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/HomeApi/move_ProductsDefaultFamily?categoryId=' + categoryId + '&catalogId=' + catalogId + '&familyId=' + familyId +'&defaultFamily=' + defaultFamily, JSON.stringify(productId));
    };

    //------------------Inverted Products--------------------------------------------------------------------------------------------------------------------
    dataFactory.GetInvertedProducts = function (catalogId, pageno, perpagecount, categoryId, selectedTemplateId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (pageno === undefined || pageno === '' || pageno === "") {
            pageno = 1;
        }
        if (perpagecount === undefined || perpagecount === '' || perpagecount === "") {
            perpagecount = 10;
        }
        return $http.post(urlBase + '/InvertedProduct/GetInvertedProducts?catalogId=' + catalogId + '&Pageno=' + pageno + '&Perpagecount=' + perpagecount + '&selectedCategoryId=' + categoryId + '&selectedTemplateId=' + selectedTemplateId);
    };

    dataFactory.GetInvertedSearchProducts = function (catalogId, pageno, perpagecount, attrName, usewildcards, searchText, selectedCategoryID, selectedTemplateId) {
        var searchContent = [];
        searchContent.push(attrName);
        searchContent.push(searchText);
        return $http.post(urlBase + '/InvertedProduct/GetInvertedSearchProducts?catalogId=' + catalogId + '&Pageno=' + pageno + '&usewildcards=' + usewildcards + '&Perpagecount=' + perpagecount + '&selectedCategoryId=' + selectedCategoryID + '&selectedTemplateId=' + selectedTemplateId, JSON.stringify(searchContent));
    };

    dataFactory.GetInvertedSearchSubProducts = function (catalogId, pageno, perpagecount, attrName, usewildcards, searchText) {
        var searchContent = [];
        searchContent.push(attrName);
        searchContent.push(searchText);
        return $http.post(urlBase + '/InvertedProduct/GetInvertedSearchSubProducts?catalogId=' + catalogId + '&Pageno=' + pageno + '&usewildcards=' + usewildcards + '&Perpagecount=' + perpagecount, JSON.stringify(searchContent));
    };

    dataFactory.GetPagenumberInverted = function (catalogId, categoryId, familyId, productId, option) {

        if (familyId === undefined || familyId === '' || familyId === "") {
            familyId = 0;
        }
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetPagenumberInverted?catalogId=' + catalogId + '&categoryId=' + categoryId + '&familyId=' + familyId + '&productId=' + productId + '&option=' + option);
    };

    dataFactory.ProdFamilyAssociatedCategory = function (productId, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + '/HomeApi/ProdFamilyAssociatedCategory?productId=' + productId + '&catalogid=' + catalogid);
    };
    dataFactory.SubProdFamilyAssociatedCategory = function (productId, subproductId, catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + '/HomeApi/SubProdFamilyAssociatedCategory?productId=' + productId + '&subproductId=' + subproductId + '&catalogid=' + catalogid);
    };
    dataFactory.GetInvertedsubProducts = function (catalogId, pageno, perpagecount) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/InvertedProduct/GetInvertedSubProducts?catalogId=' + catalogId + '&Pageno=' + pageno + '&Perpagecount=' + perpagecount);
    };
    dataFactory.getCategoryFamily = function (categoryId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/getCategoryFamily?categoryId=' + categoryId + '&catalogId=' + catalogId);
    };
    dataFactory.GetInvertedProductsCount = function (catalogId, opt) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/InvertedProduct/GetInvertedProductsCount?catalogId=' + catalogId + '&opt=' + opt);
    };

    //--------------------Export -----------------------

    dataFactory.getCategoryFamilyExport = function (categoryId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }


        return $http.get(urlBase + '/HomeApi/getCategoryFamilyExport?categoryId=' + categoryId + '&catalogId=' + catalogId);
    };



    //----------------------------------------------------------------------Suppplier---------------------------------------------------------------------------------
    dataFactory.GetAllSupplier = function (request, customerId) {
        return $http.post(urlBase + "/HomeApi/GetAllSupplier?customerId=" + customerId, JSON.stringify(request));
    };
    dataFactory.GetStates = function () {
        return $http.get(urlBase + "/HomeApi/GetStates");
    };

    dataFactory.GetCountries = function () {
        return $http.get(urlBase + "/HomeApi/GetCountries");
    };
    dataFactory.savenewsupplier = function (customerId, request) {
        return $http.post(urlBase + '/HomeApi/NewSupplierSave?customerId=' + customerId, JSON.stringify(request));
    };
    dataFactory.DeleteSupplier = function (supplierName) {
        return $http.post(urlBase + '/HomeApi/DeleteSupplier?supplierName=' + encodeURIComponent(supplierName));
    };
    dataFactory.UpdateSupplierDetails = function (supplierName, COUNTRY, STATE, request) {
        return $http.post(urlBase + '/HomeApi/UpdateSupplierDetails?supplierName=' + encodeURIComponent(supplierName) + '&Country=' + COUNTRY + '&State=' + STATE, JSON.stringify(request));
    };
    dataFactory.GetImageDrivePath = function () {
        return $http.post(urlBase + '/HomeApi/GetImageDrivePath');
    };
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------


    dataFactory.GetCopyProjectDetails = function (catalogid, projectType) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetCopyProjectDetails?catalogid=" + catalogid + "&projectType=" + projectType);
    };
    dataFactory.GetMoveProjectDetails = function (catalogid, projectType, projectId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetMoveProjectDetails?catalogid=" + catalogid + "&projectType=" + projectType + "&projectId=" + projectId);
    };
    dataFactory.moveXml = function (recordId, projectId) {
        return $http.post(urlBase + "/HomeApi/moveXml?recordId=" + recordId + "&projectId=" + projectId);
    };
    dataFactory.copyXml = function (recordId, projectId) {
        return $http.post(urlBase + "/HomeApi/copyXml?recordId=" + recordId + "&projectId=" + projectId);
    };
    //----------------Navigation Category Delete-------------------------------------------------------------------------------------------------------------
    dataFactory.CategoryremoveFromNavigator = function (categoryId, MultipleCategoryIds, catalogId, flag) {

        return $http.post(urlBase + '/HomeApi/CategoryremoveFromNavigator?categoryId=' + categoryId + '&MultipleCategoryIds=' + MultipleCategoryIds + '&catalogId=' + catalogId + '&flag=' + flag);
    };
    dataFactory.CategorytrashFromNavigator = function (categoryId, MultipleCategoryIds, catalogId, flag) {

        return $http.post(urlBase + '/HomeApi/CategorytrashFromNavigator?categoryId=' + categoryId + '&MultipleCategoryIds=' + MultipleCategoryIds + '&catalogId=' + catalogId + '&flag=' + flag);
    };
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------Navigation family delete-------------------------------------------------------------------------------------------
    dataFactory.FamilyremoveFromNavigator = function (MultipleCategoryIds, catalogId, flag, familyid) {
        return $http.post(urlBase + '/HomeApi/FamilyremoveFromNavigator?MultipleCategoryIds=' + MultipleCategoryIds + '&catalogId=' + catalogId + '&flag=' + flag + '&family_id=' + familyid);
    };
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------Navigation family delete-------------------------------------------------------------------------------------------
    dataFactory.FamilytrashFromNavigator = function (MultipleCategoryIds, catalogId, flag, familyid, ids, ProdCheckAssociate) {
        return $http.post(urlBase + '/HomeApi/FamilytrashFromNavigator?MultipleCategoryIds=' + MultipleCategoryIds + '&catalogId=' + catalogId + '&flag=' + flag + '&family_id=' + familyid + "&ids=" + ids + "&ProdCheckAssociate=" + ProdCheckAssociate);
    };
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------Navigator Association(MasterCatalog option)-------------------------------------------------------------------------
    dataFactory.Navigatorassociation = function (catalogId, ids, category_id, unchkids, option) {
        var data = [];
        data.push(unchkids);
        return $http.post(urlBase + '/HomeApi/NavigatorAssociation?catalogId=' + catalogId + '&ids=' + ids + '&categoryId=' + category_id + '&Option=' + option, JSON.stringify(data));
    };
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------

    //------------Navigator Association remove-----------------------------------------------------------------------------------------------------
    dataFactory.Navigatorassociationremove = function (catalogId, category_id, ids) {

        var data = [];
        data.push(ids);
        return $http.post(urlBase + '/HomeApi/Navigatorassociationremove?catalogId=' + catalogId + "&categoryId=" + category_id, JSON.stringify(data));
    };
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------TO GET THE CLONE FAMILY -----------------------------------------------------------------------------------------//
    dataFactory.GetCloneMasterFamily = function (catalogId, ids) {

        if (catalogId == undefined || ids == undefined || ids == "" || ids == '') {
            catalogId = 1;
            ids = 1;
        }
        return $http.get(urlBase + '/HomeApi/GetCloneFamily?Ids=' + ids + "&catalogId=" + catalogId);

    }
    //---------------------------------------------------------------------------------------------------------------------------------------------//
    //----------------------------------------------------------------SET THE CLONE FAMILY TO THE MASTER-----------------------------------------------------------------------------//
    dataFactory.Addclonefamily = function (ids, CATALOG_ID, CATEGORY_ID) {
        return $http.get(urlBase + '/HomeApi/GetMasterfamily?Ids=' + ids + "&catalogId=" + CATALOG_ID + "&categoryId=" + CATEGORY_ID);
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------//
    //--------------------------------------------------------TRASH CLONE FAMILY-------------------------------------------------------------------------------------//
    dataFactory.TrashCloneFamily = function (MultipleCategoryIds, catalogId, flag, familyid, ids) {

        return $http.get(urlBase + "/HomeApi/GetTrashCloneFamily?MultipleCategoryIds=" + MultipleCategoryIds + '&catalogId=' + catalogId + '&flag=' + flag + "&family_id=" + familyid + "&ids=" + ids);
    };
    //---------------------------------------------------------------------------------------------------------------------------------------------//
    dataFactory.AddTrashclonefamily = function (MultipleCategoryIds, catalogId, flag, familyid) {


        return $http.get(urlBase + "/HomeApi/GetTrashclonefamily?MultipleCategoryIds=" + MultipleCategoryIds + '&catalogId=' + catalogId + '&flag=' + flag + "&family_id=" + familyid);
    };
    //-----------------getTemplate ----------------------------------------------------------------------

    dataFactory.getTemplate = function () {
        return $http.get(urlBase + "/XpressCatalogApi/getTemplate");
    };

    dataFactory.FamilyLevelMultipletablePreview = function (customerId) {
        return $http.get(urlBase + '/HomeApi/FamilyLevelMultipletablePreview?CustomerId=' + customerId);
    };

    dataFactory.GetAttributeDetails = function (catalogId, familyId) {
        return $http.get(urlBase + "/HomeApi/GetAttributeDetails?catalogId=" + catalogId + "&familyId=" + familyId);
    };
    dataFactory.GetFamilyAttributeDetails = function (catalogId, familyId, categoryid) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (familyId === undefined || familyId === '' || familyId === "") {
            familyId = 0;
        }
        if (categoryid === undefined || categoryid === '' || categoryid === "") {
            categoryid = 0;
        }
        return $http.get(urlBase + "/HomeApi/GetFamilyAttributeDetails?catalogId=" + catalogId + "&familyId=" + familyId + "&categoryid=" + categoryid);
    };
    dataFactory.GetAllWorkFlowDataSourceValue = function (request) {
        return $http.post(urlBase + "/HomeApi/GetAllWorkFlowDataSourceValue", JSON.stringify(request));
    };

    dataFactory.UpdateWorkFlowData = function (request) {
        return $http.post(urlBase + "/HomeApi/UpdateWorkFlowData", JSON.stringify(request));
    };
    dataFactory.saveProductSortOrder = function (catalogId, categoryId,attr1, attr2, attr3, attr4, attr5, sortValue1, sortValue2, sortValue3, sortValue4, sortValue5, familyId) {
        return $http.post(urlBase + '/HomeApi/saveProductSortOrder?catalogId=' + catalogId + '&categoryId=' + categoryId + '&attr1=' + attr1 + '&attr2=' + attr2 + '&attr3=' + attr3 + '&attr4=' + attr4 + '&attr5=' + attr5 + '&sortValue1=' + sortValue1 + '&sortValue2=' + sortValue2 + '&sortValue3=' + sortValue3 + '&sortValue4=' + sortValue5 + '&sortValue5=' + sortValue5 + '&familyIds=' + familyId);
    };
     dataFactory.finishImport = function (sessionId, Sheet, allowduplicate, importType, catalogId, Path, batch, scheduleDate,templateId, data) {
        return $http.post(urlBase + '/AdvanceImportApi/finishImport?sessionId=' + sessionId + '&SheetName=' + Sheet + '&allowDuplicate=' + allowduplicate + '&catalogId=' + catalogId + '&importType=' + importType + '&excelPath=' + Path + '&batch=' + batch + '&scheduleDate=' + scheduleDate + '&templateId=' + templateId, JSON.stringify(data));
    };
     dataFactory.Validateimport = function (Sheet, allowduplicate, Path, importtype, catalogId, Errorlogoutputformat, validation, templateId, data) {
         var importData = [];
         importData.push(data);
         importData.push(validation);
         return $http.post(urlBase + '/ImportApi/Validateimport?SheetName=' + Sheet + '&allowDuplicate=' + allowduplicate + '&excelPath=' + Path + '&importtype=' + importtype + '&catalogId=' + catalogId + '&Errorlogoutputformat=' + Errorlogoutputformat + '&Errorlogoutputformat=' + Errorlogoutputformat + '&templateId=' + templateId, JSON.stringify(importData));
     };

    dataFactory.SaveAsTemplatePost = function (allowDuplicate, sheetName, excelPath, prodData) {
        return $http.post(urlBase + '/ImportApi/SaveAsTemplate?allowDuplicate=' + allowDuplicate + '&sheetName=' + sheetName + '&excelPath=' + excelPath, JSON.stringify(prodData));
    };
    dataFactory.DownloadTemplate = function (fileName) {
        return $http.get(urlBase + '/ImportApi/DownloadTemplate?fileName=' + fileName);
    };
    dataFactory.LoadXmlFile = function (fileName, importType) {
        return $http.get(urlBase + '/ImportApi/LoadXmlFile?templatePath=' + fileName + '&importType=' + importType);
    };

    dataFactory.LoadXmlFileTSheet = function (fileName) {
        return $http.get(urlBase + '/ImportApi/LoadXmlFileTSheet?templatePath=' + fileName);
    };
    dataFactory.GetUserRoleRightsAll = function (catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + "/UserAdminApi/GetUserRoleRightsAll?catalogId=" + catalogId);
    };
    dataFactory.GetUserWiseCatalog = function (catalogId) {
        return $http.get(urlBase + "/UserAdminApi/GetUserWiseCatalog?catalogId=" + catalogId);
    };

    dataFactory.getUserRoleRights = function (functionId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        if (catalogId == undefined) { catalogId = 0; }
        return $http.get(urlBase + "/UserAdminApi/getUserRoleRights?functionId=" + functionId + "&catalogId=" + catalogId);
    };
    dataFactory.getUserSuperAdmin = function () {
        return $http.get(urlBase + "/UserAdminApi/getUserSuperAdmin");
    };

    dataFactory.RemoveUserInvite = function (userinviteid) {
        return $http.post(urlBase + '/UserAdminApi/RemoveUserInvite?userinvite=' + userinviteid);
    };
    dataFactory.getUserProductSKUCount = function () {

        return $http.get(urlBase + "/HomeApi/getuserproductskucount");
    };
    dataFactory.getUserProductCount = function (optionStr, ids) {

        return $http.get(urlBase + "/HomeApi/getuserproductcount?optionStr=" + optionStr + "&ids=" + ids);
    };
    //---- START Cloned Category---
    dataFactory.GetOriginalCategory = function (familyid, catalogid, option, categoryId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        if (categoryId == undefined)
            categoryId = "";

        return $http.get(urlBase + '/CategoryApi/GetOriginalCategory?id=' + familyid + '&catalogid=' + catalogid + '&option=' + option + '&categoryId=' + categoryId);
    };

    dataFactory.DeletecloneFamily = function (familyid, catalogid, categoryId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }


        return $http.get(urlBase + '/CategoryApi/RemoveClonedFamily?id=' + familyid + '&catalogid=' + catalogid + '&categoryId=' + categoryId);
    };

    dataFactory.CheckmasterFamily = function (familyid, catalogid, categoryId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }


        return $http.get(urlBase + '/CategoryApi/ChkMasterFamily?id=' + familyid + '&catalogid=' + catalogid + '&categoryId=' + categoryId);
    };

    dataFactory.SwitchMasterFamily = function (familyid, catalogid, categoryId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }


        return $http.get(urlBase + '/CategoryApi/SwitchMasterFamily?id=' + familyid + '&catalogid=' + catalogid + '&categoryId=' + categoryId);
    };

    dataFactory.SwitchMasterFamilyNavigatorRemove = function (familyid, catalogid, categoryId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }


        return $http.get(urlBase + '/CategoryApi/SwitchMasterFamilyNavigatorRemove?id=' + familyid + '&catalogid=' + catalogid + '&categoryId=' + categoryId);
    };

    //---- END Cloned Category---

    //-----------Search with globalattributes-----------------//
    dataFactory.SearchWithGlobalAttributes = function (catalogId, text, check) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/SearchWithGlobalAttributes?catalogId=' + catalogId + '&searchtext=' + text + '&check=' + check);
    };

    dataFactory.getUserDefaultCatalog = function () {
        return $http.get(urlBase + "/HomeApi/getUserDefaultCatalog");
    };

    dataFactory.multipletablespecs = function (catalogId, id, displayID, categoryId, grouId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/multipletablespecs?catalogid=' + catalogId + '&familyIdString=' + id + '&displayID=' + displayID + '&CategoryId=' + categoryId + '&group_id=' + grouId);
    };

    dataFactory.multipleTableSpecs = function (catalogId, id, displayID, categoryId, pageNo, prodCount, subId, grouId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/multipleTableSpecs?catalogid=' + catalogId + '&familyIdString=' + id + '&displayID=' + displayID + '&CategoryId=' + categoryId + '&pageNo=' + pageNo + '&prodCount=' + prodCount + '&subId=' + subId + '&group_id=' + grouId);
    };

    dataFactory.getProducts = function (request, familyId, catalog_id) {

        return $http.get(urlBase + '/HomeApi/getProducts?familyId=' + familyId + '&catalogId=' + catalog_id);
    };

    dataFactory.SaveSubProduct = function (familyId, categoryId, catalogId, selectedItem, copycat, customerid, parentproductId, sortOrderValue, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/SaveSubProduct?familyId=' + familyId + '&categoryId=' + categoryId + '&catalogID=' + catalogId + '&copycat=' + copycat + '&customerid=' + customerid + '&parentproductId=' + parentproductId + '&sortOrderValue=' + sortOrderValue, "[" + JSON.stringify(request) + "," + JSON.stringify(selectedItem.split("\n")) + "]");
    };
    dataFactory.Getsubproductspecs = function (catalogId, id, displayID, categoryId, ParentproductId, pageno, perpagecount) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetSubProductspecs?catalogid=' + catalogId + '&familyIdString=' + id + '&displayID=' + displayID + '&CategoryId=' + categoryId + '&ParentProductId=' + ParentproductId + '&Pageno=' + pageno + '&Perpagecount=' + perpagecount);
    };
    dataFactory.GetsubproductspecsWithoutAttributes = function (catalogId, id, displayID, categoryId, ParentproductId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetSubProductspecsWithoutAttributes?catalogid=' + catalogId + '&familyIdString=' + id + '&displayID=' + displayID + '&CategoryId=' + categoryId + '&ParentProductId=' + ParentproductId);
    };
    dataFactory.GetAllCatalogattributesSubproducts = function (catalogid, familyId, categoryId, ParentProduct, category_id) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetAllCatalogattributesSubproducts?catalogId=" + catalogid + "&familyIdString=" + familyId + '&categoryId=' + categoryId + '&ParentProduct=' + ParentProduct + '&category_id=' + category_id);
    };
    dataFactory.GetprodfamilyAttributessubproducts = function (catalogid, familyId, Parentproduct, category_id) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetprodfamilyAttributessubproducts?catalogId=" + catalogid + "&familyIdString=" + familyId + "&ParentproductId=" + Parentproduct + '&category_id=' + category_id);
    };
    dataFactory.SavePublishAttributesforsubproducts = function (categoryId, ParentProductId, request) {
        return $http.post(urlBase + "/HomeApi/SavePublishAttributesforsubproducts?categoryId=" + categoryId + "&ParentProductId=" + ParentProductId, JSON.stringify(request));
    };
    dataFactory.UnPublishAttributessubproducts = function (ParentProductId, request) {
        return $http.post(urlBase + "/HomeApi/UnPublishAttributessubproducts?ParentProductId=" + ParentProductId, JSON.stringify(request));
    };
    dataFactory.DeletePublishAttributessubproducts = function (ParentProductId, category_id, request) {
        return $http.post(urlBase + "/HomeApi/DeletePublishAttributessubproducts?ParentProductId=" + ParentProductId + '&category_id=' + category_id, JSON.stringify(request));
    };

    dataFactory.GetAttributeDetailsSubproduct = function (catalogId, familyId, ParentProductId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetAttributeDetailsSubproduct?catalogId=" + catalogId + "&familyId=" + familyId + "&ParentProductId=" + ParentProductId);
    };

    dataFactory.SubProductPreview = function (catalogId, familyId, productId, ProductLevelMultipletablePreview, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/ViewSubProductPreview?catalogId=' + catalogId + '&familyId=' + familyId + '&productId=' + productId + '&ProductLevelMultipletablePreview=' + ProductLevelMultipletablePreview + '&categoryId=' + categoryId);
    };
    //-----------------------------------For SubProducts-----------------------------------------------------------------------------------//
    dataFactory.SubproductsimportExcelSheetSelection = function (excelPath) {
        return $http.get(urlBase + "/SubProductsImportApi/ImportExcelSheetSelection?excelPath=" + excelPath);
    };

    dataFactory.SubproductsGetImportSpecs = function (sheetName, excelPath) {
        return $http.get(urlBase + '/SubProductsImportApi/GetImportSpecs?sheetName=' + sheetName + '&excelPath=' + excelPath);
    };

    dataFactory.ValidatesubfinishImport = function (Sheet, allowduplicate, path, Errorlogoutputformatsub, data) {
        return $http.post(urlBase + '/SubProductsImportApi/validatesubproductsImport?sheetName=' + Sheet + '&allowDuplicate=' + allowduplicate + '&excelPath=' + path + '&Errorlogoutputformatsub=' + Errorlogoutputformatsub, JSON.stringify(data));
    };
    dataFactory.SubProductsfinishImport = function (Sheet, allowduplicate, path, data) {
        return $http.post(urlBase + '/SubProductsImportApi/FinishImport?sheetName=' + Sheet + '&allowDuplicate=' + allowduplicate + '&excelPath=' + path, JSON.stringify(data));
    };
    //-------------------------------------------For inverted products------------------------------//
    dataFactory.GetImportSpecsforinvertedproductsimport = function (sheetName, excelPath) {
        return $http.get(urlBase + '/SubProductsImportApi/GetImportSpecsforinvertedproductsimport?sheetName=' + sheetName + '&excelPath=' + excelPath);
    };

    //-----------------------------sub products paste-----------------------------------------//////////
    dataFactory.PasteSubProduct = function (familyId, newfamily_id, product_ids, flag, ParentProductId, customer_id, catalog_id, OldParentProductId, category_id, passselecetedCategoryId, oldcatloggid, sortOrderProduct) {
        return $http.post(urlBase + '/HomeApi/PasteSubProduct?familyId=' + familyId + '&newFamilyId=' + newfamily_id + '&product_ids=' + product_ids + '&falg=' + flag + '&ParentProductId=' + ParentProductId + '&customer_id=' + customer_id + '&catalog_id=' + catalog_id + '&OldParentProductId=' + OldParentProductId + '&category_id=' + category_id + '&passselecetedCategoryId=' + passselecetedCategoryId + '&oldcatloggid=' + oldcatloggid + '&sortOrderValue=' + sortOrderProduct);
    };
    dataFactory.getFinishSubImportSuccessResults = function (SessionId) {
        return $http.get(urlBase + '/SubProductsImportApi/getFinishImportSuccessResults?SessionId=' + SessionId);
    };
    dataFactory.DeleteSubProductPermanent = function (parentproduct, subproduct, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/DeleteSubProductPermanent?ParentProduct=' + parentproduct + '&Subproduct=' + subproduct + '&catalogId=' + catalogId);
    };

    dataFactory.GetAllcommonvaluesubproducts = function (catalogId, familyId, categoryId, id, parentid) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/SubProductsImportApi/GetAllcommonvaluesubproducts?familyid=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId + '&id=' + id + '&parentid=' + parentid);
    };

    dataFactory.GetAllcommonvalueproducts = function (catalogId, familyId, categoryId, id) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/SubProductsImportApi/GetAllcommonvalueproducts?familyid=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId + '&id=' + id);
    };
    dataFactory.Getprodattributes = function (catalogId, familyId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/SubProductsImportApi/Getprodattributes?familyid=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);
    };
    dataFactory.Getsubprodattributes = function (catalogId, familyId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/SubProductsImportApi/Getsubprodattributes?familyid=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);
    };
    dataFactory.SaveCommonProductSpecs = function (catalogId, familyId, categoryId, selectedprodids, commonvalue, attributeid, imageCaption, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/SubProductsImportApi/SaveCommonProductSpecs?familyid=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId + '&selectedprodids=' + selectedprodids + '&commonvalue=' + encodeURIComponent(commonvalue) + '&attributeid=' + attributeid + '&imageCaption=' + imageCaption, JSON.stringify(request));
    };
    dataFactory.SaveCommonSubProductSpecs = function (catalogId, familyId, categoryId, selectedprodids, commonvalue, attributeid, imageCaption, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/SubProductsImportApi/SaveCommonSubProductSpecs?familyid=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId + '&selectedprodids=' + selectedprodids + '&commonvalue=' + encodeURIComponent(commonvalue) + '&attributeid=' + attributeid + '&imageCaption=' + imageCaption, JSON.stringify(request));
    };
    dataFactory.getsubProductSortorder = function (parentProductId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetSubProductSortorder?parentProductId=' + parentProductId + '&catalogId=' + catalogId);
    };

    dataFactory.GetOpenPDFCatalogattributes = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + '/XpressCatalogApi/GetOpenPDFCatalogattributes?catalogid=' + catalogid);
    };

    dataFactory.GetOpenPDFSelectedattributes = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/XpressCatalogApi/GetOpenPDFSelectedattributes?catalogid=' + catalogid);
    };
    dataFactory.GetOpenPDFSummary = function () {
        return $http.get(urlBase + "/XpressCatalogApi/GetOpenPDFSummary");
    };

    dataFactory.OpenUpdateXpressProject = function (selecteddata) {
        var data = [];
        // data.push(projectmodel);
        data.push(selecteddata);
        return $http.post(urlBase + "/XpressCatalogApi/OpenUpdateXpressProject", JSON.stringify(data));
    };
    dataFactory.FindProjectType = function () {
        return $http.get(urlBase + "/XpressCatalogApi/FindProjectType");
    };

    //-------------------------------------Global Attribute Sub Products----------------------------------

    dataFactory.getCategoriessubProducts = function (catalogId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/SubProductsImportApi/GetCategoriessubProducts?catalogid=' + catalogId + '&categoryId=' + categoryId);
    };

    dataFactory.updateGlobalAttrValueForSelectedFamilysub = function (catalogId, attrType, attrId, attValue, familyIds) {
        return $http.post(urlBase + '/FamilyApi/UpdateGlobalAttrValueForSelectedFamilysub?catalogId=' + catalogId + "&attrType=" + attrType + "&attrId=" + attrId + "&attrValue=" + attValue, JSON.stringify(familyIds));
    };
    dataFactory.getPlanFunctionGroup = function (planId) {
        return $http.get(urlBase + '/UserAdminApi/GetPlanFunctionGroup?planId=' + planId);
    };
    dataFactory.getRoleFunctionGroup = function (roleId) {
        return $http.get(urlBase + '/UserAdminApi/GetRoleFunctionGroup?roleId=' + roleId);
    };
    dataFactory.getMyProfileFunctionGroup = function (username) {
        return $http.get(urlBase + '/UserAdminApi/GetMyProfileFunctionGroup?username=' + username);
    };
    dataFactory.getPlanFunctionsByGroupId = function (planId, functionGrpId) {
        return $http.get(urlBase + '/UserAdminApi/GetPlanFunctionsByGroupId?planId=' + planId + '&functionGrpId=' + functionGrpId);
    };
    dataFactory.getRolePlanFunctionsByGroupId = function (roleId, functionGrpId) {
        return $http.get(urlBase + '/UserAdminApi/GetRolePlanFunctionsByGroupId?roleId=' + roleId + '&functionGrpId=' + functionGrpId);
    };
    dataFactory.getMyProfilePlanFunctionsByGroupId = function (username, functionGrpId) {
        return $http.get(urlBase + '/UserAdminApi/GetMyProfilePlanFunctionsByGroupId?username=' + username + '&functionGrpId=' + functionGrpId);
    };
    dataFactory.GetAttributeListByCatalogId = function (catalogId, categoryId, attributeId, famarray) {
        //familyId,
        //   + '&familyId=' + familyId
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/GetAttributeListByCatalogId?catalogId=' + catalogId + '&categoryId=' + categoryId + '&attributeId=' + attributeId, JSON.stringify(famarray));
    };
    dataFactory.GetAttributeValues = function (catalogId, categoryId, attributeId, familyId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/GetAttributeValues?catalogId=' + catalogId + '&categoryId=' + categoryId + '&attributeId=' + attributeId, JSON.stringify(familyId)); //GetCatalogDetails
    };
    dataFactory.GetAllcommonvaluesubproductsCheck = function (catalogId, familyId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/SubProductsImportApi/GetAllcommonvaluesubproductsCheck?familyid=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);
    };


    //----------------------------------For AttributeMapping START--------------------------------------------//
    dataFactory.attributemapping = function (excelPath) {
        return $http.get(urlBase + "/AttributeMappingApi/ImportExcelSheetSelection?excelPath=" + excelPath);
    };
    dataFactory.attributemappingGetImportSpecs = function (sheetName, excelPath) {
        return $http.get(urlBase + '/AttributeMappingApi/GetImportSpecs?sheetName=' + sheetName + '&excelPath=' + excelPath);
    };
    dataFactory.attributemappingfinishImport = function (Sheet, allowduplicate, path, data) {
        return $http.post(urlBase + '/AttributeMappingApi/FinishImport?sheetName=' + Sheet + '&allowDuplicate=' + allowduplicate + '&excelPath=' + path, JSON.stringify(data));
    };

    dataFactory.BtnFamilyfinishImport = function (Sheet, allowduplicate, path, Deleteproducts, data) {

        return $http.post(urlBase + '/AttributeMappingApi/BtnFamilyfinishImport?sheetName=' + Sheet + '&allowDuplicate=' + allowduplicate + '&excelPath=' + path + '&Deleteproducts=' + Deleteproducts, JSON.stringify(data));
    };
    //---------------------------------- AttributeMapping END--------------------------------------------//

    //-------------------------------For Custom Table Management START-----------------------------------//
    dataFactory.Customtablemgmt = function () {
        return $http.post(urlBase + '/AttributeMappingApi/Customtablemgmt?');
    };
    dataFactory.Customtablemgmtexp = function () {
        return $http.post(urlBase + '/AttributeMappingApi/Customtablexport?');
    };
    //-------------------------------For Custom Table Management End-----------------------------------//

    //-------------------------------FOR CUSTOMER INCENTIVE TABLE START-----------------------------------//
    dataFactory.Customerincentivetable = function () {
        return $http.get(urlBase + '/AttributeMappingApi/Customerincentivetable');
    };

    dataFactory.Customerincentiveimport = function () {

        return $http.post(urlBase + '/AttributeMappingApi/Customerincentivetableimport');
    };

    dataFactory.adsortgrid = function () {
        return $http.get(urlBase + '/AttributeMappingApi/Sortmgmt');
    };

    dataFactory.ddlDataSource112 = function () {
        return $http.get(urlBase + '/AttributeMappingApi/Sortmgmtdropdown');
    };

    dataFactory.getFinishImportFailedpicklistResults = function (SessionId) {

        return $http.get(urlBase + '/ImportApi/getFinishImportFailedpicklistResults?SessionId=' + SessionId);
    };

    dataFactory.getFinishFamilyImportResults = function (SessionId) {

        return $http.get(urlBase + '/ImportApi/getFinishFamilyImportResults?SessionId=' + SessionId);
    };

    dataFactory.getFinishImportFailedpicklistResults12 = function (SessionId) {

        return $http.get(urlBase + '/ImportApi/getFinishImportFailedpicklistResults12?SessionId=' + SessionId);
    };

    dataFactory.getFinishImportFailedpicklistResultssubproducts = function (SessionId) {

        return $http.get(urlBase + '/ImportApi/getFinishImportFailedpicklistResultssubproducts?SessionId=' + SessionId);
    };

    dataFactory.getFinishImportFailedpicklistResultssubproductserror = function (SessionId) {

        return $http.get(urlBase + '/ImportApi/getFinishImportFailedpicklistResultssubproductserror?SessionId=' + SessionId);
    };
    dataFactory.getFinishImportFailedpicklistResultssubproductserrordelete = function (SessionId) {

        return $http.get(urlBase + '/ImportApi/getFinishImportFailedpicklistResultssubproductserrordelete?SessionId=' + SessionId);
    };
    dataFactory.GetInvertedProductsCount = function (catalogId, opt) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/InvertedProduct/GetInvertedProductsCount?catalogId=' + catalogId + '&opt=' + opt);
    };

    //-------------------------------FOR CUSTOMER INCENTIVE TABLE END-----------------------------------//

    // --------------- Add New Sub Product -------------------------//
    dataFactory.GetAddProductAttributeDetails = function (catalogId, familyId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetAddProductAttributeDetails?catalogId=" + catalogId + "&familyId=" + familyId);
    };
    dataFactory.getprodattr = function (catalogId, id, displayID, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetProdSpecs?catalogid=' + catalogId + '&familyIdString=' + id + '&displayID=' + displayID + '&CategoryId=' + categoryId);
    };
    dataFactory.getsubprodattr = function (catalogId, id, displayId, categoryId, parentproductId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetSubProductspecs?catalogid=' + catalogId + '&familyIdString=' + id + '&displayId=' + displayId + '&categoryId=' + categoryId + '&parentProductId=' + parentproductId);
    };
    dataFactory.GetAddSubProductAttributeDetails = function (catalogId, familyId, parentproductId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + "/HomeApi/GetAddSubProductAttributeDetails?catalogId=" + catalogId + "&familyId=" + familyId + '&parentProductId=' + parentproductId);
    };
    dataFactory.SaveNewSubProduct = function (customerid, request, parentproductId, sortOrderValue) {
        return $http.post(urlBase + '/HomeApi/SaveNewSubProduct?customerid=' + customerid + '&parentProductId=' + parentproductId + '&sortOrderValue=' + sortOrderValue, JSON.stringify(request));
    };
    //--------------- End Add New Sub Product -------------------------

    dataFactory.GetCategoryLevels = function (categoryId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/CategoryApi/GetCategoryLevels?categoryId=' + categoryId + "&catalogId=" + catalogId);
    };

    dataFactory.GetAllattributesCount = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }


        return $http.get(urlBase + '/AttributeApi/GetAllattributesCount?catalogid=' + catalogid);
    };

    //Attribute Export

    dataFactory.GetAllAttributesExport = function (catalogid, attributetype, displayIdcolumns) {

        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        if (attributetype == undefined || attributetype == '' || attributetype == "") {
            attributetype = 0;
        }
        return $http.get(urlBase + '/AttributeApi/GetAllAttributesExport?catalogid=' + catalogid + '&attributetype=' + attributetype + '&displayIdcolumns=' + displayIdcolumns);
    };

    //Attribute viewlog

    dataFactory.attrViewLog = function () {
        return $http.post(urlBase + '/AttributeApi/AttributeViewLog');
    }

    //Attribute download log

    dataFactory.validationdownloadresult = function (Errorlogoutputformat, fileName) {

        return $http.post(urlBase + '/AttributeApi/importlogs?Errorlogoutputformat=' + Errorlogoutputformat + '&fileName=' + fileName);
    };

    //Attribute Import sheet validation

    dataFactory.validateAttrImport = function (importExcelSheetDDSelectionValue, excelPath, importFormat) {

        return $http.post(urlBase + '/AttributeApi/ValidateAttrImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath + '&importFormat=' + importFormat);
    }
    //Attribute Import 
    dataFactory.attrImport = function (importExcelSheetDDSelectionValue, excelPath, importType, importFormat, catalog_Id) {

        return $http.post(urlBase + '/AttributeApi/AttrImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath + '&importType=' + importType + '&importFormat=' + importFormat + '&catalog_Id=' + catalog_Id);
    }
    //inverted products import
    dataFactory.validateProdImport = function (importExcelSheetDDSelectionValue, excelPath, importFormat) {

        return $http.post(urlBase + '/InvertedProduct/ValidateAttrImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath + '&importFormat=' + importFormat);
    }
    //inverted products import
    dataFactory.prodImport = function (importExcelSheetDDSelectionValue, excelPath, importType, importFormat, catalog_Id, data) {

        return $http.post(urlBase + '/InvertedProduct/ProdImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath + '&importType=' + importType + '&importFormat=' + importFormat + '&catalog_Id=' + catalog_Id, JSON.stringify(data));
    }

    dataFactory.GetAllattributesManager = function (catalogid, attributetype) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }


        if (attributetype == undefined) {
            attributetype = 0;
        }
        return $http.get(urlBase + '/AttributeApi/GetAllattributesManager?catalogid=' + catalogid + '&attributetype=' + attributetype);
    };
    dataFactory.ClearReferenceTableAttachement = function (cellId, imageValue) {
        return $http.get(urlBase + '/HomeApi/ClearReferenceTableAttachement?rowId=' + cellId + '&imageValue=' + imageValue);
    };
    dataFactory.Attributedatatype = function (attr_id) {
        return $http.post(urlBase + '/InvertedProduct/Getattribdatatype?attrib_id=' + attr_id);
    };
    dataFactory.GetAllSpecifiedattributes = function (catalogid, searchType) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetAllSpecifiedattributes?catalogid=' + catalogid + '&searchType=' + searchType);
    };

    //-------------------------------------------------------------Asset management function call starts----------------------------------------------------------//
    dataFactory.GetImageFiles = function (catalogId, customerId, path, companyName) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

        var encrutedcompanyName = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(companyName), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        var encrutedPath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(path), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/ImagManagementApi/GetImageFiles?catalogId=' + catalogId + '&customerId=' + customerId + '&path=' + encrutedPath.toString() + '&companyName=' + encrutedcompanyName.toString());
    };
    dataFactory.SaveImageManagementDetails = function (path, folderName, customerId, option, fileType) {

        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

        var encrutedfolderName = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(folderName), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        var encrutedPath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(path), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return $http.post(urlBase + '/ImagManagementApi/SaveImageManagementDetails?path=' + encrutedPath.toString() + '&folderName=' + encrutedfolderName.toString() + '&customerId=' + customerId + '&option=' + option + '&fileType=' + fileType);
    };
    dataFactory.DeleteImageManagementDetails = function (path, customerId, fileType, fileCount, data) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var encrutedpath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(path), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        return $http.post(urlBase + '/ImagManagementApi/DeleteImageManagementDetails?path=' + encrutedpath.toString() + '&customerId=' + customerId + '&fileType=' + fileType + '&fileCount=' + fileCount, JSON.stringify(data));
    };
    dataFactory.CompressImageManagementDetails = function (path, customerId, fileType, data) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var encrutedpath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(path), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return $http.post(urlBase + '/ImagManagementApi/CompressImageManagementDetails?path=' + encrutedpath.toString() + '&customerId=' + customerId + '&fileType=' + fileType, JSON.stringify(data));
    };
    dataFactory.CutCopyPasteDetails = function (data, path, customerId, pasteType, fileType) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var encrutedpath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(path), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return $http.post(urlBase + '/ImagManagementApi/CutCopyPasteDetails?path=' + encrutedpath.toString() + '&customerId=' + customerId + '&pasteType=' + pasteType + '&fileType=' + fileType, JSON.stringify(data));
    };

    dataFactory.ExtractDetails = function (data, path, customerId) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var encrutedpath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(path), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return $http.post(urlBase + '/ImagManagementApi/ExtractDetails?path=' + encrutedpath.toString() + '&customerId=' + customerId, JSON.stringify(data));
    };

    dataFactory.SaveImageManagementFile = function (data, path) {

        return $http.post(urlBase + '/ImagManagementApi/SaveImageManagementFile?path=' + path, JSON.stringify(data));
    };
    dataFactory.ExportCompressImageManagementDetails = function (path, customerId, fileType, data) {

        return $http.post(urlBase + '/ImagManagementApi/ExportCompressImageManagementDetails?path=' + path + '&customerId=' + customerId + '&fileType=' + fileType, JSON.stringify(data));
    };
    dataFactory.getFileSearchResult = function (path, searchText, customerId) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var encryptedsearchText = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(searchText), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return $http.post(urlBase + '/ImagManagementApi/getFileSearchResult?path=' + path + '&searchText=' + encryptedsearchText.toString() + '&customerId=' + customerId);
    }

    //New Size based filter done on Dec 2022
    dataFactory.getFilesBasedOnGivenSize = function (path, assetSizeRange, assetSizeValue, assetFileSize) {
        return $http.get(urlBase + '/ImagManagementApi/GetFilesBasedOnGivenSize?path=' + path + '&assetSizeRange=' + assetSizeRange + '&assetSizeValue=' + assetSizeValue + '&assetFileSize=' + assetFileSize);
    }

     dataFactory.getAssetSizeRange = function () {
         return $http.get(urlBase + '/ImagManagementApi/GetAssetSizeRange');
    }     
     dataFactory.getAssetDateRange = function () {
         return $http.get(urlBase + '/ImagManagementApi/GetAssetDateRange');
     }
     dataFactory.getAssetFileSize = function () {
         return $http.get(urlBase + '/ImagManagementApi/GetAssetFileSize');
     }
    // Filter based on Date range
     dataFactory.getFilesByDate = function (day, path, fromRange, toRange) {
         return $http.get(urlBase + '/ImagManagementApi/GetFilesByDate?day=' + day + '&path=' + path + '&fromRange=' + fromRange + '&toRange=' + toRange);
     }


    //New Add to Starred done on Dec 2022
     dataFactory.addtoStarred = function (path, userName, catalogId, fileType, fileCount, data) {
         return $http.post(urlBase + '/ImagManagementApi/AddtoStarredAssets?path=' + path + '&userName=' + userName + '&catalogId=' + catalogId + '&fileType=' + fileType + '&fileCount=' + fileCount, JSON.stringify(data));
    };
    
    //New Add To display the Starred files on Dec 2022
    dataFactory.getStarredFiles = function (userName) {
        return $http.get(urlBase + '/ImagManagementApi/GetStarredFiles?userName=' + userName);
    }

    //New Add To display the Starred files on Dec 2022
    dataFactory.deleteFromStarred = function (userName, fileName) {
        return $http.post(urlBase + '/ImagManagementApi/DeleteFromStarred?userName=' + userName + '&fileName=' + fileName);
    }

    dataFactory.getLocation = function () {
        return $http.get(urlBase + '/ImagManagementApi/getLocation');
    }


    dataFactory.getLocation = function () {
        return $http.get(urlBase + '/ImagManagementApi/getLocation');
    }


    //recent files in asset management//
    dataFactory.recentOpenedFiles = function (encryptedPath) {

        //var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        //var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        //var encryptedPath = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(path), key, {
        //    keySize: 128 / 8,
        //    iv: iv,
        //    mode: CryptoJS.mode.CBC,
        //    padding: CryptoJS.pad.Pkcs7
        //});
        return $http.post(urlBase + '/ImagManagementApi/RecentOpenedFiles?path=' + encryptedPath.toString());
    };

    dataFactory.recentFilesClick = function (path) {
      
        return $http.get(urlBase + '/ImagManagementApi/RecentAssetFiles?path=' + path);
    };
    //Find option in asset management
    dataFactory.findAssociateImage = function (selectedImagePath) {

        return $http.post(urlBase + '/ImagManagementApi/FindAssociateImage?selectedImagePath=' + selectedImagePath);
    }
    dataFactory.removeAssociation = function (arrayImages) {

        return $http.post(urlBase + '/ImagManagementApi/RemoveAssociation', JSON.stringify(arrayImages));
    }
    // Assign option in Image Management

    dataFactory.GetImageAttributes = function (catalogId, Category_ids, attributeType, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/ImagManagementApi/GetImageAttributes?catalogId=' + catalogId + '&categoryIds=' + Category_ids + '&attributeType=' + attributeType, JSON.stringify(request));
    };

    //dataFactory.GetMultipleImageAttributes = function (request) {
    //    return $http.post(urlBase + '/ImagManagementApi/GetMultipleImageAttributes', JSON.stringify(request));
    //};

    dataFactory.GetMultipleImageAttributes = function (catalogId, Category_ids, attributeType, matchWith, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/ImagManagementApi/GetMultipleImageAttributes?catalogId=' + catalogId + '&categoryIds=' + Category_ids + '&attributeType=' + attributeType + '&matchWith=' + matchWith, JSON.stringify(request));
    };

    dataFactory.CategoriesForAssignOption = function (catalogId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/ImagManagementApi/CategoriesForAssignOption?catalogid=' + catalogId + '&categoryId=' + categoryId);
    };

    dataFactory.AssociateImages = function (catalogId, matchWith, AttributeType, SelectedAttributeId, category_ids, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + "/ImagManagementApi/AssociateImages?matchWith=" + matchWith + '&catalogId=' + catalogId + '&AttributeType=' + AttributeType + '&SelectedAttributeId=' + SelectedAttributeId + '&category_ids=' + category_ids, JSON.stringify(request));
    };

    dataFactory.SaveImageAttribute = function (attributeType, categoryIds, catalogId, request) {
        return $http.post(urlBase + '/ImagManagementApi/SaveImageAttribute?attributeType=' + attributeType + '&categoryIds=' + categoryIds + '&catalogId=' + catalogId, JSON.stringify(request));
    };

    dataFactory.AssociateMultipleImages = function (catalogId, matchWith, AttributeType, SelectedAttributeId, category_ids, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + "/ImagManagementApi/AssociateMultipleImages?matchWith=" + matchWith + '&catalogId=' + catalogId + '&AttributeType=' + AttributeType + '&category_ids=' + category_ids, JSON.stringify(request));
    };

    dataFactory.saveYoutubeLink = function (link, caption, filePath) {
        return $http.post(urlBase + "/ImagManagementApi/SaveYoutubeLink?linkVal=" + link + "&captionVal=" + caption + "&filePath=" + filePath);
    };
    //-------------------------------------------------------------Asset management function call ends----------------------------------------------------------//

    dataFactory.GetAllFiltersValue = function (attributeName) {
        return $http.get(urlBase + '/HomeApi/GetFilters?attributeName=' + encodeURIComponent(attributeName));
    };
    dataFactory.GetType = function (attributeName) {
        return $http.get(urlBase + '/HomeApi/GetFilterType?attributeName=' + encodeURIComponent(attributeName));
    };
    dataFactory.GetRecentlyModifiedSubProducts = function (catalogId, startDate, endDate, option, request) {

        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }


        return $http.post(urlBase + '/DashboardAPI/GetRecentlyModifiedSubProducts?catalogId=' + catalogId + '&startDate=' + startDate + '&endDate=' + endDate + '&option=' + option, JSON.stringify(request));

    };
    dataFactory.Getfirstsubfamilydetails = function (catalogId, productId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.get(urlBase + '/HomeApi/Getfirstsubfamilydetails?catalogId=' + catalogId + '&productId=' + productId);
    };


    dataFactory.GetTemplateForImport = function () {
        return $http.get(urlBase + '/AdvanceImportApi/GetTemplateForImport');
    }
    dataFactory.attributeimpmapping = function (ImportType, attributeType) {
        return $http.get(urlBase + '/AdvanceImportApi/Loadaattrmapping?importType=' + ImportType + '&attributeType=' + attributeType);
    };
    dataFactory.attributeimpmappingexp = function () {
        return $http.get(urlBase + '/AdvanceImportApi/Loadaattrmappingexp');
    };
    dataFactory.mappingattribute = function (newmappingname, oldmaapingattribute, templateId, importType,sheetName) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var encrutedNewattr = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(newmappingname), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        var encrutedoldattr = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(oldmaapingattribute), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return $http.post(urlBase + "/AdvanceImportApi/mappingattribute?newmappingname=" + encrutedNewattr.toString() + "&oldmaapingattribute=" + encrutedoldattr.toString() + "&templateId=" + templateId + "&importType=" + importType + "&sheetName=" + sheetName);
    };
    dataFactory.Updateformulaforattributes = function (newmappingname, oldmaapingattribute) {
        return $http.post(urlBase + "/AdvanceImportApi/Updateformulaforattributes?newmappingname=" + newmappingname + "&oldmaapingattribute=" + oldmaapingattribute);
    };
    dataFactory.getmissingcolumns = function (SessionId) {

        return $http.get(urlBase + '/AdvanceImportApi/getmissingcolumns?SessionId=' + SessionId);
    };
    dataFactory.SaveTemplateDetails = function (templateNameImport, catalogIdAttr, importFormat, filePath, modifyTemplateFlag, workBookFlag) {
        return $http.post(urlBase + '/AdvanceImportApi/SaveTemplateDetails?templateNameImport=' + templateNameImport + '&catalogId=' + catalogIdAttr + '&importFormat=' + importFormat + '&filePath=' + filePath + '&modifyTemplateFlag=' + modifyTemplateFlag + '&workBookFlag=' + workBookFlag);
    }
    dataFactory.getTemplateDetails = function (templateId) {
        return $http.post(urlBase + '/AdvanceImportApi/GetTemplateDetails?templateId=' + templateId);
    };

    dataFactory.DeleteTemplate = function (templateId) {
        return $http.post(urlBase + '/AdvanceImportApi/DeleteTemplate?templateId=' + templateId);
    };
    dataFactory.checkCatalogDetails = function (excelPath, sheetName, catalogId) {
        return $http.post(urlBase + '/AdvanceImportApi/CheckCatalogDetails?excelPath=' + excelPath + "&sheetName=" + sheetName + "&catalogId=" + catalogId)
    };

    dataFactory.GetAllPickListData = function (catalogId) {

        return $http.get(urlBase + '/HomeApi/GetAllPickListData?catalogId=' + catalogId);
    };

    dataFactory.GetImportStatus = function (excelPath, sheetName) {
        return $http.get(urlBase + '/AdvanceImportApi/GetImportStatus?excelPath=' + excelPath + '&sheetName=' + sheetName);
    };
    //dataFactory.getPickListDataValue = function (pickListName) {
    //   
    //    return $http.get(urlBase + '/HomeApi/getpicklistName?pkData=' + pickListName);
    //};

    //ProductTitle
    dataFactory.getProductTitleValue = function () {

        return $http.get(urlBase + '/AccountApi/getProductTitleValue');
    };

    //dataFactory.GetBatchLogList = function () {
    //    return $http.get(urlBase + '/AdvanceImportApi/GetBatchLogList');
    //}
    dataFactory.RunImportProcess = function (batchId) {
        return $http.get(urlBase + '/AdvanceImportApi/RunImportProcess?batchId=' + batchId);
    }

    dataFactory.DownLoadErrorLog = function (batchId) {
        return $http.get(urlBase + '/AdvanceImportApi/DownLoadErrorLog?batchId=' + batchId);
    }

    dataFactory.removeFromAll = function (selectedpicklist, selectedpicklisttype, request) {

        return $http.post(urlBase + '/HomeApi/RemoveFromAll?name=' + selectedpicklist + '&selectedpicklisttype=' + selectedpicklisttype, JSON.stringify(request));
    };

    dataFactory.GetBatchLogList = function () {
        return $http.get(urlBase + '/AdvanceImportApi/GetBatchLogList');
    }
    dataFactory.PauseImportProcess = function (batchId) {
        return $http.get(urlBase + '/AdvanceImportApi/PauseImportProcess?batchId=' + batchId);
    }

    dataFactory.ClearLog = function (request) {
        return $http.post(urlBase + '/AdvanceImportApi/ClearLog', JSON.stringify(request));
    }



    dataFactory.DownLoadErrorLog = function (batchId) {
        return $http.get(urlBase + '/AdvanceImportApi/DownLoadErrorLog?batchId=' + batchId);
    }

    dataFactory.removeFromAll = function (selectedpicklist, selectedpicklisttype, request) {

        return $http.post(urlBase + '/HomeApi/RemoveFromAll?name=' + selectedpicklist + '&selectedpicklisttype=' + selectedpicklisttype, JSON.stringify(request));
    };

    //--------------To get EnableSubProduct value from prefernce setting
    dataFactory.getEnableSubProduct = function () {

        return $http.get(urlBase + '/HomeApi/getEnableSubProduct');
    }
    //started websync process

    dataFactory.GetWebSynCategoryDetails = function (fromDate, toDate, catalogId, userName, id) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }


        return $http.post(urlBase + '/WebSyncApi/GetFiltersDetails?fromDate=' + fromDate + '&toDate=' + toDate + '&catalogId=' + catalogId + '&userName=' + userName + '&id=' + id);
    }
    dataFactory.GetUserList = function () {
        return $http.get(urlBase + '/WebSyncApi/GetUserList');
    }



    dataFactory.GetCategoryFamilyProductData = function (fromDate, toDate, catalogId, categoryIds, userName) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/WebSyncApi/GetCategoryFamilyProductData?fromDate=' + fromDate + '&toDate=' + toDate + '&catalogId=' + catalogId + '&categoryIds=' + categoryIds + '&userName=' + userName);
    }

    dataFactory.GetCategoryAttributes = function (categoryIds) {
        return $http.post(urlBase + '/WebSyncApi/GetCategoryAttribute?categoryIds=' + categoryIds);
    }
    dataFactory.GetDeletedHistory = function (fromDate, toDate,catalogId) {
        return $http.post(urlBase + '/WebSyncApi/DeletedHistory?fromDate=' + fromDate + '&toDate=' + toDate + '&catalogId' + catalogId);
    }
    //dataFactory.InsertSession = function (sessionId, endTime, send, updateType, jobType, user, jobName, catalogId, fromDate, toDate) {
    //    return $http.post(urlBase + '/WebSyncApi/InsertSession?sessionId=' + sessionId + '&endTime=' + endTime + '&send=' + send + '&updateType=' + updateType + '&jobType=' + jobType + '&user=' + user + '&jobName=' + jobName + '&catalogId=' + catalogId + '&fromDate=' + fromDate + '&toDate=' + toDate);
    //}
    //}

    dataFactory.InsertSession = function (ids1) {

        return $http.post(urlBase + '/WebSyncApi/InsertSession', JSON.stringify(ids1));
    }

    dataFactory.GetCategoryAttributes = function (categoryIds, data) {
        return $http.post(urlBase + '/WebSyncApi/GetCategoryAttributes?categoryIds=' + categoryIds, JSON.stringify(data));
    }
    //end websync process
    dataFactory.GetWebSyncUrl = function () {
        return $http.get(urlBase + '/WebSyncApi/GetWebSyncUrl')
    }

    //Custom menu

    dataFactory.saveCustom = function (Header, URL) {
        return $http.post(urlBase + '/UserAdminApi/saveCustom?Header=' + Header + '&URL=' + URL);
        //return $http.post(urlBase + '/UserAdminApiController/saveCustom?Header=' + Header + '&URL=' + URL);
    }
    dataFactory.getHeader = function () {
        return $http.get(urlBase + '/UserAdminApi/getHeader');
    }
    dataFactory.getUserRoleId = function () {
        return $http.get(urlBase + '/UserAdminApi/getUserRoleId');
    }
    dataFactory.getHeaderForMenu = function () {

        return $http.post(urlBase + '/UserAdminApi/getHeaderForMenu');
    };
    dataFactory.updateCustomValues = function (request) {

        return $http.post(urlBase + '/UserAdminApi/updateCustomValues', JSON.stringify(request));
    }
    dataFactory.deleteCustomemenu = function (ID) {
        return $http.post(urlBase + '/UserAdminApi/deleteCustomemenu?ID=' + ID);
    }

    dataFactory.SaveAnnouncement = function (AnnouncementDate, AnnouncementSubject, AnnouncementDescription) {
        return $http.post(urlBase + '/UserAdminApi/SaveAnnouncement?announcementDate=' + AnnouncementDate + '&announcementSubject=' + AnnouncementSubject + '&announcementDescription=' + AnnouncementDescription);
        //return $http.post(urlBase + '/UserAdminApiController/saveCustom?Header=' + Header + '&URL=' + URL);
    }
    dataFactory.SaveApplicationSettings = function (CustomerId, announcementCount, restoreSourcePath, restoreDestPath, serverPath, dataSource, serverUserID, Password, backUpAttachement, backUpMdfPath, backUpDataBasePath, ImportSheetProductCount) {
        return $http.post(urlBase + '/UserAdminApi/SaveApplicationSettings?CustomerId=' + CustomerId + '&announcementCount=' + announcementCount + '&restoreSourcePath=' + restoreSourcePath + '&restoreDestPath=' + restoreDestPath + '&serverPath=' + serverPath + '&dataSource=' + dataSource + '&serverUserID=' + serverUserID + '&password=' + Password + '&backUpAttachement=' + backUpAttachement + '&backUpMdfPath=' + backUpMdfPath + '&backUpDataBasePath=' + backUpDataBasePath + '&importSheetProductCount=' + ImportSheetProductCount);
        //return $http.post(urlBase + '/UserAdminApiController/saveCustom?Header=' + Header + '&URL=' + URL);
    }

    dataFactory.getAnnouncement = function () {

        return $http.post(urlBase + '/UserAdminApi/getAnnouncement');
    };

    dataFactory.getAnnouncementLastDateRecords = function () {

        return $http.post(urlBase + '/UserAdminApi/getAnnouncementLastDateRecords');
    };

    dataFactory.updateAnnouncementValues = function (id, description, header, date) {

        return $http.post(urlBase + '/UserAdminApi/updateAnnouncementValues?id=' + id + '&description=' + description + '&header=' + header + '&date=' + date);
    }
    dataFactory.deleteAnnouncement = function (ID) {
        return $http.post(urlBase + '/UserAdminApi/deleteAnnouncement?ID=' + ID);
    }

    dataFactory.AnnouncementPartialPage = function () {
        return $http.get(baseUrl + '/UserAdmin/Announcement');
    };
    //Get Item Import error List
    dataFactory.GetItemImportErrorlist = function () {
        return $http.get(urlBase + '/AdvanceImportApi/GetItemImportErrorlist')
    }
    //Set default family in dashboard
    dataFactory.setDefaultFamilyFromNavigator = function (selectedDefaultCatalogId, dFamilyId) {
        return $http.post(urlBase + '/HomeApi/setDefaultFamilyFromNavigator?selectedDefaultCatalogId=' + selectedDefaultCatalogId + '&dFamilyId=' + dFamilyId);
    }
    //GetUserCataloItemNumber
    dataFactory.GetDefaultCatalogItemNumber = function () {


        return $http.get(urlBase + '/HomeApi/getUserCatalogItemNumber');
    };
    //Export Get Log

    dataFactory.GetBatchExportLogList = function () {
        return $http.get(urlBase + '/ExportApi/GetBatchExportLogList');
    }

    dataFactory.RunExportProcess = function (BATCH_ID) {
        return $http.get(urlBase + '/ExportApi/RunExportProcess?BATCH_ID=' + BATCH_ID);
    }
    dataFactory.ClearExportLogByid = function (EXPORTID) {
        return $http.get(urlBase + '/ExportApi/DeleteExportBatchById?EXPORTID=' + EXPORTID);
    }


    dataFactory.ClearLogExport = function (request) {
        return $http.post(urlBase + '/ExportApi/ClearLogExport', JSON.stringify(request));
    }


    dataFactory.UpdateProjectForBatch = function (BatchProjectId, ScheduleProjectName) {
        return $http.get(urlBase + '/ExportApi/UpdateProjectForBatch?BatchProjectId=' + BatchProjectId + '&ScheduleProjectName=' + ScheduleProjectName);
    }


    // Get all dashboard data count

    dataFactory.GetDashboardCountDetails = function (catalogId) {
        return $http.post(urlBase + '/DashboardAPI/GetDashboardCountDetails?catalogId=' + catalogId);
    }

    //Get Display Id coloum---

    dataFactory.getDisplayIdColoum = function () {

        return $http.get(urlBase + '/HomeApi/getDisplayIdColoum');
    }

    //Attribute Grouping Start
    dataFactory.saveAttributeGrouping = function (catalogId, groupName, groupType, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + "/HomeApi/saveAttributeGrouping?catalogId=" + catalogId + '&groupName=' + groupName + '&groupType=' + groupType, JSON.stringify(request));
    };


    dataFactory.GetAllAttributegroups = function (catalogId, groupType, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + "/HomeApi/GetAllAttributegroups?catalogId=" + catalogId + '&groupType=' + groupType, JSON.stringify(request));
    };

    dataFactory.GetdropdownGroupNameforFamily = function (catalogid, groupType, familyId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        if (familyId === undefined || familyId === '' || familyId === "") {
            familyId = 0;
        }
        return $http.get(urlBase + "/HomeApi/GetdropdownGroupNameforFamily?catalogid=" + catalogid + "&groupType=" + groupType + "&familyId=" + familyId);
    };

    //new changes for family group id associate
    dataFactory.GetdropdownGroupName = function (catalogid, groupType, familyId) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        if (familyId === undefined || familyId === '' || familyId === "") {
            familyId = 0;
        }
        return $http.get(urlBase + "/HomeApi/GetdropdownGroupNameforFamilyAssociate?catalogid=" + catalogid + "&groupType=" + groupType + "&familyId=" + familyId);
    };




    dataFactory.GetAttributesByGroupId = function (catalogId, groupId, packageType) {

        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (groupId === undefined || groupId === '' || groupId === "") {
            groupId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetAttributesByGroupId?catalogId=' + catalogId + '&groupId=' + groupId + '&packageType=' + packageType);
    };


    dataFactory.GetAllAttributesForGrouping = function (catalogId, groupId, packageType) {

        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (groupId === undefined || groupId === '' || groupId === "") {
            groupId = 0;
        }

        return $http.get(urlBase + '/HomeApi/GetAllAttributesForGrouping?catalogId=' + catalogId + '&groupId=' + groupId + '&packageType=' + packageType);
    };


    //dataFactory.GetdropdownGroupName = function (catalogid, groupType) {
    //    if (catalogid === undefined || catalogid === '' || catalogid === "") {
    //        catalogid = 0;
    //    }
    //    return $http.get(urlBase + "/HomeApi/GetdropdownGroupName?catalogid=" + catalogid + "&groupType=" + groupType);
    //};

    dataFactory.SaveAttributesPackaging = function (request, catalogId, groupId, packageType) {
        var option = 'Save';
        return $http.post(urlBase + "/HomeApi/SaveandDeleteAttributesPackaging?catalogId=" + catalogId + "&groupId=" + groupId + "&packageType=" + packageType + "&option=" + option, JSON.stringify(request));
    };

    dataFactory.GetTempAttributes = function (catalogId, request) {
        return $http.post(urlBase + "/HomeApi/GetTempAttributes?catalogId=" + catalogId, JSON.stringify(request));
    };

    dataFactory.DeleteAttributesPackaging = function (request, catalogId, groupId, packageType) {
        //return $http.post(urlBase + "/HomeApi/DeleteAttributesPackaging?catalogId=" + catalogId + "&groupId=" + groupId + "&packageType=" + packageType, JSON.stringify(request));
        var option = 'Delete';
        return $http.post(urlBase + "/HomeApi/SaveandDeleteAttributesPackaging?catalogId=" + catalogId + "&groupId=" + groupId + "&packageType=" + packageType + "&option=" + option, JSON.stringify(request));
    };

    dataFactory.BtnAttributeMoveUpClick = function (selectatt, groupId) {
        return $http.post(urlBase + "/HomeApi/BtnAttributeMoveUpClick?attributeId=" + selectatt + "&groupId=" + groupId);
    };
    dataFactory.BtnAttributeMoveDownClick = function (selectatt, groupId) {
        return $http.post(urlBase + "/HomeApi/BtnAttributeMoveDownClick?attributeId=" + selectatt + "&groupId=" + groupId);
    };

    dataFactory.FamilyPackages = function (catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(baseUrl + '/Catalog/FamilyPackages?catalogid=' + catalogId);
    };

    dataFactory.ProductPackages = function (catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(baseUrl + '/Catalog/ProductPackages?catalogid=' + catalogId);
    };

    dataFactory.AssociateAttributePackaging = function (groupId, selectedId, catalogId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + "/HomeApi/AssociateAttributePackaging?selectedId=" + selectedId + '&groupId=' + groupId + '&catalogId=' + catalogId);
    };


    dataFactory.DeleteGroupdetails = function (catalogId, groupId) {
        return $http.post(urlBase + '/HomeApi/DeleteGroupdetails?catalogId=' + catalogId + '&groupId=' + groupId);
    };

    dataFactory.GetFamilyPacks = function (catalogId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/HomeApi/GetFamilyPacks?catalogId=' + catalogId, JSON.stringify(request));
    };

    dataFactory.GetProductPacks = function (catalogId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/HomeApi/GetProductPacks?catalogId=' + catalogId, JSON.stringify(request));
    };


    dataFactory.GetFamilyPacksforCategory = function (catalogId, categoryId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/CategoryApi/GetFamilyPacksforCategory?catalogId=' + catalogId + '&categoryId=' + categoryId, JSON.stringify(request));
    };

    dataFactory.GetProductPacksforCategory = function (catalogId, categoryId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/CategoryApi/GetProductPacksforCategory?catalogId=' + catalogId + '&categoryId=' + categoryId, JSON.stringify(request));
    };

    dataFactory.GetFamilylevelPacks = function (catalogId, familyId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (familyId === undefined || familyId === '' || familyId === "") {
            familyId = 0;
        }

        return $http.post(urlBase + '/FamilyApi/GetFamilylevelPacks?catalogId=' + catalogId + '&familyId=' + familyId, JSON.stringify(request));
    };

    dataFactory.GetProductlevelPacks = function (catalogId, familyId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (familyId === undefined || familyId === '' || familyId === "") {
            familyId = 0;
        }

        return $http.post(urlBase + '/FamilyApi/GetProductlevelPacks?catalogId=' + catalogId + '&familyId=' + familyId, JSON.stringify(request));
    };


    dataFactory.saveProdAttributePackforfamily = function (catalogId, familyId, CheckBoxValues) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/saveProdAttributePackforfamily?catalogId=' + catalogId + '&family_id=' + familyId, JSON.stringify(CheckBoxValues));
    };

    dataFactory.saveFamilyAttributePackforfamily = function (catalogId, familyId, CheckBoxValues) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/saveFamilyAttributePackforfamily?catalogId=' + catalogId + '&family_id=' + familyId, JSON.stringify(CheckBoxValues));
    };

    dataFactory.SaveProductAttributePackforCatalog = function (catalogId, CheckBoxValues) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/SaveProductAttributePackforCatalog?catalogId=' + catalogId, JSON.stringify(CheckBoxValues));
    };
    dataFactory.SaveFamilyAttributePackforCatalog = function (catalogId, CheckBoxValues) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/SaveFamilyAttributePackforCatalog?catalogId=' + catalogId, JSON.stringify(CheckBoxValues));
    };
    dataFactory.saveProdAttributePackforcategory = function (catalogId, categoryId, CheckBoxValues) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/HomeApi/saveProdAttributePackforcategory?catalogId=' + catalogId + '&category_Id=' + categoryId, JSON.stringify(CheckBoxValues));
    };
    dataFactory.saveFamilyAttributePackforcategory = function (catalogId, categoryId, CheckBoxValues) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/HomeApi/saveFamilyAttributePackforcategory?catalogId=' + catalogId + '&category_Id=' + categoryId, JSON.stringify(CheckBoxValues));
    };

    dataFactory.SaveProductAttributePack = function (catalogId, familyId, CheckBoxValues) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        if (familyId === undefined || familyId === '' || familyId === "") {
            familyId = 0;
        }
        return $http.post(urlBase + '/HomeApi/saveProdAttributePackforfamily?catalogId=' + catalogId + '&family_id=' + familyId, JSON.stringify(CheckBoxValues));
    };

    // Attribute Grouping End

    //CategorySpecGrid
    dataFactory.savenewcategoryattribute = function (request, catalogId) {
        return $http.post(urlBase + '/CategoryApi/savenewcategoryattribute?catalogId=' + catalogId, JSON.stringify(request));
    };

    dataFactory.GetAllCatalogCategoryAttributes = function (catalogId, categoryId) {
        return $http.get(urlBase + '/CategoryApi/GetAllCatalogCategoryAttributes?catalogId=' + catalogId + '&categoryId=' + categoryId);
    };


    dataFactory.SaveCategoryPublishAttributes = function (request, categoryId, catalogId) {
        return $http.post(urlBase + "/CategoryApi/SaveCategoryPublishAttributes?categoryId=" + categoryId + '&catalogId=' + catalogId, JSON.stringify(request));
    };


    dataFactory.GetAllCategoryAttributes = function (catalogId, categoryId) {
        return $http.get(urlBase + '/CategoryApi/GetPublishedCategoryAttributes?catalogId=' + catalogId + '&categoryId=' + categoryId);
    };


    dataFactory.DeleteCategoryPublishAttributes = function (request, categoryId) {
        return $http.post(urlBase + "/CategoryApi/DeleteCategoryPublishAttributes?categoryId=" + categoryId, JSON.stringify(request));
    };

    dataFactory.BtnCategoryMoveUpClick = function (sortOrder, request, categoryId) {
        return $http.post(urlBase + "/CategoryApi/BtnCategoryMoveUpClick?sortOrder=" + sortOrder + "&categoryId=" + categoryId, JSON.stringify(request));
    };
    dataFactory.BtnCategoryMoveDownClick = function (sortOrder, request, categoryId) {
        return $http.post(urlBase + "/CategoryApi/BtnCategoryMoveDownClick?sortOrder=" + sortOrder + "&categoryId=" + categoryId, JSON.stringify(request));
    };

    //dataFactory.UnPublishCategoryAttributes = function (request, categoryId) {
    //    return $http.post(urlBase + "/CategoryApi/UnPublishCategoryAttributes?categoryId=" + categoryId, JSON.stringify(request));
    //};

    dataFactory.UnPublishCategoryAttributes = function (request, categoryId, selecetedCatalogId, request1) {

        var data = [];
        data.push(request);
        data.push(request1);

        return $http.post(urlBase + "/CategoryApi/UnPublishCategoryAttributes?categoryId=" + categoryId + "&selecetedCatalogId=" + selecetedCatalogId, JSON.stringify(data));
    };

    dataFactory.getCategoryspecification = function (catalogId, categoryId) {

        return $http.get(urlBase + '/CategoryApi/getCategoryspecification?catalogid=' + catalogId + '&CategoryId=' + categoryId);
    };
    dataFactory.GetCategoryAttributeDetails = function (catalogId, categoryid) {
        return $http.get(urlBase + "/CategoryApi/GetCategoryAttributeDetails?catalogId=" + catalogId + "&categoryid=" + categoryid);
    };

    dataFactory.SaveCategorySpecs = function (catalogId, Category_id, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }

        return $http.post(urlBase + '/CategoryApi/SaveCategorySpecs?catalogId=' + catalogId + '&Category_id=' + Category_id, JSON.stringify(request));
    };

    //End


    //Indesign Export and Import Start

    dataFactory.InDesigExport = function (catalogId, displayIdColumns, exportFormatType, projectId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (projectId === undefined || projectId === '' || projectId === "") {
            projectId = 0;
        }
        return $http.get(urlBase + '/InDesignApi/InDesigExport?catalogId=' + catalogId + '&displayIdColumns=' + displayIdColumns + '&exportFormatType=' + exportFormatType + '&projectId=' + projectId);
    };

    // Indesign Export and Import End
    // To Get PdfExpress_Values


    dataFactory.setPdfXpressType = function (Type, Category_id, Family_Name, Product_id, Catalog_ID) {
        return $http.get(urlBase + '/CategoryApi/setPdfXpressType?Type=' + Type + '&Category_id=' + Category_id + '&Family_Name=' + Family_Name + '&Product_id=' + Product_id + '&Catalog_ID=' + Catalog_ID);
    }

    dataFactory.CheckTemplatePath = function (Type, Category_id, Catalog_id) {
        return $http.post(urlBase + '/HomeApi/CheckTemplatePath?Type=' + Type + '&Category_id=' + Category_id + '&Catalog_id=' + Catalog_id );
    }

    dataFactory.CheckTemplatePath_ProductItem = function (Type, Category_id, Catalog_id) {
        return $http.post(urlBase + '/HomeApi/CheckTemplatePath_ProductItem?Type=' + Type + '&Category_id=' + Category_id + '&Catalog_id=' + Catalog_id);
    }
    //PickList Import

    dataFactory.validatePickListImport = function (importExcelSheetDDSelectionValue, excelPath) {

        return $http.post(urlBase + '/PickListImportApi/validatePickListImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath);
    }
    dataFactory.pickListImport = function (importExcelSheetDDSelectionValue, excelPath) {

        return $http.post(urlBase + '/PickListImportApi/pickListImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath);
    }
    dataFactory.pickListViewLog = function () {
        return $http.post(urlBase + '/PickListImportApi/PickListViewLog');
    }

    dataFactory.validationresults = function (Errorlogoutputformat, fileName) {

        return $http.post(urlBase + '/PickListImportApi/importlogs?Errorlogoutputformat=' + Errorlogoutputformat + '&fileName=' + fileName);
    };


    //User Export
    dataFactory.getUserExportDetails = function (exportTypeValues) {

        return $http.post(urlBase + '/UserAdminApi/GetUserExportDetails?exportTypeValues=' + exportTypeValues);
    }

    //User Import
    dataFactory.validateUserImport = function (importExcelSheetDDSelectionValue, excelPath, importType, importFormat) {

        return $http.post(urlBase + '/ImportApi/ValidateUserImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath + '&importType=' + importType + '&importFormat=' + importFormat);
    }

    dataFactory.userImport = function (importExcelSheetDDSelectionValue, excelPath, importType, importFormat) {

        return $http.post(urlBase + '/ImportApi/UserImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath + '&importType=' + importType + '&importFormat=' + importFormat);
    }

    dataFactory.userViewLog = function () {
        return $http.post(urlBase + '/ImportApi/UserViewLog');
    }

    dataFactory.checkUser = function () {

        return $http.post(urlBase + '/ImportApi/CheckUser');
    }

    //User schedule Import
    dataFactory.userScheduleImport = function (importExcelSheetDDSelectionValue, excelPath, ImportType, ImportFormat, SelectedFileForUploadnamemain, scheduleDate) {

        return $http.post(urlBase + '/ImportApi/UserScheduleImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath + '&ImportType=' + ImportType + '&ImportFormat=' + ImportFormat + '&SelectedFileForUploadnamemain=' + SelectedFileForUploadnamemain + '&scheduleDate=' + scheduleDate);
    }


    //FTP Export
    dataFactory.DownloadExportSheet = function (FileName, EnableSubProduct, Url, Username, Passwordencrpt, templateName) {

        return $http.post(urlBase + '/ExportApi/DownloadExportSheet?FileName=' + FileName + '&EnableSubProduct=' + EnableSubProduct + '&Url=' + Url + '&Username=' + Username + '&Passwordencrpt=' + Passwordencrpt + '&templateName=' + templateName);
    }

    dataFactory.btnValidate = function (url, username, password) {

        return $http.post(urlBase + '/HomeApi/FTPValidate?url=' + url + '&username=' + username + '&password=' + password);
    }

    //-------------------------------------------------PDF Index function call starts----------------------------------------------------------//
    dataFactory.addKeyword = function (Keyword, group) {
        return $http.post(urlBase + "/PDFIndexApi/addKeywordData?keyword=" + Keyword + "&group=" + group);
    };

    dataFactory.getKeywordList = function (groupType) {
        return $http.post(urlBase + "/PDFIndexApi/getKeywordsList?groupType=" + groupType)
    };

    dataFactory.getListValue = function () {
        return $http.post(urlBase + "/PDFIndexApi/GetKeywordsListDrpdwm")
    };

    dataFactory.addKeywordGroup = function (KeywordGroup) {
        return $http.post(urlBase + "/PDFIndexApi/AddKeywordGroup?keywordGroup=" + KeywordGroup);
    };

    dataFactory.getContentTemplateDetails = function () {
        return $http.post(urlBase + "/PDFIndexApi/getContentTempList")
    };

    dataFactory.saveTemplateDetail = function (templatename) {
        return $http.post(urlBase + "/PDFIndexApi/saveTemplateDetail?templateName=" + templatename)
    };

    dataFactory.sourceContentRead = function (templateName, path) {
        return $http.post(urlBase + "/PDFIndexApi/readSourceData?templateName=" + templateName + "&filePath=" + path);
    };

    dataFactory.getContentData = function (templateName) {
        return $http.post(urlBase + "/PDFIndexApi/getContentGridData?templatename=" + templateName);
    };

    dataFactory.getIndexGenTemplateDetails = function () {
        return $http.post(urlBase + "/PDFIndexApi/GetIndexTempList")
    };

    dataFactory.addIndexgenTempDetails = function (templateName, contentLibrary, keywordList, layout) {
        return $http.post(urlBase + "/PDFIndexApi/AddIndexTempDetails?templatename=" + templateName + "&contentLibrary=" + contentLibrary + "&keywordList=" + keywordList + "&layout=" + layout);
    };

    dataFactory.indexGenerationProcess = function (templateName, layout) {
        return $http.post(urlBase + "/PDFIndexApi/indexGenerationProcess?templateName=" + templateName + "&layout=" + layout);
    };

    dataFactory.indexGenPageData = function (templateName) {
        return $http.post(urlBase + "/PDFIndexApi/GetIndexGenPageData?templateName=" + templateName);
    };

    dataFactory.mergeProcess = function (layout, filePath, insertType, dataGrid) {
        //return $http.post(urlBase + "/PDFIndexApi/pdfMergeProcess?indexFile=" + indexFile + "&sourceFile=" + sourceFile + "&insertType=" + insertType);
        return $http.post(urlBase + "/PDFIndexApi/generatePDFIndex?layout=" + layout + "&filePath=" + filePath + "&insertType=" + insertType + "&dataTable=", JSON.stringify(dataGrid));
    };

    dataFactory.getExemptedwordsList = function () {
        return $http.post(urlBase + "/PDFIndexApi/getExemptedWords");
    };

    dataFactory.addExemptedword = function (keyword) {
        return $http.post(urlBase + "/PDFIndexApi/saveExemptedWord?word=" + keyword);
    };

    dataFactory.exemptedDelete = function (deleteId) {
        return $http.post(urlBase + "/PDFIndexApi/deleteExemptedWord?deleteId=" + deleteId);
    };

    dataFactory.keywordDeleteClick = function (deleteId) {
        return $http.post(urlBase + "/PDFIndexApi/deleteKeyword?deleteId=" + deleteId);
    };

    dataFactory.updateKeywords = function (modifiedList) {
        return $http.post(urlBase + "/PDFIndexApi/updateKeywords?modifiedData=", JSON.stringify(modifiedList));
    };

    dataFactory.sourcePDFmergeProcess = function (selectedArray) {
        return $http.post(urlBase + "/PDFIndexApi/mergeSourcePDF?selectedValues=", JSON.stringify(selectedArray));
    };

    dataFactory.deletePDFIndexValues = function (templateName) {
        return $http.post(urlBase + "/PDFIndexApi/DeletePDFIndexValues?templateName=" + templateName);
    };
    //-------------------------------------------------PDF Index function call ends----------------------------------------------------------//
    //Role Import and Export
    dataFactory.getRoleExport = function () {

        return $http.post(urlBase + '/UserAdminApi/GetRoleExport');
    }

    dataFactory.rolefinishImport = function (importExcelSheetDDSelectionValue, excelPath, ImportType, ImportFormat) {

        return $http.post(urlBase + '/ImportApi/RoleImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath + '&ImportType=' + ImportType + '&ImportFormat=' + ImportFormat);
    }

    dataFactory.validateRoleImport = function (importExcelSheetDDSelectionValue, excelPath, ImportType, ImportFormat) {

        return $http.post(urlBase + '/ImportApi/ValidateRoleImport?importExcelSheetDDSelectionValue=' + importExcelSheetDDSelectionValue + '&excelPath=' + excelPath + '&ImportType=' + ImportType + '&ImportFormat=' + ImportFormat);
    }

    dataFactory.roleViewLog = function () {
        return $http.post(urlBase + '/ImportApi/RoleViewLog');
    }

    dataFactory.changeRoleViewLog = function (value) {

        return $http.post(urlBase + '/ImportApi/ChangeRoleViewLog?value=' + value);
    }

    dataFactory.AttributeSheetValidation = function (excelPath, importtype) {

        return $http.post(urlBase + '/AttributeApi/SheetValidation?excelPath=' + excelPath + '&importtype=' + importtype);
    }
    dataFactory.InvertedProductSheetValidation = function (excelPath, importtype) {

        return $http.post(urlBase + '/InvertedProduct/SheetValidationForProducts?excelPath=' + excelPath + '&importtype=' + importtype);
    }

    dataFactory.SheetValidation = function (excelPath, importtype) {

        return $http.post(urlBase + '/ImportApi/SheetValidation?excelPath=' + excelPath + '&importtype=' + importtype);
    }

    dataFactory.ExportInvertedProducts = function (catalogId, opt, displayIdcolumns, exportFormatType, selectedTemplateId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/InvertedProduct/ExportInvertedProducts?catalogId=' + catalogId + '&opt=' + opt + '&displayIdcolumns=' + displayIdcolumns + '&exportFormatType=' + exportFormatType + '&selectedTemplateId=' + selectedTemplateId);
    };

    dataFactory.ExportAttributesList = function (catalogId, displayIdcolumns, attributesExportFormatType) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/AttributeApi/ExportAttributesList?catalogId=' + catalogId + '&displayIdColumns=' + displayIdcolumns + '&attributesExportFormatType=' + attributesExportFormatType);
    };


    //  // Pdf Xpress Start 
    dataFactory.setPdfXpressType = function (Type, Category_id, Family_Name, Product_Id, Catalog_ID) {


        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var Category_id = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(Category_id), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });


        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        var Family_Name = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(Family_Name), key, {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        return $http.get(urlBase + '/CategoryApi/setPdfXpressType?Type=' + Type + '&Category_id=' + Category_id + '&Family_Name=' + Family_Name + '&Product_Id=' + Product_Id + '&Catalog_ID=' + Catalog_ID);
    }



    dataFactory.getPdfXpressdefaultType = function (TYPE, Id, CatalogID) {
        return $http.post(urlBase + '/HomeApi/GetPdfXpressdefaultType?TYPE=' + TYPE + '&Id=' + Id + '&CatalogID=' + CatalogID);
    }
    //JO
    dataFactory.getPdfXpressdefaultType_Category = function (TYPE, Id) {
        return $http.post(urlBase + '/HomeApi/GetPdfXpressdefaultType_Catgeory?TYPE=' + TYPE + '&Id=' + Id);
    }
    
    dataFactory.GetPdfXpressdefaultType_Family = function (TYPE, Id, CatalogID) {
        return $http.post(urlBase + '/HomeApi/GetPdfXpressdefaultType_Family?TYPE=' + TYPE + '&Id=' + Id + '&CatalogID=' + CatalogID);
    }
    dataFactory.getPdfXpressdefaultTemplate = function (Cat_Id) {
        return $http.post(urlBase + '/HomeApi/getPdfXpressdefaultTemplate?Cat_Id=' + Cat_Id );
    }
    dataFactory.getPdfXpressdefaultTemplate_Category = function (Cat_Id, Category_Id) {
        return $http.post(urlBase + '/HomeApi/getPdfXpressdefaultTemplate_Category?Cat_Id=' + Cat_Id + '&Category_Id=' + Category_Id);
    }

    dataFactory.getPdfXpressdefaultTemplate_Family = function (TYPE, id, CatalogID) {
        return $http.post(urlBase + '/HomeApi/getPdfXpressdefaultTemplate_Family?TYPE=' + TYPE + '&id=' + id + '&CatalogID=' + CatalogID );
    }

    dataFactory.getPdfXpressdefaultTemplate_Product = function (TYPE, id, CatalogID) {
            return $http.post(urlBase + '/HomeApi/getPdfXpressdefaultTemplate_Product?TYPE=' + TYPE + '&id=' + id + '&CatalogID=' + CatalogID);
    }

    dataFactory.clearPdfXpressType = function (TYPE, id, CatalogID) {
        return $http.post(urlBase + '/HomeApi/ClearPdfXpressdefaultType?TYPE=' + TYPE + '&id=' + id + '&CatalogID=' + CatalogID);
    }
    dataFactory.clearPdfXpressType_Family = function (TYPE, id) {
        return $http.post(urlBase + '/HomeApi/ClearPdfXpressdefaultType_Family?TYPE=' + TYPE + '&id=' + id );
    }

    dataFactory.getRootCategoryPdfXpress = function (Catalog_Id, Category_ID) {
        return $http.post(urlBase + '/HomeApi/GetRootCategoryPdfXpress?Catalog_Id=' + Catalog_Id + '&Category_ID=' + Category_ID);
    }

    dataFactory.deletedefaultTemplate = function (Cat_Id) {
        return $http.post(urlBase + '/HomeApi/DeletedefaultTemplate?Cat_Id=' + Cat_Id);
    }

    // Pdf Xpress End

    dataFactory.TableDesignerExport = function (familyId, catalogId, categoryId) {

        return $http.post(urlBase + '/HomeApi/TableDesignerExport?familyId=' + familyId + '&catalogId=' + catalogId + '&categoryId=' + categoryId);

    }

    dataFactory.FinishTableDesignerImport = function (sheetName, excelSheetPath) {

        return $http.post(urlBase + '/ImportApi/FinishTableDesignerImport?excelSheetPath=' + excelSheetPath + '&sheetName=' + sheetName);
    }

    dataFactory.FamilyLevelTableDesignerViewLog = function (tableName) {

        return $http.post(urlBase + '/ImportApi/FamilyLevelTableDesignerImportViewLog?tableName=' + tableName);
    }

    dataFactory.GetCatalogCategoryattrlistDetails = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + '/HomeApi/GetCatalogCategoryattrlistDetails?catalogid=' + catalogid);
    };

    dataFactory.exportIndex = function (selectedTemplateNmae, exportFormatType) {

        return $http.post(urlBase + "/PDFIndexApi/ExportKeywordsList?selectedTemplateNmae=" + selectedTemplateNmae + '&exportFormatType=' + exportFormatType);
    }
    dataFactory.ExportExempte = function (exportFormatType) {

        return $http.post(urlBase + "/PDFIndexApi/exceptwords?exportFormatType=" + exportFormatType);
    }
    //Index PDF import validation process
    dataFactory.ValidateProcess = function (excelPath, sheetName) {
        return $http.post(baseUrl + "/PDFIndex/ValidateProcess?excelPath=" + excelPath + '&sheetName=' + sheetName);
    }
    dataFactory.ValidateExemptsheet = function (excelPath) {
        return $http.post(baseUrl + "/PDFIndex/ValidateExemptsheet?excelPath=" + excelPath);
    }

    dataFactory.indexImportPDFKeywords = function (excelPath, sheetName) {
        return $http.post(baseUrl + "/PDFIndex/ImportIndexPdf?excelPath=" + excelPath + '&sheetName=' + sheetName);
    }

    dataFactory.viewByHideProducts = function (selecetedCatalogId, selecetedFamilyId, CategoryID) {

        return $http.post(urlBase + "/HomeApi/ViewByHideProducts?selecetedCatalogId=" + selecetedCatalogId + '&selecetedFamilyId=' + selecetedFamilyId + '&CategoryID=' + CategoryID);
    }

    // mv   getAttributeDetailsbyType
    dataFactory.getAttributeDetailsbyType = function (value) {
        return $http.post(urlBase + '/HomeApi/getAttributeDetailsbyType?Type=' + value);
    }

    dataFactory.ExemptViewLog = function () {
        return $http.post(baseUrl + "/PDFIndex/ExemptViewLog");
    }

    dataFactory.IndexpdfvalidationViewLog = function () {
        return $http.post(baseUrl + "/PDFIndex/IndexpdfvalidationViewLog");
    }

    dataFactory.updateMulipleAttributeProducts = function (MulipleAttributename, UserUpadtedAttributeValue, AttributeTypeVlauesBySelect, product_ids, family_ID, Category_ID, Catalog_ID) {

        return $http.post(urlBase + "/HomeApi/updateMulipleAttributeProducts?MulipleAttributename=" + MulipleAttributename +
            '&UserUpadtedAttributeValue=' + UserUpadtedAttributeValue +
            '&AttributeTypeVlauesBySelect=' + AttributeTypeVlauesBySelect +
            '&product_ids=' + product_ids +
            '&family_ID=' + family_ID + '&Category_ID=' + Category_ID + '&Catalog_ID=' + Catalog_ID
            );
    }


    // ------------------------------- Backup and Restore -----------------------------------

    dataFactory.CreateDataBase = function (catalogId, databaseName, backUpPath, SeletedBackupItems, request) {
        return $http.post(urlBase + '/BackUpRestore/NewDatabseBackupFile?catalogId=' + catalogId + '&databaseName=' + databaseName + '&pathName=' + backUpPath + '&seletedBackupItems=' + SeletedBackupItems, JSON.stringify(request));
    }
    dataFactory.GetCustomerCatalog = function (CatalogId, CustomerId) {
        if (CatalogId === undefined || CatalogId === '' || CatalogId === "") {
            CatalogId = 0;
        }
        if (CustomerId === undefined || CustomerId === '' || CustomerId === "") {
            CustomerId = 0;
        }
        return $http.post(urlBase + '/BackUpRestore/GetCustomerCatalog?CatalogId=' + CatalogId + '&CustomerID=' + CustomerId);
    }

    dataFactory.GetAllTemplateDetails = function (catalogid) {
        if (catalogid === undefined || catalogid === '' || catalogid === "") {
            catalogid = 0;
        }
        return $http.get(urlBase + "/BackUpRestore/GetAllTemplateDetails?catalogId=" + catalogid);
    };

    dataFactory.GetTemplateDetails = function (templateId) {
        if (templateId === undefined || templateId === '' || templateId === "") {
            templateId = 0;
        }
        return $http.get(urlBase + "/BackUpRestore/GetTemplateDetails?templateId=" + templateId);
    };

    dataFactory.CreateDataBaseFile = function (catalogId, databaseName, backUpPath, SeletedBackupItems, request) {
        return $http.post(urlBase + '/BackUpRestore/NewDatabseBackupforFiles?catalogId=' + catalogId + '&databaseName=' + databaseName + '&pathName=' + backUpPath + '&seletedBackupItems=' + SeletedBackupItems, JSON.stringify(request));
    }

    dataFactory.Restore = function (catalogId, restoreAs) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(baseUrl + '/Catalog/Restore?catalogId=' + catalogId + '&restoreAs=' + restoreAs);
    };

    dataFactory.CategoriesForExport = function (catalogId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/HomeApi/CategoriesForExport?categoryId=' + categoryId + '&catalogid=' + catalogId);
    };

    // --------------------------------- Backup and Restore -----------------------------------
    dataFactory.UndoUpdateFamily = function (catalogId, Category_id, request) {
        return $http.post(urlBase + '/FamilyApi/UndoUpdateFamily?catalogId=' + catalogId + '&Category_id=' + Category_id, JSON.stringify(request));
    };

    dataFactory.GetServerFilePath = function () {
        return $http.get(urlBase + '/SeparatePathApi/GetServerFilePath');
    };

    dataFactory.getNewFamilyID = function (findFamilyId, catalogID, categoryID) {
        return $http.post(urlBase + '/CategoryApi/FindFamilyIDValue?findFamilyId=' + findFamilyId + '&catalogID=' + catalogID + '&categoryID=' + categoryID);
    };
    // --------------------------------- Backup and Restore -----------------------------------
    dataFactory.undoFamilyDataSave = function (undoData) {
        return $http.post(urlBase + '/HomeApi/UndoFamilyDataSave', JSON.stringify(undoData));
    }


    //---------------------------------Tabledesigner Multiple selection families Start - Aswin kumar------------------------------------------

    dataFactory.getTabledesignerAllCategories = function (catalogId, categoryId, workingCatalogId) {
        return $http.get(urlBase + '/TableDesignerApi/TabledesignerAllCategories?categoryId=' + categoryId + '&catalogid=' + catalogId + '&workingCatalogId=' + workingCatalogId);
    };

    dataFactory.multipleTableDesignerSelectionFamilyIds = function (familyId, structureName, catalogId, IsdefaultStructurename, IsUpdateMasterCatalog, data) {
        return $http.post(urlBase + '/TableDesignerApi/multipleTableDesignerSelectionFamilyIds?familyId=' + familyId + '&structureName=' + structureName + '&catalogId=' + catalogId + '&IsdefaultStructurename=' + IsdefaultStructurename + '&IsUpdateMasterCatalog=' + IsUpdateMasterCatalog, JSON.stringify(data));
    };

    //---------------------------------Tabledesigner Multiple selection families End - Aswin kumar-------------------------------------------

    //---------------------------------Indesign  Attribute Wise Filter Start - Aswin kumar------------------------------------------------------------------

    dataFactory.GetIndesignAllFiltersValue = function (attributeName) {
        return $http.get(urlBase + '/InDesignApi/GetIndesignAllFiltersValue?attributeName=' + encodeURIComponent(attributeName));
    };


    dataFactory.getProductAttributeType = function () {
        return $http.get(urlBase + '/InDesignApi/getProductAttributeType');
    };

    dataFactory.getIndesignProductAttributeNames = function (productAttributeType) {
        return $http.post(urlBase + '/InDesignApi/getIndesignProductAttributeNames?productAttributeType=' + productAttributeType);
    };


    //-----------------------------Indesign Attribute Wise Filter End - Aswin kumar------------------------------------------------------------------------

    //-----------------------------------------New attribute design changes in category page--------------------//
    dataFactory.GetAllCategoryAttributesNew = function (catalogId, categoryId) {
        return $http.post(urlBase + '/CategoryApi/GetAllCategoryAttributesNew?catalogId=' + catalogId + '&categoryId=' + categoryId);
    }
    //-----------------------------------------New attribute design changes in category page--------------------//

    //-----------Start------------------Inverted Products Filter Option - Kowshikkumar ------------------------------------------------------------------------

    dataFactory.categorySearchFilterTree = function (SelectedCatalogId, options, SelectedCatalogId) {
        return $http.post(urlBase + '/InvertedProduct/CategorySearchFilterTree?catalogId=' + SelectedCatalogId + '&categoryId=' + options + '&workingCatalogId=' + SelectedCatalogId);
    }


    dataFactory.GetInvertedModifiedProductsCount = function () {
        return $http.post(urlBase + '/InvertedProduct/GetInvertedModifiedProductsCount');
    };

    //-----------End------------------Inverted Products Filter Option - Kowshikkumar ------------------------------------------------------------------------


    //--------------------------------START----Hirerarchical template changes by Mohan---------------------------------------------------------//
    dataFactory.CategoriesForPDFExpress = function (catalogId, categoryId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.get(urlBase + '/HomeApi/CategoriesForPDFExpress?categoryId=' + categoryId + '&catalogid=' + catalogId);
    };

    dataFactory.getCategoryId = function (categoryId) {
        return $http.get(urlBase + '/XpressCatalogApi/getCategoryId?categoryId=' + categoryId);
    };
    //--------------------------------START----Hirerarchical template changes by Mohan---------------------------------------------------------//


    dataFactory.getAttributeListUnderCatalog = function (catalogId, selectTemplateId) {
        return $http.post(urlBase + '/InvertedProduct/GetAllAttributesUnderCatalaog?catalogId=' + catalogId + '&selectTemplateId=' + selectTemplateId);
    };

    dataFactory.getHierarchyDetailsUnderInvertedProducts = function (catalogId, hierarchyData) {
        return $http.post(urlBase + '/InvertedProduct/GetHierarchyDetailsUnderInvertedProducts?catalogId=' + catalogId, JSON.stringify(hierarchyData));
    };

    dataFactory.getSelectedInvertedProductAttributes = function (catalogId, selectTemplateId) {
        return $http.post(urlBase + '/InvertedProduct/GetInvertedProductsTemplateDetails?catalogId=' + catalogId + '&selectTemplateId=' + selectTemplateId);
    };


    dataFactory.saveSelectedInvertedProductAttributes = function (catalogId, selectTemplateId, templateName, selectedInvertedProductAttributes) {
        return $http.post(urlBase + '/InvertedProduct/SaveSelectedInvertedProductAttributes?catalogId=' + catalogId + '&selectTemplateId=' + selectTemplateId + '&templateName=' + templateName, JSON.stringify(selectedInvertedProductAttributes));
    };

    dataFactory.deleteInvertedProductPublishAttributes = function (catalogId, selectTemplateId, removeInvertedProductAttributes) {
        return $http.post(urlBase + '/InvertedProduct/DeleteInvertedProductPublishAttributes?catalogId=' + catalogId + '&selectTemplateId=' + selectTemplateId, JSON.stringify(removeInvertedProductAttributes));
    };

    dataFactory.invertedProductsTemplateList = function (catalogId) {
        return $http.post(urlBase + '/InvertedProduct/InvertedProductsTemplateList?catalogId=' + catalogId);
    };

    dataFactory.saveInvertedProductsDetails = function (catalogId, templateName) {
        return $http.post(urlBase + '/InvertedProduct/SaveInvertedProductsDetails?catalogId=' + catalogId + '&templateName=' + templateName);
    };

    dataFactory.deleteSelectedTemplateName = function (catalogId, selectedTemplateID) {
        return $http.post(urlBase + '/InvertedProduct/DeleteSelectedTemplateName?catalogId=' + catalogId + '&selectedTemplateID=' + selectedTemplateID);
    };

    dataFactory.GetAllFamilyAttributesNew = function (catalogId, familyid) {
        return $http.post(urlBase + '/FamilyApi/GetAllFamilyAttributesNew?catalogId=' + catalogId + '&familyid=' + familyid);
    }

    dataFactory.GetAllProdFamilyAttributes = function (catalogid, familyId, categoryId) {
        return $http.get(urlBase + "/HomeApi/GetAllProdFamilyAttributes?catalogId=" + catalogid + "&familyIdString=" + familyId + '&categoryId=' + categoryId);
    };

    dataFactory.GetAllProdFamilyAttributes = function (catalogid, familyId, categoryId) {
        return $http.get(urlBase + "/HomeApi/GetAllProdFamilyAttributes?catalogId=" + catalogid + "&familyIdString=" + familyId + '&categoryId=' + categoryId);
    };
    dataFactory.SaveCategoryPublishSortAttributes = function (categoryId, catalogId, model) {
        return $http.post(urlBase + "/CategoryApi/SaveCategoryPublishSortAttributes?categoryId=" + categoryId + "&catalogId=" + catalogId, JSON.stringify(model));
    };


    dataFactory.GetSavedTemplate = function (templateTextBoxValue) {
        return $http.post(urlBase + '/InvertedProduct/GetSavedTemplate?templateTextBoxValue=' + templateTextBoxValue);
    };

    dataFactory.GetApplicationSettingDetails = function (customerId) {
        return $http.get(urlBase + '/UserAdminApi/GetApplicationSettingDetails?customerId=' + customerId);
    };


    //  Picklist Export   - Start

    dataFactory.getPicklistExportData = function (SelectedPickListDataType, CatalogId, DisplayIdcolumns) {
        return $http.post(urlBase + '/HomeApi/GetPicklistExportData?SelectedPickListDataType=' + SelectedPickListDataType + '&CatalogId=' + CatalogId + '&DisplayIdcolumns=' + DisplayIdcolumns);
    };


    // Picklist Export - End

    dataFactory.GetConverttypeDetails = function (customerId) {
        //if (customerId === undefined) {
        //    customerId = 0;
        //}
        return $http.get(urlBase + '/HomeApi/GetConverttypeDetailsForCurrentCustomer?customerId=' + customerId); //GetCatalogDetails
    };

    //---------------------------------------------------------------------------------------------------------------------------------------------------
    dataFactory.CategoriesForIndesignfilter = function (catalogId, categoryId, recordId) {
        debugger
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (recordId === undefined || recordId === '' || recordId === "") {
            recordId = 0;
        }
        return $http.get(urlBase + '/HomeApi/CategoriesForIndesignfilter?catalogId=' + catalogId + '&categoryId=' + categoryId + '&recordId=' + recordId);
    };
    dataFactory.GetIndesignFilterCategoryProjectValuesfilter = function (recordId, catalogId, request) {
        debugger
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (recordId === undefined || recordId === '' || recordId === "") {
            recordId = 0;
        }
        return $http.post(urlBase + '/HomeApi/GetIndesignFilterCategoryProjectValuesfilter?recordId=' + recordId + '&catalogId=' + catalogId, JSON.stringify(request));
    };
    dataFactory.ClearAllFilter = function () {

        return $http.post(urlBase + '/HomeApi/ClearAllFilter');
    };
    dataFactory.getNullvalueAttributes = function (selectedFamilyId, pageno, ProdCountPerPage) {
        return $http.post(urlBase + '/ReportsApi/GetNullvalueAttributes?selectedFamilyId=' + selectedFamilyId + "&pageno=" + pageno + "&ProdCountPerPage=" + ProdCountPerPage)
    };
    //dataFactory.getNullvalueAttributesForSearch = function (selectedFamilyId, pageno, ProdCountPerPage) {
    //    return $http.post(urlBase + '/ReportsApi/GetNullvalueAttributesForSearch?selectedFamilyId=' + selectedFamilyId + "&pageno=" + pageno + "&ProdCountPerPage=" + ProdCountPerPage)
    //};
    dataFactory.getNullvalueAttributesForSearch = function (selectedFamilyId, getProduct_id, SelectedCatalogId) {
        return $http.post(urlBase + '/ReportsApi/GetNullvalueAttributesForSearch?selectedFamilyId=' + selectedFamilyId + "&getProduct_id=" + getProduct_id + "&SelectedCatalogId=" + SelectedCatalogId)
    };
    dataFactory.updateValueForAttr = function (request, category_id, selectedFamilyId, getSelectedRowProd_id, Parentproduct_Id, blurCheck, SaveProductSpecs) {
        var data = [];
        data.push(request);
        data.push(SaveProductSpecs);

        return $http.post(urlBase + '/FamilyApi/SaveProductSpecs?category_id=' + category_id + '&selectedFamilyId=' + selectedFamilyId + '&getSelectedRowProd_id=' + getSelectedRowProd_id + '&Parentproduct_Id=' + Parentproduct_Id + '&blurCheck=' + blurCheck, JSON.stringify(data));
    };

    dataFactory.nullValueAttrCount = function (selectedFamilyId) {
        return $http.post(urlBase + '/ReportsApi/NullValueAttrCount?selectedFamilyId=' + selectedFamilyId)
    };
    dataFactory.prodCountForTree = function (catalogId, categoryId, workingCatalogId) {

        return $http.get(urlBase + '/HomeApi/DataCompletenessCountForTree?catalogId=' + catalogId + '&categoryId=' + categoryId + '&workingCatalogId=' + workingCatalogId);
    };

    //----------------------------WORKFLOW-----------------------------------------------------------------------------------
    dataFactory.saveworkflow = function (workflowstatus, workflowdescription) {

        return $http.post(urlBase + '/UserAdminApi/saveworkflow?workflowstatus=' + workflowstatus + '&workflowdescription=' + workflowdescription);
    };

    dataFactory.Saveworkflowsettings = function (Selectedrole, Selectedfromstatus, SelectedTostatus) {

        return $http.post(urlBase + '/UserAdminApi/Saveworkflowsettings?Selectedrole=' + Selectedrole + '&Selectedfromstatus=' + Selectedfromstatus + '&SelectedTostatus=' + SelectedTostatus);
    };

    dataFactory.GetAllWorkFlowsettingsDataSourceValue = function (request) {

        return $http.post(urlBase + '/UserAdminApi/GetAllWorkFlowsettingsDataSourceValue', JSON.stringify(request));
    };


    dataFactory.getWorkflowdynamic = function (category_id, fromstatus, newcat, category_short) {
        if (category_id == 0 && newcat == 0) {
            category_id = 0;
        }
        else if (category_id == 0 && !newcat == 0) {
            category_id = newcat;
        }
        return $http.get(urlBase + '/HomeApi/getWorkflowdynamic?fromstatus=' + fromstatus + '&category_id=' + category_id + '&category_short=' + category_short);
    };

    dataFactory.getWorkflowforfamily = function (family_id) {
        return $http.get(urlBase + '/HomeApi/getWorkflowforfamily?family_id=' + family_id);
    };

    dataFactory.getWorkflowforfamilyworkflow = function (family_id) {
        return $http.get(urlBase + '/HomeApi/getWorkflowforfamilyworkflow?family_id=' + family_id);
    };

    dataFactory.bindworkflowforfamily = function (family_id) {
        return $http.get(urlBase + '/HomeApi/bindworkflowforfamily?family_id=' + family_id);
    };

    dataFactory.getworkflowforproduct = function (product_id, family_id) {
        return $http.get(urlBase + '/HomeApi/getworkflowforproduct?product_id=' + product_id + '&family_id=' + family_id);
    };

    dataFactory.getworkflowforproductdropdown = function (product_id) {
        return $http.get(urlBase + '/HomeApi/getworkflowforproductdropdown?product_id=' + product_id);
    };
    dataFactory.getWorkflowforindesign = function (project_id) {
        return $http.get(urlBase + '/HomeApi/getWorkflowforindesign?project_id=' + project_id);
    };
    dataFactory.bindWorkflowforindesign = function (project_id) {
        return $http.get(urlBase + '/HomeApi/bindWorkflowforindesign?project_id=' + project_id);
    };
    dataFactory.indesignworkflowsave = function (project_id, statusname) {
        return $http.get(urlBase + '/HomeApi/indesignworkflowsave?project_id=' + project_id + '&statusname=' + statusname);
    };

    dataFactory.getProdjectDetails = function (SelectedprojectId) {
        return $http.post(urlBase + '/AdvanceImportApi/getProdjectDetails?SelectedprojectId=' + SelectedprojectId);
    };

    dataFactory.getAllRolesForIndesign = function () {
        return $http.get(urlBase + '/AccountApi/GetAllRolesForIndesign');
    };

    dataFactory.DeleteFromStatus = function (fromstatus, tostatus, roleid) {
        return $http.post(urlBase + '/HomeApi/DeleteFromStatus?fromstatus=' + fromstatus + '&tostatus=' + tostatus + '&roleid=' + roleid);
    };
    dataFactory.DeleteWorkflowStatus = function (Status_name, Status_code) {
        return $http.post(urlBase + '/HomeApi/DeleteWorkflowStatus?Status_name=' + Status_name + '&Status_code=' + Status_code);
    };


    dataFactory.GetConverttypeDetails = function (customerId) {
        //if (customerId === undefined) {
        //    customerId = 0;
        //}
        return $http.get(urlBase + '/HomeApi/GetConverttypeDetailsForCurrentCustomer?customerId=' + customerId); //GetCatalogDetails
    };

    dataFactory.GetAllWorkFlowDataSourceValueTOSTATUS = function (fromstatus, request) {
        return $http.post(urlBase + '/HomeApi/GetAllWorkFlowDataSourceValueTOSTATUS?fromstatus=' + fromstatus, JSON.stringify(request));
    };


    dataFactory.CategoryExport = function (projectmodel, catalogid, attrBy, filter, familyFilter, catalogname, categoryId, familyId, projectId, selcategory, selecteddata, seletedAttributes, outputformat, Delimiters, fromdate, todate, exportids, hierarchy, attributeorder, famarray, schedule, scheduleDate, ftp, url, username, passwordencrpt, templateName, exporttypes, Family_ids, splitsheet, singlesheet) {
        var data = [];
        data.push(projectmodel);
        data.push(selecteddata);
        data.push(seletedAttributes);
        //data.push(selfamily);
        data.push(JSON.stringify(Family_ids));
        data.push(JSON.stringify(attributeorder));

        return $http.post(urlBase + "/HomeApi/CategoryExport?catalogid=" + catalogid + "&attrBy=" + attrBy + "&filter=" + filter + "&familyFilter=" + familyFilter + "&catalogname=" + catalogname + "&categoryId=" + categoryId + "&familyId=" + familyId + "&projectId=" + projectId + "&selcategory_id=" + selcategory + "&outputformat=" + outputformat + "&Delimiters=" + Delimiters + "&fromdate=" + fromdate + "&todate=" + todate + "&schedule=" + schedule + "&scheduleDateTime=" + scheduleDate + "&exportids=" + exportids + "&hierarchy=" + hierarchy + "&ftp=" + ftp + "&url=" + url + "&username=" + username + "&Passwordencrpt=" + passwordencrpt + "&templateName=" + templateName + "&exporttypes=" + exporttypes + "&splitsheet=" + splitsheet + "&singlesheet=" + singlesheet, JSON.stringify(data));
    };

    dataFactory.GetAllReports = function (catalogId, startDateModify, endDateModify, startDatenew, endDatenew, option, fileNames, start_date, end_date) {

        return $http.get(urlBase + '/ReportsApi/GetAllReports?catalogId=' + catalogId + '&startDateModify=' + startDateModify + '&endDateModify=' + endDateModify + '&startDatenew=' + startDatenew + '&endDatenew=' + endDatenew + '&option=' + option + '&fileNames=' + fileNames + '&start_date=' + start_date + '&end_date=' + end_date);

    };

    dataFactory.checkproducts = function (catalogId, familyid, ids) {
        return $http.get(urlBase + "/HomeApi/checkproducts?catalogId=" + catalogId + "&family_id=" + familyid);
    };

    dataFactory.Categoryexportlength = function (catalogId,category_id, familyid) {
        return $http.get(urlBase + "/HomeApi/Categoryexportlength?catalogId=" + catalogId + "&category_id=" + category_id + "&family_id=" + familyid);
    };

    //---------------------------import template changes by Aswin kumar--------------
    dataFactory.ModifyTemplateDetails = function (fileName) {

        return $http.get(urlBase + '/AdvanceImportApi/ModifyTemplateDetails?fileName=' + fileName); //GetCatalogDetails
    };

    //dataFactory.selectedImport = function (captionName, selectedTemplate, importTypeSelection) {

    //    return $http.get(urlBase + '/AdvanceImportApi/SelectedImport?captionName=' + captionName + '&selectedTemplate=' + selectedTemplate + '&importTypeSelection=' + importTypeSelection); //GetCatalogDetails
    //};

    dataFactory.updateImportValidationFlag = function (selectedTemplateID, importTypeSelection, validationStatus, sheetName) {

        return $http.post(urlBase + '/AdvanceImportApi/UpdateImportValidationFlag?selectedTemplateID=' + selectedTemplateID + '&importTypeSelection=' + importTypeSelection + '&validationStatus=' + validationStatus + '&sheetName=' + sheetName);
    };

    dataFactory.updateImportMappingFlag = function (selectedTemplateID, importTypeSelection, sheetName) {

        return $http.get(urlBase + '/AdvanceImportApi/UpdateImportMappingFlag?selectedTemplateID=' + selectedTemplateID + '&importTypeSelection=' + importTypeSelection + '&sheetName=' + sheetName);
    };

    dataFactory.saveAllMappingAttributes = function (selectedTemplate, importTypeSelection, sheetName,excelPath, request) {

        return $http.post(urlBase + '/AdvanceImportApi/saveAllMappingAttributes?selectedTemplate=' + selectedTemplate + '&importTypeSelection=' + importTypeSelection + '&sheetName=' + sheetName + '&excelPath=' + excelPath, JSON.stringify(request)); //GetCatalogDetails
    };

    dataFactory.excelImportType = function (excelPath, sheetName) {
        return $http.get(urlBase + "/ImportApi/ExcelImportType?excelPath=" + excelPath + '&sheetName=' + sheetName);
    };

    dataFactory.validationSheetnames = function (selectedTemplate) {
        return $http.get(urlBase + "/ImportApi/ValidationSheetname?templateId=" + selectedTemplate);
    };

    dataFactory.UpdateImportFlag = function (selectedTemplateID, importTypeSelection, validationStatus, sheetName) {

        return $http.post(urlBase + '/AdvanceImportApi/UpdateImportFlag?selectedTemplateID=' + selectedTemplateID + '&importTypeSelection=' + importTypeSelection + '&validationStatus=' + validationStatus + '&sheetName=' + sheetName);
    };

    dataFactory.getMappingSheetNames = function (selectedTemplate) {
        return $http.get(urlBase + "/ImportApi/GetMappingSheetNames?templateId=" + selectedTemplate);
    };

    dataFactory.getImportMissingMappingAttributeResult = function () {
        return $http.get(urlBase + '/AdvanceImportApi/ImportMissingMappingAttributeResult');
    };


    dataFactory.UpdateTemplateDetails = function (templateNameImport, catalogIdAttr, importFormat, filePath, templateId) {
        return $http.post(urlBase + '/AdvanceImportApi/UpdateTemplateDetails?templateNameImport=' + templateNameImport + '&catalogId=' + catalogIdAttr + '&importFormat=' + importFormat + '&filePath=' + filePath + '&templateId=' + templateId);
    }


    dataFactory.getdefaultfamily = function (catalogId) {
        return $http.get(baseUrl + '/Catalog/getdefaultfamily?catalogid=' + catalogId);
    };

    dataFactory.getdefaultfamilyexists = function (catalogId) {
        return $http.get(baseUrl + '/Catalog/Getdefaultfamilyexists?catalogid=' + catalogId);
    };

    //----------------------------import template changes by Aswin kumar-----------

    ////// group mapping attributes ///////////

    dataFactory.GetCurrentCatalogGroupNames = function (catalogId, importType, templateId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/AdvanceImportApi/GetCurrentCatalogGroupNames?catalogId=' + catalogId + '&importType=' + importType + '&templateId=' + templateId, JSON.stringify(request));
    };

    dataFactory.groupAttributeSheetnames = function (selectedTemplate) {
        return $http.get(urlBase + "/ImportApi/GroupAttributeSheetnames?templateId=" + selectedTemplate);
    };


    dataFactory.GetAllGroupAttributesByGroupId = function (catalogId, groupId, importType, templateId, isAvailable) {

        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        if (groupId === undefined || groupId === '' || groupId === "") {
            groupId = 0;
        }

        return $http.get(urlBase + '/AdvanceImportApi/GetAllGroupAttributesByGroupId?catalogId=' + catalogId + '&groupId=' + groupId + '&importType=' + importType + '&templateId=' + templateId + '&isAvailable=' + isAvailable);
    };

    dataFactory.SaveGroupNamesByTemplateId = function (catalogId, importType, templateId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/AdvanceImportApi/SaveGroupNamesByTemplateId?catalogId=' + catalogId + '&importType=' + importType + '&templateId=' + templateId, JSON.stringify(request));
    };

    dataFactory.DeleteGroupNamesByTemplateId = function (catalogId, importType, templateId, request) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/AdvanceImportApi/DeleteGroupNamesByTemplateId?catalogId=' + catalogId + '&importType=' + importType + '&templateId=' + templateId, JSON.stringify(request));
    };


    dataFactory.UpdateAttributeNamesByTemplateId = function (catalogId, importType, templateId, groupId, isAvailable, attributeId) {
        if (catalogId === undefined || catalogId === '' || catalogId === "") {
            catalogId = 0;
        }
        return $http.post(urlBase + '/AdvanceImportApi/UpdateAttributeNamesByTemplateId?catalogId=' + catalogId + '&importType=' + importType + '&templateId=' + templateId + '&groupId=' + groupId + '&isAvailable=' + isAvailable + '&attributeId=' + attributeId);
    };

    dataFactory.BtnAttributeNameMoveUpClick = function (attributeId, groupId, templateId) {
        return $http.post(urlBase + "/AdvanceImportApi/AttributeSortOrderMoveUp?attributeId=" + attributeId + "&groupId=" + groupId + "&templateId=" + templateId);
    };

    dataFactory.BtnAttributeNameMoveDownClick = function (attributeId, groupId, templateId) {
        return $http.post(urlBase + "/AdvanceImportApi/AttributeNameMoveDownClick?attributeId=" + attributeId + "&groupId=" + groupId + "&templateId=" + templateId);
    };

    dataFactory.BtnGroupNameMoveUp = function (templateId, groupId, importType, catalogId) {
        return $http.post(urlBase + "/AdvanceImportApi/GroupNameMoveUp?templateId=" + templateId + "&groupId=" + groupId + "&importType=" + importType + "&catalogId=" + catalogId);
    };

    dataFactory.BtnGroupNameMoveDown = function (templateId, groupId, importType, catalogId) {
        return $http.post(urlBase + "/AdvanceImportApi/GroupNameMoveDown?templateId=" + templateId + "&groupId=" + groupId + "&importType=" + importType + "&catalogId=" + catalogId);
    };

    dataFactory.SaveAttributeNamesByTemplateId = function (templateId, groupId, request) {

        return $http.post(urlBase + '/AdvanceImportApi/SaveAttributeNamesByTemplateId?templateId=' + templateId + "&groupId=" + groupId, JSON.stringify(request));
    };

    dataFactory.DeleteAttributeNamesByTemplateId = function (templateId, request) {

        return $http.post(urlBase + '/AdvanceImportApi/DeleteAttributeNamesByTemplateId?templateId=' + templateId, JSON.stringify(request));
    };

    ////// group mapping attributes ///////////
  
    dataFactory.AssetsCount = function () {

        return $http.get(urlBase + '/HomeApi/AssetsCount');
    };
    dataFactory.DataQuality = function (catalog_id) {

        return $http.get(urlBase + '/HomeApi/DataQuality?catalog_id=' + catalog_id);
    };

    //START DAL FACTORIES//////////

    dataFactory.getLoggedUserInfo = function () {
        return $http.post(urlBase + '/DALHomeApi/GetLoggedUserInfo');
    };
    dataFactory.getCategoryFamilyList = function (categoryId, option, resouceTypeId, familyId, userGroupId, userId) {
        return $http.get(urlBase + '/DALHomeApi/GetCategoryFamilyList?categoryId=' + categoryId + '&option=' + option + '&resouceTypeId=' + resouceTypeId + '&familyId=' + familyId + '&userGroupId=' + userGroupId + '&userId=' + userId);
    };

    dataFactory.getBreadCrumbList = function (categoryId, familyId, productId, breadcrumbOption) {
        return $http.get(urlBase + '/DALHomeApi/GetBreadCrumbList?categoryId=' + categoryId + '&familyId=' + familyId + '&productId=' + productId + '&breadcrumbOption=' + breadcrumbOption);
    };

    dataFactory.getProductDetails = function (categoryId, familyId, productId, userId) {
        return $http.get(urlBase + '/DALHomeApi/GetProductDetails?categoryId=' + categoryId + '&familyId=' + familyId + '&productId=' + productId + '&userId=' + userId);
    };

    dataFactory.cartItemTypeCheck = function (categoryId, familyId, productId) {
        return $http.post(urlBase + '/DALHomeApi/CartItemTypeCheck?categoryId=' + categoryId + '&familyId=' + familyId + '&productId=' + productId);
    };
    dataFactory.addToCart = function (userId, attributeId, attributeName, familyId, productId, filePath) {
        return $http.post(urlBase + '/DALHomeApi/AddDetailsToCart?userId=' + userId + '&attributeId=' + attributeId + '&pricingClass=' + attributeName + '&familyId=' + familyId + '&productId=' + productId + '&filePath=' + filePath);
    };
    dataFactory.resourcePDFGen = function (familyId, productId, pdfType, pdfFor, attributeId, userId, cobrandFlag) {
        return $http.post(urlBase + '/DALHomeApi/GenerateResourcePDF?familyId=' + familyId + '&productId=' + productId + '&pdfType=' + pdfType + '&pdfFor=' + pdfFor + '&attributeId=' + attributeId + '&userId=' + userId + '&cobrandFlag=' + cobrandFlag);
    };
    dataFactory.checkFileExits = function (filePath) {
        return $http.post(urlBase + '/DALHomeApi/CheckFileExits?filePath=' + filePath);
    };
    dataFactory.getImageCompressDetails = function (itemNumber, downloadFormat, imageList) {
        return $http.post(urlBase + '/DALHomeApi/GetImageCompressDetails?itemNumber=' + itemNumber + '&downloadFormat=' + downloadFormat, JSON.stringify(imageList));
    };
    dataFactory.updateLinkDetails = function (objData) {
        return $http.post(urlBase + '/DALHomeApi/UpdateLinkDetails?', JSON.stringify(objData));
    };
    dataFactory.userAgreementUpdate = function (userId) {
        return $http.post(urlBase + '/DALHomeApi/UserAgreementUpdate?userId=' + userId);
    };
    dataFactory.pdfAnalyticsDetailsEntry = function (pdfPath, analyticType, userId, fileType) {
        return $http.post(urlBase + '/AnalyticsApi/PDFanalyticsEntryCall?pdfPath=' + pdfPath + '&analyticType=' + analyticType + '&userId=' + userId + '&fileType=' + fileType);
    };
    dataFactory.productAnalyticsDetailsEntry = function (itemNumber, userId) {
        return $http.post(urlBase + '/AnalyticsApi/ProductAnalyticsEntryCall?itemNumber=' + itemNumber + '&userId=' + userId);
    };

    dataFactory.lineCardPDFGeneration = function (generationType, lineCardData) {
        return $http.post(urlBase + '/LineCardApi/LineCardPDFGeneration?generationType=' + generationType, JSON.stringify(lineCardData));
    };

    ///user groups//
    dataFactory.getUserNameBasedonGroupName = function (roleID, CustomerId) {
        return $http.get(urlBase + '/UserAdminApi/getUserNameBasedonGroupName?roleID=' + roleID + '&CustomerId=' + CustomerId);
    };
    dataFactory.getUsernameListExceptSelected = function (RoleId, customerid) {
        return $http.get(urlBase + '/UserAdminApi/getUsernameListExceptSelected?RoleId=' + RoleId + '&customerid=' + customerid);

    };
    dataFactory.updateGroupName = function (CustomerId, RoleId, updateDatas) {
        return $http.post(urlBase + '/UserAdminApi/updateGroupName?CustomerId=' + CustomerId + '&RoleId=' + RoleId, JSON.stringify(updateDatas));
    };
    dataFactory.deleteUser = function (EmailId) {
        return $http.get(urlBase + '/UserAdminApi/deleteUser?EmailId=' + EmailId);
    };

    dataFactory.changeStatus = function (EmailId) {
        return $http.get(urlBase + '/UserAdminApi/changeStatus?EmailId=' + EmailId);
    };
    dataFactory.saveOrganisation = function (organisationName) {
        return $http.get(urlBase + '/UserAdminApi/saveOrganisation?organisationName=' + organisationName);
    };
    dataFactory.loadOrganizationName = function () {
        return $http.get(urlBase + '/UserAdminApi/loadOrganizationName');
    };
    dataFactory.getUserNameForCheck = function () {
        return $http.get(urlBase + '/UserAdminApi/getUserNameForCheck');
    };
    dataFactory.getOrganisationName = function (organizationId) {
        return $http.get(urlBase + '/UserAdminApi/getOrganisationName?organizationId=' + organizationId);
    };
    dataFactory.updateOrganisationName = function (organizationName, organizationNameEdit) {
        return $http.get(urlBase + '/UserAdminApi/updateOrganisationName?organizationName=' + organizationName + '&organizationNameEdit=' + organizationNameEdit);
    };
    dataFactory.getGroupName = function (GroupID) {
        return $http.get(urlBase + '/UserAdminApi/getGroupName?GroupID=' + GroupID );
    };
    dataFactory.checkIforganizationIsassociated = function (OrganizationId) {
        return $http.get(urlBase + '/UserAdminApi/checkIforganizationIsassociated?OrganizationId=' + OrganizationId);
    };
    dataFactory.deleteOrganization = function (OrganizationId, checkUserAssociated) {
        return $http.get(urlBase + '/UserAdminApi/deleteOrganization?OrganizationId=' + OrganizationId + '&checkUserAssociated=' + checkUserAssociated);
    };
    dataFactory.getOrganizationId = function (OrganizationName) {
        return $http.get(urlBase + '/UserAdminApi/getOrganizationId?OrganizationName=' + OrganizationName);
    };
     dataFactory.checkifUserExistInTheSelectedGroup = function (userRoleId) {
         return $http.get(urlBase + '/UserAdminApi/checkifUserExistInTheSelectedGroup?userRoleId=' + userRoleId);
     };
     dataFactory.deleteGroups = function (userRoleId,userDelete) {
         return $http.get(urlBase + '/UserAdminApi/deleteGroups?userRoleId=' + userRoleId + '&userDelete=' + userDelete);
     };
     dataFactory.removeGroupsfromUser = function (userRoleId) {
         return $http.get(urlBase + '/UserAdminApi/removeGroupsfromUser?userRoleId=' + userRoleId);
     };
     dataFactory.getGroupNames = function (CustomerId) {
         return $http.get(urlBase + '/UserAdminApi/getGroupNames?CustomerId='+ CustomerId);
     };
     dataFactory.getRoleDetails = function (CustomerId, GroupNames) {
         return $http.get(urlBase + '/UserAdminApi/getRoleDetails?CustomerId=' + CustomerId + '&GroupNames=' + GroupNames);
     };
       dataFactory.uncheckUserfromGroup = function (RoleId, removeData) {
           return $http.get(urlBase + '/UserAdminApi/uncheckUserfromGroup?RoleId=' + RoleId + '&removeData=' + removeData);
       };
       dataFactory.editGroupName = function (RoleId) {
           return $http.get(urlBase + '/UserAdminApi/editGroupName?RoleId=' + RoleId);
       };
       dataFactory.saveEditName = function (EditedGroupName ,ExisitingGroupName,CustomerId) {
           return $http.get(urlBase + '/UserAdminApi/saveEditName?EditedGroupName=' + EditedGroupName + '&ExisitingGroupName=' + ExisitingGroupName + '&CustomerId=' + CustomerId);
       };

    //Active Directory Update Process
    //Added by Jothipriya on APril 20 2022
       dataFactory.SaveWindowsUserDetails = function (USER_NAME, SysUser_Name, SysDomain_Name, User_Status) {
           return $http.get(urlBase + '/UserAdminApi/SaveWindowsUserDetails?&USER_NAME=' + USER_NAME + '&SysDomain_Name=' + SysDomain_Name + '&SysUser_Name=' + SysUser_Name + '&User_Status=' + User_Status);
       };

       dataFactory.GetWindowsUserDetails = function (User_Name) {
           return $http.get(urlBase + '/UserAdminApi/GetWindowsUserDetails?&User_Name=' + User_Name);
       }
    //Active Directory Update Process
    //END DAL FACTORIES/////////

       dataFactory.getSAASCustomer = function () {

           return $http.get(urlBase + '/UserAdminApi/getSAASCustomer');
    };
        dataFactory.getAnalyticsUserActiveGridDetails = function (option, filterFromDate, filterToDate) {
         return $http.get(urlBase + '/AnalyticsApi/GetAnalyticsUserActiveGridDetails?option=' + option + '&filterFromDate=' + filterFromDate + '&filterToDate=' + filterToDate);
     };
        dataFactory.getAnalyticsMostVisitedGridDetails = function (option) {
            return $http.get(urlBase + '/AnalyticsApi/GetAnalyticsMostVisitedGridDetails?option=' + option);
        };
        dataFactory.forceLogOut = function (LogoutClick) {
            return $http.post(urlBase + '/AccountApi/Force_LogOut?LogoutClick=' + LogoutClick)
        };
        dataFactory.ProdCount = function (SelectedCatalogId) {

            return $http.get(urlBase + '/HomeApi/ProdCount?&SelectedCatalogId=' + SelectedCatalogId);
    };

    dataFactory.SaveIDValue = function () {
        return $http.post(urlBase + '/ExportApi/SaveIDValue');
    };
    

    
 
    //---------------------------------------------------------------------------------------------------------------------------------------------------
    return dataFactory;

}]);
