﻿LSApp.directive('onEnterBlur', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                if ($(element).val().length > 0) {
                    element.blur();
                }
                event.preventDefault();
            }
        });
    };
});