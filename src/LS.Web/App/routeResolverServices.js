﻿'use strict';
angular.module('routeResolverServices', []).provider("routeResolver", function () {

    this.$get = function () {
        return this;
    };

    this.route = function (routeConfig) {

        var resolve = function (basePath, viewName, controllerName, serviceName, secure) {
            var baseUrl = $("base").first().attr("href");
            var routeDef = {};
            routeDef.templateUrl = baseUrl + basePath + viewName + ".html?v=" + buildVersionNo;
            routeDef.controller = controllerName + "Controller";
            routeDef.secure = (secure) ? secure : false;
            routeDef.resolve = {
                load: ['$q', '$rootScope', function ($q, $rootScope) {
                    var dependenciesArray = new Array();
                    dependenciesArray.push(basePath + controllerName + "Controller.js?v=" + buildVersionNo);
                    if (serviceName != undefined && serviceName != "") {
                        dependenciesArray.push(basePath + servicePath + ".js?v=" + buildVersionNo);
                    }
                    var dependencies = dependenciesArray;
                    return resolveDependencies($q, $rootScope, dependencies);
                }]
            };

            return routeDef;
        },

        resolveDependencies = function ($q, $rootScope, dependencies) {
            var defer = $q.defer();
            require(dependencies, function () {
                defer.resolve();
                $rootScope.$apply();
            });

            return defer.promise;
        };

        return {
            resolve: resolve
        }
    }(this.routeConfig);

});