﻿
'use strict';
LSApp.controller('navBarController', ['$scope', '$window', '$location', 'dataFactory',  function ($scope, $window, $location, dataFactory) {

    $scope.isActive = function (url) {
        if ($location.absUrl().indexOf(url) > -1) {
            $("#dashboard").removeClass("topMenuActive");
            return "topMenuActive";
        }
        else {
            $("#dashboard").addClass("topMenuActive");
        }
    };
}]);