﻿/// <reference path="app.js" />
'use strict';
var LSApp = angular.module('LSApp', [
    'ngRoute',
    'ngTable',
    'routeResolverServices',
    'ngDialog',
    'kendo.directives',
    'blockUI',
    'ngSanitize', 'ngGrid', 'scrollable-table', 'ngStorage', 'uiSwitch','ngDragDrop'
    //'angular.filter'
]);

LSApp.filter('unique', function () {
    return function (input, key) {
        var unique = {};
        var uniqueList = [];
        for (var i = 0; i < input.length; i++) {
            if (typeof unique[input[i][key]] == "undefined") {
                unique[input[i][key]] = "";
                uniqueList.push(input[i]);
            }
        }
        return uniqueList;
    };
});
LSApp.config(['$httpProvider', '$routeProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', 'routeResolverProvider', function ($httpProvider, $routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, routeResolverProvider) {
    var baseUrl = $("base").first().attr("href");
    LSApp.register =
    {
        controller: $controllerProvider.register,
        directive: $compileProvider.directive,
        filter: $filterProvider.register,
        factory: $provide.factory,
        service: $provide.service
    };

    var customRouteResolver = routeResolverProvider.route;

    $httpProvider.interceptors.push('authInterceptorSrv');
}]);

LSApp.service(['$ngfilemanagerprovider', function ($ngfilemanagerprovider) {
    $ngfilemanagerprovider : [
     {
         provide: 'fileManagerUrls',
         useValue: { foldersUrl: '~/Content/ProductImages/CB', filesUrl: '~/Content/ProductImages/CB' }
     }
    ]
}]);
LSApp.config(['$localStorageProvider',
    function ($localStorageProvider) {
        $localStorageProvider.get('MyKey');
        $localStorageProvider.set('MyKey', { k: 'value' });
    }]);

LSApp.config(['ngDialogProvider', function (ngDialogProvider) {
    ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        appendTo: false,
        preCloseCallback: function () {
            console.log('default pre-close callback');
        }
    });
}]);

LSApp.config(['$httpProvider', 'blockUIConfig', function ($httpProvider, blockUIConfig) {
    // Change the default overlay message
    blockUIConfig.message = 'Please wait..';
    blockUIConfig.blockBrowserNavigation = true;
    // Change the default delay to 100ms before the blocking is visible
    blockUIConfig.delay = 100;

}]);

LSApp.run(['$rootScope', '$location', 'dataFactory', function ($rootScope, $location, dataFactory) {
    dataFactory.authenticateUser();

    $rootScope.regExpPositiveDecimal = /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/;
    $rootScope.regExpEmailId = /^[A-Za-z0-9._-]+[A-Za-z0-9._-]+@[A-Za-z0-9._-]+\.[A-Za-z.]{2,5}$/;
    $rootScope.regExpPhoneNo = /[0-9]{3}-[0-9]{3}-[0-9]{4}/;
    $rootScope.regExUrl = /^[a-zA-Z0-9\-\.]+\.(com|org|net|mil|edu|COM|ORG|NET|MIL|EDU)$/;

    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        
    });
}]);

LSApp.service('productService', function () {
    var productList = [];
    var addProduct = function (newObj) {
        productList.push(newObj);
    };

    var getProducts = function () {
        return productList;
    };

    return {
        addProduct: addProduct,
        getProducts: getProducts
    };

});


LSApp.factory("stacktraceService", function () {
    return ({
        print: printStackTrace
    });
});


LSApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});
LSApp.directive('ngRightClick', function ($parse) {
    return function (scope, element, attrs) {

        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function (event) {
            scope.$apply(function () {
                event.preventDefault();
                fn(scope, { $event: event });
            });
        });
    };
});


LSApp.directive('rightClick', function () {

    document.oncontextmenu = function (e) {
        if (e.target.hasAttribute('right-click')) {
            return false;
        }
    };
    return function (scope, el, attrs) {
        el.bind('contextmenu', function (e) {
        });
    };
});


LSApp.directive('onLongPress', function ($timeout) {
    return {
        restrict: 'A',
        link: function ($scope, $elm, $attrs) {
            $elm.bind('touchstart', function (evt) {
                // Locally scoped variable that will keep track of the long press
                $scope.longPress = true;
                // We'll set a timeout for 600 ms for a long press
                $timeout(function () {
                    if ($scope.longPress) {
                        // If the touchend event hasn't fired,
                        // apply the function given in on the element's on-long-press attribute
                        $scope.$apply(function () {
                            $scope.$eval($attrs.onLongPress)
                        });
                    }
                }, 600);
            });

            $elm.bind('touchend', function (evt) {
                // Prevent the onLongPress event from firing
                $scope.longPress = false;
                // If there is an on-touch-end function attached to this element, apply it
                if ($attrs.onTouchEnd) {
                    $scope.$apply(function () {
                        $scope.$eval($attrs.onTouchEnd);
                    });
                }
            });
        }
    };
});


LSApp.directive('$sce', function ($sce) {
    return function (scope, element, attr) {
        scope.$watch($sce.parseAsHtml(attr.ngBindHtml), function (value) {
            element.html(value || '');
        });
    };
});

LSApp.directive('ckedit', function ($parse) {
    CKEDITOR.disableAutoInline = true;
    var counter = 0,
    prefix = '__ckd_';

    return {
        restrict: 'A',
        link: function (scope, element, attrs, controller) {
            var getter = $parse(attrs.ckedit),
                setter = getter.assign;

            attrs.$set('contenteditable', true); // inline ckeditor needs this
            if (!attrs.id) {
                attrs.$set('id', prefix + (++counter));
            }
            // CKEditor stuff
            // Override the normal CKEditor save plugin

            CKEDITOR.plugins.registered['save'] =
            {
                init: function (editor) {
                    editor.addCommand('save',
                        {
                            modes: { wysiwyg: 1, source: 1 },
                            exec: function (editor) {
                                if (editor.checkDirty()) {
                                    var ckValue = editor.getData();
                                    scope.$apply(function () {
                                        setter(scope, ckValue);
                                    });
                                    ckValue = null;
                                    editor.resetDirty();
                                }
                            }
                        }
                    );
                    editor.ui.addButton('Save', { label: 'Save', command: 'save', toolbar: 'document' });
                }
            };
            var options = {};
            options.on = {
                blur: function (e) {
                    if (e.editor.checkDirty()) {
                        var ckValue = e.editor.getData();
                        scope.$apply(function () {
                            setter(scope, ckValue);
                        });
                        ckValue = null;
                        e.editor.resetDirty();
                    }
                }
            };
            options.extraPlugins = 'sourcedialog';
            options.removePlugins = 'sourcearea';
            var editorangular = CKEDITOR.inline(element[0], options); //invoke
            scope.$watch(attrs.ckedit, function (value) {
                editorangular.setData(value);
            });
        }
    };

});

LSApp.directive('appFilereader', function ($q) {
    /*
    made by elmerbulthuis@gmail.com WTFPL licensed
    */
    var slice = Array.prototype.slice;

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;

            ngModel.$render = function () {
            };

            element.bind('change', function (e) {
                var element = e.target;
                if (!element.value) return;

                element.disabled = true;
                $q.all(slice.call(element.files, 0).map(readFile))
                  .then(function (values) {
                      if (element.multiple) ngModel.$setViewValue(values);
                      else ngModel.$setViewValue(values.length ? values[0] : null);
                      element.value = null;
                      element.disabled = false;
                  });

                function readFile(file) {
                    var deferred = $q.defer();

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        deferred.resolve(e.target.result);
                    };
                    reader.onerror = function (e) {
                        deferred.reject(e);
                    };
                    reader.readAsDataURL(file);

                    return deferred.promise;
                }

            }); //change

        } //link

    }; //return

});

LSApp.factory("errorLogServiceDummy", ['$window', function ($window) {
    function log() {
        console.log("Error");
    }
    return (log);
}]);

LSApp.factory("errorLogService", ['$log', '$window', 'stacktraceService', function ($log, $window, stacktraceService) {
    function log(exception, cause) {
        $log.error.apply($log, arguments);
        try {
            var baseUrl = $("base").first().attr("href");

            var errorMessage = exception.toString();
            var stackTrace = stacktraceService.print({ e: exception.stack });

            var url = baseUrl + "/api/CodeTableAPI/JavaScriptErrorLogger";
            $.ajax({
                type: "POST",
                url: url,
                contentType: "application/json",
                data: angular.toJson({
                    errorUrl: $window.location.href,
                    errorMessage: errorMessage,
                    stackTrace: stackTrace,
                    cause: (cause || "")
                })
            }).success(function (response) {
                console.log("error logged in server");
            }).error(function (response) {
                console.log("error logged in server failed");
            });

        } catch (loggingError) {
            // For Developers - log the log-failure.
            $log.warn("Error logging failed");
            $log.log(loggingError);
        }
    }
    // Return the logging function.
    return (log);
}]);


LSApp.factory('authInterceptorSrv', ['$q', '$injector', '$location', '$window', function ($q, $injector, $location, $window) {

    var authInterceptorSrvFactory = {};

    var request = function (config) {

        config.headers = config.headers || {};
        return config;
    }; // optional method
    var response = function (response) {
        // do something on success
        var baseUrl = $("base").first().attr("href");
        var html = $.parseHTML(response.data);
        if (html != null) {
            if (angular.element(html).find("#loginWrapper").length > 0) {
                if ($window.location.href.search("Accounts/LogOn") == -1) {
                    $window.location.href = baseUrl + "Accounts/LogOn";
                }
                else {
                    return response;
                }
            }
            else {
                return response;
            }
        }
        else {
            return response;
        }

    };

    var responseError = function (rejection) {
        if (rejection.status === 401) {
            alert("You do not have the required privileges, please contact Administrator.");
        }
        else {
            var httpErrorCode = "";
            var httpErrorStatusText = "";
            var errorMessage = "";
            var data = "";
            var detailiedErrorMessage = "";
            var dhlErrorResponseHandled = false;
            if (rejection.status) {
                httpErrorCode = rejection.status;
            }
            if (rejection.statusText) {
                httpErrorStatusText = rejection.statusText;
            }
            if (rejection.data) {
                errorMessage = rejection.data;
                if (rejection.data.Message) {
                    errorMessage = rejection.data.Message;
                    if (errorMessage.search("DHL Api Response:") >= 0) {
                        alert(errorMessage);
                        dhlErrorResponseHandled = true;
                    }
                }
                if (!dhlErrorResponseHandled) {
                    if (rejection.data.MessageDetail) {
                        detailiedErrorMessage = rejection.data.MessageDetail;
                    }
                    var serverReturnedError = "Please Reload The Page and Continue.\n\n";
                    serverReturnedError = serverReturnedError + "Error Status Code: " + httpErrorCode + "\n\nError Status Text: " + httpErrorStatusText + "\n\nError Message: " + errorMessage;
                    alert(serverReturnedError);
                }
            }
            else {
                alert("Please Reload The Page and Continue.\n\n Error Status Code: " + httpErrorCode + "\n\nError Status Text: " + httpErrorStatusText);
            }

        }
        return $q.reject(rejection);
    };
    authInterceptorSrvFactory.request = request;
    authInterceptorSrvFactory.response = response;
    authInterceptorSrvFactory.responseError = responseError;

    return authInterceptorSrvFactory;
}]);

LSApp.directive('jsXls', function () {
    return {
        restrict: 'E',
        template: '<input type="file" />',
        replace: true,
        link: function (scope, element, attrs) {

            function handleSelect() {
                var files = this.files;
                for (var i = 0, f = files[i]; i != files.length; ++i) {
                    var reader = new FileReader();
                    var name = f.name;
                    reader.onload = function (e) {
                        if (!e) {
                            var data = reader.content;
                        } else {
                            var data = e.target.result;
                        }

                        /* if binary string, read with type 'binary' */
                        try {
                            var workbook = XLS.read(data, { type: 'binary' });

                            if (attrs.onread) {
                                var handleRead = scope[attrs.onread];
                                if (typeof handleRead === "function") {
                                    handleRead(workbook);
                                }
                            }
                        } catch (e) {
                            if (attrs.onerror) {
                                var handleError = scope[attrs.onerror];
                                if (typeof handleError === "function") {
                                    handleError(e);
                                }
                            }
                        }

                        // Clear input file
                        element.val('');
                    };

                    //extend FileReader
                    if (!FileReader.prototype.readAsBinaryString) {
                        FileReader.prototype.readAsBinaryString = function (fileData) {
                            var binary = "";
                            var pt = this;
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var bytes = new Uint8Array(reader.result);
                                var length = bytes.byteLength;
                                for (var i = 0; i < length; i++) {
                                    binary += String.fromCharCode(bytes[i]);
                                }
                                //pt.result  - readonly so assign binary
                                pt.content = binary;
                                $(pt).trigger('onload');
                            }
                            reader.readAsArrayBuffer(fileData);
                        }
                    }

                    reader.readAsBinaryString(f);

                }
            }

            element.on('change', handleSelect);
        }
    };
});

