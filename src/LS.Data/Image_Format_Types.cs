//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Image_Format_Types
    {
        public int ID { get; set; }
        public string FORMAT_TYPE_NAME { get; set; }
        public Nullable<int> FORMAT_TYPE { get; set; }
        public string CREATED_USER { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public Nullable<System.DateTime> MODIFIED_DATE { get; set; }
    }
}
