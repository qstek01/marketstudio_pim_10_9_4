//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_TEMPLATE_DETAILS
    {
        public int TEMPLATE_ID { get; set; }
        public Nullable<int> PROJECT_ID { get; set; }
        public Nullable<int> CATALOG_ID { get; set; }
        public string EXPORT_FORMAT { get; set; }
        public string EXPORT_ATTRIBUTE { get; set; }
        public Nullable<bool> HIERARCHY_REQUIRED { get; set; }
        public Nullable<bool> APPLY_FILTER { get; set; }
        public Nullable<bool> EXPORT_ID_COLUMN { get; set; }
        public Nullable<bool> SUB_PRODUCTS_REQUIRED { get; set; }
        public string EXPORT_TYPE { get; set; }
        public string DELIMITER { get; set; }
        public Nullable<System.DateTime> FROM_DATE { get; set; }
        public Nullable<System.DateTime> TO_DATE { get; set; }
        public string COMMENTS { get; set; }
        public Nullable<System.DateTime> LAST_MODIFIED_DATE { get; set; }
    }
}
