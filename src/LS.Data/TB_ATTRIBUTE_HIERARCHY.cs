//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_ATTRIBUTE_HIERARCHY
    {
        public int ID { get; set; }
        public Nullable<int> PACK_ID { get; set; }
        public string TYPE { get; set; }
        public string ASSIGN_TO { get; set; }
        public Nullable<int> CUSTOMER_ID { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
    
        public virtual TB_PACKAGE_MASTER TB_PACKAGE_MASTER { get; set; }
    }
}
