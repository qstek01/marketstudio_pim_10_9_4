﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;
using System.Data.Entity.Validation;
using log4net;


namespace LS.Data
{
    public abstract class BaseRepository<TObjectContext, T>
        where TObjectContext : DbContext
        where T : BaseRepository<TObjectContext, T>
    {        

        public string ConnectionString
        {
            get
            {
                var b = new EntityConnectionStringBuilder
                {
                    ProviderConnectionString =
                        ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString,
                    Metadata = ConnectionStringMetaData,
                    Provider = ConnectionStringProvider
                };
                return b.ToString();
            }
        }

        protected string ConnectionStringMetaData
        {
            get
            {
                return "res://*/LS.csdl|res://*/LS.ssdl|res://*/LS.msl";
            }
        }

        protected virtual string ConnectionStringProvider
        {
            get
            {
                return "System.Data.SqlClient";
            }
        }

        private static TObjectContext _dc = null; // to be used with non-web applications
        private static T _instance = null;

        public static TObjectContext DC
        {
            get
            {
                if (_instance == null)
                {
                    _instance = CreateInstance();
                }

                string sDictName = typeof(TObjectContext).Name;

                if (System.Web.HttpContext.Current == null)
                {
                    if (_dc == null)
                        _dc = CreateOjbectContext(_instance.ConnectionString);
                    return _dc;
                }
                if (!System.Web.HttpContext.Current.Items.Contains(sDictName))
                {
                    TObjectContext t = CreateOjbectContext(_instance.ConnectionString);
                    System.Web.HttpContext.Current.Items[sDictName] = t;
                }

                return (TObjectContext)System.Web.HttpContext.Current.Items[sDictName];
            }
            set
            {
                _dc = value;
            }
        }

        public static void ClearContext()
        {
            string sDictName = typeof(TObjectContext).Name;
            if (System.Web.HttpContext.Current.Items.Contains(sDictName))
            {
                System.Web.HttpContext.Current.Items.Remove(sDictName);
            }
        }

        public static int SaveChanges()
        {
            try
            {
                return DC.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                    ); // Add the original exception as the innerException
            }
            catch (Exception ex)
            {
                throw ex;
            }  
        }

        private static T CreateInstance()
        {
            return (T)typeof(T).GetConstructor(new System.Type[] {  }).Invoke(new string[] { });
        }

        private static TObjectContext CreateOjbectContext(string sConnectString)
        {
            return (TObjectContext)typeof(TObjectContext).GetConstructor(new System.Type[] { typeof(string) }).Invoke(new string[] { sConnectString });
        }


    }
}
