﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using LS.Data;
using LS.Data.Model;
using log4net;
using Kendo.DynamicLinq;
using LS.Data.Model.Accounts;
using System.Web.Security;

namespace LS.Data
{
    public class UserAccountsRepository : BaseRepository<CSEntities, UserAccountsRepository>
    {
        static ILog _logger = LogManager.GetLogger(typeof(UserAccountsRepository));

        //public static bool UpdateCustomer(CustomerModel model)
        //{
        //    try
        //    {
        //        _logger.Info("Inside Update Customer");
        //        Customer objCustmer = null;

        //        Address billingAddress = null;
        //        Address headQuartersAddress = null;
        //        CarrrierAccount carrierAccount = null;

        //        if (model.CustomerId > 0)
        //        {
        //            objCustmer = DC.Customers.FirstOrDefault(x => x.CustomerId == model.CustomerId);
        //            billingAddress = objCustmer.Addresses.Where(x => x.AddressType == "BillingAddress").FirstOrDefault();
        //            headQuartersAddress = objCustmer.Addresses.Where(x => x.AddressType == "HeadQuaters").FirstOrDefault();
        //            carrierAccount = objCustmer.CarrrierAccounts.FirstOrDefault(x => x.CarrierId == 12);
        //        }
        //        else
        //        {
        //            objCustmer = new Customer {DateCreated = DateTime.Now};
        //            DC.Customers.Add(objCustmer);
        //        }

        //        if (billingAddress == null)
        //        {
        //            billingAddress = new Address();
        //            billingAddress.CustomerId = objCustmer.CustomerId;
        //            objCustmer.Addresses.Add(billingAddress);
        //        }
        //        if (headQuartersAddress == null)
        //        {
        //            headQuartersAddress = new Address();
        //            headQuartersAddress.CustomerId = objCustmer.CustomerId;
        //            objCustmer.Addresses.Add(headQuartersAddress);
        //        }
        //        if (carrierAccount == null)
        //        {
        //            carrierAccount = new CarrrierAccount { CarrierId = 12 };
        //            carrierAccount.CustomerId = objCustmer.CustomerId;
        //            objCustmer.CarrrierAccounts.Add(carrierAccount);
        //        }

        //        objCustmer.CustomerName = model.CustomerName;
        //        objCustmer.CustomerNumber = model.CustomerNumber;
        //        objCustmer.RateType = "Default";
        //        objCustmer.CustomerName = model.CustomerName;
        //        objCustmer.CustomerNumber = model.CustomerNumber;
        //        objCustmer.DateUpdated = DateTime.Now;
        //        objCustmer.ActivationDate = model.ActivationDate;
        //        //objCustmer.SalesRep = model.;
        //        objCustmer.SubmitDate = model.SubmitDate;
        //        objCustmer.InActive = model.InActive;
        //        objCustmer.TaxId = model.TaxId;
                
        //        billingAddress.Address1 = model.HeadQuartersAddress.Address1;
        //        billingAddress.Address2 = model.BillingAddress.Address2;
        //        billingAddress.City = model.BillingAddress.City;
        //        billingAddress.Comments = model.BillingAddress.Comments;
        //        billingAddress.ContactName = model.BillingAddress.ContactName;
        //        billingAddress.ContactTitle = model.BillingAddress.ContactTitle;
        //        billingAddress.Country = model.BillingAddress.Country;
        //        billingAddress.EMail1 = model.BillingAddress.EMail1;
        //        billingAddress.CustomerId = model.BillingAddress.CustomerId;
        //        billingAddress.DateUpdated = DateTime.Now;
        //        billingAddress.Fax = model.BillingAddress.Fax;
        //        billingAddress.Phone = model.BillingAddress.Phone;
        //        billingAddress.State_Province = model.BillingAddress.StateProvince;
        //        billingAddress.WebSite = model.BillingAddress.WebSite;
        //        billingAddress.Zip = model.BillingAddress.Zip;

        //        headQuartersAddress.Address1 = model.HeadQuartersAddress.Address1;
        //        headQuartersAddress.Address2 = model.HeadQuartersAddress.Address2;
        //        headQuartersAddress.City = model.HeadQuartersAddress.City;
        //        headQuartersAddress.Comments = model.HeadQuartersAddress.Comments;
        //        headQuartersAddress.ContactName = model.HeadQuartersAddress.ContactName;
        //        headQuartersAddress.ContactTitle = model.HeadQuartersAddress.ContactTitle;
        //        headQuartersAddress.Country = model.HeadQuartersAddress.Country;
        //        headQuartersAddress.EMail1 = model.HeadQuartersAddress.EMail1;
        //        headQuartersAddress.CustomerId = model.HeadQuartersAddress.CustomerId;
        //        headQuartersAddress.DateUpdated = DateTime.Now;
        //        headQuartersAddress.Fax = model.HeadQuartersAddress.Fax;
        //        headQuartersAddress.Phone = model.HeadQuartersAddress.Phone;
        //        headQuartersAddress.State_Province = model.HeadQuartersAddress.StateProvince;
        //        headQuartersAddress.WebSite = model.HeadQuartersAddress.WebSite;
        //        headQuartersAddress.Zip = model.HeadQuartersAddress.Zip;
                
        //        if (model.CarrrierAccount != null)
        //        {
        //            carrierAccount.AccountNumber = model.CarrrierAccount.AccountNumber;
        //            carrierAccount.InboundAccountNumber = model.CarrrierAccount.InboundAccountNumber;
        //        }

        //        DC.SaveChanges();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at UpdateClientProfile" + ex);
        //        return false;
        //    }
        //}

        public DataSourceResult GetAppUsers(Kendo.DynamicLinq.DataSourceRequest request)
        {
            DataSourceResult result = new DataSourceResult();
            try
            {
                if (request.Sort == null || request.Sort.Count() == 0)
                {
                    request.Sort = new List<Sort>() { new Sort() { Field = "UserId", Dir = "asc" } };
                }

                result = (from x in UserAccountsRepository.DC.ProfileViews
                          select x).ToDataSourceResult<ProfileView>(request);

                List<ProfileView> response = ((List<ProfileView>)result.Data).ToList();

                var list = response.Select(x => new UserModel()
                {
                    UserName = x.UserName,
                    CustomerId = x.CustomerId,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Title = x.Title,
                    Address1 = x.Address1,
                    Address2 = x.Address2,
                    City = x.City,
                    State = x.State,
                    Zip = x.Zip,
                    Country = x.Country,
                    Phone = x.Phone,
                    Fax = x.Fax,
                    Email = x.Email,
                    Comments = x.Comments,
                    DateUpdated = x.DateUpdated.Value,
                    DateCreated = x.DateCreated.Value,
                    Active = x.IsApproved
                }).ToList();

                foreach (var x in list)
                {
                    x.UserRoles = Roles.GetRolesForUser(x.UserName);
                    //if (x.CustomerId.HasValue)
                        //x.CustomerName = UserAccountsRepository.DC.Customers.Where(y => y.CustomerId == x.CustomerId).FirstOrDefault().CustomerName;
                }

                result.Data = list;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetShoppingCarts", ex);
                return null;
            }
            return result;
        }

        //public CustomerModel GetCustomerById(int customerId)
        //{
        //    try
        //    {
        //        Customer cutomer = LS.Data.UserAccountsRepository.DC.Customers.Where(x => x.CustomerId == customerId).FirstOrDefault();
        //        CustomerModel model = CustomerModel.GetModel(cutomer);
        //        return model;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at GetCustomerById", ex);
        //        throw ex;
        //    }
        //}
    }
}
