//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_USER_WORKFLOW
    {
        public string TB_USER_ID { get; set; }
        public Nullable<bool> NEW { get; set; }
        public Nullable<bool> UPDATED { get; set; }
        public Nullable<bool> REVIEW_PROCESS { get; set; }
        public Nullable<bool> SUBMIT { get; set; }
        public Nullable<bool> APPROVE { get; set; }
    
        public virtual TB_USER TB_USER { get; set; }
    }
}
