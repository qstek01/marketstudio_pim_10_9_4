﻿using LS.Data.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using System.ComponentModel;


namespace LS.Data.Model.Accounts
{
    public enum UserRole
    {
        //[Description("Admin")]
        //Admin = 1,
        //[Description("AdminNormalUser")]
        //AdminNormalUser = 2,
        //[Description("CustomerAdmin")]
        //CustomerAdmin = 3,
        //[Description("CustomerNormalUser")]
        //CustomerNormalUser = 4
    }


    public class ResetPasswordModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New Password:")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm New Password:")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string ReturnMessage { get; set; }
    }

    public class ChangePasswordModel
    {
        //[Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Old Password:")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New Password:")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm New Password:")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

       

    }

    public class LogOnModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public partial class UserInputModel
    {
        public UserModel UserModel { get; set; }
        public PageSearchCriteria PageSearchCriteria { get; set; }
        public UserFilterCriteria FilterCriteria { get; set; }
    }



    public class UserModel
    {
        //[Required]
        //[Display(Name = "User name")]
        //[RegularExpression(@"(\S)+", ErrorMessage = "White space is not allowed")]
        public string UserName { get; set; }
        
        //[RegularExpression(@"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$", ErrorMessage = "Invalid Email (ex:example@example.com)")]
        //[RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Invalid Email (ex:example@example.com)")]
        [Required]
        //[EmailAddress(ErrorMessage = "Invalid Email Address(ex:example@example.com)")]
        //[DataType(DataType.EmailAddress)]

        [RegularExpression(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
           + "@"
           + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$", ErrorMessage = "Invalid Email Address(ex:example@example.com)")]
        [Display(Name = "Email address")]
        [System.Web.Mvc.Remote("IsEmailAlreadyExists", "App")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Entered Password & Confirm Password do not match")]
        public string ConfirmPassword { get; set; }

        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "No Special Characters ")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "No Special Characters ")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

       
      
        

        [Display(Name = "Job Title")]
        public string Title { get; set; }

        
        //[RegularExpression(@"^\d\d\d\-\d\d\d\-\d\d\d\d$", ErrorMessage = "Entered phone format is not valid.ex:123-123-1234")]
        //@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$"
       // [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        [RegularExpression(@"[\d\s\-\(\)\.]+$", ErrorMessage = "Entered phone format is not valid.")]
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }

        ////[RegularExpression(@"^\d\d\d\-\d\d\d\-\d\d\d\d$", ErrorMessage = "Entered phone format is not valid.ex:123-123-1234")]
        // [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[\-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        //[RegularExpression(@"[\d\s\-\(\)]+$", ErrorMessage = "Entered phone format is not valid.")]
        [Display(Name = "Fax")]
        public virtual string Fax { get; set; }

        [Display(Name = "Address")]
        public virtual string Address1 { get; set; }

        [Display(Name = "Address2")]
        public virtual string Address2 { get; set; }

        [Display(Name = "City")]
        public virtual string City { get; set; }

  [Required(AllowEmptyStrings = false)]
 [DisplayName("State")]
    public virtual string State { get; set; }

        //[RegularExpression(@"^(?!00000)[0-9]{5,6}$", ErrorMessage = "Invalid Zip")]
        [RegularExpression(@"^[a-zA-Z0-9]{5,6}$", ErrorMessage = "Invalid Zip")]
        [Display(Name = "Zip")]
        public virtual string Zip { get; set; }

         [Required]
        [Display(Name = "Country")]
        public virtual string Country { get; set; }

        [Display(Name = "Comments")]
        public virtual string Comments { get; set; }

        public virtual bool Active
        {
            get;
            set;
        }

        public virtual string StatusText
        {
            get
            {
                if (Active)
                {
                    return "Active";
                }
                else if (!Active)
                {
                    return "In Active";
                }

                return "";
            }

        }


        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "No Special Characters ")]
        [Display(Name = "Company Name")]
        public string CompanyName  { get; set; }

        [Display(Name = "WebSite")]
        public string WebSite { get; set; }

        public int? CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        public DateTime DateUpdated { get; set; }

        public DateTime DateCreated { get; set; }
       
        [Display(Name = "Roles")]
        public string UserRole
        {
            get
            {
                string rolesString = "";

                if (UserName != null)
                {
                    string[] currentRoles = Roles.GetRolesForUser(UserName);
                    if (currentRoles.Length > 0)
                    {
                        foreach (string role in currentRoles)
                        {
                            rolesString += "," + role;
                        }
                        return rolesString.Substring(1);
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        
        [Display(Name = "Roles")]
        public string[] UserRoles { get; set; }

        public string FullName 
        {
            get 
            {
                return FirstName + " " + LastName;
            } 
        }

        public string FullAddress
        {
            get
            {
                string stateZip = (!string.IsNullOrEmpty(State) ? State + (!string.IsNullOrEmpty(Zip) ? " - " + Zip : "") : Zip);
                return Address1 +
                    (!string.IsNullOrEmpty(Address2) ? " " + Address2 : "") +
                    (!string.IsNullOrEmpty(City) ? ", " + City : "") +
                    (!string.IsNullOrEmpty(stateZip) ? ", " + stateZip : "") +
                    (!string.IsNullOrEmpty(Country) ? ", " + Country : "");
            }
        }

        public int PlanId { get; set; }
     
       
    }

    
    public class UserWebModel
    {
       
        public int? ActiveUser { get; set; }

      
        [Required]
       

        [RegularExpression(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
           + "@"
           + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$", ErrorMessage = "Invalid Email Address(ex:example@example.com)")]
        [Display(Name = "Email address")]
        [System.Web.Mvc.Remote("IsEmailAlreadyExists", "App")]
        public string Email { get; set; }


        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "No Special Characters ")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "No Special Characters ")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }





        
        //[RegularExpression(@"[\d\s\-\(\)\.]+$", ErrorMessage = "Entered phone format is not valid.")]
        //[Display(Name = "Phone Number")]
        //public string Phone { get; set; }
        //[RegularExpression(@"[\d\s\-\(\)\.]+$", ErrorMessage = "Entered phone format is not valid.")]
        //[Display(Name = "Mobile Number")]
        //public string Mobile { get; set; }

       

        //[Display(Name = "Skype Id")]
        //public virtual string Skype { get; set; }

        //[Display(Name = "WhatsApp")]
        //public virtual string WhatsApp { get; set; }
        //[Required]
        //[Display(Name = "Designation")]
        //public virtual string Designation { get; set; }

        
        [DisplayName("Additional Information")]
        public virtual string Notes { get; set; }

      

       


        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "No Special Characters ")]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

     

        public Nullable<System.DateTime> DateUpdated { get; set; }

        public Nullable<System.DateTime> DateCreated { get; set; }


        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

       

      


    }

    public partial class UserFilterCriteria
    {
        public virtual string UserName
        {
            get;
            set;
        }

        public virtual string FirstName
        {
            get;
            set;
        }

        public virtual string Title
        {
            get;
            set;
        }

        public virtual string Phone
        {
            get;
            set;
        }

        public virtual string FullAddress
        {
            get;
            set;
        }

        public virtual string Email
        {
            get;
            set;
        }

        public virtual bool InActive
        {
            get;
            set;
        }
    }

    public class ForgorPasswordModel
    {
      //  [Required]
       // [Display(Name = "User name")]
     //   public string UserName { get; set; }
        
       
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address(ex:example@example.com)")]
      // [DataType(DataType.EmailAddress)]

       [RegularExpression(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
           + "@"
           + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$", ErrorMessage = "Invalid Email Address(ex:example@example.com)")]
        //[RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
        //                    @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
        //                    @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
        //                    ErrorMessage = "Invalid Email Address(ex:example@example.com)")]
        [Display(Name = "Email address")]
               public string Email { get; set; }

        public string ReturnMessage { get; set; }
    }

    public class UserRoleModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

      //  [Required]
        //[RegularExpression(@"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$", ErrorMessage = "Invalid Email (ex:example@example.com)")]
        //[RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Invalid Email (ex:example@example.com)")]

        [EmailAddress(ErrorMessage = "Invalid Email Address(ex:example@example.com)")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

       [Required(ErrorMessage = "Password is required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Required confirm password")]
        [Compare("Password", ErrorMessage = "Entered Password & Confirm Password do not match")]
        public string ConfirmPassword { get; set; }

        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "No Special Characters ")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "No Special Characters ")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Job Title")]
        public string Title { get; set; }

        [Required]
       // [RegularExpression(@"^\d\d\d\-\d\d\d\-\d\d\d\d$", ErrorMessage = "Entered phone format is not valid.ex:123-123-1234")]
        //[RegularExpression(@"[\d\s\-\(\)\.]+$", ErrorMessage = "Entered phone format is not valid.")]
        [RegularExpression(@"^([0-9]{10})$+", ErrorMessage = "Entered phone format is not valid.")]
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }

       // [RegularExpression(@"^\d\d\d\-\d\d\d\-\d\d\d\d$", ErrorMessage = "Entered phone format is not valid.ex:123-123-1234")]
        [Display(Name = "Fax")]
        public virtual string Fax { get; set; }

        [Display(Name = "Address")]
        public virtual string Address1 { get; set; }

        [Display(Name = "Address2")]
        public virtual string Address2 { get; set; }

        [Display(Name = "City")]
        public virtual string City { get; set; }

        [Display(Name = "State")]
        public virtual string State { get; set; }

        [Display(Name = "Zip")]
        public virtual string Zip { get; set; }

        [Display(Name = "Country")]
        public virtual string Country { get; set; }

        [Display(Name = "Comments")]
        public virtual string Comments { get; set; }

        public virtual bool Active
        {
            get;
            set;
        }

        public virtual string StatusText
        {
            get
            {
                if (Active)
                {
                    return "Active";
                }
                else if (!Active)
                {
                    return "In Active";
                }

                return "";
            }

        }

        public int? CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        public DateTime DateUpdated { get; set; }

        public DateTime DateCreated { get; set; }

        [Display(Name = "Roles")]
        public string UserRole
        {
            get
            {
                string rolesString = "";

                if (UserName != null)
                {
                    string[] currentRoles = Roles.GetRolesForUser(UserName);
                    if (currentRoles.Length > 0)
                    {
                        foreach (string role in currentRoles)
                        {
                            rolesString += "," + role;
                        }
                        return rolesString.Substring(1);
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
        }


        [Display(Name = "Roles")]
        public string[] UserRoles { get; set; }

        public string FullAddress
        {
            get
            {
                string stateZip = (!string.IsNullOrEmpty(State) ? State + (!string.IsNullOrEmpty(Zip) ? " - " + Zip : "") : Zip);
                return Address1 +
                    (!string.IsNullOrEmpty(Address2) ? " " + Address2 : "") +
                    (!string.IsNullOrEmpty(City) ? ", " + City : "") +
                    (!string.IsNullOrEmpty(stateZip) ? ", " + stateZip : "") +
                    (!string.IsNullOrEmpty(Country) ? ", " + Country : "");
            }
        }
    }

    public class UserProfileModel
    {
        [Display(Name = "Carrier")]
        public string Carrier { get; set; }

        [Required]
        [Display(Name = "User name")]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Invalid Format (only Characters")]
        public string UserName { get; set; }

        [Required]
        //[RegularExpression(@"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$", ErrorMessage = "Invalid Email (ex:example@example.com)")]
        //[RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Invalid Email (ex:example@example.com)")]

        [EmailAddress(ErrorMessage = "Invalid Email Address(ex:example@example.com)")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "Invalid Format ")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

       [RegularExpression(@"[\d\s\-\(\)\.]+$", ErrorMessage = "Entered phone format is not valid.")]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Required]
        [RegularExpression(@"^[\w -' ]+$", ErrorMessage = "Invalid Format ")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Registration / BN #")]
        public string RegistrationNo { get; set; }

        public virtual string Status
        {
            get;
            set;
        }

        public virtual string StatusText
        {
            get
            {
                if (Status == "A")
                {
                    return "Active";
                }
                else if (Status == "IA")
                {
                    return "In Active";
                }
                
                return "";
            }

        }

        public virtual bool InActive
        {
            get;
            set;
        }

        [Display(Name = "Location")]
        public string Location { get; set; }
    }


    public class Organisationlist
    {
        public string code { get; set; }
        public string organisationName { get; set; }
    }
    public class OrganizationName
    {
        public string code { get; set; }
        public string organisationName { get; set; }
    }
    public class GroupNameload
    {
        public string code { get; set; }
        public string roleName { get; set; }
    }
    public class SaveuserWebDetails
    {
        public string organisation { get; set; }
        public string role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Skype { get; set; }
        public string WhatsApp { get; set; }
        public string Company { get; set; }
        public string Designation { get; set; }
        public string Notes { get; set; }
        public bool active { get; set; }
    }


    public class RoleModel
    {
        [Required]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Invalid Format (only Characters")]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }
    }

    public class LineCardData
    {
        public string CATEGORY_ID { get; set; }
        public int FAMILY_ID { get; set; }
        public int PRODUCT_ID { get; set; }
        public int USER_ID { get; set; }
        public string IMAGE { get; set; }
        public int TEMPLATE_ID { get; set; }
        public string ITEM_NO { get; set; }
    }


    public class RowData
    {
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string CONTACT_EMAIL { get; set; }
        public Nullable<int> USER_ID { get; set; }
        //public string PHONE { get; set; }
        //public string MOBILE { get; set; }
        public string COMPANY_NAME { get; set; }
        public string NOTES { get; set; }
        public bool STATUS { get; set; }
        public string role { get; set; }
        public int ORGID { get; set; }
        public int TYPE { get; set; }

    }
    public class UpdateGroupNameForUsers
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
    public class RemoveGroupNameForUsers
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
