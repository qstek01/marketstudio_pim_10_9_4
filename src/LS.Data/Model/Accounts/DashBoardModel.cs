﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LS.Data.Model.Common;

namespace LS.Data.Model.CatalogSectionModels
{
    public class DashBoardModel
    {
        public string Data
        {
            get;
            set;
        }
        public List<PTColumns> Columns
        {
            get;
            set;
        }
        public DashBoardModel()
        {

            Data = "";
            Columns = new List<PTColumns>();
        }
    }
    public class PTColumns
    {
        public string Caption { get; set; }
        public string ColumnName { get; set; }

    }

}
