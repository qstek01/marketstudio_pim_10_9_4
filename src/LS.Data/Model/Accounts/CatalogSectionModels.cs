﻿using LS.Data.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using System.ComponentModel;

namespace LS.Data.Model.CatalogSectionModels
{
    public class CatalogSectionModels
    {
        public int CATALOG_ID { get; set; }
        public string CATEGORY_ID { get; set; }
        public int FAMILY_ID { get; set; }
        public int PRODUCT_ID { get; set; }
        public int SORT_ORDER { get; set; }
        public bool PUBLISH { get; set; }
        public string Part_No_ { get; set; }
        public string Size { get; set; }
        public string Qty_ { get; set; }
        public string Dia { get; set; }
        public string Overall_Length { get; set; }
        public string Description { get; set; }
        public string CATALOG_NAME { get; set; }
        public string CATEGORY_NAME { get; set; }
        
    }
}
