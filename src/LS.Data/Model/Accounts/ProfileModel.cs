﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.ComponentModel.DataAnnotations;

namespace LS.Data.Model.Accounts
{
    public class ProfileModel : ProfileBase
    {
        [Display(Name = "CustomerId")]
        public virtual int? CustomerId
        {
            get
            {
                return (int?)this.GetPropertyValue("CustomerId");
            }
            set
            {
                this.SetPropertyValue("CustomerId", value);
            }
        }

        [Display(Name = "First Name")]
        public virtual string FirstName
        {
            get
            {
                return (this.GetPropertyValue("FirstName").ToString());
            }
            set
            {
                this.SetPropertyValue("FirstName", value);
            }
        }

        [Display(Name = "Last Name")]
        public virtual string LastName
        {
            get
            {
                return (this.GetPropertyValue("LastName").ToString());
            }
            set
            {
                this.SetPropertyValue("LastName", value);
            }
        }

        [Display(Name = "Title")]
        public virtual string Title
        {
            get
            {
                return (this.GetPropertyValue("Title").ToString());
            }
            set
            {
                this.SetPropertyValue("Title", value);
            }
        }

        [Display(Name = "Address1")]
        public virtual string Address1
        {
            get
            {
                return (this.GetPropertyValue("Address1").ToString());
            }
            set
            {
                this.SetPropertyValue("Address1", value);
            }
        }

        [Display(Name = "Address2")]
        public virtual string Address2
        {
            get
            {
                return (this.GetPropertyValue("Address2").ToString());
            }
            set
            {
                this.SetPropertyValue("Address2", value);
            }
        }

        [Display(Name = "City")]
        public virtual string City
        {
            get
            {
                return (this.GetPropertyValue("City").ToString());
            }
            set
            {
                this.SetPropertyValue("City", value);
            }
        }

        [Display(Name = "State")]
        public virtual string State
        {
            get
            {
                return (this.GetPropertyValue("State").ToString());
            }
            set
            {
                this.SetPropertyValue("State", value);
            }
        }

        [Display(Name = "Zip")]
        public virtual string Zip
        {
            get
            {
                return (this.GetPropertyValue("Zip").ToString());
            }
            set
            {
                this.SetPropertyValue("Zip", value);
            }
        }

        [Display(Name = "Country")]
        public virtual string Country
        {
            get
            {
                return (this.GetPropertyValue("Country").ToString());
            }
            set
            {
                this.SetPropertyValue("Country", value);
            }
        }

        [Display(Name = "Phone")]
        public virtual string Phone
        {
            get
            {
                return (this.GetPropertyValue("Phone").ToString());
            }
            set
            {
                this.SetPropertyValue("Phone", value);
            }
        }



        [Display(Name = "Fax")]
        public virtual string Fax
        {
            get
            {
                return (this.GetPropertyValue("Fax").ToString());
            }
            set
            {
                this.SetPropertyValue("Fax", value);
            }
        }

        [Display(Name = "EMail")]
        public virtual string EMail
        {
            get
            {
                return (this.GetPropertyValue("EMail").ToString());
            }
            set
            {
                this.SetPropertyValue("EMail", value);
            }
        }

        [Display(Name = "Comments")]
        public virtual string Comments
        {
            get
            {
                return (this.GetPropertyValue("Comments").ToString());
            }
            set
            {
                this.SetPropertyValue("Comments", value);
            }
        }


        [Display(Name = "DateUpdated")]
        public virtual DateTime DateUpdated
        {
            get
            {
                return ((DateTime)this.GetPropertyValue("DateUpdated"));
            }
            set
            {
                this.SetPropertyValue("DateUpdated", value);
            }
        }

        [Display(Name = "DateCreated")]
        public virtual DateTime DateCreated
        {
            get
            {
                return ((DateTime)this.GetPropertyValue("DateCreated"));
            }
            set
            {
                this.SetPropertyValue("DateCreated", value);
            }
        }

        public static ProfileModel GetProfile(string username)
        {
            return Create(username) as ProfileModel;
        }
        
    }
}