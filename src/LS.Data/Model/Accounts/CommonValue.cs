﻿using LS.Data.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using System.ComponentModel;

namespace LS.Data.Model.CommonValue
{
    public class CommonValue
    {
        public int id { get; set; }
        public int FAMILY_ID { get; set; }
        public string FAMILY_NAME { get; set; }
        public int CATALOG_ID { get; set; }
        public string CATALOG_NAME { get; set; }
        public bool hasChildren { get; set; }
        public int SORT_ORDER { get; set; }
        public string spriteCssClass { get; set; }
    }
}
