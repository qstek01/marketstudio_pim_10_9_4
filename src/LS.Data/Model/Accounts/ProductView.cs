﻿using LS.Data.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using System.ComponentModel;

namespace LS.Data.Model.ProductView
{
    public class ProductView
    {
        public string id { get; set; }
        public string CATEGORY_ID { get; set; }
        public string CATEGORY_NAME { get; set; }
        public int CATALOG_ID { get; set; }
        public string CATALOG_NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string VERSION { get; set; }
        public bool Children { get; set; }
        public bool hasChildren { get; set; }
        public int SORT_ORDER { get; set; }
        public bool CategoryIdinNavigator { get; set; }
        public bool FamilyandRelatedFamily { get; set; }
        public string spriteCssClass { get; set; }
        public bool @checked { get; set; }
        public Nullable<System.DateTime> XML_Genereted_Date { get; set; }
        public System.DateTime Family_Modifed_Date { get; set; }
        public System.DateTime Category_Modifed_Date { get; set; }
        public bool check { get; set; }
        public bool encoded { get; set; }
        public string CATEGORY_SHORT { get; set; }
        public int CUSTOMER_ID { get; set; }

    }
}
