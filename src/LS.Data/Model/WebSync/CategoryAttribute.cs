﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model.WebSync
{
   public class CategoryAttribute
    {
       
     
        public string SHORT_DESC { get; set; }
        public string IMAGE_FILE { get; set; }
        public string IMAGE_TYPE { get; set; }
        public string IMAGE_NAME { get; set; }
        public string IMAGE_NAME2 { get; set; }
        public string IMAGE_FILE2 { get; set; }
        public string IMAGE_TYPE2 { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD1 { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD2 { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD3 { get; set; }
        public string CUSTOM_TEXT_FIELD1 { get; set; }
        public string CUSTOM_TEXT_FIELD2 { get; set; }
        public string CUSTOM_TEXT_FIELD3 { get; set; }
     
    }
}
