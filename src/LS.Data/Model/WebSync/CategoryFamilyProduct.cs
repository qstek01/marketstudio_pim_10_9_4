﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model.WebSync
{
    public partial class CategoryFamilyProduct 
    {
        //public int CatalogId { get; set; }
        public string CATEGORY_ID { get; set; }
        public DateTime MODIFIED_DATE { get; set; }
        public int FAMILY_ID { get; set; }
        public DateTime FMODIFIED_DATE { get; set; }
        public int PRODUCT_ID { get; set; }
        public DateTime PMODIFIED_DATE { get; set; }
        public int SUBPRODUCTID { get; set; }
       public DateTime SPMODIFIED_DATE { get; set; }
    }
    public class ListCategoryFamilyProduct
    {
        //public int CatalogId { get; set; }
        public List<string> CATEGORY_ID { get; set; }
        public List<DateTime> MODIFIED_DATE { get; set; }
        public List<int> FAMILY_ID { get; set; }
        public List<DateTime> FMODIFIED_DATE { get; set; }
        public List<int> PRODUCT_ID { get; set; }
        public List<DateTime> PMODIFIED_DATE { get; set; }
        public int SUBPRODUCTID { get; set; }
        public DateTime SPMODIFIED_DATE { get; set; }
    }
}
