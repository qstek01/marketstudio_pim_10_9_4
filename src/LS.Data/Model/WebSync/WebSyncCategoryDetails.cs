﻿using LS.Data.Model.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using System.ComponentModel;


namespace LS.Data.Model.WebSync
{
    public class WebSyncCategoryDetails
    {
        public string id { get; set; }
        public string CATEGORY_ID { get; set; }
        public string CATEGORY_NAME { get; set; }
        public string Parent_Category { get; set; }
        
        public int CATALOG_ID { get; set; }
        public string CATALOG_NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string VERSION { get; set; }
        public bool hasChildren { get; set; }
        public int SORT_ORDER { get; set; }
        public bool CategoryIdinNavigator { get; set; }
        public bool FamilyandRelatedFamily { get; set; }
        public string spriteCssClass { get; set; }
        public bool @checked { get; set; }
        public bool encoded { get; set; }
        public string CATEGORY_SHORT { get; set; }
        public int CUSTOMER_ID { get; set; }
        public DateTime Modify_Date { get; set; }
        public string STATUS { get; set; }
    }
}
