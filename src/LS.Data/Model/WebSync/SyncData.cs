﻿using System;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using System.Xml;


namespace LS.Data.Model.WebSync
{
   public class SyncData
    {
       public string sessionId { get; set; }
       public string createdUser { get; set; }
       public string updateType { get; set; }
       public string jobType { get; set; }
       public string jobName { get; set; }
       public XmlDocument xmldata { get; set; }

      // public string conStr = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;



     
    }
    public class ListOfParameter
    {
        public string categoryId { get; set; }
        public string familyId { get; set; }
        public string productId { get; set; }
        public string urlBase { get; set; }
        public string sessionId { get; set; }
        public string send { get; set; }
        public string updateType { get; set; }
        public string jobType { get; set; }
        public string user { get; set; }
        public string jobName { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public int Catalog_Id { get; set; }
        public string subProductId { get; set; }
        public DateTime endTime { get; set; }
        public string WebSyncOthers { get; set; }
        public string IndesignTemp { get; set; }
    }


}
