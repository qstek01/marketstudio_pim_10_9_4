﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class FamilyImgAttachment
    {
        public int FAMILY_ID { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public string FAMILY_NAME { get; set; }
        public string CATEGORY_ID { get; set; }
        public string STATUS { get; set; }
        public string PUBLISH { get; set; }
        public string PUBLISH2PRINT { get; set; }
        public int PARENT_FAMILY_ID { get; set; }
        public string FAMILYIMG_NAME { get; set; }
        public string FAMILYIMG_ATTRIBUTE { get; set; }
        public string OBJECT_NAME { get; set; }
    }
}
