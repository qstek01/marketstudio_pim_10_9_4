﻿using System;

namespace LS.Data.Model.Common
{
    public class PageSearchCriteria
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string sortDir { get; set; }
        public string sortedBy { get; set; }
   }
}