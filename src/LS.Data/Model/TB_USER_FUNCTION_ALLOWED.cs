﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class TB_USER_FUNCTION_ALLOWED
    {
        public int FUNCTION_ID { get; set; }
        public string FUNCTION_NAME { get; set; }
        public int ROLE_ID { get; set; }
        public int PLAN_ID { get; set; }
        public Nullable<bool> ACTION_VIEW { get; set; }
        public Nullable<bool> ACTION_MODIFY { get; set; }
        public Nullable<bool> ACTION_ADD { get; set; }
        public Nullable<bool> ACTION_REMOVE { get; set; }
    }
}
