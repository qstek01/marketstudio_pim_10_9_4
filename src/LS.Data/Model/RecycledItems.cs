﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class RecycledItems
    
    {
        
        public int RESTORE { get; set; }
        public int XML_ID { get; set; }
        public string ROOT_CATEGORY_ID { get; set; }
        public string CATEGORY_SHORT { get; set; }
        public string REMOVED_CATEGORY_NAME { get; set; }
        public string REMOVED_FAMILY { get; set; }
        public string REMOVED_SUB_FAMILY { get; set; }
        public string REMOVED_PRODUCT { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public string DELETED_USER { get; set; }
        public string ACTION_DELETED_DATEADD { get; set; }
        public int CATALOG_ID { get; set; }
        public int FAMILY_ID { get; set; }
        public int SUBFAMILY_ID { get; set; }
        public int PRODUCT_ID { get; set; }
        public int SUBPRODUCT_ID { get; set; }
        public string RECYCLE_TYPE { get; set; }
    }
}
