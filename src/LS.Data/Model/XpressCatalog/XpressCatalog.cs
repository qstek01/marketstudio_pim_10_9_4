﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using log4net;

namespace LS.Data.Model.XpressCatalog
{
  public class XpressCatalog
    {
      static ILog _logger = LogManager.GetLogger(typeof(XpressCatalog));
        CSEntities _dbcontext = new CSEntities();
        public string XpressConfig(int projectId, string projectName,string category_Ids,string family_Ids, string comment, int catalogId,
            string catalogName, string type, string pagesize, string widthValue, string heightValue, string segwidth, string segheight, string leftmrgn,
            string rightmrgn, string topmrgn, string bottommrgn, string cols, string colswidth, string colsgap, string pageformat,
            string cvrpg, string tocpg, string cattoc, string gindex, string catindex, string indexcols, JArray model)
        {
            try
            {
                var functionAlloweditem = (model[0]).Select(x => x).ToList();
                string systemAttributes = string.Empty;
                string customAttributes = string.Empty;
                string multipleAttributes = string.Empty;
                string referenceAttributes = string.Empty;
                foreach (var item in functionAlloweditem)
                {
                    if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 0 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 2)
                    { systemAttributes += item["ATTRIBUTE_NAME"] + ","; }
                   
                    else
                    {
                        customAttributes += Convert.ToInt32(item["ATTRIBUTE_ID"]) == 0
                            ? item["ATTRIBUTE_NAME"] + ","
                            : item["ATTRIBUTE_ID"] + ",";
                    }
                    if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 14 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 14)
                    { multipleAttributes += item["ATTRIBUTE_NAME"] + ","; }
                    if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) ==15 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 15)
                    { referenceAttributes += item["ATTRIBUTE_NAME"] + ","; }
                }
                systemAttributes = systemAttributes.Remove(systemAttributes.Length - 1);
                customAttributes = customAttributes.Remove(customAttributes.Length - 1);
                var stringwriter = new StringWriter();
                var xmlTextWriter = new XmlTextWriter(stringwriter);
                xmlTextWriter.WriteStartDocument();
                xmlTextWriter.WriteStartElement("PDF_PROJECT");
                xmlTextWriter.WriteStartElement("PDF_PROJECT_SETTINGS");
                xmlTextWriter.WriteElementString("PROJECT_ID",Convert.ToString( projectId));
                xmlTextWriter.WriteElementString("PROJECT_NAME", projectName);
                xmlTextWriter.WriteElementString("COMMENT", comment);
                xmlTextWriter.WriteElementString("CATALOG_ID", Convert.ToString(catalogId));
                xmlTextWriter.WriteElementString("CATALOG_NAME", catalogName);
                xmlTextWriter.WriteElementString("PUBLICATION_TYPE", type);
                xmlTextWriter.WriteElementString("PAPER_SIZE", pagesize);
                xmlTextWriter.WriteElementString("PAPER_WIDTH", widthValue);
                xmlTextWriter.WriteElementString("PAPER_HEIGHT", heightValue);
                xmlTextWriter.WriteElementString("SEGMENT_PER_WIDTH", segwidth);
                xmlTextWriter.WriteElementString("SEGMENT_PER_HEIGHT", segheight);
                xmlTextWriter.WriteElementString("LEFT_MARGIN", leftmrgn);
                xmlTextWriter.WriteElementString("RIGHT_MARGIN", rightmrgn);
                xmlTextWriter.WriteElementString("TOP_MARGIN", topmrgn);
                xmlTextWriter.WriteElementString("BOTTOM_MARGIN", bottommrgn);
                xmlTextWriter.WriteElementString("PAGE_COLUMNS", cols);
                xmlTextWriter.WriteElementString("PAGE_COLUMN_WIDTH", colswidth);
                xmlTextWriter.WriteElementString("PAGE_COLUMN_GAP", colsgap);
                xmlTextWriter.WriteElementString("ORIENTATION",pageformat);
                xmlTextWriter.WriteElementString("COVER_PAGE", cvrpg);
                xmlTextWriter.WriteElementString("CONTENT_PAGE", tocpg);
                xmlTextWriter.WriteElementString("CONTENT_PAGE_WITH_CATEGORY_FAMILY", cattoc);
                xmlTextWriter.WriteElementString("INDEX_PAGE", gindex);
                xmlTextWriter.WriteElementString("INDEX_PAGE_WITH_CATALOG_ITEM_NO", catindex);
                xmlTextWriter.WriteElementString("INDEXPAGE_COLUMNS", indexcols);
                xmlTextWriter.WriteElementString("SYSTEM_ATTRIBUTE", systemAttributes);
                xmlTextWriter.WriteElementString("CUSTOM_ATTRIBUTE", customAttributes);
                xmlTextWriter.WriteElementString("MULTIPLE_TABLES", multipleAttributes);
                xmlTextWriter.WriteElementString("REFERENCE_TABLES", referenceAttributes);


                xmlTextWriter.WriteElementString("CUSTOM_CATEGORY_ID", category_Ids);
                xmlTextWriter.WriteElementString("CUSTOM_FAMILY_ID", family_Ids);


                xmlTextWriter.WriteEndElement();
                xmlTextWriter.WriteEndElement();
                xmlTextWriter.WriteEndDocument();
                xmlTextWriter.Flush();
                var docSave = new XmlDocument();
                docSave.LoadXml(stringwriter.ToString());
               
                return stringwriter.ToString();
            }
            catch (Exception objexception)
            {
               // Logger.Error("Error at Export : ExportConfig", objexception);
                return string.Empty;

            }
        }
  
      public List<PDF_PROJECT_SETTINGS> ProjectXmlDeserializefunction(string pdfDetails)
        {
            try
            {
                if (!string.IsNullOrEmpty(pdfDetails))
                {
                    var deserializer = new XmlSerializer(typeof(PDF_PROJECT));
                    TextReader reader = new StringReader(pdfDetails);
                    object obj = deserializer.Deserialize(reader);
                    var xmlData = (PDF_PROJECT)obj;
                    return xmlData.objprojectSettings;
                }
                return null;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalog :  XmlDeserializefunction", objException);
                return null;
            }
        }
      public string OpenXpressConfig(int projectId, JArray model)
      {
          try
          {
              var functionAlloweditem = (model[0]).Select(x => x).ToList();
              string systemAttributes = string.Empty;
              string customAttributes = string.Empty;
              string multipleAttributes = string.Empty;
              string referenceAttributes = string.Empty;
              foreach (var item in functionAlloweditem)
              {
                  if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 0 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 2)
                  { systemAttributes += item["ATTRIBUTE_NAME"] + ","; }

                  else
                  {
                      customAttributes += Convert.ToInt32(item["ATTRIBUTE_ID"]) == 0
                          ? item["ATTRIBUTE_NAME"] + ","
                          : item["ATTRIBUTE_ID"] + ",";
                  }
                  if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 14 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 14)
                  { multipleAttributes += item["ATTRIBUTE_NAME"] + ","; }
                  if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 15 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 15)
                  { referenceAttributes += item["ATTRIBUTE_NAME"] + ","; }
              }
              systemAttributes = systemAttributes.Remove(systemAttributes.Length - 1);
              customAttributes = customAttributes.Remove(customAttributes.Length - 1);

              TB_PROJECT projectdetails = _dbcontext.TB_PROJECT.Find(projectId);
              var pdfDetails = projectdetails.XPRESSCATALOG_CONFIG;
              List<PDF_PROJECT_SETTINGS> objprojectSettings = ProjectXmlDeserializefunction(pdfDetails);
              if (objprojectSettings.Count > 0)
              {
                  var stringwriter = new StringWriter();
                  var xmlTextWriter = new XmlTextWriter(stringwriter);
                  xmlTextWriter.WriteStartDocument();
                  xmlTextWriter.WriteStartElement("PDF_PROJECT");
                  xmlTextWriter.WriteStartElement("PDF_PROJECT_SETTINGS");
                  xmlTextWriter.WriteElementString("PROJECT_ID", Convert.ToString(projectId));
                  xmlTextWriter.WriteElementString("PROJECT_NAME", objprojectSettings[0].PROJECT_NAME);
                  xmlTextWriter.WriteElementString("COMMENT", objprojectSettings[0].COMMENT);
                  xmlTextWriter.WriteElementString("CATALOG_ID", Convert.ToString(objprojectSettings[0].CATALOG_ID));
                  xmlTextWriter.WriteElementString("CATALOG_NAME",objprojectSettings[0].CATALOG_NAME);
                  xmlTextWriter.WriteElementString("PUBLICATION_TYPE",objprojectSettings[0].PUBLICATION_TYPE);
                  xmlTextWriter.WriteElementString("PAPER_SIZE", objprojectSettings[0].PAPER_SIZE);
                  xmlTextWriter.WriteElementString("PAPER_WIDTH", objprojectSettings[0].PAPER_WIDTH);
                  xmlTextWriter.WriteElementString("PAPER_HEIGHT", objprojectSettings[0].PAPER_HEIGHT);
                  xmlTextWriter.WriteElementString("SEGMENT_PER_WIDTH", objprojectSettings[0].SEGMENT_PER_WIDTH);
                  xmlTextWriter.WriteElementString("SEGMENT_PER_HEIGHT", objprojectSettings[0].SEGMENT_PER_HEIGHT);
                  xmlTextWriter.WriteElementString("LEFT_MARGIN", objprojectSettings[0].LEFT_MARGIN);
                  xmlTextWriter.WriteElementString("RIGHT_MARGIN", objprojectSettings[0].RIGHT_MARGIN);
                  xmlTextWriter.WriteElementString("TOP_MARGIN", objprojectSettings[0].TOP_MARGIN);
                  xmlTextWriter.WriteElementString("BOTTOM_MARGIN", objprojectSettings[0].BOTTOM_MARGIN);
                  xmlTextWriter.WriteElementString("PAGE_COLUMNS", objprojectSettings[0].PAGE_COLUMNS);
                  xmlTextWriter.WriteElementString("PAGE_COLUMN_WIDTH",objprojectSettings[0].PAGE_COLUMN_WIDTH);
                  xmlTextWriter.WriteElementString("PAGE_COLUMN_GAP",objprojectSettings[0].PAGE_COLUMN_GAP);
                  xmlTextWriter.WriteElementString("ORIENTATION", objprojectSettings[0].ORIENTATION);
                  xmlTextWriter.WriteElementString("COVER_PAGE",objprojectSettings[0].COVER_PAGE);
                  xmlTextWriter.WriteElementString("CONTENT_PAGE",objprojectSettings[0].CONTENT_PAGE);
                  xmlTextWriter.WriteElementString("CONTENT_PAGE_WITH_CATEGORY_FAMILY",objprojectSettings[0].CONTENT_PAGE_WITH_CATEGORY_FAMILY);
                  xmlTextWriter.WriteElementString("INDEX_PAGE",objprojectSettings[0].INDEX_PAGE);
                  xmlTextWriter.WriteElementString("INDEX_PAGE_WITH_CATALOG_ITEM_NO",objprojectSettings[0].INDEX_PAGE_WITH_CATALOG_ITEM_NO);
                  xmlTextWriter.WriteElementString("INDEXPAGE_COLUMNS",objprojectSettings[0].INDEXPAGE_COLUMNS );
                  xmlTextWriter.WriteElementString("SYSTEM_ATTRIBUTE", systemAttributes);
                  xmlTextWriter.WriteElementString("CUSTOM_ATTRIBUTE", customAttributes);
                  xmlTextWriter.WriteElementString("MULTIPLE_TABLES", multipleAttributes);
                  xmlTextWriter.WriteElementString("REFERENCE_TABLES", referenceAttributes);
                  if (objprojectSettings[0].CUSTOM_CATEGORY_ID == "" || objprojectSettings[0].CUSTOM_FAMILY_ID == "")
                  {
                      objprojectSettings[0].CUSTOM_CATEGORY_ID = "ALL";
                      objprojectSettings[0].CUSTOM_FAMILY_ID = "ALL";
                  }
                  xmlTextWriter.WriteElementString("CUSTOM_CATEGORY_ID", objprojectSettings[0].CUSTOM_CATEGORY_ID);
                  xmlTextWriter.WriteElementString("CUSTOM_FAMILY_ID", objprojectSettings[0].CUSTOM_FAMILY_ID);

                  xmlTextWriter.WriteEndElement();
                  xmlTextWriter.WriteEndElement();
                  xmlTextWriter.WriteEndDocument();
                  xmlTextWriter.Flush();
                  var docSave = new XmlDocument();
                  docSave.LoadXml(stringwriter.ToString());

                  return stringwriter.ToString();
              }
              else
              {
                  return string.Empty;
              }
          }
          catch (Exception objexception)
          {
              // Logger.Error("Error at Export : ExportConfig", objexception);
              return string.Empty;

          }
      }
  
     }
  public class PDF_PROJECT
  {
      [XmlElement("PDF_PROJECT_SETTINGS")]
      public List<PDF_PROJECT_SETTINGS> objprojectSettings = new List<PDF_PROJECT_SETTINGS>();

  }
  public class PDF_PROJECT_SETTINGS
  {
      public int PROJECT_ID { get; set; }
      public string PROJECT_NAME { get; set; }
      public string COMMENT { get; set; }
      public int CATALOG_ID { get; set; }
      public string CATALOG_NAME { get; set; }
      public string  PUBLICATION_TYPE { get; set; }
      public string PAPER_SIZE { get; set; }
      public string PAPER_WIDTH { get; set; }
      public string PAPER_HEIGHT { get; set; }
      public string SEGMENT_PER_WIDTH { get; set; }
      public string SEGMENT_PER_HEIGHT { get; set; }
      public string LEFT_MARGIN { get; set; }
      public string RIGHT_MARGIN { get; set; }
      public string TOP_MARGIN { get; set; }
      public string BOTTOM_MARGIN { get; set; }
      public string PAGE_COLUMNS { get; set; }
      public string PAGE_COLUMN_WIDTH { get; set; }
      public string PAGE_COLUMN_GAP { get; set; }
      public string ORIENTATION { get; set; }
      public string COVER_PAGE { get; set; }
      public string CONTENT_PAGE { get; set; }
      public string CONTENT_PAGE_WITH_CATEGORY_FAMILY { get; set; }
      public string INDEX_PAGE { get; set; }
      public string INDEX_PAGE_WITH_CATALOG_ITEM_NO { get; set; }
      public string INDEXPAGE_COLUMNS { get; set; }
      public string SYSTEM_ATTRIBUTE { get; set; }
      public string CUSTOM_ATTRIBUTE { get; set; }
      public string MULTIPLE_TABLES { get; set; }
      public string REFERENCE_TABLES { get; set; }

      public string CUSTOM_CATEGORY_ID { get; set; }
      public string CUSTOM_FAMILY_ID { get; set; }
     

  }
}
