﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model.XpressCatalog
{
  public  class TemplateList
    {
        public string SOURCE { get; set; }
        public string FILENAME { get; set; }
        public string USERNAME { get; set; }
        public DateTime MODIFIED_DATE { get; set; }
    }
}
