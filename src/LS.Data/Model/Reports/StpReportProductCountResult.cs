﻿using System;

namespace LS.Data.Model.Reports
{
    public class StpReportProductCountResult
    {
        public Nullable<int> PRODUCT_COUNT { get; set; }
      
    }
}
