﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class AddressModel
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State_Province { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string EMail1 { get; set; }
        public string Comments { get; set; }
        public string WebSite { get; set; }
        public string AddressType { get; set; }
        public int CustomerId { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public string FullAddress
        {
            get
            {
                string stateZip = (!string.IsNullOrEmpty(State_Province) ? State_Province + (!string.IsNullOrEmpty(Zip) ? " - " + Zip : "") : Zip);
                return (!string.IsNullOrEmpty(ContactName) ? ContactName + ", " : "") +
                    Address1 +
                    (!string.IsNullOrEmpty(Address2) ? " " + Address2 : "") +
                    (!string.IsNullOrEmpty(City) ? ", " + City : "") +
                    (!string.IsNullOrEmpty(stateZip) ? ", " + stateZip : "") +
                    (!string.IsNullOrEmpty(CountryCode) ? ", " + CountryCode : "");
            }
        }

        public DateTime DateUpdated { get; set; }
        public DateTime DateCreated { get; set; }
     
    }
}
