﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class Family
    {
        public int FAMILY_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public string VALUE { get; set; }
        public string ATTR_VALUE { get; set; }
        public int ATTR_ID { get; set; }
      
    }

    public class RemoveFamilyList
    {
        public int FAMILY_ID;
        public string CATEGORY_ID;
        public int RECORD_ID;
        public int? PUBLISHED_FSTATUS;
        public int CATALOG_ID;
        public string CATEGORY_NAME;
        // x.a.TB_CATEGORY.CATEGORY_NAME;

        public string FAMILY_NAME;
        public string SECTION_NAME;
        public int SORT_ORDER;
        public bool ISAvailable;
        public bool CreateXMLForSelectedAttributesToAllFamily;
    }
}
