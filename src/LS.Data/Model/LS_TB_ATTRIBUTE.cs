﻿namespace LS.Data
{
    using System;
    using System.Collections.Generic;

    public partial class LS_TB_ATTRIBUTE
    {
        public LS_TB_ATTRIBUTE()
        {
          
        }

        public string ATTRIBUTE_NAME { get; set; }
        public int ATTRIBUTE_TYPE { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public bool CREATE_BY_DEFAULT { get; set; }
        public bool VALUE_REQUIRED { get; set; }
        public string STYLE_NAME { get; set; }
        public string STYLE_FORMAT { get; set; }
        public string DEFAULT_VALUE { get; set; }
        public bool PUBLISH2PRINT { get; set; }
        public bool PUBLISH2WEB { get; set; }
        public bool PUBLISH2CDROM { get; set; }
        public bool PUBLISH2ODP { get; set; }
        public bool USE_PICKLIST { get; set; }
        public string ATTRIBUTE_DATATYPE { get; set; }
        public string ATTRIBUTE_DATAFORMAT { get; set; }
        public string ATTRIBUTE_DATARULE { get; set; }
        public string UOM { get; set; }
        public bool IS_CALCULATED { get; set; }
        public string ATTRIBUTE_CALC_FORMULA { get; set; }
        public string PICKLIST_NAME { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }

     
    }
}