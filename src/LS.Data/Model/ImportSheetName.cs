﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class  ImportSheetName
    {
        public string TABLE_NAME { get; set; }
        public string SHEET_PATH { get; set; }
        public bool DATA_EXISTS { get; set; }
    }
}
