﻿namespace LS.Data.Model
{
  public  class Product
    {
          public int CATALOG_ID { get; set; }
            public string CATEGORY_ID { get; set; }
            public int FAMILY_ID { get; set; }
            public int PRODUCT_ID { get; set; }
            public int SORTORDER { get; set; }
            public bool PUBLISH { get; set; }
            public string Part_No_ { get; set; }
            public string Size { get; set; }
            public string Qty_ { get; set; }
            public string Dia { get; set; }
            public string Overall_Length { get; set; }
            public string Description { get; set; }
      
    }
}
