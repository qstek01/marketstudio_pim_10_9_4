﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
  public  class CountryVModel
    {
        public string COUNTRY_CODE { get; set; }
        public string COUNTRY { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }

        public static CountryVModel GetModel(TB_COUNTRY country)
        {
            try
            {
                return new CountryVModel
                {
                   COUNTRY_CODE=country.COUNTRY_CODE,
                   COUNTRY=country.COUNTRY,
                   CREATED_DATE=country.CREATED_DATE,
                   CREATED_USER=country.CREATED_USER,
                   MODIFIED_DATE=country.MODIFIED_DATE,
                   MODIFIED_USER=country.MODIFIED_USER
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
