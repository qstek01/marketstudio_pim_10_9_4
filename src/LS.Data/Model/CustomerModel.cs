﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.ComponentModel.DataAnnotations;
 
namespace LS.Data.Model
{
    public class CustomerModel
    {
        static ILog _logger = null;
        public CustomerModel()
        {
            _logger = LogManager.GetLogger(typeof(CustomerModel));
        }

        public int CustomerId { get; set; }

        public int PlanId { get; set; }
         [Required]
         [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
          
        [Display(Name = "Contact Title")]
        public string ContactTitle { get; set; }
         [Required]
        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }
         [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
         [Required]
        [Display(Name = "Address1")]
        public string Address1 { get; set; }
         [Required]
        [Display(Name = "Address2")]
        public string Address2 { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "State")]

        [Required]
        public string StateProvince { get; set; }

      
        [Display(Name = "STATE_CODE")]
        public string STATE_CODE { get; set; }

        // [Required]
        // [RegularExpression(@"^(?!00000)[0-9]{5,6}$", ErrorMessage = "Invalid Zip")]
        [RegularExpression(@"^[a-zA-Z0-9]{5,6}$", ErrorMessage = "Invalid Zip")]
        [Display(Name = "Zip")]
        public string Zip { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "COUNTRY_CODE")]
        public string COUNTRY_CODE { get; set; }

        //[RegularExpression(@"^\d\d\d\-\d\d\d\-\d\d\d\d$", ErrorMessage = "Entered phone format is not valid.ex:123-123-1234")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.ex:123-123-1234")]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        //[RegularExpression(@"^\d\d\d\-\d\d\d\-\d\d\d\d$", ErrorMessage = "Entered phone format is not valid.ex:123-123-1234")]                   
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered Fax format is not valid.ex:123-123-1234")]
        [Display(Name = "Fax")]
        public string Fax { get; set; }

         [Required]
        
        //[RegularExpression(@"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$", ErrorMessage = "Invalid Email (ex:example@example.com)")]
         //[RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Invalid Email (ex:example@example.com)")]


         [EmailAddress(ErrorMessage = "Invalid Email Address(ex:example@example.com)")]
       
        [DataType(DataType.EmailAddress)]
        [Display(Name = "EMail")]
        public string EMail { get; set; }

        public string Comments { get; set; }
        [Display(Name = "WebSite")]
        public string WebSite { get; set; }

        public DateTime? ActivationDate { get; set; }

        public bool IsActive { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public PlanModel Plan { get; set; }

        public bool IsApproved { get; set; }
        public bool AllowDublicatePartNo { get; set; }
        public int additionalUsers { get; set; }
        public int additionalSKUs { get; set; }
        public decimal additionalStorage { get; set; }
        public bool? EnableSubProduct { get; set; }

        public static CustomerModel GetModel(Customer customer)
        {
            try
            {
                //_logger.Info("Inside  at  CustomerModel: GetModel");
               // var customerSettings = customer.Customer_Settings.FirstOrDefault();
                var model = new CustomerModel
                {
                    CustomerId = customer.CustomerId,
                    CustomerName = customer.CustomerName,
                    PlanId =  (customer.PlanId != null) ? (int) customer.PlanId : 0,
                    CompanyName = customer.CompanyName,
                    ContactName = customer.CompanyName,
                    ContactTitle = customer.ContactTitle,
                    Address1 = customer.Address1,
                    Address2 = customer.Address2,
                    City = customer.City,
                    StateProvince = customer.StateProvince,
                    Zip = customer.Zip,
                    Country = customer.Country,
                    Phone = customer.Phone,
                    Fax = customer.Fax,
                    EMail = customer.EMail,
                    Comments = customer.Comments,
                    WebSite = customer.WebSite,
                    ActivationDate = customer.ActivationDate,
                    IsActive = customer.IsActive,
                    IsApproved = customer.IsApproved,
                    DateUpdated = customer.DateCreated,
                    DateCreated = customer.DateUpdated,
                    //AllowDublicatePartNo=customerSettings != null && customerSettings.AllowDuplicateItem_PartNum
                };
                if (model.PlanId <= 0) return model;
                using (var dbcontext = new CSEntities())
                {
                    model.Plan = (from x in dbcontext.TB_PLAN where x.PLAN_ID == model.PlanId select x).ToList().
                        Select(i => new PlanModel { PLAN_ID = i.PLAN_ID, PLAN_NAME = i.PLAN_NAME }).FirstOrDefault();
                    model.AllowDublicatePartNo =
                        dbcontext.Customer_Settings.Find(model.CustomerId).AllowDuplicateItem_PartNum;
                    model.EnableSubProduct = (bool)dbcontext.Customer_Settings.Find(model.CustomerId).EnableSubProduct;

                }
                return model;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CustomerModel: GetModel", ex);
                return null;
            }

        }

    }

}
