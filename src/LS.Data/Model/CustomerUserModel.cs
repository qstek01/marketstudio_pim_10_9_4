﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace LS.Data.Model
{
    public partial class CustomerUserModel
    {
         static ILog _logger = null;
         public CustomerUserModel()
        {
            _logger = LogManager.GetLogger(typeof(CustomerUserModel));
        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string UserName { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateUpdated { get; set; }

        public static CustomerUserModel GetModel(Customer_User customer)
        {
            try
            {
                //_logger.Info("Inside  at  CustomerUserModel: GetModel");
               var model =  new CustomerUserModel
                {
                    Id = customer.Id,
                    UserName = customer.User_Name,
                    CustomerId = customer.CustomerId,
                    DateCreated = customer.DateCreated,
                    DateUpdated = customer.DateUpdated
                };
                return model;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CustomerUserModel: GetModel", ex);
                return null;
            }

        }

    }
}
