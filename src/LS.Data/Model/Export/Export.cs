﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using log4net;
using Newtonsoft.Json.Linq;
using DataTable = System.Data.DataTable;

namespace LS.Data.Model.Export
{
    public class Export
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(Export));
        readonly CSEntities _dbcontext = new CSEntities();
        public string ExportConfig(int catalogid, string attrBy, string filter, string catalogname, string categoryname, JArray model, string projectName)
        {
            try
            {
                var functionAlloweditem = (model[1]).Select(x => x).ToList();
                string systemAttributes = string.Empty;
                string customAttributes = string.Empty;
                foreach (var item in functionAlloweditem)
                {
                    if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 0 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 2)
                    { systemAttributes += item["ATTRIBUTE_NAME"] + ","; }
                    else
                    {
                        customAttributes += Convert.ToInt32(item["ATTRIBUTE_ID"]) == 0
                            ? item["ATTRIBUTE_NAME"] + ","
                            : item["ATTRIBUTE_ID"] + ",";
                    }
                }
                systemAttributes = systemAttributes.Remove(systemAttributes.Length - 1);
                customAttributes = customAttributes.Remove(customAttributes.Length - 1);
                var stringwriter = new StringWriter();
                var xmlTextWriter = new XmlTextWriter(stringwriter);
                xmlTextWriter.WriteStartDocument();
                xmlTextWriter.WriteStartElement("ExportSettings");
                xmlTextWriter.WriteStartElement("AttrbuteSettings");
                xmlTextWriter.WriteElementString("AttributeName", catalogname);
                xmlTextWriter.WriteElementString("AttrBy", attrBy);
                xmlTextWriter.WriteElementString("Filters", filter);
                xmlTextWriter.WriteElementString("SystemAttribute", systemAttributes);
                xmlTextWriter.WriteElementString("CustomAttribute", customAttributes);
                xmlTextWriter.WriteElementString("Catalog_ID", Convert.ToString(catalogid));
                xmlTextWriter.WriteElementString("Category_ID", categoryname);
                xmlTextWriter.WriteEndElement();
                xmlTextWriter.WriteEndElement();
                xmlTextWriter.WriteEndDocument();
                xmlTextWriter.Flush();
                var docSave = new XmlDocument();
                docSave.LoadXml(stringwriter.ToString());
                //write the path where you want to save the Xml file
                string path = HttpContext.Current.Server.MapPath("~/Content");
                docSave.Save(path + "/XML/" + projectName + ".xml");
                return stringwriter.ToString();
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at Export : ExportConfig", objexception);
                return string.Empty;

            }
        }
        public List<AttrbuteSettings> ProjectXmlDeserializefunction(string exportDetails)
        {
            try
            {
                if (!string.IsNullOrEmpty(exportDetails))
                {
                    var deserializer = new XmlSerializer(typeof(ExportSettings));
                    TextReader reader = new StringReader(exportDetails);
                    object obj = deserializer.Deserialize(reader);
                    var xmlData = (ExportSettings)obj;
                    return xmlData.objAttributeSettings;
                }
                return null;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at Export :  XmlDeserializefunction", objException);
                return null;
            }
        }
        public DataSet FamilyFilterFlatTable(DataSet flatDataset, string filter, int catalogid)
        {
            try
            {
                if (filter != "No")
                {
                    var sqLstring = new StringBuilder();
                    var familyFilterValue = _dbcontext.TB_CATALOG.FirstOrDefault(x => x.CATALOG_ID == catalogid);
                    if (familyFilterValue != null)
                    {

                        string sFamilyFilter = familyFilterValue.FAMILY_FILTERS;
                        if (!string.IsNullOrEmpty(sFamilyFilter))
                        {
                            var xmlDOc = new XmlDocument();
                            xmlDOc.LoadXml(sFamilyFilter);
                            XmlNode rNode = xmlDOc.DocumentElement;
                            if (rNode != null && rNode.ChildNodes.Count > 0)
                            {
                                for (int i = 0; i < rNode.ChildNodes.Count; i++)
                                {
                                    XmlNode tableDataSetNode = rNode.ChildNodes[i];
                                    if (tableDataSetNode.HasChildNodes)
                                    {
                                        if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                                        {
                                            tableDataSetNode.ChildNodes[2].InnerText = "=";
                                        }
                                        if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                                        {
                                            tableDataSetNode.ChildNodes[0].InnerText = "0";
                                        }
                                        string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                                        if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                        {
                                            sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTION](" + catalogid + ") WHERE (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                        }
                                        else
                                        {
                                            sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTION](" + catalogid + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                        }
                                    }
                                    if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                                    {
                                    }
                                    if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                                    {
                                        sqLstring.Append(" INTERSECT \n");
                                    }
                                    if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                                    {
                                        sqLstring.Append(" UNION \n");
                                    }
                                }
                            }
                        }
                    }
                    string familyFiltersql = sqLstring.ToString();
                    DataSet oDsFamilyFilter = new DataSet();
                    if (familyFiltersql.Length > 0)
                    {
                        string s = "SELECT FAMILY_ID FROM FAMILY(" + catalogid + ") WHERE CATALOG_ID=" + catalogid + " AND FAMILY_ID IN\n" +
                              "(\n";
                        familyFiltersql = s + familyFiltersql + "\n)";

                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = familyFiltersql;
                            objSqlCommand.CommandType = CommandType.Text;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlConnection.Open();
                            var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                            objSqlDataAdapter.Fill(oDsFamilyFilter);
                        }

                        for (int rowCount = 0; rowCount < flatDataset.Tables[flatDataset.Tables.Count - 1].Rows.Count; rowCount++)
                        {
                            DataRow odr = flatDataset.Tables[flatDataset.Tables.Count - 1].Rows[rowCount];
                            bool available = oDsFamilyFilter.Tables[0].Rows.Cast<DataRow>().Count(dr => dr["FAMILY_ID"].ToString() == odr["FAMILY_ID"].ToString() || dr["FAMILY_ID"].ToString() == odr["SUBFAMILY_ID"].ToString()) > 0;
                            if (!available)
                            {
                                odr.Delete();
                                flatDataset.AcceptChanges();
                                rowCount--;
                            }
                        }
                    }

                }
                return flatDataset;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at Export : ExportXls", objexception);
                return flatDataset;
            }
        }
        public DataSet ProductFilterFlatTable(DataSet flatDataset, string filter, int catalogid)
        {
            try
            {
                if (filter != "No")
                {
                    var sqLstring = new StringBuilder();
                    var familyFilterValue = _dbcontext.TB_CATALOG.FirstOrDefault(x => x.CATALOG_ID == catalogid);
                    if (familyFilterValue != null)
                    {

                        string sProductFilter = familyFilterValue.PRODUCT_FILTERS;
                        if (!string.IsNullOrEmpty(sProductFilter))
                        {
                            var xmlDOc = new XmlDocument();
                            xmlDOc.LoadXml(sProductFilter);
                            XmlNode rNode = xmlDOc.DocumentElement;
                            if (rNode != null && rNode.ChildNodes.Count > 0)
                            {
                                for (int i = 0; i < rNode.ChildNodes.Count; i++)
                                {
                                    XmlNode tableDataSetNode = rNode.ChildNodes[i];
                                    if (tableDataSetNode.HasChildNodes)
                                    {
                                        if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                                        {
                                            tableDataSetNode.ChildNodes[2].InnerText = "=";
                                        }
                                        if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                                        {
                                            tableDataSetNode.ChildNodes[0].InnerText = "0";
                                        }
                                        string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                                        var attribuetypeDs = _dbcontext.TB_ATTRIBUTE.FirstOrDefault(x => x.ATTRIBUTE_ID == Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText));
                                        //ocon.SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE  ATTRIBUTE_ID = " + Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText) + " ";
                                        // DataSet attribuetypeDs = ocon.CreateDataSet(); ocon._DBCon.Close();
                                        if (attribuetypeDs != null)
                                        {
                                            if (attribuetypeDs.ATTRIBUTE_DATATYPE.ToUpper().Contains("TEX") || attribuetypeDs.ATTRIBUTE_DATATYPE.ToUpper().Contains("DATE"))
                                            {
                                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                                {
                                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogid + ") WHERE (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                                }
                                                else
                                                {
                                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogid + ") WHERE (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                                }
                                            }
                                            else if (attribuetypeDs.ATTRIBUTE_DATATYPE.ToUpper().Contains("DECI") || attribuetypeDs.ATTRIBUTE_DATATYPE.ToUpper().Contains("NUM"))
                                            {
                                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                                {
                                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogid + ") WHERE  (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                                }
                                                else
                                                {
                                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogid + ") WHERE (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                                }

                                            }
                                        }

                                    }
                                    if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                                    {
                                    }
                                    if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                                    {
                                        sqLstring.Append(" INTERSECT \n");
                                    }
                                    if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                                    {
                                        sqLstring.Append(" UNION \n");
                                    }
                                }
                            }
                        }
                    }
                    string productFiltersql = sqLstring.ToString();
                    DataSet oDsProductFilter = new DataSet();
                    if (productFiltersql.Length > 0)
                    {
                        string s = "SELECT PRODUCT_ID FROM [PRODUCT FAMILY](" + catalogid + ") WHERE CATALOG_ID=" + catalogid + " AND PRODUCT_ID IN\n" +
                              "(\n";
                        productFiltersql = s + productFiltersql + "\n)";

                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = productFiltersql;
                            objSqlCommand.CommandType = CommandType.Text;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlConnection.Open();
                            var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                            objSqlDataAdapter.Fill(oDsProductFilter);
                        }

                        for (int rowCount = 0; rowCount < flatDataset.Tables[flatDataset.Tables.Count - 1].Rows.Count; rowCount++)
                        {
                            DataRow odr = flatDataset.Tables[flatDataset.Tables.Count - 1].Rows[rowCount];
                            bool available = oDsProductFilter.Tables[0].Rows.Cast<DataRow>().Count(dr => dr["PRODUCT_ID"].ToString() == odr["PRODUCT_ID"].ToString()) > 0;
                            if (!available)
                            {
                                odr.Delete();
                                flatDataset.AcceptChanges();
                                rowCount--;
                            }
                        }
                    }

                }
                return flatDataset;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at Export : ExportXls", objexception);
                return flatDataset;
            }
        }

        private int _catalogId;
        private int _familyId;
        private string _categoryID = string.Empty;
        private int[] _attributeIdList;
        private bool _mergeFamilyWithProductTable;
        string _catName = string.Empty;

        //Set to false by default, so that no extended information about the family and table will be
        //loaded with the catalog information
        private bool _loadXFamily;
        private bool _loadXProductTable;
        public DataSet CatalogXRow(int catalogId, int familyId, int[] attributeIdList,string catId1)
        {
            _catName = catId1;
            _catalogId = catalogId;
            _familyId = familyId;
            _loadXFamily = true;
            _loadXProductTable = true;
            _attributeIdList = attributeIdList;
            _mergeFamilyWithProductTable = true;

            DataSet ds = GetProductCatalogRow();
            //DataSet joinedDS = JoinedTable(ds);
            DataSet joinedDs = JoinedTableNew(ds);
            DataSet joinedDsFinal = JoinedTableNewSub(joinedDs);
            //grdResult.DataSource = ds.Tables[0];
            //grdResult2.DataSource = ds.Tables[2];
            //grdSelect.DataSource = ds.Tables[2];
            return joinedDsFinal;
        }

        public DataSet CatalogCategoryXRow(int catalogId, int familyId, int[] attributeIdList,string catID)
        {
            //var asdf = new Connection();
            // asdf.ConnSettings(connvalue);
            // var csPt = new CSDBProviderEX.ProductCatalog(catalogID, familyID, true, true, attributeList, true, "");
            _catName = "";
            _catalogId = catalogId;
            _familyId = familyId;
            _loadXFamily = true;
            _loadXProductTable = true;
            _attributeIdList = attributeIdList;
            _mergeFamilyWithProductTable = true;
            DataSet ds = GetProductCatalogRow(catID);
            //DataSet joinedDS = JoinedTable(ds);
            DataSet joinedDs = JoinedTableNew(ds);
            DataSet joinedDsNew = JoinedTableNewSub(joinedDs);
            //grdResult.DataSource = ds.Tables[0];
            //grdResult2.DataSource = ds.Tables[2];
            //grdSelect.DataSource = ds.Tables[2];
            return joinedDsNew;
        }
        public DataSet GetProductCatalogRow()
        {
            var dsPf = new DataSet { EnforceConstraints = false };

            DataTable dtCatalog = LoadProductCatalog();
            dtCatalog.TableName = "ProductFamily";

            if (_loadXFamily)
            {
                //Load product family information 
                //var csFm = new ProductFamily(_catalogId, _familyId, _attributeIdList);
                dsPf = GetProductFamily();
                dsPf.Merge(dtCatalog);
                DataRow[] tempRow = dsPf.Tables[0].Select("FAMILY_ID=0");
                DataRow[] tempR = dtCatalog.Select("FAMILY_ID=0");
                foreach (DataRow dr in tempRow)
                {
                    dsPf.Tables[0].Rows.Remove(dr);
                }
                foreach (DataRow dr in tempR)
                {
                    dsPf.Tables[0].ImportRow(dr);
                }
            }

            if (_loadXProductTable)
            {
                //Load product table
                // var csProd = new ProductTable(_catalogId, _familyId, _attributeIdList, _catName);
                DataTable dtProductTable = GetProductTableRow();
                dtProductTable.TableName = "ProductTable";
                dsPf.Tables.Add(dtProductTable);
                DataTable dtSubProductTable = GetSubProductTableRow();
                dtSubProductTable.TableName = "SubProductTable";
                dsPf.Tables.Add(dtSubProductTable);
            }

            if (_loadXFamily && _loadXProductTable)
            {
                if (_mergeFamilyWithProductTable)
                {

                    var dcpkfamily = new DataColumn[2];
                    dcpkfamily[0] = dsPf.Tables["ProductFamily"].Columns["CATALOG_ID"];
                    dcpkfamily[1] = dsPf.Tables["ProductFamily"].Columns["FAMILYID"];

                    var dcpkprodtable = new DataColumn[2];
                    dcpkprodtable[0] = dsPf.Tables["ProductTable"].Columns["CATALOG_ID"];
                    dcpkprodtable[1] = dsPf.Tables["ProductTable"].Columns["FAMILYID"];

                    dsPf.Relations.Add(dcpkfamily, dcpkprodtable);

                    var dcpksubfamily = new DataColumn[2];
                    dcpksubfamily[0] = dsPf.Tables["ProductFamily"].Columns["CATALOG_ID"];
                    dcpksubfamily[1] = dsPf.Tables["ProductFamily"].Columns["FAMILYID"];

                    //dsPF.Relations.Add(dcpksubfamily, dcpkprodtable);
                }
            }

            return dsPf;
        }

        public DataSet GetProductCatalogRow(string categoryID)
        {
            var dsPf = new DataSet { EnforceConstraints = false };

            DataTable dtCatalog = string.IsNullOrEmpty(categoryID) ? LoadProductCatalog() : LoadProductCatalog(categoryID);
            
            dtCatalog.TableName = "ProductFamily";

            if (_loadXFamily)
            {
                //Load product family information 
                //var csFm = new ProductFamily(_catalogId, _familyId, _attributeIdList);
                dsPf = GetProductFamily();
                dsPf.Merge(dtCatalog);
                DataRow[] tempRow = dsPf.Tables[0].Select("FAMILY_ID=0");
                DataRow[] tempR = dtCatalog.Select("FAMILY_ID=0");
                foreach (DataRow dr in tempRow)
                {
                    dsPf.Tables[0].Rows.Remove(dr);
                }
                foreach (DataRow dr in tempR)
                {
                    dsPf.Tables[0].ImportRow(dr);
                }
            }

            if (_loadXProductTable)
            {
                //Load product table
               // var csProd = new ProductTable(_catalogId, _familyId, _attributeIdList, _catName);
                DataTable dtProductTable = GetProductTableRow();
                DataTable dtSubProductTable = GetSubProductTableRow();
                dtProductTable.TableName = "ProductTable";
                dtSubProductTable.TableName = "SubProductTable";
                dsPf.Tables.Add(dtProductTable);
                dsPf.Tables.Add(dtSubProductTable);
            }

            if (_loadXFamily && _loadXProductTable)
            {
                if (_mergeFamilyWithProductTable)
                {

                    var dcpkfamily = new DataColumn[2];
                    dcpkfamily[0] = dsPf.Tables["ProductFamily"].Columns["CATALOG_ID"];
                    dcpkfamily[1] = dsPf.Tables["ProductFamily"].Columns["FAMILYID"];

                    var dcpkprodtable = new DataColumn[2];
                    dcpkprodtable[0] = dsPf.Tables["ProductTable"].Columns["CATALOG_ID"];
                    dcpkprodtable[1] = dsPf.Tables["ProductTable"].Columns["FAMILYID"];

                    dsPf.Relations.Add(dcpkfamily, dcpkprodtable);

                    var dcpksubfamily = new DataColumn[2];
                    dcpksubfamily[0] = dsPf.Tables["ProductFamily"].Columns["CATALOG_ID"];
                    dcpksubfamily[1] = dsPf.Tables["ProductFamily"].Columns["FAMILYID"];

                    //dsPF.Relations.Add(dcpksubfamily, dcpkprodtable);
                }
            }

            return dsPf;
        }

        private DataTable LoadProductCatalog()
        {
            //Fill the catalog table
            // var tacf = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILYTableAdapter();
            var tblcf = new DataTable();
            if (_familyId == 0)
            {
                if (_catalogId == 0)
                {
                    // tacf.Fill(tblcf);
                    FillCf(tblcf);
                }
                else
                {
                    FillByCfCatalog(tblcf, _catalogId);
                }
            }
            else
            {
                FillByCfFamily(tblcf, _catalogId, _familyId);
            }

            //Get the category hierarchy details
            //  var tacat = new CSDBProvider.CSDSTableAdapters.TB_CATEGORYTableAdapter();
            var tblcat = new DataTable();
            if (_catalogId != 0)
                FillByCCatalog(tblcat, _catalogId);
            else
                FillC(tblcat);
            var dtcatree = new DataTable();
            dtcatree.Columns.Add("MAXDEPTH", typeof(int));
            dtcatree.Columns.Add("CATEGORY_IDTEMP", typeof(string));
            dtcatree.Columns.Add("CATEGORY_NAMETEMP", typeof(string));
            int iMaxLevel = 1;
            int iCurLevel = 1;
            foreach (DataRow drcat in tblcat.Rows)
            {
                DataRow dr = dtcatree.NewRow();
                dr["CATEGORY_IDTEMP"] = drcat["CATEGORY_ID"];
                dr["CATEGORY_NAMETEMP"] = drcat["CATEGORY_NAME"];
                dtcatree.Rows.Add(dr);

                string sparent = drcat["PARENT_CATEGORY"].ToString();
                if (sparent != "0")
                {
                    while (sparent != "0")
                    {
                        iCurLevel = iCurLevel + 1;
                        var tblcattemp = new DataTable();
                        FillByCCategory(tblcattemp, sparent);
                        if (tblcattemp.Rows.Count > 0)
                        {
                            DataRow drtemp = tblcattemp.Rows[0];
                            sparent = drtemp["PARENT_CATEGORY"].ToString();
                        }
                    }
                    if (iMaxLevel < iCurLevel)
                    {
                        iMaxLevel = iCurLevel;
                    }
                }
                dr["MAXDEPTH"] = iCurLevel;
                iCurLevel = 1;
            }

            string scid;
            string scname;
            for (int i = 1; i <= iMaxLevel; i++)
            {
                scid = "CATID_L" + i;
                scname = "CATNAME_L" + i;
                dtcatree.Columns.Add(scid, typeof(string));
                dtcatree.Columns.Add(scname, typeof(string));
            }

            foreach (DataRow drcat in dtcatree.Rows)
            {
                string scurrent = drcat["CATEGORY_IDTEMP"].ToString();
                int iLevel = Convert.ToInt32(drcat["MAXDEPTH"]);

                var tblcattemp = new DataTable();
                FillByCCategory(tblcattemp, scurrent);
                DataRow drtemp = tblcattemp.Rows[0];
                string sparent = drtemp["PARENT_CATEGORY"].ToString();

                if (sparent == "0")
                {
                    //Root category
                    drcat["CATID_L" + iLevel] = drtemp["CATEGORY_ID"];
                    drcat["CATNAME_L" + iLevel] = drtemp["CATEGORY_NAME"];
                }
                else
                {
                    //Not a root category, several level of parent exists
                    while (iLevel > 1)
                    {
                        var tblcattemp2 = new DataTable();
                        FillByCCategory(tblcattemp2, scurrent);
                        DataRow drtemp2 = tblcattemp2.Rows[0];
                        drcat["CATID_L" + iLevel] = drtemp2["CATEGORY_ID"];
                        drcat["CATNAME_L" + iLevel] = drtemp2["CATEGORY_NAME"];
                        scurrent = drtemp2["PARENT_CATEGORY"].ToString();
                        iLevel = iLevel - 1;
                    }
                    if (iLevel == 1)
                    {
                        //Root category
                        var tblcattemp3 = new DataTable();
                        FillByCCategory(tblcattemp3, scurrent);
                        DataRow drtemp3 = tblcattemp3.Rows[0];
                        drcat["CATID_L" + iLevel] = drtemp3["CATEGORY_ID"];
                        drcat["CATNAME_L" + iLevel] = drtemp3["CATEGORY_NAME"];
                    }
                }
            }

            for (int d = 1; d <= iMaxLevel; d++)
            {
                scid = "CATID_L" + d;
                scname = "CATNAME_L" + d;
                tblcf.Columns.Add(scid, typeof(string));
                tblcf.Columns.Add(scname, typeof(string));
            }

            foreach (DataRow drtree in dtcatree.Rows)
            {
                DataRow[] drstblcf = tblcf.Select("CATEGORY_ID = '" + drtree["CATEGORY_IDTEMP"] + "'");
                foreach (DataRow t in drstblcf)
                {
                    int iTempLevel = Convert.ToInt32(drtree["MAXDEPTH"]);
                    for (int j = 1; j <= iTempLevel; j++)
                    {
                        t["CATID_L" + j] = drtree["CATID_L" + j];
                        t["CATNAME_L" + j] = drtree["CATNAME_L" + j];
                    }
                }
            }

            //All done, lets do some cleanup
            tblcf.Columns.Remove("CATEGORY_ID");
            for (int d = 1; d <= iMaxLevel; d++)
            {
                scid = "CATID_L" + d;
                if (scid == "CATID_L1")
                {
                    tblcf.Columns["CATID_L1"].ColumnName = "CATEGORY_ID";
                    tblcf.Columns["CATNAME_L1"].ColumnName = "CATEGORY_NAME";
                }
                else
                {
                    tblcf.Columns["CATID_L" + d].ColumnName = "SUBCATID_L" + Convert.ToString(d - 1);
                    tblcf.Columns["CATNAME_L" + d].ColumnName = "SUBCATNAME_L" + Convert.ToString(d - 1);
                }
            }

            tblcf.Columns.Add("FAMILYID", typeof(int));

            foreach (DataRow drcat in tblcf.Rows)
            {
                {
                    if (drcat["SUBFAMILY_ID"].ToString().Trim().Length > 0)
                    {
                        drcat["FAMILYID"] = drcat["SUBFAMILY_ID"];
                    }
                    else
                    {
                        drcat["FAMILYID"] = drcat["FAMILY_ID"];
                    }
                }
            }

            return tblcf;
        }

        private DataTable LoadProductCatalog(string catID)
        {
            //Fill the catalog table
            // var tacf = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILYTableAdapter();
            var tblcf = new DataTable();
            if (_familyId == 0)
            {
                if (_catalogId == 0)
                {
                    FillCf(tblcf);
                }
                else
                {
                    FillByCfCatalog(tblcf, _catalogId);
                }
            }
            else
            {
                FillByCfFamily(tblcf, _catalogId, _familyId);
            }

            //Get the category hierarchy details
            // var tacat = new CSDBProvider.CSDSTableAdapters.TB_CATEGORYTableAdapter();
            var tblcat = new DataTable();
            if (_catalogId != 0)
                FillByCCatalog(tblcat, _catalogId);
            else
                FillC(tblcat);

            var dtcatree = new DataTable();
            dtcatree.Columns.Add("MAXDEPTH", typeof(int));
            dtcatree.Columns.Add("CATEGORY_IDTEMP", typeof(string));
            dtcatree.Columns.Add("CATEGORY_NAMETEMP", typeof(string));
            int iMaxLevel = 1;
            int iCurLevel = 1;
            foreach (DataRow drcat in tblcat.Rows)
            {
                DataRow dr = dtcatree.NewRow();
                dr["CATEGORY_IDTEMP"] = drcat["CATEGORY_ID"];
                dr["CATEGORY_NAMETEMP"] = drcat["CATEGORY_NAME"];
                dtcatree.Rows.Add(dr);

                string sparent = drcat["PARENT_CATEGORY"].ToString();
                if (sparent != "0")
                {
                    while (sparent != "0")
                    {
                        iCurLevel = iCurLevel + 1;
                        var tblcattemp = new DataTable();
                        FillByCCategory(tblcattemp, sparent);
                        DataRow drtemp = tblcattemp.Rows[0];
                        sparent = drtemp["PARENT_CATEGORY"].ToString();
                    }
                    if (iMaxLevel < iCurLevel)
                    {
                        iMaxLevel = iCurLevel;
                    }
                }
                dr["MAXDEPTH"] = iCurLevel;
                iCurLevel = 1;
            }

            string scid;
            string scname;
            for (int i = 1; i <= iMaxLevel; i++)
            {
                scid = "CATID_L" + i;
                scname = "CATNAME_L" + i;
                dtcatree.Columns.Add(scid, typeof(string));
                dtcatree.Columns.Add(scname, typeof(string));
            }

            foreach (DataRow drcat in dtcatree.Rows)
            {
                string scurrent = drcat["CATEGORY_IDTEMP"].ToString();
                int iLevel = Convert.ToInt32(drcat["MAXDEPTH"]);

                var tblcattemp = new DataTable();
                FillByCCategory(tblcattemp, scurrent);
                DataRow drtemp = tblcattemp.Rows[0];
                string sparent = drtemp["PARENT_CATEGORY"].ToString();

                if (sparent == "0")
                {
                    //Root category
                    if (drtemp["CATEGORY_ID"].ToString() == catID)
                    {
                        drcat["CATID_L" + iLevel] = drtemp["CATEGORY_ID"];
                        drcat["CATNAME_L" + iLevel] = drtemp["CATEGORY_NAME"];
                    }
                }
                else
                {
                    //Not a root category, several level of parent exists
                    while (iLevel > 1)
                    {
                        var tblcattemp2 = new DataTable();
                        FillByCCategory(tblcattemp2, scurrent);
                        DataRow drtemp2 = tblcattemp2.Rows[0];
                        drcat["CATID_L" + iLevel] = drtemp2["CATEGORY_ID"];
                        drcat["CATNAME_L" + iLevel] = drtemp2["CATEGORY_NAME"];
                        scurrent = drtemp2["PARENT_CATEGORY"].ToString();
                        iLevel = iLevel - 1;
                    }
                    if (iLevel == 1)
                    {
                        //Root category
                        var tblcattemp3 = new DataTable();
                        FillByCCategory(tblcattemp3, scurrent);
                        DataRow drtemp3 = tblcattemp3.Rows[0];
                        if (drtemp3["CATEGORY_ID"].ToString() == catID)
                        {
                            drcat["CATID_L" + iLevel] = drtemp3["CATEGORY_ID"];
                            drcat["CATNAME_L" + iLevel] = drtemp3["CATEGORY_NAME"];
                        }
                    }
                }
            }
            DataRow[] drrr = dtcatree.Select("CATID_L1 like '" + catID + "'");

            for (int d = 1; d <= iMaxLevel; d++)
            {
                scid = "CATID_L" + d;
                scname = "CATNAME_L" + d;
                tblcf.Columns.Add(scid, typeof(string));
                tblcf.Columns.Add(scname, typeof(string));
            }

            foreach (DataRow drtree in drrr)//            foreach (DataRow drtree in dtcatree.Rows)
            {
                DataRow[] drstblcf = tblcf.Select("CATEGORY_ID = '" + drtree["CATEGORY_IDTEMP"] + "'");
                foreach (DataRow t in drstblcf)
                {
                    int iTempLevel = Convert.ToInt32(drtree["MAXDEPTH"]);
                    for (int j = 1; j <= iTempLevel; j++)
                    {
                        t["CATID_L" + j] = drtree["CATID_L" + j];
                        t["CATNAME_L" + j] = drtree["CATNAME_L" + j];
                    }
                }
            }

            //All done, lets do some cleanup
            DataRow[] drr = tblcf.Select("CATID_L1 like '" + catID + "'");
            DataTable tblcfDup = tblcf.Clone();
            foreach (DataRow drtree in drr)//            foreach (DataRow drtree in dtcatree.Rows)
            {
                tblcfDup.ImportRow(drtree);
            }

            tblcf.Columns.Remove("CATEGORY_ID");
            tblcfDup.Columns.Remove("CATEGORY_ID");
            for (int d = 1; d <= iMaxLevel; d++)
            {
                scid = "CATID_L" + d;
                if (scid == "CATID_L1")
                {
                    tblcfDup.Columns["CATID_L1"].ColumnName = "CATEGORY_ID";
                    tblcfDup.Columns["CATNAME_L1"].ColumnName = "CATEGORY_NAME";
                }
                else
                {
                    tblcfDup.Columns["CATID_L" + d].ColumnName = "SUBCATID_L" + Convert.ToString(d - 1);
                    tblcfDup.Columns["CATNAME_L" + d].ColumnName = "SUBCATNAME_L" + Convert.ToString(d - 1);
                }
            }

            tblcfDup.Columns.Add("FAMILYID", typeof(int));

            foreach (DataRow drcat in tblcfDup.Rows)
            {
                {
                    if (drcat["SUBFAMILY_ID"].ToString().Trim().Length > 0)
                    {
                        drcat["FAMILYID"] = drcat["SUBFAMILY_ID"];
                    }
                    else
                    {
                        drcat["FAMILYID"] = drcat["FAMILY_ID"];
                    }
                }
            }

            return tblcfDup;

            //for (int d = 1; d <= iMaxLevel; d++)
            //{
            //    scid = "CATID_L" + d;
            //    scname = "CATNAME_L" + d;
            //    if (scid == "CATID_L1")
            //    {
            //        tblcf.Columns["CATID_L1"].ColumnName = "CATEGORY_ID";
            //        tblcf.Columns["CATNAME_L1"].ColumnName = "CATEGORY_NAME";
            //    }
            //    else
            //    {
            //        tblcf.Columns["CATID_L" + d].ColumnName = "SUBCATID_L" + Convert.ToString(d - 1);
            //        tblcf.Columns["CATNAME_L" + d].ColumnName = "SUBCATNAME_L" + Convert.ToString(d - 1);
            //    }
            //}

            //tblcf.Columns.Add("FAMILYID", System.Type.GetType("System.Int32"));

            //foreach (DataRow drcat in tblcf.Rows)
            //{
            //    {
            //        if (drcat["SUBFAMILY_ID"].Trim().Length > 0)
            //        {
            //            drcat["FAMILYID"] = drcat["SUBFAMILY_ID"];
            //        }
            //        else
            //        {
            //            drcat["FAMILYID"] = drcat["FAMILY_ID"];
            //        }
            //    }
            //}

            //return tblcf;
        }
        public DataSet GetProductFamily()
        {
            //Fill the (root) family attribute data table
            // var tafamilyspecs = new TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_ATTRIBUTESTableAdapter();
            var tblfamilyspecs = new DataTable();

            //Fill the sub family attribute data table
            // var tasubfamilyspecs = new TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_SUBFAMILY_ATTRIBUTESTableAdapter();
            var tblsubfamilyspecs = new DataTable();

            //Fill the family attribute name list table
            //var tacfa = new TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_ATTRIBUTE_LISTTableAdapter();
            var tblcfa = new DataTable();

            if (_familyId == 0)
            {
                //Load all families if no id is sent and for all catalog
                if (_catalogId == 0)
                {
                    FillCfa(tblfamilyspecs);
                    FillCsfa(tblsubfamilyspecs);
                    FillCfal(tblcfa);
                }
                else
                {
                    //Load all families for a specific catalog
                    FillByCfaCatalog(tblfamilyspecs, _catalogId);
                    FillByCsfaCatalog(tblsubfamilyspecs, _catalogId);
                    FillByCfalCatalog(tblcfa, _catalogId);
                }
            }
            else
            {
                //Since we know the family id, just load the family
                FillByCfaFamily(tblfamilyspecs, _catalogId, _familyId);
                FillByCsfaFamily(tblsubfamilyspecs, _catalogId, _familyId);
                FillByCfalFamily(tblcfa, _catalogId, _familyId);
            }

            var dsF = new DataSet();
            dsF.Tables.Add(GetTransposedFamily(tblfamilyspecs, tblcfa, false));
            dsF.Tables[0].TableName = "ProductFamily";
            dsF.Tables[0].Constraints.Clear();

            var dtSubFamily = new DataTable();
            dtSubFamily = GetTransposedFamily(tblsubfamilyspecs, tblcfa, true);
            dtSubFamily.TableName = "ProductFamily";

            dsF.EnforceConstraints = false;
            dsF.Merge(dtSubFamily);
            return dsF;
        }

        public DataTable GetProductTableRow()
        {
            var dt = new DataTable("Product");
            var dt2 = new DataTable("Product2");
            var dsReturn = new DataSet();
            // var taprod = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter();
            var tblprod = new DataTable();
            var tblprodSf = new DataTable();

            //var tacpa = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTTableAdapter();
            var tblcpa = new DataTable();
            var tblcpaSf = new DataTable();

            if (_familyId == 0)
            {
                //Load all families if no id is sent and for all catalog
                if (_catalogId == 0)
                {
                    FillCfpa(tblprod);
                    FillCfalproduct(tblcpa);
                }
                else
                {
                    //Load all families for a specific catalog  
                    string listAttr = "";
                    if (_attributeIdList.Length == 0)
                    {
                        _attributeIdList = new int[1];
                        _attributeIdList[0] = 1;
                        listAttr = "1";
                    }
                    else
                    {
                        listAttr = _attributeIdList[0].ToString();
                        int[] chkAttr = _attributeIdList;
                        _attributeIdList = new int[chkAttr.Length + 1];
                        for (int vaaa = 0; vaaa < chkAttr.Length; vaaa++)
                        {
                            _attributeIdList[vaaa] = chkAttr[vaaa];
                        }
                        _attributeIdList[_attributeIdList.Length - 1] = 1;
                    }
                    for (int vaa = 1; vaa < _attributeIdList.Length; vaa++)
                    {
                        listAttr = listAttr + "," + _attributeIdList[vaa].ToString();
                    }

                    FillByproducttable1(dsReturn,_catalogId, listAttr);
                   // _DBAdapter.Fill(dsReturn);
                    //tblprod = dsReturn.Tables[0];
                    //taprod.FillByAttributeList(tblprod, _CatalogId,,"1,2");// .FillByCatalog(tblprod, _CatalogId);
                    FillByCfalproductCatalog(tblcpa, _catalogId);

                    //dt = GetTransposedProductTable(dsReturn.Tables[0], tblcpa);
                    //dt2 = GetTransposedProductTable(tblprodSF, tblcpaSF);
                    dt = dsReturn.Tables[0].Clone();
                    foreach (DataRow dr in dsReturn.Tables[0].Rows)
                        dt.ImportRow(dr);

                }
            }
            else
            {
                //string catfamquery = "EXEC  " + _familyId + "," + _catalogId;
                //CSDBProvider.Connection conn = new TradingBell.CatalogX.CSDBProvider.Connection();
                //SqlConnection Ocon = new SqlConnection(conn.GetConnSettings());
                //SqlCommand comm = new SqlCommand(catfamquery, Ocon);
                //comm.CommandTimeout = 0;
                //SqlDataAdapter _DBAdapter = new SqlDataAdapter(comm);
                //_DBAdapter.Fill(tblprod);
                FillByproducttable3(tblprod, _catalogId, _familyId);
                FillByCfpalSubFamily(tblprodSf, _catalogId, _familyId);
                FillByproducttable2(tblcpa, _catalogId, _familyId);
                FillByCfalSubFamily(tblcpaSf, _catalogId, _familyId);
                dt = GetTransposedProductTable(tblprod, tblcpa);
                dt2 = GetTransposedProductTable(tblprodSf, tblcpaSf);

            }
            //if (_FamilyId == 0)
            //    dt.Merge(dt2);

            dt.Columns.Add("FAMILYID", System.Type.GetType("System.Int32"));

            foreach (DataRow dr in dt.Rows)
            {
                dr["FAMILYID"] = dr["FAMILY_ID"];
            }
            return dt;
        }
        public DataTable GetSubProductTableRow()
        {
            var dt = new DataTable("Product");
            var dt2 = new DataTable("Product2");
            var dsReturn = new DataSet();
            // var taprod = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter();
            var tblprod = new DataTable();
            var tblprodSf = new DataTable();

            //var tacpa = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTTableAdapter();
            var tblcpa = new DataTable();
            var tblcpaSf = new DataTable();

            if (_familyId == 0)
            {
                //Load all families if no id is sent and for all catalog
                if (_catalogId == 0)
                {
                    FillCfpa(tblprod);
                    FillCfalproduct(tblcpa);
                }
                else
                {
                    //Load all families for a specific catalog  
                    string listAttr = "";
                    if (_attributeIdList.Length == 0)
                    {
                        _attributeIdList = new int[1];
                        _attributeIdList[0] = 1;
                        listAttr = "1";
                    }
                    else
                    {
                        listAttr = _attributeIdList[0].ToString();
                        int[] chkAttr = _attributeIdList;
                        _attributeIdList = new int[chkAttr.Length + 1];
                        for (int vaaa = 0; vaaa < chkAttr.Length; vaaa++)
                        {
                            _attributeIdList[vaaa] = chkAttr[vaaa];
                        }
                        _attributeIdList[_attributeIdList.Length - 1] = 1;
                    }
                    for (int vaa = 1; vaa < _attributeIdList.Length; vaa++)
                    {
                        listAttr = listAttr + "," + _attributeIdList[vaa].ToString();
                    }

                    FillByproducttable1(dsReturn, _catalogId, listAttr);
                    // _DBAdapter.Fill(dsReturn);
                    //tblprod = dsReturn.Tables[0];
                    //taprod.FillByAttributeList(tblprod, _CatalogId,,"1,2");// .FillByCatalog(tblprod, _CatalogId);
                    FillByCfalproductCatalog(tblcpa, _catalogId);

                    //dt = GetTransposedProductTable(dsReturn.Tables[0], tblcpa);
                    //dt2 = GetTransposedProductTable(tblprodSF, tblcpaSF);
                    dt = dsReturn.Tables[1].Clone();
                    foreach (DataRow dr in dsReturn.Tables[1].Rows)
                        dt.ImportRow(dr);

                }
            }
            else
            {
                //string catfamquery = "EXEC  " + _familyId + "," + _catalogId;
                //CSDBProvider.Connection conn = new TradingBell.CatalogX.CSDBProvider.Connection();
                //SqlConnection Ocon = new SqlConnection(conn.GetConnSettings());
                //SqlCommand comm = new SqlCommand(catfamquery, Ocon);
                //comm.CommandTimeout = 0;
                //SqlDataAdapter _DBAdapter = new SqlDataAdapter(comm);
                //_DBAdapter.Fill(tblprod);
                FillByproducttable3(tblprod, _catalogId, _familyId);
                FillByCfpalSubFamily(tblprodSf, _catalogId, _familyId);
                FillByproducttable2(tblcpa, _catalogId, _familyId);
                FillByCfalSubFamily(tblcpaSf, _catalogId, _familyId);
                dt = GetTransposedProductTable(tblprod, tblcpa);
                dt2 = GetTransposedProductTable(tblprodSf, tblcpaSf);

            }
            //if (_FamilyId == 0)
            //    dt.Merge(dt2);

            dt.Columns.Add("FAMILYID", System.Type.GetType("System.Int32"));

            foreach (DataRow dr in dt.Rows)
            {
                dr["FAMILYID"] = dr["FAMILY_ID"];
            }
            return dt;
        }
        public bool PDFCatalog = false;
        private DataTable GetTransposedProductTable(DataTable productInfo, DataTable productAttributes)
        {

            var dtnew = new DataTable();
            dtnew.Columns.Add("CATALOG_ID", typeof(int));
            dtnew.Columns.Add("FAMILY_ID", typeof(int));
            dtnew.Columns.Add("PRODUCT_ID", typeof(int));
            if (PDFCatalog)
                dtnew.Columns.Add("SORT_ORDER", typeof(int));

            var dcpks = new DataColumn[3];
            if (PDFCatalog)
            {
                dcpks = new DataColumn[4];
                dcpks[0] = dtnew.Columns["CATALOG_ID"];
                dcpks[1] = dtnew.Columns["FAMILY_ID"];
                dcpks[2] = dtnew.Columns["PRODUCT_ID"];
                dcpks[3] = dtnew.Columns["SORT_ORDER"];
            }
            else
            {
                dcpks[0] = dtnew.Columns["CATALOG_ID"];
                dcpks[1] = dtnew.Columns["FAMILY_ID"];
                dcpks[2] = dtnew.Columns["PRODUCT_ID"];
            }

            dtnew.Constraints.Add("PK", dcpks, true);
            foreach (DataRow dr in productAttributes.Rows)
            {

                if (_attributeIdList.Length > 0)
                {
                    bool exists = CheckExistsAny(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));
                    if (exists)
                    {
                        //_AttributeIdList[iCtr]
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                        dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        {
                            productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString(), typeof(Double));
                        }
                        else
                        {
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                        }


                        //enhance - Make sure to check if the column exists in this table

                        //OLD CODE------------
                        //DataRow[] drs = ProductInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"], "FAMILY_ID,SORT_ORDER");
                        //DataTable dtsel;
                        //dtsel = ProductInfo.Clone();
                        //for (int i = 0; i < drs.Length; i++)
                        //{
                        //    dtsel.ImportRow(drs[i]);
                        //}
                        //-------------------
                        DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"], "FAMILY_ID,SORT_ORDER");
                        var dtsel = new DataTable();
                        if (drs.Any())
                        {
                            dtsel = drs.CopyToDataTable();
                        }

                        try
                        {
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" && dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                                {
                                    bool stringValue = dtsel.Rows.Cast<DataRow>().Any(dr1 => dr1["STRING_VALUE"].ToString().Length == 0);
                                    if (stringValue == false)
                                    {
                                        dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                        dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                    }
                                }
                        }
                        catch (Exception) { }
                        dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                        dtsel.Dispose();
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                            dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                        else
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                    }
                }
                else
                {
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    else
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();




                    //enhance - Make sure to check if the column exists in this table
                    //if(dtnew.Columns[dr["ATTRIBUTE_NAME"]].ColumnName.Contains(dr["ATTRIBUTE_NAME"].ToString()))

                    dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                    DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"]);
                    DataTable dtsel;
                    dtsel = productInfo.Clone();
                    for (int i = 0; i < drs.Length; i++)
                    {
                        dtsel.ImportRow(drs[i]);
                        //    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                        //dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        //    {
                        //        dtsel.Rows[i].ItemArray[7] = 1;// Convert.ToDecimal(dtsel.Rows[i].ItemArray[0]);
                        //    }
                    }
                    // int i = 0;
                    //foreach (DataRow dr2 in dtnew.Rows)
                    //{
                    //    foreach (DataRow dr1 in dtsel.Rows)
                    //    {
                    //        dr2[dr["ATTRIBUTE_NAME"].ToString()] = dr1["STRING_VALUE"].ToString();
                    //        break;
                    //    }
                    //}
                    try
                    {
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" && dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                            {
                                bool stringValue = false;
                                foreach (DataRow dr1 in dtsel.Rows)
                                {
                                    if (dr1["STRING_VALUE"].ToString().Length == 0)
                                    {
                                        stringValue = true;
                                        break;
                                    }
                                }
                                if (stringValue == false)
                                {
                                    dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                    dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                }
                            }
                    }
                    catch (Exception) { }
                    dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                    dtsel.Dispose();
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                    else
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";

                }
            }

            return dtnew;
        }

        private bool CheckExistsAny(int iattrID)
        {
            if (iattrID == 0)
            {
                //  return true;
            }
            return _attributeIdList.Any(t => iattrID == t);
        }

        private DataTable GetTransposedFamily(DataTable FamilyInfo, DataTable FamilyAttributes, bool IsSubFamily)
        {
            int iextracols = 2;
            DataTable dtnew = new DataTable();
            dtnew.Columns.Add("CATALOG_ID", System.Type.GetType("System.Int32"));
            dtnew.Columns.Add("FAMILY_ID", System.Type.GetType("System.Int32"));

            if (IsSubFamily)
            {
                dtnew.Columns.Add("SUBFAMILY_ID", System.Type.GetType("System.Int32"));
                iextracols = 3;
            }

            DataColumn[] dcpks = new DataColumn[iextracols];
            dcpks[0] = dtnew.Columns["CATALOG_ID"];
            dcpks[1] = dtnew.Columns["FAMILY_ID"];

            if (IsSubFamily)
            {
                dcpks[2] = dtnew.Columns["SUBFAMILY_ID"];
            }

            dtnew.Constraints.Add("PK", dcpks, true);

            foreach (DataRow dr in FamilyAttributes.Rows)
            {
                if (_attributeIdList.Length > 0)
                {
                    bool exists = CheckExists(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));
                    if (exists == true)
                    {
                        FamilyInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();

                        dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                        DataRow[] drs = FamilyInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"]);
                        DataTable dtsel;
                        dtsel = FamilyInfo.Clone();
                        for (int i = 0; i < drs.Length; i++)
                        {
                            dtsel.ImportRow(drs[i]);
                        }
                        dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                        dtsel.Dispose();
                        FamilyInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                    }
                }
                else
                {
                    FamilyInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();

                    dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                    DataRow[] drs = FamilyInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"]);
                    DataTable dtsel;
                    dtsel = FamilyInfo.Clone();
                    for (int i = 0; i < drs.Length; i++)
                    {
                        dtsel.ImportRow(drs[i]);
                    }
                    dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                    dtsel.Dispose();
                    FamilyInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                }
            }
            return dtnew;
        }
        private bool CheckExists(int IattrID)
        {
            for (int i = 0; i < _attributeIdList.Length; i++)
            {
                if (IattrID == _attributeIdList[i])
                {
                    return true;
                }
            }
            return false;
        }
        public DataSet JoinedTableNew(DataSet old)
        {
            var jt = new DataTable("JoinedTable");
            int colplus = 8;
            var attrname = new string[old.Tables[0].Columns.Count + old.Tables[1].Columns.Count];
            var attrtype = new string[old.Tables[0].Columns.Count + old.Tables[1].Columns.Count];

            attrname[0] = "CATALOG_ID";
            attrtype[0] = "System.String";
            attrname[1] = "CATALOG_NAME";
            attrtype[1] = "System.String";
            attrname[2] = "CATALOG_VERSION";
            attrtype[2] = "System.String";
            attrname[3] = "CATALOG_DESCRIPTION";
            attrtype[3] = "System.String";
            attrname[4] = "CATALOG_FAMILY_FILTERS";
            attrtype[4] = "System.String";
            attrname[5] = "CATALOG_PRODUCT_FILTERS";
            attrtype[5] = "System.String";
            attrname[6] = "CATEGORY_ID";
            attrtype[6] = "System.String";
            attrname[7] = "CATEGORY_NAME";
            attrtype[7] = "System.String";
            int[] continu = { 1 };
            for (int val = old.Tables[0].Columns.Count - 3; val > 0; val = val - 2)
            {
                if (old.Tables[0].Columns[val].ToString() != "CATEGORY_ID")
                {
                    DataRow[] count1 = old.Tables[0].Select(old.Tables[0].Columns[val] + " <> ''");
                    if (count1.Length == 0)
                    {
                        old.Tables[0].Columns.Remove(old.Tables[0].Columns[val]);
                        old.Tables[0].Columns.Remove(old.Tables[0].Columns[val]);
                    }
                }
                else
                    break;
            }

            foreach (DataColumn dcolumn in old.Tables[0].Columns.Cast<DataColumn>().Where(dcolumn => ((dcolumn.Caption == "SUBCATID_L1") || (continu[0] == 0)) && (dcolumn.Caption != "FAMILYID")))
            {
                continu[0] = 0;
                attrname[colplus] = dcolumn.Caption;
                attrtype[colplus] = dcolumn.DataType.ToString();
                colplus++;
            }
            continu[0] = 1;
            foreach (DataColumn dcolumn in old.Tables[0].Columns)
            {
                if (dcolumn.Caption == "CATEGORY_ID" || dcolumn.Caption == "CATALOG_VERSION")
                {
                    continu[0] = 1;
                }
                if ((dcolumn.Caption == "CATEGORY_SHORT_DESC") || (continu[0] == 0))
                {
                    continu[0] = 0;
                    attrname[colplus] = dcolumn.Caption;
                    attrtype[colplus] = dcolumn.DataType.ToString();
                    colplus++;
                }

            }
            attrname[colplus] = "FAMILY_ID";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "FAMILY_NAME";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "SUBFAMILY_ID";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "SUBFAMILY_NAME";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "FOOT_NOTES";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "STATUS";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "PRODUCT_TABLE_STRUCTURE";
            attrtype[colplus] = "System.String"; colplus++;
            //attrname[Colplus] = "DISPLAY_TABLE_HEADER";
            attrtype[colplus] = "System.String"; colplus++;

            continu[0] = 1;
            foreach (DataColumn dcolumn in old.Tables[0].Columns)
            {
                if (dcolumn.Caption == "SUBFAMILY_ID")
                {
                    continu[0] = 1;
                }
                if ((continu[0] == 0))
                {
                    continu[0] = 0;
                    attrname[colplus] = dcolumn.Caption;
                    attrtype[colplus] = dcolumn.DataType.ToString();
                    colplus++;
                }
                if (dcolumn.Caption == "FAMILY_ID")
                {
                    continu[0] = 0;
                }

            }

            continu[0] = 1;

            if (old.Tables[1].Columns[0].ToString() == "ATTRIBUTE_NAME")
            {
                attrname[colplus] = "PRODUCT_ID";
                attrtype[colplus] = "System.Int32"; colplus++;
                attrname[colplus] = "ATTRIBUTE_ID";
                attrtype[colplus] = "System.Int32"; colplus++;
                attrname[colplus] = "ATTRIBUTE_NAME";
                attrtype[colplus] = "System.String"; colplus++;
                attrname[colplus] = "STRING_VALUE";
                attrtype[colplus] = "System.String"; colplus++;
                attrname[colplus] = "NUMERIC_VALUE";
                attrtype[colplus] = "System.Double"; colplus++;
                attrname[colplus] = "OBJECT_NAME";
                attrtype[colplus] = "System.String"; colplus++;
                attrname[colplus] = "OBJECT_TYPE";
                attrtype[colplus] = "System.String";
            }
            else
            {
                foreach (DataColumn dcolumn in old.Tables[1].Columns.Cast<DataColumn>().Where(dcolumn => ((dcolumn.Caption == "PRODUCT_ID") || (continu[0] == 0)) && (dcolumn.Caption != "FAMILYID")))
                {
                    continu[0] = 0;
                    attrname[colplus] = dcolumn.Caption;
                    attrtype[colplus] = dcolumn.DataType.ToString();
                    colplus++;
                }
            }


            for (int dco = 0; dco < attrname.Length; dco++)
            {
                try
                {
                    if (attrname[dco] != null)
                        if (attrname[dco].Trim() != "" && attrname[dco] != null)
                        {
                            if (attrtype[dco] == "System.Int32")
                                jt.Columns.Add(attrname[dco], typeof(Int32));
                            else if (attrtype[dco] == "System.Double")
                                jt.Columns.Add(attrname[dco], typeof(Double));
                            else if (attrtype[dco] == "System.Decimal")
                                jt.Columns.Add(attrname[dco], typeof(Decimal));
                            else
                                jt.Columns.Add(attrname[dco]);
                        }
                }
                catch (Exception)
                {
                }
            }

            old.Tables.Add(jt);
            int familyIdFlag = 0;
            foreach (DataRow dr in old.Tables[0].Select("", "Family_id"))
            {

                if (dr["FAMILYID"].ToString().Trim().Length > 0 && familyIdFlag != Convert.ToInt32(dr["FAMILYID"]))
                {
                    DataRow[] parent = old.Tables[1].Select("CATALOG_ID = " + dr["CATALOG_ID"] + " AND FAMILYID = " + dr["FAMILYID"]);
                    familyIdFlag = Convert.ToInt32(dr["FAMILYID"]);

                    if (parent.Length > 0)
                        foreach (DataRow drr in parent)
                        {
                            DataRow paren = drr;

                            DataRow current = jt.NewRow();

                            for (int i = 0; i < old.Tables[0].Columns.Count; i++)
                            {
                                if (old.Tables[0].Columns[i].ToString() != "SORT_ORDER" && old.Tables[0].Columns[i].ToString() != "FAMILYORDER" && old.Tables[0].Columns[i].ToString() != "PARENT_CATEGORY" && old.Tables[0].Columns[i].ToString() != "FAMILYID")
                                    current[old.Tables[0].Columns[i].ColumnName] = dr[old.Tables[0].Columns[i].ColumnName];

                            }

                            for (int i = 0; i < old.Tables[1].Columns.Count - 1; i++)
                            {
                                if (old.Tables[1].Columns[i].ToString() != "SORT_ORDER" && old.Tables[1].Columns[i].ToString() != "FAMILY_ID")
                                    current[old.Tables[1].Columns[i].ColumnName] = paren[old.Tables[1].Columns[i].ColumnName];
                            }
                            // if (paren["SUBFAMILY_ID"].Length != 0)
                            {
                                //current["FAMILY_ID"] = paren["FAMILY_ID"];
                            }
                            jt.Rows.Add(current);

                        }
                    //jt.Rows.Clear();//kalai
                }

            }


            foreach (DataRow dr in from DataRow dr in old.Tables[0].Rows where dr["FAMILYID"].ToString().Length > 0 let childs = old.Tables[1].Select("CATALOG_ID = " + dr["CATALOG_ID"] + " AND FAMILYID = " + dr["FAMILYID"]) where childs.Length == 0 select dr)
            {
                jt.NewRow();
                jt.ImportRow(dr);
            }


            DataRow[] tempR = (old.Tables[3].Select("FAMILY_ID=0"));
            foreach (DataRow dr in tempR)
            {
                dr["FAMILY_ID"] = "";
            }
            jt.Columns.Remove("PRODUCT_TABLE_STRUCTURE");
            old.Tables[0].Columns.Remove("PRODUCT_TABLE_STRUCTURE");
            return old;


        }
        public DataSet JoinedTableNewSub(DataSet old)
        {
            var jt = new DataTable("JoinedTableSub");
            int colplus = 8;
            var attrname = new string[old.Tables[0].Columns.Count + old.Tables[2].Columns.Count];
            var attrtype = new string[old.Tables[0].Columns.Count + old.Tables[2].Columns.Count];

            attrname[0] = "CATALOG_ID";
            attrtype[0] = "System.String";
            attrname[1] = "CATALOG_NAME";
            attrtype[1] = "System.String";
            attrname[2] = "CATALOG_VERSION";
            attrtype[2] = "System.String";
            attrname[3] = "CATALOG_DESCRIPTION";
            attrtype[3] = "System.String";
            attrname[4] = "CATALOG_FAMILY_FILTERS";
            attrtype[4] = "System.String";
            attrname[5] = "CATALOG_PRODUCT_FILTERS";
            attrtype[5] = "System.String";
            attrname[6] = "CATEGORY_ID";
            attrtype[6] = "System.String";
            attrname[7] = "CATEGORY_NAME";
            attrtype[7] = "System.String";
            int[] continu = { 1 };
            for (int val = old.Tables[0].Columns.Count - 3; val > 0; val = val - 2)
            {
                if (old.Tables[0].Columns[val].ToString() != "CATEGORY_ID")
                {
                    DataRow[] count1 = old.Tables[0].Select(old.Tables[0].Columns[val] + " <> ''");
                    if (count1.Length == 0)
                    {
                        old.Tables[0].Columns.Remove(old.Tables[0].Columns[val]);
                        old.Tables[0].Columns.Remove(old.Tables[0].Columns[val]);
                    }
                }
                else
                    break;
            }

            foreach (DataColumn dcolumn in old.Tables[0].Columns.Cast<DataColumn>().Where(dcolumn => ((dcolumn.Caption == "SUBCATID_L1") || (continu[0] == 0)) && (dcolumn.Caption != "FAMILYID")))
            {
                continu[0] = 0;
                attrname[colplus] = dcolumn.Caption;
                attrtype[colplus] = dcolumn.DataType.ToString();
                colplus++;
            }
            continu[0] = 1;
            foreach (DataColumn dcolumn in old.Tables[0].Columns)
            {
                if (dcolumn.Caption == "CATEGORY_ID" || dcolumn.Caption == "CATALOG_VERSION")
                {
                    continu[0] = 1;
                }
                if ((dcolumn.Caption == "CATEGORY_SHORT_DESC") || (continu[0] == 0))
                {
                    continu[0] = 0;
                    attrname[colplus] = dcolumn.Caption;
                    attrtype[colplus] = dcolumn.DataType.ToString();
                    colplus++;
                }

            }
            attrname[colplus] = "FAMILY_ID";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "FAMILY_NAME";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "SUBFAMILY_ID";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "SUBFAMILY_NAME";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "FOOT_NOTES";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "STATUS";
            attrtype[colplus] = "System.String"; colplus++;
            attrname[colplus] = "PRODUCT_TABLE_STRUCTURE";
            attrtype[colplus] = "System.String"; colplus++;
            //attrname[Colplus] = "DISPLAY_TABLE_HEADER";
            attrtype[colplus] = "System.String"; colplus++;

            continu[0] = 1;
            foreach (DataColumn dcolumn in old.Tables[0].Columns)
            {
                if (dcolumn.Caption == "SUBFAMILY_ID")
                {
                    continu[0] = 1;
                }
                if ((continu[0] == 0))
                {
                    continu[0] = 0;
                    attrname[colplus] = dcolumn.Caption;
                    attrtype[colplus] = dcolumn.DataType.ToString();
                    colplus++;
                }
                if (dcolumn.Caption == "FAMILY_ID")
                {
                    continu[0] = 0;
                }

            }

            continu[0] = 1;

            if (old.Tables[2].Columns[0].ToString() == "ATTRIBUTE_NAME")
            {
                attrname[colplus] = "PRODUCT_ID";
                attrtype[colplus] = "System.Int32"; colplus++;
                attrname[colplus] = "SUBPRODUCT_ID";
                attrtype[colplus] = "System.Int32"; colplus++;
                attrname[colplus] = "ATTRIBUTE_ID";
                attrtype[colplus] = "System.Int32"; colplus++;
                attrname[colplus] = "ATTRIBUTE_NAME";
                attrtype[colplus] = "System.String"; colplus++;
                attrname[colplus] = "STRING_VALUE";
                attrtype[colplus] = "System.String"; colplus++;
                attrname[colplus] = "NUMERIC_VALUE";
                attrtype[colplus] = "System.Double"; colplus++;
                attrname[colplus] = "OBJECT_NAME";
                attrtype[colplus] = "System.String"; colplus++;
                attrname[colplus] = "OBJECT_TYPE";
                attrtype[colplus] = "System.String";
            }
            else
            {
                foreach (DataColumn dcolumn in old.Tables[2].Columns.Cast<DataColumn>().Where(dcolumn => ((dcolumn.Caption == "PRODUCT_ID") || (continu[0] == 0)) && (dcolumn.Caption != "FAMILYID")))
                {
                    continu[0] = 0;
                    attrname[colplus] = dcolumn.Caption;
                    attrtype[colplus] = dcolumn.DataType.ToString();
                    colplus++;
                }
            }


            for (int dco = 0; dco < attrname.Length; dco++)
            {
                try
                {
                    if (attrname[dco] != null)
                        if (attrname[dco].Trim() != "" && attrname[dco] != null)
                        {
                            if (attrtype[dco] == "System.Int32")
                                jt.Columns.Add(attrname[dco], typeof(Int32));
                            else if (attrtype[dco] == "System.Double")
                                jt.Columns.Add(attrname[dco], typeof(Double));
                            else if (attrtype[dco] == "System.Decimal")
                                jt.Columns.Add(attrname[dco], typeof(Decimal));
                            else
                                jt.Columns.Add(attrname[dco]);
                        }
                }
                catch (Exception)
                {
                }
            }

            old.Tables.Add(jt);
            int familyIdFlag = 0;
            foreach (DataRow dr in old.Tables[0].Select("", "Family_id"))
            {

                if (dr["FAMILYID"].ToString().Trim().Length > 0 && familyIdFlag != Convert.ToInt32(dr["FAMILYID"]))
                {
                    DataRow[] parent = old.Tables[2].Select("CATALOG_ID = " + dr["CATALOG_ID"] + " AND FAMILYID = " + dr["FAMILYID"]);
                    familyIdFlag = Convert.ToInt32(dr["FAMILYID"]);

                    if (parent.Length > 0)
                        foreach (DataRow drr in parent)
                        {
                            DataRow paren = drr;

                            DataRow current = jt.NewRow();

                            for (int i = 0; i < old.Tables[0].Columns.Count; i++)
                            {
                                if (old.Tables[0].Columns[i].ToString() != "SORT_ORDER" && old.Tables[0].Columns[i].ToString() != "FAMILYORDER" && old.Tables[0].Columns[i].ToString() != "PARENT_CATEGORY" && old.Tables[0].Columns[i].ToString() != "FAMILYID")
                                    current[old.Tables[0].Columns[i].ColumnName] = dr[old.Tables[0].Columns[i].ColumnName];

                            }

                            for (int i = 0; i < old.Tables[2].Columns.Count - 1; i++)
                            {
                                if (old.Tables[2].Columns[i].ToString() != "SORT_ORDER" && old.Tables[2].Columns[i].ToString() != "FAMILY_ID")
                                    current[old.Tables[2].Columns[i].ColumnName] = paren[old.Tables[2].Columns[i].ColumnName];
                            }
                            // if (paren["SUBFAMILY_ID"].Length != 0)
                            {
                                //current["FAMILY_ID"] = paren["FAMILY_ID"];
                            }
                            jt.Rows.Add(current);

                        }
                    //jt.Rows.Clear();//kalai
                }

            }


            //////////foreach (DataRow dr in from DataRow dr in old.Tables[0].Rows where dr["FAMILYID"].ToString().Length > 0 let childs = old.Tables[2].Select("CATALOG_ID = " + dr["CATALOG_ID"] + " AND FAMILYID = " + dr["FAMILYID"] ) where childs.Length == 0 select dr)
            //////////{
            //////////    jt.NewRow();
            //////////    jt.ImportRow(dr);
            //////////}


            DataRow[] tempR = (old.Tables[4].Select("FAMILY_ID=0"));
            foreach (DataRow dr in tempR)
            {
                dr["FAMILY_ID"] = "";
            }
            jt.Columns.Remove("PRODUCT_TABLE_STRUCTURE");
           // old.Tables[0].Columns.Remove("PRODUCT_TABLE_STRUCTURE");
            return old;


        }


        public DataTable FillCf(DataTable objDataTable)
        {
            try
            {

                using (
                            var objSqlConnection =
                                new SqlConnection(
                                    ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCF";
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataTable);
                }
                return objDataTable;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable FillByCfCatalog(DataTable objDataTable, int catalogId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFCATALOG";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }
        public DataTable FillByCfFamily(DataTable objDataTable, int catalogId, int familyId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFFAMILY";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }

        public DataTable FillC(DataTable objDataTable)
        {
            try
            {

                using (
                            var objSqlConnection =
                                new SqlConnection(
                                    ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLC";
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataTable);
                }
                return objDataTable;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable FillByCCatalog(DataTable objDataTable, int catalogId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCCATALOG";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }
        public DataTable FillByCCategory(DataTable objDataTable, string categoryid)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCCATEGORY";
                objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryid;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }


        public DataTable FillCfa(DataTable objDataTable)
        {
            try
            {

                using (
                            var objSqlConnection =
                                new SqlConnection(
                                    ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFA";
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataTable);
                }
                return objDataTable;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable FillByCfaCatalog(DataTable objDataTable, int catalogId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFACATALOG";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }
        public DataTable FillByCfaFamily(DataTable objDataTable, int catalogId, int familyId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFAFAMILY";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }

        public DataTable FillCsfa(DataTable objDataTable)
        {
            try
            {

                using (
                            var objSqlConnection =
                                new SqlConnection(
                                    ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCSFA";
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataTable);
                }
                return objDataTable;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable FillByCsfaCatalog(DataTable objDataTable, int catalogId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCSFACATALOG";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }
        public DataTable FillByCsfaFamily(DataTable objDataTable, int catalogId, int familyId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCSFAFAMILY";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }

        public DataTable FillCfal(DataTable objDataTable)
        {
            try
            {

                using (
                            var objSqlConnection =
                                new SqlConnection(
                                    ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFAL";
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataTable);
                }
                return objDataTable;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable FillByCfalCatalog(DataTable objDataTable, int catalogId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFALCATALOG";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }
        public DataTable FillByCfalFamily(DataTable objDataTable, int catalogId, int familyId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFALFAMILY";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }

        public DataTable FillCfpa(DataTable objDataTable)
        {
            try
            {

                using (
                            var objSqlConnection =
                                new SqlConnection(
                                    ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFPA";
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataTable);
                }
                return objDataTable;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable FillCfalproduct(DataTable objDataTable)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFALPRODUCT";
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }
        public DataTable FillByCfalSubFamily(DataTable objDataTable, int catalogId, int familyId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFALSUBFAMILY";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }


        public DataTable FillByCfalproductCatalog(DataTable objDataTable, int catalogId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFALPRODUCTCATALOG";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }
        public DataTable FillByCfpalSubFamily(DataTable objDataTable, int catalogId, int familyId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "FILLCFPALSUBFAMILY";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }

        public DataSet FillByproducttable1(DataSet objDataTable, int catalogId, string attributeids)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.CommandTimeout = 0;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "PRODUCTTABLE1";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@ATTR_IDS", SqlDbType.NVarChar).Value = attributeids;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }

        public DataTable FillByproducttable2(DataTable objDataTable, int catalogId, int familyId)
        {
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_EXPORT_ROW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.CommandTimeout = 0;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 500).Value = "PRODUCTTABLE2";
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }

        public DataTable FillByproducttable3(DataTable objDataTable, int catalogId, int familyId)
        {
      
            string category_Id = string.Empty;
            var catedet = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.CATALOG_ID == catalogId && a.FAMILY_ID == familyId);
            if (catedet != null)
            {
                category_Id = catedet.CATEGORY_ID;
            }
            using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_FILLBYCATALOG_FAMILIY";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
               objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.Int).Value = category_Id;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objDataTable);
            }
            return objDataTable;
        }
    }

    public class ExportSettings
    {
        [XmlElement("AttrbuteSettings")]
        public List<AttrbuteSettings> objAttributeSettings = new List<AttrbuteSettings>();

    }
    public class AttrbuteSettings
    {
        public string AttributeName { get; set; }
        public string AttrBy { get; set; }
        public string Filters { get; set; }
        public string SystemAttribute { get; set; }
        public string CustomAttribute { get; set; }
        public int Catalog_ID { get; set; }
        public string Category_ID { get; set; }
    }

}
