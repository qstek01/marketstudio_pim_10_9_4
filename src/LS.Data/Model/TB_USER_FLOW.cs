﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class TB_USER_FLOW
    {
        public string TB_USER_ID { get; set; }
        public Nullable<bool> NEW { get; set; }
        public Nullable<bool> UPDATED { get; set; }
        public Nullable<bool> REVIEW_PROCESS { get; set; }
        public Nullable<bool> SUBMIT { get; set; }
        public Nullable<bool> APPROVE { get; set; }
    }
}
