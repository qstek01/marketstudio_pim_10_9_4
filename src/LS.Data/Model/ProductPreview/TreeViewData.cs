﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model.ProductPreview
{
     public class TreeViewData
    {
        public int id { get; set; }
        public int  ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public string COLOR { get; set; }
        public bool hasChildren { get; set; }
        public string Merge { get; set; }
         
    }

     public class TreeViewSubNodeData
     {
         public int ATTRIBUTE_ID { get; set; }
         public string ATTRIBUTE_NAME { get; set; }
         public bool hasChildren { get; set; }
     }


     public class NodeViewModel
     {
         public NodeViewModel()
         {
             this.COLOR = "Blue";
             this.items = new List<NodeViewModel>();
             this.hasChildren = true;
         }

         public   string id { get; set; }
         public int ATTRIBUTE_ID { get; set; }
         public string ATTRIBUTE_NAME { get; set; }
         public string COLOR { get; set; }
         public bool hasChildren { get; set; }
         public string Merge { get; set; }
         public IList<NodeViewModel> items { get; private set; }
     }


     public class ConstructNodeViewModel
     {
         public string id { get; set; }
         public int ATTRIBUTE_ID { get; set; }
         public string ATTRIBUTE_NAME { get; set; }
         public string COLOR { get; set; }
         public bool hasChildren { get; set; }
         public string Merge { get; set; }
         public IList<ConstructNodeViewModel> items { get; set; }
     }
}
