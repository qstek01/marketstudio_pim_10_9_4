﻿namespace LS.Data.Model
{
    public class FamilyFilterValue
    {
        public string STRING_VALUE { get; set; }
        public int ATTRIBUTE_ID { get; set; }
      
    }
    public class DrpFamilyFilter
    {
        public string STRING_VALUE { get; set; }
        public string FAMILY_IDS { get; set; }

    }
    public class State
    {
        public string STATE { get; set; }
        public string STATEVALUE { get; set; }

    }
    public class Sort_Order
    {
        public int PRODUCT_ID { get; set; }
        public int SORT_ORDER { get; set; }

    }
    public class Country
    {
        public string COUNTRY { get; set; }
        public string COUNTRYVALUE { get; set; }

    }

    
}
