﻿using System.Globalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Text.RegularExpressions;
using System.Xml.XPath;
using System.Xml.Xsl;
using log4net;
using System.Drawing;

namespace LS.Data.Model.ProductPreview
{
    public class TableDesigner
    {
        private List<string> _mRowFields;
        private List<string> _mLeftRowFields;
        private List<string> _mRightRowFields;
        private List<string> _mColumnFields;
        private List<string> _mSummaryFields;
        private List<XmlNode> _mLeftRowNodes;
        private List<XmlNode> _mRightRowNodes;
        private List<XmlNode> _mColumnNodes;
        private List<XmlNode> _mSummaryNodes;
        private string _mSummaryGroupField, _mTableGroupField;
        private int _mFamilyID = 2, _mCatalogID = 2;
        private bool _mShowRowHeaders;
        private bool _mShowColumnHeaders;
        private bool _mShowSummaryHeaders;
        private bool _mMergeRowHeaders;
        private bool _mMergeSummaryFields;
        private bool _mVerticalTable;
        private string _mPlaceHolderText, _mPivotHeaderText, _mTableLayoutName;
        private DataTable _pivotTable, _attrTablePivot, _rowTable, _leftRowTable, _rightRowTable, _columnTable, _sourceTable, _attributeTable, _groupTable;

        string _prefix = string.Empty; string _suffix = string.Empty; string _emptyCondition = string.Empty; string _replaceText = string.Empty; string _headeroptions = string.Empty; string _fornumeric = string.Empty;
        readonly Random _randomClass = new Random();
        private string _connectionString = string.Empty;
        private string _imagePath = string.Empty;
        private string _applicationStartupPath = string.Empty;
        private XmlNode _leftRowNode, _rightRowNode;
        private int _headerCnt;


        public int[] AttributeIdList;
        private bool _loadXProductTable;
        private int[] _attributeIdList;
        public bool PdfCatalog = false;

        static readonly ILog Logger = LogManager.GetLogger(typeof(TableDesigner));
        readonly CSEntities _dbcontext = new CSEntities();
        private Image _chkSize;
        string ImageMagickPath = string.Empty;
        public string CategoryId = string.Empty;
        public string CustomerFolder = string.Empty;
        public string converttype = string.Empty;
        public string Exefile = string.Empty;
        public string convertionPath = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePath"].ToString();
        public string convertionPathREA = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePathREA"].ToString();

        public int FamilyId
        {
            set
            {
                _mFamilyID = value;
            }
        }

        private void DecodeLayoutXml(string xmlStr)
        {
            try
            {
                _mRowFields = new List<string>();
                _mSummaryFields = new List<string>();
                _mColumnFields = new List<string>();
                _mLeftRowFields = new List<string>();
                _mRightRowFields = new List<string>();
                _mLeftRowNodes = new List<XmlNode>();
                _mRightRowNodes = new List<XmlNode>();
                _mSummaryNodes = new List<XmlNode>();
                _mColumnNodes = new List<XmlNode>();

                _mPlaceHolderText = "";
                _mSummaryGroupField = "";
                _mTableGroupField = "";
                _mShowColumnHeaders = false;
                _mShowRowHeaders = false;
                _mShowSummaryHeaders = false;
                _mMergeRowHeaders = false;
                _mMergeSummaryFields = false;
                _leftRowNode = null;
                _rightRowNode = null;

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();

                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "LeftRowField")
                            {
                                _leftRowNode = rootNode.Clone();
                                // leftRowNode.(childNodes[i].CloneNode(true));
                                // leftRowNode = childNodes[i].CloneNode(true);

                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        DecodeRowAttr(childNodes[i]);
                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":", StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":", StringComparison.Ordinal) + 1));
                                        SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                       " AND CATALOG_ID = " + _mCatalogID + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                _mRowFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                                _mLeftRowFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                            }
                                            else
                                            {
                                                _mRowFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                                _mLeftRowFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                            }
                                            _mLeftRowNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "RightRowField")
                            {
                                // if (rightRowNode == null)
                                // rightRowNode = childNodes[i].CloneNode(true);
                                _rightRowNode = rootNode.Clone();
                                _rightRowNode.AppendChild(childNodes[i].CloneNode(true));
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        DecodeRowAttr(childNodes[i]);
                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":", StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":", StringComparison.Ordinal) + 1));
                                        SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                       " AND CATALOG_ID = " + _mCatalogID + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                _mRowFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                                _mRightRowFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                            }
                                            else
                                            {
                                                _mRowFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                                _mRightRowFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                            }
                                            _mRightRowNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "ColumnField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    string attrID = childNodes[i].Attributes["AttrID"].Value.Substring(childNodes[i].Attributes["AttrID"].Value.IndexOf(":", StringComparison.Ordinal) + 1, childNodes[i].Attributes["AttrID"].Value.Length - (childNodes[i].Attributes["AttrID"].Value.IndexOf(":", StringComparison.Ordinal) + 1));
                                    SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID + " AND CATALOG_ID = " + _mCatalogID + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        _mColumnFields.Add(childNodes[i].Attributes["AttrID"].Value.Contains("nAttr")
                                                               ? GetAttributeNameFromAttributeID(
                                                                   childNodes[i].Attributes["AttrID"].Value.Remove(0, 6))
                                                               : GetAttributeNameFromAttributeID(
                                                                   childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                        _mColumnNodes.Add(childNodes[i].Clone());
                                    }
                                }
                            }

                            if (childNodes[i].Name == "SummaryField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        DecodeRowAttr(childNodes[i]);
                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":", StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":", StringComparison.Ordinal) + 1));
                                        SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                       " AND CATALOG_ID = " + _mCatalogID + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                _mSummaryFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                                objGroupedSummaryAttrs.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)), "None");
                                            }
                                            else
                                            {
                                                _mSummaryFields.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                                objGroupedSummaryAttrs.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)), "None");
                                            }
                                            _mSummaryNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "SummaryGroupField")
                            {
                                _mSummaryGroupField = "";
                                string attrID = string.Empty;
                                if (childNodes[i].InnerText.Trim() != string.Empty && childNodes[i].InnerText.Trim() != "[none]" && !childNodes[i].InnerText.Contains("? string: ?"))
                                {
                                    if (childNodes[i].InnerText.Count() < 5)
                                    {
                                        attrID = childNodes[i].InnerText.Trim();
                                    }
                                    else
                                    {
                                        attrID = childNodes[i].InnerText.Remove(0, 5);
                                    }
                                    SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID + " AND CATALOG_ID = " + _mCatalogID + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        if (childNodes[i].InnerText.Count() < 5)
                                        {
                                            _mSummaryGroupField = GetAttributeNameFromAttributeID(childNodes[i].InnerText);
                                        }
                                        else
                                        {
                                            _mSummaryGroupField = GetAttributeNameFromAttributeID(childNodes[i].InnerText.Remove(0, 5));
                                        }

                                    }
                                }
                            }

                            if (childNodes[i].Name == "TableGroupField")
                            {
                                _mTableGroupField = "";
                                if (childNodes[i].InnerText.Trim() != string.Empty && childNodes[i].InnerText.Trim() != "[none]")
                                {
                                    string attrID = string.Empty;
                                    if (childNodes[i].InnerText.Length < 5)
                                    {
                                        attrID = childNodes[i].InnerText;

                                    }
                                    else
                                    {
                                        attrID = childNodes[i].InnerText.Remove(0, 5);
                                    }
                                    SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        if (childNodes[i].InnerText.Length < 5)
                                        {
                                            _mTableGroupField = GetAttributeNameFromAttributeID(childNodes[i].InnerText);
                                        }
                                        else
                                        {
                                            _mTableGroupField = GetAttributeNameFromAttributeID(childNodes[i].InnerText.Remove(0, 5));
                                        }
                                }
                            }

                            if (childNodes[i].Name == "PlaceHolderText")
                                _mPlaceHolderText = childNodes[i].InnerText;

                            if (childNodes[i].Name == "DisplayRowHeader")
                                _mShowRowHeaders = Convert.ToBoolean(childNodes[i].InnerText);

                            if (childNodes[i].Name == "DisplayColumnHeader")
                                _mShowColumnHeaders = Convert.ToBoolean(childNodes[i].InnerText);

                            if (childNodes[i].Name == "DisplaySummaryHeader")
                                _mShowSummaryHeaders = Convert.ToBoolean(childNodes[i].InnerText);

                            if (childNodes[i].Name == "PivotHeaderText")
                                _mPivotHeaderText = childNodes[i].InnerText;

                            if (childNodes[i].Name == "VerticalTable")
                                _mVerticalTable = Convert.ToBoolean(childNodes[i].InnerText);
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at Tabledesigner : DecodeLayoutXml", objException);
            }
        }
        Dictionary<string, string> objGroupedSummaryAttrs = new Dictionary<string, string>();
        public TableDesigner()
        {
            SQLString = "";
        }


        public string GenerateTableHtml(string comboSummaryGroupValue, string comboGroupByColumnValue, string emptyCellTextBoxValue, string dispalyRowHeaderCheckEditorValue, string displayColumnHeaderCheckBoxValue, string dispalySummaryHeaderCheckEditorValue, string verticalTableChkBoxValue, string pivotHeaderTextBoxValue, string structureName, int userid, string category_id, string flag, int groupId, int packId, int familyId, int catalogId, int tableId, string preview_flag,int masterCatalogId, string FamilyName,object model)
        {
            //, JArray righttreeViewData, JArray columntreeViewData, JArray summarytreeViewData
            //List<TB_USER_FLOW> WorkFlowitems = ((JArray)model).Select(x => new TB_USER_FLOW
            //{
            //    TB_USER_ID = (string)x["TB_USER_ID"],
            //    NEW = (bool)x["NEW"],
            //    UPDATED = (bool)x["UPDATED"],s
            //    REVIEW_PROCESS = (bool)x["REVIEW_PROCESS"],
            //    SUBMIT = (bool)x["SUBMIT"],
            //    APPROVE = (bool)x["APPROVE"]
            //}).ToList();
            CategoryId = category_id;
            var imagemagickpath = _dbcontext.Customer_Settings.FirstOrDefault(a => a.CustomerId == userid);
            string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == userid).Select(a => a.ConverterType).SingleOrDefault();
            if (imagemagickpath != null && converttype == "Image Magic")
            {
                ImageMagickPath = convertionPath;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
            }
            else
            {

                ImageMagickPath = convertionPathREA;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
            }
            string layoutXml = string.Empty;
            if (flag == "FamilyTableDefault")
            {
                layoutXml = ConstructLayoutXmlDefaultPeview(comboSummaryGroupValue, comboGroupByColumnValue, emptyCellTextBoxValue, dispalyRowHeaderCheckEditorValue, displayColumnHeaderCheckBoxValue, dispalySummaryHeaderCheckEditorValue, verticalTableChkBoxValue, pivotHeaderTextBoxValue, model);
                flag = "FamilyTable";
                return layoutXml;
            }
            else
            {
                layoutXml = ConstructLayoutXml(comboSummaryGroupValue, comboGroupByColumnValue, emptyCellTextBoxValue, dispalyRowHeaderCheckEditorValue, displayColumnHeaderCheckBoxValue, dispalySummaryHeaderCheckEditorValue, verticalTableChkBoxValue, pivotHeaderTextBoxValue, model);
            }

            string familyName = string.Empty;
            SQLString = @"SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID = " + FamilyID + "";
            DataSet ds = CreateDataSet();
            DataTable familyNameTable = ds.Tables[0];
            if (familyNameTable != null && familyNameTable.Rows.Count > 0)
                familyName = familyNameTable.Rows[0][0].ToString();
            string xmlStr = string.Empty;
            if (packId > 0)
            {
                string packflag = "Update";
                if (preview_flag == "")
                    SaveAttributePackTablestructure(layoutXml, familyId, packId, catalogId, packflag, tableId);
                TableLayoutName = _mTableLayoutName;
                ConnectionString = _connectionString;
                DecodeLayoutXml(layoutXml);
            }
            else
            {
                SaveTableLayout(layoutXml, structureName, flag, groupId, masterCatalogId, FamilyName);

                // pplicationStartupPath = Application.StartupPath;
                TableLayoutName = _mTableLayoutName;
                ConnectionString = _connectionString;

                //SystemSettingsCollection settingMembers = SystemSettingsConfiguration.GetConfig.Members;
                // ImagePath = (HttpContext.Current.Server.MapPath("~/Content/css/images/master.css"));
                // string xmlStr = string.Empty;
                //Retrieve the Pivot Layout from Database
                DecodeLayoutXml(RetrieveTableLayout(structureName, flag, groupId));
            }
            if (_mRowFields.Count > 0)
            {
                FetchSourceData();
                SetAttributePropertiesForSourceTable();
                SplitRowtableInToLeftAndRight();
                ConstructGroupedPivot();
                _mMergeSummaryFields = true;
                _mMergeRowHeaders = true;
                int FamilyPreview = 0;
                xmlStr = _mVerticalTable ? GenerateVerticalXmlForTransformation(FamilyPreview, userid) : GenerateXmlForTransformation(FamilyPreview, userid);
                var xTransform = new XmlTransformer();
                xmlStr = xTransform.TransformToHtml(xmlStr);


                int i = 0;
                DataSet products_img = new DataSet();
                SQLString = " select * from tb_prod_specs tp "
        + " join" +
     "  tb_prod_family ts on ts.product_id = tp.product_id where family_id =" + FamilyID + " and attribute_id in( select attribute_id from tb_attribute where attribute_type=3) and String_value is not null";
                products_img = CreateDataSet();


                i = products_img.Tables[0].Rows.Count;


                string htmlString = xmlStr;

                int k = 0;

                while (xmlStr.IndexOf("<img", StringComparison.Ordinal) > 0)
                {
                    string prod_img = products_img.Tables[0].Rows[k][1].ToString();
                    string att_id = products_img.Tables[0].Rows[k][2].ToString();

                    int index = xmlStr.IndexOf("<img", StringComparison.Ordinal);
                    xmlStr = xmlStr.Substring(index);
                    int endIndex = xmlStr.IndexOf("/>", StringComparison.Ordinal);
                    string imgTag = xmlStr.Substring(0, endIndex + 2);
                    xmlStr = xmlStr.Substring(endIndex + 2);
                    string imgSource = imgTag.Substring(imgTag.IndexOf("src=", StringComparison.Ordinal) + 4);
                    imgSource = imgSource.Replace("\"", "");
                     // imgSource = imgSource.Replace(".psd", "");
                    imgSource = imgSource.Replace("&amp;", "&");
                    if (imgSource.Length > 4)
                    {
                        DataSet forobjpreview = new DataSet();
                        SQLString = " SELECT OBJECT_NAME FROM TB_PROD_SPECS WHERE PRODUCT_ID = " + prod_img + " AND ATTRIBUTE_ID = " + att_id + " ";
                        forobjpreview = CreateDataSet();
                        string nameobj = string.Empty;
                        if (forobjpreview.Tables[0].Rows.Count > 0)
                        {
                            nameobj = forobjpreview.Tables[0].Rows[0][0].ToString();
                        }

                        imgSource = imgSource.Substring(0, imgSource.IndexOf("/>", StringComparison.Ordinal));
                        // string newImageTag = ConvertImage(imgSource);
                        string newImageTag = imgTag;
                      //  imgTag = imgTag.Replace(".psd", "");
                        newImageTag = newImageTag + "<br>" + "<Label>" + nameobj + "</Label>";
                        // newImageTag = newImageTag.Replace(".psd", "");
                        if (imgSource != newImageTag)
                        {
                            htmlString = htmlString.Replace(imgTag, newImageTag);
                        }

                        if (k < i)
                        {
                            k++;
                        }
                    }
                }
                xmlStr = htmlString;
            }

            if (xmlStr.Trim() == string.Empty)
            { xmlStr = "<html><h6>Table Preview Not Available!</h6><html>"; }
            string html = xmlStr;
            html = html.Replace("&lt;br /&gt;", "<br>").Replace("&lt;/br&gt;", "<br>").Replace("&lt;br&gt;", "<br>")
                    .Replace("&lt;br/&gt;", "<br>")
                    .Replace("&amp;lt;br/&amp;gt;", "<br>").Replace("&amp;nbsp;", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
            // var tPreview = new TablePreview { FamilyName = familyName, LayoutName = _mTableLayoutName };
            SetPreview(html, familyName);
            //tPreview.ShowDialog();
            return html;
        }


        #region Attribute Pack

        public void SaveAttributePackTablestructure(string xmlString, int familyId, int packId, int catalogId, string flag, int tableId)
        {
            string SQLString = "";
            string familyIdString = Convert.ToString(familyId);

            if (tableId > 0)
            {
                if (flag == "Insert")
                {
                    string sqlStr = "Insert into TB_ATTRIBUTE_PACK_TABLE_STRUCTURE (FAMILY_STRUCTURE_ID,PACK_ID,TABLE_STRUCTURE,STRUCTURE_NAME,IS_DEFAULT,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) values (" + tableId + ',' + packId + ',' + xmlString.Replace("'", "''") + ',' + "Default Layout" + ',' + 1 + ',' + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                    SQLString = sqlStr;
                    using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        objSqlConnection.Open();
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = SQLString;
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.ExecuteNonQuery();
                    }
                }
                else
                {
                    string sqlStr = "UPDATE TB_ATTRIBUTE_PACK_TABLE_STRUCTURE SET TABLE_STRUCTURE = '" + xmlString.Replace("'", "''") + "' WHERE FAMILY_STRUCTURE_ID = " + tableId + "  and Pack_ID=" + packId + "";
                    SQLString = sqlStr;
                    using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        objSqlConnection.Open();
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = SQLString;
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.ExecuteNonQuery();
                    }
                }
            }
        }

        #endregion

        private List<ConstructNodeViewModel> subitems(JToken item)
        {
            return item.Select(x => new ConstructNodeViewModel
            {

                ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
                ATTRIBUTE_NAME = (string)x["ATTRIBUTE_NAME"],
                COLOR = (string)x["COLOR"],
                Merge = (string)x["Merge"],
                items = subitems(x["items"])
            }).ToList();
        }
        private void Constructleftxml(StringBuilder xmlTxtBuilder, string attributeName, JToken item, int level)
        {
            //var functionAlloweditems = item.Select(x => new ConstructNodeViewModel
            //{

            //    ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
            //    ATTRIBUTE_NAME = (string)x["ATTRIBUTE_NAME"],
            //    COLOR = (string)x["COLOR"],
            //    Merge = x["Merge"] == null ? "Unchecked" : (string)x["Merge"],
            //   // items =x["items"] == null ? null : subitems(x["items"])
            //}).ToList();

            //var df = functionAlloweditems.Where(x => x.COLOR.ToLower() == "blue");
            //var df12 = functionAlloweditems.Where(x => x.COLOR.ToLower() == "black");
            //var df1 = functionAlloweditems.Where(x => x.COLOR.ToLower() == "red");
            xmlTxtBuilder.Append("<LeftRowField AttrID=\"Super\" AttrName=\"" + System.Security.SecurityElement.Escape(attributeName) + "\" Level=\"" + level + "\"> ");
            foreach (var item2 in item)
            {
                if (item2["items"] == null)
                {
                    if (Convert.ToString(item2["COLOR"]).ToLower() != "red")
                    {
                        int i = level + 1;
                        xmlTxtBuilder.Append("<LeftRowField AttrID=\"");
                        xmlTxtBuilder.Append("Attr:");
                        xmlTxtBuilder.Append((string)item2["ATTRIBUTE_ID"]);
                        xmlTxtBuilder.Append("\" Merge=\"" + Convert.ToString(item2["Merge"]) + "\"");
                        xmlTxtBuilder.Append(" Level=\"" + i + "\"");
                        xmlTxtBuilder.Append(">");
                        xmlTxtBuilder.Append("</LeftRowField>");
                    }
                }
                else
                {
                    int i = level + 1;
                    Constructleftxml(xmlTxtBuilder, Convert.ToString(item2["ATTRIBUTE_NAME"]), item2["items"], i);
                }
            }
            xmlTxtBuilder.Append("</LeftRowField>");
        }

        private void Constructrightxml(StringBuilder xmlTxtBuilder, string attributeName, JToken item, int level)
        {
            xmlTxtBuilder.Append("<RightRowField AttrID=\"Super\" AttrName=\"" + System.Security.SecurityElement.Escape(attributeName) + "\" Level=\"" + level + "\"> ");
            foreach (var item2 in item)
            {
                if (item2["items"] == null)
                {
                    if (Convert.ToString(item2["COLOR"]).ToLower() != "red")
                    {
                        int i = level + 1;
                        xmlTxtBuilder.Append("<RightRowField AttrID=\"");
                        xmlTxtBuilder.Append("Attr:");
                        xmlTxtBuilder.Append((string)item2["ATTRIBUTE_ID"]);
                        xmlTxtBuilder.Append("\" Merge=\"" + Convert.ToString(item2["Merge"]) + "\"");
                        xmlTxtBuilder.Append(" Level=\"" + i + "\"");
                        xmlTxtBuilder.Append(">");
                        xmlTxtBuilder.Append("</RightRowField>");
                    }
                }
                else
                {
                    int i = level + 1;
                    Constructrightxml(xmlTxtBuilder, Convert.ToString(item2["ATTRIBUTE_NAME"]), item2["items"], i);
                }
            }
            xmlTxtBuilder.Append("</RightRowField>");
        }

        private void Constructcolumnxml(StringBuilder xmlTxtBuilder, string attributeName, JToken item, int level)
        {
            xmlTxtBuilder.Append("<ColumnField AttrID=\"Super\" AttrName=\"" + System.Security.SecurityElement.Escape(attributeName) + "\" Level=\"" + level + "\"> ");
            foreach (var item2 in item)
            {
                if (item2["items"] == null)
                {
                    if (Convert.ToString(item2["COLOR"]).ToLower() != "red")
                    {
                        int i = level + 1;
                        xmlTxtBuilder.Append("<ColumnField AttrID=\"");
                        xmlTxtBuilder.Append("Attr:");
                        xmlTxtBuilder.Append((string)item2["ATTRIBUTE_ID"]);
                        xmlTxtBuilder.Append("\" Merge=\"" + Convert.ToString(item2["Merge"]) + "\"");
                        xmlTxtBuilder.Append(" Level=\"" + i + "\"");
                        xmlTxtBuilder.Append(">");
                        xmlTxtBuilder.Append("</ColumnField>");
                    }
                }
                else
                {
                    int i = level + 1;
                    Constructcolumnxml(xmlTxtBuilder, Convert.ToString(item2["ATTRIBUTE_NAME"]), item2["items"], i);
                }
            }
            xmlTxtBuilder.Append("</ColumnField>");
        }
        private void Constructsummaryxml(StringBuilder xmlTxtBuilder, string attributeName, JToken item, int level)
        {
            xmlTxtBuilder.Append("<SummaryField AttrID=\"Super\" AttrName=\"" + System.Security.SecurityElement.Escape(attributeName) + "\" Level=\"" + level + "\"> ");
            foreach (var item2 in item)
            {
                if (item2["items"] == null)
                {
                    if (Convert.ToString(item2["COLOR"]).ToLower() != "red")
                    {
                        int i = level + 1;
                        xmlTxtBuilder.Append("<SummaryField AttrID=\"");
                        xmlTxtBuilder.Append("Attr:");
                        xmlTxtBuilder.Append((string)item2["ATTRIBUTE_ID"]);
                        xmlTxtBuilder.Append("\" Merge=\"" + Convert.ToString(item2["Merge"]) + "\"");
                        xmlTxtBuilder.Append(" Level=\"" + i + "\"");
                        xmlTxtBuilder.Append(">");
                        xmlTxtBuilder.Append("</SummaryField>");
                    }
                }
                else
                {
                    int i = level + 1;
                    Constructsummaryxml(xmlTxtBuilder, Convert.ToString(item2["ATTRIBUTE_NAME"]), item2["items"], i);
                }
            }
            xmlTxtBuilder.Append("</SummaryField>");
        }
        private string ConstructLayoutXml(string comboSummaryGroupValue, string comboGroupByColumnValue,
            string emptyCellTextBoxValue, string dispalyRowHeaderCheckEditorValue,
            string displayColumnHeaderCheckBoxValue, string dispalySummaryHeaderCheckEditorValue,
            string verticalTableChkBoxValue, string pivotHeaderTextBoxValue, object model)
        {
            var treenode = ((JArray)model).Select(x => x).ToList();
            var objlefttreedata = new List<Treenodedata>();
            var objleftinnertreedata = new List<Treenodedata>();
            var objrighttreedata = new List<Treenodedata>();
            var objcolumntreedata = new List<Treenodedata>();
            var objsummarytreedata = new List<Treenodedata>();
            var objsummaryinnertreedata = new List<Treenodedata>();

            var lt = treenode[0].ToList();
            var rt = treenode[1].ToList();
            var ct = treenode[2].ToList();
            var st = treenode[3].ToList();



            //***************************************************************************************************
            //NodeDates nodes = ser.Deserialize<Nod=eDates>(model);
            string flags = "Unchecked";
            var xmlTxtBuilder = new StringBuilder();
            //Causes child elements to be indented

            // Report element
            xmlTxtBuilder.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            xmlTxtBuilder.Append("<TradingBell TableType=\"SuperTable\">");
            // xmlTxtBuilder.Append(ConstructLeftRowXmlLayout());
            foreach (var items in lt)
            {
                //lt_i += 1;
                if (!string.IsNullOrEmpty(Convert.ToString(items["ATTRIBUTE_NAME"])))
                {
                    if (items["items"] == null || Convert.ToString(items["items"]) == "[]")
                    {
                        xmlTxtBuilder.Append("<LeftRowField AttrID=\"");
                        xmlTxtBuilder.Append("Attr:");
                        xmlTxtBuilder.Append((string)items["ATTRIBUTE_ID"]);
                        xmlTxtBuilder.Append("\" Merge=\"" + Convert.ToString(items["Merge"]) + "\"");
                        xmlTxtBuilder.Append(" Level=\"" + 0 + "\"");
                        xmlTxtBuilder.Append(">");
                        xmlTxtBuilder.Append("</LeftRowField>");

                    }
                    else
                    {

                        Constructleftxml(xmlTxtBuilder, Convert.ToString(items["ATTRIBUTE_NAME"]), items["items"], 0);

                    }


                }
            }

            foreach (var items in rt)
            {
                //lt_i += 1;
                if (!string.IsNullOrEmpty(Convert.ToString(items["ATTRIBUTE_NAME"])))
                {
                    if (items["items"] == null || Convert.ToString(items["items"]) == "[]")
                    {
                        xmlTxtBuilder.Append("<RightRowField AttrID=\"");
                        xmlTxtBuilder.Append("Attr:");
                        xmlTxtBuilder.Append((string)items["ATTRIBUTE_ID"]);
                        xmlTxtBuilder.Append("\" Merge=\"" + Convert.ToString(items["Merge"]) + "\"");
                        xmlTxtBuilder.Append(" Level=\"" + 0 + "\"");
                        xmlTxtBuilder.Append(">");
                        xmlTxtBuilder.Append("</RightRowField>");

                    }
                    else
                    {

                        Constructrightxml(xmlTxtBuilder, Convert.ToString(items["ATTRIBUTE_NAME"]), items["items"], 0);

                    }


                }
            }

            foreach (var items in ct)
            {
                //lt_i += 1;
                if (!string.IsNullOrEmpty(Convert.ToString(items["ATTRIBUTE_NAME"])))
                {
                    if (items["items"] == null || Convert.ToString(items["items"]) == "[]")
                    {
                        xmlTxtBuilder.Append("<ColumnField AttrID=\"");
                        xmlTxtBuilder.Append("Attr:");
                        xmlTxtBuilder.Append((string)items["ATTRIBUTE_ID"]);
                        xmlTxtBuilder.Append("\" Merge=\"" + Convert.ToString(items["Merge"]) + "\"");
                        xmlTxtBuilder.Append(" Level=\"" + 0 + "\"");
                        xmlTxtBuilder.Append(">");
                        xmlTxtBuilder.Append("</ColumnField>");

                    }
                    else
                    {

                        Constructcolumnxml(xmlTxtBuilder, Convert.ToString(items["ATTRIBUTE_NAME"]), items["items"], 0);

                    }


                }
            }
            foreach (var items in st)
            {
                //lt_i += 1;
                if (!string.IsNullOrEmpty(Convert.ToString(items["ATTRIBUTE_NAME"])))
                {
                    if (items["items"] == null || Convert.ToString(items["items"]) == "[]")
                    {
                        xmlTxtBuilder.Append("<SummaryField AttrID=\"");
                        xmlTxtBuilder.Append("Attr:");
                        xmlTxtBuilder.Append((string)items["ATTRIBUTE_ID"]);
                        xmlTxtBuilder.Append("\" Merge=\"" + Convert.ToString(items["Merge"]) + "\"");
                        xmlTxtBuilder.Append(" Level=\"" + 0 + "\"");
                        xmlTxtBuilder.Append(">");
                        xmlTxtBuilder.Append("</SummaryField>");

                    }
                    else
                    {

                        Constructsummaryxml(xmlTxtBuilder, Convert.ToString(items["ATTRIBUTE_NAME"]), items["items"], 0);

                    }


                }
            }
            xmlTxtBuilder.Append("<SummaryGroupField>");
            if (comboSummaryGroupValue != "null" && comboSummaryGroupValue != null && comboSummaryGroupValue != "0")
            {
                xmlTxtBuilder.Append("Attr:");
                xmlTxtBuilder.Append(comboSummaryGroupValue);
            }
            else
            {
                xmlTxtBuilder.Append("");
            }
            xmlTxtBuilder.Append("</SummaryGroupField>");

            xmlTxtBuilder.Append("<TableGroupField>");
            if (comboGroupByColumnValue != "null" && comboGroupByColumnValue != null && comboGroupByColumnValue != "0")
            {
                xmlTxtBuilder.Append("Attr:");
                xmlTxtBuilder.Append(comboGroupByColumnValue);
            }
            else
            {
                xmlTxtBuilder.Append("");
            }

            xmlTxtBuilder.Append("</TableGroupField>");

            xmlTxtBuilder.Append("<PlaceHolderText>");

            xmlTxtBuilder.Append("<![CDATA[" + emptyCellTextBoxValue + "]]>");

            xmlTxtBuilder.Append("</PlaceHolderText>");

            xmlTxtBuilder.Append("<DisplayRowHeader>");

            xmlTxtBuilder.Append(dispalyRowHeaderCheckEditorValue);

            xmlTxtBuilder.Append("</DisplayRowHeader>");

            xmlTxtBuilder.Append("<DisplayColumnHeader>");

            xmlTxtBuilder.Append(displayColumnHeaderCheckBoxValue);

            xmlTxtBuilder.Append("</DisplayColumnHeader>");

            xmlTxtBuilder.Append("<DisplaySummaryHeader>");

            xmlTxtBuilder.Append(dispalySummaryHeaderCheckEditorValue);

            xmlTxtBuilder.Append("</DisplaySummaryHeader>");

            xmlTxtBuilder.Append("<VerticalTable>");

            xmlTxtBuilder.Append(verticalTableChkBoxValue);

            xmlTxtBuilder.Append("</VerticalTable>");

            xmlTxtBuilder.Append("<PivotHeaderText>");

            xmlTxtBuilder.Append("<![CDATA[" + pivotHeaderTextBoxValue + "]]>");

            xmlTxtBuilder.Append("</PivotHeaderText>");

            xmlTxtBuilder.Append("<MergeRowHeader>");

            xmlTxtBuilder.Append("");

            xmlTxtBuilder.Append("</MergeRowHeader>");

            xmlTxtBuilder.Append("<MergeSummaryFields>");

            xmlTxtBuilder.Append("");

            xmlTxtBuilder.Append("</MergeSummaryFields>");

            xmlTxtBuilder.Append("</TradingBell>");


            ////Flush the writer and close the stream
            return xmlTxtBuilder.ToString();
        }


        private string ConstructLayoutXmlDefaultPeview(string comboSummaryGroupValue, string comboGroupByColumnValue,
    string emptyCellTextBoxValue, string dispalyRowHeaderCheckEditorValue,
    string displayColumnHeaderCheckBoxValue, string dispalySummaryHeaderCheckEditorValue,
    string verticalTableChkBoxValue, string pivotHeaderTextBoxValue, object model)
        {
            var treenode = ((JArray)model).Select(x => x).ToList();
            var objlefttreedata = new List<Treenodedata>();
            var objleftinnertreedata = new List<Treenodedata>();
            var objrighttreedata = new List<Treenodedata>();
            var objcolumntreedata = new List<Treenodedata>();
            var objsummarytreedata = new List<Treenodedata>();
            var objsummaryinnertreedata = new List<Treenodedata>();

            var lt = ((JArray)model).Select(x => x).ToList();




            //***************************************************************************************************
            //NodeDates nodes = ser.Deserialize<Nod=eDates>(model);
            string flags = "Unchecked";
            var xmlTxtBuilder = new StringBuilder();
            //Causes child elements to be indented

            // Report element
            xmlTxtBuilder.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            xmlTxtBuilder.Append("<TradingBell TableType=\"SuperTable\">");
            // xmlTxtBuilder.Append(ConstructLeftRowXmlLayout());
            foreach (var items in lt)
            {
                //lt_i += 1;
                if (!string.IsNullOrEmpty(Convert.ToString(items["ATTRIBUTE_NAME"])))
                {
                    if (items["items"] == null || Convert.ToString(items["items"]) == "[]")
                    {
                        xmlTxtBuilder.Append("<LeftRowField AttrID=\"");
                        xmlTxtBuilder.Append("Attr:");
                        xmlTxtBuilder.Append((string)items["ATTRIBUTE_ID"]);
                        xmlTxtBuilder.Append("\" Merge=\"" + Convert.ToString(items["Merge"]) + "\"");
                        xmlTxtBuilder.Append(" Level=\"" + 0 + "\"");
                        xmlTxtBuilder.Append(">");
                        xmlTxtBuilder.Append("</LeftRowField>");

                    }
                    else
                    {

                        Constructleftxml(xmlTxtBuilder, Convert.ToString(items["ATTRIBUTE_NAME"]), items["items"], 0);

                    }


                }
            }
            xmlTxtBuilder.Append("<SummaryGroupField>");
            if (comboSummaryGroupValue != "null" && comboSummaryGroupValue != null && comboSummaryGroupValue != "0")
            {
                xmlTxtBuilder.Append("Attr:");
                xmlTxtBuilder.Append(comboSummaryGroupValue);
            }
            else
            {
                xmlTxtBuilder.Append("");
            }
            xmlTxtBuilder.Append("</SummaryGroupField>");

            xmlTxtBuilder.Append("<TableGroupField>");
            if (comboGroupByColumnValue != "null" && comboGroupByColumnValue != null && comboGroupByColumnValue != "0")
            {
                xmlTxtBuilder.Append("Attr:");
                xmlTxtBuilder.Append(comboGroupByColumnValue);
            }
            else
            {
                xmlTxtBuilder.Append("");
            }

            xmlTxtBuilder.Append("</TableGroupField>");

            xmlTxtBuilder.Append("<PlaceHolderText>");

            xmlTxtBuilder.Append("<![CDATA[" + emptyCellTextBoxValue + "]]>");

            xmlTxtBuilder.Append("</PlaceHolderText>");

            xmlTxtBuilder.Append("<DisplayRowHeader>");

            xmlTxtBuilder.Append(dispalyRowHeaderCheckEditorValue);

            xmlTxtBuilder.Append("</DisplayRowHeader>");

            xmlTxtBuilder.Append("<DisplayColumnHeader>");

            xmlTxtBuilder.Append(displayColumnHeaderCheckBoxValue);

            xmlTxtBuilder.Append("</DisplayColumnHeader>");

            xmlTxtBuilder.Append("<DisplaySummaryHeader>");

            xmlTxtBuilder.Append(dispalySummaryHeaderCheckEditorValue);

            xmlTxtBuilder.Append("</DisplaySummaryHeader>");

            xmlTxtBuilder.Append("<VerticalTable>");

            xmlTxtBuilder.Append(verticalTableChkBoxValue);

            xmlTxtBuilder.Append("</VerticalTable>");

            xmlTxtBuilder.Append("<PivotHeaderText>");

            xmlTxtBuilder.Append("<![CDATA[" + pivotHeaderTextBoxValue + "]]>");

            xmlTxtBuilder.Append("</PivotHeaderText>");

            xmlTxtBuilder.Append("<MergeRowHeader>");

            xmlTxtBuilder.Append("");

            xmlTxtBuilder.Append("</MergeRowHeader>");

            xmlTxtBuilder.Append("<MergeSummaryFields>");

            xmlTxtBuilder.Append("");

            xmlTxtBuilder.Append("</MergeSummaryFields>");

            xmlTxtBuilder.Append("</TradingBell>");


            ////Flush the writer and close the stream
            return xmlTxtBuilder.ToString();
        }
        public int SaveTableLayout(string xmlTxt, string structureName, string flag, int groupId, int masterCatalogId, string FamilyName)
        {
            // this.DecodeLayoutXML(xmlTxt);
            string xmlString = xmlTxt;//ConstructLayoutXml();

            if (flag == "MultipleTable")
            {
                _mTableLayoutName = structureName;
                if (string.IsNullOrEmpty(_mTableLayoutName))
                {
                    _mTableLayoutName = "Default Layout";
                }
                string sqlStr = @"UPDATE TB_MULTIPLE_TABLE_STRUCTURE SET MULTIPLE_TABLE_STRUCTURE = '" + xmlString.Replace("'", "''") + "' WHERE GROUP_ID = " + groupId + " AND STRUCTURE_NAME='" + _mTableLayoutName.Replace("'", "''") + "'";
                SQLString = sqlStr;
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = SQLString;
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Connection = objSqlConnection;
                    //   SqlCommand dbCommand = new SqlCommand(_SQLString, objSqlConnection);
                    return objSqlCommand.ExecuteNonQuery();
                }
            }

            else
            {
                //string masterCatalogSqlStr = string.Format("IF EXISTS (SELECT * FROM TB_FAMILY_TABLE_STRUCTURE WHERE CATALOG_ID ={0} AND FAMILY_ID = {1} AND STRUCTURE_NAME='{2}') BEGIN UPDATE TB_FAMILY_TABLE_STRUCTURE SET FAMILY_TABLE_STRUCTURE ='{3}' WHERE CATALOG_ID ={0} AND FAMILY_ID = {1} AND STRUCTURE_NAME='{2}' END ELSE BEGIN INSERT INTO ", masterCatalogId, masterCatalog_FamilyId, structureName, xmlString.Replace("'", "''"));

                SqlCommand sqlCommand = new SqlCommand();
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {


                    sqlCommand = new SqlCommand("STP_TABLEDESIGNER_UPDATION", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    sqlCommand.Parameters.Add("@MASTERCATALOG_ID", SqlDbType.Int).Value = masterCatalogId;
                    sqlCommand.Parameters.Add("@MASTERFAMILY_NAME", SqlDbType.VarChar).Value = FamilyName;
                    sqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int).Value = _mCatalogID;
                    sqlCommand.Parameters.Add("@FAMILYID", SqlDbType.Int).Value = _mFamilyID;
                    sqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar).Value = xmlString.Replace("'", "''");
                    sqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar).Value = structureName;

                    sqlConnection.Open();
                    sqlCommand.ExecuteNonQuery();

                }

                _mTableLayoutName = structureName;
                string sqlStr = @"UPDATE TB_FAMILY_TABLE_STRUCTURE SET FAMILY_TABLE_STRUCTURE = '" + xmlString.Replace("'", "''") + "' WHERE FAMILY_ID = " + _mFamilyID + " AND CATALOG_ID=" + _mCatalogID + " AND STRUCTURE_NAME='" + _mTableLayoutName.Replace("'", "''") + "'";
                SQLString = sqlStr;
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = SQLString;
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Connection = objSqlConnection;
                    //   SqlCommand dbCommand = new SqlCommand(_SQLString, objSqlConnection);
                    return objSqlCommand.ExecuteNonQuery();
                }

            }
            //return ExecuteSQLQuery();

        }
        public void SetPreview(string htmlPreviewString, string familyName)
        {
            if (htmlPreviewString.Contains("&amp;lt;") && htmlPreviewString.Contains("&amp;gt;"))
            {
                htmlPreviewString = htmlPreviewString.Replace("&amp;lt;", "&lt;").Replace("&amp;gt;", "&gt;");
            }
            //if (htmlPreviewString.Contains("&lt;") && htmlPreviewString.Contains("&gt;"))
            //{
            //    htmlPreviewString = htmlPreviewString.Replace("&lt;", "<");
            //    htmlPreviewString = htmlPreviewString.Replace("&gt;", ">");
            //}

            //Name Changes
            if (htmlPreviewString.Contains("<h4> Products</h4>") )
            {
                htmlPreviewString = htmlPreviewString.Replace("<h4> Products</h4>", "<h4> Items</h4>");
            }
            //Name Changes
            if (htmlPreviewString.Contains("&nbsp;"))
            {
                htmlPreviewString = htmlPreviewString.Replace("&nbsp;", "\n");
            }
            if (htmlPreviewString.Contains("&amp;"))
            {
                htmlPreviewString = htmlPreviewString.Replace("&amp;", "&");
            }
            string strfamilyName = "Product Name :" + familyName;
            // Text = @"Table Preview - " + LayoutName;
            //  var textFile = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/css/images/master.css"));
            //  string fileContents = textFile.ReadToEnd();
            //  textFile.Close();
            var fileContents = "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>";
            htmlPreviewString = htmlPreviewString.Replace("<head />", fileContents);
            htmlPreviewString = htmlPreviewString.Replace("<h2 />", "<h2>" + strfamilyName + "</h2>");
            htmlPreviewString.Replace("ITEM#", HttpContext.Current.Session["CustomerItemNo"].ToString());
            var encOutput = Encoding.UTF8;
            var filePath = HttpContext.Current.Server.MapPath("~/Views/App/Partials/SuperColumnPreview.html");
            // string filePath = HttpContext.Current.Server.MapPath("~/Content/")  + "\\SuperColumnPreview.html";
            var fileHtml = new FileStream(filePath, FileMode.Create);
            var strwriter = new StreamWriter(fileHtml, encOutput);
            strwriter.Write(htmlPreviewString.Replace("ITEM#", HttpContext.Current.Session["CustomerItemNo"].ToString()));
            strwriter.Close();
            fileHtml.Close();
            //webBrowser1.Navigate(filePath);

        }
        public string RetrieveTableLayout(string structureName, string flag, int groupId)
        {
            string layoutXml = string.Empty;

            if (flag == "MultipleTable")
            {
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    DataSet ds = new DataSet();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText =
                        "SELECT STRUCTURE_NAME,MULTIPLE_TABLE_STRUCTURE FROM TB_MULTIPLE_TABLE_STRUCTURE WHERE GROUP_ID = " + groupId + " AND STRUCTURE_NAME='" + structureName.Replace("'", "''") + "'";
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlConnection.Open();
                    var da = new SqlDataAdapter(objSqlCommand);
                    var fs = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        layoutXml = ds.Tables[0].Rows[0]["MULTIPLE_TABLE_STRUCTURE"].ToString();
                    }
                }
            }
            else
            {

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    DataSet ds = new DataSet();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText =
                        "SELECT STRUCTURE_NAME,FAMILY_TABLE_STRUCTURE FROM TB_FAMILY_TABLE_STRUCTURE WHERE FAMILY_ID = " +
                        _mFamilyID + " AND CATALOG_ID=" + _mCatalogID + " AND STRUCTURE_NAME='" + structureName.Replace("'", "''") + "'";
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlConnection.Open();
                    var da = new SqlDataAdapter(objSqlCommand);
                    var fs = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        layoutXml = ds.Tables[0].Rows[0]["FAMILY_TABLE_STRUCTURE"].ToString();
                    }
                }
            }

            return layoutXml;
            // return null;
        }
        private void FetchSourceData()
        {
            try
            {
                var ocon =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                // string ocon = _oc.ReturnConnectionString();
                // var ofrm = new CatalogX.CatalogXfunction();
                _loadXProductTable = true;
                var attrList = new int[0];
                const bool mainFamily = true;
                DataSet newDsGlobal = CatalogProductDetailsX(Convert.ToInt32(CatalogID), Convert.ToInt32(FamilyID), attrList, mainFamily, ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);

                SQLString = @"SELECT DISTINCT  TPFL.ATTRIBUTE_ID FROM TB_PROD_FAMILY_ATTR_LIST  TPFL, TB_CATALOG_ATTRIBUTES TCA , TB_ATTRIBUTE TA WHERE  TPFL.FAMILY_ID = '" + FamilyID + "'  AND TCA.ATTRIBUTE_ID = TPFL.ATTRIBUTE_ID AND TCA.CATALOG_ID = '" + CatalogID + "' AND TPFL.ATTRIBUTE_ID=TA.ATTRIBUTE_ID AND TA.PUBLISH2PRINT=1";
                DataSet ds = CreateDataSet();
                _attrTablePivot = ds.Tables[0];

                _sourceTable = ProductFilter(newDsGlobal.Copy(), 0, false);
                _attributeTable = newDsGlobal.Tables[1];

                _attributeTable.Columns.Remove("CATALOG_ID");
                _attributeTable.Columns.Remove("FAMILY_ID");
                _attributeTable.Columns.Remove("PRODUCT_ID");
                _attributeTable.Columns.Remove("Sort");
                _attributeTable.Columns.Remove("Publish");
                _attributeTable.Columns.Remove("Publish2Print");
                _attributeTable.Columns.Remove("Publish2CD");
                _attributeTable.Columns.Remove("WorkFlowStat");

                var srcTable = new DataTable();
                for (int i = 0; i < _sourceTable.Columns.Count; i++)
                {
                    if (_sourceTable.Columns[i].Caption == "Sort")
                    {
                        srcTable.Columns.Add(_sourceTable.Columns[i].Caption, typeof(int));
                    }
                    else
                    {
                        srcTable.Columns.Add(_sourceTable.Columns[i].Caption);
                    }
                }

                for (int i = 0; i < _sourceTable.Rows.Count; i++)
                {
                    if (_sourceTable.Rows[i]["Publish2Print"].ToString() != "0")
                    {
                        DataRow dr = srcTable.NewRow();
                        for (int j = 0; j < _sourceTable.Columns.Count; j++)
                        {
                            if (_sourceTable.Rows[i][j] == DBNull.Value)
                                dr[j] = null;
                            else
                                dr[j] = _sourceTable.Rows[i][j].ToString().Trim();
                            //   sourceTable.Rows[i][j] = sourceTable.Rows[i][j].ToString().Trim();
                        }
                        srcTable.Rows.Add(dr);
                    }
                }

                _rowTable = new DataTable();
                _columnTable = new DataTable();
                _groupTable = new DataTable();
                var view = new DataView(srcTable) { Sort = "Sort" };
                _sourceTable = view.ToTable();

                string rowFields = ConstructRowFields(_attributeTable);
                if (_mColumnFields.Count > 0 && _mSummaryFields.Count > 0)
                {
                    if (objGroupedSummaryAttrs.Values.Count(a => a != "None") > 0)
                    {
                        var dtTempGroupAttr = new DataTable();
                        dtTempGroupAttr = view.ToTable(true, rowFields.Split('`'));
                        foreach (DataColumn cl in dtTempGroupAttr.Columns)
                        {
                            _rowTable.Columns.Add(new DataColumn(cl.ColumnName, cl.DataType));
                        }
                        foreach (DataRow drTemp in dtTempGroupAttr.Rows)
                        {
                            foreach (var item in objGroupedSummaryAttrs)
                            {
                                if (item.Value != "None")
                                {
                                    _rowTable.ImportRow(drTemp);
                                }
                            }
                        }
                    }
                    else
                    {
                        _rowTable = view.ToTable(true, rowFields.Split('`'));
                    }
                }
                else
                {
                    _rowTable = view.ToTable(false, rowFields.Split('`'));
                }

                string colFields = ConstructColumnFields(_attributeTable);

                if (colFields.Trim() != string.Empty)
                { _columnTable = view.ToTable(true, colFields.Split('`')); }

                if (_mTableGroupField.Trim() != string.Empty)
                {
                    _groupTable = view.ToTable(true, _mTableGroupField);
                    //groupTable = SelectDistinct(sourceTable, m_tableGroupField);
                }


            }
            catch (Exception)
            {
                // MessageBox.Show(ex.Message);
                _groupTable = new DataTable();
                _sourceTable = new DataTable();
                _rowTable = new DataTable();
                _columnTable = new DataTable();
                _attributeTable = new DataTable();
            }
        }
        //private void SetAttributePropertiesForSourceTable()
        //{

        //    string sqlStr;

        //    for (int i = 0; i < _attributeTable.Columns.Count; i++)
        //    {
        //        for (int j = 0; j < _sourceTable.Columns.Count; j++)
        //        {
        //            if (_attributeTable.Columns[i].ColumnName == _sourceTable.Columns[j].ColumnName)
        //            {
        //                if (_sourceTable.Columns[j].ExtendedProperties.Contains(_attributeTable.Rows[0][i].ToString()))
        //                {

        //                }
        //                else
        //                {
        //                    _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_ID", _attributeTable.Rows[0][i].ToString());
        //                }
        //                sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
        //                SQLString = sqlStr;
        //                DataSet dss = CreateDataSet();

        //                _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE",
        //                                                               dss.Tables[0].Rows[0].ItemArray[0].ToString()
        //                                                                                                 .StartsWith(
        //                                                                                                     "Num")
        //                                                                   ? "4"
        //                                                                   : _attributeTable.Rows[1][i].ToString());
        //                _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());

        //                #region "Date and Time Formating"
        //                if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Date"))
        //                {
        //                    sqlStr = "SELECT ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID =" + _attributeTable.Rows[0][i];
        //                    SQLString = sqlStr;
        //                    DataSet attrformat = CreateDataSet();
        //                    if (attrformat.Tables[0].Rows.Count > 0)
        //                    {
        //                        if (attrformat.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
        //                        {
        //                            for (int k = 0; k < _sourceTable.Columns.Count; k++)
        //                            {
        //                                if (_sourceTable.Columns[k] == _sourceTable.Columns[j])
        //                                {
        //                                    foreach (DataRow dr in _sourceTable.Rows)
        //                                    {
        //                                        if (dr[_sourceTable.Columns[k].ColumnName].ToString() != "")
        //                                        {
        //                                            string tempRowDate = dr[_sourceTable.Columns[k].ColumnName].ToString();
        //                                            //string tempSrcDate = _sourceTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();

        //                                            dr[_sourceTable.Columns[k].ColumnName] = tempRowDate.Substring(3, 2) + "/" + tempRowDate.Substring(0, 2) + "/" + tempRowDate.Substring(6, 4);
        //                                            //_sourceTable.Rows[j][_rowTable.Columns[j].ToString()] = tempSrcDate.Substring(3, 2) + "/" + tempSrcDate.Substring(0, 2) + "/" + tempSrcDate.Substring(6, 4);
        //                                        }
        //                                    }
        //                                }
        //                            }


        //                        }

        //                    }
        //                }

        //                #endregion
        //            }
        //        }
        //    }

        //    for (int i = 0; i < _attributeTable.Columns.Count; i++)
        //    {
        //        for (int j = 0; j < _columnTable.Columns.Count; j++)
        //        {
        //            if (_attributeTable.Columns[i].ColumnName == _columnTable.Columns[j].ColumnName)
        //            {
        //                _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_ID", _attributeTable.Rows[0][i].ToString());
        //                sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
        //                SQLString = sqlStr;
        //                DataSet dss = CreateDataSet();

        //                if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
        //                {
        //                    try
        //                    {
        //                        _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
        //                        _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
        //                    }
        //                    catch (Exception exception)
        //                    {
        //                        //_exception.Exceptions(exception); 
        //                    }
        //                }
        //                else
        //                {
        //                    _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", _attributeTable.Rows[1][i].ToString());
        //                }
        //                _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());

        //                #region "Date and Time Formating"
        //                if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Date"))
        //                {
        //                    sqlStr = "SELECT ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID =" + _attributeTable.Rows[0][i];
        //                    SQLString = sqlStr;
        //                    DataSet attrformat = CreateDataSet();
        //                    if (attrformat.Tables[0].Rows.Count > 0)
        //                    {
        //                        if (attrformat.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
        //                        {
        //                            for (int k = 0; k < _columnTable.Columns.Count; k++)
        //                            {
        //                                if (_columnTable.Columns[k] == _columnTable.Columns[j])
        //                                {
        //                                    foreach (DataRow dr in _columnTable.Rows)
        //                                    {
        //                                        if (dr[_columnTable.Columns[k].ColumnName].ToString() != "")
        //                                        {
        //                                            string tempRowDate = dr[_columnTable.Columns[k].ColumnName].ToString();
        //                                            //string tempSrcDate = _sourceTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();

        //                                            dr[_columnTable.Columns[k].ColumnName] = tempRowDate.Substring(3, 2) + "/" + tempRowDate.Substring(0, 2) + "/" + tempRowDate.Substring(6, 4);
        //                                            //_sourceTable.Rows[j][_rowTable.Columns[j].ToString()] = tempSrcDate.Substring(3, 2) + "/" + tempSrcDate.Substring(0, 2) + "/" + tempSrcDate.Substring(6, 4);
        //                                        }
        //                                    }
        //                                }
        //                            }


        //                        }

        //                    }
        //                }

        //                #endregion
        //            }
        //        }
        //    }

        //    for (int i = 0; i < _attributeTable.Columns.Count; i++)
        //    {
        //        for (int j = 0; j < _rowTable.Columns.Count; j++)
        //        {
        //            if (_attributeTable.Columns[i].ColumnName == _rowTable.Columns[j].ColumnName)
        //            {
        //                _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_ID", _attributeTable.Rows[0][i].ToString());
        //                sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
        //                SQLString = sqlStr;
        //                DataSet dss = CreateDataSet();
        //                if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
        //                {
        //                    try
        //                    {
        //                        _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
        //                        _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
        //                    }
        //                    catch (Exception exception)
        //                    { //_exception.Exceptions(exception); 
        //                    }
        //                }

        //                else
        //                {
        //                    _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", _attributeTable.Rows[1][i].ToString());
        //                }
        //                _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());
        //            }
        //        }
        //    }
        //    for (int i = 0; i < _attributeTable.Columns.Count; i++)
        //    {
        //        for (int j = 0; j < _groupTable.Columns.Count; j++)
        //        {
        //            if (_attributeTable.Columns[i].ColumnName == _groupTable.Columns[j].ColumnName)
        //            {
        //                _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_ID", _attributeTable.Rows[0][i].ToString());
        //                sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
        //                SQLString = sqlStr;
        //                DataSet dss = CreateDataSet();
        //                if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
        //                {
        //                    try
        //                    {
        //                        _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
        //                    }
        //                    catch (Exception exception)
        //                    {
        //                        //_exception.Exceptions(exception); 
        //                    }
        //                }
        //                else
        //                {
        //                    _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", _attributeTable.Rows[1][i].ToString());
        //                }
        //                _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());
        //            }
        //        }
        //    }

        //    #region " Date and Time Format Process for ROW"
        //    for (int i = 0; i < _attributeTable.Columns.Count; i++)
        //    {
        //        for (int j = 0; j < _rowTable.Columns.Count; j++)
        //        {
        //            if (_attributeTable.Columns[i].ColumnName == _rowTable.Columns[j].ColumnName)
        //            {
        //                if (_attributeTable.Rows[0][i] != null && _attributeTable.Rows[0][i] != "")
        //                {
        //                    sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
        //                    SQLString = sqlStr;
        //                    var attrDatatype = CreateDataSet();
        //                    if (attrDatatype.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Date"))
        //                    {
        //                        sqlStr = "SELECT ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID=" + _attributeTable.Rows[0][i] + "";
        //                        SQLString = sqlStr;
        //                        DataSet attrDataformat = CreateDataSet();
        //                        if (attrDataformat.Tables[0].Rows.Count > 0)
        //                        {
        //                            if (attrDataformat.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
        //                            {
        //                                for (int k = 0; k < _rowTable.Columns.Count; k++)
        //                                {
        //                                    if (_rowTable.Columns[k] == _rowTable.Columns[j])
        //                                    {
        //                                        foreach (DataRow dr in _rowTable.Rows)
        //                                        {
        //                                            if (dr[_rowTable.Columns[k].ColumnName].ToString() != "")
        //                                            {
        //                                                string tempRowDate = dr[_rowTable.Columns[k].ColumnName].ToString();
        //                                                //string tempSrcDate = _sourceTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();

        //                                                dr[_rowTable.Columns[k].ColumnName] = tempRowDate.Substring(3, 2) + "/" + tempRowDate.Substring(0, 2) + "/" + tempRowDate.Substring(6, 4);
        //                                                //_sourceTable.Rows[j][_rowTable.Columns[j].ToString()] = tempSrcDate.Substring(3, 2) + "/" + tempSrcDate.Substring(0, 2) + "/" + tempSrcDate.Substring(6, 4);
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                                //_rowTable.Rows[j]["Prod Tech Attr Date and Time"]

        //                                //dr["STRING_VALUE"] = temp.Substring(3, 2) + "/" + temp.Substring(0, 2) + "/" + temp.Substring(6, 4);
        //                            }
        //                        }

        //                    }
        //                }
        //            }
        //        }
        //    }

        //    // if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Date"))
        //    //{                            
        //    //    sqlStr = "SELECT ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID=" + _attributeTable.Rows[0][i]+"";
        //    //    _oc.SQLString = sqlStr;
        //    //    DataSet AttrDataformat = _oc.CreateDataSet();
        //    //    if (AttrDataformat.Tables[0].Rows.Count > 0)
        //    //    {
        //    //        if (AttrDataformat.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
        //    //        {
        //    //            string tempRowDate = _rowTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();
        //    //            string tempSrcDate = _sourceTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();

        //    //            _rowTable.Rows[j][_rowTable.Columns[j].ToString()] = tempRowDate.Substring(3, 2) + "/" + tempRowDate.Substring(0, 2) + "/" + tempRowDate.Substring(6, 4);
        //    //            _sourceTable.Rows[j][_rowTable.Columns[j].ToString()] = tempSrcDate.Substring(3, 2) + "/" + tempSrcDate.Substring(0, 2) + "/" + tempSrcDate.Substring(6, 4);

        //    //            //_rowTable.Rows[j]["Prod Tech Attr Date and Time"]

        //    //            //dr["STRING_VALUE"] = temp.Substring(3, 2) + "/" + temp.Substring(0, 2) + "/" + temp.Substring(6, 4);
        //    //        }
        //    //    }

        //    //}
        //    #endregion
        //}

        private void SetAttributePropertiesForSourceTable()
        {

            string sqlStr;

            for (int i = 0; i < _attributeTable.Columns.Count; i++)
            {
                for (int j = 0; j < _sourceTable.Columns.Count; j++)
                {
                    if (_attributeTable.Columns[i].ColumnName == _sourceTable.Columns[j].ColumnName)
                    {
                        _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_ID", _attributeTable.Rows[0][i].ToString());
                        sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
                        SQLString = sqlStr;
                        DataSet dss = CreateDataSet();
                        sqlStr = "SELECT ATTRIBUTE_TYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
                        SQLString = sqlStr;
                        DataSet dstyp = CreateDataSet();
                        string type = string.Empty;
                        if (dstyp.Tables[0].Rows.Count > 0)
                        {
                            type = dstyp.Tables[0].Rows[0][0].ToString();
                        }

                        //_sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE",
                        //                                               dss.Tables[0].Rows[0].ItemArray[0].ToString()
                        //                                                                                 .StartsWith(
                        //                                                                                     "Num")
                        //                                                   ? "4"
                        //                                                   : _attributeTable.Rows[1][i].ToString());

                        _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE",
                                                                    dss.Tables[0].Rows[0].ItemArray[0].ToString()
                                                                                                      .StartsWith(
                                                                                                          "Num")
                                                                        ? "" + type + ""
                                                                        : _attributeTable.Rows[1][i].ToString());

                        _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());

                        #region "Date and Time Formating"
                        if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Date"))
                        {
                            sqlStr = "SELECT ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID =" + _attributeTable.Rows[0][i];
                            SQLString = sqlStr;
                            DataSet attrformat = CreateDataSet();
                            if (attrformat.Tables[0].Rows.Count > 0)
                            {
                                if (attrformat.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
                                {
                                    for (int k = 0; k < _sourceTable.Columns.Count; k++)
                                    {
                                        if (_sourceTable.Columns[k] == _sourceTable.Columns[j])
                                        {
                                            foreach (DataRow dr in _sourceTable.Rows)
                                            {
                                                if (dr[_sourceTable.Columns[k].ColumnName].ToString() != null && dr[_sourceTable.Columns[k].ColumnName].ToString() != "")
                                                {
                                                    string tempRowDate = dr[_sourceTable.Columns[k].ColumnName].ToString();
                                                    //string tempSrcDate = _sourceTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();

                                                    dr[_sourceTable.Columns[k].ColumnName] = tempRowDate.Substring(3, 2) + "/" + tempRowDate.Substring(0, 2) + "/" + tempRowDate.Substring(6, 4);
                                                    //_sourceTable.Rows[j][_rowTable.Columns[j].ToString()] = tempSrcDate.Substring(3, 2) + "/" + tempSrcDate.Substring(0, 2) + "/" + tempSrcDate.Substring(6, 4);
                                                }
                                            }
                                        }
                                    }


                                }

                            }
                        }

                        #endregion
                    }
                }
            }

            for (int i = 0; i < _attributeTable.Columns.Count; i++)
            {
                for (int j = 0; j < _columnTable.Columns.Count; j++)
                {
                    if (_attributeTable.Columns[i].ColumnName == _columnTable.Columns[j].ColumnName)
                    {
                        _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_ID", _attributeTable.Rows[0][i].ToString());
                        sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
                        SQLString = sqlStr;
                        DataSet dss = CreateDataSet();
                        sqlStr = "SELECT ATTRIBUTE_TYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
                        SQLString = sqlStr;
                        DataSet dstyp = CreateDataSet();
                        string type = string.Empty;
                        if (dstyp.Tables[0].Rows.Count > 0)
                        {
                            type = dstyp.Tables[0].Rows[0][0].ToString();
                        }
                        if (type == "6")
                        {
                            try
                            {
                                _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", _attributeTable.Rows[1][i].ToString());
                            }
                            catch (Exception exception)
                            {
                                //_exception.Exceptions(exception); 
                            }
                            _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());
                        }
                        else
                        {
                            if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
                            {
                                try
                                {
                                    _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
                                    _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
                                }
                                catch (Exception exception)
                                {
                                    //_exception.Exceptions(exception); 
                                }
                            }
                            else
                            {
                                _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", _attributeTable.Rows[1][i].ToString());
                            }
                            _columnTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());
                        }
                        #region "Date and Time Formating"
                        if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Date"))
                        {
                            sqlStr = "SELECT ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID =" + _attributeTable.Rows[0][i];
                            SQLString = sqlStr;
                            DataSet attrformat = CreateDataSet();
                            if (attrformat.Tables[0].Rows.Count > 0)
                            {
                                if (attrformat.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
                                {
                                    for (int k = 0; k < _columnTable.Columns.Count; k++)
                                    {
                                        if (_columnTable.Columns[k] == _columnTable.Columns[j])
                                        {
                                            foreach (DataRow dr in _columnTable.Rows)
                                            {
                                                if (dr[_columnTable.Columns[k].ColumnName].ToString() != null && dr[_columnTable.Columns[k].ColumnName].ToString() != "")
                                                {
                                                    string tempRowDate = dr[_columnTable.Columns[k].ColumnName].ToString();
                                                    //string tempSrcDate = _sourceTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();

                                                    dr[_columnTable.Columns[k].ColumnName] = tempRowDate.Substring(3, 2) + "/" + tempRowDate.Substring(0, 2) + "/" + tempRowDate.Substring(6, 4);
                                                    //_sourceTable.Rows[j][_rowTable.Columns[j].ToString()] = tempSrcDate.Substring(3, 2) + "/" + tempSrcDate.Substring(0, 2) + "/" + tempSrcDate.Substring(6, 4);
                                                }
                                            }
                                        }
                                    }


                                }

                            }
                        }

                        #endregion
                    }
                }
            }

            for (int i = 0; i < _attributeTable.Columns.Count; i++)
            {
                for (int j = 0; j < _rowTable.Columns.Count; j++)
                {
                    if (_attributeTable.Columns[i].ColumnName == _rowTable.Columns[j].ColumnName)
                    {
                        _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_ID", _attributeTable.Rows[0][i].ToString());
                        sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
                        SQLString = sqlStr;
                        DataSet dss = CreateDataSet();

                        sqlStr = "SELECT ATTRIBUTE_TYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
                        SQLString = sqlStr;
                        DataSet dstyp = CreateDataSet();
                        string type = string.Empty;
                        if (dstyp.Tables[0].Rows.Count > 0)
                        {
                            type = dstyp.Tables[0].Rows[0][0].ToString();
                        }
                        if (type == "6")
                        {
                            try
                            {
                                _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", _attributeTable.Rows[1][i].ToString());
                            }
                            catch (Exception exception)
                            {
                                //_exception.Exceptions(exception); 
                            }
                            _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());
                        }
                        else
                        {
                            if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
                            {
                                try
                                {
                                    _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
                                    _sourceTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
                                }
                                catch (Exception exception)
                                { //_exception.Exceptions(exception); 
                                }
                            }

                            else
                            {
                                _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", _attributeTable.Rows[1][i].ToString());
                            }
                            _rowTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());
                        }
                    }
                }
            }
            for (int i = 0; i < _attributeTable.Columns.Count; i++)
            {
                for (int j = 0; j < _groupTable.Columns.Count; j++)
                {
                    if (_attributeTable.Columns[i].ColumnName == _groupTable.Columns[j].ColumnName)
                    {
                        _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_ID", _attributeTable.Rows[0][i].ToString());
                        sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
                        SQLString = sqlStr;
                        DataSet dss = CreateDataSet();
                        sqlStr = "SELECT ATTRIBUTE_TYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
                        SQLString = sqlStr;
                        DataSet dstyp = CreateDataSet();
                        string type = string.Empty;
                        if (dstyp.Tables[0].Rows.Count > 0)
                        {
                            type = dstyp.Tables[0].Rows[0][0].ToString();
                        }
                        if (type == "6")
                        {
                            try
                            {
                                _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", _attributeTable.Rows[1][i].ToString());
                            }
                            catch (Exception exception)
                            {
                                //_exception.Exceptions(exception); 
                            }
                            _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());
                        }
                        else
                        {
                            if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
                            {
                                try
                                {
                                    _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", "4");
                                }
                                catch (Exception exception)
                                {
                                    //_exception.Exceptions(exception); 
                                }
                            }
                            else
                            {
                                _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_TYPE", _attributeTable.Rows[1][i].ToString());
                            }
                            _groupTable.Columns[j].ExtendedProperties.Add("ATTRIBUTE_STYLE", _attributeTable.Rows[2][i].ToString());
                        }
                    }
                }
            }

            #region " Date and Time Format Process for ROW"
            for (int i = 0; i < _attributeTable.Columns.Count; i++)
            {
                for (int j = 0; j < _rowTable.Columns.Count; j++)
                {
                    if (_attributeTable.Columns[i].ColumnName == _rowTable.Columns[j].ColumnName)
                    {
                        if (_attributeTable.Rows[0][i] != null && _attributeTable.Rows[0][i] != "")
                        {
                            sqlStr = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _attributeTable.Rows[0][i];
                            SQLString = sqlStr;
                            DataSet AttrDatatype = CreateDataSet();
                            if (AttrDatatype.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Date"))
                            {
                                sqlStr = "SELECT ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID=" + _attributeTable.Rows[0][i] + "";
                                SQLString = sqlStr;
                                DataSet AttrDataformat = CreateDataSet();
                                if (AttrDataformat.Tables[0].Rows.Count > 0)
                                {
                                    if (AttrDataformat.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
                                    {
                                        for (int k = 0; k < _rowTable.Columns.Count; k++)
                                        {
                                            if (_rowTable.Columns[k] == _rowTable.Columns[j])
                                            {
                                                foreach (DataRow dr in _rowTable.Rows)
                                                {
                                                    if (dr[_rowTable.Columns[k].ColumnName].ToString() != null && dr[_rowTable.Columns[k].ColumnName].ToString() != "")
                                                    {
                                                        string tempRowDate = dr[_rowTable.Columns[k].ColumnName].ToString();
                                                        //string tempSrcDate = _sourceTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();

                                                        dr[_rowTable.Columns[k].ColumnName] = tempRowDate.Substring(3, 2) + "/" + tempRowDate.Substring(0, 2) + "/" + tempRowDate.Substring(6, 4);
                                                        //_sourceTable.Rows[j][_rowTable.Columns[j].ToString()] = tempSrcDate.Substring(3, 2) + "/" + tempSrcDate.Substring(0, 2) + "/" + tempSrcDate.Substring(6, 4);
                                                    }
                                                }
                                            }
                                        }
                                        //_rowTable.Rows[j]["Prod Tech Attr Date and Time"]

                                        //dr["STRING_VALUE"] = temp.Substring(3, 2) + "/" + temp.Substring(0, 2) + "/" + temp.Substring(6, 4);
                                    }
                                }

                            }
                        }
                    }
                }
            }

            // if (dss.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Date"))
            //{                            
            //    sqlStr = "SELECT ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID=" + _attributeTable.Rows[0][i]+"";
            //    _oc.SQLString = sqlStr;
            //    DataSet AttrDataformat = _oc.CreateDataSet();
            //    if (AttrDataformat.Tables[0].Rows.Count > 0)
            //    {
            //        if (AttrDataformat.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
            //        {
            //            string tempRowDate = _rowTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();
            //            string tempSrcDate = _sourceTable.Rows[j][_rowTable.Columns[j].ToString()].ToString();

            //            _rowTable.Rows[j][_rowTable.Columns[j].ToString()] = tempRowDate.Substring(3, 2) + "/" + tempRowDate.Substring(0, 2) + "/" + tempRowDate.Substring(6, 4);
            //            _sourceTable.Rows[j][_rowTable.Columns[j].ToString()] = tempSrcDate.Substring(3, 2) + "/" + tempSrcDate.Substring(0, 2) + "/" + tempSrcDate.Substring(6, 4);

            //            //_rowTable.Rows[j]["Prod Tech Attr Date and Time"]

            //            //dr["STRING_VALUE"] = temp.Substring(3, 2) + "/" + temp.Substring(0, 2) + "/" + temp.Substring(6, 4);
            //        }
            //    }

            //}
            #endregion
        }

        void SplitRowtableInToLeftAndRight()
        {
            _leftRowTable = _rowTable.Copy();
            _rightRowTable = _rowTable.Copy();
            for (int i = _leftRowTable.Columns.Count - 1; i >= 0; i--)
            {
                bool match = _mLeftRowFields.Any(t => _leftRowTable.Columns[i].Caption == t);
                if (match == false)
                {
                    _leftRowTable.Columns.RemoveAt(i);
                }
            }
            for (int i = _rightRowTable.Columns.Count - 1; i >= 0; i--)
            {
                bool match = _mRightRowFields.Any(t => _rightRowTable.Columns[i].Caption == t);
                if (match == false)
                {
                    _rightRowTable.Columns.RemoveAt(i);
                }
            }
        }
        private void ConstructGroupedPivot()
        {
            DataTable srcTable = _rowTable.Copy();
            DataTable colTable = _columnTable.Copy();

            if (_columnTable != null && _mPivotHeaderText != null && _mPivotHeaderText.Trim() != string.Empty)
            {
                _columnTable.Columns.Add("PivotHeader");
                _columnTable.Columns["PivotHeader"].ExtendedProperties.Add("ATTRIBUTE_ID", "-1");
                _columnTable.Columns["PivotHeader"].ExtendedProperties.Add("ATTRIBUTE_TYPE", "");
                _columnTable.Columns["PivotHeader"].ExtendedProperties.Add("ATTRIBUTE_STYLE", "");
                _columnTable.Columns["PivotHeader"].SetOrdinal(0);

                _sourceTable.Columns.Add("PivotHeader");
                _sourceTable.Columns["PivotHeader"].ExtendedProperties.Add("ATTRIBUTE_ID", "-1");
                _sourceTable.Columns["PivotHeader"].ExtendedProperties.Add("ATTRIBUTE_TYPE", "");
                _sourceTable.Columns["PivotHeader"].ExtendedProperties.Add("ATTRIBUTE_STYLE", "");


                for (int i = 0; i < _columnTable.Rows.Count; i++)
                {
                    _columnTable.Rows[i]["PivotHeader"] = _mPivotHeaderText.Trim();
                }
                for (int i = 0; i < _sourceTable.Rows.Count; i++)
                {
                    _sourceTable.Rows[i]["PivotHeader"] = _mPivotHeaderText.Trim();
                }
            }

            if (_mShowColumnHeaders && _columnTable != null)
            {

                for (int i = 0; i < colTable.Columns.Count; i++)
                {
                    _columnTable.Columns.Add(colTable.Columns[i].Caption + "`Header");
                    _columnTable.Columns[_columnTable.Columns.Count - 1].ExtendedProperties.Add("ATTRIBUTE_ID", "-1");
                    _columnTable.Columns[_columnTable.Columns.Count - 1].ExtendedProperties.Add("ATTRIBUTE_TYPE", "");
                    _columnTable.Columns[_columnTable.Columns.Count - 1].ExtendedProperties.Add("ATTRIBUTE_STYLE", "");
                    _columnTable.Columns[_columnTable.Columns.Count - 1].SetOrdinal(_columnTable.Columns[colTable.Columns[i].Caption].Ordinal);

                    _sourceTable.Columns.Add(colTable.Columns[i].Caption + "`Header");
                    _sourceTable.Columns[_sourceTable.Columns.Count - 1].ExtendedProperties.Add("ATTRIBUTE_ID", "-1");
                    _sourceTable.Columns[_sourceTable.Columns.Count - 1].ExtendedProperties.Add("ATTRIBUTE_TYPE", "");
                    _sourceTable.Columns[_sourceTable.Columns.Count - 1].ExtendedProperties.Add("ATTRIBUTE_STYLE", "");
                }

                for (int i = 0; i < _columnTable.Rows.Count; i++)
                {
                    for (int j = 0; j < _columnTable.Columns.Count; j++)
                    {
                        if (_columnTable.Columns[j].Caption.Contains("`Header"))
                        {
                            string[] colCaption = _columnTable.Columns[j].Caption.Split('`');
                            try // to handle empty and null value in columns. making null and emoty as unique
                            {
                                if (Convert.ToString(colCaption[0]) == "")
                                {
                                    if (_columnTable.Rows[i - 1][j].ToString() == colCaption[0])
                                        _columnTable.Rows[i].Delete();
                                    else
                                        _columnTable.Rows[i][j] = colCaption[0];
                                }
                                else
                                    _columnTable.Rows[i][j] = colCaption[0];
                            }
                            catch (Exception e)
                            {
                                if (e.Message.Contains("There is no row at position -1."))
                                    _columnTable.Rows[i][j] = colCaption[0];
                            }
                        }
                    }
                }

                for (int i = 0; i < _sourceTable.Rows.Count; i++)
                {
                    for (int j = 0; j < _sourceTable.Columns.Count; j++)
                    {
                        if (_sourceTable.Columns[j].Caption.Contains("`Header"))
                        {
                            string[] colCaption = _sourceTable.Columns[j].Caption.Split('`');
                            _sourceTable.Rows[i][j] = colCaption[0];
                        }
                    }
                }
            }


            if (_mTableGroupField != string.Empty)
                //  rowTable.Columns.Remove(m_tableGroupField);

                //Prepare Summary Fileds for Grouping
                if (_mSummaryGroupField.Trim() != string.Empty)
                {
                    for (int i = 0; i < _mSummaryFields.Count; i++)
                    {
                        if (_mSummaryFields[i].Trim() == _mSummaryGroupField.Trim())
                        {
                            _mSummaryFields.RemoveAt(i);
                            i = 0;
                        }
                    }
                    _mSummaryFields.Add(_mSummaryGroupField.Trim());
                }

            //int leftRowDepth = GetNodeDepth(leftRowNode);

            //this.GetCurrencySymbol();
            _pivotTable = new DataTable();
            Int32 bodyFieldCount = (_mSummaryFields.Count - objGroupedSummaryAttrs.Count(a => a.Value != "None")) + objGroupedSummaryAttrs.Values.Where(a => a != "None").Distinct().Count();
            if (_columnTable != null)
            {
                Int32 columnCount = (_columnTable.Rows.Count * bodyFieldCount) + _leftRowTable.Columns.Count + _rightRowTable.Columns.Count;

                //Add columns to Pivot Table            
                for (int i = 0; i < columnCount; i++)
                {
                    string colName = "Column" + i;
                    _pivotTable.Columns.Add(colName);
                }
            }

            //int lastColumnFieldIndex = 0;

            //Construct Header Columns
            //========================
            int leftTreeDepth = GetLeftTreeDepth();
            int rightTreeDepth = GetRightTreeDepth();
            int headerRowsCount = leftTreeDepth > rightTreeDepth ? leftTreeDepth : rightTreeDepth;

            if (_columnTable != null && headerRowsCount < _columnTable.Columns.Count)
                headerRowsCount = _columnTable.Columns.Count;

            //if (m_showColumnHeaders == true)
            //{ headerRowsCount++; }

            //if (m_pivotHeaderText.Trim() != string.Empty)
            //{ headerRowsCount++; }

            if (_mShowSummaryHeaders == false && _mShowRowHeaders == false && _mShowColumnHeaders == false)
            {
                if (_columnTable != null) headerRowsCount = _columnTable.Columns.Count;
            }

            if (_mColumnFields.Count <= 0 && _mSummaryFields.Count <= 0 && _mShowRowHeaders == false) { headerRowsCount = 0; }

            for (int j = 0; j < headerRowsCount; j++)
            {
                DataRow dr = _pivotTable.NewRow();
                Int32 colPosition = 0;

                //Set value for Corner 
                for (int k = 0; k < _leftRowTable.Columns.Count; k++)
                {
                    if (_mShowRowHeaders)
                    {
                        string colName = GetNthParent(_mLeftRowNodes[k], j);

                        for (int t = 0; t < _pivotTable.Rows.Count; t++)
                        {
                            if (colName == _pivotTable.Rows[t][k].ToString())
                                colName = "";
                        }
                        if (_pivotTable.Rows.Count > 0 && colName == _pivotTable.Rows[_pivotTable.Rows.Count - 1][k].ToString())
                            colName = "";

                        if (colName == string.Empty)
                            dr[k] = _leftRowTable.Columns[k].ColumnName;
                        else
                            dr[k] = colName;

                    }
                    colPosition = colPosition + 1;
                }

                int colIndex = j;
                if (j >= _columnTable.Columns.Count) { colIndex = colIndex - 1; }
                for (int p = 0; p < _columnTable.Rows.Count; p++)
                {
                    int forCount = objGroupedSummaryAttrs.Values.Where(a => a != "None").Distinct().Any() ? objGroupedSummaryAttrs.Values.Where(a => a != "None").Distinct().Count() + objGroupedSummaryAttrs.Values.Count(a => a == "None") : _mSummaryFields.Count;
                    for (int i = 0; i < forCount; i++)
                    {
                        GetCurrencySymbol(_columnTable.Columns[colIndex].ExtendedProperties["ATTRIBUTE_ID"].ToString());
                        if (_columnTable.Columns[colIndex].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "4")//|| columnTable.Columns[j].ExtendedProperties["ATTRIBUTE_ID"].ToString() == "3")
                        {
                            string currencyValue = string.Empty;
                            if (_columnTable.Rows[p][colIndex].ToString().Trim().Length > 0)
                            {
                                if (Convert.ToString(_columnTable.Rows[p][colIndex].ToString().Trim()) != string.Empty && _columnTable.Rows[p][colIndex].ToString().Trim() != "0.00" && _columnTable.Rows[p][colIndex].ToString().Trim() != "0" && _columnTable.Rows[p][colIndex].ToString().Trim() != "0.000000")
                                {
                                    currencyValue = ApplyStyleFormat(Convert.ToInt32(_columnTable.Columns[colIndex].ExtendedProperties["ATTRIBUTE_ID"].ToString().Trim()), _columnTable.Rows[p][colIndex].ToString().Trim());
                                }
                                else
                                {
                                    if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000")
                                    {
                                        if ((_replaceText != "") && (Isnumber(_replaceText)))
                                            currencyValue = _emptyCondition == _columnTable.Rows[p][colIndex].ToString().Trim() ? _replaceText : _columnTable.Rows[p][colIndex].ToString().Trim();
                                        else
                                            currencyValue = _columnTable.Rows[p][colIndex].ToString().Trim();
                                    }
                                    else
                                        currencyValue = ApplyStyleFormat(Convert.ToInt32(_columnTable.Columns[colIndex].ExtendedProperties["ATTRIBUTE_ID"].ToString().Trim()), _columnTable.Rows[p][colIndex].ToString().Trim());
                                }
                            }
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null || _emptyCondition == "0" || _emptyCondition == "0.000000" || _emptyCondition == "0.00") && (currencyValue == string.Empty))
                            {
                                if (_fornumeric == "1")
                                {
                                    if (Isnumber(_replaceText.Replace(",", "")))
                                        dr[colPosition] = _prefix + _replaceText + _suffix;
                                    else
                                        dr[colPosition] = _replaceText;
                                }
                                else
                                    dr[colPosition] = _prefix + _replaceText + _suffix;
                                //dr[colPosition] = _replaceText;
                            }
                            else if (currencyValue == _emptyCondition || _columnTable.Rows[p][colIndex].ToString().Trim() == _emptyCondition)
                            {
                                if (_fornumeric == "1")
                                {
                                    if (Isnumber(_replaceText.Replace(",", "")))
                                        dr[colPosition] = _prefix + _replaceText + _suffix;
                                    else
                                        dr[colPosition] = _replaceText;
                                }
                                else
                                    dr[colPosition] = _prefix + _replaceText + _suffix;
                                // dr[colPosition] = _replaceText;
                            }
                            else
                            {
                                if (_headeroptions == "All")
                                {
                                    if (Isnumber(currencyValue.Replace(",", "")))
                                    {
                                        dr[colPosition] = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                    else if (_columnTable.Columns[colIndex].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "4")
                                    {
                                        dr[colPosition] = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }

                                    else
                                    {
                                        dr[colPosition] = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                }
                                else if (j == 0 && _headeroptions == "First")
                                {
                                    if (Isnumber(currencyValue.Replace(",", "")))
                                    {
                                        dr[colPosition] = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                    else
                                    {
                                        dr[colPosition] = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                }
                                else
                                {
                                    dr[colPosition] = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                            }
                        }
                        else if (_columnTable.Columns[colIndex].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "3")
                        {
                            if (_columnTable.Columns[colIndex].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "3")
                            {
                                if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("ImageColFields"))
                                    _pivotTable.Columns[colPosition].ExtendedProperties.Add("ImageColFields", "True");
                            }
                            dr[colPosition] = _columnTable.Rows[p][colIndex].ToString();
                        }
                        else
                        {
                            if (_columnTable.Rows[p][colIndex].ToString().Trim() != string.Empty)
                            {
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000") && (_columnTable.Rows[p][colIndex].ToString() == string.Empty))
                                {
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                            dr[colPosition] = _prefix + _replaceText + _suffix;
                                        else
                                            dr[colPosition] = _replaceText;
                                    }
                                    else
                                        dr[colPosition] = _prefix + _replaceText + _suffix;
                                    // dr[colPosition] = _replaceText;
                                }
                                else if (_columnTable.Rows[p][colIndex].ToString() == _emptyCondition)
                                {
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                            dr[colPosition] = _prefix + _replaceText + _suffix;
                                        else
                                            dr[colPosition] = _replaceText;
                                    }
                                    else
                                        dr[colPosition] = _prefix + _replaceText + _suffix;
                                    //dr[colPosition] = _replaceText;
                                }
                                else
                                {
                                    /* Correction here for grouped */

                                    if (_headeroptions == "All")
                                    {
                                        dr[colPosition] = _prefix + _columnTable.Rows[p][colIndex] + _suffix;
                                    }
                                    else if (p == 0 && _headeroptions == "First") /* colPosition == 0 && */
                                    {
                                        dr[colPosition] = _prefix + _columnTable.Rows[p][colIndex] + _suffix;
                                    }
                                    else
                                    {
                                        dr[colPosition] = _columnTable.Rows[p][colIndex].ToString();
                                    }
                                }
                            }
                            else if (_columnTable.Rows[p][colIndex].ToString().Trim() == "")
                            {
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000") && (_columnTable.Rows[p][colIndex].ToString() == string.Empty))
                                {
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                            dr[colPosition] = _prefix + _replaceText + _suffix;
                                        else
                                            dr[colPosition] = _replaceText;
                                    }
                                    else
                                        dr[colPosition] = _prefix + _replaceText + _suffix;
                                }
                                else if (_columnTable.Rows[p][colIndex].ToString() == _emptyCondition)
                                {
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                            dr[colPosition] = _prefix + _replaceText + _suffix;
                                        else
                                            dr[colPosition] = _replaceText;
                                    }
                                    else
                                        dr[colPosition] = _prefix + _replaceText + _suffix;
                                    //dr[colPosition] = _replaceText;
                                }
                                //dr[colPosition] = _replaceText;
                            }
                            else
                            {
                                /* Correction here for grouped */

                                if (_headeroptions == "All")
                                {
                                    dr[colPosition] = _prefix + _columnTable.Rows[p][colIndex] + _suffix;
                                }
                                else if (p == 0 && _headeroptions == "First") /* colPosition == 0 && */
                                {
                                    dr[colPosition] = _prefix + _columnTable.Rows[p][colIndex] + _suffix;
                                }
                                else
                                {
                                    dr[colPosition] = _columnTable.Rows[p][colIndex].ToString();
                                }
                            }
                        }
                        dr[colPosition] = dr[colPosition].ToString().Trim();
                        colPosition = colPosition + 1;
                    }
                }

                //Set value for Right Corner 
                if (_mShowRowHeaders)
                {
                    for (int k = 0; k < _rightRowTable.Columns.Count; k++)
                    {
                        string colName = GetNthParentFromRightNodeTree(_mRightRowNodes[k], j);

                        if (colName == string.Empty)
                            dr[colPosition] = _rightRowTable.Columns[k].ColumnName;
                        else
                            dr[colPosition] = colName;

                        dr[colPosition] = dr[colPosition].ToString().Trim();
                        colPosition = colPosition + 1;
                    }
                }
                _headerCnt = _headerCnt + 1;
                _pivotTable.Rows.Add(dr);
            }

            int colIndexVar = 0;
            //Set value for Rows 
            for (int k = 0; k < _leftRowTable.Columns.Count; k++)
            {
                //Set Attribute Info
                if (!_pivotTable.Columns[k].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                {
                    _pivotTable.Columns[k].ExtendedProperties.Add("ATTRIBUTE_STYLE", _leftRowTable.Columns[k].ExtendedProperties["ATTRIBUTE_STYLE"].ToString());
                }
                colIndexVar = colIndexVar + 1;
            }

            //Set value for Summary Fields
            for (int r = 0; r < _pivotTable.Columns.Count - _rightRowTable.Columns.Count; r++)
            {
                for (int t = 0; t < _mSummaryFields.Count; t++)
                {
                    if (colIndexVar < _pivotTable.Columns.Count - _rightRowTable.Columns.Count)
                    {
                        colIndexVar = colIndexVar + 1;
                    }
                }
            }

            for (int k = 0; k < _rightRowTable.Columns.Count; k++)
            {
                //Set Attribute Info
                if (!_pivotTable.Columns[colIndexVar].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                {
                    _pivotTable.Columns[colIndexVar].ExtendedProperties.Add("ATTRIBUTE_STYLE", _rightRowTable.Columns[k].ExtendedProperties["ATTRIBUTE_STYLE"].ToString());
                }
                colIndexVar = colIndexVar + 1;
            }

            //Create Header for Rows and Summary Fields
            //=========================================
            if ((_mShowSummaryHeaders || _mShowRowHeaders) && headerRowsCount > 0)
            {
                DataRow drow = _pivotTable.NewRow();
                int[] colPos = { 0 };

                if (_mShowRowHeaders)
                {
                    //Set value for Rows 
                    for (int k = 0; k < _leftRowTable.Columns.Count; k++)
                    {
                        if (_leftRowTable.Columns[k].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "3")
                        {
                            if (!_pivotTable.Columns[k].ExtendedProperties.ContainsKey("ImageRowFields"))
                                _pivotTable.Columns[k].ExtendedProperties.Add("ImageRowFields", "True");
                        }

                        //Set Attribute Info
                        if (!_pivotTable.Columns[k].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                        {
                            _pivotTable.Columns[k].ExtendedProperties.Add("ATTRIBUTE_STYLE", _leftRowTable.Columns[k].ExtendedProperties["ATTRIBUTE_STYLE"].ToString());
                        }
                        if (_mShowRowHeaders)
                        {
                            drow[k] = _leftRowTable.Columns[k].ColumnName;
                        }
                        else
                        {
                            drow[colPos[0]] = _pivotTable.Rows[_pivotTable.Rows.Count - 1][colPos[0]].ToString();
                        }
                        colPos[0] = colPos[0] + 1;
                    }
                }

                //Set value for Summary Fields
                for (int r = 0; r < _pivotTable.Columns.Count - _rightRowTable.Columns.Count; r++)
                {
                    foreach (string t1 in _mSummaryFields.Where(t1 => colPos[0] < _pivotTable.Columns.Count - _rightRowTable.Columns.Count))
                    {
                        if (_mShowSummaryHeaders && colPos[0] >= _leftRowTable.Columns.Count)
                        {
                            drow[colPos[0]] = t1;
                        }
                        else
                        {
                            drow[colPos[0]] = _pivotTable.Rows[_pivotTable.Rows.Count - 1][colPos[0]].ToString();
                        }
                        drow[colPos[0]] = drow[colPos[0]].ToString().Trim();
                        colPos[0] = colPos[0] + 1;
                    }
                }


                if (_mShowRowHeaders)
                {
                    //Set value for Right Rows 
                    for (int k = 0; k < _rightRowTable.Columns.Count; k++)
                    {
                        if (_rightRowTable.Columns[k].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "3")
                        {
                            if (!_pivotTable.Columns[colPos[0]].ExtendedProperties.ContainsKey("ImageRowFields"))
                                _pivotTable.Columns[colPos[0]].ExtendedProperties.Add("ImageRowFields", "True");
                        }

                        //Set Attribute Info
                        if (!_pivotTable.Columns[colPos[0]].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                        {
                            _pivotTable.Columns[colPos[0]].ExtendedProperties.Add("ATTRIBUTE_STYLE", _rightRowTable.Columns[k].ExtendedProperties["ATTRIBUTE_STYLE"].ToString());
                        }
                        if (_mShowRowHeaders)
                        {
                            drow[colPos[0]] = _rightRowTable.Columns[k].ColumnName;
                        }
                        else
                        {
                            drow[colPos[0]] = _pivotTable.Rows[_pivotTable.Rows.Count - 1][colPos[0]].ToString();
                        }
                        colPos[0] = colPos[0] + 1;
                    }
                }
                _headerCnt = _headerCnt + 1;
                _pivotTable.Rows.Add(drow);
            }


            if (_groupTable.Rows.Count > 0)
            {

                // rowTable.Columns.Add(m_tableGroupField);
                for (int k = 0; k < _groupTable.Rows.Count; k++)
                {
                    //     rowTable.Columns.Add(m_tableGroupField);
                    _rowTable = srcTable.Copy();
                    string filterCond = "[" + _mTableGroupField + "] = " + "'" + Prepare(_groupTable.Rows[k][0].ToString()) + "'";
                    srcTable.CaseSensitive = true;
                    DataRow[] drows = srcTable.Select(filterCond);
                    srcTable.CaseSensitive = false;
                    _rowTable.Rows.Clear();

                    drows.CopyToDataTable(_rowTable, LoadOption.OverwriteChanges);
                    //       rowTable.Columns.Remove(m_tableGroupField);
                    DataRow dr = _pivotTable.NewRow();

                    for (int y = 0; y < _pivotTable.Columns.Count; y++)
                    {
                        dr[y] = _groupTable.Rows[k][0].ToString();
                        {
                            string currencyValue = string.Empty;
                            GetCurrencySymbol(_groupTable.Columns[0].ExtendedProperties["ATTRIBUTE_ID"].ToString().Trim());
                            if (dr[y].ToString().Trim().Length > 0)
                            {
                                if (Convert.ToString(dr[y].ToString()) != string.Empty && dr[y].ToString() != "0.00" && dr[y].ToString() != "0" && dr[y].ToString() != "0.000000")
                                    currencyValue = ApplyStyleFormat(Convert.ToInt32(_groupTable.Columns[0].ExtendedProperties["ATTRIBUTE_ID"].ToString().Trim()), dr[y].ToString().Trim());
                                else
                                {
                                    if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000")
                                    {
                                        if ((_replaceText != "") && (Isnumber(_replaceText)))
                                        {
                                            currencyValue = _emptyCondition == dr[y].ToString().Trim() ? _replaceText : dr[y].ToString().Trim();
                                        }
                                        else
                                            currencyValue = dr[y].ToString().Trim();
                                    }
                                    else
                                        currencyValue = ApplyStyleFormat(Convert.ToInt32(_groupTable.Columns[0].ExtendedProperties["ATTRIBUTE_ID"].ToString().Trim()), dr[y].ToString().Trim());
                                }
                            }
                            if (currencyValue == "0")
                            {
                                //       currencyValue = rowTable.Rows[k][leftRowTable.Columns[colPosition].Caption].ToString().Trim();
                            }
                            //Double PriceValue = Convert.ToDouble(rowTable.Rows[k][colPosition].ToString());
                            //currencyValue = PriceValue.ToString(currencyFormat);
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null || _emptyCondition == "0" || _emptyCondition == "0.00") && (currencyValue == string.Empty))
                            {
                                if (_fornumeric == "1")
                                {
                                    if (Isnumber(_replaceText.Replace(",", "")))
                                        dr[y] = _prefix + _replaceText + _suffix;
                                    else
                                        dr[y] = _replaceText;
                                }
                                else
                                    dr[y] = _prefix + _replaceText + _suffix;
                            }
                            else if (currencyValue == _emptyCondition || dr[y].ToString().Trim() == _emptyCondition)
                            {
                                if (_fornumeric == "1")
                                {
                                    if (Isnumber(_replaceText.Replace(",", "")))
                                        dr[y] = _prefix + _replaceText + _suffix;
                                    else
                                        dr[y] = _replaceText;
                                }
                                else
                                    dr[y] = _prefix + _replaceText + _suffix;
                            }
                            else
                            {
                                if (_headeroptions == "All")
                                {
                                    
                                    if (Isnumber(currencyValue.Replace(",", "")))
                                    {
                                        dr[y] = _prefix + currencyValue.Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                    //jo added
                                    //else if (_groupTable.Columns[k].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "4")
                                    //{
                                    //    dr[y] = _prefix + currencyValue.Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    //}
                                    //else if(Isnumber(currencyValue))
                                    //{
                                    //    dr[y] = _prefix + currencyValue.Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    //}
                                    else
                                    {
                                        dr[y] = currencyValue.Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                }
                                else if (_headeroptions == "First")
                                {
                                    if (Isnumber(currencyValue.Replace(",", "")))
                                    {
                                        dr[y] = _prefix + currencyValue.Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                    else
                                    {
                                        dr[y] = currencyValue.Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                }
                                else
                                {
                                    dr[y] = " " + currencyValue.Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + " ";
                                }
                            }
                            //dr[l] = Prefix  + currencyValue +Suffix ;
                            dr[y] = dr[y].ToString().Trim();
                        }
                    }

                    dr.RowError = @"GroupRow";
                    //headerCnt = headerCnt + 1;
                    _pivotTable.Rows.Add(dr);

                    ConstructPivotTable();
                }
            }
            else
            {
                ConstructPivotTable();
            }

            ClearUnPublishedAttrs();

            //Group Summary Fileds
            if (_mSummaryGroupField.Trim() != string.Empty)
            {
                SetGroupSummaryField();
            }
        }
        private void ClearUnPublishedAttrs()
        {
            for (int i = 0; i < _pivotTable.Columns.Count; i++)
            {
                bool status = false;
                for (int j = 0; j < _attrTablePivot.Rows.Count; j++)
                {
                    if (_pivotTable.Columns[i].ExtendedProperties["Attribute_ID"] != null && _pivotTable.Columns[i].ExtendedProperties["Attribute_ID"].ToString().Split(',').Contains(_attrTablePivot.Rows[j][0].ToString()))
                    {
                        status = true;
                    }
                }
                if (status == false)
                {
                    _pivotTable.Columns.RemoveAt(i);
                    i--;
                }
            }
        }
        private string GenerateVerticalXmlForTransformation(int PreviewData, int user_Id)
        {
            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.a.CustomerId == user_Id).Select(x => x.a.Comments).FirstOrDefault();

            if (string.IsNullOrEmpty(CustomerFolder))
            { CustomerFolder = "/STUDIOSOFT"; }
            else
            { CustomerFolder = CustomerFolder.Replace("&", ""); }

            var rowCountTable = new DataTable();
            rowCountTable.Columns.Add("RowMergeCounter");
            for (int i = 0; i < _pivotTable.Rows.Count; i++)
                rowCountTable.Rows.Add(rowCountTable.NewRow()[0] = "0");

            var strBldr = new StringBuilder();
            strBldr.Append("<?xml version=\"1.0\"?>");

            int leftTreeDepth = GetLeftTreeDepth() + 1;
            int rightTreeDepth = GetRightTreeDepth() + 1;
            int headerRowsCount = leftTreeDepth > rightTreeDepth ? leftTreeDepth : rightTreeDepth;

            if (headerRowsCount < _columnTable.Columns.Count)
                headerRowsCount = _columnTable.Columns.Count;

            if (_mColumnFields.Count <= 0 && _mSummaryFields.Count <= 0 && _mShowRowHeaders == false) { headerRowsCount = 0; }

            int hmCount = 0;

            if (_pivotTable.Rows.Count > 0 && _rowTable.Rows.Count > 0)
            {
                //Initialize the Extended properties for use in vertical merging
                for (int j = 0; j < _pivotTable.Columns.Count; j++)
                {
                    _pivotTable.Columns[j].ExtendedProperties.Remove("MergeCounter");
                    _pivotTable.Columns[j].ExtendedProperties.Add("MergeCounter", "0");
                }

                //int rowCount = pivotTable.Rows.Count;
                //if ((m_showRowHeaders == true && GetLeftTreeDepth() == 1 && GetRightTreeDepth() == 1 && m_columnFields.Count == 0 && m_summaryFields.Count == 0) || ((m_showRowHeaders == true && GetLeftTreeDepth() == 1 && GetRightTreeDepth() == 1 && (m_columnFields.Count > 0 || m_summaryFields.Count == 0)) && (m_showColumnHeaders == false && m_showSummaryHeaders == false)))
                //{
                //    rowCount--;
                //    pivotTable.Rows.RemoveAt(0);
                //}

                strBldr.Append("<products nrows=\"" + _pivotTable.Rows.Count + "\"  ncols=\"" + _pivotTable.Columns.Count + "\"  TBGUID=\"P" + _randomClass.Next() + "\">");
                strBldr.Append("<table table=\"table\" ");
                strBldr.Append("trows=\"" + _pivotTable.Columns.Count + "\" tcols=\"" + _pivotTable.Rows.Count + "\"  TBGUID=\"" + _randomClass.Next() + "\"  FORMAT=\"SuperTable\" TRANSPOSE=\"1\">");

                for (int i = 0; i < _pivotTable.Columns.Count; i++)
                {
                    strBldr.Append("<row>");

                    string attributeType = string.Empty;
                    if (_pivotTable.Columns[i].ExtendedProperties.ContainsKey("ATTRIBUTE_TYPE"))
                        attributeType = _pivotTable.Columns[i].ExtendedProperties["ATTRIBUTE_TYPE"].ToString();



                    for (int j = 0; j < _pivotTable.Rows.Count; j++)
                    {
                        string productID = string.Empty;
                        string imageFullPath = "";
                        int srcIndex = j - ((_pivotTable.Rows.Count - _sourceTable.Rows.Count));
                        if (_mColumnFields.Count == 0 && _mSummaryFields.Count == 0 && srcIndex >= 0)
                        {
                            productID = _sourceTable.Rows[srcIndex]["Product_ID"].ToString();
                        }

                        FileInfo chkFile = null;
                        try
                        {
                            chkFile = new FileInfo(_pivotTable.Rows[j][i].ToString());
                        }
                        catch (Exception exception)
                        {// _exception.Exceptions(exception); 
                        }
                        //SystemSettingsCollection SettingMembers = SystemSettingsConfiguration.GetConfig.Members;
                        bool imageType = false;
                        if (chkFile != null)
                        {
                            if (chkFile.Extension != null && chkFile.Extension.Trim().Length > 0)
                                if ((attributeType == "3") && (chkFile.Extension.ToUpper() == ".TIF" || chkFile.Extension.ToUpper() == ".PNG"
                                    || chkFile.Extension.ToUpper() == ".BMP" || chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".GIF"
                                    || chkFile.Extension.ToUpper() == ".EPS" || chkFile.Extension.ToUpper() == ".PDF" || chkFile.Extension.ToUpper() == ".PSD" ||
                                    chkFile.Extension.ToUpper() == ".RTF" || chkFile.Extension.ToUpper() == ".TIFF" || chkFile.Extension.ToUpper() == ".DOC"
                                    || chkFile.Extension.ToUpper() == ".DOCX" || chkFile.Extension.ToUpper() == ".SVG"
                                    || chkFile.Extension.ToUpper() == ".XLS" || chkFile.Extension.ToUpper() == ".XLSX" || chkFile.Extension.ToUpper() == ".ZIP"
                                    || chkFile.Extension.ToUpper() == ".RAR" || chkFile.Extension.ToUpper() == ".EXE" || chkFile.Extension.ToUpper() == ".MSI"
                                    || chkFile.Extension.ToUpper() == ".ICO"))
                                {
                                    if (chkFile.Extension.ToLower().EndsWith(".jpg") || chkFile.Extension.ToLower().EndsWith(".bmp") ||
                                                        chkFile.Extension.ToLower().EndsWith(".gif") || chkFile.Extension.ToLower().EndsWith(".ico"))
                                    {
                                        imageType = true;
                                        imageFullPath = ImagePath + _pivotTable.Rows[j][i];
                                        imageFullPath = "..\\Content\\ProductImages\\" + CustomerFolder + imageFullPath.Replace("&", "&amp;");
                                    }
                                    else if (chkFile.Extension.ToLower().EndsWith(".tif") ||
                                                      chkFile.Extension.ToLower().EndsWith(".tiff") ||
                                                      chkFile.Extension.ToLower().EndsWith(".tga") ||
                                                      chkFile.Extension.ToLower().EndsWith(".pcx") ||
                                                      chkFile.Extension.ToLower().EndsWith(".png"))
                                    {
                                        if (Directory.Exists(ImageMagickPath))
                                        {
                                            //sValue = sValue.Substring(0, sValue.LastIndexOf('.'));
                                            string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + chkFile.ToString(), HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };
                                            string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_Id).Select(a => a.ConverterType).SingleOrDefault();
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                //var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }

                                        }
                                        imageType = true;
                                        imageFullPath = "..\\Content\\ProductImages\\" + CustomerFolder + "\\temp\\ImageandAttFile\\" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                                        imageFullPath = imageFullPath.Replace("&", "");
                                    }
                                    else if (chkFile.Extension.ToLower().EndsWith(".eps"))
                                    {
                                        if (Directory.Exists(ImageMagickPath))
                                        {
                                            string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + chkFile.ToString() , HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile\\" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };
                                            string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_Id).Select(a => a.ConverterType).SingleOrDefault();
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                //var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                        }
                                        imageType = true;
                                        imageFullPath = "..\\Content\\ProductImages\\" + CustomerFolder + "\\temp\\ImageandAttFile\\" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".png";
                                        imageFullPath = imageFullPath.Replace("&", "");
                                    } //else if (chkFile.Extension.ToLower().EndsWith(".doc") || chkFile.Extension.ToLower().EndsWith(".docx"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\doc.png";
                                    //}
                                    //else if (chkFile.Extension.ToLower().EndsWith(".pdf"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\pdf.png";
                                    //}
                                    //else if (chkFile.Extension.ToLower().EndsWith(".rar") || chkFile.Extension.ToLower().EndsWith(".zip"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\zip.png";
                                    //}
                                    //else if (chkFile.Extension.ToLower().EndsWith(".xls") || chkFile.Extension.ToLower().EndsWith(".xlsx"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\xls.png";
                                    //}
                                    //else if (chkFile.Extension.ToLower().EndsWith(".exe") || chkFile.Extension.ToLower().EndsWith(".msi"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\exe.png";
                                    //}

                                    else
                                    {
                                        imageType = true;
                                        imageFullPath = ImagePath + _pivotTable.Rows[j][i];
                                    }
                                }
                        }
                        if (Convert.ToInt32(_pivotTable.Columns[i].ExtendedProperties["MergeCounter"].ToString()) > 0)
                        {
                            int temp = Convert.ToInt32(_pivotTable.Columns[i].ExtendedProperties["MergeCounter"].ToString());
                            temp = temp - 1;
                            _pivotTable.Columns[i].ExtendedProperties["MergeCounter"] = temp.ToString(CultureInfo.InvariantCulture);
                        }

                        if (Convert.ToInt32(rowCountTable.Rows[j][0].ToString()) > 0)
                        {
                            int temp = Convert.ToInt32(rowCountTable.Rows[j][0].ToString());
                            temp = temp - 1;
                            rowCountTable.Rows[j][0] = temp.ToString(CultureInfo.InvariantCulture);
                        }
                        if (hmCount > 0) { hmCount = hmCount - 1; }

                        if (_pivotTable.Rows[j][i].ToString().Trim() == string.Empty)
                        {
                            _pivotTable.Rows[j][i] = _mPlaceHolderText;
                        }
                        if ((j < headerRowsCount))
                        {
                            string cellName = "";
                            if (_pivotTable.Columns[i].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                            {
                                cellName = _pivotTable.Columns[i].ExtendedProperties["ATTRIBUTE_STYLE"].ToString();
                            }
                            if (cellName.Trim() == string.Empty)
                            { cellName = "Cell_Header"; }
                            else
                            { cellName = cellName + "_Header"; }

                            if (rowCountTable.Rows[j][0].ToString() == "0" && Convert.ToInt32(_pivotTable.Columns[i].ExtendedProperties["MergeCounter"]) == 0)
                            {
                                int mColCnt = GetHorizontalMergeCells(i, j);
                                int mRowCnt = GetVerticalMergeCells(i, j, _headerCnt, false, 2);

                                if (mColCnt == 0) { mColCnt = 1; }
                                if (mRowCnt == 0) { mRowCnt = 1; }

                                if (imageType == false)
                                {
                                    if (attributeType == "6")
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"PartsHeader\"  cellStyle=\"" + cellName + "\" table=\"cell\" ccols=\"" + mRowCnt.ToString(CultureInfo.InvariantCulture) + "\" crows=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");
                                    }
                                    else
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"Header\"  cellStyle=\"" + cellName + "\" table=\"cell\" ccols=\"" + mRowCnt.ToString(CultureInfo.InvariantCulture) + "\" crows=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");
                                    }


                                    if (PreviewData == 1)
                                    {
                                        if ((_pivotTable.Rows[j][i].ToString().Contains("<br />")) || (_pivotTable.Rows[j][i].ToString().Contains("&nbsp;")))
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i].ToString().Replace("<br />", "\r").Replace("&nbsp;", " ") + "]]>");
                                        else
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                    }
                                    else
                                    {
                                        strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                    }
                                    //  3 strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                    strBldr.Append("</Cell>");
                                    hmCount = mColCnt;
                                    rowCountTable.Rows[j][0] = mColCnt;
                                    if (mColCnt > 1 && mRowCnt > 1)
                                    {
                                        int addCounter = 0;
                                        for (int u = i; u < i + mColCnt; u++)
                                        {
                                            _pivotTable.Columns[u].ExtendedProperties["MergeCounter"] = (mRowCnt + addCounter).ToString(CultureInfo.InvariantCulture);
                                            addCounter = 1;
                                        }
                                    }
                                    else
                                    {
                                        _pivotTable.Columns[i].ExtendedProperties["MergeCounter"] = mRowCnt.ToString(CultureInfo.InvariantCulture);
                                    }
                                }
                                else//image
                                {
                                    strBldr.Append("<Cell  EmbedImage=\"\"  imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"Header\"  cellStyle=\"" + cellName + "\" table=\"cell\"  crows=\"" + mRowCnt.ToString(CultureInfo.InvariantCulture) + "\" ccols=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");
                                    strBldr.Append("<Cell COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[j][i].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                    strBldr.Append("</Cell>");
                                    hmCount = mColCnt;
                                    rowCountTable.Rows[j][0] = mColCnt;
                                    if (mColCnt > 1 && mRowCnt > 1)
                                    {
                                        int addCounter = 0;
                                        for (int u = i; u < i + mColCnt; u++)
                                        {
                                            _pivotTable.Columns[u].ExtendedProperties["MergeCounter"] = (mRowCnt + addCounter).ToString(CultureInfo.InvariantCulture);
                                            addCounter = 1;
                                        }
                                    }
                                    else
                                    {
                                        _pivotTable.Columns[i].ExtendedProperties["MergeCounter"] = mRowCnt.ToString(CultureInfo.InvariantCulture);
                                    }
                                }
                            }
                        }
                        //For Column Header and ColumnFields - Horizondal Merging
                        else if ((j < headerRowsCount && i >= _leftRowTable.Columns.Count) || _pivotTable.Rows[j].RowError == @"GroupRow")
                        {
                            string cellName = "";
                            if (_pivotTable.Rows[j].RowError != @"GroupRow")
                            {
                                if (_columnTable.Columns[i].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                                {
                                    cellName = _columnTable.Columns[i].ExtendedProperties["ATTRIBUTE_STYLE"].ToString();
                                }
                                if (cellName.Trim() == string.Empty)
                                { cellName = "Cell_Header"; }
                                else
                                { cellName = cellName + "_Header"; }


                                // if (i < rowTable.Columns.Count)
                                // { cellName = cellName + "_Header"; }
                            }

                            //newly Added
                            else
                            {
                                cellName = "Cell_Header";
                            }
                            if (rowCountTable.Rows[j][0].ToString() == "0")
                            {
                                int mColCnt = GetHorizontalMergeCellsForGroupRow(i, j);
                                hmCount = mColCnt;
                                rowCountTable.Rows[j][0] = mColCnt;
                                if (mColCnt > 1)
                                {
                                    if (imageType == false)
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"ColumnField\"  cellStyle=\"" + cellName + "\" table=\"cell\"  ccols=\"1\" crows=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");

                                        if (PreviewData == 1)
                                        {
                                            if ((_pivotTable.Rows[j][i].ToString().Contains("<br />")) || (_pivotTable.Rows[j][i].ToString().Contains("&nbsp;")))
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i].ToString().Replace("<br />", "\r").Replace("&nbsp;", " ") + "]]>");
                                            else
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        }
                                        else
                                        {
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        }
                                        // 4 strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        strBldr.Append("</Cell>");
                                        hmCount = mColCnt;
                                    }
                                    else//image
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  EmbedImage=\"\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"ColumnField\"   cellStyle=\"" + cellName + "\" table=\"cell\"  ccols=\"1\" crows=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");
                                        strBldr.Append("<Cell COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[j][i].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                        strBldr.Append("</Cell>");
                                        hmCount = mColCnt;
                                    }
                                }
                                else
                                {
                                    if (imageType == false)
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"ColumnField\"   cellStyle=\"" + cellName + "\" table=\"cell\"  ccols=\"1\" crows=\"1\">");

                                        if (PreviewData == 1)
                                        {
                                            if ((_pivotTable.Rows[j][i].ToString().Contains("<br />")) || (_pivotTable.Rows[j][i].ToString().Contains("&nbsp;")))
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i].ToString().Replace("<br />", "\r").Replace("&nbsp;", " ") + "]]>");
                                            else
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        }
                                        else
                                        {
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        }
                                        // 5  strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        strBldr.Append("</Cell>");
                                    }
                                    else//image
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  EmbedImage=\"\"  Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"ColumnField\"   cellStyle=\"" + cellName + "\" table=\"cell\" ccols=\"1\" crows=\"1\">");
                                        strBldr.Append("<Cell COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[j][i].ToString().Replace("&", "&amp") + "\"></Cell>");
                                        strBldr.Append("</Cell>");
                                    }
                                }
                            }
                        }
                        //For Row and Summary Fields -- Vertical Merging
                        else if ((j > _columnTable.Columns.Count && (_mShowSummaryHeaders || _mShowRowHeaders)) || (j >= _columnTable.Columns.Count && _mShowSummaryHeaders == false && _mShowRowHeaders == false) || headerRowsCount == 0)
                        {
                            int rowCode = i % 2;

                            string cellName = "";
                            if (_pivotTable.Columns[i].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                            {
                                cellName = _pivotTable.Columns[i].ExtendedProperties["ATTRIBUTE_STYLE"].ToString();
                            }
                            if (cellName.Trim() == string.Empty)
                            {
                                cellName = "Cell";
                            }

                            if (Convert.ToInt32(_pivotTable.Columns[i].ExtendedProperties["MergeCounter"].ToString()) == 0)
                            {
                                int mRowCnt = GetVerticalMergeCells(i, j, _headerCnt, true, 2);
                                if (((mRowCnt > 1 && _mMergeSummaryFields && i >= _rowTable.Columns.Count) || (mRowCnt > 1 && _mMergeRowHeaders && j < _rowTable.Columns.Count) || (mRowCnt > 1 && _mMergeRowHeaders && _mMergeSummaryFields)) && (_pivotTable.Columns[i].ExtendedProperties["Merge"] != null && _pivotTable.Columns[i].ExtendedProperties["Merge"].ToString() == "Checked"))
                                {
                                    if (imageType == false)
                                    {
                                        strBldr.Append("<Cell rowID=\"" + rowCode + "\" imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"SummaryField\"   cellStyle=\"" + cellName + "\" attribute_id=\"" + _pivotTable.Columns[i].ExtendedProperties["Attribute_ID"] + "\" table=\"cell\" ccols=\" " + mRowCnt.ToString(CultureInfo.InvariantCulture) + " \" crows=\"1\">");

                                        if (PreviewData == 1)
                                        {
                                            if ((_pivotTable.Rows[j][i].ToString().Contains("<br />")) || (_pivotTable.Rows[j][i].ToString().Contains("&nbsp;")))
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i].ToString().Replace("<br />", "\r").Replace("&nbsp;", " ") + "]]>");
                                            else
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        }
                                        else
                                        {
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        }
                                        // 6 strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        strBldr.Append("</Cell>");
                                        _pivotTable.Columns[i].ExtendedProperties["MergeCounter"] = mRowCnt.ToString(CultureInfo.InvariantCulture);
                                    }
                                    else//image
                                    {
                                        strBldr.Append("<Cell rowID=\"" + rowCode + "\"  imagePath=\"" + imageFullPath + "\"  EmbedImage=\"\"  Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"SummaryField\"   cellStyle=\"" + cellName + "\" attribute_id=\"" + _pivotTable.Columns[i].ExtendedProperties["Attribute_ID"] + "\" table=\"cell\" ccols=\" " + mRowCnt.ToString(CultureInfo.InvariantCulture) + " \" crows=\"1\">");
                                        strBldr.Append("<Cell COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[j][i].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                        strBldr.Append("</Cell>");
                                        _pivotTable.Columns[i].ExtendedProperties["MergeCounter"] = mRowCnt.ToString(CultureInfo.InvariantCulture);
                                    }
                                }
                                else
                                {
                                    if (imageType == false)
                                    {
                                        strBldr.Append("<Cell rowID=\"" + rowCode + "\"  imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"SummaryField\"  cellStyle=\"" + cellName + "\" attribute_id=\"" + _pivotTable.Columns[i].ExtendedProperties["Attribute_ID"] + "\" table=\"cell\" crows=\" 1 \" ccols=\"1\">");

                                        if (PreviewData == 1)
                                        {
                                            if ((_pivotTable.Rows[j][i].ToString().Contains("<br />")) || (_pivotTable.Rows[j][i].ToString().Contains("&nbsp;")))
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i].ToString().Replace("<br />", "\r").Replace("&nbsp;", " ") + "]]>");
                                            else
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        }
                                        else
                                        {
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        }
                                        //1 strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                        strBldr.Append("</Cell>");
                                    }
                                    else
                                    {
                                        strBldr.Append("<Cell rowID=\"" + rowCode + "\" imagePath=\"" + imageFullPath + "\"  EmbedImage=\"\"  Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"SummaryField\"   cellStyle=\"" + cellName + "\" attribute_id=\"" + _pivotTable.Columns[i].ExtendedProperties["Attribute_ID"] + "\" table=\"cell\" crows=\" 1 \" ccols=\"1\">");
                                        strBldr.Append("<Cell COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[j][i].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                        strBldr.Append("</Cell>");
                                    }
                                }
                            }

                        }
                        else if (j == headerRowsCount)//Row Headers and Summary Headers
                        {
                            if (Convert.ToInt32(_pivotTable.Columns[i].ExtendedProperties["MergeCounter"]) == 0)
                            {
                                string cellName = "";
                                if (_pivotTable.Columns[i].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                                {
                                    cellName = _pivotTable.Columns[i].ExtendedProperties["ATTRIBUTE_STYLE"].ToString();
                                }
                                if (cellName.Trim() == string.Empty)
                                {
                                    cellName = "Cell_Header";
                                }
                                else
                                {
                                    cellName = cellName + "_Header";
                                }
                                if (imageType == false)
                                {
                                    strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"SummaryHeader\"  cellStyle=\"" + cellName + "\" table=\"cell\" crows=\" 1 \" ccols=\"1\">");

                                    if (PreviewData == 1)
                                    {
                                        if ((_pivotTable.Rows[j][i].ToString().Contains("<br />")) || (_pivotTable.Rows[j][i].ToString().Contains("&nbsp;")))
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i].ToString().Replace("<br />", "\r").Replace("&nbsp;", " ") + "]]>");
                                        else
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                    }
                                    else
                                    {
                                        strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                    }


                                    // 2 strBldr.Append("<![CDATA[" + _pivotTable.Rows[j][i] + "]]>");
                                    strBldr.Append("</Cell>");
                                }
                                else
                                {
                                    strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\" EmbedImage=\"\"   Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\"  cellArea=\"SummaryHeader\"  cellStyle=\"" + cellName + "\" table=\"cell\"  endHeader=\"\" crows=\" 1 \" ccols=\"1\">");
                                    strBldr.Append("<Cell COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[j][i].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                    strBldr.Append("</Cell>");
                                }
                            }
                        }
                    }
                    strBldr.Append("</row>");
                }
                strBldr.Append("</table>");
                strBldr.Append("</products>");

            }
            return strBldr.ToString();
            // return null;
        }
        private string GenerateXmlForTransformation(int previewData, int user_Id)
        {
            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.a.CustomerId == user_Id).Select(x => x.a.Comments).FirstOrDefault();
            if (string.IsNullOrEmpty(CustomerFolder))
            { CustomerFolder = "/STUDIOSOFT"; }
            else
            { CustomerFolder = CustomerFolder.Replace("&", ""); }

            int tempGrouAttrCount = 0;
            var strBldr = new StringBuilder();
            strBldr.Append("<?xml version=\"1.0\"?>");
            int hmCount = 0;

            int leftTreeDepth = GetLeftTreeDepth() + 1;
            int rightTreeDepth = GetRightTreeDepth() + 1;
            int headerRowsCount = leftTreeDepth > rightTreeDepth ? leftTreeDepth : rightTreeDepth;

            if (headerRowsCount < _columnTable.Columns.Count)
                headerRowsCount = _columnTable.Columns.Count;

            if (_mColumnFields.Count <= 0 && _mSummaryFields.Count <= 0 && _mShowRowHeaders == false) { headerRowsCount = 0; }
            foreach (DataRow item in _pivotTable.Rows)
            {
                foreach (DataColumn item1 in _pivotTable.Columns)
                {
                    item[item1.ColumnName] = item[item1.ColumnName].ToString().Replace("\n", "");
                }
            }
            if (_pivotTable.Rows.Count > 0 && _rowTable.Rows.Count > 0)
            {
                //Initialize the Extended properties for use in vertical merging
                for (int j = 0; j < _pivotTable.Columns.Count; j++)
                {
                    _pivotTable.Columns[j].ExtendedProperties.Remove("MergeCounter");
                    _pivotTable.Columns[j].ExtendedProperties.Add("MergeCounter", "0");
                }

                strBldr.Append("<products nrows=\"" + _pivotTable.Rows.Count + "\"  ncols=\"" + _pivotTable.Columns.Count + "\"  TBGUID=\"P" + _randomClass.Next() + "\">");
                strBldr.Append("<table table=\"table\" class=\"table table-condensed table-bordered table-striped\" ");
                strBldr.Append("trows=\"" + _pivotTable.Rows.Count + "\" tcols=\"" + _pivotTable.Columns.Count + "\"  TBGUID=\"" + _randomClass.Next() + "\"  FORMAT=\"SuperTable\" TRANSPOSE=\"0\">");

                for (int i = 0; i < _pivotTable.Rows.Count; i++)
                {
                    string productID = string.Empty;
                    int srcIndex = i - ((_pivotTable.Rows.Count - _sourceTable.Rows.Count));
                    if (_mColumnFields.Count == 0 && _mSummaryFields.Count == 0 && srcIndex >= 0)
                    {
                        productID = _sourceTable.Rows[srcIndex]["Product_ID"].ToString();
                    }

                    strBldr.Append("<row>");
                    for (int j = 0; j < _pivotTable.Columns.Count; j++)
                    {

                        string attributeType = string.Empty;
                        if (_pivotTable.Columns[j].ExtendedProperties.ContainsKey("ATTRIBUTE_TYPE"))
                        {
                            if (_pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_TYPE"].ToString().Split(',').Length > 1)
                            {
                                attributeType = _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_TYPE"].ToString().Split(',').GetValue(tempGrouAttrCount).ToString();
                            }
                            else
                            {
                                attributeType = _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_TYPE"].ToString();
                            }
                        }

                        string imageFullPath = "";

                        FileInfo chkFile = null;
                        try
                        {
                            chkFile = new FileInfo(_pivotTable.Rows[i][j].ToString());
                        }
                        catch (Exception exception)
                        { //_exception.Exceptions(exception); 
                        }
                        bool imageType = false;
                        ///////SystemSettingsCollection SettingMembers = SystemSettingsConfiguration.GetConfig.Members;
                        if (chkFile != null)
                        {
                            if (chkFile.Extension != null && chkFile.Extension.Trim().Length > 0)
                                if ((attributeType == "3") && (chkFile.Extension.ToUpper() == ".AI" || chkFile.Extension.ToUpper() == ".BMP"
                                     || chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".GIF" || chkFile.Extension.ToUpper() == ".EPS"
                                     || chkFile.Extension.ToUpper() == ".PDF" || chkFile.Extension.ToUpper() == ".PSD" || chkFile.Extension.ToUpper() == ".PNG"
                                     || chkFile.Extension.ToUpper() == ".TIF" || chkFile.Extension.ToUpper() == ".RTF" || chkFile.Extension.ToUpper() == ".TIFF"
                                     || chkFile.Extension.ToUpper() == ".DOC" || chkFile.Extension.ToUpper() == ".DOCX" || chkFile.Extension.ToUpper() == ".SVG"
                                     || chkFile.Extension.ToUpper() == ".XLS" || chkFile.Extension.ToUpper() == ".XLSX" || chkFile.Extension.ToUpper() == ".ZIP"
                                     || chkFile.Extension.ToUpper() == ".RAR" || chkFile.Extension.ToUpper() == ".EXE" || chkFile.Extension.ToUpper() == ".MSI"
                                     || chkFile.Extension.ToUpper() == ".ICO"))
                                {
                                    if (chkFile.Extension.ToLower().EndsWith(".jpg") || chkFile.Extension.ToLower().EndsWith(".bmp") ||
                                                        chkFile.Extension.ToLower().EndsWith(".gif") || chkFile.Extension.ToLower().EndsWith(".ico") ||
                                                      chkFile.Extension.ToLower().EndsWith(".png"))
                                    {

                                        imageType = true;
                                        imageFullPath = ImagePath + _pivotTable.Rows[i][j];
                                        imageFullPath = "..\\Content\\ProductImages\\" + CustomerFolder + imageFullPath.Replace("&", "&amp;");
                                    }
                                    else if (chkFile.Extension.ToLower().EndsWith(".tif") ||
                                                      chkFile.Extension.ToLower().EndsWith(".tiff") ||
                                                      chkFile.Extension.ToLower().EndsWith(".tga") ||
                                                      chkFile.Extension.ToLower().EndsWith(".pcx"))
                                    {
                                        string file_name = Convert.ToString(_sourceTable.Rows[srcIndex]["Product_ID"]) + "_" + chkFile.Name;
                                        if (Directory.Exists(ImageMagickPath))
                                        {
                                            //string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages") + chkFile, HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };
                                            string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + chkFile, HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg" };
                                            string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_Id).Select(a => a.ConverterType).SingleOrDefault();
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                //var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                        }
                                        imageType = true;
                                        //imageFullPath = "..\\Content\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                                        imageFullPath = "..\\Content\\ProductImages\\" + CustomerFolder + "\\temp\\ImageandAttFile\\" + file_name + ".jpg";
                                        imageFullPath = imageFullPath.Replace("&", "");
                                    }
                                    else if (chkFile.Extension.ToLower().EndsWith(".psd"))
                                    {
                                        string file_name = Convert.ToString(_sourceTable.Rows[srcIndex]["Product_ID"]) + "_" + chkFile.Name;
                                        if (Directory.Exists(ImageMagickPath))
                                        {
                                            //string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages") + chkFile + "[0]", HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg" };
                                            string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + chkFile + "[0]", HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg" };
                                            string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_Id).Select(a => a.ConverterType).SingleOrDefault();
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                //var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                        }
                                        imageType = true;
                                        //imageFullPath = "..\\Content\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                                        imageFullPath = "..\\Content\\ProductImages\\" + CustomerFolder + "\\temp\\ImageandAttFile\\" + file_name + ".jpg";
                                        imageFullPath = imageFullPath.Replace("&", "");
                                    }
                                    else if (chkFile.Extension.ToLower().EndsWith(".eps"))
                                    {
                                        string file_name = Convert.ToString(_sourceTable.Rows[srcIndex]["Product_ID"]) + "_" + chkFile.Name;
                                        if (Directory.Exists(ImageMagickPath))
                                        {
                                            //string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages") + chkFile, HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".png" };
                                            string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + chkFile, HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".png" };
                                            string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_Id).Select(a => a.ConverterType).SingleOrDefault();
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                //var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                                if (File.Exists(imgargs[1].Replace("&", "")))
                                                {
                                                    File.Delete(imgargs[1].Replace("&", ""));
                                                }
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                        }
                                        imageType = true;
                                        //imageFullPath = "..\\Content\\temp\\ImageandAttFile" + chkFile.ToString().Substring(chkFile.ToString().LastIndexOf('\\'), chkFile.ToString().LastIndexOf('.') - chkFile.ToString().LastIndexOf('\\')) + ".png";
                                        imageFullPath = "..\\Content\\ProductImages\\" + CustomerFolder + "\\temp\\ImageandAttFile\\" + file_name + ".png";
                                        imageFullPath = imageFullPath.Replace("&", "");
                                    }  //else if (chkFile.Extension.ToLower().EndsWith(".doc") || chkFile.Extension.ToLower().EndsWith(".docx"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\doc.png";
                                    //}
                                    //else if (chkFile.Extension.ToLower().EndsWith(".pdf"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\pdf.png";
                                    //}
                                    //else if (chkFile.Extension.ToLower().EndsWith(".rar") || chkFile.Extension.ToLower().EndsWith(".zip"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\zip.png";
                                    //}
                                    //else if (chkFile.Extension.ToLower().EndsWith(".xls") || chkFile.Extension.ToLower().EndsWith(".xlsx"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\xls.png";
                                    //}
                                    //else if (chkFile.Extension.ToLower().EndsWith(".exe") || chkFile.Extension.ToLower().EndsWith(".msi"))
                                    //{
                                    //    imageType = true;
                                    //    imageFullPath = "..\\Content\\images\\exe.png";
                                    //}

                                    else
                                    {
                                        imageType = true;
                                        imageFullPath = ImagePath + CustomerFolder + _pivotTable.Rows[i][j];
                                    }

                                }
                        }

                        if (_pivotTable.Rows[i][j].ToString().Trim() == string.Empty)
                        {
                            _pivotTable.Rows[i][j] = _mPlaceHolderText;
                        }

                        if (Convert.ToInt32(_pivotTable.Columns[j].ExtendedProperties["MergeCounter"].ToString()) > 0)
                        {
                            int temp = Convert.ToInt32(_pivotTable.Columns[j].ExtendedProperties["MergeCounter"].ToString());
                            temp = temp - 1;
                            _pivotTable.Columns[j].ExtendedProperties["MergeCounter"] = temp.ToString(CultureInfo.InvariantCulture);
                        }

                        if (hmCount > 0) { hmCount = hmCount - 1; }

                        if ((i < headerRowsCount))
                        {
                            string cellName = _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_STYLE"].ToString().Split(',').Length > 1 ? _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_STYLE"].ToString().Split(',').GetValue(tempGrouAttrCount).ToString() : _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_STYLE"].ToString();
                            if (_pivotTable.Columns[j].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE_HEADER"))
                            {
                                cellName = _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_STYLE_HEADER"].ToString();
                            }
                            if (cellName.Trim() != string.Empty)
                            { cellName = cellName + "_Header"; }
                            else
                            { cellName = "Cell_Header"; }

                            if (hmCount == 0 && Convert.ToInt32(_pivotTable.Columns[j].ExtendedProperties["MergeCounter"]) == 0)
                            {
                                int mColCnt = GetHorizontalMergeCells(j, i);

                                int mRowCnt = GetVerticalMergeCells(j, i, _headerCnt, true, 1);


                                if (mColCnt == 0) { mColCnt = 1; }
                                if (mRowCnt == 0) { mRowCnt = 1; }

                                if (imageType == false)
                                {
                                    if (attributeType == "6")
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\" cellArea=\"PartsHeader\" cellStyle=\"" + cellName + "\" table=\"cell\" theader=\"\" crows=\"" + mRowCnt.ToString(CultureInfo.InvariantCulture) + "\" ccols=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");
                                    }
                                    else
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\" TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\" cellArea=\"Header\" cellStyle=\"" + cellName + "\" table=\"cell\" theader=\"\" crows=\"" + mRowCnt.ToString(CultureInfo.InvariantCulture) + "\" ccols=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");
                                    }
                                    strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j] + "]]>");

                                    strBldr.Append("</Cell>");
                                    hmCount = mColCnt;
                                    if (mColCnt > 1 && mRowCnt > 1)
                                    {
                                        int addCounter = 0;
                                        for (int u = j; u < j + mColCnt; u++)
                                        {
                                            _pivotTable.Columns[u].ExtendedProperties["MergeCounter"] = (mRowCnt + addCounter).ToString(CultureInfo.InvariantCulture);
                                            addCounter = 1;
                                        }
                                    }
                                    else
                                    {
                                        _pivotTable.Columns[j].ExtendedProperties["MergeCounter"] = mRowCnt.ToString(CultureInfo.InvariantCulture);
                                    }
                                }
                                else//image
                                {
                                    strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\" Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" cellArea=\"Header\" EmbedImage=\"\"  cellStyle=\"" + cellName + "\" table=\"cell\" theader=\"\" crows=\"" + mRowCnt.ToString(CultureInfo.InvariantCulture) + "\" ccols=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");
                                    strBldr.Append("<Cell Attribute_type=\"" + attributeType + "\" product_ID=\"" + productID + "\" cellStyle=\"image_name\" COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[i][j].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                    strBldr.Append("</Cell>");
                                    hmCount = mColCnt;
                                    if (mColCnt > 1 && mRowCnt > 1)
                                    {
                                        int addCounter = 0;
                                        for (int u = j; u < j + mColCnt; u++)
                                        {
                                            _pivotTable.Columns[u].ExtendedProperties["MergeCounter"] = (mRowCnt + addCounter).ToString(CultureInfo.InvariantCulture);
                                            addCounter = 1;
                                        }
                                    }
                                    else
                                    {
                                        _pivotTable.Columns[j].ExtendedProperties["MergeCounter"] = mRowCnt.ToString(CultureInfo.InvariantCulture);
                                    }
                                }
                            }
                        }
                        //For Column Header and ColumnFields - Horizondal Merging
                        else if ((i < headerRowsCount && j >= _leftRowTable.Columns.Count) || _pivotTable.Rows[i].RowError == @"GroupRow")
                        {
                            string cellName = "Cell";
                            if (_pivotTable.Rows[i].RowError != @"GroupRow")
                            {
                                if (_columnTable.Columns[i].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                                {
                                    cellName = _columnTable.Columns[i].ExtendedProperties["ATTRIBUTE_STYLE"].ToString();
                                }
                                if (cellName.Trim() != string.Empty)
                                { cellName = cellName + "_Header"; }
                                else
                                { cellName = "Cell_Header"; }

                                if (j < _rowTable.Columns.Count)
                                { cellName = cellName + "_Header"; }
                            }

                            if (hmCount == 0)
                            {
                                int mColCnt = GetHorizontalMergeCellsForGroupRow(j, i);
                                if (mColCnt > 1)
                                {
                                    if (imageType == false)
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\"  cellArea=\"ColumnField\"  cellStyle=\"" + cellName + "\" table=\"cell\" crows=\"1\" ccols=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");
                                        strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j] + "]]>");
                                        strBldr.Append("</Cell>");
                                        hmCount = mColCnt;
                                    }
                                    else//image
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" cellArea=\"ColumnField\"  EmbedImage=\"\"   cellStyle=\"" + cellName + "\" table=\"cell\" crows=\"1\" ccols=\"" + mColCnt.ToString(CultureInfo.InvariantCulture) + "\">");
                                        strBldr.Append("<Cell product_ID=\"" + productID + "\"   Attribute_type=\"" + attributeType + "\"  cellStyle=\"image_name\"   COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[i][j].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                        strBldr.Append("</Cell>");
                                        hmCount = mColCnt;
                                    }
                                }
                                else
                                {
                                    if (imageType == false)
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" cellArea=\"ColumnField\"   cellStyle=\"" + cellName + "\" table=\"cell\" crows=\"1\" ccols=\"1\">");
                                        strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j] + "]]>");
                                        strBldr.Append("</Cell>");
                                    }
                                    else//image
                                    {
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"   TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\" cellArea=\"ColumnField\"   EmbedImage=\"\"  cellStyle=\"" + cellName + "\" table=\"cell\" crows=\"1\" ccols=\"1\">");
                                        strBldr.Append("<Cell  Attribute_type=\"" + attributeType + "\"  product_ID=\"" + productID + "\" cellStyle=\"image_name\"  COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[i][j].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                        strBldr.Append("</Cell>");
                                    }
                                }
                            }
                        }
                        //For Row and Summary Fields -- Vertical Merging
                        else if ((i > _columnTable.Columns.Count && (_mShowSummaryHeaders || _mShowRowHeaders)) || (i >= _columnTable.Columns.Count && _mShowSummaryHeaders == false && _mShowRowHeaders == false) || headerRowsCount == 0)
                        {
                            string cellName = "";
                            if (_pivotTable.Columns[j].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                            {
                                //foreach (string t1 in objGroupedSummaryAttrs.Values.Distinct())
                                if (_pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_STYLE"].ToString().Split(',').Length > 1)
                                {
                                    cellName = _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_STYLE"].ToString().Split(',').GetValue(tempGrouAttrCount).ToString();
                                }
                                else
                                {
                                    cellName = _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_STYLE"].ToString();
                                }
                                //else
                                //{
                                //    if (j > 1)
                                //    {
                                //        cellName = "Model";
                                //    }
                                //    else
                                //    {
                                //        cellName = _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_STYLE"].ToString();
                                //    }
                                //}
                            }
                            if (cellName == string.Empty)
                            {
                                cellName = "Cell";
                            }

                            if (Convert.ToInt32(_pivotTable.Columns[j].ExtendedProperties["MergeCounter"].ToString()) == 0)
                            {
                                string attrId;
                                if (_pivotTable.Columns[j].ExtendedProperties["Attribute_ID"].ToString().Split(',').Length > 1)
                                {
                                    attrId = _pivotTable.Columns[j].ExtendedProperties["Attribute_ID"].ToString().Split(',').GetValue(tempGrouAttrCount).ToString();
                                }
                                else
                                {
                                    attrId = _pivotTable.Columns[j].ExtendedProperties["Attribute_ID"].ToString();
                                }
                                int mRowCnt = GetVerticalMergeCells(j, i, _headerCnt, false, 1);
                                int rowCode = i % 2;
                                if (((mRowCnt > 1 && _mMergeSummaryFields && j >= _rowTable.Columns.Count) || (mRowCnt > 1 && _mMergeRowHeaders && j < _rowTable.Columns.Count) || (mRowCnt > 1 && _mMergeRowHeaders && _mMergeSummaryFields)) && (_pivotTable.Columns[j].ExtendedProperties["Merge"] != null && _pivotTable.Columns[j].ExtendedProperties["Merge"].ToString() == "Checked"))
                                {
                                    if (imageType == false)
                                    {

                                        //if (j == 0)
                                        //    strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" rowID=\"" + rowCode + "\" cellArea=\"RowField\"   cellStyle=\"" + cellName + "\" attribute_id=\"" + attrId + "\" table=\"cell\" crows=\" " + mRowCnt.ToString(CultureInfo.InvariantCulture) + " \" ccols=\"1\">");
                                        //else
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" rowID=\"" + rowCode + "\" cellArea=\"SummaryField\"   cellStyle=\"" + cellName + "\" attribute_id=\"" + attrId + "\" table=\"cell\" crows=\" " + mRowCnt.ToString(CultureInfo.InvariantCulture) + " \" ccols=\"1\">");

                                        //// Replacing <br /> tag wit /r

                                        if (previewData == 1)
                                        {
                                            if ((_pivotTable.Rows[i][j].ToString().Contains("<br />")) || (_pivotTable.Rows[i][j].ToString().Contains("&nbsp;")))
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j].ToString().Replace("<br />", "\r").Replace("&nbsp;", " ") + "]]>");
                                            else
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j] + "]]>");
                                        }
                                        else
                                        {
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j] + "]]>");
                                        }


                                        strBldr.Append("</Cell>");
                                        _pivotTable.Columns[j].ExtendedProperties["MergeCounter"] = mRowCnt.ToString(CultureInfo.InvariantCulture);
                                    }
                                    else //image
                                    {
                                        if (j == 0)
                                            strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" rowID=\"" + rowCode + "\"  EmbedImage=\"\" cellArea=\"RowField\" cellStyle=\"" + cellName + "\" attribute_id=\"" + attrId + "\" table=\"cell\" crows=\" " + mRowCnt.ToString(CultureInfo.InvariantCulture) + " \" ccols=\"1\">");
                                        else
                                            strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" rowID=\"" + rowCode + "\"  EmbedImage=\"\" cellArea=\"SummaryField\" cellStyle=\"" + cellName + "\" attribute_id=\"" + attrId + "\" table=\"cell\" crows=\" " + mRowCnt.ToString(CultureInfo.InvariantCulture) + " \" ccols=\"1\">");

                                        strBldr.Append("<Cell  Attribute_type=\"" + attributeType + "\"   product_ID=\"" + productID + "\" cellStyle=\"image_name\" COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[i][j].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                        strBldr.Append("</Cell>");
                                        _pivotTable.Columns[j].ExtendedProperties["MergeCounter"] = mRowCnt.ToString(CultureInfo.InvariantCulture);
                                    }
                                }
                                else
                                {
                                    if (imageType == false)
                                    {
                                        //if (j == 0)
                                        //    strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" rowID=\"" + rowCode + "\" cellArea=\"RowField\" cellStyle=\"" + cellName + "\" attribute_id=\"" + attrId + "\" table=\"cell\" crows=\" 1 \" ccols=\"1\">");
                                        //else
                                        strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" rowID=\"" + rowCode + "\" cellArea=\"SummaryField\" cellStyle=\"" + cellName + "\" attribute_id=\"" + attrId + "\" table=\"cell\" crows=\" 1 \" ccols=\"1\">");

                                        // Replacing <br /> tag wit /r
                                        if (previewData == 1)
                                        {
                                            if ((_pivotTable.Rows[i][j].ToString().Contains("<br />")) || (_pivotTable.Rows[i][j].ToString().Contains("&nbsp;")))
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j].ToString().Replace("<br />", "\r").Replace("&nbsp;", " ") + "]]>");
                                            else
                                                strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j] + "]]>");

                                        }
                                        else
                                        {
                                            strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j] + "]]>");
                                        }


                                        strBldr.Append("</Cell>");
                                    }
                                    else
                                    {
                                        if (j == 0)
                                            strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\" rowID=\"" + rowCode + "\"  EmbedImage=\"\" cellArea=\"RowField\" cellStyle=\"" + cellName + "\" attribute_id=\"" + attrId + "\" table=\"cell\" crows=\" 1 \" ccols=\"1\">");
                                        else
                                            strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\" rowID=\"" + rowCode + "\"  EmbedImage=\"\" cellArea=\"SummaryField\" cellStyle=\"" + cellName + "\" attribute_id=\"" + attrId + "\" table=\"cell\" crows=\" 1 \" ccols=\"1\">");

                                        strBldr.Append("<Cell Attribute_type=\"" + attributeType + "\"   product_ID=\"" + productID + "\" cellStyle=\"image_name\"  COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[i][j].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                        strBldr.Append("</Cell>");
                                    }
                                }
                            }
                        }
                        else if (i == headerRowsCount)//Row Headers and Summary Headers
                        {
                            if (Convert.ToInt32(_pivotTable.Columns[j].ExtendedProperties["MergeCounter"]) == 0)
                            {
                                string cellName = "";
                                if (_pivotTable.Columns[j].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                                {
                                    cellName = _pivotTable.Columns[j].ExtendedProperties["ATTRIBUTE_STYLE"].ToString();
                                }

                                if (cellName.Trim() == string.Empty)
                                { cellName = "Cell_Header"; }
                                else
                                { cellName = cellName + "_Header"; }



                                if (imageType == false)
                                {
                                    strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\"  product_ID=\"" + productID + "\" cellArea=\"SummaryHeader\" cellStyle=\"" + cellName + "\" table=\"cell\" theader=\"\" crows=\" 1 \" ccols=\"1\">");
                                    strBldr.Append("<![CDATA[" + _pivotTable.Rows[i][j] + "]]>");
                                    strBldr.Append("</Cell>");
                                }
                                else
                                {
                                    strBldr.Append("<Cell imagePath=\"" + imageFullPath + "\"  Attribute_type=\"" + attributeType + "\"  TBGUID=\"TPS" + _randomClass.Next() + "\" product_ID=\"" + productID + "\" cellArea=\"SummaryHeader\"  EmbedImage=\"\"  cellStyle=\"" + cellName + "\" table=\"cell\" theader=\"\" crows=\" 1 \" ccols=\"1\">");
                                    strBldr.Append("<Cell Attribute_type=\"" + attributeType + "\"  cellStyle=\"image_name\"  COTYPE=\"IMAGE\"  TBGUID=\"CI" + _randomClass.Next() + "\"  IMAGE_FILE=\"" + _pivotTable.Rows[i][j].ToString().Replace("&", "&amp;") + "\"></Cell>");
                                    strBldr.Append("</Cell>");
                                }
                            }
                        }
                    }
                    strBldr.Append("</row>");
                    if (objGroupedSummaryAttrs.Values.Count(a => a != "None") - 1 > tempGrouAttrCount)
                    {
                        tempGrouAttrCount++;
                    }
                    else
                    {
                        tempGrouAttrCount = 0;
                    }
                }

                strBldr.Append("</table>");
                strBldr.Append("</products>");

            }

            return strBldr.ToString();

        }

        private int GetVerticalMergeCells(int colIndex, int rowIndex, int headercount, bool check, int verhorz)
        {
            int mergeCount = 0;
            if (verhorz == 1)
            {
                if (check)
                {
                    for (int i = rowIndex; i < _pivotTable.Rows.Count; i++)
                    {
                        if (i <= headercount - 1)
                        {
                            if (_pivotTable.Rows[i][colIndex].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                            { mergeCount++; }
                            else
                            {
                                break;
                            }
                        }
                        else if (rowIndex >= headercount)
                        {
                            if (_pivotTable.Rows[i][colIndex].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                            { mergeCount++; }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = rowIndex; i < _pivotTable.Rows.Count; i++)
                    {
                        if (i <= headercount - 1)
                        {
                            if (_pivotTable.Rows[i][colIndex].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                            { mergeCount++; }
                            else
                            {
                                break;
                            }
                        }
                        else //if rowIndex >= headercount)
                        {
                            if (_pivotTable.Rows[i][colIndex].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                            { mergeCount++; }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                if (check)
                {
                    for (int i = rowIndex; i < _pivotTable.Rows.Count; i++)
                    {
                        if (i <= headercount - 1)
                        {
                            if (_pivotTable.Rows[i][colIndex].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                            {
                                mergeCount++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if (_pivotTable.Rows[i][colIndex].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                            {
                                mergeCount++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = rowIndex; i < _pivotTable.Rows.Count; i++)
                    {
                        if (i <= headercount - 1)
                        {
                            if (_pivotTable.Rows[i][colIndex].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                            { mergeCount++; }
                            else
                            {
                                break;
                            }
                        }
                        else if (rowIndex >= headercount)
                        {
                            if (_pivotTable.Rows[i][colIndex].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                            { mergeCount++; }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return mergeCount;
        }

        private int GetHorizontalMergeCells(int colIndex, int rowIndex)
        {
            int mergeCount = 0;
            for (int j = colIndex; j < _pivotTable.Columns.Count; j++)
            {
                if (_pivotTable.Rows[rowIndex][j].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                {
                    if (rowIndex == 0)
                    { mergeCount++; }
                    else if ((_pivotTable.Rows[rowIndex - 1][j].ToString() == _pivotTable.Rows[rowIndex - 1][colIndex].ToString()))
                    {
                        mergeCount++;
                    }
                }
                else
                {
                    break;
                }
            }
            return mergeCount;
        }
        private int GetHorizontalMergeCellsForGroupRow(int colIndex, int rowIndex)
        {
            int mergeCount = 0;
            for (int j = colIndex; j < _pivotTable.Columns.Count; j++)
            {
                if (_pivotTable.Rows[rowIndex][j].ToString() == _pivotTable.Rows[rowIndex][colIndex].ToString())
                {
                    mergeCount++;
                }
                else
                {
                    break;
                }
            }
            return mergeCount;
        }
        private string ConvertImage(string imagePathValue)
        {
            const int valInc = 0;
            // SystemSettingsCollection settingMembers = SystemSettingsConfiguration.GetConfig.Members;
            string imageUrl = "";
            string imageSizeW = "";
            string imageSizeH = "";
            string returnstr = "";
            string imageType = "";
            imagePathValue = imagePathValue.Replace("..", "");
            string tempimagePathValue = "..\\content\\productimages\\" + imagePathValue;
            string _applicationStartupPath = "..\\content";

            imagePathValue = HttpContext.Current.Server.MapPath("~/Content/productimages/") + imagePathValue.Replace("\"", "");

            //imagePathValue =  imagePathValue.Replace("\"", "");
            //if (System.IO.File.Exists( HttpContext.Current.Server.MapPath("~/Content/productimages") + imagePathValue))
            //{
            //    imagePathValue = "/Images/unsupportedImageformat.jpg";
            //}
            if (!imagePathValue.Contains("&"))
            {
                var chkFile = new FileInfo(imagePathValue);
                if (chkFile.Exists)
                {
                    imageType = "";
                    if (chkFile.Extension.ToUpper() == ".EPS" ||
                        chkFile.Extension.ToUpper() == ".SVG" ||
                        //chkFile.Extension.ToUpper() == ".TIF" ||
                        //chkFile.Extension.ToUpper() == ".TIFF" ||
                        chkFile.Extension.ToUpper() == ".PSD" ||
                        chkFile.Extension.ToUpper() == ".TGA" ||
                        chkFile.Extension.ToUpper() == ".PCX") // ||
                    //chkFile.Extension.ToUpper() == ".PNG")
                    {
                        if (Directory.Exists(ImageMagickPath))
                        {
                            string[] imgargs = { imagePathValue, _applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg" };
                            //string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_Id).Select(a => a.ConverterType).SingleOrDefault();
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                //var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                                if (File.Exists(imgargs[1].Replace("&", "")))
                                {
                                    File.Delete(imgargs[1].Replace("&", ""));
                                }
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }

                        var chkFileVal = new FileInfo(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                        if (chkFileVal.Exists)
                        {
                            //htmlData.Append("<br /><br /><IMG src = \"" + chkFileVal.FullName.ToString() + "\" height = 200pts width = 200pts />");
                            Image imm = Image.FromFile(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                            imm.Save(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                            imm = Image.FromFile(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                            imm.Save(_applicationStartupPath + "\\temp\\" + valInc.ToString(CultureInfo.InvariantCulture) + "ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                            imm.Dispose();
                            var asd = new FileInfo(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                            asd.Delete();
                            //////////
                            if (_chkSize != null) { _chkSize.Dispose(); }
                            _chkSize = Image.FromFile(_applicationStartupPath + "\\temp\\" + valInc.ToString(CultureInfo.InvariantCulture) + "ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                            Double iHeight = _chkSize.Height;
                            Double iWidth = _chkSize.Width;
                            Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                            imageSizeH = Convert.ToString(newVal.Height);
                            imageSizeW = Convert.ToString(newVal.Width);
                            if (_chkSize != null)
                            {
                                _chkSize.Dispose();
                            }
                            imageUrl = _applicationStartupPath + "\\temp\\" + valInc.ToString(CultureInfo.InvariantCulture) + "ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg";// chkFileVal.FullName.ToString();
                            imageType = "IMAGE";
                        }

                        else
                        {
                            chkFileVal = new FileInfo(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + "-1.jpg");
                            if (chkFileVal.Exists)
                            {
                                //htmlData.Append("<br /><br /><IMG src = \"" + chkFileVal.FullName.ToString() + "\" height = 200pts width = 200pts />");
                                Image imm = Image.FromFile(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + "-1.jpg");
                                imm.Save(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                imm = Image.FromFile(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                                imm.Save(_applicationStartupPath + "\\temp\\" + valInc.ToString(CultureInfo.InvariantCulture) + "ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                imm.Dispose();
                                var asd = new FileInfo(_applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                                asd.Delete();
                                //////////
                                if (_chkSize != null) { _chkSize.Dispose(); }
                                _chkSize = Image.FromFile(_applicationStartupPath + "\\temp\\" + valInc.ToString(CultureInfo.InvariantCulture) + "ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                                Double iHeight = _chkSize.Height;
                                Double iWidth = _chkSize.Width;
                                Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                imageSizeH = Convert.ToString(newVal.Height);
                                imageSizeW = Convert.ToString(newVal.Width);
                                if (_chkSize != null)
                                {
                                    _chkSize.Dispose();
                                }
                                imageUrl = _applicationStartupPath + "\\temp\\" + valInc.ToString(CultureInfo.InvariantCulture) + "ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg";// chkFileVal.FullName.ToString();
                                imageType = "IMAGE";
                            }
                        }
                    }
                    else if (chkFile.Extension.ToUpper() == ".JPG" ||
                             chkFile.Extension.ToUpper() == ".JPEG" ||
                             chkFile.Extension.ToUpper() == ".GIF" ||
                             chkFile.Extension.ToUpper() == ".BMP" ||
                             chkFile.Extension.ToUpper() == ".ICO" ||
                             chkFile.Extension.ToUpper() == ".TIF" ||
                             chkFile.Extension.ToUpper() == ".TIFF" ||
                             chkFile.Extension.ToUpper() == ".PNG")
                    {
                        // htmlData.Append("<br /><br /><IMG src = \"" + ("C:") + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[8].ToString().Trim() + "\" height = 200pts width = 200pts />");
                        if (_chkSize != null) { _chkSize.Dispose(); }
                        try
                        {
                            _chkSize = Image.FromFile(imagePathValue);

                            Double iHeight = _chkSize.Height;
                            Double iWidth = _chkSize.Width;
                            Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                            imageSizeH = Convert.ToString(newVal.Height);
                            imageSizeW = Convert.ToString(newVal.Width);
                            if (_chkSize != null)
                            {
                                _chkSize.Dispose();
                            }
                            imageUrl = imagePathValue.Trim();
                        }
                        catch (Exception)
                        {
                            //imageUrl = (_applicationStartupPath + "\\images\\unsupportedImageformat.jpg");
                            imageUrl = "";
                            imageSizeH = "72";
                            imageSizeW = "72";
                        }
                        imageType = "IMAGE";
                    }
                    else if (chkFile.Extension.ToUpper() == ".AVI" ||
                             chkFile.Extension.ToUpper() == ".WMV" ||
                             chkFile.Extension.ToUpper() == ".MPG" ||
                             chkFile.Extension.ToUpper() == ".SWF")
                    {
                        imageUrl = imagePathValue.Trim();
                        imageType = "MEDIA";
                    }
                }
                else if (imagePathValue == "")
                {
                    // returnstr = ("<IMG SRC = \"" + _applicationStartupPath + "\\images\\unsupportedImageformat.jpg " + "\" height = 82px>");
                    returnstr = "";
                }

                if (imageType == "IMAGE")
                {

                    returnstr = "<IMG SRC = \"" + tempimagePathValue + "\" HEIGHT=" + imageSizeH + " WIDTH = " + imageSizeW + ">";

                }
                else if (imageType == "MEDIA")
                {
                    returnstr = returnstr + ("<EMBED SRC= \"" + tempimagePathValue + "\" height = 150pts width = 150pts AUTOPLAY=\"false\" CONTROLLER=\"true\"/>");
                }
                else if (imageType == "" && imagePathValue == "")  // cr1
                {
                    //returnstr = ("<IMG SRC = \"" + _applicationStartupPath + "\\images\\unsupportedImageformat.jpg " + "\" height = 82px>");
                    returnstr = "";
                }
            }
            else
            {
                var chkFile = new FileInfo(imagePathValue);
                if (chkFile.Exists)
                {
                    if (Directory.Exists(ImageMagickPath))
                    {
                        string[] imgargs = { imagePathValue, _applicationStartupPath + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg" };
                        if (converttype == "Image Magic")
                        {
                            Exefile = "convert.exe";
                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                            //var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                            if (File.Exists(imgargs[1].Replace("&", "")))
                            {
                                File.Delete(imgargs[1].Replace("&", ""));
                            }
                            pStart.CreateNoWindow = true;
                            pStart.UseShellExecute = false;
                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                            while (!cmdProcess.HasExited)
                            {
                            }
                        }
                        else
                        {
                            Exefile = "cons_rcp.exe";
                            // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");
                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1].Replace("&", "") + "\"");
                            if (File.Exists(imgargs[1].Replace("&", "")))
                            {
                                File.Delete(imgargs[1].Replace("&", ""));
                            }
                            pStart.CreateNoWindow = true;
                            pStart.UseShellExecute = false;
                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                            while (!cmdProcess.HasExited)
                            {
                            }
                        }
                        returnstr = "<IMG SRC =\" " + imgargs[1].Replace("&", "") + "\"  HEIGHT=200 WIDTH = 200>";
                    }
                }
            }
            if (returnstr == "" || returnstr == null)
            {
                //returnstr = ("<IMG SRC = \"" + _applicationStartupPath + "\\images\\unsupportedImageformat.jpg " + "\" height = 82px>");
                returnstr = "";
            }
            return (returnstr);
            // return null;
        }
        private Size ScaleImage(double origHeight, double origWidth, double width, double height)
        {
            var newSize = new Size();
            double nWidth = width;
            double nHeight = height;
            double oWidth = origWidth;
            double oHeight = origHeight;

            if (origHeight > 200 || origWidth > 200)
            {
                if (oWidth > oHeight)
                {
                    double ratio = oHeight / oWidth;
                    double final = (nWidth) * ratio;
                    nHeight = (int)final;
                }
                else
                {
                    double ratio = oWidth / oHeight;
                    double final = (nHeight) * ratio;
                    nWidth = (int)final;
                }
                newSize.Height = (int)nHeight;
                newSize.Width = (int)nWidth;
            }
            else
            {
                newSize.Height = (int)origHeight;
                newSize.Width = (int)origWidth;
            }

            return newSize;
        }
        private void DecodeRowAttr(XmlNode rowNode)
        {
            for (int i = 0; i < rowNode.ChildNodes.Count; i++)
            {
                if (rowNode.ChildNodes[i].Attributes != null)
                {
                    if (rowNode.ChildNodes[i].Attributes["AttrID"].Value == "Super")
                    {
                        DecodeRowAttr(rowNode.ChildNodes[i]);
                    }
                    else
                    {
                        if (rowNode.ChildNodes[i].Name == "RightRowField")
                        {
                            if (rowNode.ChildNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                            {
                                _mRightRowFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                _mRowFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                _mRightRowNodes.Add(rowNode.ChildNodes[i].Clone());
                            }
                            else
                            {
                                _mRightRowFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                _mRowFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                _mRightRowNodes.Add(rowNode.ChildNodes[i].Clone());
                            }
                        }
                        else if (rowNode.ChildNodes[i].Name == "SummaryField")
                        {
                            if (rowNode.ChildNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                            {
                                _mSummaryFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                objGroupedSummaryAttrs.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 5)), rowNode.Attributes["AttrName"].Value);
                            }
                            else
                            {
                                _mSummaryFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                objGroupedSummaryAttrs.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 5)), rowNode.Attributes["AttrName"].Value);
                            }
                            _mSummaryNodes.Add(rowNode.ChildNodes[i].Clone());
                        }
                        else
                        {
                            if (rowNode.ChildNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                            {
                                _mLeftRowFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                _mRowFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 6)));
                                _mLeftRowNodes.Add(rowNode.ChildNodes[i].Clone());
                            }
                            else
                            {
                                _mLeftRowFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                _mRowFields.Add(
                                    GetAttributeNameFromAttributeID(
                                        rowNode.ChildNodes[i].Attributes["AttrID"].Value.Remove(0, 5)));
                                _mLeftRowNodes.Add(rowNode.ChildNodes[i].Clone());
                            }
                        }
                    }
                }
            }
        }
        private DataTable ProductFilter(DataSet flatDataset, int tableIndex, bool removeProductColumn)
        {
            var sqLstring = new StringBuilder();


            string sqlString = " SELECT PRODUCT_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + _mCatalogID + " ";

            SQLString = sqlString;
            DataSet oDsProductFilter = CreateDataSet();


            if (oDsProductFilter.Tables[0].Rows.Count > 0 && oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
            {
                string sProductFilter = oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(sProductFilter);
                XmlNode rNode = xmlDOc.DocumentElement;

                if (rNode != null && rNode.ChildNodes.Count > 0)
                {
                    for (int i = 0; i < rNode.ChildNodes.Count; i++)
                    {
                        XmlNode tableDataSetNode = rNode.ChildNodes[i];

                        if (tableDataSetNode.HasChildNodes)
                        {
                            if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[2].InnerText = "=";
                            }
                            if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[0].InnerText = "0";
                            }
                            string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                            sqlString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE  ATTRIBUTE_ID = " + Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText) + " ";
                            SQLString = sqlString;
                            DataSet attribuetypeDs = CreateDataSet();

                            if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("TEX") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DATE"))
                            {

                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _mCatalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _mCatalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                }
                            }
                            else if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DECI") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("NUM"))
                            {
                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _mCatalogID + ") WHERE  (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _mCatalogID + ") WHERE (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                }
                            }
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                        {

                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                        {
                            sqLstring.Append(" INTERSECT \n");
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                        {
                            if (i == rNode.ChildNodes.Count - 1)
                            {

                            }
                            else
                            {
                                sqLstring.Append(" UNION \n");
                            }
                        }
                    }
                }
            }
            string productFiltersql = sqLstring.ToString();
            // Boolean variableFilter = false;
            if (productFiltersql.Length > 0)
            {
                string s = "SELECT DISTINCT PRODUCT_ID FROM [PRODUCT FAMILY](" + _mCatalogID + ") WHERE PRODUCT_ID IN\n" +
                      "(\n";// +
                //"SELECT DISTINCT PRODUCT_ID\n" +
                //"FROM [PRODUCT SPECIFICATION](" + m_catalogID + ")\n" +
                //"WHERE\n";
                productFiltersql = s + productFiltersql + "\n)";

                sqlString = productFiltersql;
                SQLString = sqlString;
                oDsProductFilter = CreateDataSet();


                var dataDs = new DataSet();

                DataTable table = flatDataset.Tables[tableIndex].Clone();
                dataDs.Tables.Add(table);
                foreach (DataRow odr in flatDataset.Tables[tableIndex].Rows)
                {
                    foreach (DataRow dr in oDsProductFilter.Tables[0].Rows)
                    {
                        try
                        {
                            if (dr["PRODUCT_ID"].ToString() == odr["PRODUCT_ID"].ToString())
                            {
                                DataRow[] productRow = table.Select("PRODUCT_ID=" + odr["PRODUCT_ID"]);
                                if (productRow.Length == 0)
                                    table.ImportRow(odr);
                            }
                        }
                        catch (Exception) { return flatDataset.Tables[tableIndex]; }
                    }

                }
                flatDataset.Tables.Clear();
                flatDataset = dataDs.Copy();
                if (removeProductColumn)
                {
                    // flatDataset.Tables[0].Columns.Remove("PRODUCT_ID");
                }
                return flatDataset.Tables[tableIndex];

            }
            if (removeProductColumn)
            {
                // flatDataset.Tables[tableIndex].Columns.Remove("PRODUCT_ID");
            }
            return flatDataset.Tables[tableIndex];

        }
        public DataSet CatalogProductDetailsX(int catalogID, int familyID, int[] attributeList, bool mainFamily, string connvalue)
        {
            //var asdf = new Connection();
            //asdf.ConnSettings(connvalue);
            var asdf =
                new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            //  ProductCatalog(catalogID, familyID, true, true, attributeList, true, "");
            DataSet ds = GetProductCatalogX(mainFamily);
            ds.Tables.Add(ExtendedProperties(catalogID, familyID));
            ds.Tables[0].Columns.Remove("FAMILYID");
            ds.Tables[1].Columns.Remove("FAMILYID");
            return ds;
        }
        private DataTable ExtendedProperties(int catalogID, int familyID)
        {
            var et = new DataTable("ExtendedProperties");
            var iAttrList = new int[0];

            //for (int i = 0; i < iAttrList.Length - 1; i++)
            //{
            //    //iAttrList[i] = (int) grdSelect.Rows[i].Cells["ATTRIBUTE_ID"].Value;
            //}
            //var csPt = new CSDBProviderEX.ProductTable(catalogID, familyID, iAttrList, "");
            DataTable eet = GetProductTableExProperties(true);
            foreach (DataColumn dc in eet.Columns)
                et.Columns.Add(dc.Caption, dc.DataType);
            foreach (DataRow dr in eet.Rows)
                et.ImportRow(dr);
            return et;
        }
        public DataTable GetProductTableExProperties(bool mainFamily)
        {
            var tblprod = new DataTable("tblprod");
            var tblcpa = new DataTable("tblcpa");
            var taprod = new DataTable("taprod");

            //var taprod = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter();
            //var tblprod = new CSDBProvider.CSDS.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESDataTable();
            //var tblcpa = new CSDBProvider.CSDS.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTDataTable();

            //taprod.FillByCatalogFamilyExProperties(tblprod, _mCatalogID, _mFamilyID);
            string cmdstr1 = "SELECT DISTINCT CAST(TB_ATTRIBUTE.ATTRIBUTE_ID AS nvarchar(MAX)) AS STRING_VALUE, - 3 AS PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID as ATTRIBUTE_ID, TB_PARTS_KEY.ATTRIBUTE_ID AS NUMERIC_VALUE, null as OBJECT_TYPE, null as OBJECT_NAME, " +
                         " TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN                          TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID   AND TB_PARTS_KEY.FAMILY_ID = TB_PROD_FAMILY.FAMILY_ID INNER JOIN TB_ATTRIBUTE ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") " +
                         " UNION SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_ID AS nvarchar(MAX)) AS STRING_VALUE, - 3 AS PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID as ATTRIBUTE_ID,  TB_ATTRIBUTE.ATTRIBUTE_ID AS NUMERIC_VALUE,  null as OBJECT_TYPE, null as OBJECT_NAME,  TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN                          TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN " +
                         " TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") UNION SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_TYPE AS nvarchar(MAX)) AS STRING_VALUE, - 2 AS PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID as ATTRIBUTE_ID,  TB_ATTRIBUTE.ATTRIBUTE_TYPE AS NUMERIC_VALUE,  null as OBJECT_TYPE, null as OBJECT_NAME,  TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN " +
                         " TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID  WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") UNION SELECT DISTINCT  TB_ATTRIBUTE.STYLE_NAME AS STRING_VALUE, - 1 AS PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID  as ATTRIBUTE_ID,  null  AS NUMERIC_VALUE,  null as OBJECT_TYPE, null as OBJECT_NAME, TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN " +
                         " TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") union SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_TYPE AS nvarchar(MAX)) AS STRING_VALUE, - 2 AS PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID as ATTRIBUTE_ID,  TB_ATTRIBUTE.ATTRIBUTE_TYPE AS NUMERIC_VALUE, null as OBJECT_TYPE, null as OBJECT_NAME,  TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID " +
                         " FROM            TB_PROD_FAMILY INNER JOIN  TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID   AND TB_PARTS_KEY.FAMILY_ID = TB_PROD_FAMILY.FAMILY_ID INNER JOIN TB_ATTRIBUTE ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") UNION SELECT DISTINCT  TB_ATTRIBUTE.STYLE_NAME AS STRING_VALUE, - 1 AS PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID  as ATTRIBUTE_ID,  null  AS NUMERIC_VALUE, null as OBJECT_TYPE, null as OBJECT_NAME, " +
                         " TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN                          TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID   AND TB_PARTS_KEY.FAMILY_ID = TB_PROD_FAMILY.FAMILY_ID INNER JOIN TB_ATTRIBUTE ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <>  " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") ORDER BY PRODUCT_ID, ATTRIBUTE_ID ";
            var Ocon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            SqlCommand sqlcmd1 = new SqlCommand(cmdstr1, Ocon1);
            sqlcmd1.CommandTimeout = 0;
            SqlDataAdapter _DBAdapter1 = new SqlDataAdapter(sqlcmd1);
            _DBAdapter1.Fill(taprod);
            _DBAdapter1.Fill(tblprod);
            string cmdstr = "SELECT 0 AS ATTRIBUTE_ID, 'Sort' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 5 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT - 1 AS ATTRIBUTE_ID, 'Publish' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 4 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -2 AS ATTRIBUTE_ID, 'Publish2Print' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, -3  AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -3 AS ATTRIBUTE_ID, 'Publish2CD' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 2 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                         " UNION " +
                            " SELECT -4 AS ATTRIBUTE_ID, 'WorkFlowStat' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 1 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                         "UNION " +
                         " SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_PROD_FAMILY_ATTR_LIST.SORT_ORDER, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE " +
                          "FROM TB_PROD_FAMILY INNER JOIN " +
                        "TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN " +
                        "TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID INNER JOIN " +
                        "TB_PROD_FAMILY_ATTR_LIST ON  " +
                        "TB_PROD_FAMILY.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST.FAMILY_ID AND " +
                        "TB_PROD_SPECS.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST.ATTRIBUTE_ID INNER JOIN " +
                        "TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND " +
                        "TB_CATALOG_ATTRIBUTES.CATALOG_ID =" + _mCatalogID + "  WHERE        (- 1 <> 2) AND (TB_PROD_FAMILY.FAMILY_ID =" + _mFamilyID + ") " +
                         "UNION " +
                       "SELECT DISTINCT " +
                        "TB_ATTRIBUTE_5.ATTRIBUTE_ID, TB_ATTRIBUTE_5.ATTRIBUTE_NAME, TB_ATTRIBUTE_5.ATTRIBUTE_TYPE,  " +
                        "TB_PROD_FAMILY_ATTR_LIST_3.SORT_ORDER, TB_ATTRIBUTE_5.ATTRIBUTE_DATATYPE " +
                          "FROM            TB_PROD_FAMILY AS TB_PROD_FAMILY_5 INNER JOIN " +
                        "TB_PARTS_KEY ON TB_PROD_FAMILY_5.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN " +
                        "TB_ATTRIBUTE AS TB_ATTRIBUTE_5 ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID INNER JOIN TB_PROD_FAMILY_ATTR_LIST AS TB_PROD_FAMILY_ATTR_LIST_3 ON " +
                       
                        "TB_PROD_FAMILY_5.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST_3.FAMILY_ID AND " +
                        "TB_PARTS_KEY.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST_3.ATTRIBUTE_ID INNER JOIN " +
                        "TB_CATALOG_ATTRIBUTES AS TB_CATALOG_ATTRIBUTES_5 ON TB_CATALOG_ATTRIBUTES_5.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID AND " +
                        "TB_CATALOG_ATTRIBUTES_5.CATALOG_ID =" + _mCatalogID + "" +
"  WHERE        (TB_PROD_FAMILY_5.FAMILY_ID =" + _mFamilyID + ") AND (- 1 <> 2) " +
"UNION " +
"select ATTRIBUTE_ID,ATTRIBUTE_NAME,ATTRIBUTE_TYPE,9999 as sort_order,ATTRIBUTE_DATATYPE from TB_ATTRIBUTE where " +
"ATTRIBUTE_ID in(select  CA.ATTRIBUTE_ID from TB_PROD_SPECS PS JOIN TB_CATALOG_ATTRIBUTES CA ON PS.ATTRIBUTE_ID=CA.ATTRIBUTE_ID where PRODUCT_ID in(select PRODUCT_ID from TB_PROD_FAMILY where FAMILY_ID=" + _mFamilyID + " ) AND CATALOG_ID=" + _mCatalogID + " UNION " +
                           "SELECT DISTINCT CA.ATTRIBUTE_ID " +
                           "FROM  TB_PARTS_KEY AS TB_PARTS_KEY_1 JOIN TB_CATALOG_ATTRIBUTES CA ON TB_PARTS_KEY_1.ATTRIBUTE_ID=CA.ATTRIBUTE_ID " +
                           "WHERE (FAMILY_ID =" + _mFamilyID + ") AND TB_PARTS_KEY_1.CATALOG_ID=" + _mCatalogID + " ) " +
                           "AND (ATTRIBUTE_ID NOT IN " +
                           "(SELECT DISTINCT ATTRIBUTE_ID " +
                           "FROM TB_PROD_FAMILY_ATTR_LIST AS TB_PROD_FAMILY_ATTR_LIST_1 " +
                           "WHERE (FAMILY_ID =" + _mFamilyID + "))) ORDER BY SORT_ORDER ";
            var Ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            SqlCommand sqlcmd = new SqlCommand(cmdstr, Ocon);
            sqlcmd.CommandTimeout = 0;
            SqlDataAdapter _DBAdapter = new SqlDataAdapter(sqlcmd);
            _DBAdapter.Fill(tblcpa);
            //tacpa.FillByCatalogFamily(tblcpa, _CatalogId, _FamilyId);   
            // tacpa.FillByCatalogFamily(tblcpa, _CatalogId, _FamilyId);

            DataTable dt = GetTransposedProductTable(tblprod, tblcpa);

            dt.Columns.Add("FAMILYID", System.Type.GetType("System.Int32"));

            return dt;
        }


        private string ConstructRowFields(DataTable attrTable)
        {
            string temp = string.Empty;

            //Populate RowList with AttributeNames
            List<string> _mRowFields1 = _mRowFields.Distinct().ToList();
            foreach (string t in _mRowFields1)
            {
                int status = 0;
                for (int j = 0; j < attrTable.Columns.Count; j++)
                {
                    if (attrTable.Columns[j].Caption == t)
                    { status = 1; }
                }
                if (status == 1)
                {
                    if (temp.Length > 0)
                    {
                        temp = temp + "`" + t;
                    }
                    else
                    {
                        temp = t;
                    }
                }
            }
            if (_mTableGroupField.Length > 0)
            {
                int status = 0;
                for (int j = 0; j < attrTable.Columns.Count; j++)
                {
                    if (attrTable.Columns[j].Caption == _mTableGroupField)
                    { status = 1; }
                }
                if (status == 1)
                {
                    if (temp.Length > 0)
                    {
                        temp = temp + "`" + _mTableGroupField;
                    }
                    else
                    {
                        temp = _mTableGroupField;
                    }
                }
            }
            return temp;
        }
        private string ConstructColumnFields(DataTable attrTable)
        {
            string temp = string.Empty;
            //Populate ColumnList with AttributeNames
            foreach (string t in _mColumnFields)
            {
                int status = 0;
                for (int j = 0; j < attrTable.Columns.Count; j++)
                {
                    if (attrTable.Columns[j].Caption == t)
                    { status = 1; }
                }
                if (status == 1)
                {
                    if (temp.Length > 0)
                    {
                        temp = temp + "`" + t;
                    }
                    else
                    {
                        temp = t;
                    }
                }
            }
            return temp;
        }

        public DataSet GetProductCatalogX(bool mainFamily)
        {
            var dsPf = new DataSet { EnforceConstraints = false };
            if (_loadXProductTable)
            {
                //Load product table
                _attributeIdList = new int[] { };

                DataTable dtProductTable = GetProductTable(mainFamily);
                dtProductTable.TableName = "ProductTable";
                dsPf.Tables.Add(dtProductTable);
            }
            return dsPf;
        }
        public DataTable GetProductTable(bool mainFamily)
        {
            var dt = new DataTable("Product");
            var dt2 = new DataTable("Product2");
            var tblprod = new DataTable("Product2");
            var tblcpa = new DataTable("Product2");
            // TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter taprod = new TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter();
            //  TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESDataTable tblprod = new TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESDataTable();


            //  TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTTableAdapter tacpa = new TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTTableAdapter();
            // TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTDataTable tblcpa = new TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTDataTable();
            string catfamquery = "EXEC STP_CATALOGSTUDIO5_FILLBYCATALOG_FAMILIY " + _mFamilyID + "," + _mCatalogID + "," + "'" + CategoryId + "'";
            // var conn = new TradingBell.CatalogX.CSDBProvider.Connection();
            //var ocon = new SqlConnection(conn.GetConnSettings());
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            var ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            var comm = new SqlCommand(catfamquery, conn);
            comm.CommandTimeout = 0;
            var dbAdapter = new SqlDataAdapter(comm);
            dbAdapter.Fill(tblprod);
            //taprod.FillByCatalogFamily(tblprod, _FamilyId, _CatalogId);
            string cmdstr = "SELECT 0 AS ATTRIBUTE_ID, 'Sort' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 5 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT - 1 AS ATTRIBUTE_ID, 'Publish' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 4 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -2 AS ATTRIBUTE_ID, 'Publish2Print' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, -3  AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -3 AS ATTRIBUTE_ID, 'Publish2CD' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 2 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -4 AS ATTRIBUTE_ID, 'WorkFlowStat' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, -1  AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                         " SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_PROD_FAMILY_ATTR_LIST.SORT_ORDER, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE " +
                          "FROM TB_PROD_FAMILY INNER JOIN " +
                        "TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN " +
                        "TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID INNER JOIN " +
                        "TB_PROD_FAMILY_ATTR_LIST ON " +
                        "TB_PROD_FAMILY.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST.FAMILY_ID AND " +
                        "TB_PROD_SPECS.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST.ATTRIBUTE_ID INNER JOIN " +
                        "TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND " +
                        "TB_CATALOG_ATTRIBUTES.CATALOG_ID =" + _mCatalogID + "  WHERE        (- 1 <> 2) AND (TB_PROD_FAMILY.FAMILY_ID =" + _mFamilyID + ") " +
                         "UNION " +
                       "SELECT DISTINCT " +
                        "TB_ATTRIBUTE_5.ATTRIBUTE_ID, TB_ATTRIBUTE_5.ATTRIBUTE_NAME, TB_ATTRIBUTE_5.ATTRIBUTE_TYPE,  " +
                        "TB_PROD_FAMILY_ATTR_LIST_3.SORT_ORDER, TB_ATTRIBUTE_5.ATTRIBUTE_DATATYPE " +
                          "FROM            TB_PROD_FAMILY AS TB_PROD_FAMILY_5 INNER JOIN " +
                        "TB_PARTS_KEY ON TB_PROD_FAMILY_5.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN " +
                        "TB_ATTRIBUTE AS TB_ATTRIBUTE_5 ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID AND TB_PARTS_KEY.FAMILY_ID=" + _mFamilyID + " INNER JOIN TB_PROD_FAMILY_ATTR_LIST AS TB_PROD_FAMILY_ATTR_LIST_3 ON " +
               "TB_PROD_FAMILY_5.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST_3.FAMILY_ID AND " +
                        "TB_PARTS_KEY.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST_3.ATTRIBUTE_ID INNER JOIN " +
                        "TB_CATALOG_ATTRIBUTES AS TB_CATALOG_ATTRIBUTES_5 ON TB_CATALOG_ATTRIBUTES_5.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID AND " +
                        "TB_CATALOG_ATTRIBUTES_5.CATALOG_ID =" + _mCatalogID + "" +
"  WHERE        (TB_PROD_FAMILY_5.FAMILY_ID =" + _mFamilyID + ") AND (- 1 <> 2) " +
"UNION " +
"select ATTRIBUTE_ID,ATTRIBUTE_NAME,ATTRIBUTE_TYPE,9999 as sort_order,ATTRIBUTE_DATATYPE from TB_ATTRIBUTE where " +
"ATTRIBUTE_ID in(select  CA.ATTRIBUTE_ID from TB_PROD_SPECS PS JOIN TB_CATALOG_ATTRIBUTES CA ON PS.ATTRIBUTE_ID=CA.ATTRIBUTE_ID where PRODUCT_ID in(select PRODUCT_ID from TB_PROD_FAMILY where FAMILY_ID=" + _mFamilyID + " ) AND CATALOG_ID=" + _mCatalogID + " UNION " +
                           "SELECT DISTINCT CA.ATTRIBUTE_ID " +
                           "FROM  TB_PARTS_KEY AS TB_PARTS_KEY_1 JOIN TB_CATALOG_ATTRIBUTES CA ON TB_PARTS_KEY_1.ATTRIBUTE_ID=CA.ATTRIBUTE_ID " +
                           "WHERE (FAMILY_ID =" + _mFamilyID + ") AND TB_PARTS_KEY_1.CATALOG_ID=" + _mCatalogID + " ) " +
                           "AND (ATTRIBUTE_ID NOT IN " +
                           "(SELECT DISTINCT ATTRIBUTE_ID " +
                           "FROM TB_PROD_FAMILY_ATTR_LIST AS TB_PROD_FAMILY_ATTR_LIST_1 " +
                           "WHERE (FAMILY_ID =" + _mFamilyID + "))) ORDER BY SORT_ORDER ";
            //CSDBProvider.Connection conn = new TradingBell.CatalogX.CSDBProvider.Connection();
            //SqlConnection Ocon = new SqlConnection(conn.getConnSettings());

            var sqlcmd = new SqlCommand(cmdstr, ocon);
            sqlcmd.CommandTimeout = 0;
            var dbAdapter1 = new SqlDataAdapter(sqlcmd);
            dbAdapter1.Fill(tblcpa);
            //tacpa.FillByCatalogFamily(tblcpa, _CatalogId, _FamilyId);  
            #region "changes for time format changing "

            #endregion
            dt = GetTransposedProductTable(tblprod, tblcpa);

            dt.Columns.Add("FAMILYID", typeof(int));

            foreach (DataRow dr in dt.Rows)
            {
                dr["FAMILYID"] = dr["FAMILY_ID"];
            }
            return dt;
        }
        private DataTable GetTransposedProductTable(DataTable productInfo, DataTable productAttributes)
        {
            try
            {
                var dtnew = new DataTable();
                dtnew.Columns.Add("CATALOG_ID", typeof(int));
                dtnew.Columns.Add("FAMILY_ID", typeof(int));
                dtnew.Columns.Add("PRODUCT_ID", typeof(int));
                if (PdfCatalog)
                    dtnew.Columns.Add("SORT_ORDER", typeof(int));

                var dcpks = new DataColumn[3];
                if (PdfCatalog)
                {
                    dcpks = new DataColumn[4];
                    dcpks[0] = dtnew.Columns["CATALOG_ID"];
                    dcpks[1] = dtnew.Columns["FAMILY_ID"];
                    dcpks[2] = dtnew.Columns["PRODUCT_ID"];
                    dcpks[3] = dtnew.Columns["SORT_ORDER"];
                }
                else
                {
                    dcpks[0] = dtnew.Columns["CATALOG_ID"];
                    dcpks[1] = dtnew.Columns["FAMILY_ID"];
                    dcpks[2] = dtnew.Columns["PRODUCT_ID"];
                }

                dtnew.Constraints.Add("PK", dcpks, true);
                foreach (DataRow dr in productAttributes.Rows)
                {

                    if (_attributeIdList.Length > 0)
                    {
                        bool exists = CheckExists(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));
                        if (exists)
                        {
                            //_AttributeIdList[iCtr]
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                            {
                                productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                            }
                            else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            {
                                productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString(), typeof(Double));
                            }
                            else
                            {
                                productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                            }


                            //enhance - Make sure to check if the column exists in this table

                            //OLD CODE------------
                            //DataRow[] drs = ProductInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"], "FAMILY_ID,SORT_ORDER");
                            //DataTable dtsel;
                            //dtsel = ProductInfo.Clone();
                            //for (int i = 0; i < drs.Length; i++)
                            //{
                            //    dtsel.ImportRow(drs[i]);
                            //}
                            //-------------------
                            DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"],
                                "FAMILY_ID,SORT_ORDER");
                            var dtsel = new DataTable();
                            if (drs.Any())
                            {
                                dtsel = drs.CopyToDataTable();
                            }

                            try
                            {
                                if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                    if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" &&
                                        dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                                    {
                                        bool stringValue =
                                            dtsel.Rows.Cast<DataRow>()
                                                .Any(dr1 => dr1["STRING_VALUE"].ToString().Length == 0);
                                        if (stringValue == false)
                                        {
                                            dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                            dtsel.Columns["STRING_VALUE"].ColumnName =
                                                dr["ATTRIBUTE_NAME"].ToString();
                                        }
                                    }
                            }
                            catch (Exception)
                            {
                            }
                            dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                            dtsel.Dispose();
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                            {
                                productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                            }
                            else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                            else
                                productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                        }
                    }
                    else
                    {
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                            dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                        else
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();




                        //enhance - Make sure to check if the column exists in this table
                        if (!dtnew.Columns.Contains(dr["ATTRIBUTE_NAME"].ToString()))
                        {
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                        }                       
                        DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"]);
                        DataTable dtsel;
                        dtsel = productInfo.Clone();
                        for (int i = 0; i < drs.Length; i++)
                        {
                            dtsel.ImportRow(drs[i]);
                            //    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                            //dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                            //    {
                            //        dtsel.Rows[i].ItemArray[7] = 1;// Convert.ToDecimal(dtsel.Rows[i].ItemArray[0]);
                            //    }
                        }
                        // int i = 0;
                        //foreach (DataRow dr2 in dtnew.Rows)
                        //{
                        //    foreach (DataRow dr1 in dtsel.Rows)
                        //    {
                        //        dr2[dr["ATTRIBUTE_NAME"].ToString()] = dr1["STRING_VALUE"].ToString();
                        //        break;
                        //    }
                        //}
                        try
                        {
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" &&
                                    dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                                {
                                    bool stringValue = false;
                                    foreach (DataRow dr1 in dtsel.Rows)
                                    {
                                        if (dr1["STRING_VALUE"].ToString().Length == 0)
                                        {
                                            stringValue = true;
                                            break;
                                        }
                                    }
                                    if (stringValue == false)
                                    {
                                        dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                        dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                    }
                                }
                        }
                        catch (Exception)
                        {
                        }
                        dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                        dtsel.Dispose();
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                            dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                        else
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";

                    }
                }
                return dtnew;
            }
            catch (Exception)
            {
                // MessageBox.Show(ex.Message);
                _groupTable = new DataTable();
                _sourceTable = new DataTable();
                _rowTable = new DataTable();
                _columnTable = new DataTable();
                _attributeTable = new DataTable();
                return null;
            }


        }

        private int GetLeftTreeDepth()
        {
            if (_leftRowNode == null) { return 1; }
            string[] strList = _leftRowNode.InnerXml.Split('<');
            return (from temp in strList.Select((t, i) => (string)strList.GetValue(i)) where temp.Contains("Level") select temp.Substring(temp.IndexOf("Level", StringComparison.Ordinal) + 7, 1) into val select Convert.ToInt32(val)).Concat(new[] { 1 }).Max();
        }

        private int GetRightTreeDepth()
        {
            if (_rightRowNode == null) { return 1; }
            string[] strList = _rightRowNode.InnerXml.Split('<');
            return (from temp in strList.Select((t, i) => (string)strList.GetValue(i)) where temp.Contains("Level") select temp.Substring(temp.IndexOf("Level", StringComparison.Ordinal) + 7, 1) into val select Convert.ToInt32(val)).Concat(new[] { 1 }).Max();
        }

        private string GetNthParent(XmlNode xNode, int level)
        {
            XmlNode node = null;
            for (int i = 0; i < _leftRowNode.ChildNodes.Count; i++)
            {
                if (_leftRowNode.ChildNodes[i].Attributes != null)
                {
                    if (xNode.Attributes != null &&
                        _leftRowNode.ChildNodes[i].Attributes["AttrID"].Value == xNode.Attributes["AttrID"].Value)
                    {
                        node = _leftRowNode.ChildNodes[i];
                        break;
                    }
                    if (_leftRowNode.ChildNodes[i].ChildNodes.Count != 0)
                    {
                        if (xNode.Attributes != null)
                            node = GetChildWithKey(_leftRowNode.ChildNodes[i], xNode.Attributes["AttrID"].Value);
                    }
                    if (node != null)
                    {
                        break;
                    }
                }
            }
            if (node == null || node.ParentNode == null)
                return "";

            string nodeName = string.Empty;


            int levelCount = 0;
            while (node.ParentNode != null)
            {
                levelCount = levelCount + 1;

                node = node.ParentNode;
                int datalevel = node.Attributes != null && node.Attributes["AttrID"] != null ? Convert.ToInt16(node.Attributes["Level"].Value) : 0;
                if (node.Attributes != null && node.Attributes["AttrName"] != null)
                {
                    nodeName = node.Attributes["AttrName"].Value;
                }
                if (datalevel == level)
                    return nodeName;
            }

            return nodeName;
        }
        private string GetNthParentFromRightNodeTree(XmlNode xNode, int level)
        {
            XmlNode node = null;

            for (int i = 0; i < _rightRowNode.ChildNodes.Count; i++)
            {
                if (_rightRowNode.ChildNodes[i].Attributes != null)
                {
                    if (xNode.Attributes != null &&
                        _rightRowNode.ChildNodes[i].Attributes["AttrID"].Value == xNode.Attributes["AttrID"].Value)
                    {
                        node = _rightRowNode.ChildNodes[i];
                        break;
                    }
                    if (xNode.Attributes != null)
                        node = GetChildWithKey(_rightRowNode.ChildNodes[i], xNode.Attributes["AttrID"].Value);
                    if (node != null)
                    {
                        break;
                    }
                }
            }
            if (node == null || node.ParentNode == null)
                return "";

            string nodeName = string.Empty;

            while (node.ParentNode != null)
            {
                int datalevel = node.Attributes != null && node.Attributes["AttrID"] != null ? Convert.ToInt16(node.Attributes["Level"].Value) : 0;
                if (node.Attributes != null && node.Attributes["AttrName"] != null)
                {
                    nodeName = node.Attributes["AttrName"].Value;
                }

                if (datalevel == level)
                {
                    return nodeName;
                }
                node = node.ParentNode;
                datalevel = node.Attributes != null && node.Attributes["AttrID"] != null ? Convert.ToInt16(node.Attributes["Level"].Value) : 0;
                if (node.Attributes != null && node.Attributes["AttrName"] != null)
                {
                    nodeName = node.Attributes["AttrName"].Value;
                }
                if (datalevel == level)
                {
                    return nodeName;
                }
            }
            return "";
        }
        private XmlNode GetChildWithKey(XmlNode xNode, string key)
        {
            if (xNode != null)
            {
                for (int i = 0; i < xNode.ChildNodes.Count; i++)
                {
                    XmlNode childNode;
                    if (xNode.ChildNodes[i].Attributes != null && xNode.ChildNodes[i].Attributes["AttrID"] != null && xNode.ChildNodes[i].Attributes["AttrID"].Value == key)
                    {
                        childNode = xNode.ChildNodes[i];
                        return childNode;
                    }
                    if (xNode.ChildNodes[i].ChildNodes.Count != 0)
                    {
                        childNode = GetChildWithKey(xNode.ChildNodes[i], key);
                        if (childNode != null) { return childNode; }
                    }
                }
            }
            return null;
        }
        private string ApplyStyleFormat(int attributeId, string numericValue)
        {
            if (Isnumber(numericValue.Replace(",", "")))
            {
                double dt;
                string sqlString = "SELECT STYLE_FORMAT,ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + attributeId;
                SQLString = sqlString;
                DataSet styleDs = CreateDataSet();
                string style = styleDs.Tables[0].Rows[0].ItemArray[0].ToString();
                string attributedatatype = styleDs.Tables[0].Rows[0].ItemArray[1].ToString();
                if (!attributedatatype.ToLower().Contains("text"))
                {
                    int index = style.IndexOf("[", StringComparison.Ordinal);
                    if (index != -1)
                    {
                        style = style.Substring(0, index - 1);
                        dt = Convert.ToDouble(numericValue.Trim());
                    }
                    else
                    {
                        dt = Convert.ToDouble(numericValue.Trim());
                        return dt.ToString(CultureInfo.InvariantCulture);
                    }

                    return dt.ToString(style.Trim());
                }
                return numericValue;
            }
            return numericValue;
        }

        private string Prepare(string strVal)
        {
            string strRetVal = strVal.Replace("'", "''");
            strRetVal = strRetVal.Trim();
            return strRetVal;
        }
        private void GetCurrencySymbol(string attrId)
        {
            string sqlStr = "SELECT ATTRIBUTE_DATARULE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + attrId + "";
            SQLString = sqlStr;
            DataSet dscUrrency = CreateDataSet();
            _prefix = string.Empty; _suffix = string.Empty; _emptyCondition = string.Empty; _replaceText = string.Empty; _headeroptions = string.Empty; _fornumeric = string.Empty;
            if (dscUrrency.Tables[0].Rows.Count > 0)
            {
                if (dscUrrency.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    string xmLstr = dscUrrency.Tables[0].Rows[0].ItemArray[0].ToString();
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmLstr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    {
                        if (rootNode != null)
                        {
                            XmlNodeList xmlNodeList = rootNode.ChildNodes;

                            for (int xmlNode = 0; xmlNode < xmlNodeList.Count; xmlNode++)
                            {
                                if (xmlNodeList[xmlNode].ChildNodes.Count > 0)
                                {
                                    if (xmlNodeList[xmlNode].ChildNodes[0].LastChild != null)
                                    {
                                        _prefix = xmlNodeList[xmlNode].ChildNodes[0].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[1].LastChild != null)
                                    {
                                        _suffix = xmlNodeList[xmlNode].ChildNodes[1].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[2].LastChild != null)
                                    {
                                        _emptyCondition = xmlNodeList[xmlNode].ChildNodes[2].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[3].LastChild != null)
                                    {
                                        _replaceText = xmlNodeList[xmlNode].ChildNodes[3].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[4].LastChild != null)
                                    {
                                        _headeroptions = xmlNodeList[xmlNode].ChildNodes[4].LastChild.Value;
                                    }
                                    //if (xmlNodeList[xmlNode].ChildNodes[5].LastChild != null)
                                    //{
                                    //    _fornumeric = xmlNodeList[xmlNode].ChildNodes[5].LastChild.Value;
                                    //}
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ConstructPivotTable()
        {
            //Populate Body Fields
            int tempGrouAttrCount = 0;
            var myData = new[] { new { a = 101, b = "tst" } };
            for (int k = 0; k < _rowTable.Rows.Count; k++)
            {
                DataRow dr = _pivotTable.NewRow();
                Int32 colPosition = 0;

                //Set value for Row Fields 
                for (int l = 0; l < _leftRowTable.Columns.Count; l++)
                {
                    XmlNode xNode = GetChildWithKey(_leftRowNode, "Attr:" + _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"]);
                    if (xNode != null)
                    {
                        if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("Merge"))
                            if (xNode.Attributes != null)
                                _pivotTable.Columns[colPosition].ExtendedProperties.Add("Merge", xNode.Attributes["Merge"].Value);
                    }

                    if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("ATTRIBUTE_TYPE"))
                        _pivotTable.Columns[colPosition].ExtendedProperties.Add("ATTRIBUTE_TYPE", _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"].ToString());

                    if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("Attribute_ID"))
                        _pivotTable.Columns[colPosition].ExtendedProperties.Add("Attribute_ID", _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString());


                    GetCurrencySymbol(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString());
                    string sqlString = "SELECT ATTRIBUTE_DATATYPE,STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"];

                    SQLString = sqlString;
                    DataSet dss = CreateDataSet();
                    int numberdecimel = 0;
                    if (_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "4") //price
                    {
                        if (dss.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().StartsWith("NUM"))
                        {
                            string dtype = dss.Tables[0].Rows[0].ItemArray[0].ToString();

                            if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                            {
                                numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                            }
                        }
                        bool styleFormat = dss.Tables[0].Rows[0].ItemArray[1].ToString().Length > 0;
                        if (_rowTable.Rows[k][_leftRowTable.Columns[colPosition].Caption] == DBNull.Value)
                        {
                            if (_rowTable.Rows[k][_leftRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_leftRowTable.Columns[l].Caption].ToString().Trim(), k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                        else
                        {
                            if (_rowTable.Rows[k][_leftRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_leftRowTable.Columns[l].Caption].ToString().Trim(), k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                    }
                    else if (_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "1") //product technical specifications
                    {
                        if (_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString() != "1")
                        {
                            bool styleFormat = dss.Tables[0].Rows[0].ItemArray[1].ToString().Length > 0;
                            if (_rowTable.Rows[k][_leftRowTable.Columns[colPosition].Caption] == DBNull.Value)
                            {
                                if (_rowTable.Rows[k][_leftRowTable.Columns[l].Caption] == DBNull.Value)
                                    dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                                else
                                    dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_leftRowTable.Columns[l].Caption].ToString().Trim(), k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            }
                            else
                            {
                                if (_rowTable.Rows[k][_leftRowTable.Columns[l].Caption] == DBNull.Value)
                                    dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                                else
                                    dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_leftRowTable.Columns[l].Caption].ToString().Trim(), k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            }
                        }
                        else
                            dr[l] = _rowTable.Rows[k][_leftRowTable.Columns[colPosition].Caption].ToString();
                    }
                    else if (_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "6") //parts key
                    {
                        bool styleFormat = dss.Tables[0].Rows[0].ItemArray[1].ToString().Length > 0;
                        if (_rowTable.Rows[k][_leftRowTable.Columns[colPosition].Caption] == DBNull.Value)
                        {
                            if (_rowTable.Rows[k][_leftRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_leftRowTable.Columns[l].Caption].ToString().Trim(), k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                        else
                        {
                            if (_rowTable.Rows[k][_leftRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_leftRowTable.Columns[l].Caption].ToString().Trim(), k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_leftRowTable.Columns[l].Caption].ToString().Trim(), k, _leftRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                    }
                    else
                    {
                        dr[l] = _rowTable.Rows[k][_leftRowTable.Columns[colPosition].Caption].ToString();

                    }

                    colPosition = colPosition + 1;
                }

                for (int m = 0; m < _columnTable.Rows.Count; m++)
                {
                    string[] columnNameTemp = { string.Empty };
                    foreach (var t1 in objGroupedSummaryAttrs.Where(t1 => columnNameTemp[0] != t1.Value || t1.Value == "None"))
                    {
                        columnNameTemp[0] = t1.Value;
                        string t = objGroupedSummaryAttrs.Count(a => a.Value == t1.Value && a.Value != "None") > 0 ? objGroupedSummaryAttrs.Where(a => a.Value == t1.Value && a.Value != "None").ElementAt(tempGrouAttrCount).Key : t1.Key;
                        XmlNode xNode = GetChildWithKey(_leftRowNode, "Attr:" + _sourceTable.Columns[t].ExtendedProperties["ATTRIBUTE_ID"]);
                        if (xNode != null)
                        {
                            if (xNode.Attributes != null)
                            {
                                if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("Merge")
                                    && !_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("Attribute_ID")
                                    && !_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("ATTRIBUTE_TYPE")
                                    && !_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE"))
                                {
                                    _pivotTable.Columns[colPosition].ExtendedProperties.Add("Merge", xNode.Attributes["Merge"].Value);
                                    _pivotTable.Columns[colPosition].ExtendedProperties.Add("Attribute_ID", _sourceTable.Columns[t].ExtendedProperties["ATTRIBUTE_ID"].ToString());
                                    _pivotTable.Columns[colPosition].ExtendedProperties.Add("ATTRIBUTE_TYPE", _sourceTable.Columns[t].ExtendedProperties["ATTRIBUTE_TYPE"].ToString());
                                    _pivotTable.Columns[colPosition].ExtendedProperties.Add("ATTRIBUTE_STYLE", _sourceTable.Columns[t].ExtendedProperties["ATTRIBUTE_STYLE"].ToString());
                                    ILookup<int, string> lookup = myData.ToLookup(x => x.a, x => x.b);
                                    IEnumerable<string> allOnes = lookup[colPosition];
                                    if (!allOnes.Contains(t))
                                    {
                                        var l = myData.ToList();
                                        l.Add(new { a = colPosition, b = t });
                                        myData = l.ToArray();
                                    }
                                    if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE_HEADER"))
                                    {
                                        if (_columnTable.Columns.Count > 0)
                                        {
                                            string r = _columnTable.Columns[0].ColumnName;
                                            _pivotTable.Columns[colPosition].ExtendedProperties.Add("ATTRIBUTE_STYLE_HEADER", _sourceTable.Columns[r].ExtendedProperties["ATTRIBUTE_STYLE"].ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    ILookup<int, string> lookup = myData.ToLookup(x => x.a, x => x.b);
                                    IEnumerable<string> allOnes = lookup[colPosition];
                                    if (!allOnes.Contains(t))
                                    {
                                        _pivotTable.Columns[colPosition].ExtendedProperties["Merge"] += "," + xNode.Attributes["Merge"].Value;
                                        _pivotTable.Columns[colPosition].ExtendedProperties["Attribute_ID"] += "," +
                                                                                                               _sourceTable
                                                                                                                   .Columns
                                                                                                                   [t]
                                                                                                                   .ExtendedProperties
                                                                                                                   [
                                                                                                                       "ATTRIBUTE_ID"
                                                                                                                   ];
                                        _pivotTable.Columns[colPosition].ExtendedProperties["ATTRIBUTE_TYPE"] += "," +
                                                                                                                 _sourceTable
                                                                                                                     .Columns
                                                                                                                     [t]
                                                                                                                     .ExtendedProperties
                                                                                                                     [
                                                                                                                         "ATTRIBUTE_TYPE"
                                                                                                                     ];
                                        _pivotTable.Columns[colPosition].ExtendedProperties["ATTRIBUTE_STYLE"] += "," +
                                                                                                                  _sourceTable
                                                                                                                      .Columns
                                                                                                                      [t
                                                                                                                      ]
                                                                                                                      .ExtendedProperties
                                                                                                                      [
                                                                                                                          "ATTRIBUTE_STYLE"
                                                                                                                      ];
                                        var l = myData.ToList();
                                        l.Add(new { a = colPosition, b = t });
                                        myData = l.ToArray();
                                    }
                                    if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("ATTRIBUTE_STYLE_HEADER"))
                                    {
                                        if (_columnTable.Columns.Count > 0)
                                        {
                                            string r = _columnTable.Columns[0].ColumnName;
                                            _pivotTable.Columns[colPosition].ExtendedProperties.Add("ATTRIBUTE_STYLE_HEADER", _sourceTable.Columns[r].ExtendedProperties["ATTRIBUTE_STYLE"].ToString());
                                        }
                                    }
                                }
                            }
                        }

                        if (_sourceTable.Columns[t].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "3")
                        {
                            if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("ImageBodyFields"))
                                _pivotTable.Columns[colPosition].ExtendedProperties.Add("ImageBodyFields", "True");
                            else
                            {
                                ILookup<int, string> lookup = myData.ToLookup(x => x.a, x => x.b);
                                IEnumerable<string> allOnes = lookup[colPosition];
                                if (!allOnes.Contains(t))
                                {
                                    _pivotTable.Columns[colPosition].ExtendedProperties["ImageBodyFields"] += "True";
                                    var l = myData.ToList();
                                    l.Add(new { a = colPosition, b = t });
                                    myData = l.ToArray();
                                }
                            }
                        }
                        dr[colPosition] = FetchBodyFieldValue(t, k, m);
                        dr[colPosition] = dr[colPosition].ToString().Trim();
                        colPosition = colPosition + 1;
                    }
                }
                if (objGroupedSummaryAttrs.Values.Count(a => a != "None") - 1 > tempGrouAttrCount)
                {
                    tempGrouAttrCount++;
                }
                else
                {
                    tempGrouAttrCount = 0;
                }

                //Set value for Right Row Fields 
                for (int l = 0; l < _rightRowTable.Columns.Count; l++)
                {
                    XmlNode xNode = GetChildWithKey(_leftRowNode, "Attr:" + _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"]);
                    if (xNode != null)
                    {
                        if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("Merge"))
                            if (xNode.Attributes != null)
                                _pivotTable.Columns[colPosition].ExtendedProperties.Add("Merge", xNode.Attributes["Merge"].Value);
                    }

                    if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("ATTRIBUTE_TYPE"))
                        _pivotTable.Columns[colPosition].ExtendedProperties.Add("ATTRIBUTE_TYPE", _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"].ToString());


                    if (!_pivotTable.Columns[colPosition].ExtendedProperties.ContainsKey("Attribute_ID"))
                        _pivotTable.Columns[colPosition].ExtendedProperties.Add("Attribute_ID", _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString());

                    GetCurrencySymbol(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString());
                    string sqlString = "SELECT ATTRIBUTE_DATATYPE,STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"];

                    SQLString = sqlString;
                    DataSet dss = CreateDataSet();
                    if (_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "4") //price attribute
                    {
                        int numberdecimel = 0;
                        if (dss.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().StartsWith("NUM"))
                        {
                            string dtype = dss.Tables[0].Rows[0].ItemArray[0].ToString();

                            if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                            {
                                numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                            }
                        }
                        bool styleFormat = dss.Tables[0].Rows[0].ItemArray[1].ToString().Length > 0;
                        if (_rowTable.Rows[k][_rightRowTable.Columns[l].Caption] == DBNull.Value)
                        {
                            if (_rowTable.Rows[k][_rightRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_rightRowTable.Columns[l].Caption].ToString().Trim(), k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                        else
                        {
                            if (_rowTable.Rows[k][_rightRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_rightRowTable.Columns[l].Caption].ToString().Trim(), k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                    }
                    else if (_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "1") //product technical specifications attribute
                    {
                        int numberdecimel = 0;
                        if (dss.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().StartsWith("NUM"))
                        {
                            string dtype = dss.Tables[0].Rows[0].ItemArray[0].ToString();

                            if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                            {
                                numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                            }
                        }
                        bool styleFormat = dss.Tables[0].Rows[0].ItemArray[1].ToString().Length > 0;
                        if (_rowTable.Rows[k][_rightRowTable.Columns[l].Caption] == DBNull.Value)
                        {
                            if (_rowTable.Rows[k][_rightRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_rightRowTable.Columns[l].Caption].ToString().Trim(), k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                        else
                        {
                            if (_rowTable.Rows[k][_rightRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_rightRowTable.Columns[l].Caption].ToString().Trim(), k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                    }
                    else if (_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "6") //parts key attribute
                    {
                        int numberdecimel = 0;
                        if (dss.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().StartsWith("NUM"))
                        {
                            string dtype = dss.Tables[0].Rows[0].ItemArray[0].ToString();

                            if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                            {
                                numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                            }
                        }
                        bool styleFormat = dss.Tables[0].Rows[0].ItemArray[1].ToString().Length > 0;
                        if (_rowTable.Rows[k][_rightRowTable.Columns[l].Caption] == DBNull.Value)
                        {
                            if (_rowTable.Rows[k][_rightRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_rightRowTable.Columns[l].Caption].ToString().Trim(), k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                        else
                        {
                            if (_rowTable.Rows[k][_rightRowTable.Columns[l].Caption] == DBNull.Value)
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), null, k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                            else
                                dr[colPosition] = DataRule(Convert.ToString(_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"]), _rowTable.Rows[k][_rightRowTable.Columns[l].Caption].ToString().Trim(), k, _rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_ID"].ToString(), numberdecimel, styleFormat);
                        }
                    }
                    else if (_rightRowTable.Columns[l].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "3")
                    {
                        dr[colPosition] = _rowTable.Rows[k][_rightRowTable.Columns[l].Caption].ToString();
                    }
                    else
                    {
                        dr[colPosition] = _rowTable.Rows[k][_rightRowTable.Columns[l].Caption].ToString();

                    }
                    colPosition = colPosition + 1;
                }
                _pivotTable.Rows.Add(dr);
            }
        }

        private string DataRule(string attributeType, string cellData, int rowNumber, string extendedPropertiesOfAttribId, int decimalplace, bool styleFormat)
        {
            string returnData = "";
            string currencyValue = string.Empty;
            if (cellData != null)
            {
                if (attributeType == "4") //for price type attribute
                {
                    if (cellData.Trim().Length > 0)
                    {
                        if (Convert.ToString(cellData) != string.Empty && cellData != "0.00" && cellData != "0")//Deleted (&& cellData != "0.000000") from IF to insert numeric values as replace text
                        {
                            currencyValue = DecPrecisionFill(cellData, decimalplace);
                            currencyValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(extendedPropertiesOfAttribId.Trim()), currencyValue.Trim()) : currencyValue;
                        }
                        else
                        {
                            if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000")
                            {
                                if ((_replaceText != "") && (Isnumber(_replaceText)))
                                {
                                    currencyValue = _emptyCondition == cellData ? _replaceText : cellData;
                                    currencyValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(extendedPropertiesOfAttribId.Trim()), currencyValue.Trim()) : currencyValue;
                                }
                                else
                                    currencyValue = DecPrecisionFill(cellData, decimalplace);
                                currencyValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(extendedPropertiesOfAttribId.Trim()), currencyValue.Trim()) : currencyValue;
                                //currencyValue = cellData;
                            }
                            else
                            {
                                currencyValue = DecPrecisionFill(cellData, decimalplace);
                                currencyValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(extendedPropertiesOfAttribId.Trim()), currencyValue.Trim()) : currencyValue;
                                //currencyValue = cellData;
                            }
                            //currencyValue = DecPrecisionFill(currencyValue, decimalplace);
                            //currencyValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(extendedPropertiesOfAttribID.Trim()), currencyValue.Trim()) : currencyValue;
                        }
                    }
                    if (_headeroptions == "All")
                    {
                        if (Isnumber(currencyValue.Replace(",", "")))
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currentvalue)
                                    {
                                        //returnData = _replaceText;
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                        {
                                            if (currentvalue == "Empty" || currencyValue == "" || currentvalue == "0.00" || currencyValue == "0" || currencyValue == "Null")
                                            {
                                                returnData = _replaceText;
                                            }
                                            else
                                            {
                                                returnData = _prefix + _replaceText + _suffix;
                                            }
                                            
                                        }
                                    }
                                    else
                                    {
                                        if (currentvalue == "Empty" || currencyValue == "" || currentvalue == "0.00" || currencyValue == "0" || currencyValue == "Null")
                                        {
                                            returnData = _replaceText;
                                        }
                                        else
                                        {
                                            returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                    }
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                        else
                        {

                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currencyValue)
                                    {
                                        //returnData = _replaceText;
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        if (currencyValue == "")
                                        {
                                            returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                    }
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                    }
                    else if (rowNumber == 0 && _headeroptions == "First")  /* colPosition == 0 &&  */
                    {
                        if (Isnumber(currencyValue.Replace(",", "")))
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currentvalue)
                                    {
                                        //returnData = _replaceText;
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        if (currencyValue == "")
                                        {
                                            returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                    }
                                }
                                else
                                {
                                    if (currencyValue == "")
                                    {
                                        returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                        else
                        {
                            returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                        }
                    }
                    else
                    {
                        if (Isnumber(currencyValue.Replace(",", "")))
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    returnData = _emptyCondition == currentvalue ? _replaceText : currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                            }
                            else if (_emptyCondition == "")
                            {

                                returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                            }
                        }
                        else
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                returnData = _emptyCondition == currencyValue ? _replaceText : currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                            }
                            else
                            {
                                returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                            }
                        }
                    }
                }

                else if (attributeType == "1") //for product technical specification attributes
                {
                    if (cellData.Trim().Length > 0)
                    {
                        currencyValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(extendedPropertiesOfAttribId.Trim()), cellData.Trim()) : cellData;
                    }

                    if (_headeroptions == "All")
                    {
                        if (Isnumber(currencyValue.Replace(",", "")))
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currentvalue || _emptyCondition == cellData)
                                    {
                                        //returnData = _replaceText;
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                        else
                        {
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currencyValue)
                                    {
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        if (currencyValue == "")
                                        {
                                            returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                    }
                                }
                                else
                                {
                                    if (currencyValue == "")
                                    {
                                        returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                    }
                    else if (rowNumber == 0 && _headeroptions == "First")  /* colPosition == 0 &&  */
                    {
                        if (Isnumber(currencyValue.Replace(",", "")))
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currentvalue)
                                    {
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        if (currencyValue == "")
                                        {
                                            returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                    }
                                }
                                else
                                {
                                    if (currencyValue == "")
                                    {
                                        returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                        else
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currentvalue)
                                    {
                                        //returnData = _replaceText;
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        if (currencyValue == "")
                                        {
                                            returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                    }
                                }
                                else
                                {
                                    if (currencyValue == "")
                                    {
                                        returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Isnumber(currencyValue.Replace(",", "")))
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    returnData = _emptyCondition == currentvalue ? _replaceText : currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                            }
                            else if (_emptyCondition == "")
                            {

                                returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                            }
                        }
                        else
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                returnData = _emptyCondition == currencyValue ? _replaceText : currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                            }
                            else
                            {
                                returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;")
                                    .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                    .Replace("<br>", "<br />").Replace("<br/>", "<br />");
                            }
                        }
                    }
                }
                else if (attributeType == "6") //for parts key attributes
                {
                    if (cellData.Trim().Length > 0)
                    {
                        currencyValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(extendedPropertiesOfAttribId.Trim()), cellData.Trim()) : cellData;
                    }

                    if (_headeroptions == "All")
                    {
                        if (Isnumber(currencyValue.Replace(",", "")))
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currentvalue)
                                    {
                                        //returnData = _replaceText;
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                                else
                                {
                                    if (currencyValue == "")
                                    {
                                        returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                        else
                        {

                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currencyValue)
                                    {
                                        //returnData = _replaceText;
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        if (currencyValue == "")
                                        {
                                            returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                    }
                                }
                                else
                                {
                                    if (currencyValue == "")
                                    {
                                        returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                    }
                    else if (rowNumber == 0 && _headeroptions == "First")  /* colPosition == 0 &&  */
                    {
                        if (Isnumber(currencyValue.Replace(",", "")))
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currentvalue)
                                    {
                                        // returnData = _replaceText;
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        if (currencyValue == "")
                                        {
                                            returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                    }
                                }
                                else
                                {
                                    if (currencyValue == "")
                                    {
                                        returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                        else
                        {
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    if (_emptyCondition == currencyValue)
                                    {
                                        //returnData = _replaceText;
                                        if (_fornumeric == "1")
                                        {
                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                returnData = _prefix + _replaceText + _suffix;
                                            else
                                                returnData = _replaceText;
                                        }
                                        else
                                            returnData = _prefix + _replaceText + _suffix;
                                    }
                                    else
                                    {
                                        if (currencyValue == "")
                                        {
                                            returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                    }
                                }
                                else
                                {
                                    if (currencyValue == "")
                                    {
                                        returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                    else
                                    {
                                        returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                }
                            }
                            else if (_emptyCondition == "")
                            {
                                if (currencyValue == "")
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = _prefix + currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Isnumber(currencyValue.Replace(",", "")))
                        {
                            string currentvalue = currencyValue;
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                            {
                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                if (_replaceText != "")
                                {
                                    returnData = _emptyCondition == currentvalue ? _replaceText : currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                            }
                            else if (_emptyCondition == "")
                            {

                                returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                            }
                        }
                        else
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                returnData = _emptyCondition == currencyValue ? _replaceText : currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                            }
                            else
                            {
                                returnData = currencyValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                            }
                        }
                    }
                }
            }
            else
            {
                SQLString = "SELECT ATTRIBUTE_ID,ATTRIBUTE_TYPE,ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE " +
                    "ATTRIBUTE_ID = " + extendedPropertiesOfAttribId;
                DataSet adt = CreateDataSet();
                if (adt.Tables.Count > 0)
                    if (adt.Tables[0].Rows.Count > 0)
                        if (adt.Tables[0].Rows[0].ItemArray[2].ToString().StartsWith("Text"))
                            cellData = "Empty";
                        else if (adt.Tables[0].Rows[0].ItemArray[2].ToString().StartsWith("Number"))
                            cellData = "Null";

                if (String.Equals(_emptyCondition, cellData, StringComparison.CurrentCultureIgnoreCase))
                //if (String.Equals(_emptyCondition, "Null", StringComparison.CurrentCultureIgnoreCase) || String.Equals(_emptyCondition, "Empty", StringComparison.CurrentCultureIgnoreCase))
                {
                    //returnData = _replaceText;
                    if (_fornumeric == "1")
                    {
                        if (Isnumber(_replaceText.Replace(",", "")))
                            returnData = _prefix + _replaceText + _suffix;
                        else
                            returnData = _replaceText;
                    }
                    else
                    {
                        returnData = _prefix + _replaceText + _suffix;
                    }
                }
                else
                {
                    if(currencyValue == null || currencyValue == string.Empty)
                    {
                        returnData = _replaceText;
                    }
                    else
                    {
                        returnData = _prefix + _replaceText + _suffix;
                    }

                }
            }
            return returnData;
        }
        private string DecPrecisionFill(string strvalue, int noofdecimal)
        {
            if (strvalue.IndexOf(".", StringComparison.Ordinal) > 0)
            {
                if ((strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1) > 0)
                {
                    if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) == 0)
                    {
                        strvalue = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal));
                        string retVal = "";
                        for (int preadd = 0; preadd < noofdecimal; preadd++)
                        { retVal = retVal + "0"; }
                        if (retVal != "")
                        {
                            strvalue = strvalue + "." + retVal;
                        }
                    }
                    else if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) > 0)
                    {
                        if (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1).Length >= noofdecimal)
                            strvalue = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal)) + "." + (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, noofdecimal));
                    }
                }
            }
            return strvalue;
        }
        private string FetchBodyFieldValue(string fieldName, int rowFieldIndex, int colFieldIndex)
        {
            string result = string.Empty;
            string condition = string.Empty;

            if (ConstructRowFieldCondition(rowFieldIndex).Trim() != string.Empty && ConstructColumnFieldCondition(colFieldIndex).Trim() != string.Empty)
            {
                condition = ConstructRowFieldCondition(rowFieldIndex) + " and " + ConstructColumnFieldCondition(colFieldIndex);
            }
            else if (ConstructRowFieldCondition(rowFieldIndex).Trim() == string.Empty)
            {
                condition = ConstructColumnFieldCondition(colFieldIndex);
            }
            else if (ConstructColumnFieldCondition(colFieldIndex).Trim() == string.Empty)
            {
                condition = ConstructRowFieldCondition(rowFieldIndex);
            }
            try
            {
                string sqlString = "SELECT ATTRIBUTE_DATATYPE,STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " +
                                   _sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_ID"];
                SQLString = sqlString;
                CreateDataSet();
                if (condition != string.Empty)
                {
                    foreach (DataRow oDr in _sourceTable.Select(condition))
                    {
                        GetCurrencySymbol(_sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_ID"].ToString());
                        if (_sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "4")
                        {
                            if (oDr[fieldName] == DBNull.Value)
                            {
                                result = null;
                            }
                            else if (oDr[fieldName].ToString() == "" && _emptyCondition == "Empty")
                            {
                                result = "Empty";
                            }
                            else
                            {
                                result =
                                    ApplyStyleFormat(
                                        Convert.ToInt32(
                                            _sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_ID"].ToString()
                                                .Trim()), oDr[fieldName].ToString().Trim());
                            }
                            if (_emptyCondition == "Null")
                                _emptyCondition = null;
                            if ((_emptyCondition == "Empty" || _emptyCondition == null || _emptyCondition == "0" ||
                                 _emptyCondition == "0.00") && (result == "Empty") || (result == null) ||
                                (result == "0") || (result == "0.00"))
                            {
                                if ((result) == (_emptyCondition) || oDr[fieldName].ToString().Trim() == _emptyCondition)
                                {
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                            result = _prefix + _replaceText + _suffix;
                                        else
                                            result = _replaceText;
                                    }
                                    else
                                        result = _prefix + _replaceText + _suffix;
                                }
                            }
                            else if (_headeroptions == "First" && rowFieldIndex == 0)
                            {
                                if (Isnumber(result.Replace(",", "")))
                                {
                                    result = _prefix +
                                             result.Replace("<", "&lt;")
                                                 .Replace(">", "&gt;")
                                                 .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                                  + _suffix;
                                }
                                else
                                {
                                    result =
                                        result.Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                            ;
                                }
                            }
                            else if (_headeroptions == "All")
                            {
                                if (Isnumber(result.Replace(",", "")))
                                {
                                    result = _prefix +
                                             result.Replace("<", "&lt;")
                                                 .Replace(">", "&gt;")
                                                 .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                                  + _suffix;
                                }
                                else if(_sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "4")
                                {
                                    result = _prefix +
                                            result.Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                                 + _suffix;
                                }
                                else
                                {
                                    result =
                                        result.Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                            ;
                                }
                            }
                            else
                            {
                                result = " " +
                                         result.Replace("<", "&lt;")
                                             .Replace(">", "&gt;")
                                             .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                              + " ";
                            }
                        }
                        else if (_sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "1") //for product technical specification attributes
                        {
                            if (oDr[fieldName] == DBNull.Value)
                            {
                                result = null;
                            }
                            else if (oDr[fieldName].ToString() == "" && _emptyCondition == "Empty")
                            {
                                result = "Empty";
                            }
                            else
                            {
                                result =
         ApplyStyleFormat(
             Convert.ToInt32(
                 _sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_ID"].ToString()
                     .Trim()), oDr[fieldName].ToString().Trim());

                            }

                            if (_emptyCondition == "Null")
                                _emptyCondition = null;
                            if ((_emptyCondition == "Empty" || _emptyCondition == null || _emptyCondition == "0" ||
                                 _emptyCondition == "0.00") && (result == "Empty") || (result == null) ||
                                (result == "0") || (result == "0.00"))
                            {
                                if ((result) == (_emptyCondition) || oDr[fieldName].ToString().Trim() == _emptyCondition)
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                            result = _prefix + _replaceText + _suffix;
                                        else
                                            result = _replaceText;
                                    }
                                    else
                                        result = _prefix + _replaceText + _suffix;
                            }
                            else if (_headeroptions == "First" && rowFieldIndex == 0)
                            {
                                result = _prefix +
                                         result.Replace("<", "&lt;")
                                             .Replace(">", "&gt;")
                                             .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                              + _suffix;
                            }
                            else if (_headeroptions == "All")
                            {
                                result = _prefix +
                                         result.Replace("<", "&lt;")
                                             .Replace(">", "&gt;")
                                             .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                              + _suffix;
                            }
                            else
                            {
                                result = " " +
                                         result.Replace("<", "&lt;")
                                             .Replace(">", "&gt;")
                                             .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                              + " ";
                            }
                        }
                        else if (_sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_TYPE"].ToString() == "6") //for parts key attributes
                        {
                            if (oDr[fieldName] == DBNull.Value)
                            {
                                result = null;
                            }
                            else if (oDr[fieldName].ToString() == "" && _emptyCondition == "Empty")
                            {
                                result = "Empty";
                            }
                            else
                            {
                                result =
          ApplyStyleFormat(
              Convert.ToInt32(
                  _sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_ID"].ToString()
                      .Trim()), oDr[fieldName].ToString().Trim());

                            }
                            if (_emptyCondition == "Null")
                                _emptyCondition = null;
                            if ((_emptyCondition == "Empty" || _emptyCondition == null || _emptyCondition == "0" ||
                                 _emptyCondition == "0.00") && (result == "Empty") || (result == null) ||
                                (result == "0") || (result == "0.00"))
                            {
                                if ((result) == (_emptyCondition) || oDr[fieldName].ToString().Trim() == _emptyCondition)
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                            result = _prefix + _replaceText + _suffix;
                                        else
                                            result = _replaceText;
                                    }
                                    else
                                        result = _prefix + _replaceText + _suffix;
                            }
                            else if (_headeroptions == "First" && rowFieldIndex == 0)
                            {
                                result = _prefix +
                                         result.Replace("<", "&lt;")
                                             .Replace(">", "&gt;")
                                             .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                              + _suffix;
                            }
                            else if (_headeroptions == "All")
                            {
                                result = _prefix +
                                         result.Replace("<", "&lt;")
                                             .Replace(">", "&gt;")
                                             .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                              + _suffix;
                            }
                            else
                            {
                                result = " " +
                                         result.Replace("<", "&lt;")
                                             .Replace(">", "&gt;")
                                             .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                              + " ";
                            }
                        }
                        else
                        {
                            result =
          ApplyStyleFormat(
              Convert.ToInt32(
                  _sourceTable.Columns[fieldName].ExtendedProperties["ATTRIBUTE_ID"].ToString()
                      .Trim()), oDr[fieldName].ToString().Trim());

                        }
                    }
                }
            }
            catch //(Exception ex)
            {
                result = _mPlaceHolderText;
            }

            if (string.IsNullOrEmpty(result))
            {
                result = _mPlaceHolderText;
            }

            return result;
        }
        private string ConstructRowFieldCondition(int rowIndex)
        {
            string rowCond = string.Empty;
            if (_rowTable.Columns.Count > 0)
            {
                for (int l = 0; l < _rowTable.Columns.Count; l++)
                {
                    rowCond = rowCond.Trim() != string.Empty
                        ? rowCond + " and [" + Prepare(_rowTable.Columns[l].ColumnName) + "] = " + "'" +
                          Prepare(_rowTable.Rows[rowIndex][l].ToString()) + "'"
                        : " [" + Prepare(_rowTable.Columns[l].ColumnName) + "] = " + "'" +
                          Prepare(_rowTable.Rows[rowIndex][l].ToString()) + "'";
                }
            }
            return rowCond;
        }

        private string ConstructColumnFieldCondition(int rowIndex)
        {
            string colCond = string.Empty;
            if (_columnTable.Columns.Count > 0)
            {
                for (int l = 0; l < _columnTable.Columns.Count; l++)
                {
                    colCond = colCond.Trim() != string.Empty
                        ? colCond + " and [" + Prepare(_columnTable.Columns[l].ColumnName) + "] = " + "'" +
                          Prepare(_columnTable.Rows[rowIndex][l].ToString()) + "'"
                        : " [" + Prepare(_columnTable.Columns[l].ColumnName) + "] = " + "'" +
                          Prepare(_columnTable.Rows[rowIndex][l].ToString()) + "'";
                }
            }
            return colCond;
        }
        private void SetGroupSummaryField()
        {
            if (_mSummaryFields.Count <= 1)
            {
                return;
            }

            if (_columnTable.Columns.Count <= 0) { return; }
            int index;
            if (_columnTable.Columns.Count > 1)
            {
                index = _columnTable.Columns.Count - 2;
            }
            else { index = 0; }

            string lastValue = _pivotTable.Rows[index][0].ToString().Trim();

            for (int c = 0; c < _pivotTable.Columns.Count; c++)
            {
                if ((_pivotTable.Rows[index][c].ToString().Trim() != lastValue) || (c == _pivotTable.Columns.Count - 1))
                {//Owner Group Changes Here
                    if (_mSummaryGroupField.Trim() == _pivotTable.Rows[_columnTable.Columns.Count][c - 1].ToString().Trim())
                    {
                        if (!_pivotTable.Columns[c - 1].ExtendedProperties.ContainsKey("GroupField"))
                            _pivotTable.Columns[c - 1].ExtendedProperties.Add("GroupField", "True");
                    }
                }
                lastValue = _pivotTable.Rows[index][c].ToString().Trim();
            }

            if (_mSummaryGroupField.Trim() == _pivotTable.Rows[_columnTable.Columns.Count][_pivotTable.Columns.Count - 1].ToString().Trim())
            {
                if (!_pivotTable.Columns[_pivotTable.Columns.Count - 1].ExtendedProperties.ContainsKey("GroupField"))
                    _pivotTable.Columns[_pivotTable.Columns.Count - 1].ExtendedProperties.Add("GroupField", "True");
            }

            for (int c = 0; c < _pivotTable.Columns.Count; c++)
            {
                if (_mSummaryGroupField.Trim() == _pivotTable.Rows[_columnTable.Columns.Count][c].ToString().Trim())
                {
                    if (!_pivotTable.Columns[c].ExtendedProperties.Contains("GroupField"))
                    {
                        _pivotTable.Columns.RemoveAt(c);
                        c = 0;
                    }
                    else
                    {
                        for (int j = _columnTable.Columns.Count - 1; j >= 0; j--)
                        {
                            _pivotTable.Rows[j][c] = "";
                        }
                    }
                }
            }
        }
        public string ImagePath
        {
            private get
            {
                return _imagePath;
            }
            set
            {
                _imagePath = value;
            }
        }
        private bool CheckExists(int iattrID)
        {
            if (iattrID == 0)
            {
                //  return true;
            }
            return _attributeIdList.Any(t => iattrID == t);
        }

        public string SQLString { get; set; }

        public DataSet CreateDataSet()
        {
            var dsReturn = new DataSet();
            using (
                var objSqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                //var ds = new DataSet();

                var dbAdapter = new SqlDataAdapter(SQLString, objSqlConnection);
                dbAdapter.SelectCommand.CommandTimeout = 0;
                dbAdapter.Fill(dsReturn);
                dbAdapter.Dispose();
                return dsReturn;
            }
        }
        public bool Isnumber(string refValue)
        {
            if (refValue == "")
            {
                return false;
            }
            //const string strReg =
            //    @"^((\d?)|(([-+]?\d+\.?\d*)|([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d+\.?\d*))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d+\.?\d*)))$";
            var retval = false;
            //var res = new Regex(strReg);
            //if (res.IsMatch(refValue))
            //{
            //    retval = true;
            //}
            //return retval;
            const string strRegx = @"^[0-9,]*(\.)?[0-9]+$";
            var re = new Regex(strRegx);
            
            if (re.IsMatch(refValue))
            {
                retval = true;
            }
            return retval; 
        }

        public int FamilyID
        {
            get { return _mFamilyID; }
            set { _mFamilyID = value; }
        }

        public int CatalogID
        {
            get { return _mCatalogID; }
            set { _mCatalogID = value; }
        }
        public string ApplicationStartupPath
        {
            get
            {
                return _applicationStartupPath;
            }
            set
            {
                _applicationStartupPath = value;
            }
        }
        public string TableLayoutName
        {
            private get
            {
                return _mTableLayoutName;
            }
            set
            {
                _mTableLayoutName = value;
            }
        }

        public string ConnectionString
        {
            get
            {
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }
        private string GetAttributeNameFromAttributeID(string attrID)
        {

            string attrName = string.Empty;
            try
            {
                string sqlStr = "SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID  = " + attrID + " ";
                SQLString = sqlStr;
                DataSet ds = CreateDataSet();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    attrName = ds.Tables[0].Rows[0]["ATTRIBUTE_NAME"].ToString();
                }

            }
            catch (Exception ex) { }
            return attrName;
        }
    }
    public class XmlTransformer
    {
        public string TransformToInDesignXml(string srcXml)
        {
            string output = string.Empty;
            try
            {
                var stream = new MemoryStream(Encoding.Unicode.GetBytes(srcXml));
                var document = new XPathDocument(stream);
                var writer = new StringWriter();
                var transform = new XslCompiledTransform();
                transform.Load(HttpContext.Current.Server.MapPath("~/Content/") + "\\XMLTemplate.xsl");
                transform.Transform(document, null, writer);
                output = writer.ToString();
            }
            catch(Exception ex)
            {

            }
            return output;
            //  return null;
        }
        public string TransformToInDesignMultipleXml(string srcXml)
        {
            string output = string.Empty;
            try
            {
                var stream = new MemoryStream(Encoding.Unicode.GetBytes(srcXml));
                var document = new XPathDocument(stream);
                var writer = new StringWriter();
                var transform = new XslCompiledTransform();
                transform.Load(HttpContext.Current.Server.MapPath("~/Content/") + "\\XMLIndesignTemplate.xsl");
                transform.Transform(document, null, writer);
                output = writer.ToString();
            }
            catch
            { }
            return output;
            //  return null;
        }

        public string TransformToHtml(string srcXml)
        {
            string output = string.Empty;
            try
            {
                var stream = new MemoryStream(Encoding.Unicode.GetBytes(srcXml));
                var document = new XPathDocument(stream);
                var writer = new StringWriter();
                var transform = new XslCompiledTransform();
                transform.Load(HttpContext.Current.Server.MapPath("~/Content/ProductImages/") + "\\HTMLTable.xsl");
                transform.Transform(document, null, writer);
                output = writer.ToString();
            }
            catch
            { }
            return output;

        }

    }
    public class SystemSettingsAttributes
    {
        #region "Declarations..."

        #endregion

        #region "Constructors..."

        public SystemSettingsAttributes()
        {

        }

        public SystemSettingsAttributes(string memberKey, string memberValue)
        {
            this.MemberKey = memberKey;
            this.MemberValue = memberValue;
        }

        #endregion

        #region "Properties..."

        [ConfigurationProperty("Key")]
        public string MemberKey { get; set; }

        [ConfigurationProperty("Value")]
        public string MemberValue { get; set; }

        #endregion

    }
    public class Treenodedata
    {

        public string ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }

    }

}
