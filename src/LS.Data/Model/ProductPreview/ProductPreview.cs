﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using log4net;
using System.Web.Configuration;


namespace LS.Data.Model.ProductPreview
{
    public class ProductPreview
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(ProductPreview));
        readonly CSEntities _dbcontext = new CSEntities();
        readonly Random _randomClass = new Random();
        private string _prefix, _suffix, _emptyCondition, _replaceText, _headeroptions = string.Empty;
        private Image _chkSize;
        private int ValInc { get; set; }
        public string convertionPath = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePath"].ToString();
        public string convertionPathREA = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePathREA"].ToString();
        private string _imageMagickPath = string.Empty;
        public string CustomerFolder = string.Empty;
        public string converttype = string.Empty;
        public string Exefile = string.Empty;

        public string GenerateSubProductGroupPreview(int catalogId, int familyId, int productId, bool productLevelMultipletablePreview, int userID, string categoryId)
        {
            try
            {
                var htmlString = new StringBuilder();
                var objDataSet = new DataSet();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = productId;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 20).Value = "SUBPRODUCTPREVIEW";
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataSet);
                }

                var imagemagickpath = _dbcontext.Customer_Settings.FirstOrDefault(a => a.CustomerId == userID);
                //if (imagemagickpath != null)
                //{
                //    _imageMagickPath = imagemagickpath.ImageConversionUtilityPath;
                //    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                //}
                //else
                //{
                //    _imageMagickPath = "C:\\Program Files\\ImageMagick-6.8.5-Q8";
                //}
                string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == userID).Select(a => a.ConverterType).SingleOrDefault();

                if (imagemagickpath != null && converttype == "Image Magic")
                {
                    _imageMagickPath = convertionPath;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                }
                else
                {
                    _imageMagickPath = convertionPathREA;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");

                }

                htmlString.Append("<html>");
                htmlString.Append("<left>");
                htmlString.Append("<head>");
                htmlString.Append("</head>");
                htmlString.Append("<body>");
                htmlString.Append("<table class=\"table table-bordered table-hover table-striped\" style=\"width:50%\" >");
                htmlString.Append("<style>td{font-family:arial Unicode ms;font-size:12px;}th{font-family:" + "Helvetica Neue" + ",Helvetica,Arial,sans-serif !important;;font-size:12px;font-weight:Bold}</style>");

                if (objDataSet.Tables.Count > 0)
                {
                    foreach (DataRow dr in objDataSet.Tables[0].Rows)
                    {
                        if (dr["ATTRIBUTE_TYPE"].ToString() != "6")
                        {
                            htmlString.Append("<tr><td align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \"><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                        }
                        else
                        {
                            htmlString.Append("<tr><td align=\"left\" style=\"background-color:lightgreen;\"><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                        }
                        const string alignVal = "Left";
                        string style = dr["STYLE_FORMAT"].ToString();
                        if (style.Length > 0)
                            style = style.Substring(0, style.IndexOf('[') - 1);
                        double dt = 0;
                        ExtractCurrenyFormat(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));

                        if (dr["ATTRIBUTE_TYPE"].ToString() == "6" && dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        {
                            if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
                            {
                                //dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
                                #region "Decimal Place Trimming"
                                string tempStr = dr["NUMERIC_VALUE"].ToString();
                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                if (noofdecimalplace != 6)
                                {
                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                }
                                if (noofdecimalplace == 0)
                                {
                                    tempStr = tempStr.TrimEnd('.');
                                }
                                dr["NUMERIC_VALUE"] = tempStr;
                                #endregion
                            }

                        }
                        else if (dr["ATTRIBUTE_TYPE"].ToString() == "4" || dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        {
                            if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
                            {
                                //dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
                                #region "Decimal Place Trimming"
                                string tempStr = dr["NUMERIC_VALUE"].ToString();
                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                if (noofdecimalplace != 6)
                                {
                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                }
                                if (noofdecimalplace == 0)
                                {
                                    tempStr = tempStr.TrimEnd('.');
                                }
                                dr["NUMERIC_VALUE"] = tempStr;
                                #endregion
                            }

                        }
                        if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
                        {
                            htmlString.Append("<TD ALIGN=\"" + alignVal + GetProductCellString(dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;"), userID));
                            htmlString.Append("<br><br/>");
                            htmlString.Append("<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + dr["OBJECT_NAME"] + "</label>");
                            htmlString.Append("</TD>");
                        }
                        else //if (chkAttrType[j] == 4)
                        {
                            string valueFortag = "";
                            if ((_headeroptions == "All") || (_headeroptions != "All"))
                            {
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null) &&
                                    ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                      dr["NUMERIC_VALUE"].ToString() == string.Empty) ||
                                     (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
                                      dr["STRING_VALUE"].ToString() == string.Empty)))
                                {
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().StartsWith("Number") &&
                                        dr["NUMERIC_VALUE"].ToString() == string.Empty)
                                        valueFortag = "Null";
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().StartsWith("Text") &&
                                        dr["STRING_VALUE"].ToString() == string.Empty)
                                        valueFortag = "Empty";

                                    valueFortag = (valueFortag == _emptyCondition) ? _replaceText : "";
                                }
                                else if ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                          dr["NUMERIC_VALUE"].ToString() == _emptyCondition) ||
                                         (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
                                          dr["STRING_VALUE"].ToString() == _emptyCondition))
                                {
                                    valueFortag = _replaceText;
                                }
                                else
                                {
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                    {
                                        if ((style != "") && (dt != 0.0))
                                            valueFortag = _prefix +
                                                          dt.ToString(style.Trim())
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")
                                                              .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        else
                                        {

                                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                                            {
                                                valueFortag = _replaceText;
                                            }
                                            else
                                            {
                                                valueFortag = _prefix +
                                                              dr["NUMERIC_VALUE"].ToString()
                                                                  .Replace("<", "&lt;")
                                                                  .Replace(">", "&gt;")
                                                                  .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (dr["STRING_VALUE"].ToString().Length > 0)
                                        {
                                            valueFortag = _prefix +
                                                          dr["STRING_VALUE"].ToString()
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")
                                                              .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                        else
                                        {
                                            valueFortag = string.Empty;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                {
                                    //valueFortag =  dt != 0.0 ? dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") : dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    if ((style != "") && (dt != 0.0))
                                        valueFortag =
                                            dt.ToString(style.Trim())
                                                .Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    else
                                        valueFortag =
                                            dr["NUMERIC_VALUE"].ToString()
                                                .Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    valueFortag =
                                        dr["STRING_VALUE"].ToString()
                                            .Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                            }

                            if (
                                dr["ATTRIBUTE_DATAFORMAT"].ToString()
                                    .StartsWith(
                                        "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                            {
                                if (
                                    dr["STRING_VALUE"].ToString()
                                        .Replace("_", "")
                                        .Replace("/", "")
                                        .Replace(":", "")
                                        .Trim()
                                        .Length != 0)
                                {
                                    valueFortag = dr["STRING_VALUE"].ToString();
                                    valueFortag = valueFortag.Substring(3, 2) + "/" + valueFortag.Substring(0, 2) + "/" +
                                                  valueFortag.Substring(6, 4);
                                }
                            }

                            htmlString.Append("<td align=\"" + alignVal +
                                              "\" valign=\"Middle\" style=\" color: Black; BACKGROUND-COLOR: white  \" >" +
                                              valueFortag + "</td></tr>");
                        }
                    }


                }


                htmlString.Append("</table>");
                htmlString.Append("</body>");
                htmlString.Append("</left>");

                //For attribute group
                var objAttributeDataSet = new DataSet();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = productId;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "PRODUCTATTRIBUTEGROUP";
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objAttributeDataSet);
                }
                string sFamilyName = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == familyId)
                    .Select(y => y.FAMILY_NAME).FirstOrDefault();

                var scatg = new StringBuilder();


                var sbHtmlTemp = new StringBuilder();


                int groupId = 0;

                var sbXml = new StringBuilder();
                bool familyLvelPreviewEnabled = productLevelMultipletablePreview;
                if (familyLvelPreviewEnabled)
                {
                    if (objAttributeDataSet.Tables[0].Rows.Count > 0)
                    {
                        //scatg.Append("<html><HEAD></HEAD><BODY><P style=\"font-size: x-medium; color: red; font-family: Verdana;\"> " + sCategoryName + " > " + sFamilyName + "</P></BODY></html>");
                        //htmlString.Append("<HTML>");
                        //htmlString.Append("<HEAD><style type='text/css'>.bdr_btm{border-bottom: 1px solid #D5D5D5;}.bdr_LB{border-bottom: 1px solid #D5D5D5;border-left: 1px solid #ACACAC;}</style>");
                        //htmlString.Append("</HEAD>");
                        //htmlString.Append("<BODY><br/><table class=\"table table-condensed table-bordered table-striped\"><tr><td colspan='3' align='center' style='font-family: Calibri; width: 100%;'><h4><b>PREVIEW WITH GROUPING</b></h4></td></tr>");
                        htmlString.Append("<table class=\"table table-condensed table-bordered table-striped\"><tr><td colspan='3' align='center' style='font-family: " + "Helvetica Neue" + ",Helvetica,Arial,sans-serif !important;; width: 100%;'><h4><b>PREVIEW WITH GROUPING</b></h4></td></tr>");

                        sbHtmlTemp.Append("<table class=\"table table-condensed table-bordered table-striped\"  style=\"width:50%\"><tr><td></td><td></td><td></td></tr>");
                        sbXml.Append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");
                        sbXml.Append("<tradingbell_root catalog_id=\"" + catalogId + "\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\">\n");
                        var catalogDetails = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(x => x).ToList();
                        foreach (var catalogDetail in catalogDetails)
                        {
                            sbXml.Append("<catalog_name  COTYPE=\"TEXT\" TBGUID=\"CN" + _randomClass.Next() + "\" aid:pstyle=\"catalog_name\"><![CDATA[" + catalogDetail.CATALOG_NAME + "]]></catalog_name>\n");
                            sbXml.Append("<version  COTYPE=\"TEXT\" TBGUID=\"CV" + _randomClass.Next() + "\" aid:pstyle=\"version\"><![CDATA[" + catalogDetail.VERSION + "]]></version>\n");
                            sbXml.Append("<description  COTYPE=\"TEXT\" TBGUID=\"CD" + _randomClass.Next() + "\" aid:pstyle=\"description\"><![CDATA[" + catalogDetail.DESCRIPTION + "]]></description>\n");
                        }

                        DataTable dtGroups = objAttributeDataSet.Tables[0].DefaultView.ToTable(true, "GROUP_ID");
                        sbXml.Append("<attribute_group xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\" tables=\"" + dtGroups.Rows.Count + "\"  COTYPE=\"\" TBGUID=\"M" + _randomClass.Next() + "\" aid:pstyle=\"attribute_group\">\n");
                        sbXml.Append("<TABLE TBGUID=\"TA" + _randomClass.Next() + "\" Format=\"SuperTable\" ncols=\"3\" nrows=\"" + objAttributeDataSet.Tables[0].Rows.Count + "\" aid:table=\"table\" aid:tcols=\"3\" aid:trows=\"" + objAttributeDataSet.Tables[0].Rows.Count + "\" aid5:tablestyle=\"TABLE\" Transpose=\"0\">");
                        foreach (DataRow dr in objAttributeDataSet.Tables[0].Rows)
                        {
                            if (groupId != Convert.ToInt32(dr["GROUP_ID"]))
                            {
                                //if (!string.IsNullOrEmpty(groupName))
                                //sbXML.Append("</" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                                groupId = Convert.ToInt32(dr["GROUP_ID"]);
                                htmlString.Append("<tr><td colspan='3' class='black_14_b bdr_btm'><strong>" + dr["GROUP_NAME"] + "</strong></td></tr>");
                                sbHtmlTemp.Append("<tr><td colspan='3' class='black_14_b bdr_btm'><strong>" + dr["GROUP_NAME"] + "</strong></td></tr>");
                                //sbXML.Append("<" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"\" group_id =\"" + dr["GROUP_ID"] + "\" TBGUID=\"C" + RandomClass.Next() + "\" aid:pstyle=\"attribute_group\">\n");
                                sbXml.Append("<group_name TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"group_name\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["GROUP_NAME"] + "]]></group_name>");
                            }
                            else
                            {
                                sbXml.Append("<group_name TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"group_name\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[]]></group_name>");
                            }
                            sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["ATTRIBUTE_NAME"] + "]]></Cell>");
                            if (string.IsNullOrEmpty(Convert.ToString(dr["NUMERIC_VALUE"])))
                            {
                                //sbXML.Append("<" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"TEXT\" TBGUID=\"AN" + RandomClass.Next() + "\" aid:pstyle=\"attribute_name\"><![CDATA[" + dr["STRING_VALUE"] + "]]></" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                                sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["STRING_VALUE"] + "]]></Cell>");
                            }
                            else
                            {
                                //sbXML.Append("<" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"TEXT\" TBGUID=\"AN" + RandomClass.Next() + "\" aid:pstyle=\"attribute_name\"><![CDATA[" + dr["NUMERIC_VALUE"] + "]]></" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                                sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["NUMERIC_VALUE"] + "]]></Cell>");
                            }
                            if (dr["ATTRIBUTE_TYPE"].ToString() != "6")
                            {
                                htmlString.Append("<tr><td width='100'>&nbsp;</td><td width='208' class='bdr_LB'><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                            }
                            else
                            {
                                htmlString.Append("<tr><td width='100'>&nbsp;</td><td width='208' class='bdr_LB' style=\"background-color:lightgreen;\"><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                            }


                            sbHtmlTemp.Append("<tr><td width='100'></td><td width='208' class='bdr_LB'><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                            const string alignVal = "Left";
                            string style = dr["STYLE_FORMAT"].ToString();
                            if (style.Length > 0)
                                style = style.Substring(0, style.IndexOf('[') - 1);
                            double dt = 0;
                            ExtractCurrenyFormat(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));

                            if ((dr["ATTRIBUTE_TYPE"].ToString() == "6") && (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM"))
                            {
                                if (dr["STRING_VALUE"].ToString().Trim().Length > 0)
                                {
                                    dt = Convert.ToDouble(dr["STRING_VALUE"].ToString());
                                }
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "4" || dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            {
                                if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
                                {
                                    dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
                                }
                            }
                            if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
                            {
                                htmlString.Append("<TD width='208' class='bdr_LB' ALIGN=\"" + alignVal + GetProductCellString(dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;"), userID));
                                htmlString.Append("<br><br/>");
                                htmlString.Append("<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + dr["OBJECT_NAME"] + "</label>");
                                htmlString.Append("</TD>");
                                sbHtmlTemp.Append("<TD width='208' class='bdr_LB' ALIGN=\"" + alignVal + GetProductCellString(dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;"), userID));
                                htmlString.Append("<br><br/>");
                                sbHtmlTemp.Append("<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + dr["OBJECT_NAME"] + "</label>");
                                sbHtmlTemp.Append("</TD>");
                            }
                            else  //if (chkAttrType[j] == 4)
                            {
                                string valueFortag;
                                if ((_headeroptions == "All") || (_headeroptions != "All"))
                                {
                                    if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null) &&
                                        ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" && dr["NUMERIC_VALUE"].ToString() == string.Empty && dr["ATTRIBUTE_TYPE"].ToString() != "6") ||
                                        (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" && dr["STRING_VALUE"].ToString() == string.Empty && dr["ATTRIBUTE_TYPE"].ToString() != "6")))
                                    {
                                        valueFortag = _replaceText;
                                    }
                                    else if ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" && dr["NUMERIC_VALUE"].ToString() == _emptyCondition && dr["ATTRIBUTE_TYPE"].ToString() != "6") ||
                                        (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" && dr["STRING_VALUE"].ToString() == _emptyCondition && dr["ATTRIBUTE_TYPE"].ToString() != "6"))
                                    {
                                        valueFortag = _replaceText;
                                    }
                                    else if (dr["STRING_VALUE"].ToString() == _emptyCondition && dr["ATTRIBUTE_TYPE"].ToString() == "6")
                                    {
                                        valueFortag = _replaceText;
                                    }
                                    else
                                    {
                                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                        {
                                            if ((style != "") && (dt != 0.0))
                                            {
                                                valueFortag = _prefix + dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                            else
                                            {
                                                valueFortag = _prefix + dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                        }
                                        else
                                        {
                                            if (dr["STRING_VALUE"].ToString().Length > 0)
                                            {
                                                valueFortag = _prefix + dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                            else
                                            {
                                                valueFortag = string.Empty;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                    {
                                        //valueFortag = dt != 0.0 ? dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") : dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        if ((style != "") && (dt != 0.0))
                                        {
                                            valueFortag = dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            valueFortag = dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                    }
                                    else
                                    {
                                        valueFortag = dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                }

                                if (dr["ATTRIBUTE_DATAFORMAT"].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                {
                                    if (dr["STRING_VALUE"].ToString().Replace("_", "").Replace("/", "").Replace(":", "").Trim().Length != 0)
                                    {
                                        valueFortag = dr["STRING_VALUE"].ToString();
                                        valueFortag = valueFortag.Substring(3, 2) + "/" + valueFortag.Substring(0, 2) + "/" + valueFortag.Substring(6, 4);
                                    }
                                }
                                if (valueFortag.EndsWith(".jpg"))
                                {
                                    //htmlString.Append("<img height='150' width='200' src='" + valueFortag + "' \\></td></tr>");
                                }
                                else
                                {
                                    htmlString.Append("<td width='208' class='bdr_LB'>" + valueFortag + "</td></tr>");
                                }
                                sbHtmlTemp.Append("<td width='208' class='bdr_LB'>" + valueFortag + "</td></tr>");
                            }
                        }
                        //sbXML.Append("</" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                        sbXml.Append("</TABLE>");
                        sbXml.Append("</attribute_group>");
                        sbXml.Append("</tradingbell_root>");

                        htmlString.Append("</table><br/>");
                        sbHtmlTemp.Append("</table><br/>");
                    }
                }
                htmlString.Append(LoadReferenceTable(familyId, productLevelMultipletablePreview, userID));
                //htmlString.Append("</body></html>");
                var encOutput = Encoding.UTF8;
                //string htmlPath = HttpContext.Current.Server.MapPath("~/Content/HTML");
                //string filePath = (htmlPath + "\\ProductPreview.html");
                //var fileHtml = new FileStream(filePath, FileMode.Create);
                //var strwriter = new StreamWriter(fileHtml, encOutput);
                htmlString = htmlString.Replace("&lt;br /&gt;", "<br>").Replace("&lt;/br&gt;", "<br>").Replace("&lt;br&gt;", "<br>").Replace("&lt;br/&gt;", "<br>");
                //htmlString = scatg.ToString() + htmlString.ToString();
                //strwriter.Write(scatg + htmlString.ToString());
                //strwriter.Close();
                //fileHtml.Close();
                //fileHtml = new FileStream(filePath, FileMode.Open);
                //var strReader = new StreamReader(fileHtml);
                string productpreview = scatg.ToString() + htmlString.ToString();
                ////strReader.Close();
                ////fileHtml.Close();
                //MemoryStream mStream = new MemoryStream(new ASCIIEncoding().GetBytes(HTMLStr));
                //  _saveFileStream = scatg.Append(htmlString);
                //webBrowser1.Navigate(filePath);
                //DataSet dsXMLFromHTML = ConvertHTMLTablesToDataSet(sbHTML_Temp.ToString());
                //dsXMLFromHTML.WriteXml("C:\\sampleee.xml");
                //FileStream fs = new FileStream("C:\\sample.xml", FileMode.Create, FileAccess.Write);
                //StreamWriter xmlwrite = new StreamWriter(fs);
                //xmlwrite.Write(sbXML);
                //xmlwrite.Close();

               

                if (HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {
                    var customerDetails = _dbcontext.Customers.Where(x => x.CustomerId == userID).Select(x => x.CustomizeSubItemNo).FirstOrDefault();

                    if (string.IsNullOrEmpty(customerDetails))
                    {
                        customerDetails = "SUBITEM#";
                    }
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerDetails;

                }



                productpreview = productpreview.Contains("ITEM#") ? productpreview.Replace("ITEM#", HttpContext.Current.Session["CustomerSubItemNo"].ToString()) : productpreview;
                return productpreview;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ProductPreview : GenerateSubProductGroupPreview", objexception);
                return string.Empty;
            }
        }
        public string GenerateSubProductFamilyPreview(int catalogId, int familyId, int productId, bool productLevelMultipletablePreview, int userID, string categoryId)
        {
            try
            {
                var htmlString = new StringBuilder();
                var objDataSet = new DataSet();
                using (
                    var objSqlConnection =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = productId;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 20).Value = "SUBPRODUCTPREVIEW";
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataSet);
                }

                var imagemagickpath = _dbcontext.Customer_Settings.FirstOrDefault(a => a.CustomerId == userID);
                //if (imagemagickpath != null)
                //{
                //    _imageMagickPath = imagemagickpath.ImageConversionUtilityPath;
                //    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                //}
                //else
                //{
                //    _imageMagickPath = "C:\\Program Files\\ImageMagick-6.8.5-Q8";
                //}
                string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == userID).Select(a => a.ConverterType).SingleOrDefault();

                if (imagemagickpath != null && converttype == "Image Magic")
                {
                    _imageMagickPath = convertionPath;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                }
                else
                {
                    _imageMagickPath = convertionPathREA;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");

                }

                htmlString.Append("<html>");
                htmlString.Append("<left>");
                htmlString.Append("<head>");
                htmlString.Append("</head>");
                htmlString.Append("<body>");
                htmlString.Append(
                    "<table id=\"sampleProdgrid1\"  class=\"table table-bordered table-striped table-hover\" style=\"background-color:white;border-radius:9px\" >");
                // htmlString.Append("<style>td{font-family:arial Unicode ms;font-size:12px;}th{font-family:arial unicode ms;font-size:12px;font-weight:Bold}</style>");

                if (objDataSet.Tables.Count > 0)
                {
                    bool prodid = false;
                    htmlString.Append("<tr>");
                    var drRow =
                        objDataSet.Tables[0].AsEnumerable().Select(x => new
                        {
                            Attribute_id = x.Field<int>("ATTRIBUTE_ID"),
                            Attribute_Type = x.Field<byte>("ATTRIBUTE_TYPE"),
                            Attribute_Name = x.Field<string>("ATTRIBUTE_NAME")

                        }).ToArray().Distinct();
                    foreach (var dr in drRow)
                    {
                        //var attrName = dr.Attribute_Name;
                        //var att = dr.Attribute_Type;
                        //var attid = dr.Attribute_id;
                        //if (dr["ATTRIBUTE_ID"].ToString() == "1" && prodid == true)
                        //{
                        //    break;
                        //}
                        //if (dr["ATTRIBUTE_ID"].ToString() == "1" && prodid == false)
                        //{
                        //    prodid = true;
                        //}


                        if (dr.Attribute_Type.ToString() != "6")
                        {
                            htmlString.Append(
                                "<td align=\"left\" style=\" color: #003f59;; BACKGROUND-COLOR: aliceblue  \"><b>" +
                                dr.Attribute_Name + "</b></td>");
                        }
                        else
                        {
                            htmlString.Append("<td align=\"left\" style=\"background-color:lightgreen;\"><b>" +
                                              dr.Attribute_Name + "</b></td>");
                        }

                    }
                    htmlString.Append("</tr>");
                    prodid = false;

                    foreach (DataRow dr in objDataSet.Tables[0].Rows)
                    {

                        if (dr["ATTRIBUTE_ID"].ToString() == "1")
                        {
                            htmlString.Append("<tr>");
                        }
                        const string alignVal = "Left";
                        string style = dr["STYLE_FORMAT"].ToString();
                        if (style.Length > 0)
                            style = style.Substring(0, style.IndexOf('[') - 1);
                        double dt = 0;
                        ExtractCurrenyFormat(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));

                        if (dr["ATTRIBUTE_TYPE"].ToString() == "6")
                        {

                        }
                        if (dr["ATTRIBUTE_TYPE"].ToString() == "6" &&
                            dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        {
                            if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
                            {
                                //dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());

                                #region "Decimal Place Trimming"

                                string tempStr = dr["NUMERIC_VALUE"].ToString();
                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                if (noofdecimalplace != 6)
                                {
                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                }
                                if (noofdecimalplace == 0)
                                {
                                    tempStr = tempStr.TrimEnd('.');
                                }
                                dr["NUMERIC_VALUE"] = tempStr;

                                #endregion
                            }


                        }
                        else if (dr["ATTRIBUTE_TYPE"].ToString() == "4" ||
                                 dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        {
                            if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
                            {
                                //dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());

                                #region "Decimal Place Trimming"

                                string tempStr = dr["NUMERIC_VALUE"].ToString();
                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                if (noofdecimalplace != 6)
                                {
                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                }
                                if (noofdecimalplace == 0)
                                {
                                    tempStr = tempStr.TrimEnd('.');
                                }
                                dr["NUMERIC_VALUE"] = tempStr;

                                #endregion
                            }

                        }
                        if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
                        {
                            htmlString.Append("<TD ALIGN=\"" + alignVal +
                                              GetProductCellString(
                                                  dr["STRING_VALUE"].ToString()
                                                      .Replace("<", "&lt;")
                                                      .Replace(">", "&gt;"), userID));
                            htmlString.Append("<br><br/>");
                            htmlString.Append(
                                "<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" +
                                dr["OBJECT_NAME"] + "</label>");
                            htmlString.Append("</TD>");
                        }
                        else //if (chkAttrType[j] == 4)
                        {
                            string valueFortag = "";
                            if ((_headeroptions == "All") || (_headeroptions != "All"))
                            {
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null) &&
                                    ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                      dr["NUMERIC_VALUE"].ToString() == string.Empty) ||
                                     (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
                                      dr["STRING_VALUE"].ToString() == string.Empty)))
                                {
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().StartsWith("Number") &&
                                        dr["NUMERIC_VALUE"].ToString() == string.Empty)
                                        valueFortag = "Null";
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().StartsWith("Text") &&
                                        dr["STRING_VALUE"].ToString() == string.Empty)
                                        valueFortag = "Empty";

                                    valueFortag = (valueFortag == _emptyCondition) ? _replaceText : "";
                                }
                                else if ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                          dr["NUMERIC_VALUE"].ToString() == _emptyCondition) ||
                                         (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
                                          dr["STRING_VALUE"].ToString() == _emptyCondition))
                                {
                                    valueFortag = _replaceText;
                                }
                                else
                                {
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                    {
                                        if ((style != "") && (dt != 0.0))
                                            valueFortag = _prefix +
                                                          dt.ToString(style.Trim())
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")

                                                              .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                          _suffix;
                                        else
                                            valueFortag = _prefix +
                                                          dr["NUMERIC_VALUE"].ToString()
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")

                                                              .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                          _suffix;
                                    }
                                    else
                                    {
                                        if (dr["STRING_VALUE"].ToString().Length > 0)
                                        {
                                            valueFortag = _prefix +
                                                          dr["STRING_VALUE"].ToString()
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")
                                                              .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                          _suffix;
                                        }
                                        else
                                        {
                                            valueFortag = string.Empty;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                {
                                    //valueFortag =  dt != 0.0 ? dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") : dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    if ((style != "") && (dt != 0.0))
                                        valueFortag =
                                            dt.ToString(style.Trim())
                                                .Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    else
                                        valueFortag =
                                            dr["NUMERIC_VALUE"].ToString()
                                                .Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    valueFortag =
                                        dr["STRING_VALUE"].ToString()
                                            .Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                            }

                            if (
                                dr["ATTRIBUTE_DATAFORMAT"].ToString()
                                    .StartsWith(
                                        "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                            {
                                if (
                                    dr["STRING_VALUE"].ToString()
                                        .Replace("_", "")
                                        .Replace("/", "")
                                        .Replace(":", "")
                                        .Trim()
                                        .Length != 0)
                                {
                                    valueFortag = dr["STRING_VALUE"].ToString();
                                    valueFortag = valueFortag.Substring(3, 2) + "/" + valueFortag.Substring(0, 2) + "/" +
                                                  valueFortag.Substring(6, 4);
                                }
                            }

                            htmlString.Append("<td align=\"" + alignVal +
                                              "\" valign=\"Middle\" style=\" color: Black; BACKGROUND-COLOR: white  \" >" +
                                              valueFortag + "</td>");
                        }
                    }


                }


                htmlString.Append("</table>");
                htmlString.Append("</body>");
                htmlString.Append("</left>");

                //For attribute group
                var objAttributeDataSet = new DataSet();
                using (
                    var objSqlConnection =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = productId;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "PRODUCTATTRIBUTEGROUP";
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objAttributeDataSet);
                }

                string sFamilyName = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == familyId)
                    .Select(y => y.FAMILY_NAME).FirstOrDefault();

                var scatg = new StringBuilder();


                var sbHtmlTemp = new StringBuilder();


                int groupId = 0;

                var sbXml = new StringBuilder();
                bool familyLvelPreviewEnabled = productLevelMultipletablePreview;
                if (familyLvelPreviewEnabled)
                {
                    if (objAttributeDataSet.Tables[0].Rows.Count > 0)
                    {
                        //scatg.Append("<html><HEAD></HEAD><BODY><P style=\"font-size: x-medium; color: red; font-family: Verdana;\"> " + sCategoryName + " > " + sFamilyName + "</P></BODY></html>");
                        //htmlString.Append("<HTML>");
                        //htmlString.Append("<HEAD><style type='text/css'>.bdr_btm{border-bottom: 1px solid #D5D5D5;}.bdr_LB{border-bottom: 1px solid #D5D5D5;border-left: 1px solid #ACACAC;}</style>");
                        //htmlString.Append("</HEAD>");
                        //htmlString.Append("<BODY><br/><table class=\"table table-condensed table-bordered table-striped\"><tr><td colspan='3' align='center' style='font-family: Calibri; width: 100%;'><h4><b>PREVIEW WITH GROUPING</b></h4></td></tr>");
                        htmlString.Append(
                            "<table class=\"table table-condensed table-bordered table-striped\"><tr><td colspan='3' align='center' style='font-family: " +
                            "Helvetica Neue" +
                            ",Helvetica,Arial,sans-serif !important;; width: 100%;'><h4><b>PREVIEW WITH GROUPING</b></h4></td></tr>");

                        sbHtmlTemp.Append(
                            "<table class=\"table table-condensed table-bordered table-striped\"  style=\"width:50%\"><tr><td></td><td></td><td></td></tr>");
                        sbXml.Append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");
                        sbXml.Append("<tradingbell_root catalog_id=\"" + catalogId +
                                     "\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\">\n");
                        var catalogDetails =
                            _dbcontext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(x => x).ToList();
                        foreach (var catalogDetail in catalogDetails)
                        {
                            sbXml.Append("<catalog_name  COTYPE=\"TEXT\" TBGUID=\"CN" + _randomClass.Next() +
                                         "\" aid:pstyle=\"catalog_name\"><![CDATA[" + catalogDetail.CATALOG_NAME +
                                         "]]></catalog_name>\n");
                            sbXml.Append("<version  COTYPE=\"TEXT\" TBGUID=\"CV" + _randomClass.Next() +
                                         "\" aid:pstyle=\"version\"><![CDATA[" + catalogDetail.VERSION +
                                         "]]></version>\n");
                            sbXml.Append("<description  COTYPE=\"TEXT\" TBGUID=\"CD" + _randomClass.Next() +
                                         "\" aid:pstyle=\"description\"><![CDATA[" + catalogDetail.DESCRIPTION +
                                         "]]></description>\n");
                        }

                        DataTable dtGroups = objAttributeDataSet.Tables[0].DefaultView.ToTable(true, "GROUP_ID");
                        sbXml.Append(
                            "<attribute_group xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\" tables=\"" +
                            dtGroups.Rows.Count + "\"  COTYPE=\"\" TBGUID=\"M" + _randomClass.Next() +
                            "\" aid:pstyle=\"attribute_group\">\n");
                        sbXml.Append("<TABLE TBGUID=\"TA" + _randomClass.Next() +
                                     "\" Format=\"SuperTable\" ncols=\"3\" nrows=\"" +
                                     objAttributeDataSet.Tables[0].Rows.Count +
                                     "\" aid:table=\"table\" aid:tcols=\"3\" aid:trows=\"" +
                                     objAttributeDataSet.Tables[0].Rows.Count +
                                     "\" aid5:tablestyle=\"TABLE\" Transpose=\"0\">");
                        foreach (DataRow dr in objAttributeDataSet.Tables[0].Rows)
                        {
                            if (groupId != Convert.ToInt32(dr["GROUP_ID"]))
                            {
                                //if (!string.IsNullOrEmpty(groupName))
                                //sbXML.Append("</" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                                groupId = Convert.ToInt32(dr["GROUP_ID"]);
                                htmlString.Append("<tr><td colspan='3' class='black_14_b bdr_btm'><strong>" +
                                                  dr["GROUP_NAME"] + "</strong></td></tr>");
                                sbHtmlTemp.Append("<tr><td colspan='3' class='black_14_b bdr_btm'><strong>" +
                                                  dr["GROUP_NAME"] + "</strong></td></tr>");
                                //sbXML.Append("<" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"\" group_id =\"" + dr["GROUP_ID"] + "\" TBGUID=\"C" + RandomClass.Next() + "\" aid:pstyle=\"attribute_group\">\n");
                                sbXml.Append("<group_name TBGUID=\"TPS" + _randomClass.Next() +
                                             "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"group_name\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" +
                                             dr["GROUP_NAME"] + "]]></group_name>");
                            }
                            else
                            {
                                sbXml.Append("<group_name TBGUID=\"TPS" + _randomClass.Next() +
                                             "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"group_name\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[]]></group_name>");
                            }
                            sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() +
                                         "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" +
                                         dr["ATTRIBUTE_NAME"] + "]]></Cell>");
                            if (string.IsNullOrEmpty(Convert.ToString(dr["NUMERIC_VALUE"])))
                            {
                                //sbXML.Append("<" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"TEXT\" TBGUID=\"AN" + RandomClass.Next() + "\" aid:pstyle=\"attribute_name\"><![CDATA[" + dr["STRING_VALUE"] + "]]></" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                                sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() +
                                             "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" +
                                             dr["STRING_VALUE"] + "]]></Cell>");
                            }
                            else
                            {
                                //sbXML.Append("<" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"TEXT\" TBGUID=\"AN" + RandomClass.Next() + "\" aid:pstyle=\"attribute_name\"><![CDATA[" + dr["NUMERIC_VALUE"] + "]]></" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                                sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() +
                                             "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" +
                                             dr["NUMERIC_VALUE"] + "]]></Cell>");
                            }
                            if (dr["ATTRIBUTE_TYPE"].ToString() != "6")
                            {
                                htmlString.Append("<tr><td width='100'>&nbsp;</td><td width='208' class='bdr_LB'>" +
                                                  dr["ATTRIBUTE_NAME"] + "</td>");
                            }
                            else
                            {
                                htmlString.Append(
                                    "<tr><td width='100'>&nbsp;</td><td width='208' class='bdr_LB' style=\"background-color:lightgreen;\">" +
                                    dr["ATTRIBUTE_NAME"] + "</td>");
                            }


                            sbHtmlTemp.Append("<tr><td width='100'></td><td width='208' class='bdr_LB'>" +
                                              dr["ATTRIBUTE_NAME"] + "</td>");
                            const string alignVal = "Left";
                            string style = dr["STYLE_FORMAT"].ToString();
                            if (style.Length > 0)
                                style = style.Substring(0, style.IndexOf('[') - 1);
                            double dt = 0;
                            ExtractCurrenyFormat(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));

                            if ((dr["ATTRIBUTE_TYPE"].ToString() == "6") &&
                                (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM"))
                            {
                                if (dr["STRING_VALUE"].ToString().Trim().Length > 0)
                                {
                                    dt = Convert.ToDouble(dr["STRING_VALUE"].ToString());
                                }
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "4" ||
                                     dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            {
                                if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
                                {
                                    dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
                                }
                            }
                            if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
                            {
                                htmlString.Append("<TD width='208' class='bdr_LB' ALIGN=\"" + alignVal +
                                                  GetProductCellString(
                                                      dr["STRING_VALUE"].ToString()
                                                          .Replace("<", "&lt;")
                                                          .Replace(">", "&gt;"), userID));
                                htmlString.Append("<br><br/>");
                                htmlString.Append(
                                    "<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" +
                                    dr["OBJECT_NAME"] + "</label>");
                                htmlString.Append("</TD>");
                                sbHtmlTemp.Append("<TD width='208' class='bdr_LB' ALIGN=\"" + alignVal +
                                                  GetProductCellString(
                                                      dr["STRING_VALUE"].ToString()
                                                          .Replace("<", "&lt;")
                                                          .Replace(">", "&gt;"), userID));
                                htmlString.Append("<br><br/>");
                                sbHtmlTemp.Append(
                                    "<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" +
                                    dr["OBJECT_NAME"] + "</label>");
                                sbHtmlTemp.Append("</TD>");
                            }
                            else //if (chkAttrType[j] == 4)
                            {
                                string valueFortag;
                                if ((_headeroptions == "All") || (_headeroptions != "All"))
                                {
                                    if ((_emptyCondition == "Null" || _emptyCondition == "Empty" ||
                                         _emptyCondition == null) &&
                                        ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                          dr["NUMERIC_VALUE"].ToString() == string.Empty &&
                                          dr["ATTRIBUTE_TYPE"].ToString() != "6") ||
                                         (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
                                          dr["STRING_VALUE"].ToString() == string.Empty &&
                                          dr["ATTRIBUTE_TYPE"].ToString() != "6")))
                                    {
                                        valueFortag = _replaceText;
                                    }
                                    else if ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                              dr["NUMERIC_VALUE"].ToString() == _emptyCondition &&
                                              dr["ATTRIBUTE_TYPE"].ToString() != "6") ||
                                             (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
                                              dr["STRING_VALUE"].ToString() == _emptyCondition &&
                                              dr["ATTRIBUTE_TYPE"].ToString() != "6"))
                                    {
                                        valueFortag = _replaceText;
                                    }
                                    else if (dr["STRING_VALUE"].ToString() == _emptyCondition &&
                                             dr["ATTRIBUTE_TYPE"].ToString() == "6")
                                    {
                                        valueFortag = _replaceText;
                                    }
                                    else
                                    {
                                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() ==
                                            "NUM")
                                        {
                                            if ((style != "") && (dt != 0.0))
                                            {
                                                valueFortag = _prefix +
                                                              dt.ToString(style.Trim())
                                                                  .Replace("<", "&lt;")
                                                                  .Replace(">", "&gt;")
                                                                  .Replace("&lt;p&gt;", "<p>")
                                                                  .Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                            else
                                            {
                                                valueFortag = _prefix +
                                                              dr["NUMERIC_VALUE"].ToString()
                                                                  .Replace("<", "&lt;")
                                                                  .Replace(">", "&gt;")
                                                                  .Replace("&lt;p&gt;", "<p>")
                                                                  .Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                        }
                                        else
                                        {
                                            if (dr["STRING_VALUE"].ToString().Length > 0)
                                            {
                                                valueFortag = _prefix +
                                                              dr["STRING_VALUE"].ToString()
                                                                  .Replace("<", "&lt;")
                                                                  .Replace(">", "&gt;")
                                                                  .Replace("&lt;p&gt;", "<p>")
                                                                  .Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                            else
                                            {
                                                valueFortag = string.Empty;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                    {
                                        //valueFortag = dt != 0.0 ? dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") : dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        if ((style != "") && (dt != 0.0))
                                        {
                                            valueFortag =
                                                dt.ToString(style.Trim())
                                                    .Replace("<", "&lt;")
                                                    .Replace(">", "&gt;")
                                                    .Replace("&lt;p&gt;", "<p>")
                                                    .Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            valueFortag =
                                                dr["NUMERIC_VALUE"].ToString()
                                                    .Replace("<", "&lt;")
                                                    .Replace(">", "&gt;")
                                                    .Replace("&lt;p&gt;", "<p>")
                                                    .Replace("&lt;/p&gt;", "</p>");
                                        }
                                    }
                                    else
                                    {
                                        valueFortag =
                                            dr["STRING_VALUE"].ToString()
                                                .Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("&lt;p&gt;", "<p>")
                                                .Replace("&lt;/p&gt;", "</p>");
                                    }
                                }

                                if (
                                    dr["ATTRIBUTE_DATAFORMAT"].ToString()
                                        .StartsWith(
                                            "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                {
                                    if (
                                        dr["STRING_VALUE"].ToString()
                                            .Replace("_", "")
                                            .Replace("/", "")
                                            .Replace(":", "")
                                            .Trim()
                                            .Length != 0)
                                    {
                                        valueFortag = dr["STRING_VALUE"].ToString();
                                        valueFortag = valueFortag.Substring(3, 2) + "/" + valueFortag.Substring(0, 2) +
                                                      "/" + valueFortag.Substring(6, 4);
                                    }
                                }
                                if (valueFortag.EndsWith(".jpg"))
                                {
                                    //htmlString.Append("<img height='150' width='200' src='" + valueFortag + "' \\></td></tr>");
                                }
                                else
                                {
                                    htmlString.Append("<td width='208' class='bdr_LB'>" + valueFortag + "</td></tr>");
                                }
                                sbHtmlTemp.Append("<td width='208' class='bdr_LB'>" + valueFortag + "</td></tr>");
                            }
                        }
                        //sbXML.Append("</" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                        sbXml.Append("</TABLE>");
                        sbXml.Append("</attribute_group>");
                        sbXml.Append("</tradingbell_root>");

                        htmlString.Append("</table><br/>");
                        sbHtmlTemp.Append("</table><br/>");
                    }
                }
                htmlString.Append(LoadReferenceTable(familyId, productLevelMultipletablePreview, userID));
                //htmlString.Append("</body></html>");
                //var encOutput = Encoding.UTF8;
                //string htmlPath = HttpContext.Current.Server.MapPath("~/Content/HTML");
                //string filePath = (htmlPath + "\\ProductPreview.html");
                //var fileHtml = new FileStream(filePath, FileMode.Create);
                //var strwriter = new StreamWriter(fileHtml, encOutput);
                htmlString =
                    htmlString.Replace("&lt;br /&gt;", "<br>")
                        .Replace("&lt;/br&gt;", "<br>")
                        .Replace("&lt;br&gt;", "<br>")
                        .Replace("&lt;br/&gt;", "<br>");
                //strwriter.Write(scatg + htmlString.ToString());
                //strwriter.Close();
                //fileHtml.Close();
                //fileHtml = new FileStream(filePath, FileMode.Open);
                //var strReader = new StreamReader(fileHtml);
                string productpreview = scatg + htmlString.ToString();
                //strReader.Close();
                //fileHtml.Close();
                //MemoryStream mStream = new MemoryStream(new ASCIIEncoding().GetBytes(HTMLStr));
                //  _saveFileStream = scatg.Append(htmlString);
                //webBrowser1.Navigate(filePath);
                //DataSet dsXMLFromHTML = ConvertHTMLTablesToDataSet(sbHTML_Temp.ToString());
                //dsXMLFromHTML.WriteXml("C:\\sampleee.xml");
                //FileStream fs = new FileStream("C:\\sample.xml", FileMode.Create, FileAccess.Write);
                //StreamWriter xmlwrite = new StreamWriter(fs);
                //xmlwrite.Write(sbXML);
                //xmlwrite.Close();

                if (HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {
                    var customerDetails = _dbcontext.Customers.Where(x => x.CustomerId == userID).Select(x => x.CustomizeSubItemNo).FirstOrDefault();

                    if (string.IsNullOrEmpty(customerDetails))
                    {
                        customerDetails = "SUBITEM#";
                    }
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerDetails;

                }


                productpreview = productpreview.Contains("ITEM#") ? productpreview.Replace("ITEM#", HttpContext.Current.Session["CustomerSubItemNo"].ToString()) : productpreview;

                return productpreview;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ProductPreview : GenerateSubProductFamilyPreview", objexception);
                return string.Empty;
            }
        }
        private string LoadReferenceTable(int familyId, bool multipletablePreview, int userId)
        {
            try
            {
                CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.a.CustomerId == userId).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }

                // SystemSettingsCollection SettingMembers = SystemSettingsConfiguration.GetConfig.Members;
                string finalResult = string.Empty;
                //System.Drawing.Image _chkSize;
                string imageSizeW = "";
                string imageSizeH = "";
                bool familyLvelPreviewEnabled = multipletablePreview;
                if (familyLvelPreviewEnabled)
                {
                    var referenceTableName = new List<string>();

                    // _ocon.SQLString = "SELECT A.TABLE_ID,B.TABLE_NAME FROM TB_REFERENCE_SECTIONS A 
                    //JOIN TB_REFERENCE_TABLE B ON A.TABLE_ID = B.TABLE_ID AND A.FAMILY_ID = " + familyId + "";
                    // DataSet dsReferenceTable = _ocon.CreateDataSet();
                    var dsReferenceTable =
                        _dbcontext.TB_REFERENCE_SECTIONS.Where(x => x.FAMILY_ID == familyId).Select(x => x).ToList();
                    foreach (var tbReferenceSections in dsReferenceTable)
                    {

                        finalResult += "<br><br><font face=\"Arial Unicode MS\" size=2px ><b>Reference Contents</b></font>";

                        if (!referenceTableName.Contains(tbReferenceSections.TB_REFERENCE_TABLE.TABLE_NAME))
                        {
                            referenceTableName.Add(tbReferenceSections.TB_REFERENCE_TABLE.TABLE_NAME);
                            DataSet dsValues = new DataSet();
                            using (
                                var objSqlConnection =
                                    new SqlConnection(
                                        ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                            {
                                var objSqlCommand = objSqlConnection.CreateCommand();
                                objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ReferenceTable";
                                objSqlCommand.CommandType = CommandType.StoredProcedure;
                                objSqlCommand.Connection = objSqlConnection;
                                objSqlCommand.Parameters.Add("@TABLE_ID", SqlDbType.BigInt).Value = tbReferenceSections.TABLE_ID;
                                objSqlConnection.Open();
                                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                objSqlDataAdapter.Fill(dsValues);
                            }
                            //  _ocon.SQLString = "EXEC STP_CATALOGSTUDIO5_ReferenceTable " + Convert.ToString(item["TABLE_ID"]) + "";
                            //  DataSet dsValues = _ocon.CreateDataSet();
                            finalResult += "<br/><br/><b>" + tbReferenceSections.TB_REFERENCE_TABLE.TABLE_NAME + " : </b><br/><br/>";
                            finalResult +=
                                "<table class=\"table table-condensed table-bordered table-striped\" >";
                            if (dsValues.Tables.Count > 0)
                            {
                                foreach (DataRow item1 in dsValues.Tables[0].Rows)
                                {
                                    finalResult += "<tr>";
                                    for (int k = 1; k < dsValues.Tables[0].Columns.Count; k++)
                                    {
                                        finalResult += "<td class='reg'>";
                                        string value = Convert.ToString(item1[k]);
                                        if (value.ToLower().EndsWith(".jpg") || value.ToLower().EndsWith(".bmp") ||
                                            value.ToLower().EndsWith(".gif") ||
                                                 value.ToLower().EndsWith(".png"))
                                        {
                                            string referenceTableImagePath = Convert.ToString(ConfigurationManager.AppSettings["ReferenceTableImagePath"]);
                                            var chkFileVal = new FileInfo(referenceTableImagePath + value);
                                            if (chkFileVal.Exists)
                                            {
                                                _chkSize = Image.FromFile(referenceTableImagePath + value);
                                                Double iHeight = _chkSize.Height;
                                                Double iWidth = _chkSize.Width;
                                                Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                imageSizeH = Convert.ToString(newVal.Height);
                                                imageSizeW = Convert.ToString(newVal.Width);
                                                if (_chkSize != null)
                                                {
                                                    _chkSize.Dispose();
                                                }
                                            }
                                            finalResult += "<img width='" + imageSizeW + "' height='" + imageSizeH +
                                                           "' src='" + referenceTableImagePath + value + "' />";
                                        }
                                        else if (value.ToLower().EndsWith(".eps") ||
                                                 value.ToLower().EndsWith(".svg") ||
                                                 value.ToLower().EndsWith(".tif") ||
                                                 value.ToLower().EndsWith(".tiff") ||
                                                 value.ToLower().EndsWith(".psd") ||
                                                 value.ToLower().EndsWith(".tga") ||
                                                 value.ToLower().EndsWith(".pcx"))
                                        {
                                            if (
                                                Directory.Exists(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder)))
                                            {
                                                string[] imgargs =
                                                    {
                                                       HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) +
                                                        value,
                                                        HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" +
                                                        value.Substring(value.LastIndexOf('\\'),
                                                            value.LastIndexOf('.') - value.LastIndexOf('\\')) + ".jpg"
                                                    };

                                                string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == userId).Select(a => a.ConverterType).SingleOrDefault();
                                                if (converttype == "Image Magic")
                                                {
                                                    Exefile = "convert.exe";
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(_imageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }
                                                }
                                                else
                                                {
                                                    Exefile = "cons_rcp.exe";
                                                    // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"")
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(_imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }
                                                }
                                            }
                                            var chkFileVal =
                                                new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" +
                                                             value.Substring(value.LastIndexOf('\\'),
                                                                 value.LastIndexOf('.') - value.LastIndexOf('\\')) +
                                                             ".jpg");
                                            if (chkFileVal.Exists)
                                            {
                                                _chkSize =
                                                    Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" +
                                                                   value.Substring(value.LastIndexOf('\\'),
                                                                       value.LastIndexOf('.') - value.LastIndexOf('\\')) +
                                                                   ".jpg");
                                                Double iHeight = _chkSize.Height;
                                                Double iWidth = _chkSize.Width;
                                                Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                imageSizeH = Convert.ToString(newVal.Height);
                                                imageSizeW = Convert.ToString(newVal.Width);
                                                if (_chkSize != null)
                                                {
                                                    _chkSize.Dispose();
                                                }
                                            }
                                            finalResult += "<img width='" + imageSizeW + "' height='" + imageSizeH +
                                                           "' src='" + HttpContext.Current.Server.MapPath("~/Content/") +
                                                           "\\temp\\ImageandAttFile" +
                                                           value.Substring(value.LastIndexOf('\\'),
                                                               value.LastIndexOf('.') - value.LastIndexOf('\\')) +
                                                           ".jpg' \\>";
                                        }
                                        else
                                        {
                                            finalResult += string.IsNullOrEmpty(value) ? "" : value;
                                            //finalResult = finalResult;
                                        }
                                        finalResult += "</td>";
                                    }
                                    finalResult += "</tr>";
                                }
                            }
                            else
                            {
                                finalResult += "<tr>";
                                finalResult += "<td td class='reg'><em>There is no associated values in this table</em>";
                                finalResult += "</td>";
                                finalResult += "</tr>";
                            }
                            finalResult += "</table>";

                        }
                    }
                }
                return finalResult;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ProductPreview : LoadReferenceTable", objexception);
                return null;
            }
        }

        private void ExtractCurrenyFormat(int attributeId)
        {
            try
            {
                string attributeDataRule = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId).Select(y => y.ATTRIBUTE_DATARULE).FirstOrDefault();
                _prefix = string.Empty;
                _suffix = string.Empty;
                _emptyCondition = string.Empty;
                _replaceText = string.Empty;
                _headeroptions = string.Empty;
                if (!string.IsNullOrEmpty(attributeDataRule))
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(attributeDataRule);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    {
                        if (rootNode != null)
                        {
                            XmlNodeList xmlNodeList = rootNode.ChildNodes;

                            for (int xmlNode = 0; xmlNode < xmlNodeList.Count; xmlNode++)
                            {
                                if (xmlNodeList[xmlNode].ChildNodes.Count > 0)
                                {
                                    if (xmlNodeList[xmlNode].ChildNodes[0].LastChild != null)
                                    {
                                        _prefix = xmlNodeList[xmlNode].ChildNodes[0].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[1].LastChild != null)
                                    {
                                        _suffix = xmlNodeList[xmlNode].ChildNodes[1].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[2].LastChild != null)
                                    {
                                        _emptyCondition = xmlNodeList[xmlNode].ChildNodes[2].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[3].LastChild != null)
                                    {
                                        _replaceText = xmlNodeList[xmlNode].ChildNodes[3].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[4].LastChild != null)
                                    {
                                        _headeroptions = xmlNodeList[xmlNode].ChildNodes[4].LastChild.Value;
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ProductPreview : ExtractCurrenyFormat", objexception);
                // return null;
            }
        }

        private string GetProductCellString(string cellData, int userId)
        {
            // To pass dynamic path location from web config   - Start
            string IsServerUpload = WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
            string pathDesignation = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
            string pathUrl = WebConfigurationManager.AppSettings["ExternalAssetDriveURL"].ToString();
            string destinationPath;
            string generatedUrl;




            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.a.CustomerId == userId).Select(x => x.a.Comments).FirstOrDefault();
            if (string.IsNullOrEmpty(CustomerFolder))
            { CustomerFolder = "/STUDIOSOFT"; }
            else
            { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }


            if (IsServerUpload.ToUpper() == "TRUE")
            {
                destinationPath = pathDesignation + "//Content//ProductImages" + CustomerFolder;

                pathUrl = pathUrl.Replace("\\", "//");
                generatedUrl = pathUrl + "Content/ProductImages";

            }
            else
            {
                destinationPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder);

                generatedUrl =  "..//Content//ProductImages//";
            }

            //  To pass dynamic path location from web config  -  End.

            if (!Directory.Exists(destinationPath))
            {
                Directory.CreateDirectory(destinationPath);
            }



            string imageUrl = "";
            string imageSizeW = "";
            string imageSizeH = "";
            string returnstr = "";
            string imageType = "";
            var chkFile = new FileInfo(destinationPath+ cellData); //"~/Content/ProductImages" + CustomerFolder + "/Images"
            if (chkFile.Exists)
            {
                imageType = "";
                if (chkFile.Extension.ToUpper() == ".EPS" ||
                    chkFile.Extension.ToUpper() == ".SVG" ||
                    chkFile.Extension.ToUpper() == ".TIF" ||
                    chkFile.Extension.ToUpper() == ".TIFF" ||
                     chkFile.Extension.ToUpper() == ".TGA" ||
                    chkFile.Extension.ToUpper() == ".PCX")
                {
                    string[] imgargs = {HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + cellData,
                                 HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.'))+ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg" };



                    string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == userId).Select(a => a.ConverterType).SingleOrDefault();
                    if (converttype == "Image Magic")
                    {
                        Exefile = "convert.exe";
                        var pStart = new System.Diagnostics.ProcessStartInfo(_imageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                        pStart.CreateNoWindow = true;
                        pStart.UseShellExecute = false;
                        pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                        while (!cmdProcess.HasExited)
                        {
                        }
                    }
                    else
                    {
                        Exefile = "cons_rcp.exe";
                        // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"")
                        var pStart = new System.Diagnostics.ProcessStartInfo(_imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                        pStart.CreateNoWindow = true;
                        pStart.UseShellExecute = false;
                        pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                        while (!cmdProcess.HasExited)
                        {
                        }
                    }
                    //object[] imgargs = {(SettingMembers.GetValue(SystemSettingsCollection.SettingsList.USERIMAGEPATH.ToString())) + CellData,
                    //                                Application.StartupPath  + "\\temp\\ImageandAttFile" + chkFile.Name.ToString().Substring(0, chkFile.Name.ToString().LastIndexOf('.'))+valInc.ToString() + ".jpg" };
                    //MagickImageClass MagickImageClassRef = new MagickImageClass();
                    //try
                    //{
                    //    MagickImageClassRef.Convert(ref imgargs);
                    //}
                    //catch (Exception)
                    //{

                    //}
                    var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                    if (chkFileVal.Exists)
                    {
                        //htmlData.Append("<br><br><IMG src = \"" + chkFileVal.FullName.ToString() + "\" height = 200pts width = 200pts />");
                        //Image imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                        //var bmbFile = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                        //if (bmbFile.Exists)
                        // {
                        //     bmbFile.Delete();
                        //   imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                        // }
                        // else
                        //{
                        //     imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                        // }

                        // imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                        //imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + valInc.ToString(CultureInfo.InvariantCulture) + "\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                        //imm.Dispose();
                        // var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                        // asd.Delete();
                        //////////
                        if (_chkSize != null) { _chkSize.Dispose(); }
                        _chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                        Double iHeight = _chkSize.Height;
                        Double iWidth = _chkSize.Width;
                        Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                        imageSizeH = Convert.ToString(newVal.Height);
                        imageSizeW = Convert.ToString(newVal.Width);
                        if (_chkSize != null)
                        {
                            _chkSize.Dispose();
                        }
                        imageUrl = "..//Content//ProductImages/" + CustomerFolder + "//temp//ImageandAttFile//" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg";
                        imageType = "IMAGE";
                        ValInc++;
                    }
                }
                else if (
              
                chkFile.Extension.ToUpper() == ".PSD" )
                {
                    string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == userId).Select(a => a.ConverterType).SingleOrDefault();

                    if (converttype == "Image Magic")
                    {

                        string[] imgargs = {HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + cellData+"[0]",
                        HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.'))+ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg" };
                        Exefile = "convert.exe";
                        var pStart = new System.Diagnostics.ProcessStartInfo(_imageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                        pStart.CreateNoWindow = true;
                        pStart.UseShellExecute = false;
                        pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                        while (!cmdProcess.HasExited)
                        {
                        }
                    }
                    else
                    {
                        string[] imgargs = {HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + cellData+"",
                        HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.'))+ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg" };
                        Exefile = "cons_rcp.exe";
                        // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"")
                        var pStart = new System.Diagnostics.ProcessStartInfo(_imageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                        pStart.CreateNoWindow = true;
                        pStart.UseShellExecute = false;
                        pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                        while (!cmdProcess.HasExited)
                        {
                        }
                    }
                    //object[] imgargs = {(SettingMembers.GetValue(SystemSettingsCollection.SettingsList.USERIMAGEPATH.ToString())) + CellData,
                    //                                Application.StartupPath  + "\\temp\\ImageandAttFile" + chkFile.Name.ToString().Substring(0, chkFile.Name.ToString().LastIndexOf('.'))+valInc.ToString() + ".jpg" };
                    //MagickImageClass MagickImageClassRef = new MagickImageClass();
                    //try
                    //{
                    //    MagickImageClassRef.Convert(ref imgargs);
                    //}
                    //catch (Exception)
                    //{

                    //}
                    var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                    if (chkFileVal.Exists)
                    {
                        //htmlData.Append("<br><br><IMG src = \"" + chkFileVal.FullName.ToString() + "\" height = 200pts width = 200pts />");
                        //Image imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                        //var bmbFile = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                        //if (bmbFile.Exists)
                        // {
                        //     bmbFile.Delete();
                        //   imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                        // }
                        // else
                        //{
                        //     imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                        // }

                        // imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                        //imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + valInc.ToString(CultureInfo.InvariantCulture) + "\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                        //imm.Dispose();
                        // var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + valInc.ToString(CultureInfo.InvariantCulture) + ".bmp");
                        // asd.Delete();
                        //////////
                        if (_chkSize != null) { _chkSize.Dispose(); }
                        _chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                        Double iHeight = _chkSize.Height;
                        Double iWidth = _chkSize.Width;
                        Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                        imageSizeH = Convert.ToString(newVal.Height);
                        imageSizeW = Convert.ToString(newVal.Width);
                        if (_chkSize != null)
                        {
                            _chkSize.Dispose();
                        }
                        imageUrl = "..//Content//ProductImages/" + CustomerFolder + "//temp//ImageandAttFile//" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg";
                        imageType = "IMAGE";
                        ValInc++;
                    }
                }
                else if (chkFile.Extension.ToUpper() == ".JPG" ||
                         chkFile.Extension.ToUpper() == ".JPEG" ||
                         chkFile.Extension.ToUpper() == ".GIF" ||
                         chkFile.Extension.ToUpper() == ".BMP" ||
                         chkFile.Extension.ToUpper() == ".PNG" ||
                         chkFile.Extension.ToUpper() == ".ICO")
                {
                    // htmlData.Append("<br><br><IMG src = \"" + (SettingMembers.GetValue(SystemSettingsCollection.SettingsList.USERIMAGEPATH.ToString())) + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[8].ToString().Trim() + "\" height = 200pts width = 200pts />");
                    if (_chkSize != null) { _chkSize.Dispose(); }
                    try
                    {
                        _chkSize = Image.FromFile(destinationPath + cellData);
                        Double iHeight = _chkSize.Height;
                        Double iWidth = _chkSize.Width;
                        Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                        imageSizeH = Convert.ToString(newVal.Height);
                        imageSizeW = Convert.ToString(newVal.Width);
                        if (_chkSize != null)
                        {
                            _chkSize.Dispose();
                        }
                        imageUrl = generatedUrl + CustomerFolder + cellData.Trim();

                    }
                    catch (Exception)
                    {
                        imageUrl = "";
                        imageSizeH = "72";
                        imageSizeW = "72";
                    }
                    imageType = "IMAGE";
                }
                else if (chkFile.Extension.ToUpper() == ".AVI" ||
                         chkFile.Extension.ToUpper() == ".WMV" ||
                         chkFile.Extension.ToUpper() == ".MPG" ||
                         chkFile.Extension.ToUpper() == ".SWF")
                {
                    imageUrl = "..//Content//ProductImages" + CustomerFolder + cellData.Trim();
                    imageType = "MEDIA";
                }
            }
            else if (cellData == "")
            {

                returnstr = ("\" VALIGN=\"Middle\" style=\" color: Black; BACKGROUND-COLOR: white  \" >");
                returnstr = returnstr + ("  ");
                //returnstr = returnstr + ("<IMG SRC = \"" + Application.StartupPath + "\\images\\unsupportedImageformat.jpg " + "\" height = 82px>");

            }

            if (imageType == "IMAGE")
            {
                returnstr = ("\" VALIGN=\"Middle\" style=\" height:150px; color: Black; BACKGROUND-COLOR: white  \" >");
                returnstr = returnstr + ("<IMG SRC = \"" + imageUrl + "\" HEIGHT=" + imageSizeH + " WIDTH = " + imageSizeW + ">");
            }
            else if (imageType == "MEDIA")
            {
                returnstr = ("\" VALIGN=\"Middle\" style=\" height:150px; color: Black; BACKGROUND-COLOR: white  \" >");
                returnstr = returnstr + ("<EMBED SRC= \"" + imageUrl + "\" height = 150pts width = 150pts AUTOPLAY=\"false\" CONTROLLER=\"true\"/>");

            }
            else if (imageType == "" && cellData != "")
            {
                returnstr = ("\" VALIGN=\"Middle\" style=\" height:82px; color: Black; BACKGROUND-COLOR: white  \" >");
                // returnstr = returnstr + ("<IMG SRC = \"" + "..//Content//ProductImages//images//unsupportedImageformat.jpg " + "\" height = 82px>");
            }
            return (returnstr);
        }

        public string GenerateProductGroupPreview(int catalogId, int familyId, int productId, bool productLevelMultipletablePreview, int userID, string categoryId)
        {
            try
            {
                var htmlString = new StringBuilder();
                var objDataSet = new DataSet();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = productId;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 20).Value = "PRODUCTPREVIEW";
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataSet);
                }

                var imagemagickpath = _dbcontext.Customer_Settings.FirstOrDefault(a => a.CustomerId == userID);
                //if (imagemagickpath != null)
                //{
                //    _imageMagickPath = imagemagickpath.ImageConversionUtilityPath;
                //    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                //}
                //else
                //{
                //    _imageMagickPath = "C:\\Program Files\\ImageMagick-6.8.5-Q8";
                //}
                string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == userID).Select(a => a.ConverterType).SingleOrDefault();

                if (imagemagickpath != null && converttype == "Image Magic")
                {
                    _imageMagickPath = convertionPath;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                }
                else
                {
                    _imageMagickPath = convertionPathREA;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");

                }
                htmlString.Append("<html>");
                htmlString.Append("<left>");
                htmlString.Append("<head>");
                htmlString.Append("</head>");
                htmlString.Append("<body>");
                htmlString.Append("<table class=\"table table-bordered table-striped table-hover\" style=\"width:50%\" >");
                htmlString.Append("<style>td{font-family:arial Unicode ms;font-size:12px;}th{font-family:" + "Helvetica Neue" + ",Helvetica,Arial,sans-serif !important;;font-size:12px;font-weight:Bold}</style>");

                if (objDataSet.Tables.Count > 0)
                {
                    foreach (DataRow dr in objDataSet.Tables[0].Rows)
                    {
                        if (dr["ATTRIBUTE_TYPE"].ToString() != "6")
                        {
                            htmlString.Append("<tr><td align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \"><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                        }
                        else
                        {
                            htmlString.Append("<tr><td align=\"left\" style=\"background-color:lightgreen;\"><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                        }
                        const string alignVal = "Left";
                        string style = dr["STYLE_FORMAT"].ToString();
                         int index=0;
                         if (style.Length > 0)
                         {
                             index = style.IndexOf("[", StringComparison.Ordinal);

                             style = style.Substring(0, index - 1);
                         }
                        double dt = 0;
                        ExtractCurrenyFormat(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));

                        if (dr["ATTRIBUTE_TYPE"].ToString() == "6" && dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        {
                            if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
                            {
                                //dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
                                #region "Decimal Place Trimming"
                                string tempStr = dr["NUMERIC_VALUE"].ToString();
                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                if (noofdecimalplace != 6)
                                {
                                    if ((tempStr.IndexOf('.') + 1 + noofdecimalplace) < tempStr.Length)
                                    {
                                        tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                    }
                                }
                                if (noofdecimalplace == 0)
                                {
                                    tempStr = tempStr.TrimEnd('.');
                                }
                                dr["NUMERIC_VALUE"] = tempStr;
                                #endregion
                            }

                        }
                        else if (dr["ATTRIBUTE_TYPE"].ToString() == "4" || dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        {
                            if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
                            {
                                //dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
                                #region "Decimal Place Trimming"
                                string tempStr = dr["NUMERIC_VALUE"].ToString();
                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                if (noofdecimalplace != 6)
                                {
                                    if ((tempStr.IndexOf('.') + 1 + noofdecimalplace) < tempStr.Length)
                                    {
                                        tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                    }
                                    // tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                }
                                if (noofdecimalplace == 0)
                                {
                                    tempStr = tempStr.TrimEnd('.');
                                }
                                dr["NUMERIC_VALUE"] = tempStr;
                                #endregion
                            }

                        }
                        if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
                        {
                            htmlString.Append("<TD ALIGN=\"" + alignVal + GetProductCellString(dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;"), userID));
                            htmlString.Append("<br><br/>");
                            htmlString.Append("<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + dr["OBJECT_NAME"] + "</label>");
                            htmlString.Append("</TD>");
                        }
                        else //if (chkAttrType[j] == 4)
                        {
                            string valueFortag = "";
                            if ((_headeroptions == "All") || (_headeroptions != "All"))
                            {
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null) &&
                                    ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                      dr["NUMERIC_VALUE"].ToString() == string.Empty) ||
                                     (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
                                      dr["STRING_VALUE"].ToString() == string.Empty)))
                                {
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().StartsWith("Number") &&
                                        dr["NUMERIC_VALUE"].ToString() == string.Empty)
                                        valueFortag = "Null";
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().StartsWith("Text") &&
                                        dr["STRING_VALUE"].ToString() == string.Empty)
                                        valueFortag = "Empty";

                                    valueFortag = (valueFortag == _emptyCondition) ? _replaceText : "";
                                }
                                else if ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                          dr["NUMERIC_VALUE"].ToString() == _emptyCondition) ||
                                         (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
                                          dr["STRING_VALUE"].ToString() == _emptyCondition))
                                {
                                    valueFortag = _replaceText;
                                }
                                else
                                {
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                    {
                                        if (index != -1 && index != 0 && dr["NUMERIC_VALUE"].ToString() != null && dr["NUMERIC_VALUE"].ToString() != "")
                                        {
                                            double dt1 = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
                                            valueFortag = _prefix +
                                                          dt1.ToString(style.Trim())
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")
                                                              .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                        else
                                            valueFortag = _prefix +
                                                          dr["NUMERIC_VALUE"].ToString()
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")
                                                              .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                    else
                                    {
                                        if (dr["STRING_VALUE"].ToString().Length > 0)
                                        {
                                            valueFortag = _prefix +
                                                          dr["STRING_VALUE"].ToString()
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")
                                                              .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                        }
                                        else
                                        {
                                            valueFortag = string.Empty;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                {
                                    //valueFortag =  dt != 0.0 ? dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") : dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    if (index != -1 && index != 0 && dr["NUMERIC_VALUE"].ToString() != null && dr["NUMERIC_VALUE"].ToString() != "")
                                    {
                                        double dt1 = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
                                        valueFortag = _prefix +
                                                      dt1.ToString(style.Trim())
                                                          .Replace("<", "&lt;")
                                                          .Replace(">", "&gt;")
                                                          .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                    }
                                    else
                                        valueFortag =
                                            dr["NUMERIC_VALUE"].ToString()
                                                .Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                                else
                                {
                                    valueFortag =
                                        dr["STRING_VALUE"].ToString()
                                            .Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                }
                            }

                            if (
                                dr["ATTRIBUTE_DATAFORMAT"].ToString()
                                    .StartsWith(
                                        "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                            {
                                if (
                                    dr["STRING_VALUE"].ToString()
                                        .Replace("_", "")
                                        .Replace("/", "")
                                        .Replace(":", "")
                                        .Trim()
                                        .Length != 0)
                                {
                                    valueFortag = dr["STRING_VALUE"].ToString();
                                    valueFortag = valueFortag.Substring(3, 2) + "/" + valueFortag.Substring(0, 2) + "/" +
                                                  valueFortag.Substring(6, 4);
                                }
                            }

                            valueFortag = "<pre>" + valueFortag + "</pre>";
                            htmlString.Append("<td align=\"" + alignVal +
                                              "\" valign=\"Middle\" style=\" color: Black; BACKGROUND-COLOR: white  \" >" +
                                              valueFortag + "</td></tr>");
                        }
                    }


                }


                htmlString.Append("</table>");
                htmlString.Append("</body>");
                htmlString.Append("</left>");

                //For attribute group
                var objAttributeDataSet = new DataSet();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = productId;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "PRODUCTATTRIBUTEGROUP";
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objAttributeDataSet);
                }
                string sFamilyName = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == familyId)
                    .Select(y => y.FAMILY_NAME).FirstOrDefault();

                var scatg = new StringBuilder();


                var sbHtmlTemp = new StringBuilder();


                int groupId = 0;

                var sbXml = new StringBuilder();
                bool familyLvelPreviewEnabled = productLevelMultipletablePreview;
                if (familyLvelPreviewEnabled)
                {
                    if (objAttributeDataSet.Tables[0].Rows.Count > 0)
                    {
                        //scatg.Append("<html><HEAD></HEAD><BODY><P style=\"font-size: x-medium; color: red; font-family: Verdana;\"> " + sCategoryName + " > " + sFamilyName + "</P></BODY></html>");
                        //htmlString.Append("<HTML>");
                        //htmlString.Append("<HEAD><style type='text/css'>.bdr_btm{border-bottom: 1px solid #D5D5D5;}.bdr_LB{border-bottom: 1px solid #D5D5D5;border-left: 1px solid #ACACAC;}</style>");
                        //htmlString.Append("</HEAD>");
                        //htmlString.Append("<BODY><br/><table class=\"table table-condensed table-bordered table-striped\"><tr><td colspan='3' align='center' style='font-family: Calibri; width: 100%;'><h4><b>PREVIEW WITH GROUPING</b></h4></td></tr>");
                        htmlString.Append("<table class=\"table table-condensed table-bordered table-striped\" ><tr><td colspan='3' align='center' style='font-family: " + "Helvetica Neue" + ",Helvetica,Arial,sans-serif !important;; width: 100%;'><h4><b>PREVIEW WITH GROUPING</b></h4></td></tr>");

                        sbHtmlTemp.Append("<table class=\"table table-condensed table-bordered table-striped\" style=\"width:50%\"><tr><td></td><td></td><td></td></tr>");
                        sbXml.Append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");
                        sbXml.Append("<tradingbell_root catalog_id=\"" + catalogId + "\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\">\n");
                        var catalogDetails = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(x => x).ToList();
                        foreach (var catalogDetail in catalogDetails)
                        {
                            sbXml.Append("<catalog_name  COTYPE=\"TEXT\" TBGUID=\"CN" + _randomClass.Next() + "\" aid:pstyle=\"catalog_name\"><![CDATA[" + catalogDetail.CATALOG_NAME + "]]></catalog_name>\n");
                            sbXml.Append("<version  COTYPE=\"TEXT\" TBGUID=\"CV" + _randomClass.Next() + "\" aid:pstyle=\"version\"><![CDATA[" + catalogDetail.VERSION + "]]></version>\n");
                            sbXml.Append("<description  COTYPE=\"TEXT\" TBGUID=\"CD" + _randomClass.Next() + "\" aid:pstyle=\"description\"><![CDATA[" + catalogDetail.DESCRIPTION + "]]></description>\n");
                        }

                        DataTable dtGroups = objAttributeDataSet.Tables[0].DefaultView.ToTable(true, "GROUP_ID");
                        sbXml.Append("<attribute_group xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\" tables=\"" + dtGroups.Rows.Count + "\"  COTYPE=\"\" TBGUID=\"M" + _randomClass.Next() + "\" aid:pstyle=\"attribute_group\">\n");
                        sbXml.Append("<TABLE TBGUID=\"TA" + _randomClass.Next() + "\" Format=\"SuperTable\" ncols=\"3\" nrows=\"" + objAttributeDataSet.Tables[0].Rows.Count + "\" aid:table=\"table\" aid:tcols=\"3\" aid:trows=\"" + objAttributeDataSet.Tables[0].Rows.Count + "\" aid5:tablestyle=\"TABLE\" Transpose=\"0\">");


                        if (HttpContext.Current.Session["CustomerSubItemNo"] == null)
                        {
                            var customerDetails = _dbcontext.Customers.Where(x => x.CustomerId == userID).Select(x => x.CustomizeSubItemNo).FirstOrDefault();

                            if (string.IsNullOrEmpty(customerDetails))
                            {
                                customerDetails = "SUBITEM#";
                            }
                            System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerDetails;

                        }

                        if (HttpContext.Current.Session["CustomerItemNo"] == null)
                        {
                            var customerDetails = _dbcontext.Customers.Where(x => x.CustomerId == userID).Select(x => x.CustomizeItemNo).FirstOrDefault();

                            if (string.IsNullOrEmpty(customerDetails))
                            {
                                customerDetails = "ITEM#";
                            }
                            System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerDetails;

                        }
                        
                        foreach (DataRow dr in objAttributeDataSet.Tables[0].Rows)
                        {

                            if (dr["ATTRIBUTE_NAME"].ToString() == "ITEM#")
                            {
                                dr["ATTRIBUTE_NAME"] = HttpContext.Current.Session["CustomerItemNo"];
                            }

                            if (dr["ATTRIBUTE_NAME"].ToString() == "SUBITEM#")
                            {
                                dr["ATTRIBUTE_NAME"] = HttpContext.Current.Session["CustomerSubItemNo"];
                            }


                            if (groupId != Convert.ToInt32(dr["GROUP_ID"]))
                            {
                                //if (!string.IsNullOrEmpty(groupName))
                                //sbXML.Append("</" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                                groupId = Convert.ToInt32(dr["GROUP_ID"]);
                                htmlString.Append("<tr><td colspan='3' class='black_14_b bdr_btm'><strong>" + dr["GROUP_NAME"] + "</strong></td></tr>");
                                sbHtmlTemp.Append("<tr><td colspan='3' class='black_14_b bdr_btm'><strong>" + dr["GROUP_NAME"] + "</strong></td></tr>");
                                //sbXML.Append("<" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"\" group_id =\"" + dr["GROUP_ID"] + "\" TBGUID=\"C" + RandomClass.Next() + "\" aid:pstyle=\"attribute_group\">\n");
                                sbXml.Append("<group_name TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"group_name\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["GROUP_NAME"] + "]]></group_name>");
                            }
                            else
                            {
                                sbXml.Append("<group_name TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"group_name\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[]]></group_name>");
                            }
                            sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["ATTRIBUTE_NAME"] + "]]></Cell>");
                            if (string.IsNullOrEmpty(Convert.ToString(dr["NUMERIC_VALUE"])))
                            {
                                //sbXML.Append("<" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"TEXT\" TBGUID=\"AN" + RandomClass.Next() + "\" aid:pstyle=\"attribute_name\"><![CDATA[" + dr["STRING_VALUE"] + "]]></" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                                sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["STRING_VALUE"] + "]]></Cell>");
                            }
                            else
                            {
                                //sbXML.Append("<" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"TEXT\" TBGUID=\"AN" + RandomClass.Next() + "\" aid:pstyle=\"attribute_name\"><![CDATA[" + dr["NUMERIC_VALUE"] + "]]></" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                                sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["NUMERIC_VALUE"] + "]]></Cell>");
                            }
                            if (dr["ATTRIBUTE_TYPE"].ToString() != "6")
                            {
                                htmlString.Append("<tr><td width='100'>&nbsp;</td><td width='208' class='bdr_LB'><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                            }
                            else
                            {
                                htmlString.Append("<tr><td width='100'>&nbsp;</td><td width='208' class='bdr_LB' style=\"background-color:lightgreen;\"><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                            }


                            sbHtmlTemp.Append("<tr><td width='100'></td><td width='208' class='bdr_LB'><b>" + dr["ATTRIBUTE_NAME"] + "</b></td>");
                            const string alignVal = "Left";
                            string style = dr["STYLE_FORMAT"].ToString();
                            if (style.Length > 0)
                                style = style.Substring(0, style.IndexOf('[') - 1);
                            double dt = 0;
                            ExtractCurrenyFormat(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));

                            if ((dr["ATTRIBUTE_TYPE"].ToString() == "6") && (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM"))
                            {
                                if (dr["STRING_VALUE"].ToString().Trim().Length > 0)
                                {
                                    dt = Convert.ToDouble(dr["STRING_VALUE"].ToString());
                                }
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "4" || dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            {
                                if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
                                {
                                    dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
                                }
                            }
                            if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
                            {
                                htmlString.Append("<TD width='208' class='bdr_LB' ALIGN=\"" + alignVal + GetProductCellString(dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;"), userID));
                                htmlString.Append("<br><br/>");
                                htmlString.Append("<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + dr["OBJECT_NAME"] + "</label>");
                                htmlString.Append("</TD>");
                                sbHtmlTemp.Append("<TD width='208' class='bdr_LB' ALIGN=\"" + alignVal + GetProductCellString(dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;"), userID));
                                htmlString.Append("<br><br/>");
                                sbHtmlTemp.Append("<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + dr["OBJECT_NAME"] + "</label>");
                                sbHtmlTemp.Append("</TD>");
                            }
                            else  //if (chkAttrType[j] == 4)
                            {
                                string valueFortag;
                                if ((_headeroptions == "All") || (_headeroptions != "All"))
                                {
                                    if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null) &&
                                        ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" && dr["NUMERIC_VALUE"].ToString() == string.Empty && dr["ATTRIBUTE_TYPE"].ToString() != "6") ||
                                        (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" && dr["STRING_VALUE"].ToString() == string.Empty && dr["ATTRIBUTE_TYPE"].ToString() != "6")))
                                    {
                                        valueFortag = _replaceText;
                                    }
                                    else if ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" && dr["NUMERIC_VALUE"].ToString() == _emptyCondition && dr["ATTRIBUTE_TYPE"].ToString() != "6") ||
                                        (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" && dr["STRING_VALUE"].ToString() == _emptyCondition && dr["ATTRIBUTE_TYPE"].ToString() != "6"))
                                    {
                                        valueFortag = _replaceText;
                                    }
                                    else if (dr["STRING_VALUE"].ToString() == _emptyCondition && dr["ATTRIBUTE_TYPE"].ToString() == "6")
                                    {
                                        valueFortag = _replaceText;
                                    }
                                    else
                                    {
                                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                        {
                                            if ((style != "") && (dt != 0.0))
                                            {
                                                valueFortag = _prefix + dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                            else
                                            {
                                                valueFortag = _prefix + dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                        }
                                        else
                                        {
                                            if (dr["STRING_VALUE"].ToString().Length > 0)
                                            {
                                                valueFortag = _prefix + dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix;
                                            }
                                            else
                                            {
                                                valueFortag = string.Empty;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                    {
                                        //valueFortag = dt != 0.0 ? dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") : dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        if ((style != "") && (dt != 0.0))
                                        {
                                            valueFortag = dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                        else
                                        {
                                            valueFortag = dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                        }
                                    }
                                    else
                                    {
                                        valueFortag = dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>");
                                    }
                                }

                                if (dr["ATTRIBUTE_DATAFORMAT"].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                {
                                    if (dr["STRING_VALUE"].ToString().Replace("_", "").Replace("/", "").Replace(":", "").Trim().Length != 0)
                                    {
                                        valueFortag = dr["STRING_VALUE"].ToString();
                                        valueFortag = valueFortag.Substring(3, 2) + "/" + valueFortag.Substring(0, 2) + "/" + valueFortag.Substring(6, 4);
                                    }
                                }
                                if (valueFortag.EndsWith(".jpg"))
                                {
                                    //htmlString.Append("<img height='150' width='200' src='" + valueFortag + "' \\></td></tr>");
                                }
                                else
                                {
                                    htmlString.Append("<td width='208' class='bdr_LB'>" + valueFortag + "</td></tr>");
                                }
                                sbHtmlTemp.Append("<td width='208' class='bdr_LB'>" + valueFortag + "</td></tr>");
                            }
                        }
                        //sbXML.Append("</" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
                        sbXml.Append("</TABLE>");
                        sbXml.Append("</attribute_group>");
                        sbXml.Append("</tradingbell_root>");

                        htmlString.Append("</table><br/>");
                        sbHtmlTemp.Append("</table><br/>");
                    }
                }
                htmlString.Append(LoadReferenceTable(familyId, productLevelMultipletablePreview, userID));

                //-----------------for subproduct check-------------------VinothKumar  //  Mariya Vijayan

                var subprodcheck = from aa in _dbcontext.TB_SUBPRODUCT
                                   where aa.CATALOG_ID == catalogId && aa.PRODUCT_ID == productId
                                   select aa;

                var subprodcheckSetting = from aaa in _dbcontext.Customer_Settings
                                          where aaa.CustomerId == userID && aaa.EnableSubProduct == true
                                          select aaa;

                if (subprodcheck.Any() && subprodcheckSetting.Any())
                {
                    htmlString.Append("<h4>SubProduct Preview</h4>");
                    htmlString.Append(GenerateSubProductGroupPreview(catalogId, familyId, productId, productLevelMultipletablePreview, userID, categoryId));
                }
               
                //---------------------------------------
                
                //htmlString.Append("</body></html>");
                //var encOutput = Encoding.UTF8;
                //string htmlPath = HttpContext.Current.Server.MapPath("~/Content/HTML");
                //string filePath = (htmlPath + "\\ProductPreview.html");
                //var fileHtml = new FileStream(filePath, FileMode.Create);
                //var strwriter = new StreamWriter(fileHtml, encOutput);
                htmlString = htmlString.Replace("&lt;br /&gt;", "<br>").Replace("&lt;/br&gt;", "<br>").Replace("&lt;br&gt;", "<br>").Replace("&lt;br/&gt;", "<br>");
                //strwriter.Write(scatg + htmlString.ToString());
                //strwriter.Close();
                //fileHtml.Close();
                //fileHtml = new FileStream(filePath, FileMode.Open);
                //var strReader = new StreamReader(fileHtml);
                string productpreview = scatg.ToString() + htmlString.ToString();
                //strReader.Close();
                //fileHtml.Close();
                //MemoryStream mStream = new MemoryStream(new ASCIIEncoding().GetBytes(HTMLStr));
                //  _saveFileStream = scatg.Append(htmlString);
                //webBrowser1.Navigate(filePath);
                //DataSet dsXMLFromHTML = ConvertHTMLTablesToDataSet(sbHTML_Temp.ToString());
                //dsXMLFromHTML.WriteXml("C:\\sampleee.xml");
                //FileStream fs = new FileStream("C:\\sample.xml", FileMode.Create, FileAccess.Write);
                //StreamWriter xmlwrite = new StreamWriter(fs);
                //xmlwrite.Write(sbXML);
                //xmlwrite.Close();
                return productpreview;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ProductPreview : GenerateProductGroupPreview", objexception);
                return string.Empty;
            }
        }


        private Size ScaleImage(double origHeight, double origWidth, double width, double height)
        {
            try
            {
                var newSize = new Size();
                double nWidth = width;
                double nHeight = height;
                double oWidth = origWidth;
                double oHeight = origHeight;

                if (origHeight > 200 || origWidth > 200)
                {
                    if (oWidth > oHeight)
                    {
                        double ratio = oHeight / oWidth;
                        double final = (nWidth) * ratio;
                        nHeight = (int)final;
                    }
                    else
                    {
                        double ratio = oWidth / oHeight;
                        double final = (nHeight) * ratio;
                        nWidth = (int)final;
                    }
                    newSize.Height = (int)nHeight;
                    newSize.Width = (int)nWidth;
                }
                else
                {
                    newSize.Height = (int)origHeight;
                    newSize.Width = (int)origWidth;
                }

                return newSize;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ScaleImage : ScaleImage", objexception);
            }

            return new Size();
        }
    }
}
