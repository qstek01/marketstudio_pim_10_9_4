﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace LS.Data.Model.ProductPreview
{
    public class ProductLayout
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(ProductPreview));
        readonly CSEntities _dbcontext = new CSEntities();
        public bool ValidateStructureName(int familyId, string structureName, int catalogId)
        {
            try
            {
                var extstructureName =
                    _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(x => x.CATALOG_ID == catalogId && x.FAMILY_ID == familyId && x.STRUCTURE_NAME == structureName);
                if (extstructureName != null)
                {
                    return true;
                }
                else if (structureName.Trim().Replace("'", "''") == "Default Layout")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at ProductLayout : ValidateStructureName", ex);
                return false;
            }
        }
        public void UpdateTableStructure(int familyId, string categoryId, int catalogId, bool isDefault, string structureName, string tableType,int masterCatalogId, string FamilyName)
        {
            try
            {
                if (isDefault)
                {
                    var tbfamilytablestructure =
                        _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(x => x.CATALOG_ID == catalogId && x.FAMILY_ID == familyId && x.IS_DEFAULT);
                    foreach (var tbFamilyTableStructure in tbfamilytablestructure)
                    {
                        tbFamilyTableStructure.IS_DEFAULT = false;

                    }
                    _dbcontext.SaveChanges();
                }
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var sqlCommand = new SqlCommand();
                    objSqlConnection.Open();
                    sqlCommand.CommandText = "STP_CATALOGSTUDIO5_InsertFamilyTableStructure";
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Connection = objSqlConnection;
                    sqlCommand.Parameters.Add("@CATEGORYID", SqlDbType.NVarChar);
                    sqlCommand.Parameters["@CATEGORYID"].Value = categoryId;
                    sqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int);
                    sqlCommand.Parameters["@CATALOGID"].Value = catalogId;
                    sqlCommand.Parameters.Add("@FAMILYID", SqlDbType.Int);
                    sqlCommand.Parameters["@FAMILYID"].Value = familyId;
                    sqlCommand.Parameters.Add("@STRUCTURENAME", SqlDbType.NVarChar, 500);
                    sqlCommand.Parameters["@STRUCTURENAME"].Value = structureName;
                    sqlCommand.Parameters.Add("@TABLETYPE", SqlDbType.NVarChar, 12);
                    sqlCommand.Parameters["@TABLETYPE"].Value = tableType;
                    if (isDefault)
                    {
                        sqlCommand.Parameters.Add("@ISDEFAULT", SqlDbType.Bit);
                        sqlCommand.Parameters["@ISDEFAULT"].Value = 1;
                    }
                    else
                    {
                        sqlCommand.Parameters.Add("@ISDEFAULT", SqlDbType.Bit);
                        sqlCommand.Parameters["@ISDEFAULT"].Value = 0;
                    }
                    sqlCommand.ExecuteNonQuery();
                    objSqlConnection.Close();

                    int table_Id = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Max(x => x.ID);
                    string tableStructure = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(a => a.CATALOG_ID == catalogId && a.FAMILY_ID == familyId && a.STRUCTURE_NAME == structureName).Select(a => a.FAMILY_TABLE_STRUCTURE).FirstOrDefault();

                    //-----------------------------------------------Master Catalog changes-------------------------------------------------//
                    SqlCommand objsqlCommand = new SqlCommand();
                    using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        objsqlCommand = new SqlCommand("STP_TABLEDESIGNER_UPDATION", sqlConnection);

                        objsqlCommand.CommandType = CommandType.StoredProcedure;
                        objsqlCommand.CommandTimeout = 0;

                        objsqlCommand.Parameters.Add("@MASTERCATALOG_ID", SqlDbType.Int).Value = masterCatalogId;
                        objsqlCommand.Parameters.Add("@MASTERFAMILY_NAME", SqlDbType.VarChar).Value = FamilyName;
                        objsqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int).Value = catalogId;
                        objsqlCommand.Parameters.Add("@FAMILYID", SqlDbType.Int).Value = familyId;
                        objsqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar).Value = tableStructure;
                        objsqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar).Value = structureName;

                        sqlConnection.Open();
                        objsqlCommand.ExecuteNonQuery();
                        objSqlConnection.Close();
                    }
                    //-----------------------------------------------Master Catalog changes-------------------------------------------------//

                    List<TB_PACKAGE_MASTER> packs = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogId && s.IS_FAMILY.ToLower() == "product").ToList();
                    foreach (var pack in packs)
                    {
                        if (pack != null)
                        {
                            string xmlString = CreateXMLAttributePack(pack.GROUP_ID);
                            string sqlStr = "Insert into TB_ATTRIBUTE_PACK_TABLE_STRUCTURE (FAMILY_STRUCTURE_ID,PACK_ID,TABLE_STRUCTURE,STRUCTURE_NAME,IS_DEFAULT,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) values (" + table_Id + "," + pack.GROUP_ID + ",'" + xmlString.Replace("'", "''") + "','" + structureName + "'," + 1 + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                            using (var objConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                            {
                                objConnection.Open();
                                SqlCommand objSqlCommand = objConnection.CreateCommand();
                                objSqlCommand.CommandText = sqlStr;
                                objSqlCommand.CommandType = CommandType.Text;
                                objSqlCommand.Connection = objConnection;
                                objSqlCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at ProductLayout : UpdateTableStructure", ex);
            }
        }
        public DataTable Checkproductfilter(string sqlquery)
        {
            var productfilter = new DataTable();
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                var objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = sqlquery;
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(productfilter);
            }
            return productfilter;
        }
        public int Insertquery(string sqlquery)
        {
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                var objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = sqlquery;
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                int rValue = objSqlCommand.ExecuteNonQuery();
                return rValue;

            }

        }

        #region Attribute Pack

        public string CreateXMLAttributePack(int packId)
        {
            var xmlTxtBuilder = new StringBuilder();
            xmlTxtBuilder.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            xmlTxtBuilder.Append("<TradingBell TableType=\"SuperTable\">");
            string mergeValue = "unchecked";
            // For item#
            xmlTxtBuilder.Append("<LeftRowField AttrID=\"");
            xmlTxtBuilder.Append("Attr:");
            xmlTxtBuilder.Append(1);
            xmlTxtBuilder.Append("\" Merge=\"" + mergeValue + "\"");
            xmlTxtBuilder.Append(" Level=\"" + 0 + "\"");
            xmlTxtBuilder.Append(">");
            xmlTxtBuilder.Append("</LeftRowField>");

            List<TB_PACKAGE_DETAILS> attributeIds = new List<TB_PACKAGE_DETAILS>();
            attributeIds = _dbcontext.TB_PACKAGE_DETAILS.Where(s => s.GROUP_ID == packId).ToList();
            if (attributeIds != null && attributeIds.Count > 0)
            {
                foreach (var attributeId in attributeIds)
                {
                    string attribute_Id = Convert.ToString(attributeId.ATTRIBUTE_ID);
                    xmlTxtBuilder.Append("<LeftRowField AttrID=\"");
                    xmlTxtBuilder.Append("Attr:");
                    xmlTxtBuilder.Append(attribute_Id);
                    xmlTxtBuilder.Append("\" Merge=\"" + mergeValue + "\"");
                    xmlTxtBuilder.Append(" Level=\"" + 0 + "\"");
                    xmlTxtBuilder.Append(">");
                    xmlTxtBuilder.Append("</LeftRowField>");
                }
            }

            xmlTxtBuilder.Append("<SummaryGroupField>");
            xmlTxtBuilder.Append("</SummaryGroupField>");
            xmlTxtBuilder.Append("<TableGroupField>");
            xmlTxtBuilder.Append("</TableGroupField>");
            xmlTxtBuilder.Append("<PlaceHolderText>");
            xmlTxtBuilder.Append("<![CDATA[]]>");
            xmlTxtBuilder.Append("</PlaceHolderText>");
            xmlTxtBuilder.Append("<DisplayRowHeader>");
            xmlTxtBuilder.Append("True");
            xmlTxtBuilder.Append("</DisplayRowHeader>");
            xmlTxtBuilder.Append("<DisplayColumnHeader>");
            xmlTxtBuilder.Append("False");
            xmlTxtBuilder.Append("</DisplayColumnHeader>");
            xmlTxtBuilder.Append("<DisplaySummaryHeader>");
            xmlTxtBuilder.Append("False");
            xmlTxtBuilder.Append("</DisplaySummaryHeader>");
            xmlTxtBuilder.Append("<VerticalTable>");
            xmlTxtBuilder.Append("false");
            xmlTxtBuilder.Append("</VerticalTable>");
            xmlTxtBuilder.Append("<PivotHeaderText>");
            xmlTxtBuilder.Append("<![CDATA[]]>");
            xmlTxtBuilder.Append("</PivotHeaderText>");
            xmlTxtBuilder.Append("<MergeRowHeader>");
            xmlTxtBuilder.Append("");
            xmlTxtBuilder.Append("</MergeRowHeader>");
            xmlTxtBuilder.Append("<MergeSummaryFields>");
            xmlTxtBuilder.Append("");
            xmlTxtBuilder.Append("</MergeSummaryFields>");
            xmlTxtBuilder.Append("</TradingBell>");
            return xmlTxtBuilder.ToString();
        }

        #endregion

    }
}
