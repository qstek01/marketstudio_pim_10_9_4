﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using log4net;
using System.Web.Configuration;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using LS.Data.Model.Accounts;
using System.Web.Mvc;
using LS.Data.Utilities;

namespace LS.Data.Model.ProductPreview
{
    public class FamilyPreviews
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(FamilyPreviews));
        readonly CSEntities _dbcontext = new CSEntities();
        readonly Random _randomClass = new Random();
        private string _prefix, _suffix, _emptyCondition, _replaceText, _headeroptions = string.Empty, _fornumeric = string.Empty;
        public static string CategoryLevel;
        public StringBuilder HtmlData;
        private Image _chkSize;
        private int _valInc;
        public string convertionPath = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePath"].ToString();
        public string convertionPathREA = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePathREA"].ToString();
        public string conPreviewPath = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedPreviewPath"].ToString();
        public string CategoryID = string.Empty;
        public string UserName = string.Empty;
        public string CustomerFolder = string.Empty;
        public string converttype = string.Empty;
        public string cust_id = string.Empty;
        public int ValInc
        {
            get
            {
                return _valInc;
            }
            set
            {
                _valInc = value;
            }
        }
        public string ImageMagickPath = string.Empty;
        public string Exefile = string.Empty;

        //public string GenerateProductGroupPreview(int catalogId, int familyId, int productId, bool ProductLevelMultipletablePreview)
        //{
        //    try
        //    {
        //        var htmlString = new StringBuilder();
        //        var objDataSet = new DataSet();
        //        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
        //        {
        //            var objSqlCommand = objSqlConnection.CreateCommand();
        //            objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
        //            objSqlCommand.CommandType = CommandType.StoredProcedure;
        //            objSqlCommand.Connection = objSqlConnection;
        //            objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
        //            objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = productId;
        //            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 20).Value = "PRODUCTPREVIEW";
        //            objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
        //            objSqlConnection.Open();
        //            var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
        //            objSqlDataAdapter.Fill(objDataSet);
        //        }

        //        htmlString.Append("<html>");
        //        htmlString.Append("<left>");
        //        htmlString.Append("<head>");
        //        htmlString.Append("</head>");
        //        htmlString.Append("<table class=\"table table-condensed table-bordered table-striped\">");
        //        // htmlString.Append("<style>td{font-family:arial Unicode ms;font-size:12px;}th{font-family:arial unicode ms;font-size:12px;font-weight:Bold}</style>");

        //        if (objDataSet.Tables.Count > 0)
        //        {
        //            foreach (DataRow dr in objDataSet.Tables[0].Rows)
        //            {
        //                htmlString.Append("<tr><td align=\"left\">" + dr["ATTRIBUTE_NAME"] + "</td>");
        //                const string alignVal = "Left";
        //                string style = dr["STYLE_FORMAT"].ToString();
        //                if (style.Length > 0)
        //                    style = style.Substring(0, style.IndexOf('[') - 1);
        //                double dt = 0;
        //                ExtractCurrenyFormat(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));

        //                if (dr["ATTRIBUTE_TYPE"].ToString() == "6" && dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
        //                {
        //                    if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
        //                    {
        //                        //dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
        //                        #region "Decimal Place Trimming"
        //                        string tempStr = dr["NUMERIC_VALUE"].ToString();
        //                        string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
        //                        datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
        //                        int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
        //                        if (noofdecimalplace != 6)
        //                        {
        //                            tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
        //                        }
        //                        if (noofdecimalplace == 0)
        //                        {
        //                            tempStr = tempStr.TrimEnd('.');
        //                        }
        //                        dr["NUMERIC_VALUE"] = tempStr;
        //                        #endregion
        //                    }

        //                }
        //                else if (dr["ATTRIBUTE_TYPE"].ToString() == "4" || dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
        //                {
        //                    if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
        //                    {
        //                        //dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
        //                        #region "Decimal Place Trimming"
        //                        string tempStr = dr["NUMERIC_VALUE"].ToString();
        //                        string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
        //                        datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
        //                        int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
        //                        if (noofdecimalplace != 6)
        //                        {
        //                            tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
        //                        }
        //                        if (noofdecimalplace == 0)
        //                        {
        //                            tempStr = tempStr.TrimEnd('.');
        //                        }
        //                        dr["NUMERIC_VALUE"] = tempStr;
        //                        #endregion
        //                    }

        //                }
        //                if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
        //                {
        //                    htmlString.Append("<TD ALIGN=\"" + alignVal + GetProductCellString(dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;")));
        //                }
        //                else //if (chkAttrType[j] == 4)
        //                {
        //                    string valueFortag = "";
        //                    if ((_headeroptions == "All") || (_headeroptions != "All"))
        //                    {
        //                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null) &&
        //                            ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
        //                              dr["NUMERIC_VALUE"].ToString() == string.Empty) ||
        //                             (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
        //                              dr["STRING_VALUE"].ToString() == string.Empty)))
        //                        {
        //                            if (dr["ATTRIBUTE_DATATYPE"].ToString().StartsWith("Number") &&
        //                                dr["NUMERIC_VALUE"].ToString() == string.Empty)
        //                                valueFortag = "Null";
        //                            if (dr["ATTRIBUTE_DATATYPE"].ToString().StartsWith("Text") &&
        //                                dr["STRING_VALUE"].ToString() == string.Empty)
        //                                valueFortag = "Empty";

        //                            valueFortag = (valueFortag == _emptyCondition) ? _replaceText : "";
        //                        }
        //                        else if ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
        //                                  dr["NUMERIC_VALUE"].ToString() == _emptyCondition) ||
        //                                 (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" &&
        //                                  dr["STRING_VALUE"].ToString() == _emptyCondition))
        //                        {
        //                            valueFortag = _replaceText;
        //                        }
        //                        else
        //                        {
        //                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
        //                            {
        //                                if ((style != "") && (dt != 0.0))
        //                                    valueFortag = _prefix +
        //                                                  dt.ToString(style.Trim())
        //                                                      .Replace("<", "&lt;")
        //                                                      .Replace(">", "&gt;")
        //                                                      .Replace("\r", "<br/>")
        //                                                      .Replace("\n", "&nbsp;") + _suffix;
        //                                else
        //                                    valueFortag = _prefix +
        //                                                  dr["NUMERIC_VALUE"].ToString()
        //                                                      .Replace("<", "&lt;")
        //                                                      .Replace(">", "&gt;")
        //                                                      .Replace("\r", "<br/>")
        //                                                      .Replace("\n", "&nbsp;") + _suffix;
        //                            }
        //                            else
        //                            {
        //                                if (dr["STRING_VALUE"].ToString().Length > 0)
        //                                {
        //                                    valueFortag = _prefix +
        //                                                  dr["STRING_VALUE"].ToString()
        //                                                      .Replace("<", "&lt;")
        //                                                      .Replace(">", "&gt;")
        //                                                      .Replace("\r", "<br/>")
        //                                                      .Replace("\n", "&nbsp;") + _suffix;
        //                                }
        //                                else
        //                                {
        //                                    valueFortag = string.Empty;
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
        //                        {
        //                            //valueFortag =  dt != 0.0 ? dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") : dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;");
        //                            if ((style != "") && (dt != 0.0))
        //                                valueFortag =
        //                                    dt.ToString(style.Trim())
        //                                        .Replace("<", "&lt;")
        //                                        .Replace(">", "&gt;")
        //                                        .Replace("\r", "<br/>")
        //                                        .Replace("\n", "&nbsp;");
        //                            else
        //                                valueFortag =
        //                                    dr["NUMERIC_VALUE"].ToString()
        //                                        .Replace("<", "&lt;")
        //                                        .Replace(">", "&gt;")
        //                                        .Replace("\r", "<br/>")
        //                                        .Replace("\n", "&nbsp;");
        //                        }
        //                        else
        //                        {
        //                            valueFortag =
        //                                dr["STRING_VALUE"].ToString()
        //                                    .Replace("<", "&lt;")
        //                                    .Replace(">", "&gt;")
        //                                    .Replace("\r", "<br/>")
        //                                    .Replace("\n", "&nbsp;");
        //                        }
        //                    }

        //                    if (
        //                        dr["ATTRIBUTE_DATAFORMAT"].ToString()
        //                            .StartsWith(
        //                                "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
        //                    {
        //                        if (
        //                            dr["STRING_VALUE"].ToString()
        //                                .Replace("_", "")
        //                                .Replace("/", "")
        //                                .Replace(":", "")
        //                                .Trim()
        //                                .Length != 0)
        //                        {
        //                            valueFortag = dr["STRING_VALUE"].ToString();
        //                            valueFortag = valueFortag.Substring(3, 2) + "/" + valueFortag.Substring(0, 2) + "/" +
        //                                          valueFortag.Substring(6, 4);
        //                        }
        //                    }

        //                    htmlString.Append("<td align=\"" + alignVal +
        //                                      "\" valign=\"Middle\" style=\" color: Black; BACKGROUND-COLOR: white  \" >" +
        //                                      valueFortag + "</td></tr>");
        //                }
        //            }


        //        }


        //        htmlString.Append("</table>");
        //        htmlString.Append("<left>");

        //        //For attribute group
        //        var objAttributeDataSet = new DataSet();
        //        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
        //        {
        //            var objSqlCommand = objSqlConnection.CreateCommand();
        //            objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
        //            objSqlCommand.CommandType = CommandType.StoredProcedure;
        //            objSqlCommand.Connection = objSqlConnection;
        //            objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
        //            objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = productId;
        //            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "PRODUCTATTRIBUTEGROUP";
        //            objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;

        //            objSqlConnection.Open();
        //            var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
        //            objSqlDataAdapter.Fill(objAttributeDataSet);
        //        }
        //        string sCategoryName = _dbcontext.TB_CATALOG_FAMILY.Where(x => x.FAMILY_ID == familyId && x.CATALOG_ID == catalogId)
        //                .Select(y => y.TB_CATEGORY.CATEGORY_NAME)
        //                .FirstOrDefault();
        //        string sFamilyName = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == familyId)
        //            .Select(y => y.FAMILY_NAME).FirstOrDefault();

        //        var scatg = new StringBuilder();


        //        var sbHtmlTemp = new StringBuilder();


        //        int groupId = 0;

        //        var sbXml = new StringBuilder();
        //        bool familyLvelPreviewEnabled = ProductLevelMultipletablePreview;
        //        if (familyLvelPreviewEnabled)
        //        {
        //            if (objAttributeDataSet.Tables[0].Rows.Count > 0)
        //            {
        //                scatg.Append("<html><HEAD></HEAD><BODY><P> " + sCategoryName + " > " + sFamilyName + "</P></BODY></html>");
        //                htmlString.Append("<HTML>");
        //                htmlString.Append("<HEAD>");
        //                htmlString.Append("</HEAD>");
        //                htmlString.Append("<BODY><br/><table class=\"table table-condensed table-bordered table-striped\"><tr><td colspan='3' align='center'><h4><b>PREVIEW WITH GROUPING</b></h4></td></tr>");

        //                sbHtmlTemp.Append("<table class=\"table table-condensed table-bordered table-striped\"><tr><td></td><td></td><td></td></tr>");
        //                sbXml.Append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");
        //                sbXml.Append("<tradingbell_root catalog_id=\"" + catalogId + "\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\">\n");
        //                var catalogDetails = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(x => x).ToList();
        //                foreach (var catalogDetail in catalogDetails)
        //                {
        //                    sbXml.Append("<catalog_name  COTYPE=\"TEXT\" TBGUID=\"CN" + _randomClass.Next() + "\" aid:pstyle=\"catalog_name\"><![CDATA[" + catalogDetail.CATALOG_NAME + "]]></catalog_name>\n");
        //                    sbXml.Append("<version  COTYPE=\"TEXT\" TBGUID=\"CV" + _randomClass.Next() + "\" aid:pstyle=\"version\"><![CDATA[" + catalogDetail.VERSION + "]]></version>\n");
        //                    sbXml.Append("<description  COTYPE=\"TEXT\" TBGUID=\"CD" + _randomClass.Next() + "\" aid:pstyle=\"description\"><![CDATA[" + catalogDetail.DESCRIPTION + "]]></description>\n");
        //                }

        //                DataTable dtGroups = objAttributeDataSet.Tables[0].DefaultView.ToTable(true, "GROUP_ID");
        //                sbXml.Append("<attribute_group xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\" tables=\"" + dtGroups.Rows.Count + "\"  COTYPE=\"\" TBGUID=\"M" + _randomClass.Next() + "\" aid:pstyle=\"attribute_group\">\n");
        //                sbXml.Append("<TABLE TBGUID=\"TA" + _randomClass.Next() + "\" Format=\"SuperTable\" ncols=\"3\" nrows=\"" + objAttributeDataSet.Tables[0].Rows.Count + "\" aid:table=\"table\" aid:tcols=\"3\" aid:trows=\"" + objAttributeDataSet.Tables[0].Rows.Count + "\" aid5:tablestyle=\"TABLE\" Transpose=\"0\">");
        //                foreach (DataRow dr in objAttributeDataSet.Tables[0].Rows)
        //                {
        //                    if (groupId != Convert.ToInt32(dr["GROUP_ID"]))
        //                    {
        //                        //if (!string.IsNullOrEmpty(groupName))
        //                        //sbXML.Append("</" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
        //                        groupId = Convert.ToInt32(dr["GROUP_ID"]);
        //                        htmlString.Append("<tr><td colspan='3' class='black_14_b bdr_btm'><strong>" + dr["GROUP_NAME"] + "</strong></td></tr>");
        //                        sbHtmlTemp.Append("<tr><td colspan='3' class='black_14_b bdr_btm'><strong>" + dr["GROUP_NAME"] + "</strong></td></tr>");
        //                        //sbXML.Append("<" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"\" group_id =\"" + dr["GROUP_ID"] + "\" TBGUID=\"C" + RandomClass.Next() + "\" aid:pstyle=\"attribute_group\">\n");
        //                        sbXml.Append("<group_name TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"group_name\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["GROUP_NAME"] + "]]></group_name>");
        //                    }
        //                    else
        //                    {
        //                        sbXml.Append("<group_name TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"group_name\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[]]></group_name>");
        //                    }
        //                    sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["ATTRIBUTE_NAME"] + "]]></Cell>");
        //                    if (string.IsNullOrEmpty(Convert.ToString(dr["NUMERIC_VALUE"])))
        //                    {
        //                        //sbXML.Append("<" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"TEXT\" TBGUID=\"AN" + RandomClass.Next() + "\" aid:pstyle=\"attribute_name\"><![CDATA[" + dr["STRING_VALUE"] + "]]></" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
        //                        sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["STRING_VALUE"] + "]]></Cell>");
        //                    }
        //                    else
        //                    {
        //                        //sbXML.Append("<" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + "  COTYPE=\"TEXT\" TBGUID=\"AN" + RandomClass.Next() + "\" aid:pstyle=\"attribute_name\"><![CDATA[" + dr["NUMERIC_VALUE"] + "]]></" + dr["ATTRIBUTE_NAME"].ToString().Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
        //                        sbXml.Append("<Cell TBGUID=\"TPS" + _randomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr["NUMERIC_VALUE"] + "]]></Cell>");
        //                    }
        //                    htmlString.Append("<tr><td width='100'>&nbsp;</td><td width='208' class='bdr_LB'>" + dr["ATTRIBUTE_NAME"] + "</td>");
        //                    sbHtmlTemp.Append("<tr><td width='100'></td><td width='208' class='bdr_LB'>" + dr["ATTRIBUTE_NAME"] + "</td>");
        //                    const string alignVal = "Left";
        //                    string style = dr["STYLE_FORMAT"].ToString();
        //                    if (style.Length > 0)
        //                        style = style.Substring(0, style.IndexOf('[') - 1);
        //                    double dt = 0;
        //                    ExtractCurrenyFormat(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));

        //                    if ((dr["ATTRIBUTE_TYPE"].ToString() == "6") && (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM"))
        //                    {
        //                        if (dr["STRING_VALUE"].ToString().Trim().Length > 0)
        //                        {
        //                            dt = Convert.ToDouble(dr["STRING_VALUE"].ToString());
        //                        }
        //                    }
        //                    else if (dr["ATTRIBUTE_TYPE"].ToString() == "4" || dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
        //                    {
        //                        if (dr["NUMERIC_VALUE"].ToString().Trim().Length > 0)
        //                        {
        //                            dt = Convert.ToDouble(dr["NUMERIC_VALUE"].ToString());
        //                        }
        //                    }
        //                    if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
        //                    {
        //                        htmlString.Append("<TD width='208' class='bdr_LB' ALIGN=\"" + alignVal + GetProductCellString(dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;")));
        //                        sbHtmlTemp.Append("<TD width='208' class='bdr_LB' ALIGN=\"" + alignVal + GetProductCellString(dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;")));

        //                    }
        //                    else  //if (chkAttrType[j] == 4)
        //                    {
        //                        string valueFortag;
        //                        if ((_headeroptions == "All") || (_headeroptions != "All"))
        //                        {
        //                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null) &&
        //                                ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" && dr["NUMERIC_VALUE"].ToString() == string.Empty && dr["ATTRIBUTE_TYPE"].ToString() != "6") ||
        //                                (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" && dr["STRING_VALUE"].ToString() == string.Empty && dr["ATTRIBUTE_TYPE"].ToString() != "6")))
        //                            {
        //                                valueFortag = _replaceText;
        //                            }
        //                            else if ((dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" && dr["NUMERIC_VALUE"].ToString() == _emptyCondition && dr["ATTRIBUTE_TYPE"].ToString() != "6") ||
        //                                (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() != "NUM" && dr["STRING_VALUE"].ToString() == _emptyCondition && dr["ATTRIBUTE_TYPE"].ToString() != "6"))
        //                            {
        //                                valueFortag = _replaceText;
        //                            }
        //                            else if (dr["STRING_VALUE"].ToString() == _emptyCondition && dr["ATTRIBUTE_TYPE"].ToString() == "6")
        //                            {
        //                                valueFortag = _replaceText;
        //                            }
        //                            else
        //                            {
        //                                if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
        //                                {
        //                                    if ((style != "") && (dt != 0.0))
        //                                    {
        //                                        valueFortag = _prefix + dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") + _suffix;
        //                                    }
        //                                    else
        //                                    {
        //                                        valueFortag = _prefix + dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") + _suffix;
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    if (dr["STRING_VALUE"].ToString().Length > 0)
        //                                    {
        //                                        valueFortag = _prefix + dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") + _suffix;
        //                                    }
        //                                    else
        //                                    {
        //                                        valueFortag = string.Empty;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
        //                            {
        //                                //valueFortag = dt != 0.0 ? dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") : dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;");
        //                                if ((style != "") && (dt != 0.0))
        //                                {
        //                                    valueFortag = dt.ToString(style.Trim()).Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;");
        //                                }
        //                                else
        //                                {
        //                                    valueFortag = dr["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;");
        //                                }
        //                            }
        //                            else
        //                            {
        //                                valueFortag = dr["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;");
        //                            }
        //                        }

        //                        if (dr["ATTRIBUTE_DATAFORMAT"].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
        //                        {
        //                            if (dr["STRING_VALUE"].ToString().Replace("_", "").Replace("/", "").Replace(":", "").Trim().Length != 0)
        //                            {
        //                                valueFortag = dr["STRING_VALUE"].ToString();
        //                                valueFortag = valueFortag.Substring(3, 2) + "/" + valueFortag.Substring(0, 2) + "/" + valueFortag.Substring(6, 4);
        //                            }
        //                        }
        //                        if (valueFortag.EndsWith(".jpg"))
        //                        {
        //                            //htmlString.Append("<img height='150' width='200' src='" + valueFortag + "' \\></td></tr>");
        //                        }
        //                        else
        //                        {
        //                            htmlString.Append("<td width='208' class='bdr_LB'>" + valueFortag + "</td></tr>");
        //                        }
        //                        sbHtmlTemp.Append("<td width='208' class='bdr_LB'>" + valueFortag + "</td></tr>");
        //                    }
        //                }
        //                //sbXML.Append("</" + groupName.Replace(' ', '_').Replace('.', '_').Replace('(', '_').Replace(')', '_') + ">\n");
        //                sbXml.Append("</TABLE>");
        //                sbXml.Append("</attribute_group>");
        //                sbXml.Append("</tradingbell_root>");

        //                htmlString.Append("</table><br/>");
        //                sbHtmlTemp.Append("</table><br/>");
        //            }
        //        }
        //        htmlString.Append(LoadReferenceTable(familyId, ProductLevelMultipletablePreview));
        //        htmlString.Append("</body></html>");
        //        var encOutput = Encoding.UTF8;
        //        string htmlPath = HttpContext.Current.Server.MapPath("~/Content/HTML");
        //        string filePath = (htmlPath + "\\ProductPreview.html");
        //        var fileHtml = new FileStream(filePath, FileMode.Create);
        //        var strwriter = new StreamWriter(fileHtml, encOutput);
        //        htmlString = htmlString.Replace("&lt;br /&gt;", "<br>").Replace("&lt;/br&gt;", "<br>").Replace("&lt;br&gt;", "<br>").Replace("&lt;br/&gt;", "<br>");
        //        strwriter.Write(scatg + htmlString.ToString());
        //        strwriter.Close();
        //        fileHtml.Close();
        //        fileHtml = new FileStream(filePath, FileMode.Open);
        //        var strReader = new StreamReader(fileHtml);
        //        string productpreview = strReader.ReadToEnd();
        //        strReader.Close();
        //        fileHtml.Close();
        //        //MemoryStream mStream = new MemoryStream(new ASCIIEncoding().GetBytes(HTMLStr));
        //        //  _saveFileStream = scatg.Append(htmlString);
        //        //webBrowser1.Navigate(filePath);
        //        //DataSet dsXMLFromHTML = ConvertHTMLTablesToDataSet(sbHTML_Temp.ToString());
        //        //dsXMLFromHTML.WriteXml("C:\\sampleee.xml");
        //        //FileStream fs = new FileStream("C:\\sample.xml", FileMode.Create, FileAccess.Write);
        //        //StreamWriter xmlwrite = new StreamWriter(fs);
        //        //xmlwrite.Write(sbXML);
        //        //xmlwrite.Close();
        //        return productpreview;
        //    }
        //    catch (Exception objexception)
        //    {
        //        Logger.Error("Error at ProductPreview : GenerateProductGroupPreview", objexception);
        //        return string.Empty;
        //    }
        //}

        public string LoadReferenceTable(int familyId, bool FamilyLevelMultipletablePreview, string UserName)
        {
            try
            {

                CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == UserName).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = CustomerFolder.Replace("&", ""); }


                // SystemSettingsCollection SettingMembers = SystemSettingsConfiguration.GetConfig.Members;
                var finalResult = string.Empty;
                var referencetableheading = string.Empty;
                string imageSizeW = "200";
                string imageSizeH = "200";
                //bool familyLvelPreviewEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["FamilyLevelPreview"]);
                bool familyLvelPreviewEnabled = FamilyLevelMultipletablePreview;
                if (familyLvelPreviewEnabled)
                {
                    var referenceTableName = new List<string>();

                    // _ocon.SQLString = "SELECT A.TABLE_ID,B.TABLE_NAME FROM TB_REFERENCE_SECTIONS A 
                    //JOIN TB_REFERENCE_TABLE B ON A.TABLE_ID = B.TABLE_ID AND A.FAMILY_ID = " + familyId + "";
                    // DataSet dsReferenceTable = _ocon.CreateDataSet();
                    var dsReferenceTable =
                        _dbcontext.TB_REFERENCE_SECTIONS.Where(x => x.FAMILY_ID == familyId).Select(x => x).ToList();
                    if (dsReferenceTable.Any())
                    {
                        referencetableheading = "<h4>Reference Contents</h4>";
                        foreach (var tbReferenceSections in dsReferenceTable)
                        {

                            //  finalResult +="<br><br><font face=\"Arial Unicode MS\" size=2px ><b>Reference Contents</b></font>";

                            if (!referenceTableName.Contains(tbReferenceSections.TB_REFERENCE_TABLE.TABLE_NAME))
                            {
                                referenceTableName.Add(tbReferenceSections.TB_REFERENCE_TABLE.TABLE_NAME);
                                DataSet dsValues = new DataSet();
                                using (
                                    var objSqlConnection =
                                        new SqlConnection(
                                            ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString)
                                    )
                                {
                                    var objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ReferenceTable";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@TABLE_ID", SqlDbType.BigInt).Value =
                                        tbReferenceSections.TABLE_ID;
                                    objSqlConnection.Open();
                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                    objSqlDataAdapter.Fill(dsValues);
                                }
                                //  _ocon.SQLString = "EXEC STP_CATALOGSTUDIO5_ReferenceTable " + Convert.ToString(item["TABLE_ID"]) + "";
                                //  DataSet dsValues = _ocon.CreateDataSet();
                                finalResult += "<h5>" + tbReferenceSections.TB_REFERENCE_TABLE.TABLE_NAME +
                                               " : </h5>";
                                finalResult +=
                                    "<table class=\"table table-condensed table-bordered table-striped\">";
                                if (dsValues.Tables.Count > 0)
                                {
                                    foreach (DataRow item1 in dsValues.Tables[0].Rows)
                                    {
                                        finalResult += "<tr>";
                                        for (int k = 1; k < dsValues.Tables[0].Columns.Count; k++)
                                        {
                                            finalResult += "<td class='reg'>";
                                            string value = Convert.ToString(item1[k]);
                                            if (value.ToLower().EndsWith(".jpg") || value.ToLower().EndsWith(".bmp") ||
                                                value.ToLower().EndsWith(".gif") || value.ToLower().EndsWith(".png"))
                                            {
                                                string referenceTableImagePath = HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder);
                                                var chkFileVal = new FileInfo(referenceTableImagePath + value);
                                                if (chkFileVal.Exists)
                                                {
                                                    _chkSize = Image.FromFile(referenceTableImagePath + value);
                                                    Double iHeight = _chkSize.Height;
                                                    Double iWidth = _chkSize.Width;
                                                    Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                    imageSizeH = Convert.ToString(newVal.Height);
                                                    imageSizeW = Convert.ToString(newVal.Width);
                                                    if (_chkSize != null)
                                                    {
                                                        _chkSize.Dispose();
                                                    }
                                                    finalResult += "<img width='" + imageSizeW + "' height='" + imageSizeH +
                                                               "' src='../Content/ProductImages/" + CustomerFolder + value.Replace("\\", "/") + "' />";
                                                }
                                                else
                                                {
                                                    finalResult += "";
                                                }

                                            }
                                            else if (value.ToLower().EndsWith(".eps") ||
                                                value.ToLower().EndsWith(".svg") ||
                                                     value.ToLower().EndsWith(".tif") ||
                                                     value.ToLower().EndsWith(".tiff") ||
                                                     value.ToLower().EndsWith(".psd") ||
                                                     value.ToLower().EndsWith(".tga") ||
                                                     value.ToLower().EndsWith(".pcx"))
                                            {
                                                value = value.Replace("/", "\\");
                                                var chkFile =
                                                        new FileInfo(
                                                            HttpContext.Current.Server.MapPath("~/Content/ProductImages/") +
                                                            value);

                                                var chkFileVal =
                                                    new FileInfo(
                                                        HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) +
                                                        conPreviewPath +
                                                        chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) +
                                                        ".jpg");
                                                if (Directory.Exists(ImageMagickPath))
                                                {

                                                    if (!chkFileVal.Exists)
                                                    {
                                                        if (!Directory.Exists(
                                                                HttpContext.Current.Server.MapPath(
                                                                    "~/Content/ProductImages/" + CustomerFolder) +
                                                                conPreviewPath))
                                                        {
                                                            Directory.CreateDirectory(
                                                                HttpContext.Current.Server.MapPath(
                                                                    "~/Content/ProductImages/" + CustomerFolder) +
                                                                conPreviewPath);

                                                        }
                                                        string[] imgargs =
                                                        {
                                                            HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) +
                                                            value,
                                                            HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) +
                                                            conPreviewPath +
                                                            value.Substring(value.LastIndexOf('\\'),
                                                                value.LastIndexOf('.') - value.LastIndexOf('\\')) +
                                                            ".jpg"
                                                        };
                                                        if (converttype == "Image Magic")
                                                        {
                                                            Exefile = "convert.exe";
                                                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                            pStart.CreateNoWindow = true;
                                                            pStart.UseShellExecute = false;
                                                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                            while (!cmdProcess.HasExited)
                                                            {
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Exefile = "cons_rcp.exe";
                                                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                            pStart.CreateNoWindow = true;
                                                            pStart.UseShellExecute = false;
                                                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                            while (!cmdProcess.HasExited)
                                                            {
                                                            }
                                                        }
                                                    }
                                                }
                                                //var chkFileVal =
                                                //    new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") +
                                                //                 "\\temp\\ImageandAttFile" +
                                                //                 value.Substring(value.LastIndexOf('\\'),
                                                //                     value.LastIndexOf('.') - value.LastIndexOf('\\')) +
                                                //                 ".jpg");
                                                if (chkFileVal.Exists)
                                                {
                                                    _chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) +
                                                            conPreviewPath + value.Substring(value.LastIndexOf('\\'), value.LastIndexOf('.') - value.LastIndexOf('\\')) + ".jpg");
                                                    Double iHeight = _chkSize.Height;
                                                    Double iWidth = _chkSize.Width;
                                                    Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                    imageSizeH = Convert.ToString(newVal.Height);
                                                    imageSizeW = Convert.ToString(newVal.Width);
                                                    if (_chkSize != null)
                                                    {
                                                        _chkSize.Dispose();
                                                    }
                                                    finalResult += "<img width='" + imageSizeW + "' height='" + imageSizeH +
                                                               "' src='../Content/ProductImages/" + CustomerFolder + conPreviewPath +
                                                               value.Substring(value.LastIndexOf('\\'),
                                                                   value.LastIndexOf('.') - value.LastIndexOf('\\')).Replace("\\", "/") +
                                                               ".jpg' />";
                                                }
                                                else
                                                {
                                                    finalResult += "";
                                                }

                                            }
                                            else
                                            {
                                                finalResult += string.IsNullOrEmpty(value)
                                                    ? ""
                                                    : value;
                                                finalResult = finalResult.Replace("\r", "<br/>");
                                            }
                                            finalResult += "</td>";
                                        }
                                        finalResult += "</tr>";
                                    }
                                }
                                else
                                {
                                    finalResult += "<tr>";
                                    finalResult +=
                                        "<td td class='reg'><em>There is no associated values in this table</em>";
                                    finalResult += "</td>";
                                    finalResult += "</tr>";
                                }
                                finalResult += "</table>";

                            }
                        }
                    }
                }
                return referencetableheading + finalResult;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ProductPreview : LoadReferenceTable", objexception);
                return null;
            }
        }

        private void ExtractCurrenyFormat(int attributeId)
        {
            try
            {
                string attributeDataRule = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId).Select(y => y.ATTRIBUTE_DATARULE).FirstOrDefault();
                _prefix = string.Empty;
                _suffix = string.Empty;
                _emptyCondition = string.Empty;
                _replaceText = string.Empty;
                _headeroptions = string.Empty;
                if (!string.IsNullOrEmpty(attributeDataRule))
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(attributeDataRule);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    {
                        if (rootNode != null)
                        {
                            XmlNodeList xmlNodeList = rootNode.ChildNodes;

                            for (int xmlNode = 0; xmlNode < xmlNodeList.Count; xmlNode++)
                            {
                                if (xmlNodeList[xmlNode].ChildNodes.Count > 0)
                                {
                                    if (xmlNodeList[xmlNode].ChildNodes[0].LastChild != null)
                                    {
                                        _prefix = xmlNodeList[xmlNode].ChildNodes[0].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[1].LastChild != null)
                                    {
                                        _suffix = xmlNodeList[xmlNode].ChildNodes[1].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[2].LastChild != null)
                                    {
                                        _emptyCondition = xmlNodeList[xmlNode].ChildNodes[2].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[3].LastChild != null)
                                    {
                                        _replaceText = xmlNodeList[xmlNode].ChildNodes[3].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[4].LastChild != null)
                                    {
                                        _headeroptions = xmlNodeList[xmlNode].ChildNodes[4].LastChild.Value;
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ProductPreview : LoadReferenceTable", objexception);
                // return null;
            }
        }



        private string GetProductCellString(string cellData, string caption, string customerFolder)
        {

            string imageUrl = "";
            string imageSizeW = "";
            string imageSizeH = "";
            string returnstr = "";
            string imageType = "";
            var chkFile = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages") + customerFolder + cellData);
            if (chkFile.Exists)
            {
                imageType = "";
                if (chkFile.Extension.ToUpper() == ".EPS" ||
                    chkFile.Extension.ToUpper() == ".SVG" ||
                    chkFile.Extension.ToUpper() == ".TIF" ||
                    chkFile.Extension.ToUpper() == ".TIFF" ||
                    chkFile.Extension.ToUpper() == ".PSD" ||
                    chkFile.Extension.ToUpper() == ".TGA" ||
                    chkFile.Extension.ToUpper() == ".PCX")
                {
                    string[] imgargs = {HttpContext.Current.Server.MapPath("~/Content/ProductImages")  + CustomerFolder + cellData,
                                                    HttpContext.Current.Server.MapPath("~/Content/")  + CustomerFolder + "\\temp\\ImageandAttFile\\" + "\\"+chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.'))+ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg" };

                    if (converttype == "Image Magic")
                    {
                        Exefile = "convert.exe";
                        var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                        pStart.CreateNoWindow = true;
                        pStart.UseShellExecute = false;
                        pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                        while (!cmdProcess.HasExited)
                        {
                        }
                    }
                    else
                    {
                        Exefile = "cons_rcp.exe";
                        // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"")
                        var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                        pStart.CreateNoWindow = true;
                        pStart.UseShellExecute = false;
                        pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                        while (!cmdProcess.HasExited)
                        {
                        }
                    }
                    var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + CustomerFolder + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                    if (chkFileVal.Exists)
                    {
                        if (_chkSize != null) { _chkSize.Dispose(); }
                        _chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + CustomerFolder + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg");
                        Double iHeight = _chkSize.Height;
                        Double iWidth = _chkSize.Width;
                        Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                        imageSizeH = Convert.ToString(newVal.Height);
                        imageSizeW = Convert.ToString(newVal.Width);
                        if (_chkSize != null)
                        {
                            _chkSize.Dispose();
                        }
                        imageUrl = "../Content/" + CustomerFolder + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture) + ".jpg";
                        imageType = "IMAGE";
                        ValInc++;
                    }
                }
                else if (chkFile.Extension.ToUpper() == ".JPG" ||
                         chkFile.Extension.ToUpper() == ".JPEG" ||
                         chkFile.Extension.ToUpper() == ".GIF" ||
                         chkFile.Extension.ToUpper() == ".BMP" ||
                         chkFile.Extension.ToUpper() == ".PNG" ||
                         chkFile.Extension.ToUpper() == ".ICO")
                {
                    // htmlData.Append("<br><br><IMG src = \"" + userImagePath+"/" +  tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() + "\" height = 200pts width = 200pts />");
                    if (_chkSize != null) { _chkSize.Dispose(); }
                    try
                    {
                        _chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages") + CustomerFolder + cellData);
                        Double iHeight = _chkSize.Height;
                        Double iWidth = _chkSize.Width;
                        Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                        imageSizeH = Convert.ToString(newVal.Height);
                        imageSizeW = Convert.ToString(newVal.Width);
                        if (_chkSize != null)
                        {
                            _chkSize.Dispose();
                        }
                        imageUrl = "..\\Content\\ProductImages" + CustomerFolder + cellData.Trim();

                    }
                    catch (Exception)
                    {
                        //imageUrl = "..\\Content\\ProductImages\\images\\unsupportedImageformat.jpg";
                        imageUrl = "";
                        imageSizeH = "72";
                        imageSizeW = "72";
                    }
                    imageType = "IMAGE";
                }
                else if (chkFile.Extension.ToUpper() == ".AVI" ||
                         chkFile.Extension.ToUpper() == ".WMV" ||
                         chkFile.Extension.ToUpper() == ".MPG" ||
                         chkFile.Extension.ToUpper() == ".SWF")
                {
                    imageUrl = "..\\Content\\ProductImages" + CustomerFolder + cellData.Trim();
                    imageType = "MEDIA";
                }
            }
            else if (cellData == "")
            {

                returnstr = ("\" VALIGN=\"Middle\" style=\" color: Black; BACKGROUND-COLOR: white  \" >");
                returnstr = returnstr + ("  ");
                returnstr = returnstr + ("</TD>");
            }

            if (imageType == "IMAGE")
            {
                returnstr = ("\" VALIGN=\"Middle\" style=\" height:150px; color: Black; BACKGROUND-COLOR: white  \" >");
                returnstr = returnstr + ("<IMG SRC = \"" + imageUrl + "\" HEIGHT=" + imageSizeH + " WIDTH = " + imageSizeW + ">");
                returnstr = returnstr + ("<br/>");
                returnstr = returnstr + ("<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + caption + "</label>");
                returnstr = returnstr + ("</TD>");

            }
            else if (imageType == "MEDIA")
            {
                returnstr = ("\" VALIGN=\"Middle\" style=\" height:150px; color: Black; BACKGROUND-COLOR: white  \" >");
                returnstr = returnstr + ("<EMBED SRC= \"" + imageUrl + "\" height = 150pts width = 150pts AUTOPLAY=\"false\" CONTROLLER=\"true\"/>");
                returnstr = returnstr + ("<br/>");
                returnstr = returnstr + ("<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + caption + "</label>");
                returnstr = returnstr + ("</TD>");
            }
            else if (imageType == "" && cellData != "")
            {
                returnstr = ("\" VALIGN=\"Middle\" style=\" height:82px; color: Black; BACKGROUND-COLOR: white  \" >");
                //returnstr = returnstr + ("<IMG SRC = \"" + "..\\Content\\ProductImages\\images\\unsupportedImageformat.jpg " + "\" height = 82px>");
                returnstr = returnstr + ("<br/>");
                returnstr = returnstr + ("<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + caption + "</label>");
                returnstr = returnstr + ("</TD>");
            }

            return (returnstr);
        }

        public Size ScaleImage(double origHeight, double origWidth, double width, double height)
        {
            var newSize = new Size();
            double nWidth = width;
            double nHeight = height;
            double oWidth = origWidth;
            double oHeight = origHeight;

            if (origHeight > 200 || origWidth > 200)
            {
                if (oWidth > oHeight)
                {
                    double ratio = oHeight / oWidth;
                    double final = (nWidth) * ratio;
                    nHeight = (int)final;
                }
                else
                {
                    double ratio = oWidth / oHeight;
                    double final = (nHeight) * ratio;
                    nWidth = (int)final;
                }
                newSize.Height = (int)nHeight;
                newSize.Width = (int)nWidth;
            }
            else
            {
                newSize.Height = (int)origHeight;
                newSize.Width = (int)origWidth;
            }

            return newSize;
        }

        private bool Checkfamilyfilter(string sqlquery)
        {
            bool retval = false;
            var familyfilter = _dbcontext.Database.SqlQuery<int>(sqlquery).ToList();
            if (familyfilter.Count > 0)
            {
                retval = true;
            }
            return retval;
        }
        private List<FamilyFilterValue> Checkfamilyfilterattrvalue(string sqlquery)
        {
            var familyfilter = _dbcontext.Database.SqlQuery<FamilyFilterValue>(sqlquery).ToList();
            return familyfilter;
        }
        private DataTable Checkproductfilter(string sqlquery)
        {
            var productfilter = new DataTable();
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                var objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = sqlquery;
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(productfilter);
            }
            return productfilter;
        }
        public bool Isnumber(string refValue)
        {
            if (refValue == "")
            {
                return false;
            }
            //const string strReg =
            //    @"^((\d?)|(([-+]?\d+\.?\d*)|([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d+\.?\d*))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d+\.?\d*)))$";
            var retval = false;
            //var res = new Regex(strReg);
            //if (res.IsMatch(refValue))
            //{
            //    retval = true;
            //}
            //return retval;
            const string strRegx = @"^[0-9]*(\.)?[0-9]+$";
            var re = new Regex(strRegx);
            if (re.IsMatch(refValue))
            {
                retval = true;
            }
            return retval;
        }
        private void GetCurrencySymbol(string attrId)
        {
            //string sqlStr = "SELECT ATTRIBUTE_DATARULE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + attrID + "";
            //Ocon.SQLString = sqlStr;
            // DataSet dscUrrency = Ocon.CreateDataSet();
            int attributeId = Convert.ToInt32(attrId);
            var dscUrrency = _dbcontext.TB_ATTRIBUTE.Find(attributeId).ATTRIBUTE_DATARULE;
            _prefix = string.Empty; _suffix = string.Empty; _emptyCondition = string.Empty; _replaceText = string.Empty; _headeroptions = string.Empty; _fornumeric = string.Empty;
            var xmlDOc = new XmlDocument();
            if (!string.IsNullOrEmpty(dscUrrency))
            {
                xmlDOc.LoadXml(dscUrrency);
                var rootNode = xmlDOc.DocumentElement;
                {
                    if (rootNode != null)
                    {
                        var xmlNodeList = rootNode.ChildNodes;

                        for (int xmlNode = 0; xmlNode < xmlNodeList.Count; xmlNode++)
                        {
                            if (xmlNodeList[xmlNode].ChildNodes.Count > 0)
                            {
                                if (xmlNodeList[xmlNode].ChildNodes[0].LastChild != null)
                                {
                                    _prefix = xmlNodeList[xmlNode].ChildNodes[0].LastChild.Value;
                                }
                                if (xmlNodeList[xmlNode].ChildNodes[1].LastChild != null)
                                {
                                    _suffix = xmlNodeList[xmlNode].ChildNodes[1].LastChild.Value;
                                }
                                if (xmlNodeList[xmlNode].ChildNodes[2].LastChild != null)
                                {
                                    _emptyCondition = xmlNodeList[xmlNode].ChildNodes[2].LastChild.Value;
                                }
                                if (xmlNodeList[xmlNode].ChildNodes[3].LastChild != null)
                                {
                                    _replaceText = xmlNodeList[xmlNode].ChildNodes[3].LastChild.Value;
                                }
                                if (xmlNodeList[xmlNode].ChildNodes[4].LastChild != null)
                                {
                                    _headeroptions = xmlNodeList[xmlNode].ChildNodes[4].LastChild.Value;
                                }
                                if (xmlNodeList[xmlNode].ChildNodes[5].LastChild != null)
                                {
                                    _fornumeric = xmlNodeList[xmlNode].ChildNodes[5].LastChild.Value;
                                }
                            }
                        }
                    }
                }
            }


        }
        private string DecPrecisionFill(string strvalue, int noofdecimal)
        {
            if (strvalue.IndexOf(".", StringComparison.Ordinal) > 0)
            {
                if ((strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1) > 0)
                {
                    if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) == 0)
                    {
                        strvalue = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal));
                        string retVal = "";
                        for (int preadd = 0; preadd < noofdecimal; preadd++)
                        { retVal = retVal + "0"; }
                        if (retVal != "")
                        {
                            strvalue = strvalue + "." + retVal;
                        }
                    }
                    else if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) > 0)
                    {
                        if (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1).Length >= noofdecimal)
                            strvalue = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal)) + "." + (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, noofdecimal));
                    }
                }
            }
            //noofdecimal = noofdecimal + 1;
            return strvalue;
        }
        private string ApplyStyleFormat(int attributeId, string numericValue)
        {
            if (Isnumber(numericValue.Replace(",", "")))
                {
                double dt;
                var style = _dbcontext.TB_ATTRIBUTE.Find(attributeId).STYLE_FORMAT;
                int index = style.IndexOf("[", StringComparison.Ordinal);
                if (index != -1)
                {
                    style = style.Substring(0, index - 1);
                    dt = Convert.ToDouble(numericValue.Trim());
                }
                else
                {
                    dt = Convert.ToDouble(numericValue.Trim());
                    return dt.ToString(CultureInfo.InvariantCulture);
                }

                return dt.ToString(style.Trim());
            }
            return numericValue;
        }

        public string[] DataVal1;
        public string Valchk(string val, string vsize)
        {
            string dr12 = vsize.Substring(6, (vsize.Length - 6));
            dr12 = dr12.Replace("(", "").Replace(")", "");
            DataVal1 = dr12.Split(',');
            string tempValue1 = val;
            string[] tempVal1 = tempValue1.Split('.');
            if (tempVal1.Length > 1)
            {

                if ((tempVal1[0].Length > Convert.ToInt32(DataVal1[0])))
                {
                    if (DataVal1.Length > 1)
                    {
                        if (tempVal1[1].Length > Convert.ToInt32(DataVal1[1]))
                        {
                            tempValue1 = tempVal1[0].Substring(0, Convert.ToInt32(DataVal1[0])) + "." + tempVal1[1].Substring(0, Convert.ToInt32(DataVal1[1]));
                        }
                    }
                    else
                    {
                        tempValue1 = tempVal1[0].Substring(0, Convert.ToInt32(DataVal1[0])) + "." + tempVal1[1].Substring(0, Convert.ToInt32(DataVal1[0]));
                    }
                }
                else
                {
                    if (tempVal1[1].Length > Convert.ToInt32(DataVal1[1]))
                    {
                        tempValue1 = tempVal1[0] + "." + tempVal1[1].Substring(0, Convert.ToInt32(DataVal1[1]));
                    }
                    else
                    {
                        tempValue1 = tempVal1[0] + "." + tempVal1[1];
                    }

                }
            }
            else
            {
                if ((tempVal1[0].Length > Convert.ToInt32(DataVal1[0])))
                {
                    tempValue1 = tempVal1[0].Substring(0, Convert.ToInt32(DataVal1[0]));
                }
                else
                {
                    if (tempVal1[0].Length > 0)
                        tempValue1 = tempVal1[0];
                }
            }
            if (tempVal1[0].Length > 0)
            {
                if (tempValue1.Substring(tempValue1.Length - 1) == ".")
                {
                    tempValue1 = tempValue1.Substring(0, tempValue1.Length - 1);
                }
            }
            return tempValue1;
        }
        public string FamilyPreview(int catalogId, string categoryId, int familyId, int productId, int check, bool FamilyLevelMultipletablePreview, int user_id, bool EnableSubProduct)
        {


            UserName = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.a.CustomerId == user_id).Select(x => x.b.User_Name).FirstOrDefault();
            if (string.IsNullOrEmpty(UserName))
            {
                UserName = "questudio";
            }
            else
            {
                UserName = UserName.Replace("&", "").Replace(" ", "");
            }

            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == UserName).Select(x => x.a.Comments).FirstOrDefault();
            if (string.IsNullOrEmpty(CustomerFolder))
            { CustomerFolder = "/STUDIOSOFT"; }
            else
            { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }





            // To pass dynamic path location from web config   - Start
            string IsServerUpload = WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
            string pathDesignation = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
            string pathUrl = WebConfigurationManager.AppSettings["ExternalAssetDriveURL"].ToString();
            string destinationPath;
            string generatedUrl;

            if (IsServerUpload.ToUpper() == "TRUE")
            {
                destinationPath = pathDesignation + "/Content/ProductImages" + CustomerFolder;

                pathUrl = pathUrl.Replace("\\", "//");
                generatedUrl = pathUrl + "/Content/ProductImages";

            }
            else
            {
                destinationPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder);

                generatedUrl = "..//Content//ProductImages//";
            }

            //  To pass dynamic path location from web config  -  End.

            //if (!Directory.Exists(destinationPath))
            //{
            //    Directory.CreateDirectory(destinationPath);
            //}

            CategoryID = categoryId;
            var bcontinueFamily = false;
            int imgSizeCount = 0;
            List<FamilyFilterValue> fFamilyFilter = null;
            //bool fFilter1 = false;
            //bool fFilter2 = false;
            //bool fFilter3 = false;
            //string sOperand1 = string.Empty;
            //string sOperand2 = string.Empty;
            //string sOperand3 = string.Empty;

            var imagemagickpath = _dbcontext.Customer_Settings.FirstOrDefault(a => a.CustomerId == user_id);
            converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_id).Select(a => a.ConverterType).SingleOrDefault();

            if (imagemagickpath != null && converttype == "Image Magic")
            {
                ImageMagickPath = convertionPath;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
                Exefile = "convert.exe";
            }
            else
            {
                ImageMagickPath = convertionPathREA;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
                Exefile = "cons_rcp.exe";
            }
            ValInc = 0;
            CategoryLevelCheck(categoryId);
            //family filters 
            var scatalogFamiilyFilter = _dbcontext.TB_CATALOG.FirstOrDefault(x => x.CATALOG_ID == catalogId);
            string sFamiilyFilter = string.Empty;
            if (scatalogFamiilyFilter != null)
            {
                sFamiilyFilter = scatalogFamiilyFilter.FAMILY_FILTERS;
            }
            if (!string.IsNullOrEmpty(sFamiilyFilter))
            {
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(sFamiilyFilter);
                var rNode = xmlDOc.DocumentElement;
                var familySQl = new StringBuilder();
                if (rNode != null && rNode.ChildNodes.Count > 0)
                {
                    bool fFiltercheck = false;

                    var sqLstring = new StringBuilder();
                    var FilterSQL = new StringBuilder();
                    for (int i = 0; i < rNode.ChildNodes.Count; i++)
                    {


                        var tableDataSetNode = rNode.ChildNodes[i];

                        if (tableDataSetNode.HasChildNodes)
                        {
                            if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[2].InnerText = "=";
                            }
                            if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[0].InnerText = "0";
                            }
                            string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                            int chilnodeid = 0;
                            if (tableDataSetNode.ChildNodes[0].InnerText != "")
                            {
                                chilnodeid = Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText);

                            }
                            var attrtype =
                                _dbcontext.TB_ATTRIBUTE.FirstOrDefault(
                                    x => x.ATTRIBUTE_ID == chilnodeid);
                            //   Ocon.SQLString = "SELECT ATTRIBUTE_DATATYPE, ATTRIBUTE_TYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText.ToString() + "";
                            //DataSet AttrType = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                            if (attrtype != null)
                            {
                                if (attrtype.ATTRIBUTE_TYPE == 13)
                                {
                                    sqLstring.Append("SELECT FAMILY_ID FROM TB_FAMILY_KEY WHERE ATTRIBUTE_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + " AND CATALOG_ID = " + catalogId + "");
                                    FilterSQL.Append("SELECT ATTRIBUTE_ID,ATTRIBUTE_VALUE as STRING_VALUE FROM TB_FAMILY_KEY WHERE ATTRIBUTE_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + " AND CATALOG_ID = " + catalogId + "");
                                }
                                else
                                {
                                    if (attrtype.ATTRIBUTE_DATATYPE.StartsWith("Num"))
                                    {
                                        sqLstring.Append("SELECT FAMILY_ID FROM TB_FAMILY_SPECS WHERE NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + "");
                                        FilterSQL.Append("SELECT ATTRIBUTE_ID, NUMERIC_VALUE as STRING_VALUE FROM TB_FAMILY_SPECS WHERE NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + "");
                                    }
                                    else
                                    {
                                        sqLstring.Append("SELECT FAMILY_ID FROM TB_FAMILY_SPECS WHERE STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + "");
                                        FilterSQL.Append("SELECT ATTRIBUTE_ID,STRING_VALUE FROM TB_FAMILY_SPECS WHERE STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + "");
                                    }
                                }
                            }

                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                        {
                            break;
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                        {
                            sqLstring.Append(" INTERSECT \n");
                            FilterSQL.Append(" INTERSECT \n");
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                        {
                            sqLstring.Append(" UNION \n");
                            FilterSQL.Append(" UNION \n");
                        }
                    }

                    fFiltercheck = Checkfamilyfilter(sqLstring.ToString());
                    fFamilyFilter = Checkfamilyfilterattrvalue(FilterSQL.ToString());
                    //  Ocon.SQLString = FilterSQL.ToString();
                    //Ocon.SQLString =  Ocon.SQLString.Replace("SELECT FAMILY_ID", "SELECT *");

                    // FamilyFilter = Ocon.CreateDataSet(); Ocon._DBCon.Close();

                    if (fFiltercheck)
                    {
                        bcontinueFamily = true;
                    }

                }
                else
                {
                    bcontinueFamily = true;
                }
            }
            else
            {
                bcontinueFamily = true;
            }
            if (bcontinueFamily == false)
            {
                //  var tbFamily = _dbcontext.TB_FAMILY.FirstOrDefault(x => x.FAMILY_ID == familyId);
                var tbFamily = (_dbcontext.TB_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID,
                        (tps, tpf) => new { tps, tpf })
                        .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tpf.FAMILY_ID, tcp => tcp.FAMILY_ID,
                            (tcptps, tcp) => new { tcptps, tcp })
                        .Join(_dbcontext.TB_CATALOG_SECTIONS, tcatcp => tcatcp.tcp.CATEGORY_ID, tca => tca.CATEGORY_ID,
                            (tcatcp, tca) => new { tcatcp, tca }))
                        .Where(x => (x.tcatcp.tcp.CATALOG_ID == catalogId && x.tcatcp.tcp.FAMILY_ID == familyId))
                        .Select(@t => @t.tcatcp.tcptps.tps).Distinct();
                //CoreDS.HTMLPreviewTableAdapters.TB_FAMILYTableAdapter familyDet = new TradingBell.CatalogStudio.CoreDS.HTMLPreviewTableAdapters.TB_FAMILYTableAdapter();
                //familyDet.Fill(htmlPreview1.TB_FAMILY, familyId, catalogId);
                var familyPreview = "Family Preview [ " + tbFamily.FirstOrDefault().FAMILY_NAME + " ]";
            }
            if (bcontinueFamily)
            {
                //family filters end  
                CategoryLevel = CategoryLevel.Substring(0, CategoryLevel.Length - 3);

                var tbFamily = (_dbcontext.TB_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID,
                       (tps, tpf) => new { tps, tpf })
                       .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tpf.FAMILY_ID, tcp => tcp.FAMILY_ID,
                           (tcptps, tcp) => new { tcptps, tcp })
                       .Join(_dbcontext.TB_CATALOG_SECTIONS, tcatcp => tcatcp.tcp.CATEGORY_ID, tca => tca.CATEGORY_ID,
                           (tcatcp, tca) => new { tcatcp, tca }))
                       .Where(x => (x.tcatcp.tcp.CATALOG_ID == catalogId && x.tcatcp.tcp.FAMILY_ID == familyId))
                       .Select(@t => @t.tcatcp.tcptps.tps).Distinct();


                // var textFile = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/css/images/Preview.css"));
                //  string fileContents = textFile.ReadToEnd();
                //textFile.Close();
                string fileContents = "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><link href=\"/Design/assets/css/bootstrap.min.css\" rel=\"stylesheet\" /></head>";
                var catalogName = _dbcontext.TB_CATALOG.Find(catalogId).CATALOG_NAME;
                HtmlData = new StringBuilder();
                //HtmlData.Append("<HTML><head /><BODY class=\"preview\">");
                HtmlData.Append("<HTML><head /><BODY>");
                HtmlData = HtmlData.Replace("<head />", fileContents);
                HtmlData.Append("<table class=\"table table-condensed table-responsive htmlpreviewtable \" style=\"background-color: white;border-radius: 9px;\">");
                HtmlData.Append("<tr class=\"htmlpreviewtr \"><td class=\"htmlpreviewtd \"><h4 class=\"headertitle \">" + catalogName + "</h4></td></tr>");
                HtmlData.Append("<tr></tr><tr><td class=\"panel-heading1 \" style=\"\"><b>" + CategoryLevel.Replace("<", "&lt;").Replace(">", "&gt;") + "</b></td></tr>");
                HtmlData.Append("<tr></tr><tr><td><b><h4>" + tbFamily.FirstOrDefault().FAMILY_NAME.Trim().Replace("<", "&lt;").Replace(">", "&gt;") + "</h4></b></td></tr>");
                //  var familyPreview = "Family Preview [ " + tbFamily.FirstOrDefault().FAMILY_NAME + " ]";
                HtmlData.Append("<tr><td class=\"featureheader \">");

                DataTable tbfamilyspecs = familySpecsDetails(catalogId, familyId, categoryId);
                //HtmlData.Append("<TABLE  class=\"table table-responsive\" width=\"80%\">");
                //HtmlData.Append("<TABLE class=\"preview\">");
                HtmlData.Append("<TABLE class=\"table table-condensed table-responsive table-bordered table-striped 9 familypreviewtr\">");
                #region "Date Format Process"
                //DataTable TempDS = tbfamilyspecs;
                foreach (DataRow dtr in tbfamilyspecs.Rows)
                {
                    // Family Filtering
                    //bool AttributeFilter = true;
                    if (fFamilyFilter != null && fFamilyFilter.Count > 0)
                    {
                        foreach (var valuefFamilyFilter in fFamilyFilter)
                        {
                            if (string.IsNullOrEmpty(valuefFamilyFilter.STRING_VALUE) &&
                                valuefFamilyFilter.ATTRIBUTE_ID != 0)
                            {
                                var AttributeDataFormatDS = _dbcontext.TB_ATTRIBUTE.Find(valuefFamilyFilter.ATTRIBUTE_ID);

                                if (AttributeDataFormatDS.ATTRIBUTE_DATATYPE == "Date and Time")
                                {

                                    string DateValue = dtr["STRING_VALUE"].ToString();
                                    if (
                                       AttributeDataFormatDS.ATTRIBUTE_DATAFORMAT.StartsWith(
                                                "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                    {
                                        dtr["STRING_VALUE"] = DateValue.Substring(3, 2) + "/" +
                                                              DateValue.Substring(0, 2) + "/" +
                                                              DateValue.Substring(6, 4);

                                    }

                                }
                            }

                        }
                    }

                }
                #endregion

                if (fFamilyFilter != null && fFamilyFilter.Count > 0)
                {
                    for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
                    {
                        foreach (var dfr in fFamilyFilter)
                        {
                            if (dfr.ATTRIBUTE_ID == Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"]) && (dfr.STRING_VALUE == tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() ||
                                 dfr.STRING_VALUE == tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString()))
                            {
                                if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) != 9)
                                {
                                    GetCurrencySymbol(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString());
                                    string sValue = string.Empty;
                                    string dtype = tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString();
                                    int numberdecimel = 0;
                                    if (
                                        Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                                    {
                                        numberdecimel =
                                            Convert.ToInt16(
                                                dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                                    }
                                    bool styleFormat =
                                        tbfamilyspecs.Rows[rowCount]["STYLE_FORMAT"].ToString().Length > 0;
                                    if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() != "12")
                                    {
                                        int attributeId = Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"]);
                                        var attrDataType = _dbcontext.TB_ATTRIBUTE.Find(attributeId).ATTRIBUTE_DATATYPE;

                                        #region "Decimal Place Trimming"


                                        if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != null && tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != "")
                                        {

                                            if (attrDataType.StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr =
                                                    tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                // string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                attrDataType = attrDataType.Remove(0, attrDataType.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(attrDataType.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr =
                                                        tempStr.Remove(tempStr.IndexOf('.') + 1 +
                                                                       noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] = tempStr;
                                            }


                                        }


                                        #endregion

                                        if (attrDataType.StartsWith("Num"))
                                        {
                                            if (
                                                tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()
                                                    .Trim() == "13")
                                            {
                                                sValue =
                                                    tbfamilyspecs.Rows[rowCount]["STRING_VALUE"]
                                                        .ToString();
                                            }
                                            else
                                            {
                                                sValue =
                                                    tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"]
                                                        .ToString();
                                            }
                                        }
                                        else
                                        {
                                            sValue = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                        }
                                        if (sValue == "" && (_emptyCondition == "Null"))
                                        {
                                            sValue = "Null";
                                        }

                                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "" ||
                                             _emptyCondition == "0" || _emptyCondition == "0.00" ||
                                             _emptyCondition == "0.000000"))
                                        {
                                            if (_emptyCondition == "Empty")
                                            {
                                                _emptyCondition = "";
                                            }
                                            if (_replaceText != "")
                                            {
                                                if (sValue == "0")
                                                {
                                                    if (_emptyCondition == "0.000000")
                                                    {
                                                        sValue = _emptyCondition;
                                                    }
                                                }
                                                if (sValue == "Null")
                                                {
                                                    _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                                }
                                                else
                                                {
                                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                }
                                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                if (_replaceText != "")
                                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                                //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                if (Convert.ToString(_replaceText) != string.Empty)
                                                {
                                                    if (Convert.ToString(_replaceText) != sValue)
                                                    {
                                                        if (_fornumeric == "1")
                                                        {
                                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                            {
                                                                if (_headeroptions == "All")
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(_prefix +
                                                                                    _replaceText.Replace("<", "&lt;")
                                                                                        .Replace(">", "&gt;")
                                                                                        .Replace("\r\n", "<br/>")
                                                                                        .Replace("\r", "<br/>")
                                                                                        .Replace("\n", "<br/>")
                                                                                        .Replace("  ",
                                                                                            "&nbsp;&nbsp;")
                                                                                        .Replace("\t",
                                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                                    _suffix + "</font>");
                                                                    HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                                else
                                                                {
                                                                    if (rowCount == 0)
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(_prefix +
                                                                                        _replaceText.Replace("<", "&lt;")
                                                                                            .Replace(">", "&gt;")
                                                                                            .Replace("\r\n", "<br/>")
                                                                                            .Replace("\r", "<br/>")
                                                                                            .Replace("\n", "<br/>")
                                                                                            .Replace("  ",
                                                                                                "&nbsp;&nbsp;")
                                                                                            .Replace("\t",
                                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                                        _suffix + "</font>");
                                                                        HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                    else
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(
                                                                            _replaceText.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                            "</font>");
                                                                        HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                    "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (_headeroptions == "All")
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_prefix +
                                                                                _replaceText.Replace("<", "&lt;")
                                                                                    .Replace(">", "&gt;")
                                                                                    .Replace("\r\n", "<br/>")
                                                                                    .Replace("\r", "<br/>")
                                                                                    .Replace("\n", "<br/>")
                                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                                    .Replace("\t",
                                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                                _suffix + "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                            else
                                                            {
                                                                if (rowCount == 0)
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(_prefix +
                                                                                    _replaceText.Replace("<", "&lt;")
                                                                                        .Replace(">", "&gt;")
                                                                                        .Replace("\r\n", "<br/>")
                                                                                        .Replace("\r", "<br/>")
                                                                                        .Replace("\n", "<br/>")
                                                                                        .Replace("  ",
                                                                                            "&nbsp;&nbsp;")
                                                                                        .Replace("\t",
                                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                                    _suffix + "</font>");
                                                                    HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                                else
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(
                                                                        _replaceText.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                        "</font>");
                                                                    HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (_headeroptions == "All")
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(_prefix +
                                                                            _replaceText.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>");
                                                            HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                        else
                                                        {
                                                            if (rowCount == 0)
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_prefix +
                                                                                _replaceText.Replace("<", "&lt;")
                                                                                    .Replace(">", "&gt;")
                                                                                    .Replace("\r\n", "<br/>")
                                                                                    .Replace("\r", "<br/>")
                                                                                    .Replace("\n", "<br/>")
                                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                                    .Replace("\t",
                                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                                _suffix + "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                    "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _replaceText = sValue;
                                            }
                                        }
                                        else if (
                                            tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString()
                                                .Trim() != "")
                                        {
                                            HtmlData.Append("<tr><td><b>" +
                                                            tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                            if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" &&
                                                sValue != "0" && sValue != "0.000000")
                                            {
                                                if (_headeroptions == "All")
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix +
                                                                    sValue.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font>"); HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>"); HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(
                                                            sValue.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("\r\n", "<br/>")
                                                                .Replace("\r", "<br/>")
                                                                .Replace("\n", "<br/>")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                            "</font>"); HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (Isnumber(sValue))
                                                {
                                                    sValue = DecPrecisionFill(sValue, numberdecimel);
                                                    sValue = styleFormat
                                                        ? ApplyStyleFormat(
                                                            Convert.ToInt32(
                                                                tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString()), sValue.Trim())
                                                        : sValue;
                                                }
                                                if (_headeroptions == "All")
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix +
                                                                    sValue.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font>"); HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(
                                                            sValue.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("\r\n", "<br/>")
                                                                .Replace("\r", "<br/>")
                                                                .Replace("\n", "<br/>")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                            "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                }
                                            }
                                        }
                                        else if (
                                            tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString()
                                                .Trim() != "")
                                        {
                                            sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            sValue = Valchk(sValue, attrDataType);

                                            //sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            if (sValue != "")
                                            {
                                                HtmlData.Append(
                                                    "<tr><td><b>" +
                                                    tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"]
                                                        .ToString().Trim() + ": </b></td>");
                                                if (Convert.ToString(sValue) != string.Empty &&
                                                    sValue != "0.00" && sValue != "0" &&
                                                    sValue != "0.000000")
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ",
                                                                                    "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(
                                                                sValue.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("\r\n", "<br/>")
                                                                    .Replace("\r", "<br/>")
                                                                    .Replace("\n", "<br/>")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>")
                                                                        +
                                                                "</font>");
                                                            HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ",
                                                                                    "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>");
                                                            HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(
                                                                sValue.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("\r\n", "<br/>")
                                                                    .Replace("\r", "<br/>")
                                                                    .Replace("\n", "<br/>")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                "</font>");
                                                            HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                    }
                                                }
                                            }


                                        }
                                        else
                                        {


                                            if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() !=
                                                null &&
                                                tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() !=
                                                "")
                                            {
                                                if (attrDataType.StartsWith("Num"))
                                                {
                                                    string tempStr = string.Empty;
                                                    tempStr =
                                                        tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"]
                                                            .ToString();
                                                    //  string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                    attrDataType = attrDataType.Remove(0, attrDataType.IndexOf(',') + 1);
                                                    int noofdecimalplace = Convert.ToInt32(attrDataType.TrimEnd(')'));
                                                    if (noofdecimalplace != 6)
                                                    {
                                                        tempStr =
                                                            tempStr.Remove(tempStr.IndexOf('.') + 1 +
                                                                           noofdecimalplace);
                                                    }
                                                    if (noofdecimalplace == 0)
                                                    {
                                                        tempStr = tempStr.TrimEnd('.');
                                                    }
                                                    tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] =
                                                        tempStr;
                                                }


                                            }

                                            if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] != DBNull.Value)
                                            {
                                                sValue =
                                                    tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                //if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000" && sValue != "Null")
                                                if (Convert.ToString(sValue) != string.Empty &&
                                                    sValue != _emptyCondition)
                                                {

                                                    sValue = Valchk(sValue, attrDataType);

                                                    // sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                    if (Isnumber(sValue))
                                                    {
                                                        //sValue = DecPrecisionFill(sValue, numberdecimel);
                                                        sValue = styleFormat
                                                            ? ApplyStyleFormat(
                                                                Convert.ToInt32(
                                                                    tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString()), sValue.Trim())
                                                            : sValue;
                                                    }
                                                }
                                            }
                                            else
                                                sValue = "Null";
                                            //if (sValue != "")
                                            {
                                                if (Convert.ToString(sValue) != string.Empty &&
                                                    sValue != _emptyCondition && sValue != "Null")
                                                {
                                                    HtmlData.Append("<tr><td><b>" +
                                                                    tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                                    if (_headeroptions == "All")
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>"); HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>"); HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(
                                                                sValue.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("\r\n", "<br/>")
                                                                    .Replace("\r", "<br/>")
                                                                    .Replace("\n", "<br/>")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                "</font>"); HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_emptyCondition == "0" || _emptyCondition == "0.00" ||
                                                        _emptyCondition == "0.000000" || _emptyCondition == "Null" ||
                                                        _emptyCondition == "Empty" || _emptyCondition == "")
                                                    {
                                                        if (_replaceText != "")
                                                        {
                                                            _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                                        }
                                                        else
                                                        {
                                                            _replaceText = "";
                                                        }
                                                        if (_replaceText != "")
                                                        {
                                                            HtmlData.Append(
                                                                "<tr><td><b>" +
                                                                tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"]
                                                                    .ToString().Trim() + ": </b></td>");
                                                            if (_fornumeric == "1")
                                                            {
                                                                if (Isnumber(_replaceText.Replace(",", "")))
                                                                {
                                                                    if (_headeroptions == "All")
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(_prefix +
                                                                                        _replaceText.Replace("<", "&lt;")
                                                                                            .Replace(">", "&gt;")
                                                                                            .Replace("\r\n", "<br/>")
                                                                                            .Replace("\r", "<br/>")
                                                                                            .Replace("\n", "<br/>")
                                                                                            .Replace("  ",
                                                                                                "&nbsp;&nbsp;")
                                                                                            .Replace("\t",
                                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                        _suffix + "</font>"); HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                    else
                                                                    {
                                                                        if (rowCount == 0)
                                                                        {
                                                                            HtmlData.Append("<td>");
                                                                            HtmlData.Append(_prefix +
                                                                                            _replaceText.Replace("<",
                                                                                                "&lt;")
                                                                                                .Replace(">", "&gt;")
                                                                                                .Replace("\r\n", "<br/>")
                                                                                                .Replace("\r", "<br/>")
                                                                                                .Replace("\n", "<br/>")
                                                                                                .Replace("  ",
                                                                                                    "&nbsp;&nbsp;")
                                                                                                .Replace("\t",
                                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") +
                                                                                            _suffix + "</font>"); HtmlData.Append("</td>");
                                                                            HtmlData.Append("</tr>");
                                                                        }
                                                                        else
                                                                        {
                                                                            HtmlData.Append("<td>");
                                                                            HtmlData.Append(
                                                                                _replaceText.Replace("<", "&lt;")
                                                                                    .Replace(">", "&gt;")
                                                                                    .Replace("\r\n", "<br/>")
                                                                                    .Replace("\r", "<br/>")
                                                                                    .Replace("\n", "<br/>")
                                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                                    .Replace("\t",
                                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                "</font>"); HtmlData.Append("</td>");
                                                                            HtmlData.Append("</tr>");
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(
                                                                        _replaceText.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        "</font>"); HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (_headeroptions == "All")
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(_prefix +
                                                                                    _replaceText.Replace("<", "&lt;")
                                                                                        .Replace(">", "&gt;")
                                                                                        .Replace("\r\n", "<br/>")
                                                                                        .Replace("\r", "<br/>")
                                                                                        .Replace("\n", "<br/>")
                                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                                        .Replace("\t",
                                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                    _suffix + "</font>"); HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                                else
                                                                {
                                                                    if (rowCount == 0)
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(_prefix +
                                                                                        _replaceText.Replace("<", "&lt;")
                                                                                            .Replace(">", "&gt;")
                                                                                            .Replace("\r\n", "<br/>")
                                                                                            .Replace("\r", "<br/>")
                                                                                            .Replace("\n", "<br/>")
                                                                                            .Replace("  ",
                                                                                                "&nbsp;&nbsp;")
                                                                                            .Replace("\t",
                                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                        _suffix + "</font>"); HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                    else
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(
                                                                            _replaceText.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            "</font>"); HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append("<tr><td><b>" +
                                                                        tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() +
                                                                        ": </b></td>");
                                                        if (_headeroptions == "All")
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>"); HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");

                                                        }
                                                        else
                                                        {
                                                            if (rowCount == 0)
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_prefix +
                                                                                sValue.Replace("<", "&lt;")
                                                                                    .Replace(">", "&gt;")
                                                                                    .Replace("\r\n", "<br/>")
                                                                                    .Replace("\r", "<br/>")
                                                                                    .Replace("\n", "<br/>")
                                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                                    .Replace("\t",
                                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                _suffix + "</font>"); HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(
                                                                    sValue.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    "</font>"); HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    //oops
                    for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
                    {
                        //foreach (DataRow dfr in FamilyFilter.Tables[0].Rows)
                        {
                            //if (dfr["ATTRIBUTE_ID"].ToString() == tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString() &&
                            //  (dfr["STRING_VALUE"].ToString() == tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() ||
                            // dfr["STRING_VALUE"].ToString() == tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString()))
                            {
                                if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) != 9)
                                {
                                    GetCurrencySymbol(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString());
                                    string sValue = string.Empty;
                                    string dtype = tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString();
                                    int numberdecimel = 0;
                                    if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                                    {
                                        numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                                    }
                                    bool styleFormat = tbfamilyspecs.Rows[rowCount]["STYLE_FORMAT"].ToString().Length > 0;
                                    if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() != "12")
                                    {
                                        int attributeId = Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"]);
                                        string attrDatatype = _dbcontext.TB_ATTRIBUTE.Find(attributeId).ATTRIBUTE_DATATYPE;
                                        //Ocon.SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString() + "";
                                        //  DataSet AttrDatatype = Ocon.CreateDataSet();
                                        #region "Decimal Place Trimming"
                                        if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != null && tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != "")
                                        {

                                            if (attrDatatype.StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                //string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                attrDatatype = attrDatatype.Remove(0, attrDatatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(attrDatatype.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] = tempStr;
                                            }


                                        }

                                        #endregion


                                        if (attrDatatype.StartsWith("Num"))
                                        {
                                            if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() == "13")
                                            {
                                                sValue = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                            }
                                            else
                                            {
                                                sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            //oops
                                            var AttributeDataFormatDS = _dbcontext.TB_ATTRIBUTE.Find(attributeId);

                                            //   Ocon.SQLString = "select ATTRIBUTE_DATATYPE, ATTRIBUTE_DATAFORMAT from tb_attribute where ATTRIBUTE_ID=" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString() + "";
                                            // DataSet AttributeDataFormatDS = Ocon.CreateDataSet(); Ocon._DBCon.Close();

                                            if (AttributeDataFormatDS.ATTRIBUTE_DATATYPE == "Date and Time")
                                            {

                                                string b;
                                                b = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                                // b = "<pre>" + b + "</pre>";
                                                string DateValue = b;
                                                if (AttributeDataFormatDS.ATTRIBUTE_DATAFORMAT.StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                                {
                                                    // sValue = DateValue.Substring(5, 2) + "/" + DateValue.Substring(0, 2) + "/" + DateValue.Substring(6, 4);
                                                    sValue = DateValue;

                                                }
                                                else
                                                {
                                                    string a;
                                                    a = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                                    //a = "<pre>" + a + "</pre>";
                                                    sValue = a;
                                                }

                                            }
                                            else
                                            {
                                                //oops
                                                string c;
                                                c = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                                c = "<pre>" + c + "</pre>";
                                                sValue = c;
                                            }


                                        }
                                        if (sValue == "" && (_emptyCondition == "Null" || _emptyCondition == "Empty"))
                                        {
                                            if (attrDatatype == "Text" && _emptyCondition == "Null")
                                                sValue = "";

                                            if (attrDatatype.StartsWith("Number") && _emptyCondition == "Null")
                                                sValue = "Null";
                                            if (attrDatatype.StartsWith("Number") && _emptyCondition == "Empty")
                                            {
                                                sValue = "";
                                                _emptyCondition = "empty";
                                            }
                                        }


                                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000" ))
                                        {
                                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                            if (_replaceText != "")
                                            {
                                                if (sValue == "0")
                                                {
                                                    if (_emptyCondition == "0.000000")
                                                    {
                                                        sValue = _emptyCondition;
                                                    }
                                                }
                                                if (sValue == "Null")
                                                {
                                                    _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                                }
                                                else
                                                {
                                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                }
                                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                if (_replaceText != "")

                                                    //HtmlData.Append("<tr><td><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + " </b></td>");
                                                    if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() == "13")
                                                        HtmlData.Append("<tr><td class=\"k-family-key\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                                    else
                                                        HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                                //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                if (Convert.ToString(_replaceText) != string.Empty)
                                                {
                                                    if (Convert.ToString(_replaceText) != sValue)
                                                    {
                                                        if (_fornumeric == "1")
                                                        {
                                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                            {
                                                                if (_headeroptions == "All")
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(_prefix +
                                                                                    _replaceText.Replace("<", "&lt;")
                                                                                        .Replace(">", "&gt;")
                                                                                        .Replace("\r\n", "<br/>")
                                                                                        .Replace("\r", "<br/>")
                                                                                        .Replace("\n", "<br/>")
                                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                                        .Replace("\t",
                                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                                    _suffix + "</font>");
                                                                    HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                                else
                                                                {
                                                                    if (rowCount == 0)
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(_prefix +
                                                                                        _replaceText.Replace("<", "&lt;")
                                                                                            .Replace(">", "&gt;")
                                                                                            .Replace("\r\n", "<br/>")
                                                                                            .Replace("\r", "<br/>")
                                                                                            .Replace("\n", "<br/>")
                                                                                            .Replace("  ",
                                                                                                "&nbsp;&nbsp;")
                                                                                            .Replace("\t",
                                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                                        _suffix + "</font>");
                                                                        HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                    else
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(
                                                                            _replaceText.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                            "</font>");
                                                                        HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>")
                                                                                .Replace("&lt;/p&gt;", "</p>") +
                                                                    "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (_headeroptions == "All")
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_prefix +
                                                                                _replaceText.Replace("<", "&lt;")
                                                                                    .Replace(">", "&gt;")
                                                                                    .Replace("\r\n", "<br/>")
                                                                                    .Replace("\r", "<br/>")
                                                                                    .Replace("\n", "<br/>")
                                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                                    .Replace("\t",
                                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                _suffix + "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                            else
                                                            {
                                                                if (rowCount == 0)
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(_prefix +
                                                                                    _replaceText.Replace("<", "&lt;")
                                                                                        .Replace(">", "&gt;")
                                                                                        .Replace("\r\n", "<br/>")
                                                                                        .Replace("\r", "<br/>")
                                                                                        .Replace("\n", "<br/>")
                                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                                        .Replace("\t",
                                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                    _suffix + "</font>");
                                                                    HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                                else
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(
                                                                        _replaceText.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        "</font>");
                                                                    HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (_headeroptions == "All")
                                                        {
                                                            HtmlData.Append("<td>");

                                                            HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                            HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                        else
                                                        {
                                                            if (rowCount == 0)
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _replaceText = sValue;
                                            }
                                        }
                                        else if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "")
                                        //oops
                                        {

                                            // htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ":</b><br>");
                                            if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() == "13")
                                            {
                                                //HtmlData.Append("<tr><td bgcolor='gray'><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ":</b></td>");
                                                HtmlData.Append(
                                                    "<tr><td class=\"k-family-key\"><b>" +
                                                    tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() +
                                                    ": </b></td>");
                                            }
                                            else
                                            {
                                                //HtmlData.Append("<tr><td><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ":</b></td>");
                                                HtmlData.Append("<tr><td><b>" +
                                                                tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString()
                                                                    .Trim() + ": </b></td>");
                                            }

                                            if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                            {
                                                if (_headeroptions == "All")
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p style=\"margin:0;\">").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p style=\"margin:0;\">").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p style=\"margin:0;\">").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (Isnumber(sValue))
                                                {
                                                    sValue = DecPrecisionFill(sValue, numberdecimel);
                                                    sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString()), sValue.Trim()) : sValue;
                                                }
                                                if (_headeroptions == "All")
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix +
                                                                    sValue.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font>"); HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>"); HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(
                                                            sValue.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("\r\n", "<br/>")
                                                                .Replace("\r", "<br/>")
                                                                .Replace("\n", "<br/>")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") +
                                                            "</font>"); HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                }
                                            }



                                        }
                                        else if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString().Trim() != "")
                                        {


                                            sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            //int noofdecimalplace = Convert.ToInt32(attrDatatype.TrimEnd(')'));
                                            //sValue = Valchk(sValue,Convert.ToString(noofdecimalplace));

                                            //sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            if (sValue != "")
                                            {
                                                HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");

                                                if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>"); HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(
                                                                sValue.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("\r\n", "<br/>")
                                                                    .Replace("\r", "<br/>")
                                                                    .Replace("\n", "<br/>")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                "</font>"); HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>"); HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>"); HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(
                                                                sValue.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("\r\n", "<br/>")
                                                                    .Replace("\r", "<br/>")
                                                                    .Replace("\n", "<br/>")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                "</font>"); HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {

                                        int attributeId = Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"]);
                                        string attrDatatype = _dbcontext.TB_ATTRIBUTE.Find(attributeId).ATTRIBUTE_DATATYPE;
                                        if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != null && tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != "")
                                        {

                                            if (attrDatatype.StartsWith("Num"))
                                            {
                                                string tempStr = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                string attrDatatypes = attrDatatype.Remove(0, attrDatatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(attrDatatypes.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] = tempStr;
                                            }

                                        }

                                        if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] != DBNull.Value)
                                        {
                                            sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            //if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000" && sValue != "Null")
                                            if (Convert.ToString(sValue) != string.Empty && sValue != _emptyCondition)
                                            {

                                                sValue = Valchk(sValue, attrDatatype);

                                                // sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                if (Isnumber(sValue))
                                                {
                                                    //sValue = DecPrecisionFill(sValue, numberdecimel);
                                                    sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString()), sValue.Trim()) : sValue;
                                                }
                                            }
                                        }
                                        else
                                            sValue = "Null";
                                        //if (sValue != "")
                                        {

                                            if (Convert.ToString(sValue) != string.Empty && sValue != _emptyCondition && sValue != "Null")
                                            {
                                                //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");

                                                //HtmlData.Append("<tr><td><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + " </b></td>");
                                                if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() == "13")
                                                    HtmlData.Append("<tr><td class=\"k-family-key\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                                else
                                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                                if (_headeroptions == "All")
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");

                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append("<td>");

                                                        HtmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");

                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000" || _emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "")
                                                {
                                                    if (_replaceText != "")
                                                    {
                                                        _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                                    }
                                                    else
                                                    {
                                                        _replaceText = "";
                                                    }
                                                    if (_replaceText != "")
                                                    {
                                                        HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + " </b></td>");

                                                        if (_fornumeric == "1")
                                                        {
                                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                            {
                                                                if (_headeroptions == "All")
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                    HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                                else
                                                                {
                                                                    if (rowCount == 0)
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                        HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                    else
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                                        HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (_headeroptions == "All")
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                            else
                                                            {
                                                                if (rowCount == 0)
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                    HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                                else
                                                                {
                                                                    HtmlData.Append("<td>");
                                                                    HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                                    HtmlData.Append("</td>");
                                                                    HtmlData.Append("</tr>");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");

                                                    if (_headeroptions == "All")
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>"); HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>"); HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append("<td>");
                                                            HtmlData.Append(
                                                                sValue.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("\r\n", "<br/>")
                                                                    .Replace("\r", "<br/>")
                                                                    .Replace("\n", "<br/>")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                "</font>"); HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

                HtmlData.Append("</TABLE>");
                HtmlData.Append("</body>");
                HtmlData.Append("</html>");


                string userImagePath = HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder);
                string imageHtml = ""; int arrImage = 0; HtmlData.Append("<br>");
                for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
                {
                    if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) == 9)
                    {
                        if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "")
                        {
                            arrImage++;
                        }
                    }
                }
                if (arrImage > 0)
                {
                    string[] imageUrl = new string[arrImage];
                    string[] imageName = new string[arrImage];
                    string[] imagePath = new string[arrImage];
                    string[] imageType = new string[arrImage];
                    string[] imageSizeW = new string[arrImage];
                    string[] imageSizeH = new string[arrImage];
                    for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
                    {
                        if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) == 9)
                        {
                            if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "")
                            {
                                Image chkSize;
                                if (!tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Contains("&"))
                                {
                                    var chkFile = new FileInfo(destinationPath + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim());
                                    if (chkFile.Exists)
                                    {
                                        if (chkFile.Extension.ToUpper() == ".EPS" ||
                                            chkFile.Extension.ToUpper() == ".TIF" ||
                                            chkFile.Extension.ToUpper() == ".SVG" ||
                                            chkFile.Extension.ToUpper() == ".TIFF" ||
                                            chkFile.Extension.ToUpper() == ".TGA" ||
                                            chkFile.Extension.ToUpper() == ".PCX")
                                        {

                                            if (Directory.Exists(ImageMagickPath))
                                            {
                                                string file_name = tbfamilyspecs.Rows[rowCount]["family_id"] + "_" + tbfamilyspecs.Rows[rowCount]["attribute_id"] + "_" + chkFile.Name;
                                                string[] imgargs = {userImagePath+"/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim(),
                                                    HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder)  + "\\temp\\ImageandAttFile\\" + file_name   + ".jpg" };
                                                if (converttype == "Image Magic")
                                                {
                                                    Exefile = "convert.exe";
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }
                                                }
                                                else
                                                {
                                                    Exefile = "cons_rcp.exe";
                                                    // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }
                                                }
                                                var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                if (chkFileVal.Exists)
                                                {
                                                    var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                                                    if (!chkFileVal.Exists)
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                                                    imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                                                    if (!chkFileVal.Exists)
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                                    imm.Dispose();
                                                    var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                                                    asd.Delete();
                                                    //////////
                                                    chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                    int iHeight = chkSize.Height;
                                                    int iWidth = chkSize.Width;
                                                    Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                    imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                    imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                    chkSize.Dispose();
                                                    imageUrl[imgSizeCount] = "..//Content/ProductImages" + CustomerFolder + "//temp//ImageandAttFile//" + file_name + ".jpg";
                                                    imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                    imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                    imageType[imgSizeCount] = "IMAGE";
                                                    imgSizeCount++;
                                                    ValInc++;
                                                }
                                                else
                                                {
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                    if (chkFileVal.Exists)
                                                    {
                                                        var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                        #region
                                                        //imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                                        //imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                                                        //imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                                        //imm.Dispose();
                                                        //var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + chkFile.Name + ".gif");
                                                        //asd.Delete();
                                                        ////////////
                                                        //chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                        //Size newVal;
                                                        //int iHeight = chkSize.Height;
                                                        //int iWidth = chkSize.Width;
                                                        //newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                        //imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                        //imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                        //chkSize.Dispose();
                                                        //imageUrl[imgSizeCount] = "..//Content//temp//ImageandAttFile//" + file_name + ".jpg";
                                                        //imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                        //imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                        //imageType[imgSizeCount] = "IMAGE";
                                                        //imgSizeCount++;
                                                        //ValInc++;
                                                        #endregion
                                                        chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                                                        if (!chkFileVal.Exists)
                                                            imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                                                        imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                                                        chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                                                        if (!chkFileVal.Exists)
                                                            imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                                        imm.Dispose();
                                                        var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                                                        asd.Delete();
                                                        //////////
                                                        chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                        int iHeight = chkSize.Height;
                                                        int iWidth = chkSize.Width;
                                                        Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                        imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                        imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                        chkSize.Dispose();
                                                        imageUrl[imgSizeCount] = "..//Content//temp//ImageandAttFile//" + file_name + ".jpg";
                                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                        imageType[imgSizeCount] = "IMAGE";
                                                        imgSizeCount++;
                                                        ValInc++;
                                                    }
                                                }
                                            }
                                            //}
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".PSD")
                                        {
                                            if (Directory.Exists(ImageMagickPath))
                                            {
                                                string file_name = tbfamilyspecs.Rows[rowCount]["family_id"] + "_" + tbfamilyspecs.Rows[rowCount]["attribute_id"] + "_" + chkFile.Name;
                                                if (converttype == "Image Magic")
                                                {
                                                    Exefile = "convert.exe";
                                                    string[] imgargs = {userImagePath+"/" +  tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim()+"[0]",
                                                    HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder)  + "\\temp\\ImageandAttFile" +"\\"+ file_name  + ".jpg" };
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }

                                                }
                                                else
                                                {
                                                    Exefile = "cons_rcp.exe";
                                                    string[] imgargs = {userImagePath+"/" +  tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim()+"",
                                                    HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder)  + "\\temp\\ImageandAttFile" +"\\"+ file_name  + ".jpg" };
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }
                                                }
                                                var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                if (chkFileVal.Exists)
                                                {
                                                    var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                                                    if (!chkFileVal.Exists)
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                                                    imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                                                    if (!chkFileVal.Exists)
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                                    imm.Dispose();
                                                    var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                                                    asd.Delete();
                                                    //////////
                                                    chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                    int iHeight = chkSize.Height;
                                                    int iWidth = chkSize.Width;
                                                    var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                    imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                    imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);

                                                    chkSize.Dispose();
                                                    imageUrl[imgSizeCount] = "..//Content/ProductImages/" + CustomerFolder + "//temp//ImageandAttFile\\" + file_name + ".jpg";
                                                    imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                    imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                    imageType[imgSizeCount] = "IMAGE";
                                                    imgSizeCount++;
                                                    ValInc++;
                                                }
                                                else
                                                {
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                    if (chkFileVal.Exists)
                                                    {
                                                        #region
                                                        //var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                        //imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                                        //imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp");
                                                        //imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                                        //imm.Dispose();
                                                        //var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".gif");
                                                        //asd.Delete();
                                                        ////////////
                                                        //chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg");
                                                        //int iHeight = chkSize.Height;
                                                        //int iWidth = chkSize.Width;
                                                        //var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                        //imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                        //imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                        //chkSize.Dispose();
                                                        //imageUrl[imgSizeCount] = "..//Content" + "//temp//" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg";
                                                        //imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                        //imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                        //imageType[imgSizeCount] = "IMAGE";
                                                        //imgSizeCount++;
                                                        //ValInc++;
                                                        #endregion
                                                        var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                        chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                                                        if (!chkFileVal.Exists)
                                                            imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                                                        imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                                                        chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                                                        if (!chkFileVal.Exists)
                                                            imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                                        imm.Dispose();
                                                        var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                                                        asd.Delete();
                                                        //////////
                                                        chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                        int iHeight = chkSize.Height;
                                                        int iWidth = chkSize.Width;
                                                        Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                        imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                        imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                        chkSize.Dispose();
                                                        imageUrl[imgSizeCount] = "..//Content/ProductImages" + CustomerFolder + "//temp//ImageandAttFile//" + file_name + ".jpg";
                                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                        imageType[imgSizeCount] = "IMAGE";
                                                        imgSizeCount++;
                                                        ValInc++;
                                                    }
                                                }
                                            }
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".JPG" ||
                                             chkFile.Extension.ToUpper() == ".GIF" ||
                                             chkFile.Extension.ToUpper() == ".JPEG" ||
                                             chkFile.Extension.ToUpper() == ".BMP" ||
                                            chkFile.Extension.ToUpper() == ".PNG")
                                        {



                                            chkSize = Image.FromFile(destinationPath + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim());
                                            int iHeight = chkSize.Height;
                                            int iWidth = chkSize.Width;
                                            var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                            imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                            imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                            chkSize.Dispose();
                                            imageUrl[imgSizeCount] = generatedUrl + CustomerFolder + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "IMAGE";
                                            imgSizeCount++;

                                        }
                                        else if (chkFile.Extension.ToUpper() == ".PDF")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\pdf_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "pdf_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages/" + CustomerFolder + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "PDF";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".RTF")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\rtf_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "rtf_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + CustomerFolder + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "RTF";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".XLS" || chkFile.Extension.ToUpper() == ".XLSX")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\xls_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "xls_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages/" + CustomerFolder + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "XLS";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".TIF")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\tif_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "tiff_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages/" + CustomerFolder + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "TIF";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".TIFF")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\tiff_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "tiff_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "TIFF";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".INDD")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\indd_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "indd_128.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "INDD";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".DOC")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\doc_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "doc_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "DOC";
                                            imgSizeCount++;
                                        }

                                        else if (chkFile.Extension.ToUpper() == ".PSD")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\psd_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "psd_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "PSD";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".EPS")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\eps_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "eps_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "EPS";
                                            imgSizeCount++;
                                        }


                                        else if (chkFile.Extension.ToUpper() == ".AVI" ||
                                            chkFile.Extension.ToUpper() == ".WMV" ||
                                            chkFile.Extension.ToUpper() == ".MPG" ||
                                            chkFile.Extension.ToUpper() == ".SWF")
                                        {
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "MEDIA";
                                            imgSizeCount++;
                                        }
                                        else
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\uns_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "uns_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "UNS";
                                            imgSizeCount++;
                                        }
                                    }
                                    else
                                    {
                                        //imageUrl[imgSizeCount] = "..//Content//ProductImages//images//unsupportedImageformat.jpg";
                                        imageUrl[imgSizeCount] = "";
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "JPG";
                                        imgSizeCount++;
                                    }
                                }
                                else
                                {
                                    var chkFile = new FileInfo(userImagePath + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim());
                                    string imagefullpath = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                    if (chkFile.Exists)
                                    {
                                        if (chkFile.Extension.ToUpper() == ".PSD")
                                        {
                                            string[] imgargs = {userImagePath+"/" +  tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim()+"[0]",
                                                    HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder)  + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture)   + ".jpg" };
                                            converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_id).Select(a => a.ConverterType).SingleOrDefault();
                                            if (imgargs[1].Contains("&"))
                                            {
                                                imagefullpath = imgargs[1].Replace("&", "");
                                            }
                                            else
                                            {
                                                imagefullpath = imgargs[1];
                                            }
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imagefullpath + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                //var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imagefullpath + "\"");
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imagefullpath + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            var chkFileVal = new FileInfo(imagefullpath);
                                            if (chkFileVal.Exists)
                                            {
                                                chkSize = Image.FromFile(imagefullpath);
                                                int iHeight = chkSize.Height;
                                                int iWidth = chkSize.Width;
                                                var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                chkSize.Dispose();
                                                imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                                imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                imageType[imgSizeCount] = "IMAGE";
                                                imgSizeCount++;
                                                ValInc++;
                                            }
                                        }

                                        else
                                        {
                                            string[] imgargs = {userImagePath+"/" +  tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim(),
                                                   HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder)  + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture)   + ".jpg" };
                                            if (imgargs[1].Contains("&"))
                                            {
                                                imagefullpath = imgargs[1].Replace("&", "");
                                            }
                                            else
                                            {
                                                imagefullpath = imgargs[1];
                                            }
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imagefullpath + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                //var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imagefullpath + "\"");
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imagefullpath + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            var chkFileVal = new FileInfo(imagefullpath);
                                            if (chkFileVal.Exists)
                                            {
                                                chkSize = Image.FromFile(imagefullpath);
                                                int iHeight = chkSize.Height;
                                                int iWidth = chkSize.Width;
                                                var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                chkSize.Dispose();
                                                imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                                imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                imageType[imgSizeCount] = "IMAGE";
                                                imgSizeCount++;
                                                ValInc++;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                    //imageHtml = "<br><br><TABLE class=\"table table-bordered\">";
                    //imageHtml = "<br><br><TABLE class=\"preview\">";
                    imageHtml = "<TABLE class=\"table table-responsive table-condensed table-bordered table-striped 10\">";
                    int calc = 4;
                    if (imgSizeCount != 0)
                        for (int rowCount = 0; rowCount < imgSizeCount; rowCount++)
                        {
                            if ((imgSizeCount - rowCount) < 4)
                            {
                                calc = imgSizeCount - rowCount;
                            }
                            imageHtml = imageHtml + "<tr style=\"background-color: #bbbbbb;\">";
                            for (int sizeCount = 0; sizeCount < calc; sizeCount++)
                            {
                                if (imageType[rowCount + sizeCount] == "MEDIA")
                                    imageHtml = imageHtml + "<td ><EMBED SRC= \"" + imageUrl[rowCount + sizeCount] + "\" height = 200pts width = 200pts AUTOPLAY=\"false\" CONTROLLER=\"true\"/></td>";
                                else if (imageType[rowCount + sizeCount] == "PDF")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\pdf_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "RTF")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\rtf_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "XLS")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\xls_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "TIF")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\tif_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "TIFF")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\tiff_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "DOC")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\doc_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "EPS")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\eps_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "INDD")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\indd_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "PSD")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\psd_64.BMP\" height = 64pts width = 64pts /></Center></td>";

                                else if (imageType[rowCount + sizeCount] == "UNS")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\uns_64.BMP\" height = 64pts width = 64pts /></Center></td>";

                                else
                                    if (imageSizeW[rowCount + sizeCount] != null && imageSizeW[rowCount + sizeCount] != "0")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"" + imageUrl[rowCount + sizeCount] + "\" height = " + imageSizeH[rowCount + sizeCount] + "pts width = " + imageSizeW[rowCount + sizeCount] + "pts /> <p>" + imageName[rowCount + sizeCount] + "</p></Center></td>";
                                else
                                {
                                    if (!string.IsNullOrEmpty(imageUrl[rowCount + sizeCount]))
                                        imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"" + imageUrl[rowCount + sizeCount] + "\" height = 64pts width = 64pts /><p>" + imageName[rowCount + sizeCount] + "</p></Center></td>";
                                    else
                                        imageHtml = imageHtml + "";
                                }

                            }
                            imageHtml = imageHtml + "</tr><tr>";
                            for (int sizeCount = 0; sizeCount < calc; sizeCount++)
                            {
                                var index = imagePath[rowCount + sizeCount].ToString().LastIndexOf(@"\") + 1;

                                if (File.Exists(userImagePath + "/" + imagePath[rowCount + sizeCount]))
                                {


                                    imageHtml = imageHtml + "<td width=200 style=\"border-bottom: 2px solid #ddd;\"><Center><a class=\"k-link\"  target=\"_blank\" href=\"" + imageUrl[rowCount + sizeCount] + "\">" + imagePath[rowCount + sizeCount].Substring(index) + "</a></Center></td>";
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(imageUrl[rowCount + sizeCount]))
                                        imageHtml = imageHtml + "<td width=200 style=\"border-bottom: 2px solid #ddd;\" ><Center><a class=\"k-link\" target=\"_blank\" href=\"" + imageUrl[rowCount + sizeCount].Replace("<", "&lt;").Replace(">", "&gt;") + "\">" + imagePath[rowCount + sizeCount].Substring(index) + "</a></Center></td>";
                                    else
                                        imageHtml = imageHtml + "";
                                }
                            }
                            imageHtml = imageHtml + "</tr><tr>";

                            imageHtml = imageHtml + "</tr>";
                            rowCount = rowCount + 3;
                        }

                }
                if (imageHtml != "" && imgSizeCount != 0)
                    HtmlData.Append(imageHtml + "</Table>");

                // prodIDTable.Fill(htmlPreview1.ProdID, catalogId, familyID);
                string prodhtml = "";
                //if (htmlPreview1.ProdID.Rows.Count > 0)
                {
                    prodhtml = GenerateFamilyPreview(catalogId, familyId, categoryId, user_id);



                    if (prodhtml.Contains("&lt;") && prodhtml.Contains("&gt;"))
                    {
                        prodhtml = prodhtml.Replace("&lt;", "<").Replace("&gt;", ">");
                    }
                    if (prodhtml != "")
                        //oops
                        if (prodhtml.EndsWith("INVALID LAYOUT SPECIFICATION ! </P></BODY></CENTER></html>") == false)
                            try
                            {
                                if (prodhtml.Contains("<html><h6>Table Preview Not Available!</h6><html>"))
                                {
                                    HtmlData.Append("<html><h6>Table Preview Not Available!</h6><html>");
                                }
                                else if (prodhtml.Contains("<TABLE"))
                                {
                                    HtmlData.Append("<br>" + prodhtml.Substring(prodhtml.IndexOf("<TABLE"), (prodhtml.IndexOf("</TABLE") - prodhtml.IndexOf("<TABLE") + 8)));
                                }
                                else if (prodhtml.Contains("<table") && prodhtml.Contains("<h4>Products</h4>"))
                                {
                                    HtmlData.Append("<br>" + prodhtml.Substring(prodhtml.IndexOf("<table") - 18, (prodhtml.IndexOf("</table") - prodhtml.IndexOf("<table") + 26)));
                                }
                                else if (prodhtml.Contains("<table"))
                                {
                                    HtmlData.Append("<br>" + prodhtml.Substring(prodhtml.IndexOf("<table"), (prodhtml.IndexOf("</table") - prodhtml.IndexOf("<table") + 8)));
                                }
                                if (prodhtml.Contains("&amp;"))
                                {
                                    HtmlData = HtmlData.Replace("&amp;", "&");
                                }
                                //HtmlData = HtmlData.Replace("<table>", "<table class=\"table table-bordered\">");
                                //HtmlData = HtmlData.Replace("<table>", "<table class=\"preview\">");
                                //HtmlData = HtmlData.Replace("<table>", "<table>");
                            }
                            catch (Exception) { }
                }

                DataTable SubFamDS = subfamilySpecsDetails(catalogId, familyId, categoryId);

                //var multiplepreview = _dbcontext.TB_ATTRIBUTE_GROUP.Join(_dbcontext.TB_ATTRIBUTE_GROUP_SECTIONS,
                //    a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a, b })
                //    .Join(_dbcontext.TB_ATTRIBUTE_GROUP_SPECS, tcptps => tcptps.a.GROUP_ID, tcp => tcp.GROUP_ID,
                //        (tcptps, tcp) => new { tcptps, tcp })
                //    .Where(x => x.tcptps.b.CATALOG_ID == catalogId && x.tcptps.b.FAMILY_ID == familyId);

                ////DataSet multiplepreview = new DataSet();
                ////multiplepreview = Ocon.CreateDataSet();
                //if (multiplepreview.Any())
                //{
                //    HtmlData.Append("<h4>Multiple / Additional Table</h4>");
                //}

                string con = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                var attrList = new int[0];
                HtmlData.Append("</td></tr></table>");
                HtmlData.Append(LoadMultipleTable(familyId, catalogId, categoryId, FamilyLevelMultipletablePreview, UserName));
                HtmlData.Append(LoadAttributePack(familyId, catalogId, categoryId, FamilyLevelMultipletablePreview, "Product"));
                HtmlData.Append(LoadReferenceTable(familyId, FamilyLevelMultipletablePreview, UserName));
                if (SubFamDS.Rows.Count > 0)
                {
                    HtmlData.Append("subFamilyList");
                }
                for (int subFamCount = 0; subFamCount < SubFamDS.Rows.Count; subFamCount++)
                {
                    subFamilyString(Convert.ToInt32(SubFamDS.Rows[subFamCount].ItemArray[0].ToString()), catalogId, user_id);
                    HtmlData.Append(LoadMultipleTable(Convert.ToInt32(SubFamDS.Rows[subFamCount].ItemArray[0].ToString()), catalogId, categoryId, FamilyLevelMultipletablePreview, UserName));
                    HtmlData.Append(LoadReferenceTable(Convert.ToInt32(SubFamDS.Rows[subFamCount].ItemArray[0].ToString()), FamilyLevelMultipletablePreview, UserName));
                    attrList = new int[0];

                    DataSet tempdsPreview = CatalogProductPreviewX(Convert.ToInt32(catalogId), Convert.ToInt32(SubFamDS.Rows[subFamCount].ItemArray[0].ToString()), attrList, con);
                    if (tempdsPreview.Tables[0].Rows.Count != 0)
                    {
                        foreach (DataRow dr in tempdsPreview.Tables[0].Rows)
                        {
                            var objProductPreviews = new ProductPreview();
                            int prod_id = Convert.ToInt32(dr["PRODUCT_ID"].ToString());
                            bool productLevelMultipletablePreview = false;
                            string HTMLStringSubProd = objProductPreviews.GenerateSubProductFamilyPreview(catalogId, Convert.ToInt32(SubFamDS.Rows[subFamCount].ItemArray[0].ToString()), prod_id, productLevelMultipletablePreview, user_id, categoryId);
                            string Prod_ID = _dbcontext.TB_PROD_SPECS.Where(a => a.PRODUCT_ID == prod_id && a.ATTRIBUTE_ID == 1).Select(x => x.STRING_VALUE).FirstOrDefault();
                            var subprodId =
                                _dbcontext.TB_SUBPRODUCT.Where(b => b.PRODUCT_ID == prod_id)
                                    .Select(x => x.SUBPRODUCT_ID);
                            //id =\"sampleProdgrid\" 
                            if (HTMLStringSubProd != "<html><left><head></head><body><table class=\"table table-responsive table-bordered table-striped table-hover\" style=\"background-color:white;border-radius:5px\" ><tr></tr></table></body></left>")
                            {
                                if (subprodId.Any() && EnableSubProduct == true)
                                {
                                    HTMLStringSubProd =
                                        HTMLStringSubProd.Replace("<html><left><head></head><body>",
                                            "<h4>Sub Products</h4><div class=\"subproductheader \" ><h3 style=\"title \">" +
                                            Prod_ID + "</h3>").Replace("</body></left>", "</div></body></left>");

                                    // HtmlData = HtmlData.Replace("</BODY></HTML>", HTMLStringSubProd);
                                    HtmlData.Append(HTMLStringSubProd);

                                }

                                HtmlData = HtmlData.Replace("</body></left>", "</BODY></HTML>");
                                HtmlData = HtmlData.Replace("<BODY>", "<BODY style=\"background-color:white;border-radius:2px\">");
                                //HtmlData.Replace("BACKGROUND-COLOR: white", "");
                            }
                        }


                    }
                }
                if (!HtmlData.ToString().Contains("</BODY></HTML>"))
                {
                    HtmlData.Append("</BODY></HTML>");
                }
                /// StreamReader textFile = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/css") + "\\master.css");
                //string fileContents = textFile.ReadToEnd();
                //  textFile.Close();
                // fileContents = "<head><style TYPE=\"text/css\"> " + fileContents + "</style></head>";
                // HtmlData = HtmlData.Replace("<head />", fileContents);

                // var ofrm = new TradingBell.CatalogX.CatalogXfunction();
                //  var objFamilyPreview = new FamilyPreviews();

                attrList = new int[0];
                // prodhtml = GenerateFamilyPreview(catalogId, familyId, categoryId, user_id);
                if (!prodhtml.Contains("<html><h6>Table Preview Not Available!</h6><html>"))
                {

                    DataSet tempdsPreview = CatalogProductPreviewX(Convert.ToInt32(catalogId), Convert.ToInt32(familyId), attrList, con);
                    if (tempdsPreview.Tables[0].Rows.Count != 0)
                    {
                        foreach (DataRow dr in tempdsPreview.Tables[0].Rows)
                        {
                            var objProductPreviews = new ProductPreview();
                            int prod_id = Convert.ToInt32(dr["PRODUCT_ID"].ToString());
                            bool productLevelMultipletablePreview = false;
                            string HTMLStringSubProd = objProductPreviews.GenerateSubProductFamilyPreview(catalogId, familyId, prod_id, productLevelMultipletablePreview, user_id, categoryId);
                            string Prod_ID = _dbcontext.TB_PROD_SPECS.Where(a => a.PRODUCT_ID == prod_id && a.ATTRIBUTE_ID == 1).Select(x => x.STRING_VALUE).FirstOrDefault();
                            var subprodId =
                                _dbcontext.TB_SUBPRODUCT.Where(b => b.PRODUCT_ID == prod_id && b.FLAG_RECYCLE == "A")
                                    .Select(x => x.SUBPRODUCT_ID);
                            // id =\"sampleProdgrid\"
                            if (HTMLStringSubProd != "<html><left><head></head><body><table class=\"table table-responsive table-bordered table-striped table-hover\" style=\"background-color:white;border-radius:5px\" ><tr></tr></table></body></left>")
                            {
                                if (subprodId.Any() && EnableSubProduct == true)
                                {
                                    HTMLStringSubProd =
                                        HTMLStringSubProd.Replace("<html><left><head></head><body>",
                                            "<h4 class=\"title \">Sub Products</h4> <div class=\"\"><h3 class=\"subheader\">" +
                                            Prod_ID + "</h3>").Replace("</body></left>", "</div></body></left>");

                                    if (HtmlData.ToString().Contains("subFamilyList"))
                                    {
                                        HtmlData = HtmlData.Replace("subFamilyList", HTMLStringSubProd);
                                    }
                                    else
                                    {
                                        HtmlData = HtmlData.Replace("</BODY></HTML>", HTMLStringSubProd);
                                    }
                                }

                                HtmlData = HtmlData.Replace("</body></left>", "</BODY></HTML>");
                                HtmlData = HtmlData.Replace("<BODY>", "<BODY style=\"background-color:white;border-radius:2px\">");
                                //HtmlData.Replace("BACKGROUND-COLOR: white", "");
                            }
                        }


                    }
                }
                for (int subFamCount = 0; subFamCount < SubFamDS.Rows.Count; subFamCount++)
                {

                }
                System.Text.Encoding encOutput = System.Text.Encoding.UTF8;
                string filePath = "";
                if (check == 1)
                {
                    filePath = (HttpContext.Current.Server.MapPath("~/Content/HTML") + "\\FamilyPreviewInDesign.html");
                }
                else
                {
                    filePath = (HttpContext.Current.Server.MapPath("~/Content/HTML") + "\\FamilyPreview.html");
                }
                //FileStream FileHtml = new FileStream(filePath, FileMode.Create);
                //StreamWriter Strwriter = new StreamWriter(FileHtml, encOutput);
                //    HtmlData = HtmlData.Replace("&lt;br /&gt;", "<br>").Replace("&lt;/br&gt;", "<br>").Replace("&lt;br&gt;", "<br>").Replace("&lt;br/&gt;", "<br>").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>").Replace("Value empty", "").Replace("\r\n\r\n", "\r\n").Replace("\r\n", "<br/>");
                //Strwriter.Write(HtmlData.Replace("Value empty", "").ToString());
                //Strwriter.Close();
                //FileHtml.Close();
                //FileHtml = new FileStream(filePath, FileMode.Open);
                //var strReader = new StreamReader(FileHtml);
                string productpreview = HtmlData.ToString().Replace("{", "").Replace("}", "").Replace("&lt;", "<").Replace("&gt;", ">");
                //strReader.Close();
                //FileHtml.Close();
                return productpreview.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            }
            else
            {
                // this.Tag = "No Preview Available";
                return "Preview is not available";
            }

        }
        public string FamilyPreviewPTM(int catalogId, string categoryId, int familyId, int productId, int check, bool FamilyLevelMultipletablePreview, int user_id)
        {
            var bcontinueFamily = false;
            int imgSizeCount = 0;
            List<FamilyFilterValue> fFamilyFilter = null;
            //bool fFilter1 = false;
            //bool fFilter2 = false;
            //bool fFilter3 = false;
            //string sOperand1 = string.Empty;
            //string sOperand2 = string.Empty;
            //string sOperand3 = string.Empty;
            var imagemagickpath = _dbcontext.Customer_Settings.FirstOrDefault(a => a.CustomerId == user_id);
            string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_id).Select(a => a.ConverterType).SingleOrDefault();
            if (imagemagickpath != null && converttype == "Image Magic")
            {
                ImageMagickPath = convertionPath;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
            }
            else
            {
                ImageMagickPath = convertionPathREA;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
            }
            ValInc = 0;
            CategoryLevelCheck(categoryId);
            //family filters 
            var scatalogFamiilyFilter = _dbcontext.TB_CATALOG.FirstOrDefault(x => x.CATALOG_ID == catalogId);
            string sFamiilyFilter = string.Empty;
            if (scatalogFamiilyFilter != null)
            {
                sFamiilyFilter = scatalogFamiilyFilter.FAMILY_FILTERS;
            }
            if (!string.IsNullOrEmpty(sFamiilyFilter))
            {
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(sFamiilyFilter);
                var rNode = xmlDOc.DocumentElement;
                var familySQl = new StringBuilder();
                if (rNode != null && rNode.ChildNodes.Count > 0)
                {
                    bool fFiltercheck = false;

                    var sqLstring = new StringBuilder();
                    var FilterSQL = new StringBuilder();
                    for (int i = 0; i < rNode.ChildNodes.Count; i++)
                    {


                        var tableDataSetNode = rNode.ChildNodes[i];

                        if (tableDataSetNode.HasChildNodes)
                        {
                            if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[2].InnerText = "=";
                            }
                            if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[0].InnerText = "0";
                            }
                            string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                            int chilnodeid = 0;
                            if (tableDataSetNode.ChildNodes[0].InnerText != "")
                            {
                                chilnodeid = Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText);

                            }
                            var attrtype =
                                _dbcontext.TB_ATTRIBUTE.FirstOrDefault(
                                    x => x.ATTRIBUTE_ID == chilnodeid);
                            //   Ocon.SQLString = "SELECT ATTRIBUTE_DATATYPE, ATTRIBUTE_TYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText.ToString() + "";
                            //DataSet AttrType = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                            if (attrtype != null)
                            {
                                if (attrtype.ATTRIBUTE_TYPE == 13)
                                {
                                    sqLstring.Append("SELECT FAMILY_ID FROM TB_FAMILY_KEY WHERE ATTRIBUTE_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + " AND CATALOG_ID = " + catalogId + "");
                                    FilterSQL.Append("SELECT ATTRIBUTE_ID,ATTRIBUTE_VALUE as STRING_VALUE FROM TB_FAMILY_KEY WHERE ATTRIBUTE_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + " AND CATALOG_ID = " + catalogId + "");
                                }
                                else
                                {
                                    if (attrtype.ATTRIBUTE_DATATYPE.StartsWith("Num"))
                                    {
                                        sqLstring.Append("SELECT FAMILY_ID FROM TB_FAMILY_SPECS WHERE NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + "");
                                        FilterSQL.Append("SELECT ATTRIBUTE_ID, NUMERIC_VALUE as STRING_VALUE FROM TB_FAMILY_SPECS WHERE NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + "");
                                    }
                                    else
                                    {
                                        sqLstring.Append("SELECT FAMILY_ID FROM TB_FAMILY_SPECS WHERE STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + "");
                                        FilterSQL.Append("SELECT ATTRIBUTE_ID,STRING_VALUE FROM TB_FAMILY_SPECS WHERE STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + " AND FAMILY_ID =" + familyId + "");
                                    }
                                }
                            }

                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                        {
                            break;
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                        {
                            sqLstring.Append(" INTERSECT \n");
                            FilterSQL.Append(" INTERSECT \n");
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                        {
                            sqLstring.Append(" UNION \n");
                            FilterSQL.Append(" UNION \n");
                        }
                    }

                    fFiltercheck = Checkfamilyfilter(sqLstring.ToString());
                    fFamilyFilter = Checkfamilyfilterattrvalue(sqLstring.ToString());
                    //  Ocon.SQLString = FilterSQL.ToString();
                    //Ocon.SQLString =  Ocon.SQLString.Replace("SELECT FAMILY_ID", "SELECT *");

                    // FamilyFilter = Ocon.CreateDataSet(); Ocon._DBCon.Close();

                    if (fFiltercheck)
                    {
                        bcontinueFamily = true;
                    }

                }
                else
                {
                    bcontinueFamily = true;
                }
            }
            else
            {
                bcontinueFamily = true;
            }
            if (bcontinueFamily == false)
            {
                //  var tbFamily = _dbcontext.TB_FAMILY.FirstOrDefault(x => x.FAMILY_ID == familyId);
                var tbFamily = (_dbcontext.TB_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID,
                        (tps, tpf) => new { tps, tpf })
                        .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tpf.FAMILY_ID, tcp => tcp.FAMILY_ID,
                            (tcptps, tcp) => new { tcptps, tcp })
                        .Join(_dbcontext.TB_CATALOG_SECTIONS, tcatcp => tcatcp.tcp.CATEGORY_ID, tca => tca.CATEGORY_ID,
                            (tcatcp, tca) => new { tcatcp, tca }))
                        .Where(x => (x.tcatcp.tcp.CATALOG_ID == catalogId && x.tcatcp.tcp.FAMILY_ID == familyId))
                        .Select(@t => @t.tcatcp.tcptps.tps).Distinct();
                //CoreDS.HTMLPreviewTableAdapters.TB_FAMILYTableAdapter familyDet = new TradingBell.CatalogStudio.CoreDS.HTMLPreviewTableAdapters.TB_FAMILYTableAdapter();
                //familyDet.Fill(htmlPreview1.TB_FAMILY, familyId, catalogId);
                var familyPreview = "Family Preview [ " + tbFamily.FirstOrDefault().FAMILY_NAME + " ]";
            }
            if (bcontinueFamily)
            {
                //family filters end                
                CategoryLevel = CategoryLevel.Substring(0, CategoryLevel.Length - 3);
                var tbFamily = (_dbcontext.TB_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID,
                       (tps, tpf) => new { tps, tpf })
                       .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tpf.FAMILY_ID, tcp => tcp.FAMILY_ID,
                           (tcptps, tcp) => new { tcptps, tcp })
                       .Join(_dbcontext.TB_CATALOG_SECTIONS, tcatcp => tcatcp.tcp.CATEGORY_ID, tca => tca.CATEGORY_ID,
                           (tcatcp, tca) => new { tcatcp, tca }))
                       .Where(x => (x.tcatcp.tcp.CATALOG_ID == catalogId && x.tcatcp.tcp.FAMILY_ID == familyId))
                       .Select(@t => @t.tcatcp.tcptps.tps).Distinct();
                // var textFile = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/css/images/Preview.css"));
                //  string fileContents = textFile.ReadToEnd();
                //textFile.Close();
                string fileContents = "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\" /></head>";
                var catalogName = _dbcontext.TB_CATALOG.Find(catalogId).CATALOG_NAME;
                HtmlData = new StringBuilder();
                //HtmlData.Append("<HTML><head /><BODY class=\"preview\">");
                HtmlData.Append("<HTML><head /><BODY>");
                HtmlData = HtmlData.Replace("<head />", fileContents);
                HtmlData.Append("<br/><h4>" + catalogName + "</h4>");
                HtmlData.Append("<br/><b>" + CategoryLevel.Replace("<", "&lt;").Replace(">", "&gt;") + "</b>");
                HtmlData.Append("<br><b><h4>" + tbFamily.FirstOrDefault().FAMILY_NAME.Trim().Replace("<", "&lt;").Replace(">", "&gt;") + "</h4></b>");
                //  var familyPreview = "Family Preview [ " + tbFamily.FirstOrDefault().FAMILY_NAME + " ]";
                DataTable tbfamilyspecs = familySpecsDetails(catalogId, familyId, CategoryID);
                //HtmlData.Append("<TABLE  class=\"table table-responsive\" width=\"80%\">");
                //HtmlData.Append("<TABLE class=\"preview\">");
                //HtmlData.Append("<TABLE class=\"table table-responsive table-condensed table-bordered table-striped 1\">");
                HtmlData.Append("<TABLE class=\"table table-condensed table-responsive table-bordered table-striped 9 familypreviewtr\">");

                #region "Date Format Process"
                //DataTable TempDS = tbfamilyspecs;
                foreach (DataRow dtr in tbfamilyspecs.Rows)
                {
                    // Family Filtering
                    //bool AttributeFilter = true;
                    if (fFamilyFilter != null && fFamilyFilter.Count > 0)
                    {
                        foreach (var valuefFamilyFilter in fFamilyFilter)
                        {
                            if (string.IsNullOrEmpty(valuefFamilyFilter.STRING_VALUE) &&
                                valuefFamilyFilter.ATTRIBUTE_ID != 0)
                            {
                                var AttributeDataFormatDS = _dbcontext.TB_ATTRIBUTE.Find(valuefFamilyFilter.ATTRIBUTE_ID);

                                if (AttributeDataFormatDS.ATTRIBUTE_DATATYPE == "Date and Time")
                                {
                                    string DateValue = dtr["STRING_VALUE"].ToString();
                                    if (
                                       AttributeDataFormatDS.ATTRIBUTE_DATAFORMAT.StartsWith(
                                                "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                    {
                                        dtr["STRING_VALUE"] = DateValue.Substring(3, 2) + "/" +
                                                              DateValue.Substring(0, 2) + "/" +
                                                              DateValue.Substring(6, 4);

                                    }

                                }
                            }

                        }
                    }

                }
                #endregion

                if (fFamilyFilter != null && fFamilyFilter.Count > 0)
                {
                    for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
                    {
                        foreach (var dfr in fFamilyFilter)
                        {
                            if (dfr.ATTRIBUTE_ID == Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"]) && (dfr.STRING_VALUE == tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() ||
                                 dfr.STRING_VALUE == tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString()))
                            {
                                if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) != 9)
                                {
                                    GetCurrencySymbol(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString());
                                    string sValue = string.Empty;
                                    string dtype = tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString();
                                    int numberdecimel = 0;
                                    if (
                                        Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                                    {
                                        numberdecimel =
                                            Convert.ToInt16(
                                                dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                                    }
                                    bool styleFormat =
                                        tbfamilyspecs.Rows[rowCount]["STYLE_FORMAT"].ToString().Length > 0;
                                    if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() != "12")
                                    {
                                        int attributeId = Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"]);
                                        var attrDataType = _dbcontext.TB_ATTRIBUTE.Find(attributeId).ATTRIBUTE_DATATYPE;

                                        #region "Decimal Place Trimming"


                                        if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != null && tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != "")
                                        {

                                            if (attrDataType.StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr =
                                                    tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                // string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                attrDataType = attrDataType.Remove(0, attrDataType.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(attrDataType.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr =
                                                        tempStr.Remove(tempStr.IndexOf('.') + 1 +
                                                                       noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] = tempStr;
                                            }


                                        }


                                        #endregion


                                        if (attrDataType.StartsWith("Num"))
                                        {
                                            if (
                                                tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()
                                                    .Trim() == "13")
                                            {
                                                sValue =
                                                    tbfamilyspecs.Rows[rowCount]["STRING_VALUE"]
                                                        .ToString();
                                            }
                                            else
                                            {
                                                sValue =
                                                    tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"]
                                                        .ToString();
                                            }
                                        }
                                        else
                                        {
                                            sValue = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                        }
                                        if (sValue == "" && (_emptyCondition == "Null"))
                                        {
                                            sValue = "Null";
                                        }

                                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" ||  _emptyCondition == "" ||
                                             _emptyCondition == "0" || _emptyCondition == "0.00" ||
                                             _emptyCondition == "0.000000"))
                                        {
                                            if (_emptyCondition == "Empty")
                                            {
                                                _emptyCondition = "";
                                            }
                                            if (_replaceText != "")
                                            {
                                                if (sValue == "0")
                                                {
                                                    if (_emptyCondition == "0.000000")
                                                    {
                                                        sValue = _emptyCondition;
                                                    }
                                                }
                                                if (sValue == "Null")
                                                {
                                                    _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                                }
                                                else
                                                {
                                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                }
                                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                if (_replaceText != "")
                                                    HtmlData.Append("<tr><td><b>" +
                                                                    tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() +
                                                                    ": </b></td>");
                                                //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                if (Convert.ToString(_replaceText) != string.Empty)
                                                {
                                                    if (Convert.ToString(_replaceText) != sValue)
                                                    {
                                                        if (_fornumeric == "1")
                                                        {
                                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                            {
                                                                if (_headeroptions == "All")
                                                                    HtmlData.Append(_prefix +
                                                                                    _replaceText.Replace("<", "&lt;")
                                                                                        .Replace(">", "&gt;")
                                                                                        .Replace("\r\n", "<br/>")
                                                                                        .Replace("\r", "<br/>")
                                                                                        .Replace("\n", "<br/>")
                                                                                        .Replace("  ",
                                                                                            "&nbsp;&nbsp;")
                                                                                        .Replace("\t",
                                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                    _suffix + "</font>");
                                                                else
                                                                {
                                                                    if (rowCount == 0)
                                                                    {
                                                                        HtmlData.Append(_prefix +
                                                                                        _replaceText.Replace("<",
                                                                                            "&lt;")
                                                                                            .Replace(">", "&gt;")
                                                                                            .Replace("\r\n", "<br/>")
                                                                                            .Replace("\r", "<br/>")
                                                                                            .Replace("\n", "<br/>")
                                                                                            .Replace("  ",
                                                                                                "&nbsp;&nbsp;")
                                                                                            .Replace("\t",
                                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                        _suffix + "</font>");
                                                                    }
                                                                    else
                                                                    {
                                                                        HtmlData.Append(
                                                                            _replaceText.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            "</font>");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                                HtmlData.Append(
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    "</font>");
                                                        }
                                                        else
                                                        {
                                                            if (_headeroptions == "All")
                                                                HtmlData.Append(_prefix +
                                                                                _replaceText.Replace("<", "&lt;")
                                                                                    .Replace(">", "&gt;")
                                                                                    .Replace("\r\n", "<br/>")
                                                                                    .Replace("\r", "<br/>")
                                                                                    .Replace("\n", "<br/>")
                                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                                    .Replace("\t",
                                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                _suffix + "</font>");
                                                            else
                                                            {
                                                                if (rowCount == 0)
                                                                {
                                                                    HtmlData.Append(_prefix +
                                                                                    _replaceText.Replace("<", "&lt;")
                                                                                        .Replace(">", "&gt;")
                                                                                        .Replace("\r\n", "<br/>")
                                                                                        .Replace("\r", "<br/>")
                                                                                        .Replace("\n", "<br/>")
                                                                                        .Replace("  ",
                                                                                            "&nbsp;&nbsp;")
                                                                                        .Replace("\t",
                                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                    _suffix + "</font>");
                                                                }
                                                                else
                                                                {
                                                                    HtmlData.Append(
                                                                        _replaceText.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        "</font>");
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (_headeroptions == "All")
                                                            HtmlData.Append(_prefix +
                                                                            _replaceText.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>");
                                                        else
                                                        {
                                                            if (rowCount == 0)
                                                            {
                                                                HtmlData.Append(_prefix +
                                                                                _replaceText.Replace("<", "&lt;")
                                                                                    .Replace(">", "&gt;")
                                                                                    .Replace("\r\n", "<br/>")
                                                                                    .Replace("\r", "<br/>")
                                                                                    .Replace("\n", "<br/>")
                                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                                    .Replace("\t",
                                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                _suffix + "</font>");
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append(
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    "</font>");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _replaceText = sValue;
                                            }
                                        }
                                        else if (
                                            tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString()
                                                .Trim() != "")
                                        {
                                            HtmlData.Append("<tr><td><b>" +
                                                            tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                            if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" &&
                                                sValue != "0" && sValue != "0.000000")
                                            {
                                                if (_headeroptions == "All")
                                                    HtmlData.Append(_prefix +
                                                                    sValue.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font>");
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>");
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append(
                                                            sValue.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("\r\n", "<br/>")
                                                                .Replace("\r", "<br/>")
                                                                .Replace("\n", "<br/>")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                            "</font>");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (Isnumber(sValue))
                                                {
                                                    sValue = DecPrecisionFill(sValue, numberdecimel);
                                                    sValue = styleFormat
                                                        ? ApplyStyleFormat(
                                                            Convert.ToInt32(
                                                                tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString()), sValue.Trim())
                                                        : sValue;
                                                }
                                                if (_headeroptions == "All")
                                                    HtmlData.Append(_prefix +
                                                                    sValue.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font>");
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>");
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append(
                                                            sValue.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("\r\n", "<br/>")
                                                                .Replace("\r", "<br/>")
                                                                .Replace("\n", "<br/>")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                            "</font>");
                                                    }
                                                }
                                            }
                                        }
                                        else if (
                                            tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString()
                                                .Trim() != "")
                                        {
                                            sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            sValue = Valchk(sValue, attrDataType);

                                            //sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            if (sValue != "")
                                            {
                                                HtmlData.Append(
                                                    "<tr><td><b>" +
                                                    tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"]
                                                        .ToString().Trim() + ": </b></td>");
                                                if (Convert.ToString(sValue) != string.Empty &&
                                                    sValue != "0.00" && sValue != "0" &&
                                                    sValue != "0.000000")
                                                {
                                                    if (_headeroptions == "All")
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>");
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ",
                                                                                    "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append(
                                                                sValue.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("\r\n", "<br/>")
                                                                    .Replace("\r", "<br/>")
                                                                    .Replace("\n", "<br/>")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                "</font>");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>");
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ",
                                                                                    "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append(
                                                                sValue.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("\r\n", "<br/>")
                                                                    .Replace("\r", "<br/>")
                                                                    .Replace("\n", "<br/>")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                "</font>");
                                                        }
                                                    }
                                                }
                                            }


                                        }
                                        else
                                        {


                                            if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() !=
                                                null &&
                                                tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() !=
                                                "")
                                            {
                                                if (attrDataType.StartsWith("Num"))
                                                {
                                                    string tempStr = string.Empty;
                                                    tempStr =
                                                        tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"]
                                                            .ToString();
                                                    //  string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                    attrDataType = attrDataType.Remove(0, attrDataType.IndexOf(',') + 1);
                                                    int noofdecimalplace = Convert.ToInt32(attrDataType.TrimEnd(')'));
                                                    if (noofdecimalplace != 6)
                                                    {
                                                        tempStr =
                                                            tempStr.Remove(tempStr.IndexOf('.') + 1 +
                                                                           noofdecimalplace);
                                                    }
                                                    if (noofdecimalplace == 0)
                                                    {
                                                        tempStr = tempStr.TrimEnd('.');
                                                    }
                                                    tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] =
                                                        tempStr;
                                                }


                                            }

                                            if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] != DBNull.Value)
                                            {
                                                sValue =
                                                    tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                //if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000" && sValue != "Null")
                                                if (Convert.ToString(sValue) != string.Empty &&
                                                    sValue != _emptyCondition)
                                                {

                                                    sValue = Valchk(sValue, attrDataType);

                                                    // sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                    if (Isnumber(sValue))
                                                    {
                                                        //sValue = DecPrecisionFill(sValue, numberdecimel);
                                                        sValue = styleFormat
                                                            ? ApplyStyleFormat(
                                                                Convert.ToInt32(
                                                                    tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString()), sValue.Trim())
                                                            : sValue;
                                                    }
                                                }
                                            }
                                            else
                                                sValue = "Null";
                                            //if (sValue != "")
                                            {
                                                if (Convert.ToString(sValue) != string.Empty &&
                                                    sValue != _emptyCondition && sValue != "Null")
                                                {
                                                    HtmlData.Append("<tr><td><b>" +
                                                                    tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                                    if (_headeroptions == "All")
                                                        HtmlData.Append(_prefix +
                                                                        sValue.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        _suffix + "</font>");
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append(
                                                                sValue.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("\r\n", "<br/>")
                                                                    .Replace("\r", "<br/>")
                                                                    .Replace("\n", "<br/>")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                "</font>");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "" ||
                                                        _emptyCondition == "0.000000" || _emptyCondition == "Null" ||
                                                        _emptyCondition == "Empty" || _emptyCondition == "")
                                                    {
                                                        if (_replaceText != "")
                                                        {
                                                            _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                                        }
                                                        else
                                                        {
                                                            _replaceText = "";
                                                        }
                                                        if (_replaceText != "")
                                                        {
                                                            HtmlData.Append(
                                                                "<br><b>" +
                                                                tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"]
                                                                    .ToString().Trim() + ": </b><br>");
                                                            if (_fornumeric == "1")
                                                            {
                                                                if (Isnumber(_replaceText.Replace(",", "")))
                                                                {
                                                                    if (_headeroptions == "All")
                                                                    {
                                                                        HtmlData.Append(_prefix +
                                                                                        _replaceText.Replace("<", "&lt;")
                                                                                            .Replace(">", "&gt;")
                                                                                            .Replace("\r\n", "<br/>")
                                                                                            .Replace("\r", "<br/>")
                                                                                            .Replace("\n", "<br/>")
                                                                                            .Replace("  ",
                                                                                                "&nbsp;&nbsp;")
                                                                                            .Replace("\t",
                                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                        _suffix + "</font>");
                                                                    }
                                                                    else
                                                                    {
                                                                        if (rowCount == 0)
                                                                        {
                                                                            HtmlData.Append(_prefix +
                                                                                            _replaceText.Replace("<",
                                                                                                "&lt;")
                                                                                                .Replace(">", "&gt;")
                                                                                                .Replace("\r\n", "<br/>")
                                                                                                .Replace("\r", "<br/>")
                                                                                                .Replace("\n", "<br/>")
                                                                                                .Replace("  ",
                                                                                                    "&nbsp;&nbsp;")
                                                                                                .Replace("\t",
                                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                            _suffix + "</font>");
                                                                        }
                                                                        else
                                                                        {
                                                                            HtmlData.Append(
                                                                                _replaceText.Replace("<", "&lt;")
                                                                                    .Replace(">", "&gt;")
                                                                                    .Replace("\r\n", "<br/>")
                                                                                    .Replace("\r", "<br/>")
                                                                                    .Replace("\n", "<br/>")
                                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                                    .Replace("\t",
                                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                "</font>");
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    HtmlData.Append(
                                                                        _replaceText.Replace("<", "&lt;")
                                                                            .Replace(">", "&gt;")
                                                                            .Replace("\r\n", "<br/>")
                                                                            .Replace("\r", "<br/>")
                                                                            .Replace("\n", "<br/>")
                                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                                            .Replace("\t",
                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                        "</font>");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (_headeroptions == "All")
                                                                {
                                                                    HtmlData.Append(_prefix +
                                                                                    _replaceText.Replace("<", "&lt;")
                                                                                        .Replace(">", "&gt;")
                                                                                        .Replace("\r\n", "<br/>")
                                                                                        .Replace("\r", "<br/>")
                                                                                        .Replace("\n", "<br/>")
                                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                                        .Replace("\t",
                                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                    _suffix + "</font>");
                                                                }
                                                                else
                                                                {
                                                                    if (rowCount == 0)
                                                                    {
                                                                        HtmlData.Append(_prefix +
                                                                                        _replaceText.Replace("<", "&lt;")
                                                                                            .Replace(">", "&gt;")
                                                                                            .Replace("\r\n", "<br/>")
                                                                                            .Replace("\r", "<br/>")
                                                                                            .Replace("\n", "<br/>")
                                                                                            .Replace("  ",
                                                                                                "&nbsp;&nbsp;")
                                                                                            .Replace("\t",
                                                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                        _suffix + "</font>");
                                                                    }
                                                                    else
                                                                    {
                                                                        HtmlData.Append(
                                                                            _replaceText.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            "</font>");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append("<br><b>" +
                                                                        tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() +
                                                                        ": </b><br>");
                                                        if (_headeroptions == "All")
                                                            HtmlData.Append(_prefix +
                                                                            sValue.Replace("<", "&lt;")
                                                                                .Replace(">", "&gt;")
                                                                                .Replace("\r\n", "<br/>")
                                                                                .Replace("\r", "<br/>")
                                                                                .Replace("\n", "<br/>")
                                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                                .Replace("\t",
                                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                            _suffix + "</font>");
                                                        else
                                                        {
                                                            if (rowCount == 0)
                                                            {
                                                                HtmlData.Append(_prefix +
                                                                                sValue.Replace("<", "&lt;")
                                                                                    .Replace(">", "&gt;")
                                                                                    .Replace("\r\n", "<br/>")
                                                                                    .Replace("\r", "<br/>")
                                                                                    .Replace("\n", "<br/>")
                                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                                    .Replace("\t",
                                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                                _suffix + "</font>");
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append(
                                                                    sValue.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("\r\n", "<br/>")
                                                                        .Replace("\r", "<br/>")
                                                                        .Replace("\n", "<br/>")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    "</font>");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
                    {
                        //foreach (DataRow dfr in FamilyFilter.Tables[0].Rows)
                        {
                            //if (dfr["ATTRIBUTE_ID"].ToString() == tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString() &&
                            //  (dfr["STRING_VALUE"].ToString() == tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() ||
                            // dfr["STRING_VALUE"].ToString() == tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString()))
                            {
                                if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) != 9)
                                {
                                    GetCurrencySymbol(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString());
                                    string sValue = string.Empty;
                                    string dtype = tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString();
                                    int numberdecimel = 0;
                                    if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                                    {
                                        numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                                    }
                                    bool styleFormat = tbfamilyspecs.Rows[rowCount]["STYLE_FORMAT"].ToString().Length > 0;
                                    if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() != "12")
                                    {
                                        int attributeId = Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"]);
                                        string attrDatatype = _dbcontext.TB_ATTRIBUTE.Find(attributeId).ATTRIBUTE_DATATYPE;
                                        //Ocon.SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString() + "";
                                        //  DataSet AttrDatatype = Ocon.CreateDataSet();
                                        #region "Decimal Place Trimming"
                                        if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != null && tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != "")
                                        {

                                            if (attrDatatype.StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                //string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                attrDatatype = attrDatatype.Remove(0, attrDatatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(attrDatatype.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] = tempStr;
                                            }


                                        }

                                        #endregion


                                        if (attrDatatype.StartsWith("Num"))
                                        {
                                            if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() == "13")
                                            {
                                                sValue = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                            }
                                            else
                                            {
                                                sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            var AttributeDataFormatDS = _dbcontext.TB_ATTRIBUTE.Find(attributeId);

                                            //   Ocon.SQLString = "select ATTRIBUTE_DATATYPE, ATTRIBUTE_DATAFORMAT from tb_attribute where ATTRIBUTE_ID=" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString() + "";
                                            // DataSet AttributeDataFormatDS = Ocon.CreateDataSet(); Ocon._DBCon.Close();

                                            if (AttributeDataFormatDS.ATTRIBUTE_DATATYPE == "Date and Time")
                                            {
                                                string DateValue = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                                if (AttributeDataFormatDS.ATTRIBUTE_DATAFORMAT.StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                                {
                                                    sValue = DateValue.Substring(3, 2) + "/" + DateValue.Substring(0, 2) + "/" + DateValue.Substring(6, 4);

                                                }
                                                else
                                                {
                                                    sValue = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                                }

                                            }
                                            else
                                            {
                                                sValue = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                                            }


                                        }
                                        if (sValue == "" && (_emptyCondition == "Null" || _emptyCondition == "Empty"))
                                        {
                                            if (attrDatatype == "Text" && _emptyCondition == "Null")
                                                sValue = "";

                                            if (attrDatatype.StartsWith("Number") && _emptyCondition == "Null")
                                                sValue = "Null";
                                            if (attrDatatype.StartsWith("Number") && _emptyCondition == "Empty")
                                            {
                                                sValue = "";
                                                _emptyCondition = "empty";
                                            }
                                        }


                                        if ((_emptyCondition == "Null" ||  _emptyCondition == "" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                                        {
                                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                            if (_replaceText != "")
                                            {
                                                if (sValue == "0")
                                                {
                                                    if (_emptyCondition == "0.000000")
                                                    {
                                                        sValue = _emptyCondition;
                                                    }
                                                }
                                                if (sValue == "Null")
                                                {
                                                    _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                                }
                                                else
                                                {
                                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                }
                                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                if (_replaceText != "")

                                                    //HtmlData.Append("<tr><td><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + " </b></td>");
                                                    if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() == "13")
                                                        HtmlData.Append("<tr><td class=\"k-family-key\">" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + "</td>");
                                                    else
                                                        HtmlData.Append("<tr><td>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + "</td>");
                                                //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                if (Convert.ToString(_replaceText) != string.Empty)
                                                {
                                                    if (Convert.ToString(_replaceText) != sValue)
                                                    {
                                                        if (_fornumeric == "1")
                                                        {
                                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                            {
                                                                if (_headeroptions == "All")
                                                                    HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                else
                                                                {
                                                                    if (rowCount == 0)
                                                                    {
                                                                        HtmlData.Append("<td>");
                                                                        HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                        HtmlData.Append("</td>");
                                                                        HtmlData.Append("</tr>");
                                                                    }
                                                                    else
                                                                    {
                                                                        HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                                HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                        }
                                                        else
                                                        {
                                                            if (_headeroptions == "All")
                                                                HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                            else
                                                            {
                                                                if (rowCount == 0)
                                                                {
                                                                    HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                }
                                                                else
                                                                {
                                                                    HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (_headeroptions == "All")
                                                        {
                                                            HtmlData.Append("<td>");

                                                            HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                            HtmlData.Append("</td>");
                                                            HtmlData.Append("</tr>");
                                                        }
                                                        else
                                                        {
                                                            if (rowCount == 0)
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append("<td>");
                                                                HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                                HtmlData.Append("</td>");
                                                                HtmlData.Append("</tr>");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _replaceText = sValue;
                                            }
                                        }
                                        else if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "")
                                        {

                                            // htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ":</b><br>");
                                            if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() == "13")
                                            {
                                                //HtmlData.Append("<tr><td bgcolor='gray'><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ":</b></td>");
                                                HtmlData.Append("<tr><td class=\"k-family-key\">" +
                                                                tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString()
                                                                    .Trim() + "</td>");
                                            }
                                            else
                                            {
                                                //HtmlData.Append("<tr><td><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ":</b></td>");
                                                HtmlData.Append("<tr><td><b>" +
                                                                 tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString()
                                                                    .Trim() + "</td>");
                                            }

                                            if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                            {
                                                if (_headeroptions == "All")
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (Isnumber(sValue))
                                                {
                                                    sValue = DecPrecisionFill(sValue, numberdecimel);
                                                    sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString()), sValue.Trim()) : sValue;
                                                }
                                                if (_headeroptions == "All")
                                                    HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                    }
                                                }
                                            }



                                        }
                                        else if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString().Trim() != "")
                                        {


                                            sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            sValue = Valchk(sValue, attrDatatype);

                                            //sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            if (sValue != "")
                                            {
                                                HtmlData.Append("<br><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");

                                                if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                {
                                                    if (_headeroptions == "All")
                                                        HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                        HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {

                                        int attributeId = Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"]);
                                        string attrDatatype = _dbcontext.TB_ATTRIBUTE.Find(attributeId).ATTRIBUTE_DATATYPE;
                                        if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != null && tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString() != "")
                                        {

                                            if (attrDatatype.StartsWith("Num"))
                                            {
                                                string tempStr = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                string attrDatatypes = attrDatatype.Remove(0, attrDatatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(attrDatatypes.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] = tempStr;
                                            }

                                        }

                                        if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] != DBNull.Value)
                                        {
                                            sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                            //if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000" && sValue != "Null")
                                            if (Convert.ToString(sValue) != string.Empty && sValue != _emptyCondition)
                                            {

                                                sValue = Valchk(sValue, attrDatatype);

                                                // sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();
                                                if (Isnumber(sValue))
                                                {
                                                    //sValue = DecPrecisionFill(sValue, numberdecimel);
                                                    sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString()), sValue.Trim()) : sValue;
                                                }
                                            }
                                        }
                                        else
                                            sValue = "Null";
                                        //if (sValue != "")
                                        {

                                            if (Convert.ToString(sValue) != string.Empty && sValue != _emptyCondition && sValue != "Null")
                                            {
                                                //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");

                                                //HtmlData.Append("<tr><td><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + " </b></td>");
                                                if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() == "13")
                                                    HtmlData.Append("<tr><td class=\"k-family-key\">" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + "</td>");
                                                else
                                                    HtmlData.Append("<tr><td>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + "</td>");
                                                if (_headeroptions == "All")
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    if (rowCount == 0)
                                                    {
                                                        HtmlData.Append("<td>");
                                                        HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");

                                                    }
                                                    else
                                                    {
                                                        HtmlData.Append("<td>");

                                                        HtmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                        HtmlData.Append("</td>");
                                                        HtmlData.Append("</tr>");

                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000" || _emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "")
                                                {
                                                    if (_replaceText != "")
                                                    {
                                                        _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                                    }
                                                    else
                                                    {
                                                        _replaceText = "";
                                                    }
                                                    if (_replaceText != "")
                                                    {
                                                        HtmlData.Append("<br><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");

                                                        if (_fornumeric == "1")
                                                        {
                                                            if (Isnumber(_replaceText.Replace(",", "")))
                                                            {
                                                                if (_headeroptions == "All")
                                                                {
                                                                    HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                }
                                                                else
                                                                {
                                                                    if (rowCount == 0)
                                                                    {
                                                                        HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                    }
                                                                    else
                                                                    {
                                                                        HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (_headeroptions == "All")
                                                            {
                                                                HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                            }
                                                            else
                                                            {
                                                                if (rowCount == 0)
                                                                {
                                                                    HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                                }
                                                                else
                                                                {
                                                                    HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<br><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");

                                                    if (_headeroptions == "All")
                                                        HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    else
                                                    {
                                                        if (rowCount == 0)
                                                        {
                                                            HtmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                        }
                                                        else
                                                        {
                                                            HtmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

                HtmlData.Append("</TABLE>");
                HtmlData.Append("</body>");
                HtmlData.Append("</html>");


                string userImagePath = HttpContext.Current.Server.MapPath("~/Content/ProductImages");
                string imageHtml = ""; int arrImage = 0; HtmlData.Append("<br>");
                for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
                {
                    if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) == 9)
                    {
                        if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "")
                        {
                            arrImage++;
                        }
                    }
                }
                if (arrImage > 0)
                {
                    string[] imageUrl = new string[arrImage];
                    string[] imageName = new string[arrImage];
                    string[] imagePath = new string[arrImage];
                    string[] imageType = new string[arrImage];
                    string[] imageSizeW = new string[arrImage];
                    string[] imageSizeH = new string[arrImage];
                    for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
                    {
                        if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) == 9)
                        {
                            if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "")
                            {
                                Image chkSize;
                                if (!tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Contains("&"))
                                {
                                    //var filename = new FileInfo(userImagePath + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim());
                                    //if (filename.Name.Contains("."))
                                    //{ 
                                    //tbfamilyspecs.Rows[rowCount]["STRING_VALUE"]=filename.Name.Replace(filename.Extension,"");
                                    //}
                                    var chkFile = new FileInfo(userImagePath + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim());
                                    //tbfamilyspecs.Rows[rowCount]["STRING_VALUE"] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"] + filename.Extension;
                                    if (chkFile.Exists)
                                    {
                                        if (chkFile.Extension.ToUpper() == ".EPS" ||
                                            chkFile.Extension.ToUpper() == ".SVG" ||
                                            chkFile.Extension.ToUpper() == ".TIF" ||
                                            chkFile.Extension.ToUpper() == ".TIFF" ||
                                            chkFile.Extension.ToUpper() == ".TGA" ||
                                            chkFile.Extension.ToUpper() == ".PCX")
                                        {

                                            if (Directory.Exists(ImageMagickPath))
                                            {

                                                string[] imgargs = {userImagePath+"/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim(),
                                                    HttpContext.Current.Server.MapPath("~/Content/")  + "\\temp\\ImageandAttFile" + chkFile.Name   + ".jpg" };

                                                if (converttype == "Image Magic")
                                                {
                                                    Exefile = "convert.exe";
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }
                                                }
                                                else
                                                {
                                                    Exefile = "cons_rcp.exe";
                                                    // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"")
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }
                                                }
                                                var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name + ".jpg");
                                                if (chkFileVal.Exists)
                                                {
                                                    var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name + ".jpg");
                                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                                    imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp");
                                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                                    imm.Dispose();
                                                    var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".gif");
                                                    asd.Delete();
                                                    //////////
                                                    chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg");
                                                    int iHeight = chkSize.Height;
                                                    int iWidth = chkSize.Width;
                                                    Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                    imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                    imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                    chkSize.Dispose();
                                                    imageUrl[imgSizeCount] = "..//Content//temp//" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg";
                                                    imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                    imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                    imageType[imgSizeCount] = "IMAGE";
                                                    imgSizeCount++;
                                                    ValInc++;
                                                }
                                                else
                                                {
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                    if (chkFileVal.Exists)
                                                    {
                                                        var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                                        imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp");
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                                        imm.Dispose();
                                                        var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".gif");
                                                        asd.Delete();
                                                        //////////
                                                        chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg");
                                                        Size newVal;
                                                        int iHeight = chkSize.Height;
                                                        int iWidth = chkSize.Width;
                                                        newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                        imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                        imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                        chkSize.Dispose();
                                                        imageUrl[imgSizeCount] = "..//Content//temp//" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg";
                                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                        imageType[imgSizeCount] = "IMAGE";
                                                        imgSizeCount++;
                                                        ValInc++;
                                                    }
                                                }
                                            }
                                            //}
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".PSD")
                                        {
                                            if (Directory.Exists(ImageMagickPath))
                                            {
                                                string[] imgargs = {userImagePath+"/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim()+"[0]",
                                                    HttpContext.Current.Server.MapPath("~/Content/")  + "\\temp\\ImageandAttFile" + chkFile.Name   + ".jpg" };
                                                if (converttype == "Image Magic")
                                                {
                                                    Exefile = "convert.exe";
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }
                                                }
                                                else
                                                {
                                                    Exefile = "cons_rcp.exe";
                                                    // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"")
                                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                    pStart.CreateNoWindow = true;
                                                    pStart.UseShellExecute = false;
                                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                    System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                    while (!cmdProcess.HasExited)
                                                    {
                                                    }
                                                }
                                                var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name + ".jpg");
                                                if (chkFileVal.Exists)
                                                {
                                                    var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name + ".jpg");
                                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                                    imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp");
                                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                                    imm.Dispose();
                                                    var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".gif");
                                                    asd.Delete();
                                                    //////////
                                                    chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg");
                                                    int iHeight = chkSize.Height;
                                                    int iWidth = chkSize.Width;
                                                    var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                    imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                    imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);

                                                    chkSize.Dispose();
                                                    imageUrl[imgSizeCount] = "..//Content" + "//temp//" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg";
                                                    imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                    imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                    imageType[imgSizeCount] = "IMAGE";
                                                    imgSizeCount++;
                                                    ValInc++;
                                                }
                                                else
                                                {
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                    if (chkFileVal.Exists)
                                                    {
                                                        var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                                        imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp");
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                                        imm.Dispose();
                                                        var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".gif");
                                                        asd.Delete();
                                                        //////////
                                                        chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg");
                                                        int iHeight = chkSize.Height;
                                                        int iWidth = chkSize.Width;
                                                        var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                        imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                        imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                        chkSize.Dispose();
                                                        imageUrl[imgSizeCount] = "..//Content" + "//temp//" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg";
                                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                        imageType[imgSizeCount] = "IMAGE";
                                                        imgSizeCount++;
                                                        ValInc++;
                                                    }
                                                }
                                            }
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".JPG" ||
                                             chkFile.Extension.ToUpper() == ".GIF" ||
                                             chkFile.Extension.ToUpper() == ".JPEG" ||
                                             chkFile.Extension.ToUpper() == ".BMP" ||
                                            chkFile.Extension.ToUpper() == ".PNG")
                                        {
                                            chkSize = Image.FromFile(userImagePath + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim());
                                            int iHeight = chkSize.Height;
                                            int iWidth = chkSize.Width;
                                            var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                            imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                            imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                            chkSize.Dispose();
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "IMAGE";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".PDF")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\pdf_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "pdf_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "PDF";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".RTF")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\rtf_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "rtf_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "RTF";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".XLS" || chkFile.Extension.ToUpper() == ".XLSX")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\xls_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "xls_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "XLS";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".TIF")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\tif_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "tiff_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "TIF";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".TIFF")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\tiff_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "tiff_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "TIFF";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".INDD")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\indd_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "indd_128.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "INDD";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".DOC")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\doc_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "doc_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "DOC";
                                            imgSizeCount++;
                                        }

                                        else if (chkFile.Extension.ToUpper() == ".PSD")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\psd_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "psd_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "PSD";
                                            imgSizeCount++;
                                        }
                                        else if (chkFile.Extension.ToUpper() == ".EPS")
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\eps_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "eps_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "EPS";
                                            imgSizeCount++;
                                        }


                                        else if (chkFile.Extension.ToUpper() == ".AVI" ||
                                            chkFile.Extension.ToUpper() == ".WMV" ||
                                            chkFile.Extension.ToUpper() == ".MPG" ||
                                            chkFile.Extension.ToUpper() == ".SWF")
                                        {
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "MEDIA";
                                            imgSizeCount++;
                                        }
                                        else
                                        {
                                            string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\uns_64.BMP";
                                            var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "uns_64.png");
                                            rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                            imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                            imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                            imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                            imageType[imgSizeCount] = "UNS";
                                            imgSizeCount++;
                                        }
                                    }
                                    else
                                    {
                                        //imageUrl[imgSizeCount] = "..//Content//ProductImages//images//unsupportedImageformat.jpg";
                                        imageUrl[imgSizeCount] = "";
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "JPG";
                                        imgSizeCount++;
                                    }
                                }
                                else
                                {
                                    var chkFile = new FileInfo(userImagePath + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim());
                                    string imagefullpath = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                    if (chkFile.Exists)
                                    {
                                        if (chkFile.Extension.ToUpper() == ".PSD")
                                        {
                                            string[] imgargs = {userImagePath+"/" +  tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim()+"[0]",
                                                    HttpContext.Current.Server.MapPath("~/Content/")  + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture)   + ".jpg" };
                                            if (imgargs[1].Contains("&"))
                                            {
                                                imagefullpath = imgargs[1].Replace("&", "");
                                            }
                                            else
                                            {
                                                imagefullpath = imgargs[1];
                                            }
                                            if (converttype == "Image Magic")
                                            { Exefile = "convert.exe"; }
                                            else { Exefile = "cons_rcp.exe"; }
                                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imagefullpath + "\"");
                                            pStart.CreateNoWindow = true;
                                            pStart.UseShellExecute = false;
                                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                            while (!cmdProcess.HasExited)
                                            {
                                            }
                                            var chkFileVal = new FileInfo(imagefullpath);
                                            if (chkFileVal.Exists)
                                            {
                                                chkSize = Image.FromFile(imagefullpath);
                                                int iHeight = chkSize.Height;
                                                int iWidth = chkSize.Width;
                                                var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                chkSize.Dispose();
                                                imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                                imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                imageType[imgSizeCount] = "IMAGE";
                                                imgSizeCount++;
                                                ValInc++;
                                            }
                                        }

                                        else
                                        {
                                            string[] imgargs = {userImagePath+"/" +  tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim(),
                                                   HttpContext.Current.Server.MapPath("~/Content/")  + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ValInc.ToString(CultureInfo.InvariantCulture)   + ".jpg" };
                                            if (imgargs[1].Contains("&"))
                                            {
                                                imagefullpath = imgargs[1].Replace("&", "");
                                            }
                                            else
                                            {
                                                imagefullpath = imgargs[1];
                                            }
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imagefullpath + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"")
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imagefullpath + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            var chkFileVal = new FileInfo(imagefullpath);
                                            if (chkFileVal.Exists)
                                            {
                                                chkSize = Image.FromFile(imagefullpath);
                                                int iHeight = chkSize.Height;
                                                int iWidth = chkSize.Width;
                                                var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                chkSize.Dispose();
                                                imageUrl[imgSizeCount] = "..//Content//ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                                imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                imageType[imgSizeCount] = "IMAGE";
                                                imgSizeCount++;
                                                ValInc++;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                    //imageHtml = "<br><br><TABLE class=\"table table-bordered\">";
                    //imageHtml = "<br><br><TABLE class=\"preview\">";
                    imageHtml = "<TABLE class=\"table table-responsive table-condensed table-bordered table-striped 2\">";
                    int calc = 4;
                    if (imgSizeCount != 0)
                        for (int rowCount = 0; rowCount < imgSizeCount; rowCount++)
                        {
                            if ((imgSizeCount - rowCount) < 4)
                            {
                                calc = imgSizeCount - rowCount;
                            }
                            imageHtml = imageHtml + "<tr>";
                            for (int sizeCount = 0; sizeCount < calc; sizeCount++)
                            {
                                if (imageType[rowCount + sizeCount] == "MEDIA")
                                    imageHtml = imageHtml + "<td ><EMBED SRC= \"" + imageUrl[rowCount + sizeCount] + "\" height = 200pts width = 200pts AUTOPLAY=\"false\" CONTROLLER=\"true\"/></td>";
                                else if (imageType[rowCount + sizeCount] == "PDF")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\pdf_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "RTF")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\rtf_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "XLS")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\xls_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "TIF")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\tif_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "TIFF")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\tiff_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "DOC")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\doc_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "EPS")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\eps_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "INDD")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\indd_64.BMP\" height = 64pts width = 64pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "PSD")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\psd_64.BMP\" height = 64pts width = 64pts /></Center></td>";

                                else if (imageType[rowCount + sizeCount] == "UNS")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\uns_64.BMP\" height = 64pts width = 64pts /></Center></td>";

                                else
                                    if (imageSizeW[rowCount + sizeCount] != null && imageSizeW[rowCount + sizeCount] != "0")
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"" + imageUrl[rowCount + sizeCount] + "\" height = " + imageSizeH[rowCount + sizeCount] + "pts width = " + imageSizeW[rowCount + sizeCount] + "pts /> <p>" + imageName[rowCount + sizeCount] + "</p></Center></td>";
                                else
                                    imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"" + imageUrl[rowCount + sizeCount] + "\" height = 64pts width = 64pts /><p>" + imageName[rowCount + sizeCount] + "</p></Center></td>";

                            }
                            imageHtml = imageHtml + "</tr><tr>";
                            for (int sizeCount = 0; sizeCount < calc; sizeCount++)
                            {
                                var index = imagePath[rowCount + sizeCount].ToString().LastIndexOf(@"\") + 1;

                                if (File.Exists(userImagePath + "/" + imagePath[rowCount + sizeCount]))
                                {


                                    imageHtml = imageHtml + "<td width=200 ><Center><a class=\"k-link\"  target=\"_blank\" href=\"" + imageUrl[rowCount + sizeCount] + "\">" + imagePath[rowCount + sizeCount].Substring(index) + "</a></Center></td>";
                                }
                                else
                                {
                                    imageHtml = imageHtml + "<td width=200 ><Center><a class=\"k-link\" target=\"_blank\" href=\"" + imageUrl[rowCount + sizeCount].Replace("<", "&lt;").Replace(">", "&gt;") + "\">" + imagePath[rowCount + sizeCount].Substring(index) + "</a></Center></td>";
                                }
                            }
                            imageHtml = imageHtml + "</tr><tr>";

                            imageHtml = imageHtml + "</tr>";
                            rowCount = rowCount + 3;
                        }

                }
                if (imageHtml != "" && imgSizeCount != 0)
                    HtmlData.Append(imageHtml + "</Table>");
                HtmlData.Clear();
                // prodIDTable.Fill(htmlPreview1.ProdID, catalogId, familyID);
                string prodhtml = "";
                //if (htmlPreview1.ProdID.Rows.Count > 0)
                {
                    prodhtml = GenerateFamilyPreview(catalogId, familyId, categoryId, user_id);
                    if (prodhtml.Contains("&lt;") && prodhtml.Contains("&gt;"))
                    {
                        prodhtml = prodhtml.Replace("&lt;", "&amp;lt;").Replace("&gt;", "&amp;gt;");
                    }
                    if (prodhtml != "")
                        if (prodhtml.EndsWith("INVALID LAYOUT SPECIFICATION ! </P></BODY></CENTER></html>") == false)
                            try
                            {
                                if (prodhtml.Contains("<html><h6>Table Preview Not Available!</h6><html>"))
                                {
                                    HtmlData.Append("<html><h6>Table Preview Not Available!</h6><html>");
                                }
                                else if (prodhtml.Contains("<TABLE"))
                                {
                                    HtmlData.Append("<br>" + prodhtml.Substring(prodhtml.IndexOf("<TABLE"), (prodhtml.IndexOf("</TABLE") - prodhtml.IndexOf("<TABLE") + 8)));
                                }
                                else if (prodhtml.Contains("<table"))
                                {
                                    HtmlData.Append("<br>" + prodhtml.Substring(prodhtml.IndexOf("<table"), (prodhtml.IndexOf("</table") - prodhtml.IndexOf("<table") + 8)));
                                }
                                if (prodhtml.Contains("&amp;"))
                                {
                                    HtmlData = HtmlData.Replace("&amp;", "&");
                                }
                                //HtmlData = HtmlData.Replace("<table>", "<table class=\"table table-bordered\">");
                                //HtmlData = HtmlData.Replace("<table>", "<table class=\"preview\">");
                                //HtmlData = HtmlData.Replace("<table>", "<table>");
                            }
                            catch (Exception) { }
                }

                DataTable SubFamDS = subfamilySpecsDetails(catalogId, familyId, categoryId);

                //var multiplepreview = _dbcontext.TB_ATTRIBUTE_GROUP.Join(_dbcontext.TB_ATTRIBUTE_GROUP_SECTIONS,
                //    a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a, b })
                //    .Join(_dbcontext.TB_ATTRIBUTE_GROUP_SPECS, tcptps => tcptps.a.GROUP_ID, tcp => tcp.GROUP_ID,
                //        (tcptps, tcp) => new { tcptps, tcp })
                //    .Where(x => x.tcptps.b.CATALOG_ID == catalogId && x.tcptps.b.FAMILY_ID == familyId);

                ////DataSet multiplepreview = new DataSet();
                ////multiplepreview = Ocon.CreateDataSet();
                //if (multiplepreview.Any())
                //{
                //    HtmlData.Append("<h4>Multiple / Additional Table</h4>");
                //}
                //////////////////////HtmlData.Append(LoadMultipleTable(familyId, catalogId, FamilyLevelMultipletablePreview));
                //////////////////////HtmlData.Append(LoadReferenceTable(familyId, FamilyLevelMultipletablePreview));
                //////////////////////for (int subFamCount = 0; subFamCount < SubFamDS.Rows.Count; subFamCount++)
                //////////////////////{
                //////////////////////    subFamilyString(Convert.ToInt32(SubFamDS.Rows[subFamCount].ItemArray[0].ToString()), catalogId);
                //////////////////////    HtmlData.Append(LoadMultipleTable(Convert.ToInt32(SubFamDS.Rows[subFamCount].ItemArray[0].ToString()), catalogId, FamilyLevelMultipletablePreview));
                //////////////////////    HtmlData.Append(LoadReferenceTable(Convert.ToInt32(SubFamDS.Rows[subFamCount].ItemArray[0].ToString()), FamilyLevelMultipletablePreview));
                //////////////////////}
                HtmlData.Append("</BODY></HTML>");
                /// StreamReader textFile = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/css") + "\\master.css");
                //string fileContents = textFile.ReadToEnd();
                //  textFile.Close();
                // fileContents = "<head><style TYPE=\"text/css\"> " + fileContents + "</style></head>";
                // HtmlData = HtmlData.Replace("<head />", fileContents);

                System.Text.Encoding encOutput = System.Text.Encoding.UTF8;
                string filePath = "";
                if (check == 1)
                {
                    filePath = (HttpContext.Current.Server.MapPath("~/Content/HTML") + "\\FamilyPreviewInDesign.html");
                }
                else
                {
                    filePath = (HttpContext.Current.Server.MapPath("~/Content/HTML") + "\\FamilyPreview.html");
                }
                FileStream FileHtml = new FileStream(filePath, FileMode.Create);
                StreamWriter Strwriter = new StreamWriter(FileHtml, encOutput);
                HtmlData = HtmlData.Replace("&lt;br /&gt;", "<br>").Replace("&lt;/br&gt;", "<br>").Replace("&lt;br&gt;", "<br>").Replace("&lt;br/&gt;", "<br>").Replace("\r\n\r\n", "\r\n").Replace("\r\n", "<br/>");
                Strwriter.Write(HtmlData.Replace("Value empty", "").ToString());
                Strwriter.Close();
                FileHtml.Close();
                FileHtml = new FileStream(filePath, FileMode.Open);
                var strReader = new StreamReader(FileHtml);
                string productpreview = strReader.ReadToEnd();
                strReader.Close();
                FileHtml.Close();
                return productpreview.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            }
            else
            {
                // this.Tag = "No Preview Available";
                return "Preview is not available";
            }

        }


        private void subFamilyString(int subFamilyID, int catalogId, int user_id)
        {
            int imgSizeCount = 0;
            var tbFamily = (_dbcontext.TB_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID,
                    (tps, tpf) => new { tps, tpf })
                    .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tpf.FAMILY_ID, tcp => tcp.FAMILY_ID,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Join(_dbcontext.TB_CATALOG_SECTIONS, tcatcp => tcatcp.tcp.CATEGORY_ID, tca => tca.CATEGORY_ID,
                        (tcatcp, tca) => new { tcatcp, tca }))
                    .Where(x => (x.tcatcp.tcp.CATALOG_ID == catalogId && x.tcatcp.tcp.FAMILY_ID == subFamilyID))
                    .Select(@t => @t.tcatcp.tcptps.tps).Distinct();

            var firstOrDefault = tbFamily.FirstOrDefault();
            if (firstOrDefault != null)
                HtmlData.Append("<div style=\"background-color:white;border-radius:9px\"><div align=\"Lleft\" style=\"padding: 5px;background-color: bbbbbb;margin-bottom: -15px;\"><h4>" + firstOrDefault.FAMILY_NAME.Trim().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>") + "</h4></div>");
            //  CoreDS.HTMLPreviewTableAdapters.FAMLIY_SPECSTableAdapter familySpecsDetails = new TradingBell.CatalogStudio.CoreDS.HTMLPreviewTableAdapters.FAMLIY_SPECSTableAdapter();
            //   familySpecsDetails.Fill(tbfamilyspecs, catalogID, subFamilyID);
            DataTable tbfamilyspecs = familySpecsDetails(catalogId, subFamilyID, CategoryID);
            HtmlData.Append("<table class=\"table table-responsive table-condensed table-bordered table-striped 3\" style=\"background-color:white;border-radius:9px\">");
            for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
            {
                if ((Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) != 9) && (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) != 12))
                {
                    if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "" && tbfamilyspecs.Rows[rowCount]["STRING_VALUE"] != DBNull.Value)
                    {
                        GetCurrencySymbol(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString());
                        // string sValue = string.Empty;
                        // string dtype = tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString();
                        //int numberdecimel = 0;
                        //if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                        //{
                        //    numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                        //}
                        // bool styleFormat = tbfamilyspecs.Rows[rowCount]["STYLE_FORMAT"].ToString().Length > 0;
                        string sValue = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString();
                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                if (sValue == "0")
                                {
                                    if (_emptyCondition == "0.000000")
                                    {
                                        sValue = _emptyCondition;
                                    }
                                }
                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                if (_replaceText != "")
                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                //HtmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                                //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                if (Convert.ToString(_replaceText) != string.Empty)
                                {
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                    }
                                    else
                                    {

                                        if (_headeroptions == "All")
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");

                                            }
                                            else
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");

                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _replaceText = sValue;
                                if (Convert.ToString(_replaceText) != string.Empty)
                                {
                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_prefix +
                                                            _replaceText.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                            _suffix + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(
                                                    _replaceText.Replace("<", "&lt;")
                                                        .Replace(">", "&gt;")
                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                        .Replace("\t",
                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                    "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "" && tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "0")
                        {
                            if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                            {
                                // HtmlData.Append("<br><TABLE class=\"table table-condensed table-bordered\"><tr><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br></tr></TABLE>");
                                if (tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString().Trim() == "13")
                                {
                                    HtmlData.Append("<tr><td class=\"k-family-key\"><b>" +
                                                    tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() +
                                                    ": </b></td>");
                                }
                                else
                                {
                                    HtmlData.Append("<tr><td><b>" +
                                                   tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() +
                                                   ": </b></td>");
                                }
                                if (_headeroptions == "All")
                                {
                                    HtmlData.Append("<td>");
                                    HtmlData.Append(_prefix +
                                                    sValue.Replace("<", "&lt;")
                                                        .Replace(">", "&gt;")
                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                        .Replace("\t",
                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                    _suffix + "</font>");
                                    HtmlData.Append("</td>");
                                    HtmlData.Append("</tr>");
                                }
                                else
                                {
                                    if (rowCount == 0)
                                    {
                                        HtmlData.Append("<td>");
                                        HtmlData.Append(_prefix +
                                                        sValue.Replace("<", "&lt;")
                                                            .Replace(">", "&gt;")
                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                            .Replace("\t",
                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                        _suffix + "</font>");
                                        HtmlData.Append("</td>");
                                        HtmlData.Append("</tr>");
                                    }
                                    else
                                    {
                                        HtmlData.Append("<td>");
                                        HtmlData.Append(
                                            sValue.Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("  ", "&nbsp;&nbsp;")
                                                .Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                            "</font>");
                                        HtmlData.Append("</td>");
                                        HtmlData.Append("</tr>");
                                    }
                                }
                            }
                        }
                        //htmlData.Append("<br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                        //htmlData.Append(tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>") + "</font><br>");
                    }
                    else
                    {
                        string sValue = "Null";
                        if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"] != DBNull.Value)
                            sValue = "";
                        GetCurrencySymbol(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString());
                        if ((_emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                if (_replaceText != "")
                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                //  HtmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                                //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                if (Convert.ToString(_replaceText) != string.Empty)
                                {
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix +
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(
                                                        _replaceText.Replace("<", "&lt;")
                                                            .Replace(">", "&gt;")
                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                            .Replace("\t",
                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                        "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_prefix +
                                                            _replaceText.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                            _suffix + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(
                                                    _replaceText.Replace("<", "&lt;")
                                                        .Replace(">", "&gt;")
                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                        .Replace("\t",
                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                    "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _replaceText = sValue;
                                if (Convert.ToString(_replaceText) != string.Empty)
                                {
                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                    //  HtmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix +
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(
                                                        _replaceText.Replace("<", "&lt;")
                                                            .Replace(">", "&gt;")
                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                            .Replace("\t",
                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                        "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_prefix +
                                                            _replaceText.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                            _suffix + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(
                                                    _replaceText.Replace("<", "&lt;")
                                                        .Replace(">", "&gt;")
                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                        .Replace("\t",
                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                    "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) == 12)
                {
                    if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString().Trim() != "" && tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"] != DBNull.Value)
                    {
                        GetCurrencySymbol(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString());
                        //  string dtype = tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString();
                        //if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                        //{
                        //}
                        bool styleFormat = tbfamilyspecs.Rows[rowCount]["STYLE_FORMAT"].ToString().Length > 0;
                        // Ocon.SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString() + "";
                        // DataSet dsSize = Ocon.CreateDataSet();
                        int attributeId = Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"]);
                        string attrDatatype = _dbcontext.TB_ATTRIBUTE.Find(attributeId).ATTRIBUTE_DATATYPE;

                        string sValue = tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString();

                        //string Valsize = dsSize.Tables[0].Rows[0]["ATTRIBUTE_DATATYPE"].ToString();
                        sValue = Valchk(sValue, attrDatatype);

                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                if (sValue == "0")
                                {
                                    if (_emptyCondition == "0.000000")
                                    {
                                        sValue = _emptyCondition;
                                    }
                                }
                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                if (_replaceText != "")
                                    HtmlData.Append("<tr>");
                                HtmlData.Append("<td>");
                                HtmlData.Append("<b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                                HtmlData.Append("</td>");
                                //  HtmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                                //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                if (Convert.ToString(_replaceText) != string.Empty)
                                {
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix +
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(
                                                        _replaceText.Replace("<", "&lt;")
                                                            .Replace(">", "&gt;")
                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                            .Replace("\t",
                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                        "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(
                                                _replaceText.Replace("<", "&lt;")
                                                    .Replace(">", "&gt;")
                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                    .Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_prefix +
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _replaceText = sValue;
                                if (Convert.ToString(_replaceText) != string.Empty)
                                {
                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                    // HtmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix +
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(
                                                        _replaceText.Replace("<", "&lt;")
                                                            .Replace(">", "&gt;")
                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                            .Replace("\t",
                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                        "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_prefix +
                                                            _replaceText.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                            _suffix + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(
                                                    _replaceText.Replace("<", "&lt;")
                                                        .Replace(">", "&gt;")
                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                        .Replace("\t",
                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                    "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString().Trim() != "" && tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString().Trim() != "0")
                        {
                            if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                            {
                                HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                //  HtmlData.Append("<br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                                if (Isnumber(sValue))
                                {
                                    //sValue = DecPrecisionFill(sValue, numberdecimel);
                                    sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString()), sValue.Trim()) : sValue;
                                }
                                if (_headeroptions == "All")
                                {
                                    HtmlData.Append("<td>");
                                    HtmlData.Append(_prefix +
                                                    sValue.Replace("<", "&lt;")
                                                        .Replace(">", "&gt;")
                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                        .Replace("\t",
                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                    _suffix + "</font><br>");
                                    HtmlData.Append("</td>");
                                    HtmlData.Append("</tr>");

                                }
                                else
                                {
                                    if (rowCount == 0)
                                    {
                                        HtmlData.Append("<td>");
                                        HtmlData.Append(_prefix +
                                                        sValue.Replace("<", "&lt;")
                                                            .Replace(">", "&gt;")
                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                            .Replace("\t",
                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                        _suffix + "</font><br>");
                                        HtmlData.Append("</td>");
                                        HtmlData.Append("</tr>");
                                    }
                                    else
                                    {
                                        HtmlData.Append("<td>");
                                        HtmlData.Append(
                                            sValue.Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("  ", "&nbsp;&nbsp;")
                                                .Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                            "</font><br>");
                                        HtmlData.Append("</td>");
                                        HtmlData.Append("</tr>");
                                    }
                                }
                            }
                        }
                        //htmlData.Append("<br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                        //htmlData.Append(tbfamilyspecs.Rows[rowCount]["NUMERIC_VALUE"].ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>") + "</font><br>");
                    }
                    else
                    {
                        string sValue = "Null";
                        GetCurrencySymbol(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_ID"].ToString());
                        if ((_emptyCondition == "Null" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                if (_replaceText != "")
                                    //   HtmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                if (Convert.ToString(_replaceText) != string.Empty)
                                {
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix +
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(
                                                        _replaceText.Replace("<", "&lt;")
                                                            .Replace(">", "&gt;")
                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                            .Replace("\t",
                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                        "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_prefix +
                                                            _replaceText.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                            _suffix + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(
                                                    _replaceText.Replace("<", "&lt;")
                                                        .Replace(">", "&gt;")
                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                        .Replace("\t",
                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                    "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _replaceText = sValue;
                                if (Convert.ToString(_replaceText) != string.Empty)
                                {
                                    HtmlData.Append("<tr><td><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b></td>");
                                    // HtmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_NAME"].ToString().Trim() + ": </b><br>");
                                    if (_fornumeric == "1")
                                    {
                                        if (Isnumber(_replaceText.Replace(",", "")))
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                if (rowCount == 0)
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(_prefix +
                                                                    _replaceText.Replace("<", "&lt;")
                                                                        .Replace(">", "&gt;")
                                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                                        .Replace("\t",
                                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                    _suffix + "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                                else
                                                {
                                                    HtmlData.Append("<td>");
                                                    HtmlData.Append(
                                                        _replaceText.Replace("<", "&lt;")
                                                            .Replace(">", "&gt;")
                                                            .Replace("  ", "&nbsp;&nbsp;")
                                                            .Replace("\t",
                                                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                        "</font><br>");
                                                    HtmlData.Append("</td>");
                                                    HtmlData.Append("</tr>");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                    }
                                    else
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            HtmlData.Append("<td>");
                                            HtmlData.Append(_prefix +
                                                            _replaceText.Replace("<", "&lt;")
                                                                .Replace(">", "&gt;")
                                                                .Replace("  ", "&nbsp;&nbsp;")
                                                                .Replace("\t",
                                                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                            _suffix + "</font><br>");
                                            HtmlData.Append("</td>");
                                            HtmlData.Append("</tr>");
                                        }
                                        else
                                        {
                                            if (rowCount == 0)
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(_prefix +
                                                                _replaceText.Replace("<", "&lt;")
                                                                    .Replace(">", "&gt;")
                                                                    .Replace("  ", "&nbsp;&nbsp;")
                                                                    .Replace("\t",
                                                                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                                _suffix + "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                            else
                                            {
                                                HtmlData.Append("<td>");
                                                HtmlData.Append(
                                                    _replaceText.Replace("<", "&lt;")
                                                        .Replace(">", "&gt;")
                                                        .Replace("  ", "&nbsp;&nbsp;")
                                                        .Replace("\t",
                                                            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") +
                                                    "</font><br>");
                                                HtmlData.Append("</td>");
                                                HtmlData.Append("</tr>");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            HtmlData.Append("</TABLE>");
            HtmlData.Append("<br>");
            // HtmlData.Append("</body>");
            // HtmlData.Append("</html>");
            string userImagePath = HttpContext.Current.Server.MapPath("~/Content/ProductImages");

            string imageHtml = "";
            int arrImage = 0;
            for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
            {
                if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) == 9)
                {
                    if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "")
                    {
                        arrImage++;
                    }
                }
            }
            if (arrImage > 0)
            {
                var imageUrl = new string[arrImage];
                var imageName = new string[arrImage];
                var imagePath = new string[arrImage];
                var imageType = new string[arrImage];
                var imageSizeW = new string[arrImage];
                var imageSizeH = new string[arrImage];
                for (int rowCount = 0; rowCount < tbfamilyspecs.Rows.Count; rowCount++)
                {
                    if (Convert.ToInt32(tbfamilyspecs.Rows[rowCount]["ATTRIBUTE_TYPE"].ToString()) == 9)
                    {
                        if (tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim() != "")
                        {
                            if (!tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Contains("&"))
                            {
                                var chkFile = new FileInfo(userImagePath + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim());
                                if (chkFile.Exists)
                                {
                                    Image chkSize;
                                    if (chkFile.Extension.ToUpper() == ".EPS" ||
                                        chkFile.Extension.ToUpper() == ".SVG" ||
                                        chkFile.Extension.ToUpper() == ".TIF" ||
                                        chkFile.Extension.ToUpper() == ".TIFF" ||
                                        chkFile.Extension.ToUpper() == ".TGA" ||
                                        chkFile.Extension.ToUpper() == ".PCX")
                                    {
                                        string file_name = Convert.ToString(tbfamilyspecs.Rows[rowCount]["family_id"]) + "_" + Convert.ToString(tbfamilyspecs.Rows[rowCount]["attribute_id"]) + "_" + chkFile.Name;
                                        if (Directory.Exists(ImageMagickPath))
                                        {
                                            string[] imgargs = {userImagePath+"/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim(),
                                                    HttpContext.Current.Server.MapPath("~/Content/")  + "\\temp\\ImageandAttFile\\" + file_name   + ".jpg" };
                                            converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_id).Select(a => a.ConverterType).SingleOrDefault();
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                            if (chkFileVal.Exists)
                                            {

                                                var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                                                if (!chkFileVal.Exists)
                                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                                                imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                                                chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                                                if (!chkFileVal.Exists)
                                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                                imm.Dispose();
                                                var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                                                asd.Delete();
                                                //////////
                                                chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                                int iHeight = chkSize.Height;
                                                int iWidth = chkSize.Width;
                                                Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                chkSize.Dispose();
                                                imageUrl[imgSizeCount] = "..\\Content\\temp\\ImageandAttFile\\" + file_name + ".jpg";
                                                imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                imageType[imgSizeCount] = "IMAGE";
                                                imgSizeCount++;
                                                ValInc++;
                                            }
                                            else
                                            {
                                                chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                if (chkFileVal.Exists)
                                                {
                                                    var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                                                    if (!chkFileVal.Exists)
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                                                    imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                                                    chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                                                    if (!chkFileVal.Exists)
                                                        imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                                    imm.Dispose();
                                                    var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                                                    asd.Delete();
                                                    //////////
                                                    chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                                                    int iHeight = chkSize.Height;
                                                    int iWidth = chkSize.Width;
                                                    var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                    imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                    imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                    chkSize.Dispose();
                                                    imageUrl[imgSizeCount] = "..\\Content\\temp\\ImageandAttFile\\" + file_name + ".jpg";
                                                    imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                    imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                    imageType[imgSizeCount] = "IMAGE";
                                                    imgSizeCount++;
                                                    ValInc++;
                                                }
                                            }
                                        }
                                        //}
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".PSD")
                                    {
                                        if (Directory.Exists(ImageMagickPath))
                                        {
                                            string[] imgargs = {userImagePath+"/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim()+"[0]",
                                                    HttpContext.Current.Server.MapPath("~/Content/")  + "\\temp\\ImageandAttFile" + chkFile.Name   + ".jpg" };
                                            converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_id).Select(a => a.ConverterType).SingleOrDefault();
                                            if (converttype == "Image Magic")
                                            {
                                                Exefile = "convert.exe";
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            else
                                            {
                                                Exefile = "cons_rcp.exe";
                                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                pStart.CreateNoWindow = true;
                                                pStart.UseShellExecute = false;
                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                while (!cmdProcess.HasExited)
                                                {
                                                }
                                            }
                                            var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name + ".jpg");
                                            if (chkFileVal.Exists)
                                            {
                                                var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name + ".jpg");
                                                imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                                imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp");
                                                imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                                imm.Dispose();
                                                var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".gif");
                                                asd.Delete();
                                                //////////
                                                chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg");
                                                int iHeight = chkSize.Height;
                                                int iWidth = chkSize.Width;
                                                var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);

                                                chkSize.Dispose();
                                                imageUrl[imgSizeCount] = "..\\Content\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg";
                                                imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                imageType[imgSizeCount] = "IMAGE";
                                                imgSizeCount++;
                                                ValInc++;
                                            }
                                            else
                                            {
                                                chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                if (chkFileVal.Exists)
                                                {
                                                    var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                                                    imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".bmp");
                                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                                                    imm.Dispose();
                                                    var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".gif");
                                                    asd.Delete();
                                                    //////////
                                                    chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/") + "\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg");
                                                    int iHeight = chkSize.Height;
                                                    int iWidth = chkSize.Width;
                                                    var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                    imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                                    imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                                    chkSize.Dispose();
                                                    imageUrl[imgSizeCount] = "..\\Content\\temp\\" + chkFile.Name + "ImageandAttFile" + chkFile.Name + ".jpg";
                                                    imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                                    imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                                    imageType[imgSizeCount] = "IMAGE";
                                                    imgSizeCount++;
                                                    ValInc++;
                                                }
                                            }
                                        }
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".JPG" ||
                                         chkFile.Extension.ToUpper() == ".GIF" ||
                                         chkFile.Extension.ToUpper() == ".JPEG" ||
                                         chkFile.Extension.ToUpper() == ".BMP" ||
                                        chkFile.Extension.ToUpper() == ".PNG")
                                    {
                                        chkSize = Image.FromFile(userImagePath + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim());
                                        int iHeight = chkSize.Height;
                                        int iWidth = chkSize.Width;
                                        var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                        imageSizeH[imgSizeCount] = Convert.ToString(newVal.Height);
                                        imageSizeW[imgSizeCount] = Convert.ToString(newVal.Width);
                                        chkSize.Dispose();
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "IMAGE";
                                        imgSizeCount++;
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".PDF")
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\pdf_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "pdf_64.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "PDF";
                                        imgSizeCount++;
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".RTF")
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\rtf_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "rtf_64.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "RTF";
                                        imgSizeCount++;
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".XLS" || chkFile.Extension.ToUpper() == ".XLSX")
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\xls_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "xls_64.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "XLS";
                                        imgSizeCount++;
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".TIF")
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\tif_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "tiff_64.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "TIF";
                                        imgSizeCount++;
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".TIFF")
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\tiff_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "tiff_64.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "TIFF";
                                        imgSizeCount++;
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".INDD")
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\indd_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "indd_128.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "INDD";
                                        imgSizeCount++;
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".DOC")
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\doc_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "doc_64.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "DOC";
                                        imgSizeCount++;
                                    }

                                    else if (chkFile.Extension.ToUpper() == ".PSD")
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\psd_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "psd_64.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "PSD";
                                        imgSizeCount++;
                                    }
                                    else if (chkFile.Extension.ToUpper() == ".EPS")
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\eps_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "eps_64.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + "/" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "EPS";
                                        imgSizeCount++;
                                    }


                                    else if (chkFile.Extension.ToUpper() == ".AVI" ||
                                        chkFile.Extension.ToUpper() == ".WMV" ||
                                        chkFile.Extension.ToUpper() == ".MPG" ||
                                        chkFile.Extension.ToUpper() == ".SWF")
                                    {
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "MEDIA";
                                        imgSizeCount++;
                                    }
                                    else
                                    {
                                        string imgPath = HttpContext.Current.Server.MapPath("~/Content/Images") + "\\uns_64.BMP";
                                        var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "uns_64.png");
                                        rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                                        imageUrl[imgSizeCount] = "..\\Content\\ProductImages" + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                        imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                        imageType[imgSizeCount] = "UNS";
                                        imgSizeCount++;
                                    }
                                }
                                else
                                {
                                    //imageUrl[imgSizeCount] = "..\\Content\\ProductImages\\Images\\unsupportedImageformat.jpg";
                                    imageUrl[imgSizeCount] = "..//Content//ProductImages/" + CustomerFolder + tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim().Replace(@"\", @"/");
                                    // imageUrl[imgSizeCount] = "";
                                    imageName[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["OBJECT_NAME"].ToString().Trim();
                                    imagePath[imgSizeCount] = tbfamilyspecs.Rows[rowCount]["STRING_VALUE"].ToString().Trim();
                                    imageType[imgSizeCount] = "JPG";
                                    imgSizeCount++;
                                }
                            }

                        }
                    }
                }

                //imageHtml = "<br><br><TABLE boder=0><style>td{font-name:\"arial unicode ms\";font-size:12px;}th{font-family:arial unicode ms;font-size:12px;font-weight:Bold}</style>";
                imageHtml = "<TABLE class=\"table table-responsive table-condensed table-bordered table-striped 4\">";
                int calc = 4;
                if (imgSizeCount != 0)
                    for (int rowCount = 0; rowCount < imgSizeCount; rowCount++)
                    {
                        if ((imgSizeCount - rowCount) < 4)
                        {
                            calc = imgSizeCount - rowCount;
                        }
                        imageHtml = imageHtml + "<tr>";
                        for (int sizeCount = 0; sizeCount < calc; sizeCount++)
                        {
                            if (imageType[rowCount + sizeCount] == "MEDIA")
                                imageHtml = imageHtml + "<td ><EMBED SRC= \"" + imageUrl[rowCount + sizeCount] + "\" height = 200pts width = 200pts AUTOPLAY=\"false\" CONTROLLER=\"true\"/></td>";
                            else if (imageType[rowCount + sizeCount] == "PDF")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\pdf_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                            else if (imageType[rowCount + sizeCount] == "RTF")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\rtf_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                            else if (imageType[rowCount + sizeCount] == "XLS")
                                imageHtml = imageHtml + "<td height=200pts width=200pts><Center><IMG src = \"..\\Content\\Images\\xls_64.BMP\" height = 200pts width = 200pts /></Center></td>";

                            else if (imageType[rowCount + sizeCount] == "TIF")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\tif_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                            else if (imageType[rowCount + sizeCount] == "TIFF")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\tiff_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                            else if (imageType[rowCount + sizeCount] == "DOC")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\doc_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                            else if (imageType[rowCount + sizeCount] == "EPS")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\eps_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                            else if (imageType[rowCount + sizeCount] == "INDD")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\indd_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                            else if (imageType[rowCount + sizeCount] == "PSD")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\psd_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                            /*    else if (imageType[rowCount + sizeCount] == "SWF")
                                    imageHTML = imageHTML + "<td height=200pts width=200pts><Center><IMG src = \"..\\Content\\Images\\swf_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "WMV")
                                    imageHTML = imageHTML + "<td height=200pts width=200pts><Center><IMG src = \"..\\Content\\Images\\wmv_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                                else if (imageType[rowCount + sizeCount] == "MPG")
                                    imageHTML = imageHTML + "<td height=200pts width=200pts><Center><IMG src = \"..\\Content\\Images\\mpg_64.BMP\" height = 200pts width = 200pts /></Center></td>";
                                
                                */
                            else if (imageType[rowCount + sizeCount] == "UNS")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"..\\Content\\Images\\uns_64.BMP\" height = 200pts width = 200pts /></Center></td>";

                            else
                                if (imageSizeW[rowCount + sizeCount] != null && imageSizeW[rowCount + sizeCount] != "0")
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"" + imageUrl[rowCount + sizeCount] + "\" height = " + imageSizeH[rowCount + sizeCount] + "pts width = " + imageSizeW[rowCount + sizeCount] + "pts /></Center></td>";
                            else
                                imageHtml = imageHtml + "<td height=200pts width=200pts class=\" imagesec\" ><Center><IMG src = \"" + imageUrl[rowCount + sizeCount] + "\" height = 200pts width = 200pts /></Center></td>";

                        }
                        imageHtml = imageHtml + "</tr><tr>";
                        for (int sizeCount = 0; sizeCount < calc; sizeCount++)
                        {
                            var index = imagePath[rowCount + sizeCount].ToString().LastIndexOf(@"\") + 1;

                            if (File.Exists(userImagePath + "/" + imagePath[rowCount + sizeCount]))
                            {
                                imageHtml = imageHtml + "<td width=200 ><Center><a href=\"" + imageUrl[rowCount + sizeCount] + "\">" + imagePath[rowCount + sizeCount].Substring(index) + "</a></Center></td>";
                            }
                            else
                            {
                                imageHtml = imageHtml + "<td width=200 ><Center><a href=\"" + imageUrl[rowCount + sizeCount] + "\">" + imagePath[rowCount + sizeCount].Substring(index) + "</a></Center></td>";
                            }
                        }
                        imageHtml = imageHtml + "</tr><tr>";
                        //for (int sizeCount = 0; sizeCount < calc; sizeCount++)
                        //{
                        //    if (System.IO.File.Exists((SettingMembers.GetValue(SystemSettingsCollection.SettingsList.USERIMAGEPATH.ToString())) + imagePath[rowCount + sizeCount].ToString()) == false)
                        //    {
                        //        imageHTML = imageHTML + "<td  width=200 ><Center><a href=\"" + imageUrl[rowCount + sizeCount].ToString() + "\">" + (SettingMembers.GetValue(SystemSettingsCollection.SettingsList.USERIMAGEPATH.ToString()))+imagePath[rowCount + sizeCount].ToString() + "</a></Center></td>";
                        //    }
                        //}
                        imageHtml = imageHtml + "</tr>";
                        rowCount = rowCount + 3;
                    }

            }
            if (imageHtml != "" && imgSizeCount != 0)
                HtmlData.Append(imageHtml + "</Table>");


            string prodHtml = GenerateFamilyPreview(catalogId, subFamilyID, CategoryID, user_id);
            if (prodHtml.Contains("&lt;") && prodHtml.Contains("&gt;"))
            {
                prodHtml = prodHtml.Replace("&lt;", "&amp;lt;").Replace("&gt;", "&amp;gt;");
            }
            //prodString.Dispose();
            if (prodHtml != "")
                if (prodHtml.EndsWith("SPECIFICATION ! </P></BODY></CENTER></html>") == false && !prodHtml.Contains("<html><h6>Table Preview Not Available!</h6><html>"))
                {
                    HtmlData.Append("<br>" + prodHtml.Substring(prodHtml.ToUpper().IndexOf("<TABLE", StringComparison.Ordinal) - 18, (prodHtml.ToUpper().IndexOf("</TABLE", StringComparison.Ordinal) - prodHtml.ToUpper().IndexOf("<TABLE", StringComparison.Ordinal) + 26)));
                    if (prodHtml.Contains("&amp;"))
                    {
                        HtmlData = HtmlData.Replace("&amp;", "&");
                    }
                }
                else
                {
                    HtmlData.Append("<html><h6>Table Preview Not Available!</h6><html>");
                }
            HtmlData.Append("</div>");

        }
        public string LoadMultipleTable(int familyId, int catalogId, string categoryId, bool FamilyLevelMultipletablePreview, string UserName)
        {
            var objSuperTable = new SuperTable();
            string multipleTableValueToReplace = string.Empty;
            string groupnames = string.Empty;
            bool familyLvelPreviewEnabled = FamilyLevelMultipletablePreview;
            if (familyLvelPreviewEnabled)
            {
                var dsMultipleTables = Multipletable(catalogId, familyId);
                if (dsMultipleTables.Rows.Count > 0)
                {
                    var view = new DataView(dsMultipleTables);
                    string[] columns = { "GROUP_ID", "GROUP_NAME" };
                    DataTable distinctValues = view.ToTable(true, columns);
                    multipleTableValueToReplace = "<div style=\"background-color:white;border-radius:9px; max-width: 100%; overflow: auto;\"><br/><h4><b>Multiple / Additional Table</h4>";
                    for (int k = 0; k < distinctValues.Rows.Count; k++)
                    {
                        int groupId = Convert.ToInt32(distinctValues.Rows[k]["GROUP_ID"]);
                        groupnames += "<div style=\"background-color:white;border-radius:9px; max-width: 100%; overflow: auto;\"><br/><h5 style=\"background-color:#337ab7;color:white;border-top-right-radius: 9px;border-top-left-radius: 9px;Padding:10px;\">" + distinctValues.Rows[k]["GROUP_NAME"] + ":</h5>";
                        string tableLayout = FetchTableLayout_MultipleTable(groupId);
                        groupnames = groupnames + objSuperTable.GenerateMultipleTableHtml(groupId, tableLayout, familyId, catalogId, categoryId, ImageMagickPath, UserName, 0, "") + "</div>";
                    }
                    multipleTableValueToReplace += groupnames;

                }
            }

            multipleTableValueToReplace = multipleTableValueToReplace.Replace("&lt;br /&gt;", "<br>").Replace("&lt;/br&gt;", "<br>").Replace("&lt;br&gt;", "<br>")
                .Replace("&lt;br/&gt;", "<br>").Replace("&amp;lt;br/&amp;gt;", "<br>").Replace("&amp;nbsp;", "&nbsp;");
            return multipleTableValueToReplace.Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>").Replace("&lt;p/&gt;", "</p>");
        }

        public string LoadMultipleTables(int familyId, int catalogId, bool FamilyLevelMultipletablePreview)
        {

            string multipleTableValueToReplace = string.Empty;
            Image _chkSize;
            string imageSizeW = "";
            string imageSizeH = "";
            // bool familyLvelPreviewEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["FamilyLevelPreview"]);
            bool familyLvelPreviewEnabled = FamilyLevelMultipletablePreview;
            if (familyLvelPreviewEnabled)
            {
                var dsMultipleTables = Multipletable(catalogId, familyId);
                if (dsMultipleTables.Rows.Count > 0)
                {
                    #region product filter
                    var dynamicSQl = new StringBuilder();
                    //var oDsProdFilter = new DataSet();
                    string sProdFilter = string.Empty;
                    var productfilter = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(x => x);
                    // Ocon.SQLString = " SELECT PRODUCT_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + catalogID + " ";
                    //  oDsProdFilter = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                    if (productfilter.Any())
                    {
                        var prodFilter = productfilter.FirstOrDefault();

                        if (prodFilter != null)
                        {
                            sProdFilter = prodFilter.PRODUCT_FILTERS;
                        }

                        if (!string.IsNullOrEmpty(sProdFilter))
                        {
                            var xmlDOc = new XmlDocument();
                            xmlDOc.LoadXml(sProdFilter);
                            XmlNode rNode = xmlDOc.DocumentElement;
                            // StringBuilder SQLstring = new StringBuilder();

                            if (rNode != null && rNode.ChildNodes.Count > 0)
                            {
                                for (int i = 0; i < rNode.ChildNodes.Count; i++)
                                {
                                    var sqLstring = new StringBuilder();
                                    var tableDataSetNode = rNode.ChildNodes[i];
                                    if (tableDataSetNode.HasChildNodes)
                                    {
                                        if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                                        {
                                            tableDataSetNode.ChildNodes[2].InnerText = "=";
                                        }
                                        var strVal = tableDataSetNode.ChildNodes[3].InnerText.Trim().Replace("'", "''");
                                        //DataSet oDsattrtype = new DataSet();
                                        // StringBuilder DynamicattrSQl = new StringBuilder();
                                        // Ocon.SQLString = " SELECT attribute_datatype FROM TB_attribute WHERE  attribute_ID = " + TableDataSetNode.ChildNodes[0].InnerText + " ";
                                        // oDsattrtype = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                                        int attributeId = Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText);
                                        var oDsattrtype = _dbcontext.TB_ATTRIBUTE.Find(attributeId);

                                        if (oDsattrtype.ATTRIBUTE_DATATYPE.ToUpper().StartsWith("N") == false)
                                        {
                                            //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE Numeric_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " " + StrVal + "  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
                                            sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" +
                                                             catalogId + ") WHERE (STRING_VALUE " +
                                                             tableDataSetNode.ChildNodes[2].InnerText + " '" + strVal +
                                                             "' AND ATTRIBUTE_ID = " +
                                                             tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                        }
                                        else
                                        {
                                            //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE STRING_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " N'" + StrVal + "'  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
                                            sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" +
                                                             catalogId + ") WHERE  (NUMERIC_VALUE " +
                                                             tableDataSetNode.ChildNodes[2].InnerText + " '" + strVal +
                                                             "' AND ATTRIBUTE_ID = " +
                                                             tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");

                                        }
                                    }
                                    if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                                    {
                                        dynamicSQl.Append(sqLstring);
                                        break;
                                    }
                                    dynamicSQl.Append(sqLstring);
                                    if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                                    {

                                        dynamicSQl.Append(" INTERSECT \n");
                                    }
                                    if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                                    {
                                        dynamicSQl.Append(" UNION \n");
                                    }

                                    //if (i > 0)
                                    //{
                                    //    DynamicSQl.Append(" union ");
                                    //}
                                }
                            }
                        }

                    }
                    var DSfilter = new DataTable();

                    // Ocon.SQLString = DynamicSQl.ToString();
                    if (dynamicSQl.ToString() != string.Empty)
                    {
                        DSfilter = Checkproductfilter(dynamicSQl.ToString());
                        // DSfilter = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                    }
                    if (DSfilter != null && DSfilter.Rows.Count > 0)
                    {
                        //DataSet temp = dsMultipleTables;

                        // var t1 = new DataTable();

                        var dspreviewaltered = dsMultipleTables.Clone();
                        foreach (DataRow dr in dsMultipleTables.Rows)
                        {
                            DataRow[] drfilteredRows = DSfilter.Select("PRODUCT_ID = " + dr["PRODUCT_ID"]);
                            if (drfilteredRows != null && drfilteredRows.Length != 0)
                            {
                                //DataRow datarw = t1.NewRow();
                                dspreviewaltered.ImportRow(dr);
                            }

                        }
                        dsMultipleTables = new DataTable();
                        dsMultipleTables = dspreviewaltered.Clone();
                        foreach (DataRow dr in dspreviewaltered.Rows)
                        {
                            dsMultipleTables.ImportRow(dr);
                        }

                    }
                    #endregion
                    var dsGroupAttrs =
                   (_dbcontext.TB_ATTRIBUTE_GROUP_SPECS.Join(_dbcontext.TB_ATTRIBUTE_GROUP, tcp => tcp.GROUP_ID,
                       tpf => tpf.GROUP_ID, (tcp, tpf) => new { tcp, tpf })
                       .Join(_dbcontext.TB_ATTRIBUTE_GROUP_SECTIONS, tcptps => tcptps.tpf.GROUP_ID, tcf => tcf.GROUP_ID,
                           (tcptps, tcf) => new { tcptps, tcf })
                       .Join(_dbcontext.TB_ATTRIBUTE_GROUP, tcatcp => tcatcp.tcf.GROUP_ID, tf => tf.GROUP_ID,
                           (tcatcp, tf) => new { tcatcp, tf })
                       .Join(_dbcontext.TB_CATALOG_ATTRIBUTES, tpfatpf => tpfatpf.tcatcp.tcptps.tcp.ATTRIBUTE_ID, tc => tc.ATTRIBUTE_ID,
                           (tpfatpf, tc) => new { tpfatpf, tc })
                       .Where(
                           x => (x.tc.CATALOG_ID == catalogId && x.tpfatpf.tcatcp.tcf.FAMILY_ID == familyId && x.tpfatpf.tcatcp.tcf.CATALOG_ID == catalogId && x.tc.TB_ATTRIBUTE.PUBLISH2PRINT == true))
                       .Select(x => new
                       {
                           x.tc.TB_ATTRIBUTE.ATTRIBUTE_NAME,
                           x.tpfatpf.tcatcp.tcptps.tcp.SORT_ORDER,
                           x.tpfatpf.tcatcp.tcptps.tcp.GROUP_ID,
                           x.tpfatpf.tf.GROUP_NAME,
                           x.tpfatpf.tcatcp.tcf.CATALOG_ID,
                           x.tc.TB_ATTRIBUTE.ATTRIBUTE_ID,
                           x.tc.TB_ATTRIBUTE.ATTRIBUTE_TYPE,
                           x.tpfatpf.tcatcp.tcptps.tpf.LAYOUT
                       }).OrderBy(y => y.SORT_ORDER));

                    #region " Date and Time Format Process"
                    if (dsMultipleTables.Rows.Count > 0)
                    {
                        foreach (DataRow dtr in dsMultipleTables.Rows)
                        {
                            var prodid = Convert.ToInt32(dtr["PRODUCT_ID"]);
                            var prodAttributes = _dbcontext.TB_PROD_SPECS.Join(_dbcontext.TB_ATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                        .Where(x => (x.tcp.PRODUCT_ID == prodid && x.tpf.ATTRIBUTE_DATATYPE.ToLower().Contains("date")))
                        .Select(x => new
                        {
                            x.tpf.ATTRIBUTE_NAME,
                            x.tpf.ATTRIBUTE_DATATYPE,
                            x.tpf.ATTRIBUTE_DATAFORMAT
                        });

                            foreach (var dr in prodAttributes)
                            {
                                for (int i = 0; i < dsMultipleTables.Columns.Count; i++)
                                {
                                    if (dsMultipleTables.Columns[i].ColumnName == dr.ATTRIBUTE_NAME)
                                    {
                                        if (dr.ATTRIBUTE_DATATYPE.ToLower().StartsWith("date"))
                                        {
                                            if (dr.ATTRIBUTE_DATAFORMAT.StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
                                            {
                                                string tempdate = dtr[dsMultipleTables.Columns[i].ColumnName].ToString();
                                                dtr[dsMultipleTables.Columns[i].ColumnName] = tempdate.Substring(3, 2) + "/" + tempdate.Substring(0, 2) + "/" + tempdate.Substring(6, 4);

                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                    #endregion
                    if (dsGroupAttrs.Any())
                    {

                        multipleTableValueToReplace += "<br/><h4><b>Multiple / Additional Table</h4><br/>";
                        var dr = dsGroupAttrs.Select(x => new { x.GROUP_ID, x.GROUP_NAME, x.LAYOUT }).Distinct();

                        foreach (var itemh in dr)
                        {
                            bool isVericalTable = itemh.LAYOUT.ToLower() == "vertical";

                            if (!isVericalTable)
                            {
                                //foreach (DataRow itemh1 in dr.Rows)
                                {
                                    //multipleTableValueToReplace += "<thead><tr bgcolor='Aqua'>";
                                    //int colSpan = dsGroupAttrs.Tables[0].Select("GROUP_ID = " + itemh.ItemArray[0] + "").Length;
                                    //multipleTableValueToReplace += "<th colspan='" + colSpan + "'>" + itemh.ItemArray[1] + "</th>";
                                    //multipleTableValueToReplace += "</tr></thead>";
                                    if (dsMultipleTables.Rows.Count > 0)
                                    {
                                        multipleTableValueToReplace += "<br/><br/><h5>" + itemh.GROUP_NAME + " : </h5><br/><br/>";
                                        multipleTableValueToReplace += "<table class=\"table table-responsive table-condensed table-bordered table-striped 5\">";
                                        multipleTableValueToReplace += "<tr>";
                                        var groupdetails = dsGroupAttrs.Where(x => x.GROUP_ID == itemh.GROUP_ID);
                                        //.OrderBy(x => x.ATTRIBUTE_ID);
                                        foreach (var item in groupdetails)
                                        {
                                            multipleTableValueToReplace += "<th scope='col'>" + item.ATTRIBUTE_NAME.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") + "</th>";
                                        }
                                        multipleTableValueToReplace += "</tr>";
                                        int i = 0;
                                        foreach (DataRow item1 in dsMultipleTables.Rows)
                                        {
                                            var prodid = Convert.ToInt32(item1["PRODUCT_ID"]);
                                            if (itemh.GROUP_ID == Convert.ToInt32(item1["GROUP_ID"]))
                                            {
                                                multipleTableValueToReplace += "<tr>";
                                                foreach (var item in dsGroupAttrs)
                                                {
                                                    if (itemh.GROUP_ID == item.GROUP_ID)
                                                    {
                                                        string sValue = "";
                                                        string caption = string.Empty;

                                                        if (item1[item.ATTRIBUTE_NAME] == DBNull.Value)
                                                        {
                                                            sValue = null;

                                                        }
                                                        else
                                                        {
                                                            sValue = item1[item.ATTRIBUTE_NAME].ToString();
                                                            // string caption = string.Empty;
                                                            var captiondetails =
                                                                _dbcontext.TB_PROD_SPECS.Where(
                                                                    x =>
                                                                        x.ATTRIBUTE_ID == item.ATTRIBUTE_ID &&
                                                                        x.PRODUCT_ID == prodid);
                                                            //item1["OBJECT_NAME"].ToString();
                                                            if (captiondetails.Any())
                                                            {
                                                                var capitons = captiondetails.FirstOrDefault();
                                                                if (capitons != null)
                                                                {
                                                                    caption = capitons.OBJECT_NAME;

                                                                }
                                                            }
                                                        }

                                                        if (sValue != null)
                                                        {
                                                            if (item.ATTRIBUTE_ID == 1)
                                                            {
                                                                multipleTableValueToReplace += "<th class='spec' scope='row'>" + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") + "</th>";
                                                            }
                                                            else
                                                            {
                                                                if (sValue.ToLower().EndsWith(".jpg") || sValue.ToLower().EndsWith(".bmp") ||
                                                                    sValue.ToLower().EndsWith(".gif") ||
                                                                  sValue.ToLower().EndsWith(".png"))
                                                                {
                                                                    Double iHeight = 200, iWidth = 200;
                                                                    var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages") + sValue);
                                                                    if (chkFileVal.Exists)
                                                                    {
                                                                        _chkSize =
                                                                            Image.FromFile(
                                                                                HttpContext.Current.Server.MapPath(
                                                                                    "~/Content/ProductImages") + sValue);
                                                                        iHeight = _chkSize.Height;
                                                                        iWidth = _chkSize.Width;
                                                                        Size newVal = ScaleImage(iHeight, iWidth, 200,
                                                                            200);
                                                                        imageSizeH = Convert.ToString(newVal.Height);
                                                                        imageSizeW = Convert.ToString(newVal.Width);
                                                                        if (_chkSize != null)
                                                                        {
                                                                            _chkSize.Dispose();
                                                                        }
                                                                        multipleTableValueToReplace += "<td class='reg'><img height='" + imageSizeH + "' width='" + imageSizeW + "' src='" + "..\\Content\\ProductImages" + sValue + "' />";
                                                                        multipleTableValueToReplace += "<br>";
                                                                        multipleTableValueToReplace += "<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + caption + "</label>";
                                                                        multipleTableValueToReplace += "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        //string imageUrl = "..\\Content\\ProductImages\\images\\unsupportedImageformat.jpg";
                                                                        //string imageUrl = "";
                                                                        imageSizeH = "72";
                                                                        imageSizeW = "72";
                                                                        // multipleTableValueToReplace += "<td class='reg'><img height='" + imageSizeH + "' width='" + imageSizeW + "' src='" + imageUrl + "' /></td>";
                                                                        multipleTableValueToReplace += "";
                                                                    }

                                                                }
                                                                else if (sValue.ToLower().EndsWith(".tif") ||
                                                                  sValue.ToLower().EndsWith(".tiff") ||
                                                                  sValue.ToLower().EndsWith(".tga") ||
                                                                  sValue.ToLower().EndsWith(".pcx"))
                                                                {
                                                                    if (Directory.Exists(ImageMagickPath))
                                                                    {
                                                                        //sValue = sValue.Substring(0, sValue.LastIndexOf('.'));
                                                                        string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages") + sValue, HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".jpg" };
                                                                        if (converttype == "Image Magic")
                                                                        {
                                                                            Exefile = "convert.exe";
                                                                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                                            pStart.CreateNoWindow = true;
                                                                            pStart.UseShellExecute = false;
                                                                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                                            while (!cmdProcess.HasExited)
                                                                            {
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            Exefile = "cons_rcp.exe";
                                                                            // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                                            pStart.CreateNoWindow = true;
                                                                            pStart.UseShellExecute = false;
                                                                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                                            while (!cmdProcess.HasExited)
                                                                            {
                                                                            }
                                                                        }
                                                                    }
                                                                    Double iHeight = 200, iWidth = 200;
                                                                    var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".jpg");
                                                                    if (chkFileVal.Exists)
                                                                    {
                                                                        _chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".jpg");
                                                                        iHeight = _chkSize.Height;
                                                                        iWidth = _chkSize.Width;
                                                                        Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                                        imageSizeH = Convert.ToString(newVal.Height);
                                                                        imageSizeW = Convert.ToString(newVal.Width);
                                                                        if (_chkSize != null)
                                                                        {
                                                                            _chkSize.Dispose();
                                                                        }
                                                                        multipleTableValueToReplace += "<td class='reg'><img height='" + imageSizeH + "' width='" + imageSizeW + "' src='" + "..\\Content" + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".jpg' />";
                                                                        multipleTableValueToReplace += "<br>";
                                                                        multipleTableValueToReplace += "<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + caption + "</label>";
                                                                        multipleTableValueToReplace += "</td>";
                                                                    }

                                                                    else
                                                                    {
                                                                        //string imageUrl = "..\\Content\\ProductImages\\images\\unsupportedImageformat.jpg";
                                                                        //string imageUrl = "";
                                                                        imageSizeH = "72";
                                                                        imageSizeW = "72";
                                                                        //multipleTableValueToReplace += "<td class='reg'><img height='" + imageSizeH + "' width='" + imageSizeW + "' src='" + imageUrl + "' /></td>";
                                                                        multipleTableValueToReplace += "";
                                                                    }

                                                                }
                                                                else if (sValue.ToLower().EndsWith(".eps"))
                                                                {
                                                                    if (Directory.Exists(ImageMagickPath))
                                                                    {
                                                                        //sValue = sValue.Substring(0, sValue.LastIndexOf('.'));
                                                                        string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages") + sValue, HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".png" };
                                                                        if (converttype == "Image Magic")
                                                                        {
                                                                            Exefile = "convert.exe";
                                                                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                                            pStart.CreateNoWindow = true;
                                                                            pStart.UseShellExecute = false;
                                                                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                                            while (!cmdProcess.HasExited)
                                                                            {
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            Exefile = "cons_rcp.exe";
                                                                            // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                                            pStart.CreateNoWindow = true;
                                                                            pStart.UseShellExecute = false;
                                                                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                                            while (!cmdProcess.HasExited)
                                                                            {
                                                                            }
                                                                        }
                                                                    }
                                                                    Double iHeight = 200, iWidth = 200;
                                                                    var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".png");
                                                                    if (chkFileVal.Exists)
                                                                    {
                                                                        _chkSize =
                                                                            Image.FromFile(
                                                                                HttpContext.Current.Server.MapPath(
                                                                                    "~/Content") +
                                                                                "\\temp\\ImageandAttFile" +
                                                                                sValue.Substring(
                                                                                    sValue.LastIndexOf('\\'),
                                                                                    sValue.LastIndexOf('.') -
                                                                                    sValue.LastIndexOf('\\')) + ".png");
                                                                        iHeight = _chkSize.Height;
                                                                        iWidth = _chkSize.Width;
                                                                        Size newVal = ScaleImage(iHeight, iWidth, 200,
                                                                            200);
                                                                        imageSizeH = Convert.ToString(newVal.Height);
                                                                        imageSizeW = Convert.ToString(newVal.Width);
                                                                        if (_chkSize != null)
                                                                        {
                                                                            _chkSize.Dispose();
                                                                        }

                                                                        multipleTableValueToReplace +=
                                                                            "<td class='reg'><img height='" + imageSizeH +
                                                                            "' width='" + imageSizeW + "' src='" +
                                                                            "..\\Content" + "\\temp\\ImageandAttFile" +
                                                                            sValue.Substring(sValue.LastIndexOf('\\'),
                                                                                sValue.LastIndexOf('.') -
                                                                                sValue.LastIndexOf('\\')) +
                                                                            ".png' />";

                                                                        multipleTableValueToReplace += "<br>";
                                                                        multipleTableValueToReplace += "<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + caption + "</label>";
                                                                        multipleTableValueToReplace += "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        //string imageUrl = "..\\Content\\ProductImages\\images\\unsupportedImageformat.jpg";
                                                                        //string imageUrl = "";
                                                                        imageSizeH = "72";
                                                                        imageSizeW = "72";
                                                                        //multipleTableValueToReplace += "<td class='reg'><img height='" + imageSizeH + "' width='" + imageSizeW + "' src='" + imageUrl + "' /></td>";
                                                                        multipleTableValueToReplace += "";

                                                                    }
                                                                }
                                                                else if (sValue.ToLower().EndsWith(".psd"))
                                                                {
                                                                    if (Directory.Exists(ImageMagickPath))
                                                                    {
                                                                        //sValue = sValue.Substring(0, sValue.LastIndexOf('.'));
                                                                        string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages") + sValue + "[0]", HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".jpg" };
                                                                        if (converttype == "Image Magic")
                                                                        {
                                                                            Exefile = "convert.exe";
                                                                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                                            pStart.CreateNoWindow = true;
                                                                            pStart.UseShellExecute = false;
                                                                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                                            while (!cmdProcess.HasExited)
                                                                            {
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            Exefile = "cons_rcp.exe";
                                                                            // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                                            pStart.CreateNoWindow = true;
                                                                            pStart.UseShellExecute = false;
                                                                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                                            while (!cmdProcess.HasExited)
                                                                            {
                                                                            }
                                                                        }
                                                                    }
                                                                    Double iHeight = 200, iWidth = 200;
                                                                    var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".jpg");
                                                                    if (chkFileVal.Exists)
                                                                    {
                                                                        _chkSize =
                                                                            Image.FromFile(
                                                                                HttpContext.Current.Server.MapPath(
                                                                                    "~/Content") +
                                                                                "\\temp\\ImageandAttFile" +
                                                                                sValue.Substring(
                                                                                    sValue.LastIndexOf('\\'),
                                                                                    sValue.LastIndexOf('.') -
                                                                                    sValue.LastIndexOf('\\')) + ".jpg");
                                                                        iHeight = _chkSize.Height;
                                                                        iWidth = _chkSize.Width;
                                                                        Size newVal = ScaleImage(iHeight, iWidth, 200,
                                                                            200);
                                                                        imageSizeH = Convert.ToString(newVal.Height);
                                                                        imageSizeW = Convert.ToString(newVal.Width);
                                                                        if (_chkSize != null)
                                                                        {
                                                                            _chkSize.Dispose();
                                                                        }

                                                                        multipleTableValueToReplace +=
                                                                            "<td class='reg'><img height='" + imageSizeH +
                                                                            "' width='" + imageSizeW + "' src='" +
                                                                            "..\\Content" + "\\temp\\ImageandAttFile" +
                                                                            sValue.Substring(sValue.LastIndexOf('\\'),
                                                                                sValue.LastIndexOf('.') -
                                                                                sValue.LastIndexOf('\\')) +
                                                                            ".jpg' />";

                                                                        multipleTableValueToReplace += "<br>";
                                                                        multipleTableValueToReplace += "<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + caption + "</label>";
                                                                        multipleTableValueToReplace += "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        // string imageUrl = "..\\Content\\ProductImages\\images\\unsupportedImageformat.jpg";
                                                                        // string imageUrl = "";
                                                                        imageSizeH = "72";
                                                                        imageSizeW = "72";
                                                                        //multipleTableValueToReplace += "<td class='reg'><img height='" + imageSizeH + "' width='" + imageSizeW + "' src='" + imageUrl + "' /></td>";
                                                                        multipleTableValueToReplace += "";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    //string sqlString = "SELECT ATTRIBUTE_DATATYPE,STYLE_FORMAT,DEFAULT_VALUE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + ;
                                                                    var dss =
                                                                        _dbcontext.TB_ATTRIBUTE.Find(item.ATTRIBUTE_ID);
                                                                    // Ocon.SQLString = sqlString;
                                                                    // DataSet dss = Ocon.CreateDataSet();
                                                                    //if (item["ATTRIBUTE_TYPE"].ToString() == "4")
                                                                    {
                                                                        GetCurrencySymbol(Convert.ToString(item.ATTRIBUTE_ID));
                                                                        int numberdecimel = 0;
                                                                        if (dss.ATTRIBUTE_DATATYPE.ToUpper().StartsWith("NUM"))
                                                                        {
                                                                            string dtype = dss.ATTRIBUTE_DATATYPE;

                                                                            if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                                                                            {
                                                                                numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                                                                            }
                                                                            bool styleFormat = false;
                                                                            if (dss.STYLE_FORMAT != null)
                                                                            {

                                                                                styleFormat = dss.STYLE_FORMAT.Length > 0;
                                                                            }
                                                                            if (sValue.Trim().Length > 0)
                                                                            {
                                                                                if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                                                {
                                                                                    sValue = DecPrecisionFill(sValue, numberdecimel);
                                                                                    sValue = styleFormat ? ApplyStyleFormat(item.ATTRIBUTE_ID, sValue.Trim()) : sValue;
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000")
                                                                                    {
                                                                                        if ((_replaceText != "") && (Isnumber(_replaceText)))
                                                                                        {
                                                                                            sValue = _emptyCondition == sValue ? _replaceText : sValue;
                                                                                        }
                                                                                    }
                                                                                    if (Isnumber(sValue))
                                                                                    {
                                                                                        sValue = DecPrecisionFill(sValue, numberdecimel);
                                                                                        sValue = styleFormat ? ApplyStyleFormat(item.ATTRIBUTE_ID, sValue.Trim()) : sValue;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        //sValue = DataRule(item1[item["ATTRIBUTE_TYPE"].ToString()].ToString(), sValue, 0, item1[item["ATTRIBUTE_ID"].ToString()].ToString(), numberdecimel, styleFormat);
                                                                    }
                                                                    if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                                    {
                                                                        if (_headeroptions == "All")
                                                                            multipleTableValueToReplace += "<td class='reg'>" + _prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                                        else
                                                                            if (i == 0)
                                                                        {
                                                                            multipleTableValueToReplace += "<td class='reg'>" + _prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                                        }
                                                                        else
                                                                        {
                                                                            multipleTableValueToReplace += "<td class='reg'>" + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</td>";
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                                                                        {
                                                                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                                                            if (_replaceText != "")
                                                                            {
                                                                                _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                                            }
                                                                            else
                                                                            {
                                                                                _replaceText = sValue;
                                                                            }
                                                                            if (_fornumeric == "1")
                                                                            {
                                                                                if (Isnumber(_replaceText.Replace(",", "")))
                                                                                    multipleTableValueToReplace += "<td class='reg'>" + _prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                                                else
                                                                                    multipleTableValueToReplace += "<td class='reg'>" + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</td>";
                                                                            }
                                                                            else
                                                                                multipleTableValueToReplace += "<td class='reg'>" + _prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";

                                                                        }
                                                                        else
                                                                        {
                                                                            if (_headeroptions == "All")
                                                                                multipleTableValueToReplace += "<td class='reg'>" + _prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                                            else
                                                                                if (i == 0)
                                                                            {
                                                                                multipleTableValueToReplace += "<td class='reg'>" + _prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                multipleTableValueToReplace += "<td class='reg'>" + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</td>";
                                                                            }
                                                                        }
                                                                        //if (_headeroptions == "All")
                                                                        //    multipleTableValueToReplace += "<td class='reg'>" + _replaceText + "</td>";
                                                                        //else
                                                                        //{
                                                                        //    if (i == 0)
                                                                        //    {
                                                                        //        multipleTableValueToReplace += "<td class='reg'>" + _replaceText + "</td>";
                                                                        //    }
                                                                        //    else
                                                                        //    {
                                                                        //        multipleTableValueToReplace += "<td class='reg'>" + sValue + "</td>";
                                                                        //    }
                                                                        //}
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            GetCurrencySymbol(Convert.ToString(item.ATTRIBUTE_ID));
                                                            if (_emptyCondition == "Null")
                                                            {
                                                                sValue = _replaceText;
                                                            }
                                                            else
                                                            {
                                                                sValue = "";
                                                            }
                                                            multipleTableValueToReplace += "<td class='reg'>" + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</td>";
                                                        }

                                                    }
                                                }
                                                multipleTableValueToReplace += "</tr>";
                                                i++;
                                            }

                                        }
                                        multipleTableValueToReplace += "</table><br/>";
                                    }
                                    else
                                    {
                                        multipleTableValueToReplace += "<h2>Multiple Table Preview Not Available!</h2>";
                                    }
                                }
                            }
                            else
                            {
                                //foreach (DataRow itemh in dr.Rows)
                                {
                                    if (dsMultipleTables.Rows.Count > 0)
                                    {
                                        multipleTableValueToReplace += "<br/><br/><b>" + itemh.GROUP_NAME + " : </b><br/><br/>";
                                        multipleTableValueToReplace += "<table class=\"table table-responsive table-condensed table-bordered table-striped 6\">";
                                        //string groupName = string.Empty;
                                        var groupdetails = dsGroupAttrs.Where(x => x.GROUP_ID == itemh.GROUP_ID).OrderBy(x => x.ATTRIBUTE_ID);
                                        foreach (var item in groupdetails)
                                        {
                                            multipleTableValueToReplace += "<tr>";
                                            //if (groupName != item["GROUP_NAME"].ToString())
                                            //{
                                            //    groupName = item["GROUP_NAME"].ToString();
                                            //    int rowSpan = dsGroupAttrs.Tables[0].Select("GROUP_ID = " + item["GROUP_ID"] + "").Length;
                                            //    multipleTableValueToReplace += "<th scope='col' rowspan='" + rowSpan + "'>" + groupName + "</th>";
                                            //}
                                            multipleTableValueToReplace += "<th scope='col'>" + item.ATTRIBUTE_NAME.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") + "</th>";
                                            int i = 0;
                                            foreach (DataRow item1 in dsMultipleTables.Select("GROUP_ID = " + itemh.GROUP_ID + ""))
                                            {
                                                var prodid = Convert.ToInt32(item1["PRODUCT_ID"]);
                                                string sValue = "";
                                                string caption = "";

                                                if (item1[item.ATTRIBUTE_NAME] == DBNull.Value)
                                                    sValue = null;
                                                else
                                                {
                                                    sValue = item1[item.ATTRIBUTE_NAME].ToString();
                                                    // caption = item1["OBJECT_NAME"].ToString();
                                                    var captiondetails =
                                                                _dbcontext.TB_PROD_SPECS.Where(
                                                                    x =>
                                                                        x.ATTRIBUTE_ID == item.ATTRIBUTE_ID &&
                                                                        x.PRODUCT_ID == prodid);
                                                    //item1["OBJECT_NAME"].ToString();
                                                    if (captiondetails.Any())
                                                    {
                                                        var capitons = captiondetails.FirstOrDefault();
                                                        if (capitons != null)
                                                        {
                                                            caption = capitons.OBJECT_NAME;

                                                        }
                                                    }
                                                }

                                                if (sValue != null)
                                                {
                                                    if (sValue.ToLower().EndsWith(".jpg") || sValue.ToLower().EndsWith(".bmp") ||
                                                                 sValue.ToLower().EndsWith(".gif") ||
                                                              sValue.ToLower().EndsWith(".png"))
                                                    {
                                                        Double iHeight = 200, iWidth = 200;
                                                        var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages") + sValue);
                                                        if (chkFileVal.Exists)
                                                        {
                                                            _chkSize =
                                                                Image.FromFile(
                                                                    HttpContext.Current.Server.MapPath(
                                                                        "~/Content/ProductImages") + sValue);
                                                            iHeight = _chkSize.Height;
                                                            iWidth = _chkSize.Width;
                                                            var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                            imageSizeH = Convert.ToString(newVal.Height);
                                                            imageSizeW = Convert.ToString(newVal.Width);
                                                            if (_chkSize != null)
                                                            {
                                                                _chkSize.Dispose();
                                                            }

                                                            multipleTableValueToReplace +=
                                                                "<td class='reg'><img height='" + imageSizeH +
                                                                "' width='" + imageSizeW + "' src='" +
                                                                "..\\Content\\ProductImages" + sValue + "' />";
                                                            multipleTableValueToReplace += "<br>";
                                                            multipleTableValueToReplace += "<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + caption + "</label>";
                                                            multipleTableValueToReplace += "</td>";

                                                        }
                                                        else
                                                        {
                                                            //string imageUrl = "..\\Content\\ProductImages\\images\\unsupportedImageformat.jpg";
                                                            //string imageUrl = "";
                                                            imageSizeH = "72";
                                                            imageSizeW = "72";
                                                            //multipleTableValueToReplace += "<td class='reg'><img height='" + imageSizeH + "' width='" + imageSizeW + "' src='" + imageUrl + "' /></td>";
                                                            multipleTableValueToReplace += "";
                                                        }
                                                    }
                                                    else if (sValue.ToLower().EndsWith(".eps") ||
                                                        sValue.ToLower().EndsWith(".svg") ||
                                                              sValue.ToLower().EndsWith(".tif") ||
                                                              sValue.ToLower().EndsWith(".tiff") ||
                                                              sValue.ToLower().EndsWith(".psd") ||
                                                              sValue.ToLower().EndsWith(".tga") ||
                                                              sValue.ToLower().EndsWith(".pcx"))
                                                    {
                                                        if (Directory.Exists(ImageMagickPath))
                                                        {
                                                            //sValue = sValue.Substring(0, sValue.LastIndexOf('.'));
                                                            string[] imgargs = { HttpContext.Current.Server.MapPath("~/Content/ProductImages") + sValue, HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".jpg" };
                                                            if (converttype == "Image Magic")
                                                            {
                                                                Exefile = "convert.exe";
                                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                                pStart.CreateNoWindow = true;
                                                                pStart.UseShellExecute = false;
                                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                                while (!cmdProcess.HasExited)
                                                                {
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Exefile = "cons_rcp.exe";
                                                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                                                pStart.CreateNoWindow = true;
                                                                pStart.UseShellExecute = false;
                                                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                                                while (!cmdProcess.HasExited)
                                                                {
                                                                }
                                                            }
                                                        }
                                                        Double iHeight = 200, iWidth = 200;
                                                        var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content") + "\\temp\\ImageandAttFile" + sValue.Substring(sValue.LastIndexOf('\\'), sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) + ".jpg");
                                                        if (chkFileVal.Exists)
                                                        {
                                                            _chkSize =
                                                                Image.FromFile(
                                                                    HttpContext.Current.Server.MapPath("~/Content") +
                                                                    "\\temp\\ImageandAttFile" +
                                                                    sValue.Substring(sValue.LastIndexOf('\\'),
                                                                        sValue.LastIndexOf('.') -
                                                                        sValue.LastIndexOf('\\')) + ".jpg");
                                                            iHeight = _chkSize.Height;
                                                            iWidth = _chkSize.Width;
                                                            var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                                            imageSizeH = Convert.ToString(newVal.Height);
                                                            imageSizeW = Convert.ToString(newVal.Width);
                                                            if (_chkSize != null)
                                                            {
                                                                _chkSize.Dispose();
                                                            }

                                                            multipleTableValueToReplace +=
                                                                "<td class='reg'><img height='" + imageSizeH +
                                                                "' width='" + imageSizeW + "' src='" + "..\\Content" +
                                                                "\\temp\\ImageandAttFile" +
                                                                sValue.Substring(sValue.LastIndexOf('\\'),
                                                                    sValue.LastIndexOf('.') - sValue.LastIndexOf('\\')) +
                                                                ".jpg' />";

                                                            multipleTableValueToReplace += "<br>";
                                                            multipleTableValueToReplace += "<label align=\"left\" style=\" color: Black; BACKGROUND-COLOR: white  \">" + caption + "</label>";
                                                            multipleTableValueToReplace += "</td>";
                                                        }
                                                        else
                                                        {
                                                            //string imageUrl = "..\\Content\\ProductImages\\images\\unsupportedImageformat.jpg";
                                                            string imageUrl = "";
                                                            imageSizeH = "72";
                                                            imageSizeW = "72";
                                                            multipleTableValueToReplace += "<td class='reg'><img height='" + imageSizeH + "' width='" + imageSizeW + "' src='" + imageUrl + "' /></td>";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //  string sqlString = "SELECT ATTRIBUTE_DATATYPE,STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + item.ATTRIBUTE_ID;

                                                        //   Ocon.SQLString = sqlString;
                                                        // DataSet dss = Ocon.CreateDataSet();

                                                        var dss = _dbcontext.TB_ATTRIBUTE.Find(item.ATTRIBUTE_ID);
                                                        //if (item["ATTRIBUTE_TYPE"].ToString() == "4")
                                                        {
                                                            GetCurrencySymbol(Convert.ToString(item.ATTRIBUTE_ID));
                                                            int numberdecimel = 0;
                                                            if (dss.ATTRIBUTE_DATATYPE.ToUpper().StartsWith("NUM"))
                                                            {
                                                                string dtype = dss.ATTRIBUTE_DATATYPE;

                                                                if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                                                                {
                                                                    numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                                                                }
                                                                bool styleFormat = false;
                                                                if (dss.STYLE_FORMAT != null)
                                                                {

                                                                    styleFormat = dss.STYLE_FORMAT.Length > 0;
                                                                }
                                                                if (sValue.Trim().Length > 0)
                                                                {
                                                                    if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                                    {
                                                                        sValue = DecPrecisionFill(sValue, numberdecimel);
                                                                        sValue = styleFormat ? ApplyStyleFormat(item.ATTRIBUTE_ID, sValue.Trim()) : sValue;
                                                                    }
                                                                    else
                                                                    {
                                                                        if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000")
                                                                        {
                                                                            if ((_replaceText != "") && (Isnumber(_replaceText)))
                                                                            {
                                                                                sValue = _emptyCondition == sValue ? _replaceText : sValue;
                                                                            }
                                                                        }
                                                                        if (Isnumber(sValue))
                                                                        {
                                                                            sValue = DecPrecisionFill(sValue, numberdecimel);
                                                                            sValue = styleFormat ? ApplyStyleFormat(item.ATTRIBUTE_ID, sValue.Trim()) : sValue;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            //sValue = DataRule(item1[item["ATTRIBUTE_TYPE"].ToString()].ToString(), sValue, 0, item1[item["ATTRIBUTE_ID"].ToString()].ToString(), numberdecimel, styleFormat);
                                                        }
                                                        if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                                        {
                                                            if (_headeroptions == "All")
                                                                multipleTableValueToReplace += "<td class='reg'>" + _prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                            else
                                                                if (i == 0)
                                                            {
                                                                multipleTableValueToReplace += "<td class='reg'>" + _prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                            }
                                                            else
                                                            {
                                                                multipleTableValueToReplace += "<td class='reg'>" + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</td>";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                                                            {
                                                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                                                if (_replaceText != "")
                                                                {
                                                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                                                }
                                                                else
                                                                {
                                                                    _replaceText = sValue;
                                                                }
                                                                if (_fornumeric == "1")
                                                                {
                                                                    if (Isnumber(_replaceText.Replace(",", "")))
                                                                        multipleTableValueToReplace += "<td class='reg'>" + _prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                                    else
                                                                        multipleTableValueToReplace += "<td class='reg'>" + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") + "</td>";
                                                                }
                                                                else
                                                                    multipleTableValueToReplace += "<td class='reg'>" + _prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                                //multipleTableValueToReplace += "<td class='reg'>" + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;") + "</td>";
                                                            }
                                                            else
                                                            {
                                                                if (_headeroptions == "All")
                                                                    multipleTableValueToReplace += "<td class='reg'>" + _prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                                else
                                                                    if (i == 0)
                                                                {
                                                                    multipleTableValueToReplace += "<td class='reg'>" + _prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + _suffix + "</td>";
                                                                }
                                                                else
                                                                {
                                                                    multipleTableValueToReplace += "<td class='reg'>" + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</td>";
                                                                }
                                                            }
                                                            //if (_headeroptions == "All")
                                                            //    multipleTableValueToReplace += "<td class='reg'>" + _replaceText + "</td>";
                                                            //else
                                                            //{
                                                            //    if (i == 0)
                                                            //    {
                                                            //        multipleTableValueToReplace += "<td class='reg'>" + _replaceText + "</td>";
                                                            //    }
                                                            //    else
                                                            //    {
                                                            //        multipleTableValueToReplace += "<td class='reg'>" + sValue + "</td>";
                                                            //    }
                                                            //}
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    GetCurrencySymbol(Convert.ToString(item.ATTRIBUTE_ID));
                                                    if (_emptyCondition == "Null")
                                                    {
                                                        sValue = _replaceText;
                                                    }
                                                    else
                                                    {
                                                        sValue = "";
                                                    }
                                                    multipleTableValueToReplace += "<td class='reg'>" + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;").Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>") + "</td>";
                                                }
                                                i++;
                                            }
                                            multipleTableValueToReplace += "</tr>";
                                        }
                                        multipleTableValueToReplace += "</table><br/>";
                                    }
                                    else
                                    {
                                        multipleTableValueToReplace += "<h2>Multiple Table Preview Not Available!</h2>";
                                    }
                                }
                            }
                        }


                    }
                    else
                    {
                        //multipleTableValueToReplace += "<h2>Multiple Table Preview Not Available!</h2>";
                    }
                }
            }
            return multipleTableValueToReplace;
        }

        public string FetchTableLayout_MultipleTable(int groupId)
        {
            //DBConnection Ocon = new DBConnection();
            string layoutXml = "HORIZONTAL TABLE";
            string layoutType = string.Empty;
            var dSlayout = new DataSet();
            var layoutXmLs = _dbcontext.TB_MULTIPLE_TABLE_STRUCTURE.Where(x => x.GROUP_ID == groupId);
            //Ocon.SQLString = "select multiple_table_structure from tb_multiple_table_structure where group_id = " + groupId + "";
            // DSlayout = Ocon.CreateDataSet(); Ocon._DBCon.Close();

            if (layoutXmLs.Any())
            {
                var tbMultipleTableStructure = layoutXmLs.FirstOrDefault();
                if (tbMultipleTableStructure != null)
                {
                    layoutXml = tbMultipleTableStructure.MULTIPLE_TABLE_STRUCTURE;
                }

                if (!string.IsNullOrEmpty(layoutXml))
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(layoutXml);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        layoutType = rootNode.Attributes["TableType"].Value;

                        if (rootNode.Attributes["TableType"].Value == "Grouped")
                        {
                            XmlNodeList xmlNodeList = rootNode.ChildNodes;

                            for (int i = 0; i < xmlNodeList.Count; i++)
                            {
                                if (xmlNodeList[i].Name == "LayoutXML")
                                {
                                    if (xmlNodeList[i].ChildNodes.Count > 0)
                                    {
                                        layoutXml = xmlNodeList[i].ChildNodes[0].Value;
                                    }
                                }
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(layoutXml))
                {
                    var fileHtml = new FileStream(HttpContext.Current.Server.MapPath("~/Content/") + "\\Layout.xml", FileMode.Create);
                    var strwriter = new StreamWriter(fileHtml);
                    strwriter.Write(layoutXml);
                    strwriter.Close();
                    fileHtml.Close();
                    //obj.DisplayLayout.LoadFromXml(TempFilepath + "\\Layout.xml");
                }

            }

            if (layoutType.Length == 0)
                layoutType = "Horizontal";
            return layoutType;
        }


        //public string FetchLayout(int group_id)
        //{
        //    string LayoutXML = "HORIZONTAL TABLE";
        //    string layoutType = string.Empty;
        //    DataSet DSlayout = new DataSet();
        //    var dslayout=_dbcontext.TB_MULTIPLE_TABLE_STRUCTURE.where(x=>x.GROUP_ID==group_id)
        //    Ocon.SQLString = "  SELECT MULTIPLE_TABLE_STRUCTURE FROM TB_MULTIPLE_TABLE_STRUCTURE WHERE GROUP_ID = " + group_id + " AND IS_DEFAULT=1";
        //    DSlayout = Ocon.CreateDataSet(); Ocon._DBCon.Close();

        //    if (DSlayout != null && DSlayout.Tables.Count > 0 && DSlayout.Tables[0].Rows.Count > 0 && DSlayout.Tables[0].Rows[0][0].ToString() != "")
        //    {
        //        LayoutXML = DSlayout.Tables[0].Rows[0].ItemArray[0].ToString();
        //        if (LayoutXML != null || LayoutXML != string.Empty)
        //        {
        //            XmlDocument xmlDOc = new XmlDocument();
        //            xmlDOc.LoadXml(LayoutXML);
        //            XmlNode rootNode = xmlDOc.DocumentElement;
        //            layoutType = rootNode.Attributes["TableType"].Value;

        //            if (rootNode.Attributes["TableType"].Value == "Grouped")
        //            {
        //                XmlNodeList xmlNodeList;
        //                xmlNodeList = rootNode.ChildNodes;

        //                for (int i = 0; i < xmlNodeList.Count; i++)
        //                {
        //                    if (xmlNodeList[i].Name == "LayoutXML")
        //                    {
        //                        if (xmlNodeList[i].ChildNodes.Count > 0)
        //                        {
        //                            LayoutXML = xmlNodeList[i].ChildNodes[0].Value;
        //                        }
        //                    }
        //                }
        //            }
        //            if (LayoutXML != null || LayoutXML != string.Empty)
        //            {
        //                FileStream FileHtml = new FileStream(Application.StartupPath + "\\Layout.xml", FileMode.Create);
        //                StreamWriter Strwriter = new StreamWriter(FileHtml);
        //                Strwriter.Write(LayoutXML);
        //                Strwriter.Close();
        //                FileHtml.Close();
        //                //obj.DisplayLayout.LoadFromXml(TempFilepath + "\\Layout.xml");
        //            }
        //        }
        //    }
        //    if (layoutType.Length == 0)
        //        layoutType = "Horizontal";
        //    return layoutType;
        //}

        private DataTable familySpecsDetails(int catalogId, int familyId, string CategoryID)
        {
            var familyspecstable = new DataTable();
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                var objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@PRODUCT_ID", SqlDbType.Int).Value = 0;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 20).Value = "FAMILYPREVIEW";
                objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = CategoryID;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(familyspecstable);
            }
            return familyspecstable;
        }
        private DataTable Multipletable(int catalogId, int familyId)
        {
            var familyspecstable = new DataTable();
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                var objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_MULTIPLE_TABLE_FAMILY_PREVIEW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(familyspecstable);
            }
            return familyspecstable;
        }
        private DataTable subfamilySpecsDetails(int catalogId, int familyId, string categoryId)
        {
            var familyspecstable = new DataTable();
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                var objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_QS_LS_PICKPRODUCTPREVIEW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 20).Value = "SUBFAMILYPREVIEW";
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(familyspecstable);
            }
            return familyspecstable;
        }
        private void CategoryLevelCheck(string categoryId)
        {
            CategoryLevel = string.Empty;
            var categorycheck = _dbcontext.TB_CATEGORY.FirstOrDefault(x => x.CATEGORY_ID == categoryId);
            if (categorycheck != null)
            {
                if (categorycheck.PARENT_CATEGORY != "0")
                {
                    CategoryLevelCheck(categorycheck.PARENT_CATEGORY);
                }
                CategoryLevel = CategoryLevel + categorycheck.CATEGORY_NAME + " > ";
            }

        }
        public string FetchTableLayout(int catalogId, int familyId)
        {
            //DBConnection Ocon = new DBConnection();
            string layoutXml = "";
            //SystemSettingsCollection SettingMemebers = SystemSettingsConfiguration.GetConfig.Members;
            // string TempFilepath = SettingMemebers.GetValue(SystemSettingsCollection.SettingsList.TEMPORARYPATH.ToString());
            string layoutType = string.Empty;

            // Ocon.SQLString = "  SELECT FAMILY_TABLE_STRUCTURE FROM TB_FAMILY_TABLE_STRUCTURE WHERE FAMILY_ID = " + _familyID + " AND CATALOG_ID=" + CatalogID + " AND IS_DEFAULT=1";
            // DSlayout = Ocon.CreateDataSet(); Ocon._DBCon.Close();
            var tbFamilyTableStructure = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(
                x => x.FAMILY_ID == familyId && x.CATALOG_ID == catalogId && x.IS_DEFAULT == true);
            if (tbFamilyTableStructure != null)
                layoutXml = tbFamilyTableStructure.FAMILY_TABLE_STRUCTURE;
            if (tbFamilyTableStructure != null || layoutXml != string.Empty)
            {
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(layoutXml);
                var rootNode = xmlDOc.DocumentElement;
                if (rootNode != null)
                {
                    layoutType = rootNode.Attributes["TableType"].Value;

                    if (rootNode.Attributes["TableType"].Value == "Grouped")
                    {
                        XmlNodeList xmlNodeList;
                        xmlNodeList = rootNode.ChildNodes;

                        for (int i = 0; i < xmlNodeList.Count; i++)
                        {
                            if (xmlNodeList[i].Name == "LayoutXML")
                            {
                                if (xmlNodeList[i].ChildNodes.Count > 0)
                                {
                                    layoutXml = xmlNodeList[i].ChildNodes[0].Value;
                                }
                            }
                        }
                    }
                }
                if (layoutXml != null || layoutXml != string.Empty)
                {
                    var fileHtml = new FileStream(HttpContext.Current.Server.MapPath("~/Content") + "\\Layout.xml", FileMode.Create);
                    var strwriter = new StreamWriter(fileHtml);
                    strwriter.Write(layoutXml);
                    strwriter.Close();
                    fileHtml.Close();
                    //obj.DisplayLayout.LoadFromXml(TempFilepath + "\\Layout.xml");
                }
            }
            if (layoutType.Length == 0)
                layoutType = "Horizontal";
            return layoutType;
        }
        public string GenerateFamilyPreview(int catalogId, int familyId, string categoryId, int user_id)
        {
            UserName = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.a.CustomerId == user_id).Select(x => x.b.User_Name).FirstOrDefault();
            if (string.IsNullOrEmpty(UserName))
            { UserName = "questudio"; }
            else
            { UserName = UserName.Replace("&", "").Replace(" ", ""); }

            int[] chkAttrType;
            _valInc = 0;
            DataSet DsPreview = null;
            string tableLayout = this.FetchTableLayout(catalogId, familyId);
            string HTMLString = string.Empty;
            bool DisplayHeaders = true;
            var DynamicSQl = new StringBuilder();
            string sProdFilter = string.Empty;
            var prodFilter = _dbcontext.TB_CATALOG.FirstOrDefault(x => x.CATALOG_ID == catalogId);
            if (prodFilter != null)
            {
                sProdFilter = prodFilter.PRODUCT_FILTERS;
            }

            if (!string.IsNullOrEmpty(sProdFilter))
            {
                //sProdFilter = oDsProdFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(sProdFilter);
                var rNode = xmlDOc.DocumentElement;
                if (rNode.ChildNodes.Count > 0)
                {
                    for (int i = 0; i < rNode.ChildNodes.Count; i++)
                    {
                        var SQLstring = new StringBuilder();
                        var TableDataSetNode = rNode.ChildNodes[i];
                        if (TableDataSetNode.HasChildNodes)
                        {
                            if (TableDataSetNode.ChildNodes[2].InnerText == " ")
                            {
                                TableDataSetNode.ChildNodes[2].InnerText = "=";
                            }
                            string StrVal = TableDataSetNode.ChildNodes[3].InnerText.Trim().Replace("'", "''");
                            // var oDsattrtype = new DataSet();
                            // StringBuilder DynamicattrSQl = new StringBuilder();
                            //Ocon.SQLString = " SELECT attribute_datatype,attribute_type FROM TB_attribute WHERE  attribute_ID = " + TableDataSetNode.ChildNodes[0].InnerText + " ";
                            //oDsattrtype = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                            int attributeId = Convert.ToInt32(TableDataSetNode.ChildNodes[0].InnerText);
                            var oDsattrtype = _dbcontext.TB_ATTRIBUTE.Find(attributeId);
                            if (oDsattrtype.ATTRIBUTE_TYPE == 6)
                            {
                                SQLstring.Append("SELECT DISTINCT PRODUCT_ID FROM TB_PARTS_KEY WHERE CATALOG_ID = " + catalogId + " AND ATTRIBUTE_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + StrVal + "' AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "\n");
                            }
                            else
                            {
                                if (oDsattrtype.ATTRIBUTE_DATATYPE.ToUpper().StartsWith("N") == false)
                                {
                                    //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE Numeric_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " " + StrVal + "  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
                                    SQLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogId + ") WHERE (STRING_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + StrVal + "' AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE STRING_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " N'" + StrVal + "'  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
                                    SQLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogId + ") WHERE  (NUMERIC_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + StrVal + "' AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");

                                }
                            }
                        }
                        if (TableDataSetNode.ChildNodes[4].InnerText == "NONE")
                        {
                            DynamicSQl.Append(SQLstring);
                            break;
                        }
                        DynamicSQl.Append(SQLstring);
                        if (TableDataSetNode.ChildNodes[4].InnerText == "AND")
                        {

                            DynamicSQl.Append(" INTERSECT \n");
                        }
                        if (TableDataSetNode.ChildNodes[4].InnerText == "OR")
                        {
                            DynamicSQl.Append(" UNION \n");
                        }

                        //if (i > 0)
                        //{
                        //    DynamicSQl.Append(" union ");
                        //}
                    }
                }

            }
            var DSfilter = new DataTable();

            // Ocon.SQLString = DynamicSQl.ToString();
            if (DynamicSQl.ToString() != string.Empty)
            {
                DSfilter = Checkproductfilter(DynamicSQl.ToString());
                // DSfilter = Ocon.CreateDataSet(); Ocon._DBCon.Close();
            }

            ChkAttributeExistinFamily(familyId);
            var attrList = new int[0];
            string con = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
            // var ofrm = new TradingBell.CatalogX.CatalogXfunction();
            bool mainFamily = true;
            DataSet tempdsPreview = CatalogProductPreviewX(Convert.ToInt32(catalogId), Convert.ToInt32(familyId), attrList, con);
            //DsPreview = ofrm.CatalogProductPreviewX(Convert.ToInt32(_catalogID), Convert.ToInt32(_familyID), attrList, con);

            /* this code is for sorting the product */
            if (tempdsPreview.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow dr in tempdsPreview.Tables[0].Rows)
                {
                    int prod_id = Convert.ToInt32(dr["PRODUCT_ID"].ToString());
                    var publish =
                        _dbcontext.TB_PROD_FAMILY.FirstOrDefault(x => x.FAMILY_ID == familyId && x.PRODUCT_ID == prod_id);
                    if (publish != null)
                    {
                        if (publish.PUBLISH == false)
                        {
                            dr.Delete();
                        }
                    }

                }
            }
            var DSSS = new DataTable("ProductTable");
            foreach (DataColumn DC in tempdsPreview.Tables["ProductTable"].Columns)
            {
                if (DC.Caption == "Sort")
                {
                    DSSS.Columns.Add(DC.Caption, typeof(System.Int32));
                }
                else
                {
                    DSSS.Columns.Add(DC.Caption);
                }
            }
            foreach (DataRow row in tempdsPreview.Tables["ProductTable"].Rows)
                DSSS.ImportRow(row);

            DataRow[] tempDsRows = DSSS.Select("Family_ID=" + familyId, "Sort");

            DsPreview = tempdsPreview.Clone();

            foreach (DataRow row in tempDsRows)
                DsPreview.Tables["ProductTable"].ImportRow(row);
            DataRow[] tempsDsRows = tempdsPreview.Tables["ExtendedProperties"].Select("Family_ID=" + familyId, "Sort");
            foreach (DataRow row in tempsDsRows)
                DsPreview.Tables["ExtendedProperties"].ImportRow(row);
            /* this code is for sorting the product */


            //for filters 
            if (DSfilter != null && DSfilter.Rows.Count > 0)
            {

                var temp = DsPreview;
                var Dspreviewaltered = new DataSet();
                var t1 = new DataTable();
                Dspreviewaltered.Tables.Add(DsPreview.Tables[0].Clone());

                foreach (DataRow DR in DsPreview.Tables[0].Rows)
                {
                    DataRow[] DrfilteredRows = DSfilter.Select("PRODUCT_ID = " + DR["PRODUCT_ID"]);
                    if (DrfilteredRows != null && DrfilteredRows.Length != 0)
                    {
                        //DataRow datarw = t1.NewRow();
                        Dspreviewaltered.Tables[0].ImportRow(DR);
                    }

                }
                DsPreview = new DataSet();
                DsPreview.Tables.Add(Dspreviewaltered.Tables[0].Clone());
                foreach (DataRow DR in Dspreviewaltered.Tables[0].Rows)
                {
                    DsPreview.Tables[0].ImportRow(DR);
                }
                DataTable dsf = temp.Tables[1].Clone();
                foreach (DataRow Dr in temp.Tables[1].Rows)
                {
                    DataRow DRTEMP = dsf.NewRow();
                    dsf.ImportRow(Dr);
                }
                DsPreview.Tables.Add(dsf);
                //for Filters

            }

            #region " Date and Time Format Process"
            foreach (DataRow dtr in DsPreview.Tables[0].Rows)
            {
                string prodattr = "select ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT from TB_PROD_SPECS aps join TB_ATTRIBUTE b on aps.ATTRIBUTE_ID=b.ATTRIBUTE_ID where PRODUCT_ID=" + dtr["PRODUCT_ID"] + " ";
                //and ATTRIBUTE_DATATYPE like 'Date%'";
                //DataSet ProdAttributes = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                DataTable prodAttributes = Checkproductfilter(prodattr);
                foreach (DataRow dr in prodAttributes.Rows)
                {
                    for (int i = 0; i < DsPreview.Tables[0].Columns.Count; i++)
                    {
                        if (DsPreview.Tables[0].Columns[i].ColumnName == dr["ATTRIBUTE_NAME"].ToString())
                        {
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().StartsWith("Date"))
                            {
                                if (dr["ATTRIBUTE_DATAFORMAT"].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
                                {
                                    string tempdate = dtr[DsPreview.Tables[0].Columns[i].ColumnName].ToString();
                                    if (tempdate != "" && tempdate != null)
                                        dtr[DsPreview.Tables[0].Columns[i].ColumnName] = tempdate.Substring(3, 2) + "/" + tempdate.Substring(0, 2) + "/" + tempdate.Substring(6, 4);

                                }
                            }
                        }
                    }
                }

            }
            #endregion

            //for Publish
            //Ocon.SQLString = " SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID IN ( SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_FAMILY_ATTR_LIST WHERE FAMILY_ID = " + familyId + "  ) ";
            //DataSet DSpublish = new DataSet();
            //DSpublish = Ocon.CreateDataSet(); Ocon._DBCon.Close();
            var tbattrFamily = (_dbcontext.TB_ATTRIBUTE.Join(_dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID,
                       (tps, tpf) => new { tps, tpf })
                       .Where(x => x.tpf.FAMILY_ID == familyId).Select(@t => @t.tps).Distinct().ToList());

            if (tbattrFamily.Count > 0)
            {
                foreach (var Dr in tbattrFamily)
                {
                    if (DsPreview.Tables[0].Columns.Contains(Dr.ATTRIBUTE_NAME))
                    {
                        DsPreview.Tables[0].Columns[Dr.ATTRIBUTE_NAME].Caption = "`" + DsPreview.Tables[0].Columns[Dr.ATTRIBUTE_NAME].Caption;
                    }
                }
                for (int i = 0; i < DsPreview.Tables[0].Columns.Count; i++)
                {
                    if (DsPreview.Tables[0].Columns[i].Caption.Contains("`") == false)
                    {
                        if (DsPreview.Tables[0].Columns[i].Caption != "CATALOG_ID" && DsPreview.Tables[0].Columns[i].Caption != "FAMILY_ID" && DsPreview.Tables[0].Columns[i].Caption != "PRODUCT_ID" && DsPreview.Tables[0].Columns[i].Caption != "Publish" && DsPreview.Tables[0].Columns[i].Caption != "Sort")
                        {
                            DsPreview.Tables[0].Columns.RemoveAt(i);
                            i = 0;
                        }

                    }

                }
                for (int i = 0; i < DsPreview.Tables[0].Columns.Count; i++)
                {
                    if (DsPreview.Tables[0].Columns[i].Caption.Contains("`"))
                    {
                        if (DsPreview.Tables[0].Columns[i].Caption != "CATALOG_ID" && DsPreview.Tables[0].Columns[i].Caption != "FAMILY_ID" && DsPreview.Tables[0].Columns[i].Caption != "PRODUCT_ID" && DsPreview.Tables[0].Columns[i].Caption != "Publish" && DsPreview.Tables[0].Columns[i].Caption != "Sort")
                        {
                            DsPreview.Tables[0].Columns[i].Caption = DsPreview.Tables[0].Columns[i].Caption.Remove(0, 1);
                        }

                    }

                }

            }

            //for publish
            if (DsPreview.Tables.Count == 2)
            {
                var Dsp = DsPreview;
                var et = new DataTable("ProductTable");
                foreach (DataColumn DC in Dsp.Tables[0].Columns)
                {
                    if (DC.Caption != "CATALOG_ID" && DC.Caption != "Publish" && DC.Caption != "FAMILY_ID" && DC.Caption != "PRODUCT_ID" && DC.Caption != "Sort")
                        et.Columns.Add(DC.Caption, DC.DataType);
                }
                foreach (DataRow DR in Dsp.Tables[0].Rows)
                    et.ImportRow(DR);
                DsPreview = new DataSet();
                DsPreview.Tables.Add(et);
                DsPreview.Tables.Add(Dsp.Tables[1].Clone());
                foreach (DataRow DR in Dsp.Tables[1].Rows)
                    DsPreview.Tables[1].ImportRow(DR);

                //  if (DsPreview.Tables[1].Rows.Count >= 2 && DsPreview.Tables[1].Columns.Count > 6)
                //{
                chkAttrType = new int[DsPreview.Tables[1].Columns.Count - 5];
                for (int attrtypeCount = 0; attrtypeCount < chkAttrType.Length; attrtypeCount++)
                {
                    if (!string.IsNullOrEmpty(DsPreview.Tables[1].Rows[1].ItemArray[5 + attrtypeCount].ToString()))
                        chkAttrType[attrtypeCount] = Convert.ToInt32(DsPreview.Tables[1].Rows[1].ItemArray[5 + attrtypeCount].ToString());
                }
                //   }
                if (tableLayout == "Pivot")
                {

                    var pivotGenerator = new TradingBell.CatalogStudio.PivotTableGenerator.PivotTableGenerator(Convert.ToString(familyId), Convert.ToString(catalogId));
                    HTMLString = pivotGenerator.GeneratePivotHTML();

                }
                else if (tableLayout == "SuperTable")
                {
                    // var tGen = new TradingBell.CatalogStudio.TableGenerator.TableGenerator(Convert.ToString(familyId), Convert.ToString(catalogId));
                    //tGen.ApplicationStartupPath = HttpContext.Current.Server.MapPath("~/Content/");
                    // tGen.ImagePath = HttpContext.Current.Server.MapPath("~/Content/ProductImages/");
                    var objSuperTable = new SuperTable();
                    objSuperTable.ImageMagickPath = ImageMagickPath;
                    HTMLString = objSuperTable.GenerateTableHtml(familyId, catalogId, CategoryID, UserName);
                }
                else if (tableLayout == "Grouped")
                {
                    var DupDsPreview = DsPreview.Copy();
                    foreach (DataColumn DC in DupDsPreview.Tables[0].Columns)
                    {
                        DC.ColumnName = DC.ColumnName.Replace("\r", "").Replace("\n", "");
                    }
                    foreach (DataColumn DC in DupDsPreview.Tables[1].Columns)
                    {
                        DC.ColumnName = DC.ColumnName.Replace("\r", "").Replace("\n", "");
                    }
                    //  HTMLString = GenHTMLGroupedTable();
                    HTMLString = GenerateHorizontalHTML(DsPreview, chkAttrType, DisplayHeaders, Dsp.Tables[0], UserName);
                }
                else if (tableLayout == "Horizontal" || tableLayout == "")
                {

                    HTMLString = GenerateHorizontalHTML(DsPreview, chkAttrType, DisplayHeaders, Dsp.Tables[0], UserName);

                }
                else if (tableLayout == "Vertical")
                {
                    HTMLString = GenerateVerticalHTML(DsPreview, chkAttrType, DisplayHeaders, Dsp.Tables[0], UserName);
                }
                else if (tableLayout.Trim() == string.Empty)
                {
                    HTMLString = "<html><CENTER ><HEAD></HEAD><BODY><P> INVALID LAYOUT SPECIFICATION ! </P></BODY></CENTER></html>";

                }



            }

            return HTMLString.Replace("&lt;br /&gt;", "<br>").Replace("&lt;/br&gt;", "<br>").Replace("&lt;br&gt;", "<br>")
                .Replace("&lt;br/&gt;", "<br>").Replace("&amp;lt;br/&amp;gt;", "<br>").Replace("&amp;nbsp;", "&nbsp;");

        }
        //End
        private void ChkAttributeExistinFamily(int familyId)
        {

            DataTable OdsCalc = new DataTable();
            DataTable Dschk = new DataTable();
            string AttrCalcFormaula = string.Empty;
            int ContinueCalclautedCols = 0;
            string TempStr = string.Empty;
            int CheckCalculatedCtr = 0;
            var attrcalcformula = " SELECT ATTRIBUTE_CALC_FORMULA FROM TB_ATTRIBUTE WHERE IS_CALCULATED =1  AND ATTRIBUTE_CALC_FORMULA <> '' " +
                            "  AND ATTRIBUTE_ID IN (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID IN " +
                            "  (SELECT PRODUCT_ID FROM TB_PROD_FAMILY WHERE FAMILY_ID = " + familyId + "))";
            OdsCalc = Checkproductfilter(attrcalcformula);
            List<string> attrList = new List<string>();
            foreach (DataRow Dr in OdsCalc.Rows)
            {
                AttrCalcFormaula = Dr[0].ToString();
                string tempAttrEq = AttrCalcFormaula;

                while (tempAttrEq.Contains("["))
                {
                    int startIndex = tempAttrEq.IndexOf('[');
                    int endIndex = tempAttrEq.IndexOf(']');
                    attrList.Add(tempAttrEq.Substring(startIndex + 1, endIndex - startIndex - 1));
                    tempAttrEq = tempAttrEq.Substring(endIndex + 1, tempAttrEq.Length - endIndex - 1);
                }
            }
            for (int j = 0; j < attrList.Count; j++)
            {
                var attrnames = " SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE TA WHERE ATTRIBUTE_ID IN ( SELECT  DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID IN  " +
                                " (SELECT PRODUCT_ID FROM TB_PROD_fAMILY WHERE FAMILY_ID = " + familyId + ")) AND TA.ATTRIBUTE_NAME = '" + attrList[j].Replace("'", "''") + "' ";
                Dschk = Checkproductfilter(attrnames);
                if (Dschk.Rows.Count > 0)
                {
                    CheckCalculatedCtr++;
                }
            }

            if (CheckCalculatedCtr == attrList.Count)
            {
                ContinueCalclautedCols = 1;
            }
        }

        private string GenerateHorizontalHTML(DataSet DsPreview, int[] chkAttrType, bool DisplayHeaders, DataTable dsp, string username)
        {
            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == username).Select(x => x.a.Comments).FirstOrDefault();
            if (string.IsNullOrEmpty(CustomerFolder))
            { CustomerFolder = "/STUDIOSOFT"; }
            else
            { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }

            //ServiceProvider.ProductValidationServices Oservices = new TradingBell.CatalogStudio.ServiceProvider.ProductValidationServices();
            string AttrID = string.Empty;
            StringBuilder strBldr = new StringBuilder();
            strBldr.Append("<HTML>");
            strBldr.Append("<CENTER>");
            strBldr.Append("<HEAD>");
            strBldr.Append("</HEAD>");
            //strBldr.Append("<TABLE class=\"preview\">");
            strBldr.Append("<TABLE class=\"table table-condensed table-responsive table-bordered table-striped 7\">");
            // strBldr.Append("<style>td{font-family:arial Unicode ms;font-size:12px;}th{font-family:arial unicode ms;font-size:12px;font-weight:Bold}</style>");
            // bool DisplayHeaders = true;
            if (DisplayHeaders)
            {
                strBldr.Append("<TR>");
                if (DsPreview.Tables[0].Rows.Count > 0)
                    for (int j = 0; j < DsPreview.Tables[0].Columns.Count; j++)
                    {
                        //strBldr.Append("<TD ALIGN=\"Center\" VALIGN=\"Middle\" style=\"width: 200px; color: Black; BACKGROUND-COLOR: #99CCFF   \" >");
                        strBldr.Append("<TD>");
                        strBldr.Append(DsPreview.Tables[0].Columns[j].Caption.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r", "<br/>").Replace("\n", "&nbsp;"));
                        strBldr.Append("</TD>");
                    }
                strBldr.Append("</TR>");
            }
            string ValueFortag = string.Empty;
            for (int i = 0; i < DsPreview.Tables[0].Rows.Count; i++)
            {
                strBldr.Append("<TR>");

                for (int j = 0; j < DsPreview.Tables[0].Columns.Count; j++)
                {
                    string alignVal = "LEFT";
                    AttrID = DsPreview.Tables[1].Rows[0].ItemArray[5 + j].ToString();
                    ExtractCurrenyFormat(Convert.ToInt32(AttrID));
                    int attributeId = Convert.ToInt32(AttrID);
                    var attrDatatype = _dbcontext.TB_ATTRIBUTE.Find(attributeId);

                    // Ocon.SQLString = "SELECT ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + AttrID;
                    // DataSet DSS = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                    //DataSet styleDS = new DataSet();
                    double dt = 0;
                    string style = string.Empty;
                    // Ocon.SQLString = "SELECT STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + AttrID;
                    // styleDS = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                    if (!string.IsNullOrEmpty(attrDatatype.STYLE_FORMAT))
                    {
                        style = attrDatatype.STYLE_FORMAT;
                    }
                    int index = 0;
                    index = style.IndexOf("[");
                    if (index != -1 && DsPreview.Tables[0].Rows[i][j].ToString().Length > 0)
                    {
                        style = style.Substring(0, index - 1);
                        dt = Convert.ToDouble(DsPreview.Tables[0].Rows[i][j].ToString());
                    }

                    if (chkAttrType[j] == 4 || attrDatatype.ATTRIBUTE_DATATYPE.Substring(0, 3).ToUpper() == "NUM")
                    {
                        alignVal = "RIGHT";
                        if (DsPreview.Tables[0].Rows[i][j].ToString().Length > 0)
                            dt = Convert.ToDouble(DsPreview.Tables[0].Rows[i][j].ToString());

                    }

                    if (chkAttrType[j] == 3)
                    {
                        var captionname = string.Empty;
                        var productid = Convert.ToInt32(dsp.Rows[i]["PRODUCT_ID"]);
                        var productimagecaption = _dbcontext.TB_PROD_SPECS.FirstOrDefault(x => x.ATTRIBUTE_ID == attributeId && x.PRODUCT_ID == productid);
                        if (productimagecaption != null)
                        {
                            captionname = productimagecaption.OBJECT_NAME;
                        }
                        strBldr.Append("<TD ALIGN=\"" + alignVal +
                                       GetProductCellString(
                                           DsPreview.Tables[0].Rows[i][j].ToString()
                                               .Replace("<", "&lt;")
                                               .Replace(">", "&gt;"),
                                           captionname, CustomerFolder));
                    }
                    else //if (chkAttrType[j] == 4)
                    {
                        if ((_headeroptions == "All") || (_headeroptions != "All" && i == 0))
                        {
                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null) &&
                                (DsPreview.Tables[0].Rows[i][j].ToString() == string.Empty))
                            {
                                ValueFortag = _replaceText;
                            }
                            else if ((DsPreview.Tables[0].Rows[i][j].ToString()) == (_emptyCondition))
                            {
                                ValueFortag = _replaceText;
                            }
                            else
                            {
                                if (
                                    Isnumber(
                                        DsPreview.Tables[0].Rows[i][j].ToString()
                                            .Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("\r", "<br/>")
                                            .Replace("\n", "&nbsp;")))
                                {
                                    if (dt != 0.0)
                                    {
                                        ValueFortag = _prefix +
                                                      dt.ToString(style.Trim())
                                                          .Replace("<", "&lt;")
                                                          .Replace(">", "&gt;")
                                                          .Replace("\r", "<br/>")
                                                          .Replace("\n", "&nbsp;") + _suffix;
                                    }
                                    else
                                    {
                                        ValueFortag = _prefix +
                                                      DsPreview.Tables[0].Rows[i][j].ToString()
                                                          .Replace("<", "&lt;")
                                                          .Replace(">", "&gt;")
                                                          .Replace("\r", "<br/>")
                                                          .Replace("\n", "&nbsp;") + _suffix;
                                    }
                                }
                                else
                                {
                                    if (DsPreview.Tables[0].Rows[i][j].ToString().Length > 0)
                                    {
                                        ValueFortag = _prefix +
                                                      DsPreview.Tables[0].Rows[i][j].ToString()
                                                          .Replace("<", "&lt;")
                                                          .Replace(">", "&gt;")
                                                          .Replace("\r", "<br/>")
                                                          .Replace("\n", "&nbsp;") + _suffix;
                                    }
                                    else
                                    {
                                        ValueFortag = string.Empty;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (
                                Isnumber(
                                    DsPreview.Tables[0].Rows[i][j].ToString()
                                        .Replace("<", "&lt;")
                                        .Replace(">", "&gt;")
                                        .Replace("\r", "<br/>")
                                        .Replace("\n", "&nbsp;")))
                            {
                                if (dt != 0.0)
                                {
                                    ValueFortag =
                                        dt.ToString(style.Trim())
                                            .Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("\r", "<br/>")
                                            .Replace("\n", "&nbsp;");
                                }
                                else
                                {
                                    ValueFortag =
                                        DsPreview.Tables[0].Rows[i][j].ToString()
                                            .Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("\r", "<br/>")
                                            .Replace("\n", "&nbsp;");
                                }
                            }
                            else
                            {
                                ValueFortag =
                                    DsPreview.Tables[0].Rows[i][j].ToString()
                                        .Replace("<", "&lt;")
                                        .Replace(">", "&gt;")
                                        .Replace("\r", "<br/>")
                                        .Replace("\n", "&nbsp;");
                            }
                        }
                        if (
                            attrDatatype.ATTRIBUTE_DATAFORMAT.StartsWith(
                                "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                        {
                            if (
                                DsPreview.Tables[0].Rows[i][j].ToString()
                                    .Replace("_", "")
                                    .Replace("/", "")
                                    .Replace(":", "")
                                    .Trim()
                                    .Length != 0)
                            {
                                ValueFortag = DsPreview.Tables[0].Rows[i][j].ToString();
                                if (ValueFortag != "--")
                                    ValueFortag = ValueFortag.Substring(3, 2) + "/" + ValueFortag.Substring(0, 2) + "/" +
                                                  ValueFortag.Substring(6, 4);
                            }
                        }
                        strBldr.Append("<TD ALIGN=\"" + alignVal + "\" VALIGN=\"Middle\">" + ValueFortag + "</TD>");
                    }
                }
                strBldr.Append("</TR>");
            }

            strBldr.Append("</TABLE>");
            strBldr.Append("<CENTER>");
            strBldr.Append("</HTML>");

            return strBldr.ToString();

        }
        private string GenerateVerticalHTML(DataSet DsPreview, int[] chkAttrType, bool DisplayHeaders, DataTable dsp, string username)
        {
            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == username).Select(x => x.a.Comments).FirstOrDefault();
            if (string.IsNullOrEmpty(CustomerFolder))
            { CustomerFolder = "/STUDIOSOFT"; }
            else
            { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }

            string AttrID = string.Empty;
            string ValueFortag = string.Empty;
            StringBuilder strBldr = new StringBuilder();
            strBldr.Append("<HTML>");
            strBldr.Append("<CENTER>");
            strBldr.Append("<HEAD>");
            strBldr.Append("</HEAD>");
            //strBldr.Append("<TABLE class=\"preview\">");
            strBldr.Append("<TABLE class=\"table table-condensed table-responsive table-bordered table-striped 8\">");
            //strBldr.Append("<style>td{font-family:arial Unicode ms;font-size:12px;}th{font-family:arial Unicode ms;font-size:12px;font-weight:Bold}</style>");

            bool newRow = true;
            for (int i = 0; i < DsPreview.Tables[0].Columns.Count; i++)
            {
                strBldr.Append("<TR>");
                newRow = true;
                for (int j = 0; j < DsPreview.Tables[0].Rows.Count; j++)
                {
                    string alignVal = "LEFT";
                    AttrID = DsPreview.Tables[1].Rows[0].ItemArray[5 + i].ToString();
                    ExtractCurrenyFormat(Convert.ToInt32(AttrID));
                    //Ocon.SQLString = "SELECT ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + AttrID;
                    //DataSet DSS = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                    //DataSet styleDS = new DataSet();
                    double dt = 0;
                    string style = string.Empty;
                    // Ocon.SQLString = "SELECT STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + AttrID;
                    // styleDS = Ocon.CreateDataSet(); Ocon._DBCon.Close();
                    // style = styleDS.Tables[0].Rows[0].ItemArray[0].ToString();
                    int attributeId = Convert.ToInt32(AttrID);
                    var attrDatatype = _dbcontext.TB_ATTRIBUTE.Find(attributeId);
                    style = attrDatatype.STYLE_FORMAT;
                    int index = 0;
                    index = style.IndexOf("[");
                    if (index != -1 && DsPreview.Tables[0].Rows[j][i].ToString().Length > 0)
                    {
                        style = style.Substring(0, index - 1);
                        dt = Convert.ToDouble(DsPreview.Tables[0].Rows[j][i].ToString());
                    }

                    if (chkAttrType[i] == 4 || attrDatatype.ATTRIBUTE_DATATYPE.Substring(0, 3).ToUpper() == "NUM")
                    {
                        alignVal = "RIGHT";
                        if (DsPreview.Tables[0].Rows[j][i].ToString().Length > 0)
                            dt = Convert.ToDouble(DsPreview.Tables[0].Rows[j][i].ToString());
                    }
                    if (DisplayHeaders && newRow)
                    {
                        //strBldr.Append("<TD ALIGN=\"" + alignVal + "\" VALIGN=\"Middle\" style=\"width: 200px; color: Black; BACKGROUND-COLOR: #99CCFF   \" >");
                        strBldr.Append(DsPreview.Tables[0].Columns[i].Caption.Replace("<", "&lt;").Replace(">", "&gt;") + "</TD>");
                        //strBldr.Append(DsPreview.Tables[0].Columns[i].Caption + "</TD>");
                        j = -1;
                        newRow = false;
                    }
                    else
                    {
                        if (chkAttrType[i] == 3)
                        {
                            var captionname = string.Empty;
                            var productid = Convert.ToInt32(dsp.Rows[i]["PRODUCT_ID"]);
                            var productimagecaption = _dbcontext.TB_PROD_SPECS.FirstOrDefault(x => x.ATTRIBUTE_ID == attributeId && x.PRODUCT_ID == productid);
                            if (productimagecaption != null)
                            {
                                captionname = productimagecaption.OBJECT_NAME;
                            }
                            strBldr.Append("<TD ALIGN=\"" + alignVal +
                                           GetProductCellString(
                                               DsPreview.Tables[0].Rows[j][i].ToString()
                                                   .Replace("<", "&lt;")
                                                   .Replace(">", "&gt;"),
                                               captionname, CustomerFolder));
                        }
                        else //if (chkAttrType[i] == 4)
                        {
                            if ((_headeroptions == "All") || (_headeroptions != "All" && j == 0))
                            {
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == null) &&
                                    (DsPreview.Tables[0].Rows[j][i].ToString() == string.Empty))
                                {
                                    ValueFortag = _replaceText;
                                }
                                else if ((DsPreview.Tables[0].Rows[j][i].ToString()) == (_emptyCondition))
                                {
                                    ValueFortag = _replaceText;
                                }
                                else
                                {
                                    if (
                                        Isnumber(
                                            DsPreview.Tables[0].Rows[j][i].ToString()
                                                .Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("\r", "<br/>")
                                                .Replace("\n", "&nbsp;")))
                                    {
                                        if (dt != 0.0)
                                        {
                                            ValueFortag = _prefix +
                                                          dt.ToString(style.Trim())
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")
                                                              .Replace("\r", "<br/>")
                                                              .Replace("\n", "&nbsp;") + _suffix;
                                        }
                                        else
                                        {
                                            ValueFortag = _prefix +
                                                          DsPreview.Tables[0].Rows[j][i].ToString()
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")
                                                              .Replace("\r", "<br/>")
                                                              .Replace("\n", "&nbsp;") + _suffix;
                                        }
                                    }
                                    else
                                    {
                                        if (DsPreview.Tables[0].Rows[j][i].ToString().Length > 0)
                                        {
                                            ValueFortag = _prefix +
                                                          DsPreview.Tables[0].Rows[j][i].ToString()
                                                              .Replace("<", "&lt;")
                                                              .Replace(">", "&gt;")
                                                              .Replace("\r", "<br/>")
                                                              .Replace("\n", "&nbsp;") + _suffix;
                                        }
                                        else
                                        {
                                            ValueFortag = string.Empty;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (
                                    Isnumber(
                                        DsPreview.Tables[0].Rows[j][i].ToString()
                                            .Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("\r", "<br/>")
                                            .Replace("\n", "&nbsp;")))
                                {
                                    if (dt != 0.0)
                                    {
                                        ValueFortag =
                                            dt.ToString(style.Trim())
                                                .Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("\r", "<br/>")
                                                .Replace("\n", "&nbsp;")
                                                .ToString();
                                    }
                                    else
                                    {
                                        ValueFortag =
                                            DsPreview.Tables[0].Rows[j][i].ToString()
                                                .Replace("<", "&lt;")
                                                .Replace(">", "&gt;")
                                                .Replace("\r", "<br/>")
                                                .Replace("\n", "&nbsp;")
                                                .ToString();
                                    }
                                }
                                else
                                {
                                    ValueFortag =
                                        DsPreview.Tables[0].Rows[j][i].ToString()
                                            .Replace("<", "&lt;")
                                            .Replace(">", "&gt;")
                                            .Replace("\r", "<br/>")
                                            .Replace("\n", "&nbsp;");
                                }
                            }
                            if (Isnumber(ValueFortag.Trim()))
                            // if (chkAttrType[j] == 4 && ValueFortag != string.Empty && Oservices.Isnumber(ValueFortag) == true)
                            {
                                if (dt != 0.0)
                                {
                                    ValueFortag = dt.ToString(style.Trim());
                                }
                                else
                                {
                                    ValueFortag = DsPreview.Tables[0].Rows[j][i].ToString();
                                }

                            }
                            if (
                                attrDatatype.ATTRIBUTE_DATAFORMAT.StartsWith(
                                    "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                            {
                                if (
                                    DsPreview.Tables[0].Rows[j][i].ToString()
                                        .Replace("_", "")
                                        .Replace("/", "")
                                        .Replace(":", "")
                                        .Trim()
                                        .Length != 0)
                                {
                                    ValueFortag = DsPreview.Tables[0].Rows[i][j].ToString();
                                    ValueFortag = ValueFortag.Substring(3, 2) + "/" + ValueFortag.Substring(0, 2) + "/" +
                                                  ValueFortag.Substring(6, 4);
                                }
                            }
                            strBldr.Append("<TD ALIGN=\"" + alignVal + "\" VALIGN=\"Middle\"> " + ValueFortag + "</TD>");
                        }

                    }


                }

                strBldr.Append("</TR>");
            }

            strBldr.Append("</TABLE>");
            strBldr.Append("</CENTER>");
            strBldr.Append("</HTML>");

            return strBldr.ToString();

        }

        public DataSet CatalogProductPreviewX(int catalogID, int familyID, int[] attributeList, string connvalue)
        {
            // var asdf = new Connection();
            // asdf.ConnSettings(connvalue);
            // var csPt = new CSDBProviderEX.ProductCatalog(catalogID, familyID, true, true, attributeList, true, "");
            DataSet ds = GetProductCatalogPreview(catalogID, familyID, attributeList);
            ds.Tables.Add(ExtendedPropertiesPreview(catalogID, familyID, attributeList));
            ds.Tables[0].Columns.Remove("FAMILYID");
            ds.Tables[1].Columns.Remove("FAMILYID");
            if (ds.Tables[0].Columns.Count < ds.Tables[1].Columns.Count)
            {
                int lastchkVal = ds.Tables[1].Columns.Count - ds.Tables[0].Columns.Count;
                for (int colCount = 0; colCount < lastchkVal; colCount++)
                {
                    ds.Tables[1].Columns.RemoveAt(ds.Tables[0].Columns.Count);
                }
            }
            return ds;
        }
        private DataTable ExtendedPropertiesPreview(int catalogID, int familyID, int[] attributeList)
        {
            var et = new DataTable("ExtendedProperties");
            var iAttrList = new int[0];

            //for (int i = 0; i < iAttrList.Length - 1; i++)
            //{
            //    //iAttrList[i] = (int) grdSelect.Rows[i].Cells["ATTRIBUTE_ID"].Value;
            //}
            // var csPt = new CSDBProviderEX.ProductTable(catalogID, familyID, iAttrList, "");
            DataTable eet = GetProductTableExPropertiesPreview(catalogID, familyID, true, attributeList);
            foreach (DataColumn dc in eet.Columns)
                et.Columns.Add(dc.Caption, dc.DataType);
            foreach (DataRow dr in eet.Rows)
                et.ImportRow(dr);
            return et;
        }
        public DataTable GetProductTableExPropertiesPreview(int catalogID, int familyID, bool mainFamily, int[] attributeList)
        {
            var dt2 = new DataTable("Product2");
            // taprod.FillByCatalogFamilyExProperties(tblprod, _catalogId, _FamilyId);

            //  tacpa.FillByPreview(tblcpa, _catalogId, _FamilyId);
            var tblprod = new DataTable();
            var tblcpa = new DataTable();
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "FillByCatalogFamilyExProperties";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@CATALOG_ID"].Value = catalogID;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@FAMILY_ID"].Value = familyID;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(tblprod);
            }
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "FillByPreview";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@CATALOG_ID"].Value = catalogID;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@FAMILY_ID"].Value = familyID;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(tblcpa);
            }
            DataTable dt = GetTransposedProductTable(tblprod, tblcpa, attributeList, false);

            dt.Columns.Add("FAMILYID", typeof(int));

            return dt;
        }

        //public DataSet  ProductCatalog(int catalogId, int familyId, bool loadXFamily, bool loadXProductTable, int[] attributeIdList, bool mergeFamilyWithProductTable, string catId1)
        //{
        //    _catName = catId1;
        //    _catalogId = catalogId;
        //    _familyId = familyId;
        //    _loadXFamily = loadXFamily;
        //    _loadXProductTable = loadXProductTable;
        //    _attributeIdList = attributeIdList;
        //    _mergeFamilyWithProductTable = mergeFamilyWithProductTable;
        //}
        public DataSet GetProductCatalogPreview(int catalogID, int familyID, int[] attributeList)
        {
            var dsPf = new DataSet { EnforceConstraints = false };

            //Load product table
            // var csProd = new ProductTable(_catalogId, _familyId, _attributeIdList, _catName);
            DataTable dtProductTable = GetProductTablePreview(catalogID, familyID, attributeList);
            dtProductTable.TableName = "ProductTable";
            dsPf.Tables.Add(dtProductTable);
            return dsPf;
        }
        public DataTable GetProductTablePreview(int catalogID, int familyID, int[] attributeList)
        {

            var tblprod = new DataTable();
            var tblprodSf = new DataTable();
            var tblcpa = new DataTable();
            var tblcpaSf = new DataTable();
            // taprod.FillByCatalogFamilyExPreview(tblprod, _catalogId, _FamilyId);
            //  taprod.FillByCatalogSubFamily(tblprodSf, _catalogId, _FamilyId);

            //  tacpa.FillByPreview(tblcpa, _catalogId, _FamilyId);
            //  tacpa.FillBySubPreview(tblcpaSf, _catalogId, _FamilyId);
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "FillByCatalogFamilyExPreview";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@CATALOG_ID"].Value = catalogID;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@FAMILY_ID"].Value = familyID;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(tblprod);
            }
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "FillByCatalogSubFamily";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@CATALOG_ID"].Value = catalogID;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@FAMILY_ID"].Value = familyID;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(tblprodSf);
            }
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "FillBySubPreview";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@CATALOG_ID"].Value = catalogID;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@FAMILY_ID"].Value = familyID;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(tblcpaSf);
            }
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                //objSqlConnection.ConnectionTimeout = 0;
                objSqlCommand.CommandText = "FillByPreview";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@CATALOG_ID"].Value = catalogID;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int);
                objSqlCommand.Parameters["@FAMILY_ID"].Value = familyID;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                objSqlCommand.CommandTimeout = 0;
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(tblcpa);
            }
            DataTable dt = GetTransposedProductTable(tblprod, tblcpa, attributeList, false);
            DataTable dt2 = GetTransposedProductTable(tblprodSf, tblcpaSf, attributeList, false);

            dt.Columns.Add("FAMILYID", typeof(int));

            foreach (DataRow dr in dt.Rows)
            {
                dr["FAMILYID"] = dr["FAMILY_ID"];
            }
            return dt;
        }

        private DataTable GetTransposedProductTable(DataTable productInfo, DataTable productAttributes, int[] attributeList, bool PDFCatalog)
        {

            var dtnew = new DataTable();
            dtnew.Columns.Add("CATALOG_ID", typeof(int));
            dtnew.Columns.Add("FAMILY_ID", typeof(int));
            dtnew.Columns.Add("PRODUCT_ID", typeof(int));
            if (PDFCatalog)
                dtnew.Columns.Add("SORT_ORDER", typeof(int));

            var dcpks = new DataColumn[3];
            if (PDFCatalog)
            {
                dcpks = new DataColumn[4];
                dcpks[0] = dtnew.Columns["CATALOG_ID"];
                dcpks[1] = dtnew.Columns["FAMILY_ID"];
                dcpks[2] = dtnew.Columns["PRODUCT_ID"];
                dcpks[3] = dtnew.Columns["SORT_ORDER"];
            }
            else
            {
                dcpks[0] = dtnew.Columns["CATALOG_ID"];
                dcpks[1] = dtnew.Columns["FAMILY_ID"];
                dcpks[2] = dtnew.Columns["PRODUCT_ID"];
            }

            dtnew.Constraints.Add("PK", dcpks, true);

            foreach (DataRow dr in productAttributes.Rows)
            {

                if (attributeList.Length > 0)
                {
                    bool exists = CheckExists(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()), attributeList);
                    if (exists)
                    {
                        //_AttributeIdList[iCtr]
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                        dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        {
                            productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString(), typeof(Double));
                        }
                        else
                        {
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                        }


                        //enhance - Make sure to check if the column exists in this table

                        //OLD CODE------------
                        //DataRow[] drs = ProductInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"], "FAMILY_ID,SORT_ORDER");
                        //DataTable dtsel;
                        //dtsel = ProductInfo.Clone();
                        //for (int i = 0; i < drs.Length; i++)
                        //{
                        //    dtsel.ImportRow(drs[i]);
                        //}
                        //-------------------
                        DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"], "FAMILY_ID,SORT_ORDER");
                        var dtsel = new DataTable();
                        if (drs.Any())
                        {
                            dtsel = drs.CopyToDataTable();
                        }

                        try
                        {
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" && dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                                {
                                    bool stringValue = dtsel.Rows.Cast<DataRow>().Any(dr1 => dr1["STRING_VALUE"].ToString().Length == 0);
                                    if (stringValue == false)
                                    {
                                        dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                        dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                    }
                                }
                        }
                        catch (Exception) { }

                        dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                        dtsel.Dispose();
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                            dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                        else
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";




                    }


                }
                else
                {
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    else
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();




                    //enhance - Make sure to check if the column exists in this table

                    DataColumnCollection dtnewcolumns = dtnew.Columns;

                    if (!dtnewcolumns.Contains(dr["ATTRIBUTE_NAME"].ToString()))
                        dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());

                    DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"]);
                    DataTable dtsel;
                    dtsel = productInfo.Clone();
                    for (int i = 0; i < drs.Length; i++)
                    {
                        dtsel.ImportRow(drs[i]);
                        //    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                        //dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        //    {
                        //        dtsel.Rows[i].ItemArray[7] = 1;// Convert.ToDecimal(dtsel.Rows[i].ItemArray[0]);
                        //    }
                    }
                    // int i = 0;
                    //foreach (DataRow dr2 in dtnew.Rows)
                    //{
                    //    foreach (DataRow dr1 in dtsel.Rows)
                    //    {
                    //        dr2[dr["ATTRIBUTE_NAME"].ToString()] = dr1["STRING_VALUE"].ToString();
                    //        break;
                    //    }
                    //}
                    try
                    {
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" && dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                            {
                                bool stringValue = false;
                                foreach (DataRow dr1 in dtsel.Rows)
                                {
                                    if (dr1["STRING_VALUE"].ToString().Length == 0)
                                    {
                                        stringValue = true;
                                        break;
                                    }
                                }
                                if (stringValue == false)
                                {
                                    dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                    dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                }
                            }
                    }
                    catch (Exception) { }

                    dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                    dtsel.Dispose();
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                    else
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";

                }
            }

            return dtnew;
        }
        private bool CheckExists(int iattrID, int[] attributeList)
        {
            if (iattrID == 0)
            {
                //  return true;
            }
            return attributeList.Any(t => iattrID == t);
        }

        public string ImageConversionForAll(string value, string id, int attrid, string ImageMagickPath, string customerFolder)
        {
            string preview_path = "";
            System.Drawing.Image chkSize;
            string userImagePath = HttpContext.Current.Server.MapPath("~/Content/ProductImages");
            var chkFile = new FileInfo(userImagePath + value.Trim());
            if (chkFile.Exists)
            {
                if (chkFile.Extension.ToUpper() == ".EPS" ||
                    chkFile.Extension.ToUpper() == ".SVG" ||
                    chkFile.Extension.ToUpper() == ".TIF" ||
                    chkFile.Extension.ToUpper() == ".TIFF" ||
                    chkFile.Extension.ToUpper() == ".TGA" ||
                    chkFile.Extension.ToUpper() == ".PCX")
                {

                    if (Directory.Exists(ImageMagickPath))
                    {
                        string file_name = id + "_" + attrid.ToString() + "_" + chkFile.Name;
                        string[] imgargs = {userImagePath + value,
                                                    HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder)  + "\\temp\\ImageandAttFile\\" + file_name   + ".jpg" };
                        var chkfile = new FileInfo(imgargs[1]);
                        if (!chkfile.Exists)
                        {
                            if (converttype == "Image Magic")
                            {
                                Exefile = "convert.exe";
                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                            else
                            {
                                Exefile = "cons_rcp.exe";
                                // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                                var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                                pStart.CreateNoWindow = true;
                                pStart.UseShellExecute = false;
                                pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                                while (!cmdProcess.HasExited)
                                {
                                }
                            }
                        }
                        var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                        if (chkFileVal.Exists)
                        {
                            var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                            chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                            if (!chkFileVal.Exists)
                                imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                            imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                            chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                            if (!chkFileVal.Exists)
                                imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                            imm.Dispose();
                            var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                            asd.Delete();
                            //////////
                            chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                            int iHeight = chkSize.Height;
                            int iWidth = chkSize.Width;
                            Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                            chkSize.Dispose();
                            preview_path = "..//Content//ProductImages/" + customerFolder + "//temp//ImageandAttFile//" + file_name + ".jpg";


                        }
                        else
                        {
                            chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                            if (chkFileVal.Exists)
                            {
                                var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                                if (!chkFileVal.Exists)
                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                                imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                                chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                                if (!chkFileVal.Exists)
                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                imm.Dispose();
                                var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                                asd.Delete();
                                //////////
                                chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                int iHeight = chkSize.Height;
                                int iWidth = chkSize.Width;
                                Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                chkSize.Dispose();
                                preview_path = "..//Content//temp//ImageandAttFile//" + file_name + ".jpg";

                            }
                        }
                    }
                    //}
                }
                else if (chkFile.Extension.ToUpper() == ".PSD")
                {
                    if (Directory.Exists(ImageMagickPath))
                    {
                        string file_name = id.ToString() + "_" + attrid.ToString() + "_" + chkFile.Name;
                        string[] imgargs = {userImagePath+"/" + value.Trim()+"[0]",
                                                    HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder)  + "\\temp\\ImageandAttFile\\" + file_name   + ".jpg" };
                        if (converttype == "Image Magic")
                        {
                            Exefile = "convert.exe";
                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                            pStart.CreateNoWindow = true;
                            pStart.UseShellExecute = false;
                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                            while (!cmdProcess.HasExited)
                            {
                            }
                        }
                        else
                        {
                            Exefile = "cons_rcp.exe";
                            // var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1] + "\"");
                            var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, " -s " + "\"" + imgargs[0] + "\"" + " -o " + " \"" + imgargs[1] + "\"");
                            pStart.CreateNoWindow = true;
                            pStart.UseShellExecute = false;
                            pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            System.Diagnostics.Process cmdProcess = System.Diagnostics.Process.Start(pStart);
                            while (!cmdProcess.HasExited)
                            {
                            }
                        }
                        var chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                        if (chkFileVal.Exists)
                        {
                            var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                            chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                            if (!chkFileVal.Exists)
                                imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                            imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                            chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                            if (!chkFileVal.Exists)
                                imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                            imm.Dispose();
                            var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                            asd.Delete();
                            //////////
                            chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                            int iHeight = chkSize.Height;
                            int iWidth = chkSize.Width;
                            var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                            chkSize.Dispose();
                            preview_path = "..//Content//ProductImages//" + customerFolder + "//temp//ImageandAttFile\\" + file_name + ".jpg";

                        }
                        else
                        {
                            chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile" + chkFile.Name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                            if (chkFileVal.Exists)
                            {
                                var imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name.Substring(0, chkFile.Name.LastIndexOf('.')) + ".jpg");
                                chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");

                                if (!chkFileVal.Exists)
                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);

                                imm = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".bmp");
                                chkFileVal = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");

                                if (!chkFileVal.Exists)
                                    imm.Save(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                imm.Dispose();
                                var asd = new FileInfo(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".gif");
                                asd.Delete();
                                //////////
                                chkSize = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder) + "\\temp\\ImageandAttFile\\" + file_name + ".jpg");
                                int iHeight = chkSize.Height;
                                int iWidth = chkSize.Width;
                                Size newVal = ScaleImage(iHeight, iWidth, 200, 200);
                                chkSize.Dispose();
                                preview_path = "..//Content//ProductImages//" + customerFolder + "//temp//ImageandAttFile//" + file_name + ".jpg";

                            }
                        }
                    }
                }
                else if (chkFile.Extension.ToUpper() == ".JPG" ||
                     chkFile.Extension.ToUpper() == ".GIF" ||
                     chkFile.Extension.ToUpper() == ".JPEG" ||
                     chkFile.Extension.ToUpper() == ".BMP" ||
                    chkFile.Extension.ToUpper() == ".PNG")
                {
                    chkSize = Image.FromFile(userImagePath + "/" + value.Trim());
                    int iHeight = chkSize.Height;
                    int iWidth = chkSize.Width;
                    var newVal = ScaleImage(iHeight, iWidth, 200, 200);
                    chkSize.Dispose();
                    preview_path = "..//Content//ProductImages" + value.Trim().Replace(@"\", @"/");
                }
                else if (chkFile.Extension.ToUpper() == ".PDF")
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\pdf_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images/Preview/") + "pdf_64.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "../Content/Images/pdf_64.BMP";
                }
                else if (chkFile.Extension.ToUpper() == ".RTF")
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\rtf_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images/Preview/") + "rtf_64.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "..//Content//Images//rtf_64.BMP";
                }
                else if (chkFile.Extension.ToUpper() == ".XLS" || chkFile.Extension.ToUpper() == ".XLSX")
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\xls_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/Images/Preview/") + "xls_64.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "..//Content//Images//xls_64.BMP";
                }
                else if (chkFile.Extension.ToUpper() == ".TIF")
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\tif_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images/Preview/") + "tiff_64.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "..//Content//Images//tif_64.BMP";
                }
                else if (chkFile.Extension.ToUpper() == ".TIFF")
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\tiff_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images/Preview/") + "tiff_64.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "..//Content//Images//tiff_64.BMP";
                }
                else if (chkFile.Extension.ToUpper() == ".INDD")
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\indd_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images/Preview/") + "indd_128.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "..//Content//Images//indd_64.BMP";
                }
                else if (chkFile.Extension.ToUpper() == ".DOC")
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\doc_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images/Preview/") + "doc_64.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "..//Content//Images//doc_64.BMP";
                }

                else if (chkFile.Extension.ToUpper() == ".PSD")
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\psd_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images/Preview/") + "psd_64.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "..//Content//Images//psd_64.BMP";
                }
                else if (chkFile.Extension.ToUpper() == ".EPS")
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\eps_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images/Preview/") + "eps_64.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "..//Content//Images//eps_64.BMP";
                }


                else if (chkFile.Extension.ToUpper() == ".AVI" ||
                    chkFile.Extension.ToUpper() == ".WMV" ||
                    chkFile.Extension.ToUpper() == ".MPG" ||
                    chkFile.Extension.ToUpper() == ".SWF")
                {
                    preview_path = "..//Content//ProductImages//" + customerFolder + value.Trim().Replace(@"\", @"/");

                }
                else
                {
                    string imgPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images") + "\\uns_64.BMP";
                    var rscImage = new Bitmap(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images/Preview/") + "uns_64.png");
                    rscImage.Save(imgPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    preview_path = "~/Content/Images/uns_64.BMP";
                }
            }
            else
            {
                //imageUrl[imgSizeCount] = "..//Content//ProductImages//images//unsupportedImageformat.jpg";
                preview_path = "..//Content//ProductImages//" + customerFolder + value.Trim().Replace(@"\", @"/");
            }
            return preview_path;
        }

        #region Attribute Pack

        public string LoadAttributePack(int familyId, int catalogId, string categoryId, bool FamilyLevelAttributePack, string packType)
        {
            var objSuperTable = new SuperTable();
            string multipleTableValueToReplace = string.Empty;
            string groupnames = string.Empty;
            int failureCount = 0;
            bool familyLvelPreviewEnabled = FamilyLevelAttributePack;
            if (familyLvelPreviewEnabled)
            {
                int packId = 0;
                var dsMultipleTables = AttributePack(catalogId, familyId, packId, packType);
                if (dsMultipleTables.Rows.Count > 0)
                {
                    var view = new DataView(dsMultipleTables);
                    string[] columns = { "GROUP_ID", "GROUP_NAME" };
                    DataTable distinctValues = view.ToTable(true, columns);
                    multipleTableValueToReplace = "<div style=\"background-color:white;border-radius:9px; max-width: 100%; overflow: auto;\"><br/><h4><b>Attribute Group</h4>";
                    for (int k = 0; k < distinctValues.Rows.Count; k++)
                    {
                        int pack_Id = Convert.ToInt32(distinctValues.Rows[k]["GROUP_ID"]);
                        groupnames += "<div style=\"background-color:white;border-radius:9px; max-width: 100%; overflow: auto;\"><br/><h5 style=\"background-color:#337ab7;color:white;border-top-right-radius: 9px;border-top-left-radius: 9px;Padding:10px;\">" + distinctValues.Rows[k]["GROUP_NAME"] + ":</h5>";
                        //  string html = GenerateAttributePackHtml(catalogId, familyId, packId, packType);
                        //  groupnames = groupnames + html + "</div>";
                        var attributePacks = (from apTableStructure in _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE
                                              join fTableStructure in _dbcontext.TB_FAMILY_TABLE_STRUCTURE on apTableStructure.FAMILY_STRUCTURE_ID equals fTableStructure.ID
                                              where fTableStructure.FAMILY_ID == familyId && fTableStructure.IS_DEFAULT == true && apTableStructure.PACK_ID == pack_Id
                                              select new
                                              {
                                                  apTableStructure.PACK_ID,
                                                  apTableStructure.TABLE_STRUCTURE,
                                                  apTableStructure.STRUCTURE_NAME,
                                                  apTableStructure.FAMILY_STRUCTURE_ID,
                                                  apTableStructure.IS_DEFAULT,
                                                  apTableStructure.ID,
                                              }).FirstOrDefault();
                        if (attributePacks != null)
                        {
                            string layoutName = attributePacks.STRUCTURE_NAME + ',' + attributePacks.FAMILY_STRUCTURE_ID;

                            string tableLayout = FetchTableLayout_AttributePack(pack_Id, layoutName);

                            groupnames = groupnames + objSuperTable.GenerateMultipleTableHtml(0, tableLayout, familyId, catalogId, categoryId, ImageMagickPath, UserName, pack_Id, layoutName) + "</div>";
                        }
                        else
                        {
                            groupnames = "";
                            failureCount = failureCount + 1;
                            if (distinctValues.Rows.Count == failureCount)
                                multipleTableValueToReplace = "";
                        }
                    }
                    multipleTableValueToReplace += groupnames;

                }
            }

            multipleTableValueToReplace = multipleTableValueToReplace.Replace("&lt;br /&gt;", "<br>").Replace("&lt;/br&gt;", "<br>").Replace("&lt;br&gt;", "<br>")
                .Replace("&lt;br/&gt;", "<br>").Replace("&amp;lt;br/&amp;gt;", "<br>").Replace("&amp;nbsp;", "&nbsp;");
            return multipleTableValueToReplace.Replace("&lt;p&gt;", "<p>").Replace("&lt;/p&gt;", "</p>").Replace("&lt;p/&gt;", "</p>");
        }


        private DataTable AttributePack(int catalogId, int familyId, int packId, string packType)
        {
            var familyspecstable = new DataTable();
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                var objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_LS_ATTRIBUTE_PACK_FAMILY_PREVIEW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                objSqlCommand.Parameters.Add("@PACK_ID", SqlDbType.Int).Value = packId;
                objSqlCommand.Parameters.Add("@PACK_TYPE", SqlDbType.NVarChar).Value = packType;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(familyspecstable);
            }
            return familyspecstable;
        }


        public string FetchTableLayout_AttributePack(int packId, string tableLayoutName)
        {
            string layoutXml = "HORIZONTAL TABLE";
            string layoutType = string.Empty;
            var dSlayout = new DataSet();

            string[] values = new string[3];
            if (tableLayoutName != null)
                values = tableLayoutName.Split(',');
            int tableId = 0;
            string layoutName = string.Empty;
            if (values != null)
            {
                layoutName = Convert.ToString(values[0]);
                tableId = Convert.ToInt32(values[1]);
            }

            layoutXml = _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE.Where(s => s.PACK_ID == packId && s.STRUCTURE_NAME == layoutName && s.FAMILY_STRUCTURE_ID == tableId).FirstOrDefault().TABLE_STRUCTURE;
            if (!string.IsNullOrEmpty(layoutXml))
            {
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(layoutXml);
                XmlNode rootNode = xmlDOc.DocumentElement;
                if (rootNode != null)
                {
                    layoutType = rootNode.Attributes["TableType"].Value;

                    if (rootNode.Attributes["TableType"].Value == "Grouped")
                    {
                        XmlNodeList xmlNodeList = rootNode.ChildNodes;

                        for (int i = 0; i < xmlNodeList.Count; i++)
                        {
                            if (xmlNodeList[i].Name == "LayoutXML")
                            {
                                if (xmlNodeList[i].ChildNodes.Count > 0)
                                {
                                    layoutXml = xmlNodeList[i].ChildNodes[0].Value;
                                }
                            }
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(layoutXml))
            {
                var fileHtml = new FileStream(HttpContext.Current.Server.MapPath("~/Content/") + "\\Layout.xml", FileMode.Create);
                var strwriter = new StreamWriter(fileHtml);
                strwriter.Write(layoutXml);
                strwriter.Close();
                fileHtml.Close();
            }



            if (layoutType.Length == 0)
                layoutType = "Horizontal";
            return layoutType;
        }

        #endregion

    }
}
