﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public partial class STP_LS_GET_USER
    {
         
        public int FUNCTION_ID { get; set; }
        public string FUNCTION_NAME { get; set; }
        public int ROLE_ID { get; set; }
        public bool ACTION_VIEW { get; set; }
        public bool ACTION_MODIFY { get; set; }
        public bool ACTION_ADD { get; set; }
        public bool ACTION_REMOVE { get; set; }
    }
}
