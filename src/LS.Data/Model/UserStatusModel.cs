﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class UserStatusModel
    {

        public string TB_USER_ID { get; set; }

        public string ROLE_ID { get; set; }

        public string ROLE_NAME { get; set; }

        public string STATUS { get; set; }

    }

    public class CustomerUsers
    {

        public int CustomerId { get; set; }

        public int NO_OF_USERS { get; set; }

    }

    public class userRoleModel
    {
        public int FUNCTION_ID { get; set; }
        public string FUNCTION_NAME { get; set; }
        public int ROLE_ID { get; set; }
        public bool ACTION_VIEW { get; set; }
        public bool ACTION_MODIFY { get; set; }
        public bool ACTION_ADD { get; set; }
        public bool ACTION_REMOVE { get; set; }
        public bool ACTION_DETACH { get; set; }
    }
}
