﻿

using System;

namespace LS.Data.Model
{
    public partial class LS_TB_CATEGORY
    {
        public LS_TB_CATEGORY()
        {
            //this.TB_CATALOG_FAMILY = new HashSet<TB_CATALOG_FAMILY>();
            //this.TB_CATALOG_SECTIONS = new HashSet<TB_CATALOG_SECTIONS>();
            //this.TB_CATEGORY_FAMILY_ATTR_LIST = new HashSet<TB_CATEGORY_FAMILY_ATTR_LIST>();
            //this.TB_PROJECT_SECTION_DETAILS = new HashSet<TB_PROJECT_SECTION_DETAILS>();
            //this.TB_FAMILY = new HashSet<TB_FAMILY>();
            //this.TB_PRODUCT = new HashSet<TB_PRODUCT>();
        }

        public string CATEGORY_ID { get; set; }
        public string CATEGORY_NAME { get; set; }
        public string PARENT_CATEGORY { get; set; }
        public string SHORT_DESC { get; set; }
        public string IMAGE_FILE { get; set; }
        public string IMAGE_TYPE { get; set; }
        public string IMAGE_NAME { get; set; }
        public string IMAGE_NAME2 { get; set; }
        public string IMAGE_FILE2 { get; set; }
        public string IMAGE_TYPE2 { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD1 { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD2 { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD3 { get; set; }
        public string CUSTOM_TEXT_FIELD1 { get; set; }
        public string CUSTOM_TEXT_FIELD2 { get; set; }
        public string CUSTOM_TEXT_FIELD3 { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
        public bool PUBLISH { get; set; }
        public bool PUBLISH2PRINT { get; set; }
        public bool PUBLISH2CD { get; set; }
        public int WORKFLOW_STATUS { get; set; }
        public string WORKFLOW_COMMENTS { get; set; }
        public bool CLONE_LOCK { get; set; }
        public Nullable<int> IS_CLONE { get; set; }

        //public virtual ICollection<TB_CATALOG_FAMILY> TB_CATALOG_FAMILY { get; set; }
        //public virtual ICollection<TB_CATALOG_SECTIONS> TB_CATALOG_SECTIONS { get; set; }
        //public virtual ICollection<TB_CATEGORY_FAMILY_ATTR_LIST> TB_CATEGORY_FAMILY_ATTR_LIST { get; set; }
        //public virtual ICollection<TB_PROJECT_SECTION_DETAILS> TB_PROJECT_SECTION_DETAILS { get; set; }
        //public virtual ICollection<TB_FAMILY> TB_FAMILY { get; set; }
        //public virtual ICollection<TB_PRODUCT> TB_PRODUCT { get; set; }
    }
}
