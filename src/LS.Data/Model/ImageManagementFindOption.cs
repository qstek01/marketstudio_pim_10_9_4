﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class ImageManagementFindOption
    {
        public string ATTRIBUTE_NAME { get; set; }
        public int PRODUCT_ID { get; set; }
        public string TYPE { get; set; }
        public string STRING_VALUE { get; set; }
    }

}
