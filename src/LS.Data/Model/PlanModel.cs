﻿using System;
using System.Collections.Generic;

namespace LS.Data.Model
{
    public class PlanModel
    {
        public int PLAN_ID { get; set; }

        public string PLAN_NAME { get; set; }

        public int SKU_COUNT { get; set; }

        public int NO_OF_USERS { get; set; }

        public int SUBSCRIPTION_IN_MONTHS { get; set; }

        public bool IS_ACTIVE { get; set; }

        public string CREATED_USER { get; set; }

        public DateTime CREATED_DATE { get; set; }

        public string MODIFIED_USER { get; set; }

        public DateTime MODIFIED_DATE { get; set; }

        public decimal Price_In_Dollars { get; set; }

        public decimal StorageGB { get; set; }

        public int CustomPDFCatalogTemplates { get; set; }

        public List<FunctionModel> FunctionModels { get; set; }

        public List<FunctionGroupModel> FunctionGroupModel { get; set; }

        public static PlanModel GetModel(TB_PLAN plan)
        {
            try
            {
                return new PlanModel
                {
                    PLAN_ID = plan.PLAN_ID,
                    PLAN_NAME = plan.PLAN_NAME,
                    SKU_COUNT = plan.SKU_COUNT,
                    NO_OF_USERS = plan.NO_OF_USERS,
                    SUBSCRIPTION_IN_MONTHS = plan.SUBSCRIPTION_IN_MONTHS,
                    IS_ACTIVE = plan.IS_ACTIVE,
                    CREATED_DATE = plan.CREATED_DATE,
                    CREATED_USER = plan.CREATED_USER,
                    MODIFIED_USER = plan.MODIFIED_USER,
                    MODIFIED_DATE = plan.MODIFIED_DATE,
                    Price_In_Dollars = plan.Price_In_Dollars,
                    StorageGB = plan.StorageGB,
                    CustomPDFCatalogTemplates = plan.CustomPDFCatalogTemplates
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class FunctionGroupModel
    {

        public int FUNCTION_GROUP_ID { get; set; }
        public int FUNCTION_GROUP_NAME { get; set; }
        public int SORT_ORDER { get; set; }
        public bool DEFAULT_ACTION_ALLOW { get; set; }
        
    }

}
