﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model.Search
{
   public class CategorySearch
    {

        public int CATALOG_ID { get; set; }
        public string CATALOG_NAME { get; set; }
        public string CATEGORY_ID { get; set; }
        public string CATEGORY_NAME { get; set; }
        public string PARENT_CATEGORY { get; set; }
        public string SHORT_DESC { get; set; }
        public string IMAGE_FILE { get; set; }
        public string IMAGE_TYPE { get; set; }
        public string IMAGE_NAME { get; set; }
        public string IMAGE_NAME2 { get; set; }
        public string IMAGE_FILE2 { get; set; }
        public string IMAGE_TYPE2 { get; set; }
        public string CUSTOM_NUM_FIELD1 { get; set; }
        public string CUSTOM_NUM_FIELD2 { get; set; }
        public string CUSTOM_NUM_FIELD3 { get; set; }
        public string CUSTOM_TEXT_FIELD1 { get; set; }
        public string CUSTOM_TEXT_FIELD2 { get; set; }
        public string CUSTOM_TEXT_FIELD3 { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
        public bool PUBLISH { get; set; }
        public bool PUBLISH2PRINT { get; set; }
        public bool PUBLISH2CD { get; set; }
        public int WORKFLOW_STATUS { get; set; }
        public string WORKFLOW_COMMENTS { get; set; }
        public bool CLONE_LOCK { get; set; }
        public Nullable<int> IS_CLONE { get; set; }
    }
}
