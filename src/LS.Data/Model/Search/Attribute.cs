﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LS.Data.Model
{
    public class Attribute
    {

    }

    public class arrayAttrs
    {
        public int[] newselectedattr { get; set; }
        public int[] removedattr { get; set; }
    }
    public class AttributePublish
    {
        public int CATALOG_ID { get; set; }
        public int FAMILY_ID { get; set; }
        public int PRODUCT_ID { get; set; }
        public int SUBPRODUCT_ID { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public int ATTRIBUTE_TYPE { get; set; }
        public bool ISAvailable { get; set; }
        public bool ISPublish { get; set; }
        public int SORT_ORDER { get; set; }
        public string FLAG { get; set; }
    }
    public class CategoryAttributePublish
    {
        public int CATALOG_ID { get; set; }
        public string CATEGORY_ID { get; set; }
        public int PRODUCT_ID { get; set; }
        public int SUBPRODUCT_ID { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public int ATTRIBUTE_TYPE { get; set; }
        public bool ISAvailable { get; set; }
        public int SORT_ORDER { get; set; }
        public string FLAG { get; set; }
    }

    public class AttributePack
    {
        public int CATALOG_ID { get; set; }
        public int GROUP_ID { get; set; }
        public string GROUP_NAME { get; set; }
        public int PACK_TYPE { get; set; }
        public bool ISAvailable { get; set; }
        public int SORT_ORDER { get; set; }
    }

    public class AssignOption
    {
        public string FILENAME { get; set; }
        public string FOLDERNAME { get; set; }
        public string EXTENSION { get; set; }
        public string PATH { get; set; }
        public string FULLPATH { get; set; }
        public string PREVIEW_PATH { get; set; }
    }

    public class SelectedAttribute
    {
        public string CatalogField { get; set; }
        public string ExcelColumn { get; set; }
        public string FieldType { get; set; }
        public bool IsSystemField { get; set; }
        public bool SelectedToImport { get; set; }
    }
    public class ValidationList
    {
        public bool column { get; set; }
        public bool id { get; set; }
        public bool pickList { get; set; }
        public bool dataType { get; set; }
        public bool subCatalog { get; set; }
    }
    public class NewDataSet
    {
        public string ATTRIBUTE_NAME { get; set; }
        public string CAPTION { get; set; }
        public byte ATTRIBUTE_TYPE { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public bool CREATE_BY_DEFAULT { get; set; }
        public bool VALUE_REQUIRED { get; set; }
        public string STYLE_NAME { get; set; }
        public string STYLE_FORMAT { get; set; }
        public string DEFAULT_VALUE { get; set; }
        public bool PUBLISH2PRINT { get; set; }
        public bool PUBLISH2WEB { get; set; }
        public bool PUBLISH2CDROM { get; set; }
        public bool PUBLISH2ODP { get; set; }
        public bool USE_PICKLIST { get; set; }
        public string ATTRIBUTE_DATATYPE { get; set; }
        public string ATTRIBUTE_DATAFORMAT { get; set; }
        public string UOM { get; set; }
        public bool IS_CALCULATED { get; set; }
        public string ATTRIBUTE_CALC_FORMULA { get; set; }
        public string PICKLIST_NAME { get; set; }
        public string CREATED_USER { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public DateTime MODIFIED_DATE { get; set; }
        public string ATTRIBUTE_DATARULE { get; set; }
        public string NUMERIC { get; set; }
        public string DECIMAL { get; set; }
        public bool ALLOWEDIT { get; set; }
        public bool PUBLISH2PDF { get; set; }
        public bool PUBLISH2EXPORT { get; set; }
        public bool PUBLISH2PORTAL { get; set; }
        [XmlElement("DataRule")]
        public List<DataRule> dataRuleList = new List<DataRule>();
    }

    public class productfilterxml
    {
        public string Attribute_id { get; set; }
        public string Attribute { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
        public bool Condition { get; set; }
     
    }
    public class DataRule
    {
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string Condition { get; set; }
        public string CustomValue { get; set; }
        public string ApplyTo { get; set; }
        public string ApplyForNumericOnly { get; set; }
    }

    public class NewAttribute
    {
        public string ATTRIBUTE_NAME { get; set; }
        public string CAPTION { get; set; }
        public byte ATTRIBUTE_TYPE { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public bool CREATE_BY_DEFAULT { get; set; }
        public bool VALUE_REQUIRED { get; set; }
        public string STYLE_NAME { get; set; }
        public string STYLE_FORMAT { get; set; }
        public string DEFAULT_VALUE { get; set; }
        public bool PUBLISH2PRINT { get; set; }
        public bool PUBLISH2WEB { get; set; }
        public bool USE_PICKLIST { get; set; }
        public string ATTRIBUTE_DATATYPE { get; set; }
        public string ATTRIBUTE_DATAFORMAT { get; set; }
        public string UOM { get; set; }
        public bool IS_CALCULATED { get; set; }
        public string ATTRIBUTE_CALC_FORMULA { get; set; }
        public string PICKLIST_NAME { get; set; }
        public string CREATED_USER { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public DateTime MODIFIED_DATE { get; set; }
        public string ATTRIBUTE_DATARULE { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string Condition { get; set; }
        public string CustomValue { get; set; }
        public string ApplyTo { get; set; }
        public string ApplyForNumericOnly { get; set; }
        public int ATTRIBUTE_SIZE { get; set; }
        public int ATTRIBUTE_DECIMAL { get; set; }
        public bool PUBLISH2PDF { get; set; }
        public bool PUBLISH2EXPORT { get; set; }
        public bool PUBLISH2PORTAL { get; set; }
    }
    public class AttributeList
    {
        public int ATTRIBUTE_ID { get; set; }
       
    }
    public class NewFilters
    {
        public string FilterName { get; set; }
        public int FilterId { get; set; }
    }

    public class IndesignFilters
    {
        public string FilterName { get; set; }
        public int FilterId { get; set; }

    }

    public class productAttributeFiltername
    {
        public string productAttributeProductFilterName { get; set; }
    }
    public class productAttributeTypename
    {
        public string productAttributeTypeFilterName { get; set; }

    }

    public class EXPORTBATCHALLOWEDITEM
    {
        public string ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public string DISPLAY_NAME { get; set; }
        public string ATTRIBUTE_TYPE { get; set; }
        public bool PUBLISH2CDROM { get; set; }
        public bool PUBLISH2ODP { get; set; }
        public bool PUBLISH2PRINT { get; set; }
        public bool PUBLISH2WEB { get; set; }
        public bool ISAvailable { get; set; }
    }
    public class EXPORTBATCHDISPLAYATTRIBUTES
    {
        public string ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public string DISPLAY_NAME { get; set; }
        public string ATTRIBUTE_TYPE { get; set; }
        public bool PUBLISH2CDROM { get; set; }
        public bool PUBLISH2ODP { get; set; }
        public bool PUBLISH2PRINT { get; set; }
        public bool PUBLISH2WEB { get; set; }
        public bool ISAvailable { get; set; }
    }
}