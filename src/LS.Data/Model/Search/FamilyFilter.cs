﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
namespace LS.Data.Model
{
    public class FamilyFilter
    {
    }
    public class CatalogFamilyOption
    {
        public int CATALOG_ID { get; set; }
        public string CATALOG_NAME { get; set; }
        public string VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string FAMILY_FILTERS { get; set; }
        public string PRODUCT_FILTERS { get; set; }
        public string CREATED_USER { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public DateTime MODIFIED_DATE { get; set; }
        public int DEFAULT_FAMILY { get; set; }
        [XmlElement("Family_Filters")]
        public List<FamilyFilters> familyfilter = new List<FamilyFilters>();
        [XmlElement("Product_Filters")]
        public List<ProductFilter> productfilter = new List<ProductFilter>();
        
    }
    public class FamilyFilters
    {
        public string Attribute_id { get; set; }
        public string Attribute { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
        public string Condition { get; set; }
    }
    public class ProductFilter
    {
        public string Attribute_id { get; set; }
        public string Attribute { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
        public string Condition { get; set; }

    }
}