﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model.Search
{
    public class FamilySearch
    {
        public int CATALOG_ID { get; set; }
        public string CATALOG_NAME { get; set; }
        public string CATEGORY_ID { get; set; }
        public string CATEGORY_NAME { get; set; }
        public int FAMILY_ID { get; set; }
        public string FAMILY_NAME { get; set; }
        public string STATUS { get; set; }
        public Nullable<int> ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public string STRING_VALUE { get; set; }
        public Nullable<int> ATTRIBUTE_TYPE { get; set; }
        public Nullable<decimal> NUMERIC_VALUE { get; set; }
        public string OBJECT_NAME { get; set; }
        public string OBJECT_TYPE { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
    }
}
