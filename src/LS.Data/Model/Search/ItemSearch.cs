﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LS.Data.Model
{

    public class ItemSearch
    {

        public int CATALOG_ID { get; set; }
        public string CATALOG_NAME { get; set; }
        public string CATEGORY_ID { get; set; }
        public string SUBCATNAME_L1 { get; set; }
        public string SUBCATNAME_L2 { get; set; }
        public string SUBCATNAME_L3 { get; set; }
        public string CATEGORY_NAME { get; set; }
        public int FAMILY_ID { get; set; }
        public string FAMILY_NAME { get; set; }
        public string SUBFAMILY_NAME { get; set; }
        public int PRODUCT_ID { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public string STRING_VALUE { get; set; }
        public byte ATTRIBUTE_TYPE { get; set; }
        public Nullable<decimal> NUMERIC_VALUE { get; set; }
        public string OBJECT_TYPE { get; set; }
        public string OBJECT_NAME { get; set; }
        public string CREATED_USER { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public DateTime MODIFIED_DATE { get; set; }
        public string ITEM_NO { get; set; }
    }

    public class SubItemSearch
    {

        public int CATALOG_ID { get; set; }
        public string CATALOG_NAME { get; set; }
        public string CATEGORY_ID { get; set; }
        public string SUBCATNAME_L1 { get; set; }
        public string SUBCATNAME_L2 { get; set; }
        public string SUBCATNAME_L3 { get; set; }
        public string CATEGORY_NAME { get; set; }
        public int FAMILY_ID { get; set; }
        public string FAMILY_NAME { get; set; }
        public string SUBFAMILY_NAME { get; set; }
        public int MAINPRODUCT_ID { get; set; }
        public string CATALOG_ITEM_NO { get; set; }
        public int SUBPRODUCT_ID { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public string STRING_VALUE { get; set; }
        public byte ATTRIBUTE_TYPE { get; set; }
        public Nullable<decimal> NUMERIC_VALUE { get; set; }
        public string OBJECT_TYPE { get; set; }
        public string OBJECT_NAME { get; set; }
        public string CREATED_USER { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public DateTime MODIFIED_DATE { get; set; }
        public string ITEM_NO { get; set; }
    }
}