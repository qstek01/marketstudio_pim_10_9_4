﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class UserRolesList
    {
        private string _functionName;
        private int _roleId;
        private bool _actionView;
        private bool _actionModify;
        private bool _actionAdd;
        private bool _actionRemove;
        private string _userId;

        public string User_ID
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public string Function_Name
        {
            get { return _functionName; }
            set { _functionName = value; }
        }

        public int Role_Id
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        public bool Action_View
        {
            get { return _actionView; }
            set { _actionView = value; }
        }

        public bool Action_Modify
        {
            get { return _actionModify; }
            set { _actionModify = value; }
        }

        public bool Action_Add
        {
            get { return _actionAdd; }
            set { _actionAdd = value; }
        }
        public bool Action_Remove
        {
            get { return _actionRemove; }
            set { _actionRemove = value; }
        }
    }
}
