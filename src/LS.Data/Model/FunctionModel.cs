﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace LS.Data.Model
{
    public class FunctionModel
    {
        static ILog _logger = null;
        public FunctionModel()
        {
            _logger = LogManager.GetLogger(typeof(FunctionModel));
        }
        public int FUNCTION_ID { get; set; }
        public int ROLE_ID { get; set; }
        public int PLAN_ID { get; set; }
        public string FUNCTION_NAME { get; set; }
        public bool ACTION_VIEW { get; set; }
        public bool ACTION_MODIFY { get; set; }
        public bool ACTION_ADD { get; set; }
        public bool ACTION_REMOVE { get; set; }
        public bool ACTION_DETACH { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
        public bool DEFAULT_ACTION_ALLOW { get; set; }
        public int FUNCTION_GROUP_ID { get; set; }

        public static FunctionModel GetModel(TB_FUNCTION function)
        {
            try
            {
                //_logger.Info("Inside  at  FunctionModel: GetModel");
                var model = new FunctionModel
                {
                   // PLAN_ID = function.
                    FUNCTION_ID = function.FUNCTION_ID,
                    ROLE_ID = function.FUNCTION_ID,
                    FUNCTION_NAME = function.FUNCTION_NAME,
                    ACTION_ADD = function.DEFAULT_ACTION_ADD,
                    ACTION_MODIFY = function.DEFAULT_ACTION_MODIFY,
                    ACTION_REMOVE = function.DEFAULT_ACTION_REMOVE,
                    ACTION_VIEW = function.DEFAULT_ACTION_VIEW,
                    CREATED_USER = function.CREATED_USER,
                    CREATED_DATE = function.CREATED_DATE,
                    MODIFIED_USER = function.MODIFIED_USER,
                    MODIFIED_DATE = function.MODIFIED_DATE,
                };
                return model;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CustomerModel: GetModel", ex);
                return null;
            }
        }
        public static FunctionModel GetModelForEmptyPlan(TB_FUNCTION function)
        {
            try
            {
                //_logger.Info("Inside  at  FunctionModel: GetModel");
                var model = new FunctionModel
                {
                    FUNCTION_ID = function.FUNCTION_ID,
                    FUNCTION_NAME = function.FUNCTION_NAME,
                    ACTION_ADD = function.DEFAULT_ACTION_ADD,
                    ACTION_MODIFY = function.DEFAULT_ACTION_MODIFY,
                    ACTION_REMOVE = function.DEFAULT_ACTION_REMOVE,
                    ACTION_VIEW = function.DEFAULT_ACTION_VIEW,
                };
                return model;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CustomerModel: GetModel", ex);
                return null;
            }
        }
    }
}
