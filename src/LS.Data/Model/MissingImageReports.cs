﻿namespace LS.Data.Model
{
    public class MissingImageReports
    {
        public string MissingImageName { get; set; }
        public string ImagePath { get; set; }

    }

}