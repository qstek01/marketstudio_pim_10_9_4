﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Model
{
    public class StateModel
    {
        public string STATE_CODE { get; set; }
        public string STATE { get; set; }
        public string COUNTRY_CODE { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }

        public static StateModel GetModel(TB_STATE state)
        {
            try
            {
                return new StateModel
                {
                    COUNTRY_CODE = state.COUNTRY_CODE,
                    CREATED_DATE = state.CREATED_DATE,
                    CREATED_USER = state.CREATED_USER,
                    MODIFIED_DATE = state.MODIFIED_DATE,
                    MODIFIED_USER = state.MODIFIED_USER,
                    STATE = state.STATE,
                    STATE_CODE = state.STATE_CODE
                };
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
              
    }
}
