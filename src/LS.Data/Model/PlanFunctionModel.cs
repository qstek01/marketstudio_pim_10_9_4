﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace LS.Data.Model
{
    public class PlanFunctionModel
    {
        static ILog _logger = null;
        public PlanFunctionModel()
        {
            _logger = LogManager.GetLogger(typeof(PlanFunctionModel));
        }
        public int FUNCTION_ID { get; set; }
        public int PLAN_ID { get; set; }
        public int ROLE_ID { get; set; }
        public string FUNCTION_NAME { get; set; }
        public bool ACTION_VIEW { get; set; }
        public bool ACTION_MODIFY { get; set; }
        public bool ACTION_ADD { get; set; }
        public bool ACTION_REMOVE { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
        public int FUNC_ID { get; set; }
        public int FUNCTION_GROUP_ID { get; set; }

        public static PlanFunctionModel GetModel(TB_PLAN_FUNCTIONS function)
        {
            try
            {
                //_logger.Info("Inside  at  PlanFunctionModel: GetModel");
                var model = new PlanFunctionModel
                {
                    FUNCTION_ID = function.FUNCTION_ID,
                    FUNCTION_NAME = function.FUNCTION_NAME,
                    PLAN_ID = function.PLAN_ID,
                    ROLE_ID = function.PLAN_ID,
                    ACTION_ADD = function.ACTION_ADD,
                    ACTION_MODIFY = function.ACTION_MODIFY,
                    ACTION_REMOVE = function.ACTION_REMOVE,
                    ACTION_VIEW = function.ACTION_VIEW,
                    CREATED_USER = function.CREATED_USER,
                    CREATED_DATE = function.CREATED_DATE,
                    MODIFIED_USER = function.MODIFIED_USER,
                    MODIFIED_DATE = function.MODIFIED_DATE,
                    FUNC_ID = function.FUNCTION_ID,
                   // FUNCTION_GROUP_ID = function.FUNCTION_GROUP_ID,
                };
                return model;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at PlanFunctionModel: GetModel", ex);
                return null;
            }
        }
    }
}
