//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LS.Data
{
    using System;
    
    public partial class STP_LS_GET_USER_READ_ONLY_ATTRIBUTE_LIST_Result
    {
        public string ATTRIBUTE_NAME { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public int ROLE_ID { get; set; }
    }
}
