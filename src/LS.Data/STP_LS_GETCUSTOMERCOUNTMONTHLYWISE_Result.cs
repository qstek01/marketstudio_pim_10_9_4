//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LS.Data
{
    using System;
    
    public partial class STP_LS_GETCUSTOMERCOUNTMONTHLYWISE_Result
    {
        public Nullable<int> ActivationYear { get; set; }
        public string ActivationMonth { get; set; }
        public int ActivationMonthInNumbers { get; set; }
        public Nullable<int> CustomerCount { get; set; }
    }
}
