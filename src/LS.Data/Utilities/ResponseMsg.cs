﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data.Utilities
{
    //public enum MessageType
    //{
    //    Custom,
    //    Error,
    //    Inserted,
    //    Updated,
    //    Deleted
    //}
    public static class ResponseMsg
    {

        public static string Error = "Please reload the page and continue.";
        public static string Duplicate = "Name already exists. Please enter a different name for the Group.";
        public static string Inserted = "saved successfully.";
        public static string Updated = "Update completed.";
        public static string Deleted = "Delete successfully.";
        public static string NotFound = "Not Found.";
        public static string Categories = "Selected Catalog have some categories. Please delete all and then try again.";
        public static string FamilyAssociationRemove = "Product association removed successfully.";
        public static string ProductAssociationRemove = "Item association removed successfully.";
        //public static string Get(MessageType type)
        //{
        //    if(type == MessageType.Inserted)
        //    {
        //        return string.Format(InsertSuccessfully,string.Empty);
        //    }
        //    else if (type == MessageType.Updated)
        //    {
        //        return string.Format(UpdatedSuccessfully,string.Empty);
        //    }
        //    else if (type == MessageType.Deleted)
        //    {
        //        return string.Format(DeletedSuccessfully,string.Empty);
        //    }
        //    else if (type == MessageType.Error)
        //    {
        //        return string.Format(Error, string.Empty); 
        //    }
        //    else
        //    {
        //        return string.Format(Custom, string.Empty); 
        //    }
        //}

        //public static string Get(MessageType type,string message)
        //{
        //    if (type == MessageType.Inserted)
        //    {
        //        return string.Format(InsertSuccessfully, message);
        //    }
        //    else if (type == MessageType.Updated)
        //    {
        //        return string.Format(UpdatedSuccessfully, message);
        //    }
        //    else if (type == MessageType.Updated)
        //    {
        //        return string.Format(DeletedSuccessfully, message);
        //    }
        //    else if (type == MessageType.Error)
        //    {
        //        return string.Format(Error, string.Empty);
        //    }
        //    else
        //    {
        //        return string.Format(Custom, message);
        //    }
        //}
    }
}
