//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_CUSTOMER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TB_CUSTOMER()
        {
            this.TB_CAMPAIGN_RUN = new HashSet<TB_CAMPAIGN_RUN>();
        }
    
        public int CUSTOMER_ID { get; set; }
        public string CUSTOMER_FNAME { get; set; }
        public string CUSTOMER_MNAME { get; set; }
        public string CUSTOMER_LNAME { get; set; }
        public string CUSTOMER_PREFIX { get; set; }
        public string CUSTOMER_SUFFIX { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public bool CUSTOMER_DONT_CALL { get; set; }
        public Nullable<System.DateTime> CUSTOMER_DONT_CALL_FLAG_DATE { get; set; }
        public string OCCUPATION { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD_1 { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD_2 { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD_3 { get; set; }
        public bool CUSTOMER_SUSPEND_CALLING { get; set; }
        public bool CUSTOMER_DONT_MAIL { get; set; }
        public Nullable<System.DateTime> CUSTOMER_DONT_MAIL_TILL_DATE { get; set; }
        public string CUSTOM_TEXT_FIELD_1 { get; set; }
        public string CUSTOM_TEXT_FIELD_2 { get; set; }
        public string CUSTOM_TEXT_FIELD_3 { get; set; }
        public string CUSTOM_TEXT_FIELD_4 { get; set; }
        public Nullable<System.DateTime> INCEPTION_DATE { get; set; }
        public string ADDR1 { get; set; }
        public string ADDR2 { get; set; }
        public string ADDR3 { get; set; }
        public string CITY { get; set; }
        public string STATE_CODE { get; set; }
        public string ZIP { get; set; }
        public string HOME_PHONE { get; set; }
        public string WORK_PHONE { get; set; }
        public string MOBILE_PHONE { get; set; }
        public string FAX { get; set; }
        public string EMAIL { get; set; }
        public string URL { get; set; }
        public string NOTES { get; set; }
        public string CALL_NUMBER { get; set; }
        public string COUNTRY_CODE { get; set; }
        public Nullable<int> TIME_ZONE_ID { get; set; }
        public string PROVINCE { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD_4 { get; set; }
        public Nullable<decimal> CUSTOM_NUM_FIELD_5 { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_CAMPAIGN_RUN> TB_CAMPAIGN_RUN { get; set; }
        public virtual TB_COUNTRY TB_COUNTRY { get; set; }
        public virtual TB_STATE TB_STATE { get; set; }
    }
}
