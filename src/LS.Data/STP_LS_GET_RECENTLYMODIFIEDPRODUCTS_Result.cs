//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LS.Data
{
    using System;
    
    public partial class STP_LS_GET_RECENTLYMODIFIEDPRODUCTS_Result
    {
        public int C1 { get; set; }
        public string C2 { get; set; }
        public string C3 { get; set; }
        public int PRODUCT_ID { get; set; }
        public string STRING_VALUE { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
    }
}
