﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;

namespace LS.Data
{
    public partial class ProfileView
    {
        public string RolesString
        {
            get
            {
                string rolesString = "";
                string[] currentRoles = Roles.GetRolesForUser(this.UserName);
                if (currentRoles.Length > 0)
                {
                    foreach (string role in currentRoles)
                    {
                        rolesString += "," + role;
                    }
                    return rolesString.Substring(1);
                }
                else
                {
                    return "";
                }
            }
        }
    }


    public partial class ConcreteTestDetails
    {

        public string TestDateText
        {
            get
            {
                return "";
            }
        }

    }
}
