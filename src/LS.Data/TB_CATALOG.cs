//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_CATALOG
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TB_CATALOG()
        {
            this.Customer_Catalog = new HashSet<Customer_Catalog>();
            this.TB_ATTRIBUTE_GROUP_SECTIONS = new HashSet<TB_ATTRIBUTE_GROUP_SECTIONS>();
            this.TB_FAMILY_KEY = new HashSet<TB_FAMILY_KEY>();
            this.TB_REFERENCE_SECTIONS = new HashSet<TB_REFERENCE_SECTIONS>();
            this.TB_PROJECT = new HashSet<TB_PROJECT>();
            this.TB_CATALOG_FAMILY = new HashSet<TB_CATALOG_FAMILY>();
            this.TB_CATALOG_PRODUCT = new HashSet<TB_CATALOG_PRODUCT>();
            this.TB_CATALOG_USER = new HashSet<TB_CATALOG_USER>();
            this.TB_SUBPRODUCT = new HashSet<TB_SUBPRODUCT>();
            this.TB_CATALOG_SECTIONS = new HashSet<TB_CATALOG_SECTIONS>();
            this.TB_CATALOG_ATTRIBUTES = new HashSet<TB_CATALOG_ATTRIBUTES>();
            this.TB_INVERTEDPRODUCTS_TEMPLATE = new HashSet<TB_INVERTEDPRODUCTS_TEMPLATE>();
        }
    
        public int CATALOG_ID { get; set; }
        public string CATALOG_NAME { get; set; }
        public string VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string FAMILY_FILTERS { get; set; }
        public string PRODUCT_FILTERS { get; set; }
        public Nullable<int> DEFAULT_FAMILY { get; set; }
        public string CREATED_USER { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public string MODIFIED_USER { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
        public string FLAG_RECYCLE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Customer_Catalog> Customer_Catalog { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_ATTRIBUTE_GROUP_SECTIONS> TB_ATTRIBUTE_GROUP_SECTIONS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_FAMILY_KEY> TB_FAMILY_KEY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_REFERENCE_SECTIONS> TB_REFERENCE_SECTIONS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_PROJECT> TB_PROJECT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_CATALOG_FAMILY> TB_CATALOG_FAMILY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_CATALOG_PRODUCT> TB_CATALOG_PRODUCT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_CATALOG_USER> TB_CATALOG_USER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_SUBPRODUCT> TB_SUBPRODUCT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_CATALOG_SECTIONS> TB_CATALOG_SECTIONS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_CATALOG_ATTRIBUTES> TB_CATALOG_ATTRIBUTES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TB_INVERTEDPRODUCTS_TEMPLATE> TB_INVERTEDPRODUCTS_TEMPLATE { get; set; }
    }
}
