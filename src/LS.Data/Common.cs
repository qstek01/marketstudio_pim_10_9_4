﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LS.Data
{
    public class ImageList
    {
        public string STRING_VALUE { get; set; }
        public string HIGH_RESOLUTION { get; set; }
        public string HIGH_RESOLUTION_PSD { get; set; }
        public int PRODUCT_ID { get; set; }
    }

    public class ShippingAddress
    {
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int ZipCode { get; set; }
        public DateTime DeliveryDateRequested { get; set; }
        public string OtherText { get; set; }
    }
    
    public class SearchFilter
    {
        public int ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_VALUE { get; set; }
    }
    public class LINK_DETAILS
    {
        public string CATEGORY_ID { get; set; }
        public int FAMILY_ID { get; set; }
        public int PRODUCT_ID { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public string OPTION { get; set; }
        public int USER_ID { get; set; }
        public int USER_GROUP { get; set; }
        public string IMAGE { get; set; }
        public string TYPE { get; set; }
        public string STRING_VALUE { get; set; }
        public string PRINT_CLASS { get; set; }
        public int CO_BRAND_FLAG { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
    }
    public class ATTRIBUTE_DETAILS
    {
        
        public int CATALOG_ID { get; set; }
        public int ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }
        public int ATTRIBUTE_TYPE { get; set; }
      
    }

    public class FEEDBACK_DETAILS
    {
        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public string EmailSubject { get; set; }
        public string EmailContent { get; set; }
    }
    public class FileDetails
    {
        public string PRINT_CLASS_NAME { get; set; }
        public string PDF_PREFIX { get; set; }
        public string PDF_NAME { get; set; }
        public string PDF_SUFFIX { get; set; }
        public string PDF_LOW { get; set; }
        public string PDF_HIGH { get; set; }
    }
    public class GROUP_EMAIL
    {
        public string EMAIL_TO { get; set; }
        public string EMAIL_SUBJECT { get; set; }
        public string EMAIL_BODY { get; set; }
        public string EMAIL_ATTACHMENTS { get; set; }
    }
    public class COLLECTION_LIST
    {
        public string COLLECTION_NAME { get; set; }
        public int COLLECTION_ID { get; set; }
        public Nullable <int>  SORT_ORDER { get; set; }
        
    }
}
