

ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO_FAMILYINSERTTEMPDATA](@TABLENAME NVARCHAR(500),@TEMPTABLE ImportTableBulk READONLY)
As 
BEGIN
--select * into ##TEMPTABLE1 from @TEMPTABLE
--return
 IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TEMPTABLE')  
   BEGIN  
    EXEC('drop table ##TEMPTABLE ')  
   END  
select [PRODUCT_ID],[CATALOG_ITEM_NO] ,[ATTRIBUTENAME],[ATTRIBUTEVALUE],[Row] into ##TEMPTABLE from @TEMPTABLE

--set @TABLENAME='[##IMPORTTEMP1a703192-9bc4-4749-b3c4-ebc2669177c8]'
DECLARE @COLUMNSPIV nVARCHAR(MAX)=''
SELECT @COLUMNSPIV =@COLUMNSPIV +'['+ col.name + '],'  from tempdb.sys.columns col join tempdb.sys.tables tab on col.object_id=tab.object_id and tab.name like ''+replace(replace(@TABLENAME,'[',''),']','')+'%' and col.name not in ('PRODUCT_ID','CATALOG_ITEM_NO')
 order by col.column_id
SET @COLUMNSPIV = LEFT(@COLUMNSPIV, LEN(@COLUMNSPIV) - 1)
print @COLUMNSPIV

DECLARE @COLUMNS nVARCHAR(MAX)=''
SELECT @COLUMNS =@COLUMNS +' case when ISNUMERIC(['+ col.name + '])=1 then ['+ col.name + '] else CONVERT(NVARCHAR(MAX), CONVERT(VARBINARY(MAX), CONVERT(IMAGE, CAST(['+ col.name + '] AS VARBINARY(MAX))),2)) end as ['+col.name+'],'  from tempdb.sys.columns col join tempdb.sys.tables tab on col.object_id=tab.object_id and tab.name like ''+replace(replace(@TABLENAME,'[',''),']','')+'%' --and col.name not in ('PRODUCT_ID','CATALOG_ITEM_NO')
 order by col.column_id
SET @COLUMNS = LEFT(@COLUMNS, LEN(@COLUMNS) - 1)
print @COLUMNS


DECLARE @INSERTCOLUMNS VARCHAR(MAX)=''
SELECT @INSERTCOLUMNS =@INSERTCOLUMNS +'['+ col.name + '],'  from tempdb.sys.columns col join tempdb.sys.tables tab on col.object_id=tab.object_id and tab.name like ''+replace(replace(@TABLENAME,'[',''),']','')+'%' --and col.name not in ('PRODUCT_ID','CATALOG_ITEM_NO')
 order by col.column_id
SET @INSERTCOLUMNS = LEFT(@INSERTCOLUMNS, LEN(@INSERTCOLUMNS) - 1)
print @INSERTCOLUMNS
 IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME=''+replace(replace(@TABLENAME,'[',''),']','')+'')  
   BEGIN  
    EXEC('drop table  '+@TABLENAME+'')  
   END  

exec('select '+@COLUMNS+' into '+@TABLENAME+' from (select distinct '+@COLUMNS+',[Row] from
(select distinct [PRODUCT_ID],[CATALOG_ITEM_NO] ,[ATTRIBUTENAME],[ATTRIBUTEVALUE],[Row] from ##TEMPTABLE )as temp1 
pivot
(
max(attributevalue) for ATTRIBUTENAME in ('+@COLUMNSPIV+')
)
 as tt) as temp where catalog_name is not null and catalog_name<>'''' order by [Row]')

 --exec('select * from '+@TABLENAME)
END
















