
ALTER FUNCTION [dbo].[ValidateURL] (@Url VARCHAR(200))
RETURNS INT
AS
BEGIN
    IF (CHARINDEX('www.', @url) <> 1 or (@url not like '%.com'))
    BEGIN
        RETURN 0;   -- Not a valid URL
    END

    -- Get rid of the http:// stuff
    SET @Url = REPLACE(@URL, 'www.', '');
	 SET @Url = REPLACE(@URL, '.com', '');

    -- Now we need to check that we only have digits or numbers
    IF (@Url LIKE '%[^a-zA-Z0-9]%') 
    BEGIN
        RETURN 0;
    END

    -- It is a valid URL
    RETURN 1;
END