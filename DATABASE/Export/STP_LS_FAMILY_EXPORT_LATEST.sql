

 ALTER PROCEDURE [dbo].[STP_LS_FAMILY_EXPORT_LATEST]
(
	@SESSID AS VARCHAR(500) = '6212d26e391c435183544d60211e4631',
	@CATALOG_ID VARCHAR(10) = '96',
	@ATTRIBUTE_IDS VARCHAR(MAX) = '1,3809,4247',
	@CATEGORY_ID VARCHAR(50) = 'CAT12395',
	@FAMILY_ID VARCHAR(50)= '24938'
)
AS
DECLARE @FLG INT=1
DECLARE @COLNAME VARCHAR(100)='SUBCATID_L' 
DECLARE @FTABNAME VARCHAR(100)='##TEMP1'
DECLARE @ITABNAME VARCHAR(100)='##TEMP2'
DECLARE @AttributeValue int =1;

Declare @AttributeIteName varchar(100) = (select  top 1 ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_ID = 1)

DECLARE @CNT INT=1
DECLARE @CHK INT=0
DECLARE @QUERY nvarchar(MAX);
DECLARE @ParmDefinition nvarchar(500);
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

update TB_ATTRIBUTE set FLAG_RECYCLE='A' where FLAG_RECYCLE is null
IF(@CATEGORY_ID = '0')
 SET @CATEGORY_ID = 'All'

IF(@ATTRIBUTE_IDS IS NULL OR @ATTRIBUTE_IDS = '')
	BEGIN
		SET @ATTRIBUTE_IDS = '0';
	END
DECLARE @FTABMAIN VARCHAR(100)='##TEMP1'
SET @FTABMAIN = '##TEMPTABLEMAIN'+@SESSID;

DECLARE @FTABMAINFAM VARCHAR(100)='##TEMP1'
SET @FTABMAINFAM = '##TEMPTABLEMAIN'+@SESSID+'FAM';

IF OBJECT_ID('tempdb..##TEMP1') IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMP1')
	END
	IF OBJECT_ID('tempdb..##TEMPTABLEMAIN'+@SESSID+'FAM') IS NOT NULL
	BEGIN
		EXEC('DROP TABLE [##TEMPTABLEMAIN'+@SESSID+'FAM]')
	END
IF OBJECT_ID('tempdb..##TEMPRESULTSUBPRODUCTS'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMPRESULTSUBPRODUCTS'+@SESSID)
	END
IF OBJECT_ID('tempdb..##TEMPTABLEMAIN'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMPTABLEMAIN'+@SESSID)
		SET @FTABMAIN = '##TEMPTABLEMAIN'+@SESSID;
	END
IF OBJECT_ID('tempdb..##TEMPTABLESUB'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMPTABLESUB'+@SESSID)
	END
IF OBJECT_ID('tempdb..##TEMP1'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMP1'+@SESSID)
	END
IF OBJECT_ID('tempdb..##TEMP2'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMP2'+@SESSID)
	END
IF(@CATEGORY_ID = 'All')
	BEGIN
	PRINT'ALL'
			EXEC ('SELECT DISTINCT A.CATEGORY_SHORT,A.CATEGORY_ID,A.CATEGORY_NAME INTO ##TEMP1'+ @SESSID +' FROM TB_CATEGORY A JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID AND A.PUBLISH2EXPORT=1 AND B.CATALOG_ID = '+ @CATALOG_ID +' AND A.FLAG_RECYCLE =''A'' AND  A.WORKFLOW_STATUS=99 AND A.PARENT_CATEGORY = '+''''+'0'+'''')
	END
ELSE
	BEGIN
	PRINT'ALL2018'
			EXEC ('SELECT DISTINCT A.CATEGORY_SHORT,A.CATEGORY_ID,A.CATEGORY_NAME INTO ##TEMP1'+ @SESSID +' FROM TB_CATEGORY A JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID  AND A.PUBLISH2EXPORT=1 AND B.CATALOG_ID = '+ @CATALOG_ID +'   AND A.FLAG_RECYCLE =''A'' AND A.CATEGORY_ID IN(select * from SplitString('''+@CATEGORY_ID+''','','')) AND  A.WORKFLOW_STATUS=99 AND A.PARENT_CATEGORY = '+''''+'0'+'''')

	END
	
WHILE @FLG<>0
	BEGIN
	
		IF OBJECT_ID('tempdb..##TEMP1'+@SESSID) IS NOT NULL
			BEGIN
				SET @FTABNAME = '##TEMP1'+@SESSID;
				SET @ITABNAME = '##TEMP2'+@SESSID;
			END
		ELSE
			BEGIN
				SET @FTABNAME = '##TEMP2'+@SESSID;
				SET @ITABNAME = '##TEMP1'+@SESSID;
			END
			
		
		IF (@CNT=1)
			BEGIN
			--PRINT'COUNT'
				SET @QUERY ='SELECT @retvalOUT = COUNT(*) FROM ' + @FTABNAME + ' T,TB_CATEGORY TC WHERE T.CATEGORY_ID=TC.PARENT_CATEGORY  AND TC.PUBLISH2EXPORT=1'
				 PRINT @QUERY
				SET @ParmDefinition = '@retvalOUT int OUTPUT';
				EXEC sp_executesql @QUERY, @ParmDefinition, @retvalOUT=@CHK OUTPUT;
				 
			END
		ELSE
			BEGIN
			--PRINT'COUNT2018'
				SET @QUERY ='SELECT @retvalOUT = COUNT(*) FROM ' + @FTABNAME + ' T,TB_CATEGORY TC WHERE T.'+ @COLNAME +'=TC.PARENT_CATEGORY  AND TC.PUBLISH2EXPORT=1'
				PRINT @QUERY;
				SET @ParmDefinition = '@retvalOUT int OUTPUT';
				EXEC sp_executesql @QUERY, @ParmDefinition, @retvalOUT=@CHK OUTPUT;
				--print @CHK
			END
	
		IF(@CHK > 0)
			BEGIN
				IF (@CNT=1)
					BEGIN
					--PRINT 'LOT'
						SET @QUERY = 'SELECT T.*,TC.CATEGORY_SHORT AS SUBCATSHORT_L'+CONVERT(VARCHAR(MAX),@CNT)+',TC.CATEGORY_ID AS SUBCATID_L'+CONVERT(VARCHAR(MAX),@CNT)+',TC.CATEGORY_NAME AS SUBCATNAME_L'+CONVERT(VARCHAR(MAX),@CNT)+' INTO ' + @ITABNAME + ' FROM '+ @FTABNAME +' T
									  LEFT OUTER JOIN
									  TB_CATEGORY TC ON T.CATEGORY_ID=TC.PARENT_CATEGORY AND TC.FLAG_RECYCLE =''A'' AND TC.WORKFLOW_STATUS=99  AND TC.PUBLISH2EXPORT=1'
						PRINT @QUERY;
						EXEC (@QUERY);
						SET @COLNAME='SUBCATID_L';
						SET @COLNAME = @COLNAME + CONVERT(VARCHAR(100),@CNT)
						SET @CNT= @CNT + 1;
						--RETURN
						EXEC ('DROP TABLE ' + @FTABNAME);
						
					END
				ELSE
					BEGIN
						--PRINT 'LOT2018'
						SET @QUERY = 'SELECT T.*,TC.CATEGORY_SHORT AS SUBCATSHORT_L'+CONVERT(VARCHAR(MAX),@CNT)+',TC.CATEGORY_ID AS SUBCATID_L'+CONVERT(VARCHAR(MAX),@CNT)+',TC.CATEGORY_NAME AS SUBCATNAME_L'+CONVERT(VARCHAR(MAX),@CNT)+' INTO ' + @ITABNAME + ' FROM '+ @FTABNAME +' T
									  LEFT OUTER JOIN
									  TB_CATEGORY TC ON T.'+@COLNAME+'=TC.PARENT_CATEGORY AND TC.FLAG_RECYCLE =''A'' AND TC.WORKFLOW_STATUS=99  AND TC.PUBLISH2EXPORT=1'
						EXEC (@QUERY);
						PRINT @QUERY;
						SET @COLNAME='SUBCATID_L';
						SET @COLNAME = @COLNAME + CONVERT(VARCHAR(100),@CNT)
						SET @CNT= @CNT + 1;
						--RETURN
						EXEC ('DROP TABLE ' + @FTABNAME);
						
					END
			END
		ELSE
			SET @FLG=0
	END

DECLARE @TABLE_NAME VARCHAR(100);
IF OBJECT_ID('tempdb..##TEMP1'+@SESSID) IS NOT NULL
	BEGIN
		SET @TABLE_NAME = '##TEMP1'+@SESSID;
	END
IF OBJECT_ID('tempdb..##TEMP2'+@SESSID) IS NOT NULL
	BEGIN
		SET @TABLE_NAME = '##TEMP2'+@SESSID;
	END
--EXEC('SELECT * FROM '+ @TABLE_NAME +'')
DECLARE @COUNT INT = @CNT;
DECLARE @CATEGORY_SQL VARCHAR(MAX) = '';
DECLARE @CATEGORY_SQL_SUBFAMILY VARCHAR(MAX) = '';
DECLARE @CATEGORY_SQL_WITHOUT_FAMILES VARCHAR(MAX) = '';
DECLARE @EEEE VARCHAR(MAX) = '';
DECLARE @TEMP_COUNT_INNER INT = 0;
DECLARE @TEMP_DIFF INT = 0;
----------NEWCATEGORY ATTRIBUTE ---
DECLARE @SubCategoryAttribute VARCHAR(MAX) = '';
DECLARE @ParentCategoryAttribute VARCHAR(MAX) = '';
DECLARE @ALLCATEGORYATTRIBUTE VARCHAR(MAX) = '';
DECLARE @SUBCATEGORYATTRIBUTE6 VARCHAR(MAX) = '';
DECLARE @SUBCATEGORYATTRIBUTE5 VARCHAR(MAX) = '';
DECLARE @SUBCATEGORYATTRIBUTE4 VARCHAR(MAX) = '';
DECLARE @SUBCATEGORYATTRIBUTE3 VARCHAR(MAX) = '';
DECLARE @SUBCATEGORYATTRIBUTE2 VARCHAR(MAX) = '';
DECLARE @SelectedAttribute VARCHAR(MAX) = '';
DECLARE @ONLYPARENTVALUE VARCHAR(MAX);
DECLARE @ONLYPARENTATTRIBUTE VARCHAR(MAX);
DECLARE @PrintOneTIME int=1;
DECLARE @PrintOneTIME3 int=1;
DECLARE @PrintOneTIME4 int=1;
DECLARE @PrintOneTIME5 int=1;
DECLARE @PrintOneTIME6 int=1;
DECLARE @CategoryAttributeCount int=0 ;

IF OBJECT_ID('tempdb..##CategoryAttribute') IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##CategoryAttribute')
	END
	IF OBJECT_ID('tempdb..##SelectedAllCategoryAttribute') IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##SelectedAllCategoryAttribute')
	END		
	IF OBJECT_ID('tempdb..##ResultCategoryAttribute') IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##ResultCategoryAttribute')
	END
SET @SelectedAttribute ='select IDENTITY(INT,1,1) AS ID,* into ##SelectedAllCategoryAttribute from '+@TABLE_NAME+' '	
EXEC  (@SelectedAttribute)
------------ END------------------------


WHILE(@COUNT <> 0)
	BEGIN
	 	Print('While Count')
	 	Print @COUNT
		SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +'SELECT C.CATALOG_ID,C.CATALOG_NAME';
		SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +'SELECT B.CATALOG_ID,D.CATALOG_NAME';
		SET @CATEGORY_SQL = @CATEGORY_SQL +'SELECT B.CATALOG_ID,D.CATALOG_NAME';
		IF(@COUNT = 1)
			BEGIN
			Print('Count =1')
				SET @TEMP_COUNT_INNER = 1;
				WHILE(@TEMP_COUNT_INNER <> (@CNT + 1))
					BEGIN
						IF((@TEMP_COUNT_INNER - 1) < @COUNT)
							BEGIN
								IF(@TEMP_COUNT_INNER = 1)
									BEGIN
									----------NEWCATEGORY ATTRIBUTE --------------------------------------
										DECLARE @ATTRIBUTE_NAMES varchar(max)
										DECLARE @CategoryATTRIBUTEVALUE varchar(max)
										Set @CategoryATTRIBUTEVALUE ='select * into ##CategoryAttribute from '+@TABLE_NAME+' '
										exec(@CategoryATTRIBUTEVALUE);
										With CategoryCTE 
										AS  
										 (SELECT DISTINCT TA.ATTRIBUTE_ID, TA.ATTRIBUTE_NAME, TA.ATTRIBUTE_TYPE, TA.ATTRIBUTE_DATATYPE from ##CategoryAttribute A
										JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = @CATALOG_ID
										JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
										JOIN TB_CATEGORY D ON A.CATEGORY_ID = D.CATEGORY_ID   AND D.PUBLISH2EXPORT=1
										Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.CATEGORY_ID and TCS.PUBLICHCATEGORY_FLAG=1
										join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID 
										inner join   TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TA.FLAG_RECYCLE ='A'   and TA.PUBLISH2EXPORT=1
										AND TA.ATTRIBUTE_ID IN (SELECT Item AS ATTRIBUTE_ID FROM dbo.SplitString(@ATTRIBUTE_IDS, ',')) AND TA.ATTRIBUTE_TYPE IN (21,23,25) AND TA.FLAG_RECYCLE ='A' WHERE (TCA.CATALOG_ID = @CATALOG_ID))
										select @ATTRIBUTE_NAMES=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE(ATTRIBUTE_NAME,']',']]') + ']' FROM CategoryCTE FOR XML PATH('')),2,200000))
										SELECT @ATTRIBUTE_NAMES = (SELECT REPLACE(@ATTRIBUTE_NAMES, ',[],', ','))
										IF (@ATTRIBUTE_NAMES is null or @ATTRIBUTE_NAMES = '')
											 BEGIN
											 SET  @ATTRIBUTE_NAMES='[NULL]';
											 END										   
											 IF OBJECT_ID('tempdb..##MainCategory') IS NOT NULL
											 BEGIN
											 EXEC('DROP TABLE ##MainCategory')
											 END
											 SET @ParentCategoryAttribute='select * into ##MainCategory from (select A.ID, C.CATALOG_ID,C.CATALOG_NAME, A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME,CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%Num%'' THEN CONVERT(VARCHAR,TCS.NUMERIC_VALUE) ELSE CONVERT(VARCHAR(max),TCS.STRING_VALUE) END AS STRING_VALUE,TA.ATTRIBUTE_NAME';	
									  ------------------------------------ END------------------------
										SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +', A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME';
										SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME';
										SET @CATEGORY_SQL = @CATEGORY_SQL +',A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME';
									END
								ELSE
									BEGIN
										SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] ,A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
										SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
										SET @CATEGORY_SQL = @CATEGORY_SQL +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
									END
							END
						ELSE
							BEGIN
								SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
								SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
								SET @CATEGORY_SQL = @CATEGORY_SQL +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
							END
						SET @TEMP_COUNT_INNER = @TEMP_COUNT_INNER + 1;						
					END
				SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES + ' FROM '+ @TABLE_NAME +' A 
												JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID 
												JOIN TB_CATEGORY D ON A.CATEGORY_ID = D.CATEGORY_ID   AND D.PUBLISH2EXPORT=1'
				SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY + ',C.PARENT_FAMILY_ID AS FAMILY_ID,(SELECT TF.FAMILY_NAME FROM TB_FAMILY TF WHERE TF.FAMILY_ID = C.PARENT_FAMILY_ID )FAMILY_NAME,C.FAMILY_ID AS SUBFAMILY_ID,C.FAMILY_NAME AS SUBFAMILY_NAME,C.FOOT_NOTES FAMILY_FOOT_NOTES,C.STATUS FAMILY_STATUS FROM '+ @TABLE_NAME +' A 
												JOIN TB_CATALOG_FAMILY B ON 
												A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'   AND B.FLAG_RECYCLE =''''''''A''''''''
												JOIN TB_FAMILY C ON C.FAMILY_ID = B.FAMILY_ID AND C.ROOT_FAMILY = 0 AND C.WORKFLOW_STATUS=99   AND C.PUBLISH2EXPORT=1
												JOIN TB_CATALOG D ON D.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY E ON A.CATEGORY_ID = E.CATEGORY_ID  AND E.FLAG_RECYCLE =''''''''A''''''''  AND E.PUBLISH2EXPORT=1
												JOIN TB_CATALOG_SECTIONS CS ON E.CATEGORY_ID = CS.CATEGORY_ID  AND CS.FLAG_RECYCLE =''''''''A'''''''' AND CS.CATALOG_ID ='+ @CATALOG_ID +' '
				SET @CATEGORY_SQL = @CATEGORY_SQL + ',B.FAMILY_ID,C.FAMILY_NAME,NULL AS SUBFAMILY_ID,CONVERT(VARCHAR(MAX),NULL) AS SUBFAMILY_NAME,C.FOOT_NOTES FAMILY_FOOT_NOTES,C.STATUS FAMILY_STATUS FROM '+ @TABLE_NAME +' A 
									JOIN TB_CATALOG_FAMILY B ON 
									 
									A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'  AND B.FLAG_RECYCLE =''''''''A''''''''
									JOIN TB_FAMILY C ON C.FAMILY_ID = B.FAMILY_ID AND C.ROOT_FAMILY = 1 AND C.WORKFLOW_STATUS=99  AND C.FLAG_RECYCLE =''''''''A''''''''  AND C.PUBLISH2EXPORT=1
									JOIN TB_CATALOG D ON D.CATALOG_ID = B.CATALOG_ID
									JOIN TB_CATEGORY E ON A.CATEGORY_ID = E.CATEGORY_ID  AND E.FLAG_RECYCLE =''''''''A''''''''  AND E.PUBLISH2EXPORT=1
									JOIN TB_CATALOG_SECTIONS CS ON E.CATEGORY_ID = CS.CATEGORY_ID  AND CS.FLAG_RECYCLE =''''''''A'''''''' AND CS.CATALOG_ID ='+ @CATALOG_ID +' '

									PRINT'PRAENT1'
				SET @ParentCategoryAttribute = @ParentCategoryAttribute + ' FROM ##SelectedAllCategoryAttribute A 
												JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID 
												JOIN TB_CATEGORY D ON A.CATEGORY_ID = D.CATEGORY_ID   AND D.PUBLISH2EXPORT=1
												Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.CATEGORY_ID and TCS.PUBLICHCATEGORY_FLAG=1
								                left join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID)table1
								                pivot(Max(STRING_VALUE) FOR ATTRIBUTE_NAME IN('+@ATTRIBUTE_NAMES+'))as PIVOTE_PARENT_CATEGORY '
												PRINT'PRAENT2'

												if (@ATTRIBUTE_NAMES ='[NULL]')
											     begin
											    SET  @ATTRIBUTE_NAMES='';
												 SET  @ONLYPARENTATTRIBUTE=','+@ATTRIBUTE_NAMES +''
											    end 
											    ELSE
											    BEGIN
											   print @ATTRIBUTE_NAMES
											  SET  @ONLYPARENTATTRIBUTE=','+@ATTRIBUTE_NAMES +','
											  END  
				SET @ALLCATEGORYATTRIBUTE='SELECT MC.CATALOG_ID,MC.CATALOG_NAME,MC.CATEGORY_ID,MC.CATEGORY_NAME'+ @ONLYPARENTATTRIBUTE+@ALLCATEGORYATTRIBUTE
			   if(@COUNT=1 and @ONLYPARENTVALUE<>1 or @ONLYPARENTVALUE='' or @ONLYPARENTVALUE is null)
			begin

		                                   	if (@ATTRIBUTE_NAMES ='[NULL]' or @ATTRIBUTE_NAMES ='')
											     begin
											    SET  @ATTRIBUTE_NAMES='';
												 SET  @ONLYPARENTATTRIBUTE=' '+@ATTRIBUTE_NAMES +' '
											    end 
											    ELSE
											    BEGIN
											   print @ATTRIBUTE_NAMES
											  SET  @ONLYPARENTATTRIBUTE=','+@ATTRIBUTE_NAMES +''
											  END                      
											
			declare @ALLPARENTCATEGORYATTRIBUTE varchar(max)
			SET @ALLCATEGORYATTRIBUTE=''
			    SET  @ALLPARENTCATEGORYATTRIBUTE ='SELECT MC.CATALOG_ID,MC.CATALOG_NAME,MC.CATEGORY_ID,MC.CATEGORY_NAME'+ @ONLYPARENTATTRIBUTE+@ALLCATEGORYATTRIBUTE               
				SET @ALLCATEGORYATTRIBUTE=@ALLPARENTCATEGORYATTRIBUTE +  'into ##ResultCategoryAttribute FROM ##SelectedAllCategoryAttribute AL Left join ##MainCategory MC on AL.ID=MC.ID'
			end

			
			
			END
		ELSE
			BEGIN
			Print('Count <> 1')
			SET @ONLYPARENTVALUE=1
				SET @TEMP_COUNT_INNER = 1;
				WHILE(@TEMP_COUNT_INNER <> (@CNT + 1))
					BEGIN						
						IF((@TEMP_COUNT_INNER - 1) < @COUNT)
							BEGIN
								IF(@TEMP_COUNT_INNER = 1)
									BEGIN
										SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME';
										SET @CATEGORY_SQL = @CATEGORY_SQL +',A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME';
										SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME';
									END
								ELSE
									BEGIN
										SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';

										SET @CATEGORY_SQL = @CATEGORY_SQL +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';

										SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';


										IF(@COUNT =2)
											BEGIN
											
											IF(@COUNT-@AttributeValue =1)
											BEGIN
												IF OBJECT_ID('tempdb..##SubCategoryAttribute1') IS NOT NULL
	                                         BEGIN
	                                       	EXEC('DROP TABLE ##SubCategoryAttribute1')
	                                             END
											declare @SubCategoryATTRIBUTEVALUE1 varchar(max)
									        Set @SubCategoryATTRIBUTEVALUE1 ='select * into ##SubCategoryAttribute1 from '+@TABLE_NAME+' '
									        exec(@SubCategoryATTRIBUTEVALUE1);
											 With CategoryCTE 
                                             AS  
                                            (SELECT DISTINCT TA.ATTRIBUTE_ID, TA.ATTRIBUTE_NAME, TA.ATTRIBUTE_TYPE, TA.ATTRIBUTE_DATATYPE from ##SubCategoryAttribute1 A
	                                        JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L1 = B.CATEGORY_ID AND B.CATALOG_ID = @CATALOG_ID
	                                        JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
                                            JOIN TB_CATEGORY D ON  A.SUBCATID_L1 = D.CATEGORY_ID   AND D.PUBLISH2EXPORT=1
	                                        Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L1 and TCS.PUBLICHCATEGORY_FLAG=1
                                            join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID 
	                                        inner join   TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TA.FLAG_RECYCLE ='A'   and TA.PUBLISH2EXPORT=1
                                            AND TA.ATTRIBUTE_ID IN (SELECT Item AS ATTRIBUTE_ID FROM dbo.SplitString(@ATTRIBUTE_IDS, ',')) AND TA.ATTRIBUTE_TYPE IN (21,23,25) AND TA.FLAG_RECYCLE ='A' WHERE (TCA.CATALOG_ID = @CATALOG_ID))
                                            select @ATTRIBUTE_NAMES=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE('SUBL1-'+ATTRIBUTE_NAME,']',']]') + ']' FROM CategoryCTE FOR XML PATH('')),2,200000))
                                             SELECT @ATTRIBUTE_NAMES = (SELECT REPLACE(@ATTRIBUTE_NAMES, ',[],', ','))  
                                              if (@ATTRIBUTE_NAMES is null or @ATTRIBUTE_NAMES = '')
											  begin
											  SET  @ATTRIBUTE_NAMES='[NULL]';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=''+@ATTRIBUTE_NAMES+''
											   END
											   IF OBJECT_ID('tempdb..##SubCategory1') IS NOT NULL
	                                           BEGIN
	                                       	      EXEC('DROP TABLE ##SubCategory1')
	                                            END
												SET @SubCategoryAttribute=@SubCategoryAttribute+'select * into ##SubCategory1 from (select A.ID,A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME,A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],
												CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%Num%'' THEN CONVERT(VARCHAR(MAX),TCS.NUMERIC_VALUE) ELSE CONVERT(VARCHAR(MAX),TCS.STRING_VALUE) END AS STRING_VALUE,concat(''SUBL1-'',TA.ATTRIBUTE_NAME) as ATTRIBUTE_NAME  FROM ##SelectedAllCategoryAttribute A 
												JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY D ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = D.CATEGORY_ID  AND D.PUBLISH2EXPORT=1 
												Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' and TCS.PUBLICHCATEGORY_FLAG=1
								                left join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID)table1 
												pivot(Max(STRING_VALUE) FOR ATTRIBUTE_NAME IN('+ISNULL(@ATTRIBUTE_NAMES,'''') +'))as PIVOTE_PARENT_CATEGORY' ;
											 SET	@AttributeValue=@AttributeValue+1;
											 if (@ATTRIBUTE_NAMES ='[NULL]')
											  begin
											  SET  @ATTRIBUTE_NAMES='';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=','+@ATTRIBUTE_NAMES+''
											   END
										
											        SET @CategoryAttributeCount=2+@CategoryAttributeCount							
												END
											END
											ELSE IF(@COUNT =3)

											BEGIN
											
											IF(@COUNT-@AttributeValue=2)
											BEGIN
												IF OBJECT_ID('tempdb..##SubCategoryAttribute2') IS NOT NULL
	                                         BEGIN
	                                       	EXEC('DROP TABLE ##SubCategoryAttribute2')
	                                             END
												

                                              IF(@PrintOneTIME=1)
											 BEGIN
											 SET @PrintOneTIME=2
											declare @SubCategoryATTRIBUTEVALUE2 varchar(max)
									        Set @SubCategoryATTRIBUTEVALUE2 ='select * into ##SubCategoryAttribute2 from '+@TABLE_NAME+' '
									        exec(@SubCategoryATTRIBUTEVALUE2);
											 With CategoryCTE 
                                             AS  
                                            (SELECT DISTINCT TA.ATTRIBUTE_ID, TA.ATTRIBUTE_NAME, TA.ATTRIBUTE_TYPE, TA.ATTRIBUTE_DATATYPE from ##SubCategoryAttribute2 A
	                                        JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L2 = B.CATEGORY_ID AND B.CATALOG_ID = @CATALOG_ID
	                                        JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
                                            JOIN TB_CATEGORY D ON  A.SUBCATID_L2 = D.CATEGORY_ID   AND D.PUBLISH2EXPORT=1
	                                        Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L2 and TCS.PUBLICHCATEGORY_FLAG=1
                                            join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID 
	                                        inner join   TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TA.FLAG_RECYCLE ='A'   and TA.PUBLISH2EXPORT=1
                                            AND TA.ATTRIBUTE_ID IN (SELECT Item AS ATTRIBUTE_ID FROM dbo.SplitString(@ATTRIBUTE_IDS, ',')) AND TA.ATTRIBUTE_TYPE IN (21,23,25) AND TA.FLAG_RECYCLE ='A' WHERE (TCA.CATALOG_ID = @CATALOG_ID))
                                            select @ATTRIBUTE_NAMES=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE('SUBL2-'+ATTRIBUTE_NAME,']',']]') + ']' FROM CategoryCTE FOR XML PATH('')),2,200000))
                                             SELECT @ATTRIBUTE_NAMES = (SELECT REPLACE(@ATTRIBUTE_NAMES, ',[],', ','))  
                                              if (@ATTRIBUTE_NAMES is null or @ATTRIBUTE_NAMES = '')
											  begin
											  SET  @ATTRIBUTE_NAMES='[NULL]';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=''+@ATTRIBUTE_NAMES+''
											   END
											    
											IF OBJECT_ID('tempdb..##SubCategory2') IS NOT NULL
	                                        BEGIN
	                                       	EXEC('DROP TABLE ##SubCategory2')
	                                        END
											
												SET @SubCategoryAttribute=@SubCategoryAttribute+'select * into ##SubCategory2 from (select A.ID, A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME,A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%Num%'' THEN CONVERT(VARCHAR,TCS.NUMERIC_VALUE) ELSE CONVERT(VARCHAR(max),TCS.STRING_VALUE) END AS STRING_VALUE,concat(''SUBL2-'',TA.ATTRIBUTE_NAME) as ATTRIBUTE_NAME   FROM ##SelectedAllCategoryAttribute A 
												JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY D ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = D.CATEGORY_ID  AND D.PUBLISH2EXPORT=1 
												Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' and TCS.PUBLICHCATEGORY_FLAG=1
								                left join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID)table1 
												pivot(Max(STRING_VALUE) FOR ATTRIBUTE_NAME IN('+@ATTRIBUTE_NAMES+'))as PIVOTE_PARENT_CATEGORY ' ;

										      IF (@ATTRIBUTE_NAMES ='[NULL]')
											  BEGIN
											  SET  @ATTRIBUTE_NAMES='';
											  END
											  ELSE
											  BEGIN
											  SET  @ATTRIBUTE_NAMES=','+@ATTRIBUTE_NAMES+''
											  END
											  SET @SUBCATEGORYATTRIBUTE2 =',S2.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S2.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] '+ @ATTRIBUTE_NAMES+''
											     SET @CategoryAttributeCount=3+@CategoryAttributeCount

											END
											END
											END
											ELSE IF(@COUNT =4)
											BEGIN
											print '565'
											print (@COUNT-@AttributeValue)
											IF(@COUNT-@AttributeValue =3)
											BEGIN
												IF OBJECT_ID('tempdb..##SubCategoryAttribute3') IS NOT NULL
	                                         BEGIN
	                                       	EXEC('DROP TABLE ##SubCategoryAttribute3')
	                                             END
											declare @SubCategoryATTRIBUTEVALUE3 varchar(max)
									        Set @SubCategoryATTRIBUTEVALUE3 ='select * into ##SubCategoryAttribute3 from '+@TABLE_NAME+' '
									        exec(@SubCategoryATTRIBUTEVALUE3);
											 With CategoryCTE 
                                             AS  
                                            (SELECT DISTINCT TA.ATTRIBUTE_ID, TA.ATTRIBUTE_NAME, TA.ATTRIBUTE_TYPE, TA.ATTRIBUTE_DATATYPE from ##SubCategoryAttribute3 A
	                                        JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L3 = B.CATEGORY_ID AND B.CATALOG_ID = @CATALOG_ID
	                                        JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
                                            JOIN TB_CATEGORY D ON  A.SUBCATID_L3 = D.CATEGORY_ID   AND D.PUBLISH2EXPORT=1
	                                        Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L3 and TCS.PUBLICHCATEGORY_FLAG=1
                                            join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID 
	                                        inner join   TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TA.FLAG_RECYCLE ='A'   and TA.PUBLISH2EXPORT=1
                                            AND TA.ATTRIBUTE_ID IN (SELECT Item AS ATTRIBUTE_ID FROM dbo.SplitString(@ATTRIBUTE_IDS, ',')) AND TA.ATTRIBUTE_TYPE IN (21,23,25) AND TA.FLAG_RECYCLE ='A' WHERE (TCA.CATALOG_ID = @CATALOG_ID))
                                            select @ATTRIBUTE_NAMES=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE('SUBL3-'+ATTRIBUTE_NAME,']',']]') + ']' FROM CategoryCTE FOR XML PATH('')),2,200000))
                                             SELECT @ATTRIBUTE_NAMES = (SELECT REPLACE(@ATTRIBUTE_NAMES, ',[],', ','))  
                                              if (@ATTRIBUTE_NAMES is null or @ATTRIBUTE_NAMES = '')
											  begin
											  SET  @ATTRIBUTE_NAMES='[NULL]';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=''+@ATTRIBUTE_NAMES+''
											   END
											  
											   IF OBJECT_ID('tempdb..##SubCategory3') IS NOT NULL
	                                        BEGIN
	                                       	EXEC('DROP TABLE ##SubCategory3')
	                                        END
											print 'fggf'
											print @PrintOneTIME3
											 IF(@PrintOneTIME3=1)
											 BEGIN
											 SET @PrintOneTIME3=2
												SET @SubCategoryAttribute=@SubCategoryAttribute+'select * into ##SubCategory3  from (select A.ID, A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME,A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%Num%'' THEN CONVERT(VARCHAR,TCS.NUMERIC_VALUE) ELSE CONVERT(VARCHAR(max),TCS.STRING_VALUE) END AS STRING_VALUE,concat(''SUBL3-'',TA.ATTRIBUTE_NAME) as ATTRIBUTE_NAME FROM ##SelectedAllCategoryAttribute A 
												JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY D ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = D.CATEGORY_ID  AND D.PUBLISH2EXPORT=1 
												Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' and TCS.PUBLICHCATEGORY_FLAG=1
								                left join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID)table1 
												pivot(Max(STRING_VALUE) FOR ATTRIBUTE_NAME IN('+@ATTRIBUTE_NAMES+'))as PIVOTE_PARENT_CATEGORY ' ;
												--SET	@AttributeValue=@AttributeValue+1;
												 if (@ATTRIBUTE_NAMES ='[NULL]')
											  begin
											  SET  @ATTRIBUTE_NAMES='';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=','+@ATTRIBUTE_NAMES+''
											   END
												SET @SUBCATEGORYATTRIBUTE3 =',S3.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S3.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] '+ @ATTRIBUTE_NAMES+''
												SET @CategoryAttributeCount=4+@CategoryAttributeCount
												END
												END
											END
											ELSE IF(@COUNT =5)

											BEGIN
											IF(@COUNT-@AttributeValue =4)
											BEGIN
											IF OBJECT_ID('tempdb..##SubCategoryAttribute4') IS NOT NULL
	                                         BEGIN
	                                       	EXEC('DROP TABLE ##SubCategoryAttribute4')
	                                             END
											declare @SubCategoryATTRIBUTEVALUE4 varchar(max)
									        Set @SubCategoryATTRIBUTEVALUE4 ='select * into ##SubCategoryAttribute4 from '+@TABLE_NAME+' '
									        exec(@SubCategoryATTRIBUTEVALUE4);
											 With CategoryCTE 
                                             AS  
                                            (SELECT DISTINCT TA.ATTRIBUTE_ID, TA.ATTRIBUTE_NAME, TA.ATTRIBUTE_TYPE, TA.ATTRIBUTE_DATATYPE from ##SubCategoryAttribute4 A
	                                        JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L4 = B.CATEGORY_ID AND B.CATALOG_ID = @CATALOG_ID
	                                        JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
                                            JOIN TB_CATEGORY D ON  A.SUBCATID_L4 = D.CATEGORY_ID   AND D.PUBLISH2EXPORT=1
	                                        Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L4 and TCS.PUBLICHCATEGORY_FLAG=1
                                            join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID 
	                                        inner join   TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TA.FLAG_RECYCLE ='A'   and TA.PUBLISH2EXPORT=1
                                            AND TA.ATTRIBUTE_ID IN  (SELECT Item AS ATTRIBUTE_ID FROM dbo.SplitString(@ATTRIBUTE_IDS, ',')) AND TA.ATTRIBUTE_TYPE IN (21,23,25) AND TA.FLAG_RECYCLE ='A' WHERE (TCA.CATALOG_ID = @CATALOG_ID))
                                            select @ATTRIBUTE_NAMES=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE('SUBL4-'+ATTRIBUTE_NAME,']',']]') + ']' FROM CategoryCTE FOR XML PATH('')),2,200000))
                                             SELECT @ATTRIBUTE_NAMES = (SELECT REPLACE(@ATTRIBUTE_NAMES, ',[],', ','))  
                                             
                                              if (@ATTRIBUTE_NAMES is null or @ATTRIBUTE_NAMES = '')
											  begin
											  SET  @ATTRIBUTE_NAMES='[NULL]';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=''+@ATTRIBUTE_NAMES+''
											   END
											  
											   IF OBJECT_ID('tempdb..##SubCategory4') IS NOT NULL
	                                        BEGIN
	                                       	EXEC('DROP TABLE ##SubCategory4')
	                                        END
											IF(@PrintOneTIME4=1)
											 BEGIN
											 SET @PrintOneTIME4=2
												SET @SubCategoryAttribute=@SubCategoryAttribute+'select * into ##SubCategory4 from (select A.ID, A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME,A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%Num%'' THEN CONVERT(VARCHAR,TCS.NUMERIC_VALUE) ELSE CONVERT(VARCHAR(max),TCS.STRING_VALUE) END AS STRING_VALUE,concat(''SUBL4-'',TA.ATTRIBUTE_NAME) as ATTRIBUTE_NAME   FROM ##SelectedAllCategoryAttribute A 
												JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY D ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = D.CATEGORY_ID  AND D.PUBLISH2EXPORT=1 
												Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' and TCS.PUBLICHCATEGORY_FLAG=1
								                left join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID)table1 
												pivot(Max(STRING_VALUE) FOR ATTRIBUTE_NAME IN('+@ATTRIBUTE_NAMES+'))as PIVOTE_PARENT_CATEGORY ' ;
												--SET	@AttributeValue=@AttributeValue+1;
												 if (@ATTRIBUTE_NAMES ='[NULL]')
											  begin
											  SET  @ATTRIBUTE_NAMES='';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=','+@ATTRIBUTE_NAMES+''
											   END
												SET @SUBCATEGORYATTRIBUTE4 =',S4.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S4.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +']'+ @ATTRIBUTE_NAMES+''
											   SET @CategoryAttributeCount=5+@CategoryAttributeCount
												END
												END
											END
											ELSE IF(@COUNT =6)

											BEGIN
											print @COUNT
											print @AttributeValue
											IF(@COUNT-@AttributeValue =5)
											BEGIN
											IF OBJECT_ID('tempdb..##SubCategoryAttribute5') IS NOT NULL
	                                         BEGIN
	                                       	EXEC('DROP TABLE ##SubCategoryAttribute5')
	                                         END
											declare @SubCategoryATTRIBUTEVALUE5 varchar(max)
									        Set @SubCategoryATTRIBUTEVALUE5 ='select * into ##SubCategoryAttribute5 from '+@TABLE_NAME+' '
									        exec(@SubCategoryATTRIBUTEVALUE5);
											 With CategoryCTE 
                                             AS  
                                            (SELECT DISTINCT TA.ATTRIBUTE_ID, TA.ATTRIBUTE_NAME, TA.ATTRIBUTE_TYPE, TA.ATTRIBUTE_DATATYPE from ##SubCategoryAttribute5 A
	                                        JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L5 = B.CATEGORY_ID AND B.CATALOG_ID = @CATALOG_ID
	                                        JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
                                            JOIN TB_CATEGORY D ON  A.SUBCATID_L5 = D.CATEGORY_ID   AND D.PUBLISH2EXPORT=1
	                                        Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L5 and TCS.PUBLICHCATEGORY_FLAG=1
                                            join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID 
	                                        inner join   TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TA.FLAG_RECYCLE ='A'   and TA.PUBLISH2EXPORT=1
                                            AND TA.ATTRIBUTE_ID IN (SELECT Item AS ATTRIBUTE_ID FROM dbo.SplitString(@ATTRIBUTE_IDS, ',')) AND TA.ATTRIBUTE_TYPE IN (21,23,25) AND TA.FLAG_RECYCLE ='A' WHERE (TCA.CATALOG_ID = @CATALOG_ID))
                                             select @ATTRIBUTE_NAMES=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE('SUBL5-'+ATTRIBUTE_NAME,']',']]') + ']' FROM CategoryCTE FOR XML PATH('')),2,200000))
                                             SELECT @ATTRIBUTE_NAMES = (SELECT REPLACE(@ATTRIBUTE_NAMES, ',[],', ','))  
											 	  if (@ATTRIBUTE_NAMES is null or @ATTRIBUTE_NAMES = '')
											  begin
											  SET  @ATTRIBUTE_NAMES='[NULL]';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=','+@ATTRIBUTE_NAMES+''
											   END

                                            IF OBJECT_ID('tempdb..##SubCategory5') IS NOT NULL
	                                        BEGIN
	                                       	EXEC('DROP TABLE ##SubCategory5')
	                                        END
												IF(@PrintOneTIME5=1)
											 BEGIN
											 SET @PrintOneTIME5=2
												SET @SubCategoryAttribute=@SubCategoryAttribute+'select * into ##SubCategory5 from (select A.ID, A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME,A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%Num%'' THEN CONVERT(VARCHAR,TCS.NUMERIC_VALUE) ELSE CONVERT(VARCHAR(max),TCS.STRING_VALUE) END AS STRING_VALUE,concat(''SUBL5-'',TA.ATTRIBUTE_NAME) as ATTRIBUTE_NAME FROM ##SelectedAllCategoryAttribute A 
												JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY D ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = D.CATEGORY_ID  AND D.PUBLISH2EXPORT=1 
												Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' and TCS.PUBLICHCATEGORY_FLAG=1
								                left join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID)table1
												pivot(Max(STRING_VALUE) FOR ATTRIBUTE_NAME IN('+@ATTRIBUTE_NAMES+'))as PIVOTE_PARENT_CATEGORY ' ;
												--SET	@AttributeValue=@AttributeValue+1;
												 if (@ATTRIBUTE_NAMES ='[NULL]')
											  begin
											  SET  @ATTRIBUTE_NAMES='';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=''+@ATTRIBUTE_NAMES+''
											   END
												SET @SUBCATEGORYATTRIBUTE5 =',S5.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S5.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +']'+ @ATTRIBUTE_NAMES+''
												SET @CategoryAttributeCount=6+@CategoryAttributeCount
												END
												END
											END
											ELSE IF(@COUNT =7)

											BEGIN
											print @COUNT
											print @AttributeValue

											IF OBJECT_ID('tempdb..##SubCategoryAttribute6') IS NOT NULL
	                                         BEGIN
	                                       	EXEC('DROP TABLE ##SubCategoryAttribute6')
	                                         END
										
											declare @SubCategoryATTRIBUTEVALUE6 varchar(max)
										
									        Set @SubCategoryATTRIBUTEVALUE6 ='select * into ##SubCategoryAttribute6 from '+@TABLE_NAME+' '
									        exec(@SubCategoryATTRIBUTEVALUE6);
											
											 With CategoryCTE 
                                             AS  
                                            (SELECT DISTINCT TA.ATTRIBUTE_ID, TA.ATTRIBUTE_NAME, TA.ATTRIBUTE_TYPE, TA.ATTRIBUTE_DATATYPE from ##SubCategoryAttribute6 A
	                                        JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L6  = B.CATEGORY_ID AND B.CATALOG_ID = @CATALOG_ID
	                                        JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
                                            JOIN TB_CATEGORY D ON A.SUBCATID_L6  = D.CATEGORY_ID   AND D.PUBLISH2EXPORT=1
	                                        Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L6  and TCS.PUBLICHCATEGORY_FLAG=1
                                            join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID 
	                                        inner join   TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TA.FLAG_RECYCLE ='A'   and TA.PUBLISH2EXPORT=1
                                            AND TA.ATTRIBUTE_ID IN (SELECT Item AS ATTRIBUTE_ID FROM dbo.SplitString(@ATTRIBUTE_IDS, ',')) AND TA.ATTRIBUTE_TYPE IN (21,23,25) AND TA.FLAG_RECYCLE ='A' WHERE (TCA.CATALOG_ID = @CATALOG_ID))
                                              select @ATTRIBUTE_NAMES=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE('SUBL6-'+ATTRIBUTE_NAME,']',']]') + ']' FROM CategoryCTE FOR XML PATH('')),2,200000))
                                              SELECT @ATTRIBUTE_NAMES = (SELECT REPLACE(@ATTRIBUTE_NAMES, ',[],', ','))  
											  if (@ATTRIBUTE_NAMES is null or @ATTRIBUTE_NAMES = '')
											  begin
											  SET  @ATTRIBUTE_NAMES='[NULL]';
											  end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=''+@ATTRIBUTE_NAMES+''
											   END
								          
											IF(@COUNT-@AttributeValue =6)
											BEGIN
											IF OBJECT_ID('tempdb..##SubCategory6') IS NOT NULL
	                                        BEGIN
	                                       	EXEC('DROP TABLE ##SubCategory6')
	                                        END
											IF(@PrintOneTIME6=1)
											 BEGIN
											 SET @PrintOneTIME6=2
									        SET @SubCategoryAttribute=@SubCategoryAttribute+'select * into ##SubCategory6 from (select A.ID, A.CATEGORY_SHORT AS CATEGORY_ID,A.CATEGORY_NAME,A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%Num%'' THEN CONVERT(VARCHAR,TCS.NUMERIC_VALUE) ELSE CONVERT(VARCHAR(max),TCS.STRING_VALUE) END AS STRING_VALUE,concat(''SUBL6-'',TA.ATTRIBUTE_NAME) as ATTRIBUTE_NAME  FROM ##SelectedAllCategoryAttribute A 
												JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY D ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = D.CATEGORY_ID  AND D.PUBLISH2EXPORT=1 
												Left join TB_CATEGORY_SPECS TCS ON TCS.CATEGORY_ID=A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' and TCS.PUBLICHCATEGORY_FLAG=1
								                left join TB_ATTRIBUTE TA on TA.ATTRIBUTE_ID=TCS.ATTRIBUTE_ID)table1 
												pivot(Max(STRING_VALUE) FOR ATTRIBUTE_NAME IN('+@ATTRIBUTE_NAMES+'))as PIVOTE_PARENT_CATEGORY ' ;
												--SET	@AttributeValue=@AttributeValue+1;
												print @SubCategoryAttribute
												 if (@ATTRIBUTE_NAMES ='[NULL]')
											   begin
											    SET  @ATTRIBUTE_NAMES='';
											   end
											   ELSE
											   BEGIN
											    SET  @ATTRIBUTE_NAMES=','+@ATTRIBUTE_NAMES+''
											   END
												SET @SUBCATEGORYATTRIBUTE6 =',S6.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S6.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] '+ @ATTRIBUTE_NAMES+''
												SET @CategoryAttributeCount=7
												END
											END	
											END					
									END
							END
						ELSE
							BEGIN
								SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';								
								SET @CATEGORY_SQL = @CATEGORY_SQL +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';								
								SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';								
							END
						SET @TEMP_COUNT_INNER = @TEMP_COUNT_INNER + 1;			
					END
				SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES + ' FROM '+ @TABLE_NAME +' A 
												JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY D ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = D.CATEGORY_ID  AND D.PUBLISH2EXPORT=1 UNION '
				SET @CATEGORY_SQL = @CATEGORY_SQL + ',B.FAMILY_ID,C.FAMILY_NAME,NULL AS SUBFAMILY_ID,CONVERT(VARCHAR(MAX),NULL) AS SUBFAMILY_NAME,C.FOOT_NOTES FAMILY_FOOT_NOTES,C.STATUS FAMILY_STATUS FROM '+ @TABLE_NAME +' A 
									JOIN TB_CATALOG_FAMILY B ON 
									A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'  AND B.FLAG_RECYCLE =''''''''A''''''''
									JOIN TB_FAMILY C ON C.FAMILY_ID = B.FAMILY_ID AND C.ROOT_FAMILY = 1 AND C.WORKFLOW_STATUS=99   AND C.FLAG_RECYCLE =''''''''A''''''''  AND C.PUBLISH2EXPORT=1
									JOIN TB_CATALOG D ON D.CATALOG_ID = B.CATALOG_ID
									JOIN TB_CATEGORY E ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = E.CATEGORY_ID  AND E.FLAG_RECYCLE =''''''''A''''''''  AND E.PUBLISH2EXPORT=1
									JOIN TB_CATALOG_SECTIONS CS ON E.CATEGORY_ID = CS.CATEGORY_ID  AND CS.FLAG_RECYCLE =''''''''A'''''''' AND CS.CATALOG_ID ='+ @CATALOG_ID +' UNION '
				SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY + ',C.PARENT_FAMILY_ID AS FAMILY_ID,(SELECT TF.FAMILY_NAME FROM TB_FAMILY TF WHERE TF.FAMILY_ID = C.PARENT_FAMILY_ID)FAMILY_NAME,C.FAMILY_ID AS SUBFAMILY_ID,C.FAMILY_NAME AS SUBFAMILY_NAME,C.FOOT_NOTES FAMILY_FOOT_NOTES,C.STATUS FAMILY_STATUS FROM '+ @TABLE_NAME +' A 
												JOIN TB_CATALOG_FAMILY B ON 
												A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'  AND B.FLAG_RECYCLE =''''''''A''''''''
												JOIN TB_FAMILY C ON C.FAMILY_ID = B.FAMILY_ID AND C.ROOT_FAMILY = 0 AND C.WORKFLOW_STATUS=99  AND C.FLAG_RECYCLE =''''''''A''''''''  AND C.PUBLISH2EXPORT=1
												JOIN TB_CATALOG D ON D.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY E ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = E.CATEGORY_ID  AND E.FLAG_RECYCLE =''''''''A''''''''  AND E.PUBLISH2EXPORT=1
												JOIN TB_CATALOG_SECTIONS CS ON E.CATEGORY_ID = CS.CATEGORY_ID  AND CS.FLAG_RECYCLE =''''''''A'''''''' AND CS.CATALOG_ID ='+ @CATALOG_ID +' UNION '
                                                 print 'DFF'
												   print( @CategoryAttributeCount)
												    IF (@CategoryAttributeCount=27)
													BEGIN
													SET @ALLCATEGORYATTRIBUTE=''
													SET @ALLCATEGORYATTRIBUTE ='S1.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S1.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] '+ @ATTRIBUTE_NAMES+@SUBCATEGORYATTRIBUTE2+@SUBCATEGORYATTRIBUTE3+@SUBCATEGORYATTRIBUTE4+@SUBCATEGORYATTRIBUTE5+@SUBCATEGORYATTRIBUTE6+''
													SET @ALLCATEGORYATTRIBUTE=@ALLCATEGORYATTRIBUTE+'into ##ResultCategoryAttribute FROM ##SelectedAllCategoryAttribute AL Left join ##MainCategory MC on AL.ID=MC.ID
                                                                                  Left join ##SubCategory1 S1 on S1.ID=AL.ID
																				  Left join ##SubCategory2 S2 on S2.ID=AL.ID
																				   Left join ##SubCategory3 S3 on S3.ID=AL.ID
																				    Left join ##SubCategory4 S4 on S4.ID=AL.ID
																					Left join ##SubCategory5 S5 on S5.ID=AL.ID
																					Left join ##SubCategory6 S6 on S6.ID=AL.ID'	
													END
													ELSE IF(@CategoryAttributeCount=20)
													BEGIN
													SET @ALLCATEGORYATTRIBUTE=''
													SET @ALLCATEGORYATTRIBUTE ='S1.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S1.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] '+ @ATTRIBUTE_NAMES+@SUBCATEGORYATTRIBUTE2+@SUBCATEGORYATTRIBUTE3+@SUBCATEGORYATTRIBUTE4+@SUBCATEGORYATTRIBUTE5+''	
													SET @ALLCATEGORYATTRIBUTE=@ALLCATEGORYATTRIBUTE+'into ##ResultCategoryAttribute FROM ##SelectedAllCategoryAttribute AL Left join ##MainCategory MC on AL.ID=MC.ID
                                                                                  Left join ##SubCategory1 S1 on S1.ID=AL.ID
																				  Left join ##SubCategory2 S2 on S2.ID=AL.ID
																				   Left join ##SubCategory3 S3 on S3.ID=AL.ID
																				    Left join ##SubCategory4 S4 on S4.ID=AL.ID
																					Left join ##SubCategory5 S5 on S5.ID=AL.ID'
													END
													
													ELSE IF(@CategoryAttributeCount=14)
													BEGIN
													SET @ALLCATEGORYATTRIBUTE=''
													SET @ALLCATEGORYATTRIBUTE ='S1.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S1.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] '+ @ATTRIBUTE_NAMES+@SUBCATEGORYATTRIBUTE2+@SUBCATEGORYATTRIBUTE3+@SUBCATEGORYATTRIBUTE4+''	
													SET @ALLCATEGORYATTRIBUTE=@ALLCATEGORYATTRIBUTE+'into ##ResultCategoryAttribute FROM ##SelectedAllCategoryAttribute AL Left join ##MainCategory MC on AL.ID=MC.ID
                                                                                  Left join ##SubCategory1 S1 on S1.ID=AL.ID
																				  Left join ##SubCategory2 S2 on S2.ID=AL.ID
																				   Left join ##SubCategory3 S3 on S3.ID=AL.ID
																				    Left join ##SubCategory4 S4 on S4.ID=AL.ID'
													END
													ELSE IF(@CategoryAttributeCount=9)
													BEGIN
													SET @ALLCATEGORYATTRIBUTE=''
													SET @ALLCATEGORYATTRIBUTE ='S1.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S1.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] '+ @ATTRIBUTE_NAMES+@SUBCATEGORYATTRIBUTE2+@SUBCATEGORYATTRIBUTE3+''	
													SET @ALLCATEGORYATTRIBUTE=@ALLCATEGORYATTRIBUTE+'into ##ResultCategoryAttribute FROM ##SelectedAllCategoryAttribute AL Left join ##MainCategory MC on AL.ID=MC.ID
                                                                                  Left join ##SubCategory1 S1 on S1.ID=AL.ID
																				  Left join ##SubCategory2 S2 on S2.ID=AL.ID
																				   Left join ##SubCategory3 S3 on S3.ID=AL.ID'
													END
													ELSE IF(@CategoryAttributeCount=5)
													BEGIN
													SET @ALLCATEGORYATTRIBUTE=''
													SET @ALLCATEGORYATTRIBUTE ='S1.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S1.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] '+ @ATTRIBUTE_NAMES+@SUBCATEGORYATTRIBUTE2+''	
													SET @ALLCATEGORYATTRIBUTE=@ALLCATEGORYATTRIBUTE+'into ##ResultCategoryAttribute FROM ##SelectedAllCategoryAttribute AL Left join ##MainCategory MC on AL.ID=MC.ID
                                                                                  Left join ##SubCategory1 S1 on S1.ID=AL.ID
																				  Left join ##SubCategory2 S2 on S2.ID=AL.ID'
																				  
													END
													ELSE
													BEGIN
													SET @ALLCATEGORYATTRIBUTE=''
													SET @ALLCATEGORYATTRIBUTE ='S1.[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'],S1.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@COUNT - 1)) +'] '+ @ATTRIBUTE_NAMES+''	

													SET @ALLCATEGORYATTRIBUTE=@ALLCATEGORYATTRIBUTE+'into ##ResultCategoryAttribute FROM ##SelectedAllCategoryAttribute AL Left join ##MainCategory MC on AL.ID=MC.ID
                                                                                  Left join ##SubCategory1 S1 on S1.ID=AL.ID'
                                                                                
													END
			
			END
		SET @COUNT = @COUNT - 1;
 
	END
	

--SELECT @EEEE
DECLARE @SQL_FAMILY VARCHAR(MAX);

--SELECT @CATEGORY_SQL_WITHOUT_FAMILES
--SELECT @CATEGORY_SQL
--SELECT @CATEGORY_SQL_SUBFAMILY
 PRINT 'REWREWR'
SET @SQL_FAMILY = '
Print(''Check 1 '')
DECLARE @ATTRIBUTE_NAMES VARCHAR(MAX);




--EXEC(@ParentCategoryAttribute);
 --    EXEC(@SubCategoryAttribute);
 --    EXEC(@ALLCATEGORYATTRIBUTE)




;WITH PVT_TEMP_CTE
AS
(
	SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
	FROM            TB_ATTRIBUTE INNER JOIN
                         TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.FLAG_RECYCLE =''A''   and TB_ATTRIBUTE.PUBLISH2EXPORT=1
                          AND TB_ATTRIBUTE.ATTRIBUTE_ID IN (' + @ATTRIBUTE_IDS + ') 
                        -- JOIN (select DISTINCT Item from   dbo.SplitString('''+ @ATTRIBUTE_IDS +''', '','')) DD  ON DD.Item = TB_ATTRIBUTE.ATTRIBUTE_ID
                         AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13) AND TB_ATTRIBUTE.FLAG_RECYCLE =''A''  
	WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
)
SELECT @ATTRIBUTE_NAMES = (SELECT  SUBSTRING((SELECT '',['' +  REPLACE(ATTRIBUTE_NAME,'']'','']]'') + '']'' FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
SELECT @ATTRIBUTE_NAMES = (SELECT REPLACE(@ATTRIBUTE_NAMES, '',[],'', '',''))

DECLARE @ATTRIBUTE_NAMES1 VARCHAR(MAX);

DECLARE @ATTRIBUTE_NAMESCOMMNULL VARCHAR(MAX);
Print(''Check 2 '')
	if exists(select top 1 * from tb_subProduct)
Begin
	;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
	except
	select * from (
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca 
	JOIN TB_ATTRIBUTE TA on tca.ATTRIBUTE_ID=TA.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 join TB_PROD_SPECS tps on TA.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID<>1 and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
union
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca JOIN TB_ATTRIBUTE TA on tca.ATTRIBUTE_ID=TA.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 join TB_PROD_SPECS tps on TA.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.SUBPRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
							) as tt  ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1  AND ta.ATTRIBUTE_TYPE IN (1,3,4,6) and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ') AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID<>1
				)
				SELECT @ATTRIBUTE_NAMESCOMMNULL = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				SELECT @ATTRIBUTE_NAMESCOMMNULL = (SELECT REPLACE(@ATTRIBUTE_NAMESCOMMNULL, '',[],'', '',''))

				end 
				else 
				begin
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
	except
	select * from (
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca JOIN TB_ATTRIBUTE TA on tca.ATTRIBUTE_ID=TA.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 join TB_PROD_SPECS tps on TA.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							   where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID<>1 and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
union
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca JOIN TB_ATTRIBUTE TA on tca.ATTRIBUTE_ID=TA.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 join TB_PROD_SPECS tps on TA.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.SUBPRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
							) as tt  ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  and TA.PUBLISH2EXPORT=1 AND ta.ATTRIBUTE_TYPE IN (1,3,4,6) and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ') AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID<>1
				)
				SELECT @ATTRIBUTE_NAMESCOMMNULL = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				SELECT @ATTRIBUTE_NAMESCOMMNULL = (SELECT REPLACE(@ATTRIBUTE_NAMESCOMMNULL, '',[],'', '',''))
				end

				print @ATTRIBUTE_NAMESCOMMNULL
declare @ATTRIBUTE_NAMESCOMM nvarchar(max)=''''
							SELECT @ATTRIBUTE_NAMESCOMM = (SELECT  SUBSTRING((SELECT '',['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM (select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ') and tca.ATTRIBUTE_ID<>1
	except
	select * from (
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca JOIN TB_ATTRIBUTE TA on tca.ATTRIBUTE_ID=TA.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 join TB_PROD_SPECS tps on TA.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ') and tca.ATTRIBUTE_ID<>1
union
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca JOIN TB_ATTRIBUTE TA on tca.ATTRIBUTE_ID=TA.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 join TB_PROD_SPECS tps on TA.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.SUBPRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ') and tca.ATTRIBUTE_ID<>1
							) as tt  ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 AND ta.ATTRIBUTE_TYPE IN (1,3,4,6) and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ') AND ta.FLAG_RECYCLE =''A'')Attr FOR XML PATH('''')),2,200000))
SELECT @ATTRIBUTE_NAMESCOMM = (SELECT REPLACE(@ATTRIBUTE_NAMESCOMM, '',[],'', '',''))

print @ATTRIBUTE_NAMESCOMM

							SELECT @ATTRIBUTE_NAMES1 = (SELECT  SUBSTRING((SELECT '',['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM (select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	

	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  JOIN TB_ATTRIBUTE TA on tca.ATTRIBUTE_ID=TA.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 join TB_PROD_SPECS tps on TA.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  AND ta.ATTRIBUTE_TYPE IN (1,3,4,6) and TA.PUBLISH2EXPORT=1  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + '))Attr FOR XML PATH('''')),2,200000))
SELECT @ATTRIBUTE_NAMES1 = (SELECT REPLACE(@ATTRIBUTE_NAMES1, '',[],'', '',''))
print ''fdf''
print @ATTRIBUTE_NAMES1

if(@ATTRIBUTE_NAMESCOMM!='''' and (@ATTRIBUTE_NAMES1 is null or @ATTRIBUTE_NAMES1=''''))
Begin
	set @ATTRIBUTE_NAMES1=@ATTRIBUTE_NAMESCOMM
	End
	else if(@ATTRIBUTE_NAMESCOMM!='''' and @ATTRIBUTE_NAMES1!='''')
	Begin
	set @ATTRIBUTE_NAMES1=@ATTRIBUTE_NAMES1+'',''+@ATTRIBUTE_NAMESCOMM
	End
	

print @ATTRIBUTE_NAMES1
print ''fdf123''
declare @ATTRIBUTE_NAMESSUB nvarchar(max)
							SELECT @ATTRIBUTE_NAMESSUB = (SELECT  SUBSTRING((SELECT '',['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM (select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	
	
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca JOIN TB_ATTRIBUTE TA on tca.ATTRIBUTE_ID=TA.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 join TB_PROD_SPECS tps on TA.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.SUBPRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
						  ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID AND ta.ATTRIBUTE_TYPE IN (1,3,4,6) and TA.PUBLISH2EXPORT=1  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID<>1 and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + '))Attr FOR XML PATH('''')),2,200000))
SELECT @ATTRIBUTE_NAMESSUB = (SELECT REPLACE(@ATTRIBUTE_NAMESSUB, '',[],'', '',''))
if(@ATTRIBUTE_NAMESCOMM!='''')
	set @ATTRIBUTE_NAMESSUB=@ATTRIBUTE_NAMESSUB+'',''+@ATTRIBUTE_NAMESCOMM
print @ATTRIBUTE_NAMESSUB



--;WITH PVT_TEMP_CTE1
--AS
--(
--	SELECT DISTINCT  TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
--	FROM            TB_ATTRIBUTE INNER JOIN
--							 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID 
--							 AND TB_ATTRIBUTE.ATTRIBUTE_ID IN (' + @ATTRIBUTE_IDS + ') 
--							 -- JOIN (select DISTINCT Item from   dbo.SplitString('''+ @ATTRIBUTE_IDS +''', '','')) DD  ON DD.Item = TB_ATTRIBUTE.ATTRIBUTE_ID
--							 AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (1,3,4,6)  AND TB_ATTRIBUTE.FLAG_RECYCLE =''A''
--	WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
--)
--SELECT @ATTRIBUTE_NAMES1 = (SELECT  SUBSTRING((SELECT '',['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM PVT_TEMP_CTE1 FOR XML PATH('''')),2,200000))
--SELECT @ATTRIBUTE_NAMES1 = (SELECT REPLACE(@ATTRIBUTE_NAMES1, '',[],'', '',''))




PRINT @ATTRIBUTE_NAMES1
Print(''Check 3 '')
--PRINT @ATTRIBUTE_NAMES
DECLARE @FAMILY_ROWS_COUNT INT = 0;
		;WITH FMILY_TEMP_CTE
		AS
		(
			'+ REPLACE(@CATEGORY_SQL,'''''''''','''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''') +'
		)
		SELECT @FAMILY_ROWS_COUNT = 1 FROM FMILY_TEMP_CTE
		
		DECLARE @PRODUCT_ROWS_COUNT INT = 0;
		IF(@FAMILY_ROWS_COUNT = 1)
			BEGIN
				;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''') +'
				)
				,PRODUCT_CTE
				AS
				(
					--SELECT	tcf.FAMILY_ID AS FAMILY_ID_1,tpf.PRODUCT_ID
					--FROM TB_CATALOG_FAMILY tcf						
					--,TB_PROD_FAMILY tpf 
					--where TCF.ROOT_CATEGORY = ''0'' AND tcf.FAMILY_ID=tpf.FAMILY_ID
					--and tcf.CATALOG_ID=' + @CATALOG_ID + ' 
					
					SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1,tpf.PRODUCT_ID
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''0'' AND tcf.FAMILY_ID=tpf.FAMILY_ID AND tpf.FLAG_RECYCLE =''A'' AND tcf.FLAG_RECYCLE =''A'' and tpf.PUBLISH2EXPORT=1
					and tcf.CATALOG_ID=' + @CATALOG_ID + ' and tpf.FAMILY_ID='+@FAMILY_ID+'
					UNION
					SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1,NULL
					FROM TB_CATALOG_FAMILY tcf						
					,TB_FAMILY tpf  
					where TCF.ROOT_CATEGORY = ''0''   AND tcf.FLAG_RECYCLE =''A'' AND tpf.FLAG_RECYCLE =''A''
					and tcf.CATALOG_ID=' + @CATALOG_ID + '  AND  TCF.FAMILY_ID NOT IN(SELECT	tcf.FAMILY_ID 
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''0'' AND tcf.FAMILY_ID=tpf.FAMILY_ID  AND tpf.FLAG_RECYCLE =''A''  AND tcf.FLAG_RECYCLE =''A'' and tpf.PUBLISH2EXPORT=1
					and tcf.CATALOG_ID=' + @CATALOG_ID + ' )
				)
				,FINALE_PRODUCTS
				AS
				(
					SELECT DISTINCT * FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
					UNION
					SELECT DISTINCT * FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				)
				SELECT @PRODUCT_ROWS_COUNT = 1 FROM FINALE_PRODUCTS
			END

PRINT @FAMILY_ROWS_COUNT;
PRINT @PRODUCT_ROWS_COUNT;
DECLARE @SQLFAM NVARCHAR(MAX);
DECLARE @SQL1 NVARCHAR(MAX);
DECLARE @FAMILY_SPECS_VALUES VARCHAR(MAX) = '''';
DECLARE @PRODUCT_SPECS_VALUES VARCHAR(MAX) = '''';

Print(''Check 4 '')

IF(@PRODUCT_ROWS_COUNT = 0 AND @FAMILY_ROWS_COUNT = 0)
	BEGIN
Print(''Check 5 '')
		IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
				
				SET @SQLFAM = '';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUUBFAMILY_ID,NULL AS SUBFAMILY_NAME INTO '+ @FTABMAINFAM + '  FROM FAMILY_TEMP_CTE'';

				SET @SQL1 = ''	;WITH FAMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUUBFAMILY_ID,NULL AS SUBFAMILY_NAME,NULL AS PRODUCT_ID INTO '+ @FTABMAIN + '  FROM FAMILY_TEMP_CTE
				'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN			
			PRINT 2	
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
										 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.PUBLISH2EXPORT=1 AND TB_ATTRIBUTE.FLAG_RECYCLE =''A'' AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
										 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				SET @SQLFAM = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES + '
				)				
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,'' + @FAMILY_SPECS_VALUES + '' INTO '+ @FTABMAINFAM + 'FAM  FROM FAMILY_TEMP_CTE'''')'';
				
				SET @SQL1 = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES + '
				)				
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,'' + @FAMILY_SPECS_VALUES + ''
				,NULL AS PRODUCT_ID INTO '+ @FTABMAIN + '  FROM FAMILY_TEMP_CTE'''')'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN	
					PRINT 3	
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	

	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
	  left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  AND TA.PUBLISH2EXPORT=1  AND ta.ATTRIBUTE_TYPE IN (1,3,4,6)  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')

				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				if(@ATTRIBUTE_NAMESCOMMNULL!='''')
					   set @PRODUCT_SPECS_VALUES=@PRODUCT_SPECS_VALUES+'',''+@ATTRIBUTE_NAMESCOMMNULL 

				SET @SQLfAM = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES + '
				)				
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME
				INTO '+ @FTABMAINFAM + ' FROM FAMILY_TEMP_CTE'''')'';				

				SET @SQL1 = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES + '
				)				
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,NULL AS PRODUCT_ID,'' + @PRODUCT_SPECS_VALUES + ''
				INTO '+ @FTABMAIN + ' FROM FAMILY_TEMP_CTE'''')'';				
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN
			PRINT 4
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
										 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.PUBLISH2EXPORT=1  AND TB_ATTRIBUTE.FLAG_RECYCLE =''A'' AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
										 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))				
				print @FAMILY_SPECS_VALUES
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	

	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  AND TA.PUBLISH2EXPORT=1   AND ta.ATTRIBUTE_TYPE IN (1,3,4,6)  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				if(@ATTRIBUTE_NAMESCOMMNULL!='''')
					   set @PRODUCT_SPECS_VALUES=@PRODUCT_SPECS_VALUES+'',''+@ATTRIBUTE_NAMESCOMMNULL
					print @PRODUCT_SPECS_VALUES
				SET @SQLFAM = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES +'
				)
				SELECT  *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,'' + @FAMILY_SPECS_VALUES + ''  INTO '+ @FTABMAINFAM + '  FROM FAMILY_TEMP_CTE
				'''')	
				'';
				
				SET @SQL1 = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES + '
				)				
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,NULL AS PRODUCT_ID,'' + @PRODUCT_SPECS_VALUES + ''
				INTO '+ @FTABMAIN + ' FROM FAMILY_TEMP_CTE'''')'';				
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN
			PRINT 4
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
										 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID  AND TB_ATTRIBUTE.PUBLISH2EXPORT=1  AND TB_ATTRIBUTE.FLAG_RECYCLE =''A'' AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
										 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))				
				print @FAMILY_SPECS_VALUES
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	

	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  AND TA.PUBLISH2EXPORT=1   AND ta.ATTRIBUTE_TYPE IN (1,3,4,6)  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
					print @PRODUCT_SPECS_VALUES
					if(@ATTRIBUTE_NAMESCOMMNULL!='''')
					   set @PRODUCT_SPECS_VALUES=@PRODUCT_SPECS_VALUES+'',''+@ATTRIBUTE_NAMESCOMMNULL
					SET @SQLFAM = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES +'
				)
				SELECT  *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,'' + @FAMILY_SPECS_VALUES + ''  INTO '+ @FTABMAINFAM + '  FROM FAMILY_TEMP_CTE
				'''')
				'';

				SET @SQL1 = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES +'
				)
				SELECT  *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,'' + @FAMILY_SPECS_VALUES + '',NULL AS PRODUCT_ID,'' + @PRODUCT_SPECS_VALUES + ''  INTO '+ @FTABMAIN + '  FROM FAMILY_TEMP_CTE
				'''')
				'';
			END			
	END
ELSE IF(@PRODUCT_ROWS_COUNT = 1 AND @FAMILY_ROWS_COUNT = 1)
	BEGIN
	 Print(''Check 6 '')
		IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
				Print(''Check 6A '')
				SET @SQLFAM = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION 
					'+ REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'					
				)
				,PRODUCT_CTE
				AS
				(
					 
						SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FAMILY_ID=tpf.FAMILY_ID
					and tcf.CATALOG_ID=' + @CATALOG_ID + '  AND tpf.FLAG_RECYCLE =''''A'''' 
					 AND tcf.FLAG_RECYCLE =''''A''''  and tpf.PUBLISH2EXPORT=1 and tpf.FAMILY_ID='+@FAMILY_ID+'
					UNION
					SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1
					FROM TB_CATALOG_FAMILY tcf						
					,TB_FAMILY tpf  
					where TCF.ROOT_CATEGORY = ''''0''''  AND tcf.FLAG_RECYCLE =''''A'''' AND tpf.FLAG_RECYCLE =''''A''''
					and tcf.CATALOG_ID=' + @CATALOG_ID + '  AND  TCF.FAMILY_ID NOT IN(SELECT	tcf.FAMILY_ID 
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FAMILY_ID=tpf.FAMILY_ID  AND tpf.FLAG_RECYCLE =''''A''''  AND tcf.FLAG_RECYCLE =''''A'''' and tpf.PUBLISH2EXPORT=1
					and tcf.CATALOG_ID=' + @CATALOG_ID + ' )
						
				)
				SELECT DISTINCT *  INTO '+ @FTABMAINFAM + '  FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
				UNION 
				SELECT DISTINCT * FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1 FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1 FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL FROM 
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				ORDER BY FAMILY_ID
					'';
						
				SET @SQL1 = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION 
					'+ REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'					
				)
				,PRODUCT_CTE
				AS
				(
					 
						SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1,tpf.PRODUCT_ID
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FAMILY_ID=tpf.FAMILY_ID
					and tcf.CATALOG_ID=' + @CATALOG_ID + '  AND tpf.FLAG_RECYCLE =''''A''''  
					AND tcf.FLAG_RECYCLE =''''A''''  and tpf.PUBLISH2EXPORT=1 and tpf.FAMILY_ID='+@FAMILY_ID+'
					UNION
					SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1,NULL
					FROM TB_CATALOG_FAMILY tcf						
					,TB_FAMILY tpf  
					where TCF.ROOT_CATEGORY = ''''0''''  AND tcf.FLAG_RECYCLE =''''A'''' AND tpf.FLAG_RECYCLE =''''A''''
					and tcf.CATALOG_ID=' + @CATALOG_ID + '  AND  TCF.FAMILY_ID NOT IN(SELECT	tcf.FAMILY_ID 
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FAMILY_ID=tpf.FAMILY_ID  AND tpf.FLAG_RECYCLE =''''A''''  AND tcf.FLAG_RECYCLE =''''A'''' and tpf.PUBLISH2EXPORT=1
					and tcf.CATALOG_ID=' + @CATALOG_ID + ' )
						
				)
				SELECT DISTINCT *  INTO '+ @FTABMAIN + '  FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
				UNION
				SELECT DISTINCT * FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL FROM 
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				ORDER BY FAMILY_ID
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
			Print(''Check 7 '')
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
									TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID  AND TB_ATTRIBUTE.PUBLISH2EXPORT=1   AND TB_ATTRIBUTE.FLAG_RECYCLE =''A'' AND TB_ATTRIBUTE.ATTRIBUTE_ID 
									IN (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				PRINT (@FAMILY_SPECS_VALUES)
				
				SET @SQLFAM = ''
				;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION 
					'+ REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT  * FROM 
					(
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL 
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID  AND E.FLAG_RECYCLE =''''A''''
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID  AND E.FLAG_RECYCLE =''''A''''
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR, D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID  AND E.FLAG_RECYCLE =''''A''''
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)
				,PRODUCT_CTE
				AS
				(
						 
					SELECT	DISTINCT TCF.FAMILY_ID AS FAMILY_ID_1
					FROM TB_CATALOG_FAMILY TCF						
					,TB_PROD_FAMILY TPF 
					WHERE TCF.ROOT_CATEGORY = ''''0'''' AND TCF.FAMILY_ID=TPF.FAMILY_ID  AND tcf.FLAG_RECYCLE =''''A'''' 
					 AND tpf.FLAG_RECYCLE =''''A'''' and tpf.PUBLISH2EXPORT=1
					AND TCF.CATALOG_ID=' + @CATALOG_ID + ' and tpf.FAMILY_ID='+@FAMILY_ID+'
					UNION
					SELECT	TCF.FAMILY_ID AS FAMILY_ID_1,NULL
					FROM TB_CATALOG_FAMILY TCF						
					,TB_FAMILY TPF  
					WHERE TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FLAG_RECYCLE =''''A'''' AND tcf.FLAG_RECYCLE =''''A''''
					AND TCF.CATALOG_ID=' + @CATALOG_ID + '  AND  TCF.FAMILY_ID NOT IN(SELECT	TCF.FAMILY_ID 
					FROM TB_CATALOG_FAMILY TCF						
					,TB_PROD_FAMILY TPF 
					WHERE TCF.ROOT_CATEGORY = ''''0'''' AND TCF.FAMILY_ID=TPF.FAMILY_ID  AND tcf.FLAG_RECYCLE =''''A''''  AND TPF.FLAG_RECYCLE =''''A'''' and tpf.PUBLISH2EXPORT=1
					AND TCF.CATALOG_ID=' + @CATALOG_ID + ' )
				)
				SELECT DISTINCT  * INTO '+ @FTABMAIN + ' FROM TEM_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
				UNION
				SELECT DISTINCT * FROM TEM_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,''+ @FAMILY_SPECS_VALUES + '',NULL AS FAMILY_ID_1 FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,''+ @FAMILY_SPECS_VALUES + '',NULL AS FAMILY_ID_1 FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT *,NULL,NULL,NULL,NULL,NULL,NULL,''+ @FAMILY_SPECS_VALUES + '',NULL FROM 
				(				
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';


				SET @SQL1 = ''
				;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION 
					'+ REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT  * FROM 
					(
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL 
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID  AND E.FLAG_RECYCLE =''''A''''
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID  AND E.FLAG_RECYCLE =''''A''''
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR, D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID  AND E.FLAG_RECYCLE =''''A''''
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)
				,PRODUCT_CTE
				AS
				(
						 
					SELECT	DISTINCT TCF.FAMILY_ID AS FAMILY_ID_1,TPF.PRODUCT_ID
					FROM TB_CATALOG_FAMILY TCF						
					,TB_PROD_FAMILY TPF 
					WHERE TCF.ROOT_CATEGORY = ''''0'''' AND TCF.FAMILY_ID=TPF.FAMILY_ID  AND tcf.FLAG_RECYCLE =''''A''''  AND tpf.FLAG_RECYCLE =''''A'''' and tpf.PUBLISH2EXPORT=1
					AND TCF.CATALOG_ID=' + @CATALOG_ID + ' and tpf.FAMILY_ID='+@FAMILY_ID+'
					UNION
					SELECT	TCF.FAMILY_ID AS FAMILY_ID_1,NULL
					FROM TB_CATALOG_FAMILY TCF						
					,TB_FAMILY TPF  
					WHERE TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FLAG_RECYCLE =''''A'''' AND tcf.FLAG_RECYCLE =''''A''''
					AND TCF.CATALOG_ID=' + @CATALOG_ID + '  AND  TCF.FAMILY_ID NOT IN(SELECT	TCF.FAMILY_ID 
					FROM TB_CATALOG_FAMILY TCF						
					,TB_PROD_FAMILY TPF 
					WHERE TCF.ROOT_CATEGORY = ''''0'''' AND TCF.FAMILY_ID=TPF.FAMILY_ID  AND tcf.FLAG_RECYCLE =''''A''''  AND TPF.FLAG_RECYCLE =''''A'''' and tpf.PUBLISH2EXPORT=1
					AND TCF.CATALOG_ID=' + @CATALOG_ID + ' )
				)
				SELECT DISTINCT  * INTO '+ @FTABMAIN + ' FROM TEM_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
				UNION
				SELECT DISTINCT * FROM TEM_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,''+ @FAMILY_SPECS_VALUES + '',NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,''+ @FAMILY_SPECS_VALUES + '',NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT *,NULL,NULL,NULL,NULL,NULL,NULL,''+ @FAMILY_SPECS_VALUES + '',NULL,NULL FROM 
				(				
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN
			 	Print(''Check 8 '')
				if exists(select top 1 * from  TB_SUBPRODUCT)
				begin
			;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	

	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1 AND ta.ATTRIBUTE_TYPE IN (1,3,4,6)  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE  order by ATTRIBUTE_ID FOR XML PATH('''')),2,200000))
				end 
				else
				begin
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	

	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							  join tb_product tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID and TA.PUBLISH2EXPORT=1  AND ta.ATTRIBUTE_TYPE IN (1,3,4,6)  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE  order by ATTRIBUTE_ID FOR XML PATH('''')),2,200000))
				end
				if(@ATTRIBUTE_NAMESCOMMNULL!='''' and @PRODUCT_SPECS_VALUES !='''')
					   set @PRODUCT_SPECS_VALUES=@PRODUCT_SPECS_VALUES+'',''+@ATTRIBUTE_NAMESCOMMNULL
				else if(@ATTRIBUTE_NAMESCOMMNULL!='''' and (@PRODUCT_SPECS_VALUES ='''' or  @PRODUCT_SPECS_VALUES is null))
					   set @PRODUCT_SPECS_VALUES=@ATTRIBUTE_NAMESCOMMNULL

				print @PRODUCT_SPECS_VALUES 
				SET @SQLFAM = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION 
					'+ REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'					
				)
				,PRODUCT_CTE
				AS
				(
				  SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FAMILY_ID=tpf.FAMILY_ID
					and tcf.CATALOG_ID=' + @CATALOG_ID + '  AND tpf.FLAG_RECYCLE =''''A'''' 
					 AND tcf.FLAG_RECYCLE =''''A''''  and tpf.PUBLISH2EXPORT=1 and tpf.FAMILY_ID='+@FAMILY_ID+'
					UNION
					SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1
					FROM TB_CATALOG_FAMILY tcf						
					,TB_FAMILY tpf  
					where TCF.ROOT_CATEGORY = ''''0''''  AND tcf.FLAG_RECYCLE =''''A'''' AND tpf.FLAG_RECYCLE =''''A''''
					and tcf.CATALOG_ID=' + @CATALOG_ID + '  AND  TCF.FAMILY_ID NOT IN(SELECT	tcf.FAMILY_ID 
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FAMILY_ID=tpf.FAMILY_ID  AND tpf.FLAG_RECYCLE =''''A''''  AND tcf.FLAG_RECYCLE =''''A'''' and tpf.PUBLISH2EXPORT=1
					and tcf.CATALOG_ID=' + @CATALOG_ID + ' )
						
				)
				SELECT DISTINCT *  INTO '+ @FTABMAINFAM + '  FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
				UNION
				SELECT DISTINCT * FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1 FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1 FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				
					'';

				SET @SQL1 = ''EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL + ' UNION ' + @CATEGORY_SQL_SUBFAMILY +'
				)				
				,PRODUCT_CTE
				AS
				(
					SELECT DISTINCT FAMILY_ID_1,PRODUCT_ID ,'' + @ATTRIBUTE_NAMES1 + '' FROM 
					(
						SELECT	tcf.FAMILY_ID AS FAMILY_ID_1,tps.PRODUCT_ID ,CASE 
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Tex%''''''''
							THEN tps.STRING_VALUE + CASE WHEN tps.OBJECT_NAME IS NOT NULL AND tps.OBJECT_NAME <> '''''''''''''''' THEN ''''''''[Image_Name:''''''''+ tps.OBJECT_NAME +'''''''']'''''''' ELSE '''''''''''''''' END
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(tps.NUMERIC_VALUE, 0 ))
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Date%''''''''
							THEN CONVERT(VARCHAR,tps.STRING_VALUE)
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Hyperlink%''''''''
							THEN CONVERT(VARCHAR,tps.STRING_VALUE)
						END AS STRING_VALUE
						,ta.ATTRIBUTE_NAME
						FROM TB_CATALOG_FAMILY tcf
						,TB_PROD_SPECS tps
						,TB_PROD_FAMILY tpf 
						,TB_ATTRIBUTE ta 
						where TCF.ROOT_CATEGORY = ''''''''0'''''''' AND tcf.FAMILY_ID=tpf.FAMILY_ID   AND tcf.FLAG_RECYCLE =''''''''A'''''''' and tpf.PUBLISH2EXPORT=1
						and ta.ATTRIBUTE_ID=tps.ATTRIBUTE_ID 
						and tps.PRODUCT_ID=tpf.PRODUCT_ID   AND tpf.FLAG_RECYCLE =''''''''A''''''''
						and tcf.CATALOG_ID=' + @CATALOG_ID + ' 	and tpf.FAMILY_ID='+@FAMILY_ID+'					
						UNION
						SELECT	tcf.FAMILY_ID AS FAMILY_ID_1,tps.PRODUCT_ID ,TPS.ATTRIBUTE_VALUE
						,ta.ATTRIBUTE_NAME
						FROM TB_CATALOG_FAMILY tcf
						,TB_PARTS_KEY tps
						,TB_PROD_FAMILY tpf 
						,TB_ATTRIBUTE ta 
						where TCF.ROOT_CATEGORY = ''''''''0'''''''' AND tcf.FAMILY_ID=tpf.FAMILY_ID  AND tcf.FLAG_RECYCLE =''''''''A'''''''' and tpf.PUBLISH2EXPORT=1
						and ta.ATTRIBUTE_ID=tps.ATTRIBUTE_ID 
						and tps.PRODUCT_ID=tpf.PRODUCT_ID
						and tps.FAMILY_ID=tpf.FAMILY_ID    AND tpf.FLAG_RECYCLE =''''''''A''''''''
						and tcf.CATALOG_ID=tps.CATALOG_ID 
						and tcf.CATALOG_ID=' + @CATALOG_ID + '
						AND TCF.CATEGORY_ID = TPS.CATEGORY_ID AND TPS.CATALOG_ID = ' + @CATALOG_ID + '
					)AS P1 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES1 + '')
					) AS PV1
				)
				SELECT DISTINCT  * INTO '+ @FTABMAIN + ' FROM FAMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
 				UNION
				SELECT DISTINCT * FROM FAMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID,''+ @PRODUCT_SPECS_VALUES +'' FROM FAMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID,''+ @PRODUCT_SPECS_VALUES +'' FROM FAMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,''+ @PRODUCT_SPECS_VALUES +'' FROM 
				(				
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				ORDER BY FAMILY_ID 
				'''')
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN	
				Print(''Check 9 '')		
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
									TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID  AND TB_ATTRIBUTE.PUBLISH2EXPORT=1  AND TB_ATTRIBUTE.FLAG_RECYCLE =''A'' AND TB_ATTRIBUTE.ATTRIBUTE_ID 
									IN (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				
				SELECT @FAMILY_SPECS_VALUES=(SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
			
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID,ROW_NUMBER() over(order by  ta.ATTRIBUTE_ID, ta.ATTRIBUTE_NAME, ta.ATTRIBUTE_TYPE, ta.ATTRIBUTE_DATATYPE) as RowValue from (
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  AND TA.PUBLISH2EXPORT=1  AND ta.ATTRIBUTE_TYPE IN (1,3,4,6)  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE order by RowValue FOR XML PATH('''')),2,200000))

				if(@ATTRIBUTE_NAMESCOMMNULL!='''')
					   set @PRODUCT_SPECS_VALUES=@PRODUCT_SPECS_VALUES+'',''+@ATTRIBUTE_NAMESCOMMNULL

				print @PRODUCT_SPECS_VALUES

					SET @SQLFAM = ''
				;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION 
					'+ REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT  * FROM 
					(
						SELECT	DISTINCT A.*,CASE 
						WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
						THEN CONVERT(NVARCHAR(MAX),ISNULL(D.NUMERIC_VALUE, 0 ))
						ELSE CONVERT(NVARCHAR(MAX),D.STRING_VALUE)
						END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL 
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID  AND E.FLAG_RECYCLE =''''A''''
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID  AND E.FLAG_RECYCLE =''''A''''
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,CASE 
						WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
						THEN CONVERT(NVARCHAR(MAX),ISNULL(D.NUMERIC_VALUE, 0 ))
						ELSE CONVERT(NVARCHAR(MAX), D.STRING_VALUE)
						END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID  AND E.FLAG_RECYCLE =''''A''''
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)
				
				SELECT DISTINCT  * INTO '+ @FTABMAINFAM + ' FROM TEM_CTE A  where SUBFAMILY_ID is null
			    UNION 
				SELECT DISTINCT  *  FROM TEM_CTE A  where SUBFAMILY_ID is not null
			    UNION 
				SELECT DISTINCT *,''+ @FAMILY_SPECS_VALUES + '' FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID FROM TEM_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION 
				SELECT DISTINCT *,''+ @FAMILY_SPECS_VALUES + '' FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT SUBFAMILY_ID FROM TEM_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT *,NULL,NULL,NULL,NULL,NULL,NULL,''+ @FAMILY_SPECS_VALUES + '' FROM 
				(				
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';

				SET @SQL1 = ''EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL + ' UNION ' + @CATEGORY_SQL_SUBFAMILY +'
				)				
				,PRODUCT_CTE
				AS
				(
					SELECT DISTINCT FAMILY_ID_1,PRODUCT_ID ,SORT,'' + @ATTRIBUTE_NAMES1 + '' FROM 
					(
						SELECT	tcf.FAMILY_ID AS FAMILY_ID_1,tps.PRODUCT_ID,tpf.SORT_ORDER as Sort ,CASE 
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Tex%''''''''
							THEN tps.STRING_VALUE + CASE WHEN tps.OBJECT_NAME IS NOT NULL AND tps.OBJECT_NAME <> '''''''''''''''' THEN ''''''''[Image_Name:''''''''+ tps.OBJECT_NAME +'''''''']'''''''' ELSE '''''''''''''''' END
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(tps.NUMERIC_VALUE, 0 ))
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Date%''''''''
							THEN CONVERT(VARCHAR,tps.STRING_VALUE)
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Hyperlink%''''''''
							THEN CONVERT(VARCHAR,tps.STRING_VALUE)
						END AS STRING_VALUE
						,ta.ATTRIBUTE_NAME
						FROM TB_CATALOG_FAMILY tcf
						,TB_PROD_SPECS tps
						,TB_PROD_FAMILY tpf 
						,TB_ATTRIBUTE ta 
						where TCF.ROOT_CATEGORY = ''''''''0'''''''' AND tcf.FAMILY_ID=tpf.FAMILY_ID   AND tcf.FLAG_RECYCLE =''''''''A'''''''' and tpf.PUBLISH2EXPORT=1
						and ta.ATTRIBUTE_ID=tps.ATTRIBUTE_ID 
						and tps.PRODUCT_ID=tpf.PRODUCT_ID   AND tpf.FLAG_RECYCLE =''''''''A''''''''
						and tcf.CATALOG_ID=' + @CATALOG_ID + ' 	 and tpf.FAMILY_ID='+@FAMILY_ID+'					
						UNION
						SELECT	tcf.FAMILY_ID AS FAMILY_ID_1,tps.PRODUCT_ID,tpf.SORT_ORDER as Sort,TPS.ATTRIBUTE_VALUE
						,ta.ATTRIBUTE_NAME
						FROM TB_CATALOG_FAMILY tcf
						,TB_PARTS_KEY tps
						,TB_PROD_FAMILY tpf 
						,TB_ATTRIBUTE ta 
						where TCF.ROOT_CATEGORY = ''''''''0'''''''' AND tcf.FAMILY_ID=tpf.FAMILY_ID  AND tcf.FLAG_RECYCLE =''''''''A'''''''' and tpf.PUBLISH2EXPORT=1
						and ta.ATTRIBUTE_ID=tps.ATTRIBUTE_ID 
						and tps.PRODUCT_ID=tpf.PRODUCT_ID
						and tps.FAMILY_ID=tpf.FAMILY_ID    AND tpf.FLAG_RECYCLE =''''''''A''''''''
						and tcf.CATALOG_ID=tps.CATALOG_ID 
						and tcf.CATALOG_ID=' + @CATALOG_ID + '
						AND TCF.CATEGORY_ID = TPS.CATEGORY_ID AND TPS.CATALOG_ID = ' + @CATALOG_ID + '
					)AS P1 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES1 + '')
					) AS PV1
				)
				
				SELECT DISTINCT  * INTO '+ @FTABMAIN + ' FROM FAMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
 				UNION
				SELECT DISTINCT * FROM FAMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID,Null as SORT,''+ @PRODUCT_SPECS_VALUES +'' FROM FAMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID,Null as SORT,''+ @PRODUCT_SPECS_VALUES +'' FROM FAMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION  
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,Null,''+ @PRODUCT_SPECS_VALUES +'' FROM 
				(				
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				ORDER BY FAMILY_ID 
				'''')				
					'';
			END		
			
	END
ELSE IF(@PRODUCT_ROWS_COUNT = 0 AND @FAMILY_ROWS_COUNT = 1)
	BEGIN
	Print(''Check 10 '')
		IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
			SET @SQLFAM = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				SELECT DISTINCT * FROM FMILY_TEMP_CTE
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL FROM
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';

				SET @SQL1 = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				SELECT DISTINCT *,NULL AS PRODUCT_ID FROM FMILY_TEMP_CTE
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL FROM
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
			;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
									TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID  AND TB_ATTRIBUTE.PUBLISH2EXPORT=1   AND TB_ATTRIBUTE.FLAG_RECYCLE =''''''A'''''' AND TB_ATTRIBUTE.ATTRIBUTE_ID 
									IN (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				
				SET @SQLFAM = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT  * FROM 
					(
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID   AND E.FLAG_RECYCLE =''''A''''
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID   AND E.FLAG_RECYCLE =''''A''''
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)
				SELECT DISTINCT * FROM TEM_CTE
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,''+ @FAMILY_SPECS_VALUES + '' FROM
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';

				SET @SQL1 = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT  * FROM 
					(
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID   AND E.FLAG_RECYCLE =''''A''''
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID   AND E.FLAG_RECYCLE =''''A''''
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)
				SELECT DISTINCT *,NULL AS PRODUCT_ID FROM TEM_CTE
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,''+ @FAMILY_SPECS_VALUES + '',NULL FROM
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							 left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  AND TA.PUBLISH2EXPORT=1  AND ta.ATTRIBUTE_TYPE IN (1,3,4,6)  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				if(@ATTRIBUTE_NAMESCOMMNULL!='''')
					   set @PRODUCT_SPECS_VALUES=@PRODUCT_SPECS_VALUES+'',''+@ATTRIBUTE_NAMESCOMMNULL
				SET @SQLFAM = '';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUUBFAMILY_ID,NULL AS SUBFAMILY_NAME INTO '+ @FTABMAINFAM + '  FROM FAMILY_TEMP_CTE'';

				SET @SQL1 = ''	EXEC(''''				
				;WITH FMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL + ' UNION ' + @CATEGORY_SQL_SUBFAMILY +'
				)
				SELECT *,NULL AS PRODUCT_ID,'' + @PRODUCT_SPECS_VALUES + ''  FROM FMILY_TEMP_CTE
				UNION
				SELECT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'' + @PRODUCT_SPECS_VALUES + '' FROM 
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				'''')
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	

 select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
 left outer join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  AND TA.PUBLISH2EXPORT=1  AND ta.ATTRIBUTE_TYPE IN (1,3,4,6)  AND ta.FLAG_RECYCLE =''A'' and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				if(@ATTRIBUTE_NAMESCOMMNULL!='''')
					   set @PRODUCT_SPECS_VALUES=@PRODUCT_SPECS_VALUES+'',''+@ATTRIBUTE_NAMESCOMMNULL

				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
									TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID   AND TB_ATTRIBUTE.PUBLISH2EXPORT=1  AND TB_ATTRIBUTE.FLAG_RECYCLE =''''''A'''''' AND TB_ATTRIBUTE.ATTRIBUTE_ID 
									IN (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				
				SET @SQLFAM = ''	EXEC('''';WITH FMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL + ' UNION ' + @CATEGORY_SQL_SUBFAMILY +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT * FROM 
					(
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL
						JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID 
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID   AND E.FLAG_RECYCLE =''''A''''
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID   AND E.FLAG_RECYCLE =''''A''''
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION ALL
						SELECT DISTINCT A.*,NULL AS STRING_VALUE,NULL AS ATTRIBUTE_NAME 
						FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT DISTINCT F.FAMILY_ID FROM TB_FAMILY_SPECS F) 
						AND A.SUBFAMILY_ID IS NULL
						UNION ALL
						SELECT A.*,NULL AS STRING_VALUE,NULL AS ATTRIBUTE_NAME 
						FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT DISTINCT F.FAMILY_ID FROM TB_FAMILY_SPECS F) 
						AND A.SUBFAMILY_ID IS NOT NULL
						
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)				
				SELECT DISTINCT *  FROM TEM_CTE
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,'' + @FAMILY_SPECS_VALUES + '' FROM 
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				'''')
					'';


				SET @SQL1 = ''	EXEC('''';WITH FMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL + ' UNION ' + @CATEGORY_SQL_SUBFAMILY +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT * FROM 
					(
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL
						JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT	DISTINCT A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							ELSE CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID 
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID   AND E.FLAG_RECYCLE =''''A''''
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT	DISTINCT A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID   AND E.FLAG_RECYCLE =''''A''''
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION ALL
						SELECT DISTINCT A.*,NULL AS STRING_VALUE,NULL AS ATTRIBUTE_NAME 
						FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT DISTINCT F.FAMILY_ID FROM TB_FAMILY_SPECS F) 
						AND A.SUBFAMILY_ID IS NULL
						UNION ALL
						SELECT A.*,NULL AS STRING_VALUE,NULL AS ATTRIBUTE_NAME 
						FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT DISTINCT F.FAMILY_ID FROM TB_FAMILY_SPECS F) 
						AND A.SUBFAMILY_ID IS NOT NULL
						
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)				
				SELECT DISTINCT *,NULL AS PRODUCT_ID,'' + @PRODUCT_SPECS_VALUES + ''  FROM TEM_CTE
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,'' + @FAMILY_SPECS_VALUES + '',NULL,'' + @PRODUCT_SPECS_VALUES + '' FROM 
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				'''')
					'';
			END
	END
	
EXEC (@SQLFAM)
exec (@SQL1)
PRINT (''2018'')

IF(@SQLFAM IS NOT NULL)
BEGIN
ALTER TABLE '+ @FTABMAINFAM + ' ADD CATEGORY_PUBLISH2WEB nvarchar(10)	
ALTER TABLE '+ @FTABMAINFAM + ' ADD CATEGORY_PUBLISH2PRINT nvarchar(10)	
ALTER TABLE '+ @FTABMAINFAM + ' ADD CATEGORY_PUBLISH2PDF nvarchar(10)	
ALTER TABLE '+ @FTABMAINFAM + ' ADD CATEGORY_PUBLISH2EXPORT nvarchar(10)	
ALTER TABLE '+ @FTABMAINFAM + ' ADD CATEGORY_PUBLISH2PORTAL nvarchar(10)	


--Working on Pdf Xpress  - Start

ALTER TABLE '+ @FTABMAINFAM + ' ADD Family_PdfTemplate nvarchar(max)
ALTER TABLE '+ @FTABMAINFAM + ' ADD Product_PdfTemplate nvarchar(max)

 --  select * into ##Cake from  '+ @FTABMAINFAM + '
EXEC(''
 if exists(select * from TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '+ @CATALOG_ID +'   and TYPE = ''''Catalog'''')
                       Begin

					
					   
 											   update  temp set temp.Family_PdfTemplate = TPH.TEMPLATE_PATH
											   from '+ @FTABMAINFAM + ' temp inner join TB_PDFXPRESS_HIERARCHY TPH on temp.FAMILY_ID = TPH.ASSIGN_TO  where TPH.TYPE = ''''Family''''

											   update  temp set temp.Product_PdfTemplate = TPH.TEMPLATE_PATH
											   from '+ @FTABMAINFAM + ' temp inner join TB_PDFXPRESS_HIERARCHY TPH on temp.FAMILY_ID = TPH.ASSIGN_TO  where TPH.TYPE = ''''Product''''

           end
		  '')

-- Working on  Pdf Xpress - End



ALTER TABLE '+ @FTABMAINFAM + ' ADD FAMILY_PUBLISH2WEB nvarchar(10)	
ALTER TABLE '+ @FTABMAINFAM + ' ADD FAMILY_PUBLISH2PRINT nvarchar(10)	
ALTER TABLE '+ @FTABMAINFAM + ' ADD FAMILY_PUBLISH2PDF nvarchar(10)	
ALTER TABLE '+ @FTABMAINFAM + ' ADD FAMILY_PUBLISH2EXPORT nvarchar(10)	
ALTER TABLE '+ @FTABMAINFAM + ' ADD FAMILY_PUBLISH2PORTAL nvarchar(10)	

print ''test1''

 UPDATE T SET CATEGORY_PUBLISH2WEB = CASE WHEN  C.PUBLISH2WEB=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PRINT = CASE WHEN  C.PUBLISH2PRINT=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PDF = CASE WHEN  C.PUBLISH2PDF=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2EXPORT = CASE WHEN  C.PUBLISH2EXPORT=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PORTAL = CASE WHEN  C.PUBLISH2PORTAL=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 
 print ''test2''
 IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = ''SUBCATID_L1'' AND TABLE_NAME LIKE '''+ @FTABMAINFAM + '%'')
 BEGIN
 DECLARE @LEASTLEVELCATEGORY NVARCHAR(100)
 
SELECT TOP 1 @LEASTLEVELCATEGORY=COLUMN_NAME FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME LIKE ''SUBCATID_L%'' AND TABLE_NAME LIKE '''+ @FTABMAINFAM + '%'' ORDER BY COLUMN_NAME DESC
print ''test3''
EXEC(''
 UPDATE T SET CATEGORY_PUBLISH2WEB = CASE WHEN  C.PUBLISH2WEB=1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.''+ @LEASTLEVELCATEGORY + ''= C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PRINT = CASE WHEN  C.PUBLISH2PRINT=1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.''+ @LEASTLEVELCATEGORY + '' = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PDF = CASE WHEN  C.PUBLISH2PDF=1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.''+ @LEASTLEVELCATEGORY + '' = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2EXPORT = CASE WHEN  C.PUBLISH2EXPORT=1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.''+ @LEASTLEVELCATEGORY + '' = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PORTAL = CASE WHEN  C.PUBLISH2PORTAL=1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAINFAM + ' T JOIN TB_CATEGORY C ON T.''+ @LEASTLEVELCATEGORY + '' = C.CATEGORY_SHORT '')

END
print ''test4''
UPDATE T SET FAMILY_PUBLISH2WEB = CASE WHEN  C.PUBLISH2WEB=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_FAMILY C ON CASE WHEN T.SUBFAMILY_ID IS NULL THEN T.FAMILY_ID ELSE T.FAMILY_ID END= C.FAMILY_ID 
UPDATE T SET FAMILY_PUBLISH2PDF = CASE WHEN  C.PUBLISH2PDF=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_FAMILY C ON CASE WHEN T.SUBFAMILY_ID IS NULL THEN T.FAMILY_ID ELSE T.FAMILY_ID END= C.FAMILY_ID 
UPDATE T SET FAMILY_PUBLISH2PRINT = CASE WHEN  C.PUBLISH2PRINT=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_FAMILY C ON CASE WHEN T.SUBFAMILY_ID IS NULL THEN T.FAMILY_ID ELSE T.FAMILY_ID END= C.FAMILY_ID 
UPDATE T SET FAMILY_PUBLISH2EXPORT = CASE WHEN  C.PUBLISH2EXPORT=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_FAMILY C ON CASE WHEN T.SUBFAMILY_ID IS NULL THEN T.FAMILY_ID ELSE T.FAMILY_ID END= C.FAMILY_ID 
UPDATE T SET FAMILY_PUBLISH2PORTAL = CASE WHEN  C.PUBLISH2PORTAL=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAINFAM + ' T JOIN TB_FAMILY C ON CASE WHEN T.SUBFAMILY_ID IS NULL THEN T.FAMILY_ID ELSE T.FAMILY_ID END= C.FAMILY_ID 

END
DECLARE @FAMILY_ATTRIBUTE_NAME Nvarchar(50)
DECLARE @FAMILY_attributeDetails Nvarchar(50)
DECLARE FAMILY_ATTRIBUTE_CURSOR CURSOR FOR
SELECT ATTRIBUTE_NAME from TB_ATTRIBUTE TA JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID = TCA.ATTRIBUTE_ID where TA.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ') and TA.ATTRIBUTE_DATATYPE like''num%'' AND TA.ATTRIBUTE_TYPE IN (7,9,11,12,13) AND TCA.CATALOG_ID ='+ @CATALOG_ID +' 

OPEN FAMILY_ATTRIBUTE_CURSOR

FETCH NEXT FROM FAMILY_ATTRIBUTE_CURSOR
INTO @FAMILY_ATTRIBUTE_NAME

WHILE @@FETCH_STATUS = 0
BEGIN



set  @FAMILY_attributeDetails = ''['' + @FAMILY_ATTRIBUTE_NAME + '']''


print @FAMILY_attributeDetails


print @FAMILY_attributeDetails
EXEC(''update '+ @FTABMAINFAM + ' SET ''+@FAMILY_attributeDetails+''= 0  where ''+@FAMILY_attributeDetails+'' is null'')


FETCH NEXT FROM FAMILY_ATTRIBUTE_CURSOR
INTO @FAMILY_ATTRIBUTE_NAME

END
CLOSE FAMILY_ATTRIBUTE_CURSOR;
DEALLOCATE FAMILY_ATTRIBUTE_CURSOR;

 if(@SQL1 is null)
 begin
 return
 end
 PRINT(''LOCAL'')

  ALTER TABLE '+ @FTABMAIN + ' ADD PUBLISH2WEB nvarchar(10)	
 ALTER TABLE '+ @FTABMAIN + ' ADD PUBLISH2PRINT nvarchar(10)	
 ALTER TABLE '+ @FTABMAIN + ' ADD PUBLISH2PDF nvarchar(10)	
 ALTER TABLE '+ @FTABMAIN + ' ADD PUBLISH2EXPORT nvarchar(10)	
 ALTER TABLE '+ @FTABMAIN + ' ADD PUBLISH2PORTAL nvarchar(10)
 	
 print ''test5''
 UPDATE T SET PUBLISH2WEB = CASE WHEN C.PUBLISH2WEB =1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON  T.PRODUCT_ID= C.PRODUCT_ID and T.FAMILY_ID= C.FAMILY_ID
UPDATE T SET PUBLISH2PDF = CASE WHEN  C.PUBLISH2PDF =1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID and T.FAMILY_ID= C.FAMILY_ID
UPDATE T SET PUBLISH2PRINT =  CASE WHEN C.PUBLISH2PRINT=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID and T.FAMILY_ID= C.FAMILY_ID
UPDATE T SET PUBLISH2EXPORT = CASE WHEN  C.PUBLISH2EXPORT=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID and T.FAMILY_ID= C.FAMILY_ID
UPDATE T SET PUBLISH2PORTAL = CASE WHEN  C.PUBLISH2PORTAL=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID and T.FAMILY_ID= C.FAMILY_ID

 UPDATE T SET PUBLISH2WEB = CASE WHEN C.PUBLISH2WEB =1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON  T.PRODUCT_ID= C.PRODUCT_ID and T.SUBFAMILY_ID= C.FAMILY_ID
UPDATE T SET PUBLISH2PDF = CASE WHEN  C.PUBLISH2PDF =1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID and T.SUBFAMILY_ID= C.FAMILY_ID
UPDATE T SET PUBLISH2PRINT =  CASE WHEN C.PUBLISH2PRINT=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID and T.SUBFAMILY_ID= C.FAMILY_ID
UPDATE T SET PUBLISH2EXPORT = CASE WHEN  C.PUBLISH2EXPORT=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID and T.SUBFAMILY_ID= C.FAMILY_ID
UPDATE T SET PUBLISH2PORTAL = CASE WHEN  C.PUBLISH2PORTAL=1 THEN ''Y'' ELSE ''N'' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID and T.SUBFAMILY_ID= C.FAMILY_ID
 UPDATE T SET CATEGORY_ID = C.CATEGORY_SHORT FROM '+ @FTABMAIN + ' T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_ID 
IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = ''SUBCATID_L1'' AND TABLE_NAME LIKE '''+ @FTABMAIN + ''')
 BEGIN
  UPDATE T SET SUBCATID_L1 = C.CATEGORY_SHORT FROM '+ @FTABMAIN + ' T JOIN TB_CATEGORY C ON T.SUBCATID_L1 = C.CATEGORY_ID 
END
IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = ''SUBCATID_L2'' AND TABLE_NAME LIKE '''+ @FTABMAIN + ''')
 BEGIN
  UPDATE T SET SUBCATID_L2 = C.CATEGORY_SHORT FROM '+ @FTABMAIN + ' T JOIN TB_CATEGORY C ON T.SUBCATID_L2 = C.CATEGORY_ID 
END
IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = ''SUBCATID_L3'' AND TABLE_NAME LIKE '''+ @FTABMAIN + ''')
 BEGIN
  UPDATE T SET SUBCATID_L3 = C.CATEGORY_SHORT FROM '+ @FTABMAIN + ' T JOIN TB_CATEGORY C ON T.SUBCATID_L3 = C.CATEGORY_ID 
END
IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = ''SUBCATID_L4'' AND TABLE_NAME LIKE '''+ @FTABMAIN + ''')
 BEGIN
  UPDATE T SET SUBCATID_L4 = C.CATEGORY_SHORT FROM '+ @FTABMAIN + ' T JOIN TB_CATEGORY C ON T.SUBCATID_L4 = C.CATEGORY_ID 
END
  
PRINT(''CHECK 11 '')

DECLARE @ATTRIBUTE_NAME Nvarchar(50)
DECLARE @attributeDetails Nvarchar(50)
DECLARE ATTRIBUTE_CURSOR CURSOR FOR
SELECT ATTRIBUTE_NAME from TB_ATTRIBUTE TA JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID = TCA.ATTRIBUTE_ID where TA.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ') and TA.ATTRIBUTE_DATATYPE like''num%'' AND TA.ATTRIBUTE_TYPE IN (1,3,4,6) AND TCA.CATALOG_ID ='+ @CATALOG_ID +'

OPEN ATTRIBUTE_CURSOR

FETCH NEXT FROM ATTRIBUTE_CURSOR
INTO @ATTRIBUTE_NAME

WHILE @@FETCH_STATUS = 0
BEGIN



set  @attributeDetails = ''['' + @ATTRIBUTE_NAME + '']''






EXEC(''update '+ @FTABMAIN + ' SET ''+@attributeDetails+''= 0  where ''+@attributeDetails+'' is null'')


FETCH NEXT FROM ATTRIBUTE_CURSOR
INTO @ATTRIBUTE_NAME

END
CLOSE ATTRIBUTE_CURSOR;
DEALLOCATE ATTRIBUTE_CURSOR;

 SELECT DISTINCT * FROM '+ @FTABMAIN + ' WHERE '+@AttributeIteName+' IS NOT NULL AND PUBLISH2EXPORT=''Y'' AND (CASE WHEN SUBFAMILY_ID IS NULL THEN FAMILY_ID ELSE SUBFAMILY_ID END) IN (SELECT (CASE WHEN SUBFAMILY_ID IS NULL THEN FAMILY_ID ELSE SUBFAMILY_ID END) AS FAMILY_ID FROM '+@FTABMAINFAM+')
 if exists(select * from tb_subproduct)
 Begin
ALTER TABLE '+ @FTABMAIN + '
ADD CATALOG_ITEM_NO VARCHAR(MAX),
   SUBPRODUCT_ID INT, 
   SUBCATALOG_ITEM_NO VARCHAR(MAX) 
PRINT(''CLUB'')
   

UPDATE T SET CATALOG_ITEM_NO = TPS.STRING_VALUE from  '+ @FTABMAIN + ' T join TB_PROD_SPECS tps on tps.PRODUCT_ID = T.Product_id AND tps.ATTRIBUTE_ID=1

Insert into  '+ @FTABMAIN + ' (CATALOG_ID,CATALOG_NAME,CATEGORY_ID, CATEGORY_NAME, FAMILY_ID, FAMILY_NAME, SUBFAMILY_ID, SUBFAMILY_NAME, PRODUCT_ID , CATALOG_ITEM_NO ,
                                SUBCATALOG_ITEM_NO , SUBPRODUCT_ID )
                                SELECT T.CATALOG_ID,T.CATALOG_NAME,T.CATEGORY_ID, T.CATEGORY_NAME, T.FAMILY_ID, T.FAMILY_NAME,  T.SUBFAMILY_ID, T.SUBFAMILY_NAME, T.PRODUCT_ID , T.CATALOG_ITEM_NO ,
                                T.SUBCATALOG_ITEM_NO , Ts.SUBPRODUCT_ID FROM '+ @FTABMAIN + ' T left outer join TB_SUBPRODUCT TS on ts.PRODUCT_ID = T.PRODUCT_ID 
								AND ts.CATALOG_ID = T.CATALOG_ID AND TS.FLAG_RECYCLE =''A''
PRINT(''ONCE'')
UPDATE T SET SUBCATALOG_ITEM_NO = TPS.STRING_VALUE from  '+ @FTABMAIN + ' T join TB_PROD_SPECS tps on tps.PRODUCT_ID = T.SUBPRODUCT_ID AND tps.ATTRIBUTE_ID=1

if(@ATTRIBUTE_NAMESSUB is not null OR @ATTRIBUTE_NAMESSUB <> '''')
 begin
 
 --set @ATTRIBUTE_NAMESSUB=replace(@ATTRIBUTE_NAMESSUB,''[ITEM#]'','''')
 PRINT(''ONCE2018'')

EXEC (''SELECT DISTINCT * into [##TEMPRESULTSUBPRODUCTS'+@SESSID+'] FROM (select DISTINCT T.CATALOG_ID,T.CATALOG_NAME,T.CATEGORY_ID, T.CATEGORY_NAME, T.FAMILY_ID, T.FAMILY_NAME,  T.SUBFAMILY_ID, T.SUBFAMILY_NAME, T.PRODUCT_ID , T.CATALOG_ITEM_NO ,
                             T.SUBPRODUCT_ID, T.SUBCATALOG_ITEM_NO ,CASE WHEN TS.PUBLISH2WEB =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2WEB,CASE WHEN  TS.PUBLISH2PRINT =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PRINT,CASE WHEN  TS.PUBLISH2PDF =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PDF,CASE WHEN  TS.PUBLISH2EXPORT =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2EXPORT,CASE WHEN TS.PUBLISH2PORTAL =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PORTAL,
                             CASE 
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,TPS2.NUMERIC_VALUE)
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''%Date%''''
							THEN CONVERT(VARCHAR,TPS2.STRING_VALUE)
						ELSE
						   TPS2.STRING_VALUE  + CASE WHEN TPS2.OBJECT_NAME IS NOT NULL AND TPS2.OBJECT_NAME <> '''''''' THEN ''''[Image_Name:''''+ TPS2.OBJECT_NAME +'''']'''' ELSE '''''''' END
						END AS STRING_VALUE,
                              TA.ATTRIBUTE_NAME	 from '+ @FTABMAIN + ' T 
                            JOIN  TB_PROD_SPECS TPS2 on T.SUBPRODUCT_ID = TPS2.PRODUCT_ID --AND TPS2.ATTRIBUTE_ID <> 1
							left outer JOIN TB_SUBPRODUCT TS ON TPS2.PRODUCT_ID=TS.SUBPRODUCT_ID AND TS.PUBLISH2EXPORT=1
                            JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS2.ATTRIBUTE_ID   AND TA.FLAG_RECYCLE =''''A''''  --AND TPS2.ATTRIBUTE_ID <> 1
                            UNION ALL
                            select DISTINCT T.CATALOG_ID,T.CATALOG_NAME, T.CATEGORY_ID, T.CATEGORY_NAME, T.FAMILY_ID, T.FAMILY_NAME,  T.SUBFAMILY_ID, T.SUBFAMILY_NAME,	T.PRODUCT_ID , T.CATALOG_ITEM_NO ,
                             T.SUBPRODUCT_ID, T.SUBCATALOG_ITEM_NO ,CASE WHEN TS.PUBLISH2WEB =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2WEB,CASE WHEN  TS.PUBLISH2PRINT =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PRINT,CASE WHEN  TS.PUBLISH2PDF =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PDF,CASE WHEN  TS.PUBLISH2EXPORT =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2EXPORT,CASE WHEN TS.PUBLISH2PORTAL =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PORTAL,
                            TPS2.ATTRIBUTE_VALUE   AS STRING_VALUE,
                              TA.ATTRIBUTE_NAME	 from '+ @FTABMAIN + ' T 
                             JOIN  TB_CATALOG_FAMILY TCF on TCF.CATALOG_ID =T.CATALOG_ID AND TCF.FAMILY_ID = T.FAMILY_ID   AND TCF.FLAG_RECYCLE =''''A'''' AND TCF.CATALOG_ID = T.CATALOG_ID
                             JOIN  TB_SUBPRODUCT_KEY TPS2 on T.SUBPRODUCT_ID = TPS2.SUBPRODUCT_ID AND T.PRODUCT_ID = TPS2.PRODUCT_ID   AND TPS2.FAMILY_ID = TCF.FAMILY_ID   
							 left outer JOIN TB_SUBPRODUCT TS ON TPS2.PRODUCT_ID=TS.SUBPRODUCT_ID AND TS.PUBLISH2EXPORT=1
                             AND TPS2.ATTRIBUTE_VALUE IS NOT NULL
							  AND TPS2.CATALOG_ID = T.CATALOG_ID  AND TPS2.CATEGORY_ID = T.CATEGORY_ID
							 JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS2.ATTRIBUTE_ID --AND  TA.ATTRIBUTE_ID <> 1   
                            
                             ) AS S
                             
                             PIVOT      
                       (        MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMESSUB + '')) AS PV1
                       
                       
                       '')  

END
ELSE
BEGIN
BEGIN

exec (''SELECT DISTINCT * into [##TEMPRESULTSUBPRODUCTS'+@SESSID+']  FROM (select DISTINCT T.CATALOG_ID,T.CATALOG_NAME,T.CATEGORY_ID, T.CATEGORY_NAME, T.FAMILY_ID, T.FAMILY_NAME,  T.SUBFAMILY_ID, T.SUBFAMILY_NAME, T.PRODUCT_ID , T.CATALOG_ITEM_NO ,
                             T.SUBPRODUCT_ID, T.SUBCATALOG_ITEM_NO ,CASE WHEN TS.PUBLISH2WEB =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2WEB,CASE WHEN  TS.PUBLISH2PRINT =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PRINT,CASE WHEN  TS.PUBLISH2PDF =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PDF,CASE WHEN  TS.PUBLISH2EXPORT =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2EXPORT,CASE WHEN TS.PUBLISH2PORTAL =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PORTAL from '+ @FTABMAIN + ' T 
                            JOIN  TB_PROD_SPECS TPS2 on T.SUBPRODUCT_ID = TPS2.PRODUCT_ID --AND TPS2.ATTRIBUTE_ID <> 1
							left outer JOIN TB_SUBPRODUCT TS ON TPS2.PRODUCT_ID=TS.SUBPRODUCT_ID AND TS.PUBLISH2EXPORT=1
                            JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS2.ATTRIBUTE_ID  AND TA.FLAG_RECYCLE =''''A''''   AND T.FAMILY_ID IN (SELECT FAMILY_ID FROM '+ @FTABMAIN + ' WHERE  '+@AttributeIteName+' IS NOT NULL AND PUBLISH2EXPORT=1)
                              
    
                            UNION ALL
                            
                            select DISTINCT T.CATALOG_ID,T.CATALOG_NAME, T.CATEGORY_ID, T.CATEGORY_NAME, T.FAMILY_ID, T.FAMILY_NAME,  T.SUBFAMILY_ID, T.SUBFAMILY_NAME,	T.PRODUCT_ID , T.CATALOG_ITEM_NO ,
                             T.SUBPRODUCT_ID, T.SUBCATALOG_ITEM_NO, CASE WHEN TS.PUBLISH2WEB =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2WEB,CASE WHEN  TS.PUBLISH2PRINT =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PRINT,CASE WHEN  TS.PUBLISH2PDF =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PDF,CASE WHEN  TS.PUBLISH2EXPORT =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2EXPORT,CASE WHEN TS.PUBLISH2PORTAL =1 THEN 1 ELSE 0 END AS SUBPRODUCT_PUBLISH2PORTAL  from '+ @FTABMAIN + ' T 
                             JOIN  TB_CATALOG_FAMILY TCF on TCF.CATALOG_ID =T.CATALOG_ID AND TCF.FAMILY_ID = T.FAMILY_ID   AND TCF.FLAG_RECYCLE =''''A''''   
                             JOIN  TB_SUBPRODUCT_KEY TPS2 on T.SUBPRODUCT_ID = TPS2.SUBPRODUCT_ID AND T.PRODUCT_ID = TPS2.PRODUCT_ID   AND TPS2.FAMILY_ID = TCF.FAMILY_ID   AND TPS2.ATTRIBUTE_VALUE IS NOT NULL
							 left outer JOIN TB_SUBPRODUCT TS ON TPS2.SUBPRODUCT_ID=TS.SUBPRODUCT_ID AND TS.PUBLISH2EXPORT=1
							 AND TPS2.CATALOG_ID = T.CATALOG_ID AND TPS2.CATEGORY_ID = T.CATEGORY_ID  
							 JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS2.ATTRIBUTE_ID --AND  TA.ATTRIBUTE_ID <> 1   
                             ) AS S
                       '')  
END
END
EXEC(''UPDATE T SET T.CATEGORY_NAME=TC.CATEGORY_NAME FROM [##TEMPRESULTSUBPRODUCTS'+@SESSID+'] T JOIN TB_CATALOG_FAMILY TCF 
ON TCF.FAMILY_ID = T.FAMILY_ID   AND TCF.FLAG_RECYCLE =''''A''''
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TCF.CATEGORY_ID   AND TC.FLAG_RECYCLE =''''A'''' '')

EXEC(''SELECT * FROM [##TEMPRESULTSUBPRODUCTS'+@SESSID+'] where (case when SUBFAMILY_ID IS NULL THEN FAMILY_ID ELSE SUBFAMILY_ID END)  IN (SELECT case when SUBFAMILY_ID IS NULL THEN FAMILY_ID ELSE SUBFAMILY_ID END FROM '+ @FTABMAIN + ' WHERE  '+@AttributeIteName+' IS NOT NULL AND PUBLISH2EXPORT=''''Y'''')'')
end
PRINT (''LATEST'')

IF OBJECT_ID(''tempdb..##TEMPRESULTSUBPRODUCTS'+@SESSID+''') IS NULL
BEGIN
CREATE TABLE #TEMPSUB(CATALOG_ID INT PRIMARY KEY,
CATALOG_NAME VARCHAR(MAX),
CATEGORY_ID INT, 
CATEGORY_NAME VARCHAR(MAX),
 FAMILY_ID INT,
  FAMILY_NAME VARCHAR(MAX), 
   SUBFAMILY_ID INT,
    SUBFAMILY_NAME VARCHAR(MAX),
	 PRODUCT_ID INT, 
	 CATALOG_ITEM_NO VARCHAR(MAX),
     SUBPRODUCT_ID INT, 
	 SUBCATALOG_ITEM_NO VARCHAR(MAX),
	 SUBPRODUCT_PUBLISH2WEB BIT,
	  SUBPRODUCT_PUBLISH2PRINT BIT,
	   SUBPRODUCT_PUBLISH2PDF BIT,	
	   SUBPRODUCT_PUBLISH2EXPORT BIT,	 
	   SUBPRODUCT_PUBLISH2PORTAL BIT
	
	  )
	 SELECT * FROM #TEMPSUB

	 DROP TABLE #TEMPSUB
END
	BEGIN
	Declare @FAMATTR nvarchar(max)=''''
	SELECT  @FAMATTR = (SELECT  SUBSTRING((SELECT '',['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM (select col.name as ATTRIBUTE_NAME from tempdb.sys.columns col join tempdb.sys.tables tab on col.object_id=tab.object_id and tab.name =''##TEMPTABLEMAIN'+@SESSID+'FAM'') as tt FOR XML PATH('''')),2,200000))

	--SELECT  @FAMATTR = (SELECT  SUBSTRING((SELECT '',['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM (select col.name as ATTRIBUTE_NAME from tempdb.sys.columns col join tempdb.sys.tables tab on col.object_id=tab.object_id and tab.name =''##TEMPTABLEMAIN'+@SESSID+'FAM'') as tt FOR XML PATH('''')),2,200000))
	
--PRINT ''OK''
--EXEC(''select * from [##TEMPTABLEMAIN'+@SESSID+'FAM]'')
	
SELECT @FAMATTR = (SELECT REPLACE(@FAMATTR, '',[],'', '',''))
print @FAMATTR
		exec(''select ''+@FAMATTR+'' from (
		select * from (
		select *,Row_number() over(partition by FAMILY_ID order by family_id) rno from [##TEMPTABLEMAIN'+@SESSID+'FAM] where family_name is not null AND FAMILY_ID IN (SELECT FAMILY_ID FROM '+ @FTABMAIN + ')
		) r where rno>1
		union
				select * from (
		select *,Row_number() over(partition by FAMILY_ID order by family_id) rno from [##TEMPTABLEMAIN'+@SESSID+'FAM] where family_name is not null AND FAMILY_ID IN (SELECT FAMILY_ID FROM '+ @FTABMAIN + ')
		) r where rno=1) as tt WHERE CATEGORY_PUBLISH2EXPORT=''''Y'''' AND FAMILY_PUBLISH2EXPORT=''''Y''''
		'')	
			 
	END
	 '
	 
exec (@SQL_FAMILY)
EXEC(@ParentCategoryAttribute);
EXEC(@SubCategoryAttribute);
EXEC (@ALLCATEGORYATTRIBUTE)

-- Pdf Xpress - Start
ALTER TABLE ##ResultCategoryAttribute ADD Category_PdfTemplate nvarchar(max)


				 if exists(select * from TB_PDFXPRESS_HIERARCHY where CATALOG_ID = @CATALOG_ID and TYPE = 'Catalog')
                       Begin

					 
update  temp set temp.Category_PdfTemplate = TPH.TEMPLATE_PATH 
from ##ResultCategoryAttribute temp inner join TB_PDFXPRESS_HIERARCHY TPH on temp.CATALOG_ID = TPH.CATALOG_ID   
inner join TB_CATEGORY TC on TC.CATEGORY_ID = TPH.ASSIGN_TO 
where (temp.CATEGORY_ID = TC.CATEGORY_SHORT) 

					   end

-- Pdf Xpress - End


IF(@ALLCATEGORYATTRIBUTE IS NOT NULL)
BEGIN
ALTER TABLE ##ResultCategoryAttribute ADD CATEGORY_PUBLISH2WEB nvarchar(10)																				  
ALTER TABLE ##ResultCategoryAttribute ADD CATEGORY_PUBLISH2PRINT nvarchar(10)	
ALTER TABLE ##ResultCategoryAttribute ADD CATEGORY_PUBLISH2PDF nvarchar(10)	
ALTER TABLE ##ResultCategoryAttribute ADD CATEGORY_PUBLISH2EXPORT nvarchar(10)	
ALTER TABLE ##ResultCategoryAttribute ADD CATEGORY_PUBLISH2PORTAL nvarchar(10)	
print '1'
 UPDATE T SET CATEGORY_PUBLISH2WEB = CASE WHEN  C.PUBLISH2WEB=1 THEN 'Y' ELSE 'N' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PRINT = CASE WHEN  C.PUBLISH2PRINT=1 THEN 'Y' ELSE 'N' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PDF = CASE WHEN  C.PUBLISH2PDF=1 THEN 'Y' ELSE 'N' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2EXPORT = CASE WHEN  C.PUBLISH2EXPORT=1 THEN 'Y' ELSE 'N' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PORTAL = CASE WHEN  C.PUBLISH2PORTAL=1 THEN 'Y' ELSE 'N' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.CATEGORY_ID = C.CATEGORY_SHORT 

  IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'SUBCATID_L1' AND TABLE_NAME = '##ResultCategoryAttribute' )
 BEGIN
 DECLARE @LEASTLEVELCATEGORY NVARCHAR(100)
 
SELECT TOP 1 @LEASTLEVELCATEGORY=COLUMN_NAME FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME LIKE 'SUBCATID_L%' AND TABLE_NAME = '##ResultCategoryAttribute' ORDER BY COLUMN_NAME DESC

EXEC('UPDATE T SET CATEGORY_PUBLISH2WEB = CASE WHEN  C.PUBLISH2WEB=1 THEN ''Y'' ELSE ''N'' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.'+ @LEASTLEVELCATEGORY + '= C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PRINT = CASE WHEN  C.PUBLISH2PRINT=1 THEN ''Y'' ELSE ''N'' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.'+ @LEASTLEVELCATEGORY + ' = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PDF = CASE WHEN  C.PUBLISH2PDF=1 THEN ''Y'' ELSE ''N'' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.'+ @LEASTLEVELCATEGORY + ' = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2EXPORT = CASE WHEN  C.PUBLISH2EXPORT=1 THEN ''Y'' ELSE ''N'' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.'+ @LEASTLEVELCATEGORY + ' = C.CATEGORY_SHORT 
 
 UPDATE T SET CATEGORY_PUBLISH2PORTAL = CASE WHEN  C.PUBLISH2PORTAL=1 THEN ''Y'' ELSE ''N'' END FROM ##ResultCategoryAttribute T JOIN TB_CATEGORY C ON T.'+ @LEASTLEVELCATEGORY + ' = C.CATEGORY_SHORT ')
 END
 END
 select * from ##ResultCategoryAttribute

