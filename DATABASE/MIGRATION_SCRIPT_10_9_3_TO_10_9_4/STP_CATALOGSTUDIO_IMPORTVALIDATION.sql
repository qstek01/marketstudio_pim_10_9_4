
ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO_IMPORTVALIDATION](@SESSIONID VARCHAR(100)='5038079f-7682-44f0-8e8f-869685c63ad0')
AS BEGIN
 if not exists(select 1 from tempdb.sys.tables t  join tempdb.sys.columns c on t.object_id=c.object_id where t.name like '%validationResult'+@SESSIONID+'%' and c.name='ROWID')
 Begin
  
  exec('  alter table [##validationResult'+@SESSIONID+'] add ROWID int identity')
 End

 IF EXISTS(select 1 from tempdb.sys.tables t  join tempdb.sys.columns c on t.object_id=c.object_id where t.name like '%validationResult'+@SESSIONID+'%' AND C.name like 'PRODUCT_PUBLISH2%')
	BEGIN
	print '1'
	EXEC('update [##validationResult'+@SESSIONID+'] SET 
PRODUCT_PUBLISH2PDF=CASE WHEN PRODUCT_PUBLISH2PDF=''Y'' THEN ''1'' Else ''0'' end,
PRODUCT_PUBLISH2PRINT=CASE WHEN PRODUCT_PUBLISH2PRINT=''Y'' THEN ''1'' Else ''0'' end,
PRODUCT_PUBLISH2WEB=CASE WHEN PRODUCT_PUBLISH2WEB=''Y'' THEN ''1'' Else ''0'' end
 from [##validationResult'+@SESSIONID+']')
 END
 --PRODUCT_PUBLISH2EXPORT=CASE WHEN PRODUCT_PUBLISH2EXPORT=''Y'' THEN ''1'' Else ''0'' end 
 --PRODUCT_PUBLISH2PORTAL=CASE WHEN PRODUCT_PUBLISH2PORTAL=''Y'' THEN ''1'' Else ''0'' end,
--exec('select * into ##attrTemp from [##AttributeTemp'+@SESSIONID+']')
 	 declare @val varchar(max)
 -- select @val=COALESCE(@val + ',[' + Replace(attribute_name,'.','#')+']',+'['+Replace(attribute_name,'.','#')+']')  from ##attrTemp where attribute_type<>0 and attribute_name<>'' and attribute_name not in ('CATALOG_ID',
 --'CATALOG_NAME','CATEGORY_ID','CATEGORY_NAME','FAMILY_ID','FAMILY_NAME','SUBFAMILY_ID','SUBFAMILY_NAME','PRODUCT_ID','CATALOG_ITEM_NO')
 --print @val
 
 declare @CATEGORYLEVEL varchar(max)
 select @CATEGORYLEVEL=COALESCE(@CATEGORYLEVEL+',['+c.name+']',+'['+c.name+']') from tempdb.sys.tables t  join tempdb.sys.columns c on t.object_id=c.object_id where t.name like '%validationResult'+@SESSIONID+'%' and (c.name like 'Subcatname%' or c.name='CATEGORY_NAME')
 select @val=COALESCE(@val + ',[' + name+']',+'['+name+']') 
 from (
 select distinct name  from tempdb.sys.columns col join TB_ATTRIBUTE ta on col.name=ta.ATTRIBUTE_NAME
 where object_id=OBJECT_ID(N'tempdb..##validationResult'+@SESSIONID+'') and name not in ('CATALOG_ID', 'CATALOG_NAME','CATEGORY_ID','CATEGORY_NAME','FAMILY_ID',
 'FAMILY_NAME','SUBFAMILY_ID','SUBFAMILY_NAME','PRODUCT_ID','CATALOG_ITEM_NO','ITEM#')) as temp
 --drop table ##attrTemp
 select @val=COALESCE(@val + ',[' + name+']',+'['+name+']') 
 from (
 select distinct name  from tempdb.sys.columns col where object_id=OBJECT_ID(N'tempdb..##validationResult'+@SESSIONID+'') and name like 'PRODUCT_PUBLISH2%'
 ) as temp
 print @val
 if exists(select 1  from sys.tables where name like 'FinalvalidationResult'+@SESSIONID+'%')
 begin
 exec('Drop table [FinalvalidationResult'+@SESSIONID+']')
 End

 if((select  Count(name)  from sys.tables where name in('Missingcolumns'+@SESSIONID+'','validatedresult'+@SESSIONID+'','Picklistlog'+@SESSIONID+''))=3)
 begin
	print '1'
	if(@val is null )
	Begin
	exec(' select * into #validateResult from (select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end 
	as DUPLICATE_ITEM_NO, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case 
	when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  
	case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No order by 
   CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
       ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null 


	   select distinct *  into [FinalvalidationResult' + @SESSIONID + '] from (

  select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee

	union
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME, CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME, CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [validatedresult'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	else
	Begin
	exec(' select * into #validateResult from (select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end 
	as DUPLICATE_ITEM_NO, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case 
	when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  
	case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No order by 
   CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
       ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null 


	   select distinct *  into [FinalvalidationResult' + @SESSIONID + '] from (

  select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee

	union
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME, CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME, CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [validatedresult'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else  if((select  Count(name)  from sys.tables where name in('Missingcolumns'+@SESSIONID+'','validatedresult'+@SESSIONID+''))=2)
 begin
	print '2'
	if(@val is null )
	Begin
	exec(' select * into #validateResult from (select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO, 
	
	case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%''  then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No order by  
  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  
  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
      ) as validate where  DATATYPE_VALIDATION is not null or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null or NEW_CATALOG_ITEM_NO is not null

  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (

  select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [validatedresult'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	else 
	Begin
	exec(' select * into #validateResult from (select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO, 
	
	case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%''  then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No order by  
  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  
  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
      ) as validate where  DATATYPE_VALIDATION is not null or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null or NEW_CATALOG_ITEM_NO is not null

  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (

  select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [validatedresult'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else  if ((select Count(name)  from sys.tables where name in('Missingcolumns'+@SESSIONID+'','Picklistlog'+@SESSIONID+''))=2)
 begin
	print '3'
	if(@val is null )
	Begin
	exec('select * into #validateResult from( select distinct temp.*,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO, case when PICKLIST_VALUE is not null
	 then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%''  then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null  or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null 


	select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt
	print ''1111''
	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	Else
	Begin
	exec('select * into #validateResult from( select distinct temp.*,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO, case when PICKLIST_VALUE is not null
	 then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null  or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null 


	select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt
	print ''1111''
	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else if((select  Count(name)  from sys.tables where name in('validatedresult'+@SESSIONID+'','Picklistlog'+@SESSIONID+''))=2)
 begin
	print '4'
	if(@val is null )
	Begin
exec('select * into #validateResult from ( select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO
, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%''  then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO

 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null  or DUPLICATE_ITEM_NO is not null

   select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [validatedresult'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	else
	Begin
	exec('select * into #validateResult from ( select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO
, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO

 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null  or DUPLICATE_ITEM_NO is not null

   select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [validatedresult'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else if((select  Count(name)  from sys.tables where name in('validatedresult'+@SESSIONID+''))=1)
 begin
	print '5'
	if(@val is null )
	Begin
exec('select * into #validateResult from ( select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,case when rowno=2 then ''Duplicate Records'' end as 
DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null  or DUPLICATE_ITEM_NO is not null
    
   select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME ,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME ,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 

	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [validatedresult'+@SESSIONID+']

	drop table [##validationResult'+@SESSIONID+']
	')
	end
	else 
	Begin
	exec('select * into #validateResult from ( select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,case when rowno=2 then ''Duplicate Records'' end as 
DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null  or DUPLICATE_ITEM_NO is not null
    
   select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME ,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME ,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 

	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [validatedresult'+@SESSIONID+']

	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END

	else if((select  Count(name)  from sys.tables where name in('Picklistlog'+@SESSIONID+''))=1)
 begin
	print '7'
	if(@val is null )
	Begin
exec('select * into #validateResult from ( select distinct temp.*,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO
, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO

  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null   or DUPLICATE_ITEM_NO is not null

    select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	Else
	Begin
	exec('select * into #validateResult from ( select distinct temp.*,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO
, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO

  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null   or DUPLICATE_ITEM_NO is not null

    select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else  if ((select Count(name)  from sys.tables where name in('Missingcolumns'+@SESSIONID+''))=1)
 begin
	print '8'
	print @val
	if(@val is null )
	Begin
	exec('select * into #validateResult from( select distinct temp.*,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where   DATATYPE_VALIDATION is not null or MISSING_COLUMNS is not null or DUPLICATE_ITEM_NO is not null


	  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-'' ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	

	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	
drop table [Missingcolumns'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	end 

	else 
	Begin
	exec('select * into #validateResult from( select distinct temp.*,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where   DATATYPE_VALIDATION is not null or MISSING_COLUMNS is not null or DUPLICATE_ITEM_NO is not null


	  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-'' ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	

	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	
drop table [Missingcolumns'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END

	else
	Begin
	print '11'
	if(@val is null )
	Begin
	exec('select * into #validateResult from( select distinct temp.*,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case 
	when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where  DATATYPE_VALIDATION is not null or DUPLICATE_ITEM_NO is not null 


	  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	

	drop table [##validationResult'+@SESSIONID+']
	')
	End
	Else
	Begin
	exec('select * into #validateResult from( select distinct temp.*,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case 
	when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name and attr.FLAG_RECYCLE = ''A''
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where  DATATYPE_VALIDATION is not null or DUPLICATE_ITEM_NO is not null 


	  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	

	drop table [##validationResult'+@SESSIONID+']
	')
	end
	End

	 if exists(select 1  from sys.tables where name like 'FinalvalidationResult'+@SESSIONID+'%')
 begin
 	print '9'
 exec('select * from [FinalvalidationResult'+@SESSIONID+']')
 End
 declare @tempName varchar(100)
declare temptable cursor 
static for
select name from sys.tables where replace(datepart(hh, modify_date)-datepart(hh, GETDATE()),'-','')>2 and (name like 'validatedresult%'  or name like 'errorlog%' or name like 'Missingcolumns%' or name like 'Picklistlog%' or name like 'tempresult%' or name like 'tempresultsub%' or name like 'FinalvalidationResult%')
open temptable
if(@@cursor_rows>0)
Begin 
fetch next from temptable into @tempName
while(@@fetch_status=0)
begin

exec('drop table ['+ @tempName+']')

fetch next from temptable into @tempName
End

End
close temptable
deallocate temptable

END

GO




ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO_UPDATEIDCOLUMNS]
(@SESSIONID NVARCHAR(100)='aa36983d-f5e7-4909-a47f-96b4c27ef4d6')
AS
BEGIN
DECLARE @VAL VARCHAR(MAX)
DECLARE @VAL1 VARCHAR(MAX)

SELECT @VAL1= Count(COL.name) FROM TEMPDB.SYS.COLUMNS COL JOIN TEMPDB.SYS.OBJECTS TAB ON COL.OBJECT_ID=TAB.OBJECT_ID AND TAB.NAME LIKE '##IMPORTTEMP'+@SESSIONID+'%' WHERE COL.NAME LIKE 'CATALOG_ITEM_NO%' 
SELECT @VAL=COALESCE(@VAL + ',[COL' + CAST(COLUMN_ID AS VARCHAR(50))+']',+'[COL'+CAST(COLUMN_ID AS VARCHAR(50))+']') FROM TEMPDB.SYS.COLUMNS COL JOIN TEMPDB.SYS.OBJECTS TAB ON COL.OBJECT_ID=TAB.OBJECT_ID AND TAB.NAME LIKE '##IMPORTTEMP'+@SESSIONID+'%' WHERE COL.NAME LIKE 'SUBCATID%' 
PRINT @VAL
Print @VAL1
EXEC
('
IF((SELECT COUNT(*) FROM [##IMPORTTEMP'+@SESSIONID+'] WHERE CATEGORY_ID IS NULL OR CATEGORY_ID='''')>0)
BEGIN
UPDATE TEMP SET TEMP.CATEGORY_ID=TC.CATEGORY_SHORT
FROM [##IMPORTTEMP'+@SESSIONID+'] TEMP JOIN TB_CATEGORY TC ON  TEMP.CATEGORY_NAME=TC.CATEGORY_NAME AND PARENT_CATEGORY=''0''  AND TC.FLAG_RECYCLE=''A'' 
JOIN TB_CATALOG_SECTIONS TCS ON TC.CATEGORY_ID=TCS.CATEGORY_ID AND TCS.CATALOG_ID=TEMP.CATALOG_ID AND TCS.FLAG_RECYCLE=''A''

UPDATE TEMP SET TEMP.FAMILY_ID=TF.FAMILY_ID  FROM [##IMPORTTEMP'+@SESSIONID+'] TEMP JOIN TB_FAMILY TF ON TEMP.FAMILY_NAME=TF.FAMILY_NAME AND TEMP.CATEGORY_ID IS NOT NULL AND TEMP.CATEGORY_ID<>''''
JOIN TB_CATEGORY TC ON  TEMP.CATEGORY_ID=TC.CATEGORY_SHORT  
JOIN TB_CATALOG_FAMILY TCS ON TCS.CATEGORY_ID=TC.CATEGORY_ID  AND TCS.CATALOG_ID=TEMP.CATALOG_ID AND TCS.FAMILY_ID=TF.FAMILY_ID AND TCS.FLAG_RECYCLE=''A''

IF((SELECT COUNT(COL.NAME) FROM TEMPDB.SYS.COLUMNS COL JOIN TEMPDB.SYS.OBJECTS TAB ON COL.OBJECT_ID=TAB.OBJECT_ID AND TAB.NAME=''##IMPORTTEMP'+@SESSIONID+''' WHERE COL.NAME=''SUBFAMILY_NAME'')>0)
BEGIN 
 UPDATE TEMP SET TEMP.SUBFAMILY_ID=TF.FAMILY_ID  FROM [##IMPORTTEMP'+@SESSIONID+'] TEMP JOIN TB_FAMILY TF ON TEMP.SUBFAMILY_NAME=TF.FAMILY_NAME AND TF.PARENT_FAMILY_ID=TEMP.FAMILY_ID AND TEMP.FAMILY_ID IS NOT NULL
 JOIN TB_CATEGORY TC ON  TEMP.CATEGORY_ID=TC.CATEGORY_SHORT
JOIN TB_CATALOG_FAMILY TCS ON TCS.CATEGORY_ID=TC.CATEGORY_ID AND TCS.CATALOG_ID=TEMP.CATALOG_ID AND TCS.FAMILY_ID=TF.FAMILY_ID AND TCS.FLAG_RECYCLE=''A''
END
END

')
if(@VAL1 = 1)
BEGIN
print'a'
EXEC('
IF((SELECT COUNT(*) FROM [##IMPORTTEMP'+@SESSIONID+'] WHERE PRODUCT_ID IS NULL OR PRODUCT_ID='''')>0)
BEGIN
;With CTE AS (
Select Distinct TPS.PRODUCT_ID,TPID.FAMILY_ID ,TPS.STRING_VALUE From [##IMPORTTEMP'+@SESSIONID+'] TPID 
Join TB_PROD_SPECS TPS ON TPID.CATALOG_ITEM_NO = TPS.STRING_VALUE AND TPS.ATTRIBUTE_ID = 1 
JOIN TB_CATALOG_PRODUCT TCP on TCP.CATALOG_ID = TPID.CATALOG_ID AND TCP.PRODUCT_ID = TPS.PRODUCT_ID
 
),

CTE1 AS (
Select  distinct CTE.FAMILY_ID,TPF.PRODUCT_ID,CTE.STRING_VALUE From CTE
join TB_PROD_FAMILY TPF on TPF.FAMILY_ID = CTE.FAMILY_ID AND TPF.PRODUCT_ID = CTE.PRODUCT_ID
Join [##IMPORTTEMP'+@SESSIONID+'] TPID on TPID.FAMILY_ID = CTE.FAMILY_ID Where TPF.FLAG_RECYCLE = ''A''
)
,
CTE2 AS(
Select Distinct TPID.FAMILY_ID,C1.PRODUCT_ID,C1.STRING_VALUE From CTE1 C1 Join
[##IMPORTTEMP'+@SESSIONID+'] TPID on C1.STRING_VALUE = TPID.CATALOG_ITEM_NO AND TPID.FAMILY_ID = C1.FAMILY_ID
)


UPDATE [##IMPORTTEMP'+@SESSIONID+'] SET [##IMPORTTEMP'+@SESSIONID+'].PRODUCT_ID = c2.PRODUCT_ID  from CTE2 c2 jOIN [##IMPORTTEMP'+@SESSIONID+'] TPID on c2.STRING_VALUE = TPID.CATALOG_ITEM_NO AND c2.FAMILY_ID = TPID.FAMILY_ID
END

')
END
IF(@VAL<>'')
BEGIN
EXEC('
DECLARE @CATEGORY_NAME VARCHAR(100)
DECLARE @CATEGORYID VARCHAR(100)
DECLARE @PARENTCATEGORY VARCHAR(100)
DECLARE @SUBCATEGORY CURSOR
SET @SUBCATEGORY =CURSOR FOR

SELECT CATEGORYID,CATEGORYNAME,CASE WHEN CATEGORYID=''SUBCATID_L1'' THEN ''CATEGORY_ID'' ELSE ''SUBCATID_L''+CAST((ROWNO-1) AS VARCHAR(10))END AS PARENTCATEGORY  FROM (
SELECT COL.NAME  ,''CATEGORYID'' COLUMN_NAME,ROW_NUMBER() OVER (ORDER BY COLUMN_ID) AS  ROWNO FROM TEMPDB.SYS.COLUMNS COL JOIN TEMPDB.SYS.OBJECTS TAB ON COL.OBJECT_ID=TAB.OBJECT_ID AND TAB.NAME LIKE ''##IMPORTTEMP'+@SESSIONID+'%'' WHERE COL.NAME LIKE ''SUBCATID%'' 
UNION 
SELECT COL.NAME,''CATEGORYNAME'' COLUMN_NAME,ROW_NUMBER() OVER (ORDER BY COLUMN_ID) AS  ROWNO FROM TEMPDB.SYS.COLUMNS COL JOIN TEMPDB.SYS.OBJECTS TAB ON COL.OBJECT_ID=TAB.OBJECT_ID AND TAB.NAME LIKE ''##IMPORTTEMP'+@SESSIONID+'%'' WHERE COL.NAME NOT LIKE ''SUBCATID%'' AND COL.NAME LIKE ''SUBCAT%''  ) AS TEMMP
PIVOT
(
  MAX(NAME)  FOR COLUMN_NAME IN (CATEGORYID,CATEGORYNAME)
)FF
OPEN @SUBCATEGORY
IF(@@CURSOR_ROWS>0)
BEGIN
FETCH NEXT 
FROM @SUBCATEGORY INTO @CATEGORYID ,@CATEGORY_NAME,@PARENTCATEGORY
WHILE (@@FETCH_STATUS=0)
BEGIN
EXEC(''
IF((SELECT COUNT(TEMP.''+@CATEGORY_NAME+'') FROM [##IMPORTTEMP'+@SESSIONID+'] TEMP WHERE TEMP.''+@CATEGORY_NAME+'' IS NOT NULL)>0)
BEGIN
EXEC ('''' UPDATE TEMP SET TEMP.''+@CATEGORYID+''=TC.CATEGORY_SHORT
FROM [##IMPORTTEMP'+@SESSIONID+'] TEMP JOIN TB_CATEGORY TC ON   TEMP.''+@CATEGORY_NAME+''=TC.CATEGORY_NAME AND TC.FLAG_RECYCLE=''''''''A'''''''' AND TC.CATEGORY_PARENT_SHORT=TEMP.''+@PARENTCATEGORY+''

JOIN TB_CATALOG_SECTIONS TCS ON TC.CATEGORY_ID=TCS.CATEGORY_ID AND TCS.CATALOG_ID=TEMP.CATALOG_ID AND TCS.FLAG_RECYCLE=''''''''A''''''''

UPDATE TEMP SET TEMP.FAMILY_ID=TF.FAMILY_ID  FROM [##IMPORTTEMP'+@SESSIONID+'] TEMP JOIN TB_FAMILY TF ON TEMP.FAMILY_NAME=TF.FAMILY_NAME 
JOIN TB_CATEGORY TC ON  TEMP.''+@CATEGORYID+''=TC.CATEGORY_SHORT AND TEMP.''+@CATEGORYID+'' IS NOT NULL AND TEMP.''+@CATEGORYID+''<>''''''''''''''''
JOIN TB_CATALOG_FAMILY TCS ON TCS.CATEGORY_ID=TC.CATEGORY_ID  AND TCS.CATALOG_ID=TEMP.CATALOG_ID AND TCS.FAMILY_ID=TF.FAMILY_ID AND TCS.FLAG_RECYCLE=''''''''A''''''''


IF((SELECT COUNT(COL.NAME) FROM TEMPDB.SYS.COLUMNS COL JOIN TEMPDB.SYS.OBJECTS TAB ON COL.OBJECT_ID=TAB.OBJECT_ID AND TAB.NAME=''''''''##IMPORTTEMP'+@SESSIONID+''''''''' WHERE COL.NAME=''''''''SUBFAMILY_NAME'''''''')>0)
BEGIN 
 UPDATE TEMP SET TEMP.SUBFAMILY_ID=TF.FAMILY_ID  FROM [##IMPORTTEMP'+@SESSIONID+'] TEMP JOIN TB_FAMILY TF ON TEMP.SUBFAMILY_NAME=TF.FAMILY_NAME AND TF.PARENT_FAMILY_ID=TEMP.FAMILY_ID AND TEMP.FAMILY_ID IS NOT NULL
 JOIN TB_CATEGORY TC ON  TEMP.''+@CATEGORYID+''=TC.CATEGORY_SHORT 
JOIN TB_CATALOG_FAMILY TCS ON TCS.CATEGORY_ID=TC.CATEGORY_ID  AND TCS.CATALOG_ID=TEMP.CATALOG_ID AND TCS.FAMILY_ID=TF.FAMILY_ID AND TCS.FLAG_RECYCLE=''''''''A''''''''
END'''')
END
ELSE 
BEGIN
PRINT NULL
END
'')
FETCH NEXT 
FROM @SUBCATEGORY INTO  @CATEGORYID,@CATEGORY_NAME,@PARENTCATEGORY
END

END
CLOSE @SUBCATEGORY
DEALLOCATE @SUBCATEGORY
 	 ')
END
END

Go




ALTER PROCEDURE [dbo].[STP_TABLEDESIGNER_UPDATION]
(@MASTERCATALOG_ID INT = 89,
@MASTERFAMILY_NAME VARCHAR(MAX)='WEAR RINGS',
@CATALOGID INT=89,
@CATEGORYID NVARCHAR(MAX)='CAT12718',
@FAMILYID INT=26846,
@FAMILY_TABLE_STRUCTURE NVARCHAR(MAX) ='<?xml version="1.0" encoding="utf-8"?><TradingBell TableType="SuperTable"><LeftRowField AttrID="Attr:1" Merge="unchecked" Level="0"></LeftRowField><LeftRowField AttrID="Attr:3696" Merge="unchecked" Level="0"></LeftRowField><LeftRowField AttrID="Attr:3729" Merge="" Level="0"></LeftRowField><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>true</DisplayRowHeader><DisplayColumnHeader>false</DisplayColumnHeader><DisplaySummaryHeader>false</DisplaySummaryHeader><VerticalTable>false</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>',
@STRUCTURE_NAME NVARCHAR(200)='Default Layout')
AS
BEGIN

IF OBJECT_ID('TEMPDB..##TEMP_FAMILY_ID_DETAILS') IS NOT NULL   DROP TABLE  ##TEMP_FAMILY_ID_DETAILS 
 
select ROW_NUMBER()  OVER( order by tf.FAMILY_ID asc) AS Family_Count, tf.FAMILY_ID from TB_FAMILY tf join TB_CATALOG_FAMILY tcf on TCF.FAMILY_ID=TF.FAMILY_ID where family_name=@MASTERFAMILY_NAME and tcf.CATALOG_ID=@MASTERCATALOG_ID

select ROW_NUMBER() OVER(ORDER BY  tf.FAMILY_ID DeSC) AS Family_Count , tf.FAMILY_ID into ##TEMP_FAMILY_ID_DETAILS from TB_FAMILY tf join TB_CATALOG_FAMILY tcf on TCF.FAMILY_ID=TF.FAMILY_ID   where family_name=@MASTERFAMILY_NAME and tcf.CATALOG_ID=@MASTERCATALOG_ID AND tf.CATEGORY_ID = @CATEGORYID


declare @LOOP_COUNT nvarchar(max);
DECLARE @FAMILY_ID NVARCHAR(MAX);

set  @LOOP_COUNT = (SELECT MAX(y.Family_Count) FROM (SELECT COUNT(*) AS Family_count FROM  ##TEMP_FAMILY_ID_DETAILS) y)

print  @LOOP_COUNT

while @LOOP_COUNT >0

BEGIN

--print 'loop in'
set @FAMILY_ID = (SELECT TOP (1) FAMILY_ID from ##TEMP_FAMILY_ID_DETAILS)

print @FAMILY_ID

IF NOT EXISTS(SELECT * FROM TB_FAMILY_TABLE_STRUCTURE WHERE FAMILY_ID=@FAMILY_ID AND CATALOG_ID=@MASTERCATALOG_ID AND STRUCTURE_NAME=@STRUCTURE_NAME)
BEGIN
--print 'in'
INSERT INTO TB_FAMILY_TABLE_STRUCTURE(CATALOG_ID,FAMILY_ID,FAMILY_TABLE_STRUCTURE,STRUCTURE_NAME,IS_DEFAULT)
SELECT @MASTERCATALOG_ID,@FAMILY_ID,@FAMILY_TABLE_STRUCTURE,@STRUCTURE_NAME,IS_DEFAULT FROM TB_FAMILY_TABLE_STRUCTURE
WHERE FAMILY_ID = @FAMILYID AND CATALOG_ID = @CATALOGID AND STRUCTURE_NAME=@STRUCTURE_NAME
END
ELSE 
BEGIN
--print 'up'
UPDATE TB_FAMILY_TABLE_STRUCTURE SET FAMILY_TABLE_STRUCTURE = @FAMILY_TABLE_STRUCTURE 
WHERE FAMILY_ID=@FAMILY_ID AND CATALOG_ID=@MASTERCATALOG_ID AND STRUCTURE_NAME=@STRUCTURE_NAME
END

delete TOP (1)  from ##TEMP_FAMILY_ID_DETAILS

set  @LOOP_COUNT = @LOOP_COUNT -1;

END


END

GO


/****** Object:  Table [dbo].[TB_ASSET_DATERANGE]    Script Date: 2023-01-19 16:13:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_DATERANGE]') AND type in (N'U'))
DROP TABLE [dbo].[TB_ASSET_DATERANGE]
GO
/****** Object:  Table [dbo].[TB_ASSET_DATERANGE]    Script Date: 2023-01-19 16:13:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_DATERANGE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_ASSET_DATERANGE](
	[ID] [int] NOT NULL,
	[DATERANGE] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TB_ASSET_DATERANGE] ([ID], [DATERANGE]) VALUES (1, N'Any Time')
INSERT [dbo].[TB_ASSET_DATERANGE] ([ID], [DATERANGE]) VALUES (2, N'Today')
INSERT [dbo].[TB_ASSET_DATERANGE] ([ID], [DATERANGE]) VALUES (3, N'Yesterday')
INSERT [dbo].[TB_ASSET_DATERANGE] ([ID], [DATERANGE]) VALUES (4, N'Last Week')
INSERT [dbo].[TB_ASSET_DATERANGE] ([ID], [DATERANGE]) VALUES (5, N'Before a Week')
INSERT [dbo].[TB_ASSET_DATERANGE] ([ID], [DATERANGE]) VALUES (6, N'Custom Date')


GO



ALTER PROCEDURE [dbo].[STP_LS_ProductPivotTable_For_Publish2] 
@FAMILY_ID INT=27761,  
@CATALOG_ID INT=134,  
@USER_ID VARCHAR(50)  ='demo@questudio.com',  
@CATEGORY_ID nvarchar(100)='CAT12835',  
@PAGENO INT =1,    
@PERPAGECOUNT INT =10,  
@CATALOG_ITEM_ID int =0  ,
@GROUP_ID INT = 0  ,
@type nvarchar(50) = '',
@AttributeSeach nvarchar(max) = '',
@SearchType nvarchar(20) = '',
@data nvarchar(max) = '',
@ProductSpec bit = 1,
@ProductImg  bit =  1,
@ProductPrice bit = 1,
@ProductKey  bit = 1
--  STP_LS_ProductPivotTable_For_Publish 1958,1032,'emercadal@balboasun.com','CAT115',1,10,0,2024

as
--cast(Header.docId as varchar(50))
-----------------------------------------------------------------------
declare @KeyValues nvarchar(20) = 0

if(@ProductSpec <> 1)
begin
set @KeyValues =  cast(@KeyValues as varchar(50)) +''+1
end

if(@ProductImg <> 1)
begin
print 'img'
set @KeyValues = cast(3 as varchar(50))
end



if(@ProductKey <> 1)
begin
set @KeyValues = cast(6 as varchar(50))
end

if(@ProductPrice <> 1)
begin
set @KeyValues = cast(4 as varchar(50))
end


print @KeyValues

--execute ('select ATTRIBUTE_ID from tb_attribute where attribute_Type not  in ('+ @KeyValues+ ')')

--------------------------------------------------------------------------
DECLARE @ATTRIBUTE_NAMES VARCHAR(MAX);  
DECLARE @ROLE_ID INT =0  

DECLARE @PRODUCT_ID INT =0  
DECLARE @FROMPAGE INT =0  
DECLARE @TOPAGE INT =0  
DECLARE @MAINPROD int =0  
 select  Top 1  @MAINPROD =PRODUCT_ID from TB_SUBPRODUCT where SUBPRODUCT_ID=@CATALOG_ITEM_ID and CATALOG_ID=@CATALOG_ID  
   
   
SET @ATTRIBUTE_NAMES = ''   
Select @ROLE_ID = Role_id from aspnet_Users  au join aspnet_UsersInRoles aur on au.UserId = aur.UserId JOIN aspnet_Roles ar on  ar.RoleId = aur.RoleId where UserName =@USER_ID  
  
  
--Select TOP 1 @PRODUCT_ID = PRODUCT_ID FROM TB_PROD_FAMILY tpf where FAMILY_ID = @FAMILY_ID  
  
print @ROLE_ID  
IF OBJECT_ID('tempdb..#t') IS NOT NULL  
Begin  
 EXEC('DROP TABLE #t')  
end  
IF OBJECT_ID('tempdb..##TEMPSEARCHDATA') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##TEMPSEARCHDATA')  
end  
IF OBJECT_ID('tempdb..##finaldata') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##finaldata')  
end  
IF OBJECT_ID('tempdb..##FilterValu') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterValu')  
end  
IF OBJECT_ID('tempdb..##FilterResult') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterResult')  
end  
IF OBJECT_ID('tempdb..##FilterValu1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterValu1')  
end  
IF OBJECT_ID('tempdb..##FilterResult1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterResult1')  
end  



  
select * into #t from (  
select (ROW_NUMBER() over (partition by family_id,product_id order by family_id))as Row,* from TB_PROD_FAMILY_ATTR_LIST where FAMILY_ID=@FAMILY_ID ---and PRODUCT_ID=36  
) as temp   
  
--Select TOP 1 @PRODUCT_ID = TPF.PRODUCT_ID FROM TB_PROD_FAMILY tpf   
--join TB_PROD_FAMILY_ATTR_LIST TPFL on TPFL.FAMILY_ID = tpf.FAMILY_ID       
--where TPF.FAMILY_ID = @FAMILY_ID AND TPFL.FAMILY_ID = @FAMILY_ID group by tpf.PRODUCT_ID,tpf.FAMILY_ID   
--order by count(ATTRIBUTE_ID)   
  
Select TOP 1 @PRODUCT_ID = TPF.PRODUCT_ID FROM TB_PROD_FAMILY tpf   
--join TB_PROD_FAMILY_ATTR_LIST TPFL on TPFL.FAMILY_ID = tpf.FAMILY_ID       
left outer join TB_PROD_SPECS TPS  on TPS.PRODUCT_ID = tpf.PRODUCT_ID      
where TPF.FAMILY_ID = @FAMILY_ID  AND tpf.FAMILY_ID = @FAMILY_ID    and tpf.FLAG_RECYCLE='A'
group by tpf.PRODUCT_ID,tpf.FAMILY_ID   
order by count(TPS.ATTRIBUTE_ID) desc  
  
print @PRODUCT_ID  
SET @FROMPAGE =  ((@PAGENO-1) * @PERPAGECOUNT) + 1   
SET @TOPAGE =  ((@PAGENO-1) * @PERPAGECOUNT) + @PERPAGECOUNT   
  
  
 if(@GROUP_ID=0)
 begin 

 print 'king1123'

;WITH TTT  
AS  
(  

SELECT DISTINCT * FROM (  
SELECT DISTINCT TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME='Supplier' THEN ATTRIBUTE_NAME +'__OBJ__0' +'__'+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +'__OBJ__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) END +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END AS ATTRIBUTE_NAME,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
--JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TPFAL.PRODUCT_ID = TPF.PRODUCT_ID AND TF.FAMILY_ID = TPFAL.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID  
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID AND ATTRIBUTE_TYPE NOT IN (9,7,11,12,13) --AND TP.PRODUCT_ID = @PRODUCT_ID  
UNION ALL  
SELECT DISTINCT TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME+'__' + 'OBJ'+'__'+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END +'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END  AS ATTRIBUTE_NAME,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   
FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID  
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID AND ATTRIBUTE_TYPE NOT IN (9,7,11,12,13) --AND TP.PRODUCT_ID = @PRODUCT_ID  
UNION ALL  
SELECT DISTINCT TPS.ATTRIBUTE_ID,ATTRIBUTE_NAME+ '__1__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END   AS ATTRIBUTE_NAME  
,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   
  FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID  AND TA.ATTRIBUTE_TYPE = 3 AND ATTRIBUTE_TYPE NOT IN (9,7,11,12,13)--AND  TP.PRODUCT_ID = @PRODUCT_ID  
  
 ) DD    
)   
  --execute ('select ATTRIBUTE_ID from tb_attribute where attribute_Type not  in ('+ @KeyValues+ ')')
-- EXEC('select * into #FilterValu from TTT where ATTRIBUTE_ID not in ('+ @KeyValues+ ')')
select * into ##FilterValu from TTT


--select * from ##FilterValu
--exec('select * into ##FilterResult from ##FilterValu  where ATTRIBUTE_ID not in ('+ @KeyValues+ ')') 
if(@KeyValues = 0)
BEGIN
exec('select * into ##FilterResult from ##FilterValu  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE not in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID) ') 
END
ELSE
BEGIN
exec('select * into ##FilterResult from ##FilterValu  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID) ') 
END
   --exec('select * into ##FilterResult from ##FilterValu  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID) ') 



SELECT @ATTRIBUTE_NAMES = @ATTRIBUTE_NAMES +'['+ ATTRIBUTE_NAME + '],' FROM ##FilterResult ORDER BY ATTRIBUTE_ID  


--select @ATTRIBUTE_NAMES

END

else 

Begin 

print 'king'

;WITH TTT  
AS  
(  
SELECT DISTINCT * FROM (  
SELECT DISTINCT TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME='Supplier' THEN ATTRIBUTE_NAME +'__OBJ__0' +'__'+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +'__OBJ__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) END +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END AS ATTRIBUTE_NAME,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
--JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TPFAL.PRODUCT_ID = TPF.PRODUCT_ID AND TF.FAMILY_ID = TPFAL.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID  
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID  --AND TP.PRODUCT_ID = @PRODUCT_ID  
UNION ALL  
SELECT DISTINCT TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME+'__' + 'OBJ'+'__'+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END +'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END  AS ATTRIBUTE_NAME,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   
FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID  
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID --AND TP.PRODUCT_ID = @PRODUCT_ID  
UNION ALL  
SELECT DISTINCT TPS.ATTRIBUTE_ID,ATTRIBUTE_NAME+ '__1__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END   AS ATTRIBUTE_NAME  
,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   
  FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID  AND TA.ATTRIBUTE_TYPE = 3   --AND TP.PRODUCT_ID = @PRODUCT_ID  
  
 ) DD    
)   

    select * into ##FilterValu1 from TTT
	
	 exec('select * into ##FilterResult1 from ##FilterValu1  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE not in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID)') 

SELECT @ATTRIBUTE_NAMES = @ATTRIBUTE_NAMES +'['+ ATTRIBUTE_NAME + '],' FROM ##FilterResult1 where ATTRIBUTE_ID in 
(Select ATTRIBUTE_ID from TB_PACKAGE_DETAILS where GROUP_ID=@GROUP_ID) or ATTRIBUTE_ID=1 ORDER BY SORT_ORDER,ATTRIBUTE_ID

End  

  
IF(@ATTRIBUTE_NAMES!=NULL OR @ATTRIBUTE_NAMES !='')  
BEGIN  
SET @ATTRIBUTE_NAMES = LEFT(@ATTRIBUTE_NAMES, LEN(@ATTRIBUTE_NAMES) - 1)  
print @ATTRIBUTE_NAMES  
END  
  print '1'
print @ATTRIBUTE_NAMES  
  Declare @sortOrderPage int 
  set @sortOrderPage=@FROMPAGE-1
IF(@ATTRIBUTE_NAMES!=NULL OR @ATTRIBUTE_NAMES !='')  
 BEGIN  
     
	 print 'oopsssss'
   
  EXEC('  
  IF (SELECT object_id(''TempDB..#TEMP_ITEMS'')) IS NOT NULL  
BEGIN  
    DROP TABLE #TEMP_ITEMS  
END  
  
 DECLARE @PCNT INT=0  
 DECLARE @TEMPROW INT=0  
 DECLARE @TMPFRM INT=0  
 DEClare @TMPTO INT=0  
     
  SELECT ROW_NUMBER() over (Partition by ID1 ORDER BY SORT) AS ID, * INTO #TEMPPRODSPECS FROM (  
  SELECT ROW_NUMBER() over (Partition by TPF.PRODUCT_ID ORDER BY TPF.PRODUCT_ID) AS ID1,TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END   
  as PUBLISH2PRINT,  
  TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,TPS.STRING_VALUE STRING_VALUE,  ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+ CAST(ATTRIBUTE_TYPE as nvarchar)   
   +''__true__true''     AS ATTRIBUTE_NAMETEMP,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL
    FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
   --JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
   JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +'''
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS  
   JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   WHERE TF.FAMILY_ID='+ @FAMILY_ID +' AND TCF.CATALOG_ID='+ @CATALOG_ID +'  AND TA.ATTRIBUTE_ID =1  
    ) as AA where id1=''1'' 
   
    
   
 IF( (LEN(ISNULL('+@MAINPROD +', '''')) <> 0 ) AND ('+@MAINPROD +'<>0) )  
 BEGIN  
   
   
 SELECT @TEMPROW= ID FROM #TEMPPRODSPECS WHERE PRODUCT_ID='+@MAINPROD+'  
    
SET @PCNT  =(((@TEMPROW)/'+@PERPAGECOUNT+')* '+@PERPAGECOUNT+')  
   
   
  SET @TMPFRM =   @PCNT + 1  
  
  set @TMPTO=@PCNT +'+@PERPAGECOUNT+'  
  
    
      
    
  END  
 ELSE  
 BEGIN  
   
 SET @TMPFRM = '+ @FROMPAGE +'  
 SET @TMPTO = '+ @TOPAGE +'  
 END  
    
 
 SELECT * INTO #TEMP_ITEMS FROM #TEMPPRODSPECS WHERE ID BETWEEN   @TMPFRM  AND  @TMPTO   
    
    
  SELECT  * into #TempData FROM   
  (  
     
  SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%TEXT%'' THEN TPS.STRING_VALUE   
  WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%NUM%'' THEN --CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE)   
  REPLACE(STR(ROUND(CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE),cast(substring(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''),  
  CHARINDEX('','',REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))+1, len(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))) as int)),25,cast(substring(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''),  
  CHARINDEX('','',REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))+1, len(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))) as int)),'' '','''')   
  WHEN  TA.ATTRIBUTE_DATATYPE LIKE ''%HYPER%''  THEN TPS.STRING_VALUE  
  ELSE TPS.STRING_VALUE  
  END   
  STRING_VALUE,CASE WHEN ATTRIBUTE_NAME=''Supplier'' THEN ATTRIBUTE_NAME +''__OBJ__0''+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  ELSE ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  END +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END   AS ATTRIBUTE_NAME,
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL     
    FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
   --JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
   JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +'''
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS  
   LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON  TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   JOIN #TEMP_ITEMS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
   WHERE TF.FAMILY_ID='+ @FAMILY_ID +' AND TCF.CATALOG_ID='+ @CATALOG_ID +' AND ATTRIBUTE_TYPE <> 6  
  UNION ALL  
   SELECT DISTINCT TPS.CATALOG_ID,TPS.FAMILY_ID,TPS.PRODUCT_ID,TPF.SORT_ORDER SORT,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT, TWS.STATUS_NAME as [WORKFLOW STATUS] ,  
   ATTRIBUTE_VALUE,  
   ATTRIBUTE_NAME + ''__OBJ''+''__''+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
   +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL
   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
   JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID and TPS.CATEGORY_ID = '''+@CATEGORY_ID+'''  
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +''' 
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS   
   LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   JOIN #TEMP_ITEMS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
   WHERE TF.FAMILY_ID='+ @FAMILY_ID +' AND TCF.CATALOG_ID='+ @CATALOG_ID +'    
  UNION ALL  
  SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,ISNULL(TPS.OBJECT_NAME , '''')  AS STRING_VALUE  
  ,ATTRIBUTE_NAME + ''__1__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE   
  WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME
  ,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL  
   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
  JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
  JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
  JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
  JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
  JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'    
  join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +''' 
  JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS   
  LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
  LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON  TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
  LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
  JOIN #TEMP_ITEMS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
  WHERE TF.FAMILY_ID='+ @FAMILY_ID +' AND TCF.CATALOG_ID='+ @CATALOG_ID +'  AND TA.ATTRIBUTE_TYPE = 3 AND ATTRIBUTE_TYPE <> 6    
  )AS A   
  PIVOT  
  (  
   MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('+ @ATTRIBUTE_NAMES +')  
  )AS P  
   
  update #TempData set SORT=tt1.SORT_ORDER from #TempData join (select *,ROW_NUMBER() over(order by SORT,PRODUCT_ID)+'+@sortOrderPage+' SORT_ORDER from #TempData) as tt1 on #TempData.PRODUCT_ID=tt1.PRODUCT_ID
  select  * into ##finaldata from #TempData order by SORT  

   SELECT  * into #SearchTempData FROM   
  (  
     
  SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%TEXT%'' THEN TPS.STRING_VALUE   
  WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%NUM%'' THEN --CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE)   
  REPLACE(STR(ROUND(CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE),cast(substring(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''),  
  CHARINDEX('','',REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))+1, len(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))) as int)),25,cast(substring(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''),  
  CHARINDEX('','',REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))+1, len(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))) as int)),'' '','''')   
  WHEN  TA.ATTRIBUTE_DATATYPE LIKE ''%HYPER%''  THEN TPS.STRING_VALUE  
  ELSE TPS.STRING_VALUE  
  END   
  STRING_VALUE,CASE WHEN ATTRIBUTE_NAME=''Supplier'' THEN ATTRIBUTE_NAME +''__OBJ__0''+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  ELSE ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  END +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END   AS ATTRIBUTE_NAME,
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL     
    FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
   --JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
   JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +'''
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS  
   LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON  TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   JOIN #TEMPPRODSPECS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
   WHERE TF.FAMILY_ID='+ @FAMILY_ID +' AND TCF.CATALOG_ID='+ @CATALOG_ID +' AND ATTRIBUTE_TYPE <> 6  
  UNION ALL  
   SELECT DISTINCT TPS.CATALOG_ID,TPS.FAMILY_ID,TPS.PRODUCT_ID,TPF.SORT_ORDER SORT,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT, TWS.STATUS_NAME as [WORKFLOW STATUS] ,  
   ATTRIBUTE_VALUE,  
   ATTRIBUTE_NAME + ''__OBJ''+''__''+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
   +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL
   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
   JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID and TPS.CATEGORY_ID = '''+@CATEGORY_ID+'''  
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +''' 
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS   
   LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   JOIN #TEMPPRODSPECS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
   WHERE TF.FAMILY_ID='+ @FAMILY_ID +' AND TCF.CATALOG_ID='+ @CATALOG_ID +'    
  UNION ALL  
  SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,ISNULL(TPS.OBJECT_NAME , '''')  AS STRING_VALUE  
  ,ATTRIBUTE_NAME + ''__1__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE   
  WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME
  ,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL  
   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
  JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
  JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
  JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
  JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
  JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'    
  join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +''' 
  JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS   
  LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
  LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON  TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
  LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
  JOIN #TEMPPRODSPECS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
  WHERE TF.FAMILY_ID='+ @FAMILY_ID +' AND TCF.CATALOG_ID='+ @CATALOG_ID +'  AND TA.ATTRIBUTE_TYPE = 3 AND ATTRIBUTE_TYPE <> 6    
  )AS A   
  PIVOT  
  (  
   MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('+ @ATTRIBUTE_NAMES +')  
  )AS S 
      
	 
update #SearchTempData set SORT=tt1.SORT_ORDER from #SearchTempData join (select *,ROW_NUMBER() over(order by SORT,PRODUCT_ID)+'+@sortOrderPage+' SORT_ORDER from #SearchTempData) as tt1 on #SearchTempData.PRODUCT_ID=tt1.PRODUCT_ID


select * into ##TEMPSEARCHDATA from #SearchTempData order by SORT

 '  

   )  
  
 --------------------To implement the search functionality  - Start



 IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE #Temp1')  
end  


if(@type = 'product')

begin





 
 print 'kids'

 Select column_id as Row_Number,name into #Temp1 From  Tempdb.Sys.Columns Where Object_ID = Object_ID('tempdb..##TEMPSEARCHDATA')
  
   declare @a int = 1;
   declare @name nvarchar(max);
   declare @sysAttribute nvarchar(max) ;
   declare @coloum nvarchar(max);
   declare @ColumName nvarchar(max)


   while ((select count(*) + 1 from  #Temp1)  > @a)
   begin
        set @name = (select name from  #Temp1 where Row_Number =  @a)
		if CHARINDEX('__OBJ',@name) > 0
		  begin
			if(@AttributeSeach =  (select SUBSTRING(@name,1,(SELECT CHARINDEX ('__OBJ',@name) -1))))
			begin
			set @sysAttribute = @name
			print @sysAttribute
			end
		end
		   set @a = @a + 1
   end

   ----------------------------------------------------------------------------------------------------------------------------
set @coloum = @sysAttribute
set @ColumName = '##TEMPSEARCHDATA.[' +@coloum +']'
print 'ooooops1'
print @ColumName
print 'ooooops1'
print @data


if(@ColumName is not null)
begin
print 'IAM not Null'
if(@SearchType = 'Contains')
begin
print 'Contains'
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   like   ''%'+@data+'%''')
exec('select Count(*) from ##TEMPSEARCHDATA where '+@ColumName+'   like   ''%'+@data+'%''') 
end
else if(@SearchType = 'Exact')
begin
print 'EXACT'
print @ColumName
print @data
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   =   '''+@data+'''   ')
exec('select Count(*) from ##TEMPSEARCHDATA where '+@ColumName+'   =   '''+@data+'''   ') 
end
else if(@SearchType = 'Starts With')
begin
print 'Starts With'
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   like   '''+@data+'%''')
exec('select Count(*) from ##TEMPSEARCHDATA where '+@ColumName+'   like   '''+@data+'%''') 
end
else if(@SearchType = 'Ends With')
begin
print 'Ends With'
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   like   ''%'+@data+'''')
exec('select Count(*) from ##TEMPSEARCHDATA where '+@ColumName+'   like   ''%'+@data+'''') 
end

end
else
begin
print 'IAm a Null'
--select * from ##finaldata

--delete from ##finaldata
select * from ##finaldata
exec('select Count(*) from ##TEMPSEARCHDATA') 
end





End
else
begin
 print '2'  
select * from ##finaldata order by SORT 
exec('select Count(*) from ##TEMPSEARCHDATA') 
 print '22'  
end



 -----------------To implement the search functionality  - End
 
 
 END  
   
ELSE  
 BEGIN  

 	 print 'oops1'

  EXEC('  
  SELECT ROW_NUMBER() over (ORDER BY PRODUCT_ID) AS ID, * FROM   
  (  
  SELECT distinct TCF.CATALOG_ID,TF.CATEGORY_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,TPF.PUBLISH2PRINT ,TPF.PUBLISH2WEB ,TPF.PUBLISH2PORTAL,TPF.PUBLISH2EXPORT,TPF.PUBLISH2PDF
  FROM TB_FAMILY TF   
  JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID AND TF.FAMILY_ID = '+ @FAMILY_ID +' AND TCF.CATALOG_ID = '+ @CATALOG_ID +'  
  JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
  JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
  JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID  
  JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE =''A''  
  JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID
    
  )AS A    
  --SELECT * FROM #TEMPPRODSPECS WHERE ID BETWEEN  '+ @FROMPAGE +' AND '+ @TOPAGE +'  
   ')  
 END  

SET ANSI_NULLS ON

Go


GO
/****** Object:  Table [dbo].[TB_ASSET_SIZE_UNIT]    Script Date: 2023-01-19 18:01:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_SIZE_UNIT]') AND type in (N'U'))
DROP TABLE [dbo].[TB_ASSET_SIZE_UNIT]
GO
/****** Object:  Table [dbo].[TB_ASSET_SIZE_RANGE]    Script Date: 2023-01-19 18:01:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_SIZE_RANGE]') AND type in (N'U'))
DROP TABLE [dbo].[TB_ASSET_SIZE_RANGE]
GO
/****** Object:  Table [dbo].[TB_ASSET_SIZE_RANGE]    Script Date: 2023-01-19 18:01:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_SIZE_RANGE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_ASSET_SIZE_RANGE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SIZE_RANG] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_ASSET_SIZE_UNIT]    Script Date: 2023-01-19 18:01:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_SIZE_UNIT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_ASSET_SIZE_UNIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SIZE_UNIT] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TB_ASSET_SIZE_RANGE] ON 

INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (1, N'Greater than')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (2, N'Less than')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (3, N'Tiny (0KB - 16KB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (4, N'Small (16KB - 1MB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (5, N'Medium (1MB - 128MB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (6, N'Large (128MB - 1GB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (7, N'Extra Large (1GB - 4GB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (8, N'Huge (Greater than 4GB)')
SET IDENTITY_INSERT [dbo].[TB_ASSET_SIZE_RANGE] OFF
SET IDENTITY_INSERT [dbo].[TB_ASSET_SIZE_UNIT] ON 

INSERT [dbo].[TB_ASSET_SIZE_UNIT] ([ID], [SIZE_UNIT]) VALUES (1, N'KB')
INSERT [dbo].[TB_ASSET_SIZE_UNIT] ([ID], [SIZE_UNIT]) VALUES (2, N'MB')
INSERT [dbo].[TB_ASSET_SIZE_UNIT] ([ID], [SIZE_UNIT]) VALUES (3, N'GB')
SET IDENTITY_INSERT [dbo].[TB_ASSET_SIZE_UNIT] OFF


GO


GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__TB_ASSET___selec__7335AFFF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TB_ASSET_STARRED] DROP CONSTRAINT [DF__TB_ASSET___selec__7335AFFF]
END

GO
/****** Object:  Table [dbo].[TB_ASSET_STARRED]    Script Date: 2023-01-19 17:58:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_STARRED]') AND type in (N'U'))
DROP TABLE [dbo].[TB_ASSET_STARRED]
GO
/****** Object:  Table [dbo].[TB_ASSET_STARRED]    Script Date: 2023-01-19 17:58:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_STARRED]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_ASSET_STARRED](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FILE_NAME] [varchar](255) NOT NULL,
	[EXTENSION] [varchar](255) NULL,
	[FILE_PATH] [varchar](max) NOT NULL,
	[USER_NAME] [varchar](255) NOT NULL,
	[CATALOG_ID] [int] NULL,
	[SIZE] [varchar](255) NULL,
	[LAST_MODIFIED_DATE] [varchar](255) NULL,
	[PREVIEW_PATH] [varchar](max) NOT NULL,
	[EXTERNAL_DRIVE_FLAG] [varchar](255) NOT NULL,
	[selected] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__TB_ASSET___selec__7335AFFF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TB_ASSET_STARRED] ADD  DEFAULT ((0)) FOR [selected]
END

GO


GO

Select * From TB_ATTRIBUTE Where ATTRIBUTE_NAME like '%Descriptions%'


Update TB_ATTRIBUTE set ATTRIBUTE_NAME = 'ITEM COLORS',CAPTION = 'ITEM COLORS' Where ATTRIBUTE_ID = 2088

Update TB_ATTRIBUTE set ATTRIBUTE_NAME = 'Product Desc',CAPTION = 'Product Desc' Where ATTRIBUTE_ID = 2102

GO

alter table TB_PROJECT_SECTIONS add  XML_GENERATED_DATE datetime

GO


GO
/****** Object:  StoredProcedure [dbo].[STP_CATALOGSTUDIO5_AddNewUser]    Script Date: 16-02-2023 11:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO5_AddNewUser] 
@USERID NVARCHAR(100)= 'jothipriya@questudio.com', 
@STATUS NVARCHAR(10) = 'InActive', 
@ROLEID INT = 0
AS 

BEGIN 

DECLARE @WEBUSERID NVARCHAR(100) =''
DECLARE @WEBROLEID nvarchar(100) =''

	IF NOT EXISTS (SELECT TB_USER_ID FROM TB_USER WHERE TB_USER_ID=@USERID) 
	  BEGIN     
	   INSERT INTO TB_USER (TB_USER_ID,STATUS) VALUES (@USERID,@STATUS)  
	 END 
	ELSE    
		UPDATE TB_USER SET STATUS=@STATUS WHERE TB_USER_ID=@USERID  
	IF NOT EXISTS (SELECT TB_USER_ID FROM TB_USER_ROLE WHERE TB_USER_ID=@USERID) 
	   BEGIN    
	--  ALTER TABLE [dbo].[TB_USER_ROLE] DROP CONSTRAINT [R_100]
     
		 INSERT INTO TB_USER_ROLE (TB_USER_ID,ROLE_ID) VALUES (@USERID,@ROLEID)  

		--- ALTER TABLE [dbo].[TB_USER_ROLE]  WITH CHECK ADD  CONSTRAINT [R_100] FOREIGN KEY([ROLE_ID]) REFERENCES [dbo].[TB_ROLE] ([ROLE_ID]) ON DELETE CASCADE


--ALTER TABLE [dbo].[TB_USER_ROLE] CHECK CONSTRAINT [R_100]
	   END 
	ELSE  
	  UPDATE TB_USER_ROLE SET ROLE_ID=@ROLEID WHERE TB_USER_ID=@USERID  
	  
	  IF NOT EXISTS (SELECT UserId FROM aspnet_UsersInRoles WHERE  UserId=(SELECT UserId FROM aspnet_Users WHERE UserName=@USERID)  and RoleId =(SELECT RoleId FROM aspnet_Roles WHERE Role_id =@ROLEID) ) 
	  BEGIN 
			 SELECT @WEBUSERID = UserId FROM aspnet_Users WHERE UserName=@USERID    
			  SELECT @WEBROLEID =  RoleId FROM aspnet_Roles WHERE Role_id =@ROLEID
	  IF ( @WEBUSERID <> ''   and @WEBROLEID <>'') 
		  BEGIN
			   INSERT INTO aspnet_UsersInRoles (UserId,RoleId) VALUES (@WEBUSERID,@WEBROLEID)  
		  END 
	 END 
	  
END

GO



ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO5_CATEGORYATTRIBUTEIMPORT]
@SID VARCHAR(100)='60910821-bdcb-4a2a-af4e-ff34ac190b41',
@ALLOWDUP INT=1,
@CUSTOMERID INT=8,
@CUSTOMERNAME NVARCHAR(500)='demo@questudio.com'
AS
BEGIN
	BEGIN TRY 

	IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##IMPORTTEMPTRANSPOSE'+ @SID +'')  
	   BEGIN  
		EXEC('DROP TABLE [##IMPORTTEMPTRANSPOSE'+ @SID +']')  
	   END
 
     IF EXISTS(select 1 from tempdb.sys.tables t  join tempdb.sys.columns c on t.object_id=c.object_id where T.NAME='##IMPORTTEMP' + @SID + '' AND C.name like 'CATEGORY_PUBLISH2%')
	BEGIN
	print'a'
	EXEC('update [##IMPORTTEMP' + @SID + '] SET CATEGORY_PUBLISH2WEB=CASE WHEN CATEGORY_PUBLISH2WEB is null THEN ''1'' ELSE CASE WHEN CATEGORY_PUBLISH2WEB=''1'' OR CATEGORY_PUBLISH2WEB=''Y'' THEN ''1'' Else ''0'' end END , 
CATEGORY_PUBLISH2PRINT=CASE WHEN CATEGORY_PUBLISH2PRINT is null THEN ''1'' ELSE CASE WHEN CATEGORY_PUBLISH2PRINT=''1'' OR CATEGORY_PUBLISH2PRINT=''Y'' THEN ''1'' Else ''0'' end END,
CATEGORY_PUBLISH2PDF=CASE WHEN CATEGORY_PUBLISH2PDF is null THEN ''1'' ELSE CASE WHEN CATEGORY_PUBLISH2PDF=''1'' OR CATEGORY_PUBLISH2PDF=''Y'' THEN ''1'' Else ''0'' end END,
CATEGORY_PUBLISH2EXPORT=CASE WHEN CATEGORY_PUBLISH2EXPORT is null THEN ''1'' ELSE CASE WHEN CATEGORY_PUBLISH2EXPORT=''Y'' THEN ''1'' Else ''0'' end END,
CATEGORY_PUBLISH2PORTAL=CASE WHEN CATEGORY_PUBLISH2PORTAL is null THEN ''1'' ELSE CASE WHEN CATEGORY_PUBLISH2PORTAL=''Y'' THEN ''1'' Else ''0'' end END
from [##IMPORTTEMP' + @SID + ']')

	END
	--and CATEGORY_PUBLISH2EXPORT is not null and CATEGORY_PUBLISH2PORTAL is not null
	--CATEGORY_PUBLISH2EXPORT=CASE WHEN CATEGORY_PUBLISH2EXPORT=''Y'' THEN ''1'' Else ''0'' end,
--CATEGORY_PUBLISH2PORTAL=CASE WHEN CATEGORY_PUBLISH2PORTAL=''Y'' THEN ''1'' Else ''0'' end,
	DECLARE @TRANSACTION_NAME VARCHAR(100) = '['+@SID+']';
	BEGIN TRANSACTION @TRANSACTION_NAME
	IF EXISTS(SELECT C.NAME FROM TEMPDB.SYS.TABLES T INNER JOIN TEMPDB.SYS.COLUMNS C ON C.OBJECT_ID=T.OBJECT_ID  WHERE T.NAME='##IMPORTTEMP' + @SID + '' AND C.NAME IN ('CATEGORY_NAME'))
	BEGIN
	print '24'
	  EXEC('CREATE TABLE [##IMPORTTEMPTRANSPOSE'+ @SID +']([Catalog_id] int,[Product_id] int,[ColumnName]  nvarchar(4000),[Value] sql_variant,FAMILY_ID int,SUBFAMILY_ID int,ATTRIBUTE_ID int,ATTRIBUTE_DATATYPE nvarchar(50),ATTRIBUTE_TYPE tinyint,ATTR_SORT_ORDER int,family_name varchar(500))')  
    EXEC('CREATE NONCLUSTERED INDEX ix_tempNCIndexAft ON [##IMPORTTEMPTRANSPOSE'+ @SID +'] ([Catalog_id],[Product_id],[FAMILY_ID],[ATTRIBUTE_ID])')
	IF EXISTS(SELECT C.NAME FROM TEMPDB.SYS.TABLES T INNER JOIN TEMPDB.SYS.COLUMNS C ON C.OBJECT_ID=T.OBJECT_ID  WHERE T.NAME='##IMPORTTEMP' + @SID + '' AND C.NAME IN ('CATEGORY_NAME'))
	BEGIN
		---------------------------To replace empty values ------------------------
	print'233'
		EXEC('DELETE FROM [##IMPORTTEMP'+ @SID +'] WHERE CATALOG_NAME IS NULL AND CATEGORY_NAME IS NULL')
		DECLARE @CATALOG_EXISTS VARCHAR(100)
		declare @CUSTOMERID_ varchar(100)
		set @CUSTOMERID_=CONVERT(VARCHAR(100),@CUSTOMERID)
		print '23'
	
		DECLARE @QRY_OUT NVARCHAR(MAX) = 'EXEC STP_CATALOGSTUDIO5_CATEGORYATTRIBUTE_IMPORT '''+ @SID +''','''+@CUSTOMERID_+''','''+@CUSTOMERNAME+''',@retvalOUT OUTPUT'
		DECLARE @ParmDefinition NVARCHAR(1000) = '@retvalOUT NVARCHAR(MAX) OUTPUT';
		EXEC sp_executesql @QRY_OUT, @ParmDefinition, @retvalOUT=@CATALOG_EXISTS OUTPUT;
		SELECT @CATALOG_EXISTS
		EXEC ('STP_CATALOGSTUDIO5_CATEGORYATTRIBUTE_SPECSIMPORT '''+ @SID +''','+@CUSTOMERID+','''+@CUSTOMERNAME+'''')
		select 'Catalog'

		

			IF(@CATALOG_EXISTS !='')
			BEGIN
				IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##LOGTEMPTABLE'+ @SID +'')
				BEGIN
					PRINT 'IF124'
					EXEC('DROP TABLE [##LOGTEMPTABLE'+ @SID +']')
					RAISERROR(@CATALOG_EXISTS,16,1)
			
				
					EXEC('SELECT
					ERROR_MESSAGE() AS ErrorMessage,
					ERROR_PROCEDURE() AS ErrorProcedure,
					ERROR_SEVERITY() AS ErrorSeverity,
					ERROR_STATE() AS ErrorState,
					ERROR_NUMBER() AS ErrorNumber,
					ERROR_LINE() AS ErrorLine INTO [##LOGTEMPTABLE'+ @SID +']')
					
					RETURN
					
				END	
				ELSE
				BEGIN
					PRINT 'ELSE'
					RAISERROR(@CATALOG_EXISTS,16,1)
					
					EXEC('SELECT
				ERROR_MESSAGE() AS ErrorMessage,
					ERROR_PROCEDURE() AS ErrorProcedure,
					ERROR_SEVERITY() AS ErrorSeverity,
					ERROR_STATE() AS ErrorState,
					ERROR_NUMBER() AS ErrorNumber,
					ERROR_LINE() AS ErrorLine INTO [##LOGTEMPTABLE'+ @SID +']')
					RETURN
				END
			END
			ELSE
			BEGIN
			PRINT 'ERRE'
				IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##LOGTEMPTABLE'+ @SID +'')
				BEGIN
					EXEC('DROP TABLE [##LOGTEMPTABLE'+ @SID +']')
				END
			END
		PRINT 'CATEGORY COMPLETED';
		
	END
	END	
	ELSE IF EXISTS(SELECT C.NAME FROM TEMPDB.SYS.TABLES T INNER JOIN TEMPDB.SYS.COLUMNS C ON C.OBJECT_ID=T.OBJECT_ID  WHERE T.NAME='##IMPORTTEMP' + @SID + '' AND C.NAME IN ('REMOVE_CATEGORY'))
		BEGIN
		print 'c'
		EXEC('STP_CATALOGSTUDIO5_Category_Remove'''+ @SID +'''')
		END
		COMMIT TRANSACTION @TRANSACTION_NAME
	END TRY
BEGIN CATCH
		ROLLBACK TRANSACTION @TRANSACTION_NAME	
		IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##LOGTEMPTABLE'+ @SID +'')
			BEGIN
			--PRINT 'IF'
				--EXEC('INSERT INTO [##LOGTEMPTABLE'+ @SID +'] (
				--ERRORMESSAGE,
				--ERRORPROCEDURE, 
				--ERRORSEVERITY,
				--ERRORSTATE,
				--ERRORNUMBER, 
				--ERRORLINE) SELECT ERRORMESSAGE,
				--ERRORPROCEDURE, 
				--ERRORSEVERITY,
				--ERRORSTATE,
				--ERRORNUMBER, 
				--ERRORLINE FROM [##LOGTEMPTABLE'+ @SID +']')
				EXEC('DROP TABLE [##LOGTEMPTABLE'+ @SID +']')
			END	
		--ELSE
		--	BEGIN
			--PRINT 'ELSE'
	
		
				EXEC('SELECT
				ERROR_MESSAGE() AS ErrorMessage,
				ERROR_PROCEDURE() AS ErrorProcedure,
				ERROR_SEVERITY() AS ErrorSeverity,
				ERROR_STATE() AS ErrorState,
				ERROR_NUMBER() AS ErrorNumber,
				ERROR_LINE() AS ErrorLine INTO [##LOGTEMPTABLE'+ @SID +']')
			--END
	END CATCH;
END

Go

ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO5_EXPORT_SINGLESHEET]
(
	@SESSID AS VARCHAR(500)='656dea3dab2e4043af3f4547165509e1',
	@CATALOG_ID VARCHAR(10)='8',
	@ATTRIBUTE_IDS VARCHAR(MAX)= '1',
	@CATEGORY_ID VARCHAR(50)='ALL'
)
AS
DECLARE @FLG INT=1
DECLARE @COLNAME VARCHAR(100)='SUBCATID_L'
DECLARE @FTABNAME VARCHAR(100)='##TEMP1'
DECLARE @ITABNAME VARCHAR(100)='##TEMP2'

Declare @AttributeIteName varchar(100) = (select  top 1 ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_ID = 1)

DECLARE @CNT INT=1
DECLARE @CHK INT=0
DECLARE @QUERY nvarchar(MAX);
DECLARE @ParmDefinition nvarchar(500);
IF(@CATEGORY_ID = '0')
SET @CATEGORY_ID = 'All'

IF(@ATTRIBUTE_IDS IS NULL OR @ATTRIBUTE_IDS = '')
	BEGIN
		SET @ATTRIBUTE_IDS = '0';
	END
	
	DECLARE @FTABMAIN VARCHAR(100)='##TEMP1'
SET @FTABMAIN = '##TEMPTABLEMAIN'+@SESSID;

DECLARE @FTABMAINFAM VARCHAR(100)='##TEMP1'
SET @FTABMAINFAM = '##TEMPTABLEMAIN'+@SESSID+'FAM';


	IF OBJECT_ID('tempdb..##TEMPTABLEMAIN'+@SESSID+'FAM') IS NOT NULL
	BEGIN
		EXEC('DROP TABLE [##TEMPTABLEMAIN'+@SESSID+'FAM]')
	END
IF OBJECT_ID('tempdb..##TEMPRESULTSUBPRODUCTS'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMPRESULTSUBPRODUCTS'+@SESSID)
	END
IF OBJECT_ID('tempdb..##TEMPTABLEMAIN'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMPTABLEMAIN'+@SESSID)
		SET @FTABMAIN = '##TEMPTABLEMAIN'+@SESSID;
	END
IF OBJECT_ID('tempdb..##TEMPTABLESUB'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMPTABLESUB'+@SESSID)
	END
	

IF OBJECT_ID('tempdb..##TEMP1'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMP1'+@SESSID)
	END
IF OBJECT_ID('tempdb..##TEMP2'+@SESSID) IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMP2'+@SESSID)
	END
IF(@CATEGORY_ID = 'All')
	BEGIN
		EXEC('SELECT DISTINCT A.CATEGORY_SHORT, A.CATEGORY_ID,A.CATEGORY_NAME INTO ##TEMP1'+ @SESSID +' FROM TB_CATEGORY A JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +' AND  A.WORKFLOW_STATUS=99 AND A.FLAG_RECYCLE = ''A'' AND A.PARENT_CATEGORY = '+''''+'0'+'''')
	END
ELSE
	BEGIN
		EXEC ('SELECT DISTINCT A.CATEGORY_SHORT, A.CATEGORY_ID,A.CATEGORY_NAME INTO ##TEMP1'+ @SESSID +' FROM TB_CATEGORY A JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +' AND A.CATEGORY_ID = ''' + @CATEGORY_ID +'''')
		--PRINT ('SELECT A.CATEGORY_ID,A.CATEGORY_NAME INTO ##TEMP1'+ @SESSID +' FROM TB_CATEGORY A JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +' AND A.CATEGORY_ID = ''' + @CATEGORY_ID +'''')
	END

WHILE @FLG<>0
	BEGIN
	
		IF OBJECT_ID('tempdb..##TEMP1'+@SESSID) IS NOT NULL
			BEGIN
			print'a'
				SET @FTABNAME = '##TEMP1'+@SESSID;
				SET @ITABNAME = '##TEMP2'+@SESSID;
			END
		ELSE
			BEGIN
			print 'b'
				SET @FTABNAME = '##TEMP2'+@SESSID;
				SET @ITABNAME = '##TEMP1'+@SESSID;
			END
			
		IF (@CNT=1)
			BEGIN
				SET @QUERY ='SELECT @retvalOUT = COUNT(*) FROM ' + @FTABNAME + ' T,TB_CATEGORY TC WHERE T.CATEGORY_ID=TC.PARENT_CATEGORY'
				--PRINT @QUERY
				SET @ParmDefinition = '@retvalOUT int OUTPUT';
				EXEC sp_executesql @QUERY, @ParmDefinition, @retvalOUT=@CHK OUTPUT;
			END
		ELSE
			BEGIN
				SET @QUERY ='SELECT @retvalOUT = COUNT(*) FROM ' + @FTABNAME + ' T,TB_CATEGORY TC WHERE T.'+ @COLNAME +'=TC.PARENT_CATEGORY'
				--PRINT @QUERY;
				SET @ParmDefinition = '@retvalOUT int OUTPUT';
				EXEC sp_executesql @QUERY, @ParmDefinition, @retvalOUT=@CHK OUTPUT;
			END
		
		IF(@CHK > 0)
			BEGIN
				IF (@CNT=1)
					BEGIN
					--PRINT 'LOT'
						SET @QUERY = 'SELECT T.*,TC.CATEGORY_SHORT AS SUBCATSHORT_L'+CONVERT(VARCHAR(MAX),@CNT)+',TC.CATEGORY_ID AS SUBCATID_L'+CONVERT(VARCHAR(MAX),@CNT)+',TC.CATEGORY_NAME AS SUBCATNAME_L'+CONVERT(VARCHAR(MAX),@CNT)+' INTO ' + @ITABNAME + ' FROM '+ @FTABNAME +' T
									  LEFT OUTER JOIN
									  TB_CATEGORY TC ON T.CATEGORY_ID=TC.PARENT_CATEGORY AND TC.FLAG_RECYCLE =''A'' AND TC.WORKFLOW_STATUS=99  AND TC.PUBLISH2EXPORT=1'
						PRINT @QUERY;
						EXEC (@QUERY);
						SET @COLNAME='SUBCATID_L';
						SET @COLNAME = @COLNAME + CONVERT(VARCHAR(100),@CNT)
						SET @CNT= @CNT + 1;
						--RETURN
						EXEC ('DROP TABLE ' + @FTABNAME);
						
					END
				ELSE
					BEGIN
						--PRINT 'LOT2018'
						SET @QUERY = 'SELECT T.*,TC.CATEGORY_SHORT AS SUBCATSHORT_L'+CONVERT(VARCHAR(MAX),@CNT)+',TC.CATEGORY_ID AS SUBCATID_L'+CONVERT(VARCHAR(MAX),@CNT)+',TC.CATEGORY_NAME AS SUBCATNAME_L'+CONVERT(VARCHAR(MAX),@CNT)+' INTO ' + @ITABNAME + ' FROM '+ @FTABNAME +' T
									  LEFT OUTER JOIN
									  TB_CATEGORY TC ON T.'+@COLNAME+'=TC.PARENT_CATEGORY AND TC.FLAG_RECYCLE =''A'' AND TC.WORKFLOW_STATUS=99  AND TC.PUBLISH2EXPORT=1'
						EXEC (@QUERY);
						PRINT @QUERY;
						SET @COLNAME='SUBCATID_L';
						SET @COLNAME = @COLNAME + CONVERT(VARCHAR(100),@CNT)
						SET @CNT= @CNT + 1;
						--RETURN
						EXEC ('DROP TABLE ' + @FTABNAME);
						
					END
			END
		ELSE
			SET @FLG=0
	END

DECLARE @TABLE_NAME VARCHAR(100);
IF OBJECT_ID('tempdb..##TEMP1'+@SESSID) IS NOT NULL
	BEGIN
		SET @TABLE_NAME = '##TEMP1'+@SESSID;
	END
IF OBJECT_ID('tempdb..##TEMP2'+@SESSID) IS NOT NULL
	BEGIN
		SET @TABLE_NAME = '##TEMP2'+@SESSID;
	END
--EXEC('SELECT * FROM '+ @TABLE_NAME +'')
DECLARE @COUNT INT = @CNT;
DECLARE @CATEGORY_SQL VARCHAR(MAX) = '';
DECLARE @CATEGORY_SQL_SUBFAMILY VARCHAR(MAX) = '';
DECLARE @CATEGORY_SQL_WITHOUT_FAMILES VARCHAR(MAX) = '';
DECLARE @EEEE VARCHAR(MAX) = '';
DECLARE @TEMP_COUNT_INNER INT = 0;
DECLARE @TEMP_DIFF INT = 0;
WHILE(@COUNT <> 0)
	BEGIN
		SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +'SELECT DISTINCT C.CATALOG_ID,C.CATALOG_NAME,C.VERSION CATALOG_VERSION,C.DESCRIPTION CATALOG_DESCRIPTION';
		SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +'SELECT DISTINCT B.CATALOG_ID,D.CATALOG_NAME,D.VERSION CATALOG_VERSION,D.DESCRIPTION CATALOG_DESCRIPTION';
		SET @CATEGORY_SQL = @CATEGORY_SQL +'SELECT DISTINCT B.CATALOG_ID,D.CATALOG_NAME,D.VERSION CATALOG_VERSION,D.DESCRIPTION CATALOG_DESCRIPTION';
		
		IF(@COUNT = 1)
			BEGIN
			print''
				SET @TEMP_COUNT_INNER = 1;
				WHILE(@TEMP_COUNT_INNER <> (@CNT + 1))
					BEGIN
						IF((@TEMP_COUNT_INNER - 1) < @COUNT)
							BEGIN

								IF(@TEMP_COUNT_INNER = 1)
									BEGIN
									print'a1'
										SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',A.CATEGORY_SHORT as CATEGORY_ID,A.CATEGORY_NAME';
										SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',A.CATEGORY_SHORT as CATEGORY_ID,A.CATEGORY_NAME';
										SET @CATEGORY_SQL = @CATEGORY_SQL +',A.CATEGORY_SHORT as CATEGORY_ID,A.CATEGORY_NAME';
									END
								ELSE
									BEGIN
									print'b1'
										SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] ,A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
										SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
										SET @CATEGORY_SQL = @CATEGORY_SQL +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
									END
							END
						ELSE
							BEGIN
							print'c1'
								SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
								SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
								SET @CATEGORY_SQL = @CATEGORY_SQL +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
							END
						SET @TEMP_COUNT_INNER = @TEMP_COUNT_INNER + 1;						
					END

				SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES + ',E.CUSTOM_NUM_FIELD1 CATEGORY_CUSTOM_NUM_FIELD1,E.CUSTOM_NUM_FIELD2 CATEGORY_CUSTOM_NUM_FIELD2,E.CUSTOM_NUM_FIELD3 CATEGORY_CUSTOM_NUM_FIELD3,E.CUSTOM_TEXT_FIELD1 CATEGORY_CUSTOM_TEXT_FIELD1,E.CUSTOM_TEXT_FIELD2 CATEGORY_CUSTOM_TEXT_FIELD2,E.CUSTOM_TEXT_FIELD3 CATEGORY_CUSTOM_TEXT_FIELD3,E.IMAGE_FILE CATEGORY_IMAGE_FILE,E.IMAGE_FILE2 CATEGORY_IMAGE_FILE2,E.IMAGE_NAME CATEGORY_IMAGE_NAME,E.IMAGE_NAME2 CATEGORY_IMAGE_NAME2,E.IMAGE_TYPE CATEGORY_IMAGE_TYPE,E.IMAGE_TYPE2 CATEGORY_IMAGE_TYPE2,E.SHORT_DESC CATEGORY_SHORT_DESC FROM '+ @TABLE_NAME +' A  
												JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID 
												JOIN TB_CATEGORY E ON A.CATEGORY_ID = E.CATEGORY_ID   AND E.PUBLISH2EXPORT=1'
				SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY + ',E.CUSTOM_NUM_FIELD1 CATEGORY_CUSTOM_NUM_FIELD1,E.CUSTOM_NUM_FIELD2 CATEGORY_CUSTOM_NUM_FIELD2,E.CUSTOM_NUM_FIELD3 CATEGORY_CUSTOM_NUM_FIELD3,E.CUSTOM_TEXT_FIELD1 CATEGORY_CUSTOM_TEXT_FIELD1,E.CUSTOM_TEXT_FIELD2 CATEGORY_CUSTOM_TEXT_FIELD2,E.CUSTOM_TEXT_FIELD3 CATEGORY_CUSTOM_TEXT_FIELD3,E.IMAGE_FILE CATEGORY_IMAGE_FILE,E.IMAGE_FILE2 CATEGORY_IMAGE_FILE2,E.IMAGE_NAME CATEGORY_IMAGE_NAME,E.IMAGE_NAME2 CATEGORY_IMAGE_NAME2,E.IMAGE_TYPE CATEGORY_IMAGE_TYPE,E.IMAGE_TYPE2 CATEGORY_IMAGE_TYPE2,E.SHORT_DESC CATEGORY_SHORT_DESC,C.PARENT_FAMILY_ID AS FAMILY_ID,(SELECT TF.FAMILY_NAME FROM TB_FAMILY TF WHERE TF.FAMILY_ID = C.PARENT_FAMILY_ID )FAMILY_NAME,C.FAMILY_ID AS SUBFAMILY_ID,C.FAMILY_NAME AS SUBFAMILY_NAME,C.FOOT_NOTES FAMILY_FOOT_NOTES,C.STATUS FAMILY_STATUS FROM '+ @TABLE_NAME +' A 
												JOIN TB_CATALOG_FAMILY B ON 
												A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'   AND B.FLAG_RECYCLE =''''''''A''''''''
												JOIN TB_FAMILY C ON C.FAMILY_ID = B.FAMILY_ID AND C.ROOT_FAMILY = 0 AND C.WORKFLOW_STATUS=99   AND C.PUBLISH2EXPORT=1
												JOIN TB_CATALOG D ON D.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY E ON A.CATEGORY_ID = E.CATEGORY_ID  AND E.FLAG_RECYCLE =''''''''A''''''''  AND E.PUBLISH2EXPORT=1
												JOIN TB_CATALOG_SECTIONS CS ON E.CATEGORY_ID = CS.CATEGORY_ID  AND CS.FLAG_RECYCLE =''''''''A'''''''' AND CS.CATALOG_ID ='+ @CATALOG_ID +' '
				SET @CATEGORY_SQL = @CATEGORY_SQL + ',E.CUSTOM_NUM_FIELD1 CATEGORY_CUSTOM_NUM_FIELD1,E.CUSTOM_NUM_FIELD2 CATEGORY_CUSTOM_NUM_FIELD2,E.CUSTOM_NUM_FIELD3 CATEGORY_CUSTOM_NUM_FIELD3,E.CUSTOM_TEXT_FIELD1 CATEGORY_CUSTOM_TEXT_FIELD1,E.CUSTOM_TEXT_FIELD2 CATEGORY_CUSTOM_TEXT_FIELD2,E.CUSTOM_TEXT_FIELD3 CATEGORY_CUSTOM_TEXT_FIELD3,E.IMAGE_FILE CATEGORY_IMAGE_FILE,E.IMAGE_FILE2 CATEGORY_IMAGE_FILE2,E.IMAGE_NAME CATEGORY_IMAGE_NAME,E.IMAGE_NAME2 CATEGORY_IMAGE_NAME2,E.IMAGE_TYPE CATEGORY_IMAGE_TYPE,E.IMAGE_TYPE2 CATEGORY_IMAGE_TYPE2,E.SHORT_DESC CATEGORY_SHORT_DESC,B.FAMILY_ID,C.FAMILY_NAME,NULL AS SUBFAMILY_ID,CONVERT(VARCHAR(MAX),NULL) AS SUBFAMILY_NAME,C.FOOT_NOTES FAMILY_FOOT_NOTES,C.STATUS FAMILY_STATUS FROM '+ @TABLE_NAME +' A 
									JOIN TB_CATALOG_FAMILY B ON 
									 
									A.CATEGORY_ID = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'  AND B.FLAG_RECYCLE =''''''''A''''''''
									JOIN TB_FAMILY C ON C.FAMILY_ID = B.FAMILY_ID AND C.ROOT_FAMILY = 1 AND C.WORKFLOW_STATUS=99  AND C.FLAG_RECYCLE =''''''''A''''''''  AND C.PUBLISH2EXPORT=1
									JOIN TB_CATALOG D ON D.CATALOG_ID = B.CATALOG_ID
									JOIN TB_CATEGORY E ON A.CATEGORY_ID = E.CATEGORY_ID  AND E.FLAG_RECYCLE =''''''''A''''''''  AND E.PUBLISH2EXPORT=1
									JOIN TB_CATALOG_SECTIONS CS ON E.CATEGORY_ID = CS.CATEGORY_ID  AND CS.FLAG_RECYCLE =''''''''A'''''''' AND CS.CATALOG_ID ='+ @CATALOG_ID +' '
			END
		ELSE
			BEGIN
			print's'
				SET @TEMP_COUNT_INNER = 1;
				WHILE(@TEMP_COUNT_INNER <> (@CNT + 1))
					BEGIN						
						IF((@TEMP_COUNT_INNER - 1) < @COUNT)
							BEGIN
								IF(@TEMP_COUNT_INNER = 1)
									BEGIN
										SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',A.CATEGORY_SHORT as CATEGORY_ID,A.CATEGORY_NAME';
										SET @CATEGORY_SQL = @CATEGORY_SQL +',A.CATEGORY_SHORT as CATEGORY_ID,A.CATEGORY_NAME';
										SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',A.CATEGORY_SHORT as CATEGORY_ID,A.CATEGORY_NAME';
									END
								ELSE
									BEGIN
									    SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] ,A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
										SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
										SET @CATEGORY_SQL = @CATEGORY_SQL +',A.[SUBCATSHORT_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'] AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],A.[SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
									END
							END
						ELSE
							BEGIN
								SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
								SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
								SET @CATEGORY_SQL = @CATEGORY_SQL +',NULL AS [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +'],NULL AS [SUBCATNAME_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT_INNER - 1)) +']';
							END
						SET @TEMP_COUNT_INNER = @TEMP_COUNT_INNER + 1;
						
					END
				SET @CATEGORY_SQL_WITHOUT_FAMILES = @CATEGORY_SQL_WITHOUT_FAMILES + ',E.CUSTOM_NUM_FIELD1 CATEGORY_CUSTOM_NUM_FIELD1,E.CUSTOM_NUM_FIELD2 CATEGORY_CUSTOM_NUM_FIELD2,E.CUSTOM_NUM_FIELD3 CATEGORY_CUSTOM_NUM_FIELD3,E.CUSTOM_TEXT_FIELD1 CATEGORY_CUSTOM_TEXT_FIELD1,E.CUSTOM_TEXT_FIELD2 CATEGORY_CUSTOM_TEXT_FIELD2,E.CUSTOM_TEXT_FIELD3 CATEGORY_CUSTOM_TEXT_FIELD3,E.IMAGE_FILE CATEGORY_IMAGE_FILE,E.IMAGE_FILE2 CATEGORY_IMAGE_FILE2,E.IMAGE_NAME CATEGORY_IMAGE_NAME,E.IMAGE_NAME2 CATEGORY_IMAGE_NAME2,E.IMAGE_TYPE CATEGORY_IMAGE_TYPE,E.IMAGE_TYPE2 CATEGORY_IMAGE_TYPE2,E.SHORT_DESC CATEGORY_SHORT_DESC FROM '+ @TABLE_NAME +' A 
												JOIN TB_CATALOG_SECTIONS B ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_CATALOG C ON C.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY E ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = E.CATEGORY_ID UNION '
				SET @CATEGORY_SQL = @CATEGORY_SQL + ',E.CUSTOM_NUM_FIELD1 CATEGORY_CUSTOM_NUM_FIELD1,E.CUSTOM_NUM_FIELD2 CATEGORY_CUSTOM_NUM_FIELD2,E.CUSTOM_NUM_FIELD3 CATEGORY_CUSTOM_NUM_FIELD3,E.CUSTOM_TEXT_FIELD1 CATEGORY_CUSTOM_TEXT_FIELD1,E.CUSTOM_TEXT_FIELD2 CATEGORY_CUSTOM_TEXT_FIELD2,E.CUSTOM_TEXT_FIELD3 CATEGORY_CUSTOM_TEXT_FIELD3,E.IMAGE_FILE CATEGORY_IMAGE_FILE,E.IMAGE_FILE2 CATEGORY_IMAGE_FILE2,E.IMAGE_NAME CATEGORY_IMAGE_NAME,E.IMAGE_NAME2 CATEGORY_IMAGE_NAME2,E.IMAGE_TYPE CATEGORY_IMAGE_TYPE,E.IMAGE_TYPE2 CATEGORY_IMAGE_TYPE2,E.SHORT_DESC CATEGORY_SHORT_DESC,B.FAMILY_ID,C.FAMILY_NAME,NULL AS SUBFAMILY_ID,CONVERT(VARCHAR(MAX),NULL) AS SUBFAMILY_NAME,C.FOOT_NOTES FAMILY_FOOT_NOTES,C.STATUS FAMILY_STATUS FROM '+ @TABLE_NAME +' A 
									JOIN TB_CATALOG_FAMILY B ON 
									A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
									JOIN TB_FAMILY C ON C.FAMILY_ID = B.FAMILY_ID AND C.ROOT_FAMILY = 1 AND C.WORKFLOW_STATUS=99 
									JOIN TB_CATALOG D ON D.CATALOG_ID = B.CATALOG_ID
									JOIN TB_CATEGORY E ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = E.CATEGORY_ID
									JOIN TB_CATALOG_SECTIONS CS ON E.CATEGORY_ID = CS.CATEGORY_ID AND CS.CATALOG_ID ='+ @CATALOG_ID +' UNION '
				SET @CATEGORY_SQL_SUBFAMILY = @CATEGORY_SQL_SUBFAMILY + ',E.CUSTOM_NUM_FIELD1 CATEGORY_CUSTOM_NUM_FIELD1,E.CUSTOM_NUM_FIELD2 CATEGORY_CUSTOM_NUM_FIELD2,E.CUSTOM_NUM_FIELD3 CATEGORY_CUSTOM_NUM_FIELD3,E.CUSTOM_TEXT_FIELD1 CATEGORY_CUSTOM_TEXT_FIELD1,E.CUSTOM_TEXT_FIELD2 CATEGORY_CUSTOM_TEXT_FIELD2,E.CUSTOM_TEXT_FIELD3 CATEGORY_CUSTOM_TEXT_FIELD3,E.IMAGE_FILE CATEGORY_IMAGE_FILE,E.IMAGE_FILE2 CATEGORY_IMAGE_FILE2,E.IMAGE_NAME CATEGORY_IMAGE_NAME,E.IMAGE_NAME2 CATEGORY_IMAGE_NAME2,E.IMAGE_TYPE CATEGORY_IMAGE_TYPE,E.IMAGE_TYPE2 CATEGORY_IMAGE_TYPE2,E.SHORT_DESC CATEGORY_SHORT_DESC,C.PARENT_FAMILY_ID AS FAMILY_ID,(SELECT TF.FAMILY_NAME FROM TB_FAMILY TF WHERE TF.FAMILY_ID = C.PARENT_FAMILY_ID)FAMILY_NAME,C.FAMILY_ID AS SUBFAMILY_ID,C.FAMILY_NAME AS SUBFAMILY_NAME,C.FOOT_NOTES FAMILY_FOOT_NOTES,C.STATUS FAMILY_STATUS FROM '+ @TABLE_NAME +' A 
												JOIN TB_CATALOG_FAMILY B ON 
												A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = B.CATEGORY_ID AND B.CATALOG_ID = '+ @CATALOG_ID +'
												JOIN TB_FAMILY C ON C.FAMILY_ID = B.FAMILY_ID AND C.ROOT_FAMILY = 0 AND C.WORKFLOW_STATUS=99
												JOIN TB_CATALOG D ON D.CATALOG_ID = B.CATALOG_ID
												JOIN TB_CATEGORY E ON A.SUBCATID_L'+ CONVERT(VARCHAR(MAX),(@COUNT - 1)) +' = E.CATEGORY_ID
												JOIN TB_CATALOG_SECTIONS CS ON E.CATEGORY_ID = CS.CATEGORY_ID AND CS.CATALOG_ID ='+ @CATALOG_ID +' UNION '
			END
		SET @COUNT = @COUNT - 1;
	END
--SELECT @EEEE

DECLARE @SQL_FAMILY VARCHAR(MAX);


SET @SQL_FAMILY = '
DECLARE @ATTRIBUTE_NAMES VARCHAR(MAX);

;WITH PVT_TEMP_CTE
AS
(
	SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
	FROM            TB_ATTRIBUTE INNER JOIN
                         TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
                         (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
	WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
)
SELECT @ATTRIBUTE_NAMES = (SELECT  SUBSTRING((SELECT '',['' +  REPLACE(ATTRIBUTE_NAME,'']'','']]'') + '']'' FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
SELECT @ATTRIBUTE_NAMES = REPLACE(@ATTRIBUTE_NAMES,''&#x0D;'',CHAR(13))

DECLARE @ATTRIBUTE_NAMES1 VARCHAR(MAX);

;WITH PVT_TEMP_CTE1
AS
(
	SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
	FROM            TB_ATTRIBUTE INNER JOIN
							 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
							 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (1,3,4,6)
	WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
)
SELECT @ATTRIBUTE_NAMES1 = (SELECT  SUBSTRING((SELECT '',['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM PVT_TEMP_CTE1 FOR XML PATH('''')),2,200000))
SELECT @ATTRIBUTE_NAMES1 = REPLACE(@ATTRIBUTE_NAMES1,''&#x0D;'',CHAR(13))
PRINT @ATTRIBUTE_NAMES1
--PRINT @ATTRIBUTE_NAMES
DECLARE @FAMILY_ROWS_COUNT INT = 0;
		;WITH FMILY_TEMP_CTE
		AS
		(
			'+ REPLACE(@CATEGORY_SQL,'''''''''','''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''') +'
		)
		SELECT @FAMILY_ROWS_COUNT = 1 FROM FMILY_TEMP_CTE
		
		DECLARE @PRODUCT_ROWS_COUNT INT = 0;
		IF(@FAMILY_ROWS_COUNT = 1)
			BEGIN
				;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''') +'
				)
				,PRODUCT_CTE
				AS
				(					
					SELECT DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1,tpf.PRODUCT_ID
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''0'' AND tcf.FAMILY_ID=tpf.FAMILY_ID
					and tcf.CATALOG_ID=' + @CATALOG_ID + ' 
					UNION
					SELECT DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1,NULL
					FROM TB_CATALOG_FAMILY tcf						
					,TB_FAMILY tpf  
					where TCF.ROOT_CATEGORY = ''0'' 
					and tcf.CATALOG_ID=' + @CATALOG_ID + '  AND  TCF.FAMILY_ID NOT IN(SELECT	tcf.FAMILY_ID 
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''0'' AND tcf.FAMILY_ID=tpf.FAMILY_ID
					and tcf.CATALOG_ID=' + @CATALOG_ID + ' )
				)
				,FINALE_PRODUCTS
				AS
				(
					SELECT DISTINCT  * FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
					UNION
					SELECT DISTINCT  * FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				)
				SELECT @PRODUCT_ROWS_COUNT = 1 FROM FINALE_PRODUCTS
			END
PRINT @FAMILY_ROWS_COUNT;
PRINT @PRODUCT_ROWS_COUNT;
DECLARE @SQLFAM NVARCHAR(MAX);
DECLARE @SQL1 NVARCHAR(MAX);
DECLARE @FAMILY_SPECS_VALUES VARCHAR(MAX) = '''';
DECLARE @PRODUCT_SPECS_VALUES VARCHAR(MAX) = '''';

IF(@PRODUCT_ROWS_COUNT = 0 AND @FAMILY_ROWS_COUNT = 0)
	BEGIN
		IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
			print ''not null f&p begin5''
			SET @SQL1 = '';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUUBFAMILY_ID,NULL AS SUBFAMILY_NAME,NULL AS PRODUCT_ID FROM FAMILY_TEMP_CTE'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN				
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
										 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
										 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				PRINT (@FAMILY_SPECS_VALUES)
				SET @SQL1 = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES + '
				)
				
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,'' + @FAMILY_SPECS_VALUES + '' INTO '+ @FTABMAINFAM + 'FAM  FROM FAMILY_TEMP_CTE'''')'';
						
				SET @SQL1 = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES + '
				)				
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,'' + @FAMILY_SPECS_VALUES + ''
				,NULL AS PRODUCT_ID FROM FAMILY_TEMP_CTE'''')'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN	
					PRINT 3	
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	

	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							  join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  AND ta.ATTRIBUTE_TYPE IN (1,3,4,6) and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')

				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
											

				SET @SQL1 = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES + '
				)				
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,NULL AS PRODUCT_ID,'' + @PRODUCT_SPECS_VALUES + '' FROM FAMILY_TEMP_CTE'''')'';				
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN
			PRINT 4
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
										 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
										 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))				
				print @FAMILY_SPECS_VALUES
				;WITH PVT_TEMP_CTE
				AS
				(
					select distinct ATTRIBUTE_NAME,ta.ATTRIBUTE_ID from (
	

	select distinct tca.ATTRIBUTE_ID from TB_CATALOG_ATTRIBUTES tca  join TB_PROD_SPECS tps on tca.ATTRIBUTE_ID=tps.ATTRIBUTE_ID
							  join TB_SUBPRODUCT tp on tp.PRODUCT_ID=tps.PRODUCT_ID where tca.CATALOG_ID = ' + @CATALOG_ID + ' and tca.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
 ) as commanAttr join TB_ATTRIBUTE ta on ta.ATTRIBUTE_ID=commanAttr.ATTRIBUTE_ID  AND ta.ATTRIBUTE_TYPE IN (1,3,4,6)  and ta.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				
				
				SET @SQL1 = ''	EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL_WITHOUT_FAMILES + '
				)				
				SELECT DISTINCT *,NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS SUBFAMILY_ID,NULL AS SUBFAMILY_NAME,NULL AS PRODUCT_ID,'' + @PRODUCT_SPECS_VALUES + ''FROM FAMILY_TEMP_CTE'''')'';				
			END	
	END
ELSE IF(@PRODUCT_ROWS_COUNT = 1 AND @FAMILY_ROWS_COUNT = 1)
	BEGIN
	print ''not null f&p begin3''
		IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
			print ''not null f&p begin4''
			SET @SQL1 = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION 
					'+ REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'					
				)
				,PRODUCT_CTE
				AS
				(
					 
				    SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FAMILY_ID=tpf.FAMILY_ID
					and tcf.CATALOG_ID=' + @CATALOG_ID + ' 
					UNION
					SELECT	DISTINCT tcf.FAMILY_ID AS FAMILY_ID_1
					FROM TB_CATALOG_FAMILY tcf						
					,TB_FAMILY tpf  
					where TCF.ROOT_CATEGORY = ''''0''''
					and tcf.CATALOG_ID=' + @CATALOG_ID + '  AND  TCF.FAMILY_ID NOT IN(SELECT	tcf.FAMILY_ID 
					FROM TB_CATALOG_FAMILY tcf						
					,TB_PROD_FAMILY tpf 
					where TCF.ROOT_CATEGORY = ''''0'''' AND tcf.FAMILY_ID=tpf.FAMILY_ID
					and tcf.CATALOG_ID=' + @CATALOG_ID + ' )
						
				)
				SELECT DISTINCT *  FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
				UNION
				SELECT DISTINCT * FROM FMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1 FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1 FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL,,NULL FROM 
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				ORDER BY FAMILY_ID
					'';									
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
			print ''not null f&p begin2''
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
									TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID 
									IN (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				PRINT (@FAMILY_SPECS_VALUES)
				
				
				
				SET @SQL1 = ''
				;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION 
					'+ REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT * FROM 
					(
						SELECT DISTINCT	A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Tex%''''
							THEN D.STRING_VALUE + CASE WHEN D.OBJECT_NAME IS NOT NULL AND D.OBJECT_NAME <> '''''''' THEN ''''[Image_Name:''''+ D.OBJECT_NAME +'''']'''' ELSE '''''''' END  
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Date%''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Hyper%''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL 
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT DISTINCT	A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT DISTINCT	A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
							SELECT DISTINCT	A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Tex%''''
							THEN D.STRING_VALUE + CASE WHEN D.OBJECT_NAME IS NOT NULL AND D.OBJECT_NAME <> '''''''' THEN ''''[Image_Name:''''+ D.OBJECT_NAME +'''']'''' ELSE '''''''' END  
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
						WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Date%''''
						THEN CONVERT(VARCHAR(MAX),D.string_value)						
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Hyper%''''
						THEN CONVERT(VARCHAR(MAX),D.string_value)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)
				,PRODUCT_CTE
				AS
				(
					SELECT	TCF.FAMILY_ID AS FAMILY_ID_1,TPF.PRODUCT_ID
					FROM TB_CATALOG_FAMILY TCF						
					,TB_PROD_FAMILY TPF 
					WHERE TCF.ROOT_CATEGORY = ''''0'''' AND TCF.FAMILY_ID=TPF.FAMILY_ID
					AND TCF.CATALOG_ID=' + @CATALOG_ID + ' 
					UNION
					SELECT	TCF.FAMILY_ID AS FAMILY_ID_1,NULL
					FROM TB_CATALOG_FAMILY TCF						
					,TB_FAMILY TPF  
					WHERE TCF.ROOT_CATEGORY = ''''0''''
					AND TCF.CATALOG_ID=' + @CATALOG_ID + '  AND  TCF.FAMILY_ID NOT IN(SELECT	TCF.FAMILY_ID 
					FROM TB_CATALOG_FAMILY TCF						
					,TB_PROD_FAMILY TPF 
					WHERE TCF.ROOT_CATEGORY = ''''0'''' AND TCF.FAMILY_ID=TPF.FAMILY_ID
					AND TCF.CATALOG_ID=' + @CATALOG_ID + ' )
				)
				SELECT DISTINCT   *  FROM TEM_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
				UNION
				SELECT DISTINCT   * FROM TEM_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT   *,''+ @FAMILY_SPECS_VALUES + '',NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT   *,''+ @FAMILY_SPECS_VALUES + '',NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,''+ @FAMILY_SPECS_VALUES + '',NULL,NULL FROM 
				(				
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN
			print ''not null f&p begin1''
			;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
										 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
										 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (1,3,4,6)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				
				
				SET @SQL1 = ''EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL + ' UNION ' + @CATEGORY_SQL_SUBFAMILY +'
				)				
				,PRODUCT_CTE
				AS
				(
					SELECT DISTINCT FAMILY_ID_1,PRODUCT_ID ,'' + @ATTRIBUTE_NAMES1 + '' FROM 
					(
						SELECT DISTINCT	tcf.FAMILY_ID AS FAMILY_ID_1,tps.PRODUCT_ID ,CASE 
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Tex%''''''''
							THEN tps.STRING_VALUE + CASE WHEN tps.OBJECT_NAME IS NOT NULL AND tps.OBJECT_NAME <> '''''''''''''''' THEN ''''''''[Image_Name:''''''''+ tps.OBJECT_NAME +'''''''']'''''''' ELSE '''''''''''''''' END
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(tps.NUMERIC_VALUE, 0 ))
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Date%''''''''
							THEN CONVERT(VARCHAR(MAX),tps.STRING_VALUE)
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Hyperlink%''''''''
							THEN CONVERT(VARCHAR(MAX),tps.STRING_VALUE)
						END AS STRING_VALUE
						,ta.ATTRIBUTE_NAME
						FROM TB_CATALOG_FAMILY tcf
						,TB_PROD_SPECS tps
						,TB_PROD_FAMILY tpf 
						,TB_ATTRIBUTE ta 
						where TCF.ROOT_CATEGORY = ''''''''0'''''''' AND tcf.FAMILY_ID=tpf.FAMILY_ID 
						and ta.ATTRIBUTE_ID=tps.ATTRIBUTE_ID 
						and tps.PRODUCT_ID=tpf.PRODUCT_ID
						and tcf.CATALOG_ID=' + @CATALOG_ID + ' 	AND tpf.FLAG_RECYCLE =''''''''A''''''''				
						UNION
						SELECT DISTINCT	tcf.FAMILY_ID AS FAMILY_ID_1,tps.PRODUCT_ID ,TPS.ATTRIBUTE_VALUE
						,ta.ATTRIBUTE_NAME
						FROM TB_CATALOG_FAMILY tcf
						,TB_PARTS_KEY tps
						,TB_PROD_FAMILY tpf 
						,TB_ATTRIBUTE ta 
						where TCF.ROOT_CATEGORY = ''''''''0'''''''' AND tcf.FAMILY_ID=tpf.FAMILY_ID AND tpf.FLAG_RECYCLE =''''''''A''''''''
						and ta.ATTRIBUTE_ID=tps.ATTRIBUTE_ID 
						and tps.PRODUCT_ID=tpf.PRODUCT_ID
						and tps.FAMILY_ID=tpf.FAMILY_ID 
						and tcf.CATALOG_ID=tps.CATALOG_ID 
						and tcf.CATALOG_ID=' + @CATALOG_ID + ' 
						AND TCF.CATEGORY_ID = TPS.CATEGORY_ID AND TPS.CATALOG_ID = ' + @CATALOG_ID + '  AND tpf.FLAG_RECYCLE =''''''''A''''''''
					)AS P1 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES1 + '')
					) AS PV1
				)
				SELECT DISTINCT  * INTO '+ @FTABMAIN + ' FROM FAMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
 				UNION
				SELECT DISTINCT * FROM FAMILY_TEMP_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID,''+ @PRODUCT_SPECS_VALUES +'' FROM FAMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID,''+ @PRODUCT_SPECS_VALUES +'' FROM FAMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,''+ @PRODUCT_SPECS_VALUES +'' FROM 
				(				
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				ORDER BY FAMILY_ID
				'''')
 ALTER TABLE '+ @FTABMAIN + ' ADD PRODUCT_PUBLISH2WEB nvarchar(10)
 ALTER TABLE '+ @FTABMAIN + ' ADD PRODUCT_PUBLISH2PRINT nvarchar(10)
 ALTER TABLE '+ @FTABMAIN + ' ADD PRODUCT_PUBLISH2PDF nvarchar(10)


 UPDATE T SET PRODUCT_PUBLISH2WEB = CASE WHEN C.PUBLISH2WEB =1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON  T.PRODUCT_ID= C.PRODUCT_ID 
 UPDATE T SET PRODUCT_PUBLISH2PDF = CASE WHEN  C.PUBLISH2PDF =1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID 
UPDATE T SET PRODUCT_PUBLISH2PRINT =  CASE WHEN C.PUBLISH2PRINT=1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID 
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN	
		     	print ''not null f&p begin147''	
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
									TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID 
									IN (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
										 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
										 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (1,3,4,6)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				
				
				
				SET @SQL1 = ''EXEC('''';WITH FAMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL + ' UNION ' + @CATEGORY_SQL_SUBFAMILY +'
				)	
				
				,TEM_CTE
				AS
				(
					SELECT * FROM 
					(
						SELECT	A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Tex%''''''''
							THEN D.STRING_VALUE + CASE WHEN D.OBJECT_NAME IS NOT NULL AND D.OBJECT_NAME <> '''''''''''''''' THEN ''''''''[Image_Name:''''''''+ D.OBJECT_NAME +'''''''']'''''''' ELSE '''''''''''''''' END  
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Date%''''''''
							THEN CONVERT(VARCHAR,D.STRING_VALUE)
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Hyper%''''''''
							THEN CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FAMILY_TEMP_CTE A JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL
						JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT	A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Tex%''''''''
							THEN D.STRING_VALUE + CASE WHEN D.OBJECT_NAME IS NOT NULL AND D.OBJECT_NAME <> '''''''''''''''' THEN ''''''''[Image_Name:''''''''+ D.OBJECT_NAME +'''''''']'''''''' ELSE '''''''''''''''' END  
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Date%''''''''
							THEN CONVERT(VARCHAR,D.STRING_VALUE)
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Hyper%''''''''
							THEN CONVERT(VARCHAR,D.STRING_VALUE)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FAMILY_TEMP_CTE A JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID AND A.SUBFAMILY_ID IS NOT NULL
						JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT	A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FAMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						UNION
						SELECT	A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FAMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						UNION ALL
						SELECT A.*,NULL AS STRING_VALUE,NULL AS ATTRIBUTE_NAME 
						FROM FAMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT DISTINCT F.FAMILY_ID FROM TB_FAMILY_SPECS F) 
						AND A.SUBFAMILY_ID IS NULL
						UNION ALL
						SELECT A.*,NULL AS STRING_VALUE,NULL AS ATTRIBUTE_NAME 
						FROM FAMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT DISTINCT F.FAMILY_ID FROM TB_FAMILY_SPECS F) 
						AND A.SUBFAMILY_ID IS NOT NULL
						
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)
				
				
				
				
							
				,PRODUCT_CTE
				AS
				(
					SELECT DISTINCT FAMILY_ID_1,PRODUCT_ID ,'' + @ATTRIBUTE_NAMES1 + '' FROM 
					(
						SELECT DISTINCT	tcf.FAMILY_ID AS FAMILY_ID_1,tps.PRODUCT_ID,CASE 
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Tex%''''''''
							THEN tps.STRING_VALUE + CASE WHEN tps.OBJECT_NAME IS NOT NULL AND tps.OBJECT_NAME <> '''''''''''''''' THEN ''''''''[Image_Name:''''''''+ tps.OBJECT_NAME +'''''''']'''''''' ELSE '''''''''''''''' END
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(tps.NUMERIC_VALUE, 0 ))
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Date%''''''''
							THEN CONVERT(VARCHAR(MAX),tps.STRING_VALUE)
						WHEN ta.ATTRIBUTE_DATATYPE LIKE ''''''''%Hyperlink%''''''''
							THEN CONVERT(VARCHAR(MAX),tps.STRING_VALUE)
						END AS STRING_VALUE
						,ta.ATTRIBUTE_NAME
						FROM TB_CATALOG_FAMILY tcf
						,TB_PROD_SPECS tps
						,TB_PROD_FAMILY tpf 
						,TB_ATTRIBUTE ta 
						where TCF.ROOT_CATEGORY = ''''''''0'''''''' AND tcf.FAMILY_ID=tpf.FAMILY_ID 
						and ta.ATTRIBUTE_ID=tps.ATTRIBUTE_ID 
						and tps.PRODUCT_ID=tpf.PRODUCT_ID
						and tcf.FLAG_RECYCLE=''''''''A''''''''
						and tcf.CATALOG_ID=' + @CATALOG_ID + ' 	AND tpf.FLAG_RECYCLE =''''''''A''''''''					
						UNION
						SELECT DISTINCT	tcf.FAMILY_ID AS FAMILY_ID_1,tps.PRODUCT_ID ,TPS.ATTRIBUTE_VALUE
						,ta.ATTRIBUTE_NAME
						FROM TB_CATALOG_FAMILY tcf
						,TB_PARTS_KEY tps
						,TB_PROD_FAMILY tpf 
						,TB_ATTRIBUTE ta 
						where TCF.ROOT_CATEGORY = ''''''''0'''''''' AND tcf.FAMILY_ID=tpf.FAMILY_ID AND tpf.FLAG_RECYCLE =''''''''A''''''''
						and ta.ATTRIBUTE_ID=tps.ATTRIBUTE_ID 
						and tps.PRODUCT_ID=tpf.PRODUCT_ID
						and tps.FAMILY_ID=tpf.FAMILY_ID 
						and tcf.CATALOG_ID=tps.CATALOG_ID
						and tcf.FLAG_RECYCLE=''''''''A''''''''
						and tcf.CATALOG_ID=' + @CATALOG_ID + ' 
						AND TCF.CATEGORY_ID = TPS.CATEGORY_ID AND TPS.CATALOG_ID = ' + @CATALOG_ID + ' AND tpf.FLAG_RECYCLE =''''''''A''''''''
					)AS P1 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES1 + '')
					) AS PV1
				)

				SELECT DISTINCT  * INTO '+ @FTABMAIN + '  FROM TEM_CTE A JOIN PRODUCT_CTE B ON A.FAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NULL
 				UNION
				SELECT DISTINCT * FROM TEM_CTE A JOIN PRODUCT_CTE B ON A.SUBFAMILY_ID = B.FAMILY_ID_1 AND A.SUBFAMILY_ID IS NOT NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID,''+ @PRODUCT_SPECS_VALUES +'' FROM TEM_CTE A WHERE A.FAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NULL
				UNION ALL
				SELECT DISTINCT *,NULL AS FAMILY_ID_1,NULL AS PRODUCT_ID,''+ @PRODUCT_SPECS_VALUES +'' FROM TEM_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT FAMILY_ID_1 FROM PRODUCT_CTE) 
				AND A.SUBFAMILY_ID IS NOT NULL
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,''+ @FAMILY_SPECS_VALUES +'',NULL,NULL,''+ @PRODUCT_SPECS_VALUES +'' FROM 
				(				
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				ORDER BY FAMILY_ID
				'''')
				


  ALTER TABLE '+ @FTABMAIN + ' ADD PRODUCT_PUBLISH2WEB nvarchar(10)
 ALTER TABLE '+ @FTABMAIN + ' ADD PRODUCT_PUBLISH2PRINT nvarchar(10)
 ALTER TABLE '+ @FTABMAIN + ' ADD PRODUCT_PUBLISH2PDF nvarchar(10)

 

 UPDATE T SET PRODUCT_PUBLISH2WEB = CASE WHEN C.PUBLISH2WEB =1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON  T.PRODUCT_ID= C.PRODUCT_ID 
 UPDATE T SET PRODUCT_PUBLISH2PDF = CASE WHEN  C.PUBLISH2PDF =1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID 
UPDATE T SET PRODUCT_PUBLISH2PRINT =  CASE WHEN C.PUBLISH2PRINT=1 THEN ''''Y'''' ELSE ''''N'''' END FROM '+ @FTABMAIN + ' T JOIN TB_PROD_FAMILY C ON T.PRODUCT_ID= C.PRODUCT_ID 





					'';


					print ''familyspc''
					--select @SQLFAM
					print @FAMILY_SPECS_VALUES
					print ''not null f&p end''

			END		
	END
ELSE IF(@PRODUCT_ROWS_COUNT = 0 AND @FAMILY_ROWS_COUNT = 1)
	BEGIN
		IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
			print(''a'')
				SET @SQL1 = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				SELECT *,NULL AS PRODUCT_ID FROM FMILY_TEMP_CTE
				UNION
				SELECT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL FROM
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NULL OR @ATTRIBUTE_NAMES1 = ''''))
			BEGIN
			print(''b'')
			;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
									TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID 
									IN (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))				
				SET @SQL1 = ''	;WITH FMILY_TEMP_CTE
				AS
				(
					'+ REPLACE(@CATEGORY_SQL,'''''''''','''''') + ' UNION ' + REPLACE(@CATEGORY_SQL_SUBFAMILY,'''''''''','''''') +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT * FROM 
					(
							SELECT DISTINCT	A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Tex%''''
							THEN D.STRING_VALUE + CASE WHEN D.OBJECT_NAME IS NOT NULL AND D.OBJECT_NAME <> '''''''' THEN ''''[Image_Name:''''+ D.OBJECT_NAME +'''']'''' ELSE '''''''' END  
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Date%''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Hyper%''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT DISTINCT	A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Tex%''''
							THEN D.STRING_VALUE + CASE WHEN D.OBJECT_NAME IS NOT NULL AND D.OBJECT_NAME <> '''''''' THEN ''''[Image_Name:''''+ D.OBJECT_NAME +'''']'''' ELSE '''''''' END  
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Num%''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Date%''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''%Hyper%''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						LEFT JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT DISTINCT	A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION
						SELECT DISTINCT	A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)
				SELECT DISTINCT *,NULL AS PRODUCT_ID FROM TEM_CTE
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,''+ @FAMILY_SPECS_VALUES + '',NULL FROM
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NULL OR @ATTRIBUTE_NAMES = '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN
			print(''a\c'')
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
										 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
										 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (1,3,4,6)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				
				SET @SQL1 = ''	EXEC(''''				
				;WITH FMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL + ' UNION ' + @CATEGORY_SQL_SUBFAMILY +'
				)
				SELECT *,NULL AS PRODUCT_ID,'' + @PRODUCT_SPECS_VALUES + '' FROM FMILY_TEMP_CTE
				UNION
				SELECT *,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'' + @PRODUCT_SPECS_VALUES + '' FROM 
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				'''')
					'';
			END
		ELSE IF((@ATTRIBUTE_NAMES IS NOT NULL OR @ATTRIBUTE_NAMES <> '''') AND (@ATTRIBUTE_NAMES1 IS NOT NULL OR @ATTRIBUTE_NAMES1 <> ''''))
			BEGIN
			print ''not null begin''
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
										 TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID IN 
										 (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (1,3,4,6)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @PRODUCT_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				
				;WITH PVT_TEMP_CTE
				AS
				(
					SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE
					FROM            TB_ATTRIBUTE INNER JOIN
									TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.ATTRIBUTE_ID 
									IN (' + @ATTRIBUTE_IDS + ') AND TB_ATTRIBUTE.ATTRIBUTE_TYPE IN (7,9,11,12,13)
					WHERE        (TB_CATALOG_ATTRIBUTES.CATALOG_ID = ' + @CATALOG_ID + ')
				)
				SELECT @FAMILY_SPECS_VALUES = (SELECT  SUBSTRING((SELECT DISTINCT '',NULL AS ['' + REPLACE (REPLACE(ATTRIBUTE_NAME,'']'','']]''),'''''''','''''''''''') + '']'' 
				FROM PVT_TEMP_CTE FOR XML PATH('''')),2,200000))
				
				
				
				SET @SQL1 = ''	EXEC('''';WITH FMILY_TEMP_CTE
				AS
				(
					'+ @CATEGORY_SQL + ' UNION ' + @CATEGORY_SQL_SUBFAMILY +'
				)
				,TEM_CTE
				AS
				(
					SELECT DISTINCT * FROM 
					(
						SELECT DISTINCT	A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Tex%''''''''
							THEN D.STRING_VALUE + CASE WHEN D.OBJECT_NAME IS NOT NULL AND D.OBJECT_NAME <> '''''''''''''''' THEN ''''''''[Image_Name:''''''''+ D.OBJECT_NAME +'''''''']'''''''' ELSE '''''''''''''''' END  
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Date%''''''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Hyper%''''''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL
						JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT DISTINCT	A.*,CASE 
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Tex%''''''''
							THEN D.STRING_VALUE + CASE WHEN D.OBJECT_NAME IS NOT NULL AND D.OBJECT_NAME <> '''''''''''''''' THEN ''''''''[Image_Name:''''''''+ D.OBJECT_NAME +'''''''']'''''''' ELSE '''''''''''''''' END  
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Num%''''''''
							THEN CONVERT(VARCHAR,ISNULL(D.NUMERIC_VALUE, 0 ))
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Date%''''''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							WHEN E.ATTRIBUTE_DATATYPE LIKE ''''''''%Hyper%''''''''
							THEN CONVERT(VARCHAR(MAX),D.string_value)
							END AS STRING_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A JOIN TB_FAMILY_SPECS D ON D.FAMILY_ID = A.SUBFAMILY_ID
						JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D.ATTRIBUTE_ID
						UNION
						SELECT DISTINCT	A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.FAMILY_ID AND A.SUBFAMILY_ID IS NULL AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +'
						UNION
						SELECT DISTINCT	A.*,D1.ATTRIBUTE_VALUE
						,E.ATTRIBUTE_NAME
						FROM FMILY_TEMP_CTE A 
						 LEFT JOIN TB_FAMILY_KEY D1 ON D1.FAMILY_ID = A.SUBFAMILY_ID AND D1.CATALOG_ID='+ @CATALOG_ID +' 
						 LEFT JOIN TB_ATTRIBUTE E ON E.ATTRIBUTE_ID = D1.ATTRIBUTE_ID
						 LEFT JOIN TB_CATEGORY_FAMILY_ATTR_LIST CFL ON CFL.FAMILY_ID = D1.FAMILY_ID AND CFL.CATEGORY_ID = D1.CATEGORY_ID AND CFL.CATALOG_ID = '+ @CATALOG_ID +' 
						UNION ALL
						SELECT DISTINCT A.*,NULL AS STRING_VALUE,NULL AS ATTRIBUTE_NAME 
						FROM FMILY_TEMP_CTE A WHERE A.FAMILY_ID NOT IN (SELECT DISTINCT F.FAMILY_ID FROM TB_FAMILY_SPECS F) 
						AND A.SUBFAMILY_ID IS NULL
						UNION ALL
						SELECT DISTINCT A.*,NULL AS STRING_VALUE,NULL AS ATTRIBUTE_NAME 
						FROM FMILY_TEMP_CTE A WHERE A.SUBFAMILY_ID NOT IN (SELECT DISTINCT F.FAMILY_ID FROM TB_FAMILY_SPECS F) 
						AND A.SUBFAMILY_ID IS NOT NULL
						
					)AS P 
					PIVOT 
					(
						MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('' + @ATTRIBUTE_NAMES + '')
					) AS PV
				)				
				SELECT DISTINCT *,NULL AS PRODUCT_ID,'' + @PRODUCT_SPECS_VALUES + '' FROM TEM_CTE
				UNION
				SELECT DISTINCT *,NULL,NULL,NULL,NULL,NULL,NULL,'' + @FAMILY_SPECS_VALUES + '',NULL,'' + @PRODUCT_SPECS_VALUES + '' FROM 
				(
					'+ REPLACE(@CATEGORY_SQL_WITHOUT_FAMILES,'''''''''','''''') +'
				)A
				'''')
					'';
					print ''not null end''
			END
	END
	--exec (@SQLFAM)
exec (@SQL1)'
EXEC (@SQL_FAMILY)

EXEC(
'DECLARE @ATTRIBUTE_NAME Nvarchar(50)
DECLARE @attributeDetails Nvarchar(50)
DECLARE ATTRIBUTE_CURSOR CURSOR FOR
SELECT ATTRIBUTE_NAME from TB_ATTRIBUTE TA JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID = TCA.ATTRIBUTE_ID where TA.ATTRIBUTE_ID in (' + @ATTRIBUTE_IDS + ') and TA.ATTRIBUTE_DATATYPE like''num%'' AND TA.ATTRIBUTE_TYPE IN (1,3,4,6,7,9,11,12,13) AND TCA.CATALOG_ID ='+ @CATALOG_ID +'

OPEN ATTRIBUTE_CURSOR

FETCH NEXT FROM ATTRIBUTE_CURSOR
INTO @ATTRIBUTE_NAME

WHILE @@FETCH_STATUS = 0
BEGIN



set  @attributeDetails = ''['' + @ATTRIBUTE_NAME + '']''




print''a''

EXEC(''update '+ @FTABMAIN + ' SET ''+@attributeDetails+''= 0  where ''+@attributeDetails+'' is null'')
print''b''


FETCH NEXT FROM ATTRIBUTE_CURSOR
INTO @ATTRIBUTE_NAME

END
CLOSE ATTRIBUTE_CURSOR;
DEALLOCATE ATTRIBUTE_CURSOR;
')


EXEC ('Select * From '+@FTABMAIN+' WHERE FAMILY_ID IS NOT NULL AND PRODUCT_ID IS NOT NULL AND ITEM# IS NOT NULL' )

EXEC ('Select * From '+@FTABMAIN+' WHERE FAMILY_ID IS NOT NULL AND PRODUCT_ID IS NOT NULL AND ITEM# IS NOT NULL' )


EXEC ('Select * From '+@FTABMAIN+' WHERE FAMILY_ID IS NOT NULL AND PRODUCT_ID IS NOT NULL AND ITEM# IS NOT NULL' )


EXEC ('Select * From '+@FTABMAIN+' WHERE FAMILY_ID IS NOT NULL AND PRODUCT_ID IS NOT NULL AND ITEM# IS NOT NULL' )











GO



ALTER procedure [dbo].[STP_FOR_EDIT_INVERTED_PRODUCTS]
 @family_id varchar(max)=22527,
@product_Id varchar (max)=13447,
 @catalog_id varchar(max)=29,
 @created_user nvarchar(max)='demo@questudio.com',
 @Role_id int=30
 --@PAGENO int=1,
 --@PERPAGECOUNT int=10,
 --@NULLVALUEATTRIBUTE varchar(max)=''

 as 
 

 IF OBJECT_ID('tempdb..##Temp') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##Temp')  
end 

IF OBJECT_ID('tempdb..##Temp1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE  ##Temp1')  
end 

BEGIN

 WITH TTT  
AS  
(  
 
SELECT DISTINCT TCF.CATALOG_ID, TPF.FAMILY_ID,
TPS.PRODUCT_ID, TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME='Supplier' THEN ATTRIBUTE_NAME +'__OBJ__0' +'__'+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +'__OBJ__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) END +'__'+ CASE WHEN CAST(ISNULL(TA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END AS ATTRIBUTE_NAME,TPS.STRING_VALUE   
 from TB_PROD_SPECS TPS
join TB_ATTRIBUTE TA on TPS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID
join TB_PROD_FAMILY TPF on TPF.PRODUCT_ID=TPS.PRODUCT_ID
join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TPF.FAMILY_ID
join TB_CATALOG TC on TC.CATALOG_ID=tcf.CATALOG_ID
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@catalog_id 
where TC.CATALOG_ID=@catalog_id and TCF.FAMILY_ID=@family_id and TPS.PRODUCT_ID=@product_Id and TA.ATTRIBUTE_ID<>3  and TA.ATTRIBUTE_TYPE<>4 and TA.ATTRIBUTE_TYPE not in (7,9,11,12,13)
UNION 

SELECT DISTINCT TCF.CATALOG_ID, TPF.FAMILY_ID,
TPS.PRODUCT_ID, TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME='Supplier' THEN ATTRIBUTE_NAME +'__OBJ__0' +'__'+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +'__OBJ__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) END +'__'+ CASE WHEN CAST(ISNULL(TA.ATTRIBUTE_ID,0) 
  as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END AS
   ATTRIBUTE_NAME,CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE )  
 from TB_PROD_SPECS TPS
join TB_ATTRIBUTE TA on TPS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID
join TB_PROD_FAMILY TPF on TPF.PRODUCT_ID=TPS.PRODUCT_ID
join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TPF.FAMILY_ID
join TB_CATALOG TC on TC.CATALOG_ID=tcf.CATALOG_ID
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@catalog_id
where TC.CATALOG_ID=@catalog_id and TCF.FAMILY_ID=@family_id and TPS.PRODUCT_ID=@product_Id and TA.ATTRIBUTE_ID<>3 and TA.ATTRIBUTE_TYPE=4 and TA.ATTRIBUTE_TYPE not in (7,9,11,12,13)
  
)
  select * into ##Temp from TTT
  




;WITH PROD
as
(
SELECT DISTINCT TCF.CATALOG_ID, TPS.FAMILY_ID , TPS.PRODUCT_ID, TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME+'__' + 'OBJ'+'__'+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END +'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END  AS ATTRIBUTE_NAME
,TPS.ATTRIBUTE_VALUE as STRING_VALUE FROM  TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID  
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@created_user 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID and TA.ATTRIBUTE_TYPE not in (7,9,11,12,13)
)

select * into ##Temp1
---TM.PRODUCT_ID,TM.ATTRIBUTE_ID + PD.ATTRIBUTE_ID as ATTRIBUTE_ID,TM.ATTRIBUTE_NAME+PD.ATTRIBUTE_NAME as ATTRIBUTE_NAME, TM.STRING_VALUE+PD.STRING_VALUE as STRING_VALUE 
from PROD PD
--left join ##Temp TM on TM.PRODUCT_ID=PD.PRODUCT_ID
--order by TM.ATTRIBUTE_ID

;with FinalResult
as
(
select * from ##Temp
union
select * from ##Temp1
)

select * from FinalResult where PRODUCT_ID=@product_Id 


--drop table ##Temp
--drop table ##Temp1

 end

GO



ALTER procedure [dbo].[STP_LS_ATTRIBUTE_IMPORT](
@ATTRNAME NVARCHAR(MAX)='',
@SESSION_ID NVARCHAR(MAX)='4614eac9-7599-4ebd-b1f8-ec0c8f4f2307',
@OPTION Nvarchar(50)='DATATYPE VALIDATION',
@CUSTOMER_ID INT=8,
@CREATED_USER VARCHAR(MAX)='demo@questudio.com' ,
@MODIFIED_USER VARCHAR(MAX)='demo@questudio.com',
@CATALOG_ID INT=8
) 
as
begin
declare @idcount int
                              /*-------------------VALIDATION-------------------*/
if(@OPTION='VALIDATION ATTR_NAMES and caption')
begin
EXEC('
;with cte
As(
select  Attribute_name,Caption from [##IMPORTTEMP'+@SESSION_ID+'] where Attribute_name not in (select Attribute_name from tb_attribute) and Attribute_id is null)
select Attribute_name from cte where Attribute_name <> caption and Attribute_name in (select caption from tb_attribute) 
')

EXEC('
;with cte
As(
select Attribute_name,Caption from [##IMPORTTEMP'+@SESSION_ID+'] where Attribute_name not in (select Attribute_name from tb_attribute) and Attribute_id is null)
select Attribute_name from cte where Attribute_name <> caption and Attribute_name in (select caption from [##IMPORTTEMP'+@SESSION_ID+']) 
')

end

if(@OPTION='DATATYPE VALIDATION')
begin

EXEC('
SELECT ATTRIBUTE_NAME,ATTRIBUTE_TYPE, ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM [##IMPORTTEMP'+@SESSION_ID+'] WHERE ATTRIBUTE_TYPE IN (''Product Specifications'',''Category Specifications'',''Family Specifications'',''Family Key'',''Product Key'',''Category Description'') and ((ATTRIBUTE_DATATYPE like ''Tex%'' and ATTRIBUTE_DATAFORMAT not in (''All Characters'',''Alpha Numeric'',''System default'',''Alphabets Only''))or 
(ATTRIBUTE_DATATYPE like ''Num%'' and ATTRIBUTE_DATAFORMAT not in (''Integer'',''Real Numbers'') )or (ATTRIBUTE_DATATYPE like ''Date%'' and ATTRIBUTE_DATAFORMAT not in(''Short Format (dd/mm/yyyy)'',''Short Format (mm/dd/yyyy)'',''Long Format'',''DateTime'',''System default''))or
(ATTRIBUTE_DATATYPE like ''Hyper%'' and ATTRIBUTE_DATAFORMAT not like ''Hyper%''))

union all 

SELECT ATTRIBUTE_NAME,ATTRIBUTE_TYPE, ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM [##IMPORTTEMP'+@SESSION_ID+'] WHERE ATTRIBUTE_TYPE IN (''Product Image/Attachment'',''Family Image/Attachment'',''Category Image/Attachment'',''Family Description'') and (ATTRIBUTE_DATATYPE <> ''Text'' 
or ATTRIBUTE_DATAFORMAT <> ''All Characters'' )

union all 

SELECT ATTRIBUTE_NAME,ATTRIBUTE_TYPE, ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM [##IMPORTTEMP'+@SESSION_ID+'] WHERE ATTRIBUTE_TYPE in (''Product Price'',''Family Price'') and (ATTRIBUTE_DATATYPE not like ''Number%''
or ATTRIBUTE_DATAFORMAT <> ''Integer'' and ATTRIBUTE_DATAFORMAT <> ''Real Numbers'')

union all

select ATTRIBUTE_NAME,ATTRIBUTE_TYPE, ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT from [##IMPORTTEMP'+@SESSION_ID+'] where Attribute_name not in (select Attribute_name from tb_attribute) and Attribute_id is null and CHARINDEX(''__'' ,Attribute_name ) > 0 
or CHARINDEX(''%'' ,Attribute_name ) > 0 or CHARINDEX(''*'' ,Attribute_name ) > 0
or CHARINDEX(''-'' ,Attribute_name ) > 0 or CHARINDEX(''+'' ,Attribute_name ) > 0 or CHARINDEX(''&'' ,Attribute_name ) > 0

')






end




if(@OPTION='VALIDATION DUP_ATTR_NAME')
begin
print 0
-- EXEC('
--;WITH VALCTE
--AS (
--  SELECT ATTRIBUTE_NAME,CAPTION FROM TB_ATTRIBUTE TA 
--  JOIN  CUSTOMERATTRIBUTE CA ON TA.ATTRIBUTE_ID = CA.ATTRIBUTE_ID WHERE CA.CUSTOMER_ID = '''+@CUSTOMER_ID+'''
   
--   EXCEPT

--    SELECT ATTRIBUTE_NAME,CAPTION FROM [##IMPORTTEMP'+@SESSION_ID+']
--	)

--   select * from VALCTE WHERE ATTRIBUTE_NAME IN (SELECT CAPTION FROM [##IMPORTTEMP'+@SESSION_ID+']) OR ATTRIBUTE_NAME IN (SELECT ATTRIBUTE_NAME FROM [##IMPORTTEMP'+@SESSION_ID+']) OR CAPTION IN (SELECT ATTRIBUTE_NAME FROM [##IMPORTTEMP'+@SESSION_ID+'])
--   OR CAPTION IN (SELECT CAPTION FROM [##IMPORTTEMP'+@SESSION_ID+'])
--   ')
   
   
exec('
;WITH VALCTE
AS (

SELECT Attribute_id,ATTRIBUTE_NAME,CAPTION FROM [##IMPORTTEMP'+@SESSION_ID+'] where Attribute_id is not null
  
   
   EXCEPT

   SELECT TA.Attribute_id,ATTRIBUTE_NAME,CAPTION FROM TB_ATTRIBUTE TA 
  JOIN  CUSTOMERATTRIBUTE CA ON TA.ATTRIBUTE_ID = CA.ATTRIBUTE_ID WHERE CA.CUSTOMER_ID = '''+@CUSTOMER_ID+''' 
	)

   select VALCTE.* from VALCTE join TB_ATTRIBUTE ta on VALCTE.ATTRIBUTE_ID=ta.ATTRIBUTE_ID where (VALCTE.ATTRIBUTE_NAME<>ta.ATTRIBUTE_NAME or VALCTE.Caption<>ta.Caption) 
   and (VALCTE.ATTRIBUTE_NAME IN (SELECT CAPTION FROM TB_ATTRIBUTE) 
   OR VALCTE.ATTRIBUTE_NAME IN (SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE)) 
   AND (VALCTE.CAPTION IN (SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE)
  OR VALCTE.CAPTION IN (SELECT CAPTION FROM TB_ATTRIBUTE))
  ')
end




                                    /*----------Delete------------*/

 if(@OPTION = 'DELETE')
begin

--delete from CUSTOMERATTRIBUTE CA join tb_attribute ta on CA.Attribute_id=ta.Attribute_id where ta.attribute_name=@ATTRNAME
delete from TB_ATTRIBUTE where ATTRIBUTE_NAME = @ATTRNAME
END


                              /*--------------------------IMPORT----------------------------*/

IF(@OPTION = 'IMPORT')
BEGIN

IF OBJECT_ID('tempdb..##attrwithid') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##attrwithid')
	END

IF OBJECT_ID('tempdb..##attrwithoutid') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##attrwithoutid')
	END

IF OBJECT_ID('tempdb..##attrwithoutids') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##attrwithoutids')
	END
exec('alter table [##IMPORTTEMP'+@SESSION_ID+'] drop column Action')
exec('alter table [##IMPORTTEMP'+@SESSION_ID+'] add FLAG1 varchar(20)')
	print 1;
	--select * from [##importtemp93bd09a5-3a39-47e7-8320-71ddb07930b6]

--CREATE TABLE ##attrwithid(ATTRIBUTE_NAME nvarchar(50),ATTRIBUTE_TYPE int,ATTRIBUTE_ID int,CREATE_BY_DEFAULT bit,VALUE_REQUIRED bit,
--STYLE_NAME decimal(5,2),STYLE_FORMAT nvarchar(max),DEFAULT_VALUE INT,PUBLISH2PRINT BIT,PUBLISH2WEB BIT,USE_PICKLIST BIT,ATTRIBUTE_DATATYPE NVARCHAR(50),
--ATTRIBUTE_DATAFORMAT NVARCHAR(MAX),IS_CALCULATED BIT,ATTRIBUTE_CALC_FORMULA NVARCHAR(MAX),PICKLIST_NAME NVARCHAR(50),ATTRIBUTE_DATARULE NVARCHAR(MAX),
--PUBLISH2PDF BIT,PUBLISH2EXPORT BIT,PUBLISH2PORTAL BIT
--);

--print 2
--CREATE TABLE ##attrwithoutid(ATTRIBUTE_NAME nvarchar(50),ATTRIBUTE_TYPE int,ATTRIBUTE_ID int,CREATE_BY_DEFAULT bit,VALUE_REQUIRED bit,
--STYLE_NAME decimal(5,2),STYLE_FORMAT nvarchar(max),DEFAULT_VALUE INT,PUBLISH2PRINT BIT,PUBLISH2WEB BIT,USE_PICKLIST BIT,ATTRIBUTE_DATATYPE NVARCHAR(50),
--ATTRIBUTE_DATAFORMAT NVARCHAR(MAX),IS_CALCULATED BIT,ATTRIBUTE_CALC_FORMULA NVARCHAR(MAX),PICKLIST_NAME NVARCHAR(50),ATTRIBUTE_DATARULE NVARCHAR(MAX),
--PUBLISH2PDF BIT,PUBLISH2EXPORT BIT,PUBLISH2PORTAL BIT
--);

print 3

exec('select * into ##attrwithid from [##IMPORTTEMP'+@SESSION_ID+'] where ATTRIBUTE_ID IS NOT NULL')
exec('select * into ##attrwithoutid from [##IMPORTTEMP'+@SESSION_ID+'] where ATTRIBUTE_ID IS NULL')
--EXEC('insert into ##attrwithid select * from [##IMPORTTEMP'+@SESSION_ID+'] where ATTRIBUTE_ID IS NOT NULL')
--EXEC('insert into ##attrwithoutid select * from [##IMPORTTEMP'+@SESSION_ID+'] where ATTRIBUTE_ID IS NULL')
 --select *,dense_RANK() OVER (ORDER BY ATTRIBUTE_NAME)ATTRIBUTE_NAME_dense_rank INTO ##attrwithoutids FROM ##attrwithoutid;

 print 4

             /*----------------------ATTR WITH_ID----------------*/


if (SELECT COUNT(*) FROM ##attrwithid)>0 
BEGIN


EXEC('UPDATE [##IMPORTTEMP'+@SESSION_ID+']  SET FLAG1=''update'' FROM [##IMPORTTEMP'+@SESSION_ID+']  TEMP JOIN TB_ATTRIBUTE TA ON TEMP.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME')

EXEC('SELECT COUNT(*) as updated FROM  [##IMPORTTEMP'+@SESSION_ID+'] WHERE FLAG1=''update''')



EXEC('UPDATE TA SET TA.ATTRIBUTE_NAME=TEMP.ATTRIBUTE_NAME,TA.STYLE_NAME=TEMP.STYLE_NAME,TA.PUBLISH2PRINT=TEMP.PUBLISH2PRINT,TA.PUBLISH2WEB=TEMP.PUBLISH2WEB,TA.PUBLISH2PDF=TEMP.PUBLISH2PDF,
TA.PUBLISH2EXPORT=1,TA.PUBLISH2PORTAL=1,TA.CREATE_BY_DEFAULT=TEMP.CREATE_BY_DEFAULT,TA.VALUE_REQUIRED=TEMP.VALUE_REQUIRED,TA.USE_PICKLIST=TEMP.USE_PICKLIST,
TA.CAPTION=TEMP.CAPTION
FROM TB_ATTRIBUTE TA JOIN [##IMPORTTEMP'+@SESSION_ID+'] TEMP ON TA.ATTRIBUTE_ID=TEMP.ATTRIBUTE_ID WHERE TA.FLAG_RECYCLE=''A''')



END

print 5

if (SELECT COUNT(*) FROM ##attrwithoutid)>0
BEGIN


EXEC('UPDATE [##IMPORTTEMP'+@SESSION_ID+']  SET [##IMPORTTEMP'+@SESSION_ID+'].ATTRIBUTE_ID = TA.ATTRIBUTE_ID FROM [##IMPORTTEMP'+@SESSION_ID+']  TEMP JOIN TB_ATTRIBUTE TA ON TEMP.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME Where TA.FLAG_RECYCLE = ''A''')


EXEC('UPDATE [##IMPORTTEMP'+@SESSION_ID+']  SET FLAG1=''update'' FROM [##IMPORTTEMP'+@SESSION_ID+']  TEMP JOIN TB_ATTRIBUTE TA ON TEMP.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME')

EXEC('SELECT COUNT(*) as updated FROM  [##IMPORTTEMP'+@SESSION_ID+'] WHERE ATTRIBUTE_ID IS NOT NULL')
EXEC('SELECT COUNT(*) as inserted FROM  [##IMPORTTEMP'+@SESSION_ID+'] WHERE ATTRIBUTE_ID IS NULL')

--update temp set temp.ATTRIBUTE_ID = TA.ATTRIBUTE_ID from ##attrwithoutids temp join TB_ATTRIBUTE TA on temp.ATTRIBUTE_NAME = TA.ATTRIBUTE_NAME where temp.Flag ='update'

--if (select COUNT(*) from ##attrwithoutids where Flag is null)>0
-- begin

--   set @idcount = (select MAX(ATTRIBUTE_ID) from TB_ATTRIBUTE)
--   print @idcount

--   update ##attrwithoutids set ATTRIBUTE_ID=(@idcount+ATTRIBUTE_NAME_dense_rank)
-- end

EXEC('UPDATE TB_ATTRIBUTE SET TB_ATTRIBUTE.STYLE_NAME=TEMP.STYLE_NAME,TB_ATTRIBUTE.PUBLISH2PRINT=TEMP.PUBLISH2PRINT,TB_ATTRIBUTE.PUBLISH2WEB=TEMP.PUBLISH2WEB,TB_ATTRIBUTE.PUBLISH2PDF=TEMP.PUBLISH2PDF,
TB_ATTRIBUTE.PUBLISH2EXPORT=1,TB_ATTRIBUTE.PUBLISH2PORTAL=1,TB_ATTRIBUTE.CREATE_BY_DEFAULT=TEMP.CREATE_BY_DEFAULT,TB_ATTRIBUTE.VALUE_REQUIRED=TEMP.VALUE_REQUIRED,TB_ATTRIBUTE.USE_PICKLIST=TEMP.USE_PICKLIST,
TB_ATTRIBUTE.CAPTION=TEMP.CAPTION
FROM TB_ATTRIBUTE TA JOIN [##IMPORTTEMP'+@SESSION_ID+'] TEMP ON TA.ATTRIBUTE_ID=TEMP.ATTRIBUTE_ID WHERE temp.FLAG1 =''update'' and TA.FLAG_RECYCLE=''A''')

print 555



ALTER TABLE ##attrwithoutid ADD FLAG VARCHAR(20);

ALTER TABLE ##attrwithid ADD FLAG VARCHAR(20);

UPDATE ##attrwithoutid SET FLAG='update' FROM ##attrwithoutid TEMP JOIN TB_ATTRIBUTE TA ON TEMP.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME

update ##attrwithid set FLAG = 'update' from ##attrwithid temp join TB_ATTRIBUTE TA on temp.ATTRIBUTE_ID = TA.ATTRIBUTE_ID


if(SELECT COUNT(CAPTION) FROM ##attrwithoutid WHERE FLAG IS NULL)>0
BEGIN
print'a'
Exec('insert into TB_ATTRIBUTE (ATTRIBUTE_NAME,ATTRIBUTE_TYPE,CREATE_BY_DEFAULT,VALUE_REQUIRED,STYLE_NAME,STYLE_FORMAT,DEFAULT_VALUE,PUBLISH2PRINT,PUBLISH2WEB,PUBLISH2CDROM,PUBLISH2ODP,USE_PICKLIST,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT,ATTRIBUTE_DATARULE,IS_CALCULATED,ATTRIBUTE_CALC_FORMULA,PICKLIST_NAME,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE,PUBLISH2PDF,PUBLISH2EXPORT,PUBLISH2PORTAL,CAPTION)
 select ATTRIBUTE_NAME,ATTRIBUTE_TYPE,CREATE_BY_DEFAULT,VALUE_REQUIRED,STYLE_NAME,STYLE_FORMAT,DEFAULT_VALUE,PUBLISH2PRINT,PUBLISH2WEB,1,1,USE_PICKLIST,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT,ATTRIBUTE_DATARULE,IS_CALCULATED,ATTRIBUTE_CALC_FORMULA,PICKLIST_NAME,'''+ @CREATED_USER +''',GETDATE(),'''+ @MODIFIED_USER +''',GETDATE(),PUBLISH2PDF,1,1,CAPTION from ##attrwithoutid where ATTRIBUTE_ID is null')
 END
ELSE
BEGIN
print'b'
EXEC('insert into TB_ATTRIBUTE (ATTRIBUTE_NAME,ATTRIBUTE_TYPE,CREATE_BY_DEFAULT,VALUE_REQUIRED,STYLE_NAME,STYLE_FORMAT,DEFAULT_VALUE,PUBLISH2PRINT,PUBLISH2WEB,PUBLISH2CDROM,PUBLISH2ODP,USE_PICKLIST,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT,ATTRIBUTE_DATARULE,IS_CALCULATED,ATTRIBUTE_CALC_FORMULA,PICKLIST_NAME,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE,PUBLISH2PDF,PUBLISH2EXPORT,PUBLISH2PORTAL,CAPTION)
select ATTRIBUTE_NAME,ATTRIBUTE_TYPE,CREATE_BY_DEFAULT,VALUE_REQUIRED,STYLE_NAME,STYLE_FORMAT,DEFAULT_VALUE,PUBLISH2PRINT,PUBLISH2WEB,1,1,USE_PICKLIST,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT,ATTRIBUTE_DATARULE,IS_CALCULATED,ATTRIBUTE_CALC_FORMULA,PICKLIST_NAME,'''+ @CREATED_USER +''',GETDATE(),'''+ @MODIFIED_USER +''',GETDATE(),PUBLISH2PDF,1,1,ATTRIBUTE_NAME from ##attrwithoutid where ATTRIBUTE_ID is null')
END



update temp set temp.attribute_id= ta.attribute_id from ##attrwithoutid temp join tb_attribute ta on temp.attribute_name = ta.attribute_name where ta.flag_recycle='A' 

INSERT INTO TB_CATALOG_ATTRIBUTES (CATALOG_ID,ATTRIBUTE_ID,CREATED_USER,MODIFIED_USER)
SELECT @CATALOG_ID,ATTRIBUTE_ID,@CREATED_USER,@MODIFIED_USER from ##attrwithoutid where FLAG is null AND @CATALOG_ID <>1


insert into  CUSTOMERATTRIBUTE (CUSTOMER_ID,MODIFIED_USER,CREATED_USER,ATTRIBUTE_ID)
select @CUSTOMER_ID,@MODIFIED_USER, @CREATED_USER,ATTRIBUTE_ID from ##attrwithoutid where FLAG is null


Set IDENTITY_INSERT TB_Attribute ON





END	


		  

END


END


--select * from TB_ATTRIBUTE




GO



ALTER PROCEDURE [dbo].[STP_LS_ProductPivotTable_For_Publish]
@FAMILY_ID VARCHAR(MAX)='24739',  
@CATALOG_ID INT=94,  
@USER_ID VARCHAR(50)  ='demo@questudio.com',  
@CATEGORY_ID nvarchar(100)='CAT12377',  
@PAGENO INT =1,    
@PERPAGECOUNT INT =10,  
@CATALOG_ITEM_ID int =0  ,
@GROUP_ID INT = 0  ,
@type nvarchar(50) = '',
@AttributeSeach nvarchar(max) = '',
@SearchType nvarchar(20) = '',
@data nvarchar(max) = '',
@ProductSpec bit = 1,
@ProductImg  bit =  1,
@ProductPrice bit = 1,
@ProductKey  bit = 1

as


 EXEC('DROP TABLE PRODUCT_ID')  

IF OBJECT_ID('tempdb..##TEMP') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##TEMP')  
end

declare @KeyValues nvarchar(20) = 0

if(@ProductSpec <> 1)
begin
set @KeyValues =  cast(@KeyValues as varchar(50)) +''+1
end

if(@ProductImg <> 1)
begin
print 'img'
set @KeyValues = cast(@KeyValues as varchar(50)) + ',' +cast(3 as varchar(50))
end



if(@ProductKey <> 1)
begin
set @KeyValues = cast(@KeyValues as varchar(50)) + ',' +cast(6 as varchar(50))
end

if(@ProductPrice <> 1)
begin
set @KeyValues = cast(@KeyValues as varchar(50)) + ',' +cast(4 as varchar(50))
end


print @KeyValues

--execute ('select ATTRIBUTE_ID from tb_attribute where attribute_Type not  in ('+ @KeyValues+ ')')

--------------------------------------------------------------------------
DECLARE @ATTRIBUTE_NAMES VARCHAR(MAX);  
DECLARE @ROLE_ID INT =0  

DECLARE @PRODUCT_ID INT =0  
DECLARE @FROMPAGE INT =0  
DECLARE @TOPAGE INT =0  
DECLARE @MAINPROD int =0  
 select  Top 1  @MAINPROD =PRODUCT_ID from TB_SUBPRODUCT where SUBPRODUCT_ID=@CATALOG_ITEM_ID and CATALOG_ID=@CATALOG_ID  
   
   
SET @ATTRIBUTE_NAMES = ''   
Select @ROLE_ID = Role_id from aspnet_Users  au join aspnet_UsersInRoles aur on au.UserId = aur.UserId JOIN aspnet_Roles ar on  ar.RoleId = aur.RoleId where UserName =@USER_ID  


  
  
--Select TOP 1 @PRODUCT_ID = PRODUCT_ID FROM TB_PROD_FAMILY tpf where FAMILY_ID = @FAMILY_ID  
  
print @ROLE_ID  
IF OBJECT_ID('tempdb..#t') IS NOT NULL  
Begin  
 EXEC('DROP TABLE #t')  
end  
IF OBJECT_ID('tempdb..##TEMPSEARCHDATA') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##TEMPSEARCHDATA')  
end  

IF OBJECT_ID('tempdb..##finaldata') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##finaldata')  
end  

IF OBJECT_ID('tempdb..##FilterValu') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterValu')  
end  


IF OBJECT_ID('tempdb..##FilterResult') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterResult')  
end  


IF OBJECT_ID('tempdb..##FilterValu1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterValu1')  
end  


IF OBJECT_ID('tempdb..##FilterResult1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterResult1')  
end  

Declare @FAMILY_ID1 Varchar(max);
SET @FAMILY_ID1 = REPLACE(@FAMILY_ID, '''', '')

  
EXEC('select * into #t from (  
select (ROW_NUMBER() over (partition by family_id,product_id order by family_id))as Row,* from TB_PROD_FAMILY_ATTR_LIST where FAMILY_ID IN ('+@FAMILY_ID+') ---and PRODUCT_ID=36  
) as temp ')  
  
  
--Select TOP 1 @PRODUCT_ID = TPF.PRODUCT_ID FROM TB_PROD_FAMILY tpf   
--join TB_PROD_FAMILY_ATTR_LIST TPFL on TPFL.FAMILY_ID = tpf.FAMILY_ID       
--where TPF.FAMILY_ID = @FAMILY_ID AND TPFL.FAMILY_ID = @FAMILY_ID group by tpf.PRODUCT_ID,tpf.FAMILY_ID   
--order by count(ATTRIBUTE_ID)   
  


EXEC('Select TOP 1  PRODUCT_ID INTO PRODUCT_ID FROM TB_PROD_FAMILY where FAMILY_ID IN ('+@FAMILY_ID+')  and FLAG_RECYCLE=''A''')
  
SET @PRODUCT_ID = (Select PRODUCT_ID FROM PRODUCT_ID)
PRINT @PRODUCT_ID

SET @FROMPAGE =  ((@PAGENO-1) * @PERPAGECOUNT) + 1   
SET @TOPAGE =  ((@PAGENO-1) * @PERPAGECOUNT) + @PERPAGECOUNT   
  



  
 if(@GROUP_ID=0)
 begin 

 print 'king1123'

EXEC(';WITH TTT  
AS  
(  
SELECT DISTINCT * FROM (  
SELECT DISTINCT TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME=''Supplier'' THEN ATTRIBUTE_NAME +''__OBJ__0'' +''__''+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) END +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END AS ATTRIBUTE_NAME ,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as SORT_ORDER FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE = ''A'' 
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE =''A'' 
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE =''A'' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  AND TA.CREATE_BY_DEFAULT = 1  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+@CATALOG_ID+' 
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID ='+@ROLE_ID+'  
WHERE TF.FAMILY_ID IN ('+@FAMILY_ID1+') AND TCF.CATALOG_ID= '+@CATALOG_ID+' AND TA.ATTRIBUTE_TYPE NOT IN (9,7,11,12,13) 
UNION ALL  
SELECT DISTINCT TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME+''__'' + ''OBJ''+''__''+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END +''__''+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME ,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as SORT_ORDER
FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID  
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE =''A'' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  AND TA.CREATE_BY_DEFAULT = 1   
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+@CATALOG_ID+'   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID ='+@ROLE_ID+'  
WHERE TF.FAMILY_ID IN ('+@FAMILY_ID1+') AND TCF.CATALOG_ID='+@CATALOG_ID+' AND TA.ATTRIBUTE_TYPE NOT IN (9,7,11,12,13)
UNION ALL  
SELECT DISTINCT TPS.ATTRIBUTE_ID,ATTRIBUTE_NAME+ ''__1__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END   AS ATTRIBUTE_NAME  ,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as SORT_ORDER
 FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  AND (TA.FLAG_RECYCLE =''A'' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)    AND TA.CREATE_BY_DEFAULT = 1  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+@CATALOG_ID+'   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID ='+@ROLE_ID+'  
WHERE TF.FAMILY_ID IN ('+@FAMILY_ID1+') AND TCF.CATALOG_ID='+@CATALOG_ID+'  AND TA.ATTRIBUTE_TYPE = 3 AND TA.ATTRIBUTE_TYPE NOT IN (9,7,11,12,13)
  
 ) DD    
)   
select * into ##FilterValu from TTT



')
if(@KeyValues = 0)
BEGIN
print'a1'
exec('select * into ##FilterResult from ##FilterValu  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE not in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID) ') 
END
ELSE
BEGIN
print'b'
exec('select * into ##FilterResult from ##FilterValu  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID) ') 
END
SELECT  @ATTRIBUTE_NAMES = @ATTRIBUTE_NAMES +'['+ ATTRIBUTE_NAME + '],' FROM ##FilterResult ORDER BY SORT_ORDER,ATTRIBUTE_ID  

Print @ATTRIBUTE_NAMES
END

else 

Begin 

print 'king'

;WITH TTT  
AS  
(  
SELECT DISTINCT * FROM (  
SELECT DISTINCT TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME='Supplier' THEN ATTRIBUTE_NAME +'__OBJ__0' +'__'+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +'__OBJ__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) END +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END AS ATTRIBUTE_NAME,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
--JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TPFAL.PRODUCT_ID = TPF.PRODUCT_ID AND TF.FAMILY_ID = TPFAL.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID  
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID  --AND TP.PRODUCT_ID = @PRODUCT_ID  
UNION ALL  
SELECT DISTINCT TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME+'__' + 'OBJ'+'__'+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END +'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END  AS ATTRIBUTE_NAME,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   
FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID  
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID --AND TP.PRODUCT_ID = @PRODUCT_ID  
UNION ALL  
SELECT DISTINCT TPS.ATTRIBUTE_ID,ATTRIBUTE_NAME+ '__1__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END   AS ATTRIBUTE_NAME  
,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   
  FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID  AND TA.ATTRIBUTE_TYPE = 3   --AND TP.PRODUCT_ID = @PRODUCT_ID  
  
 ) DD    
)   
    select * into ##FilterValu1 from TTT

	 exec('select * into ##FilterResult1 from ##FilterValu1  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE not in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID)') 

SELECT @ATTRIBUTE_NAMES = @ATTRIBUTE_NAMES +'['+ ATTRIBUTE_NAME + '],' FROM ##FilterResult1 where ATTRIBUTE_ID in 
(Select ATTRIBUTE_ID from TB_PACKAGE_DETAILS where GROUP_ID=@GROUP_ID) or ATTRIBUTE_ID=1 ORDER BY SORT_ORDER,ATTRIBUTE_ID

End  

  
IF(@ATTRIBUTE_NAMES!=NULL OR @ATTRIBUTE_NAMES !='')  
BEGIN  
SET @ATTRIBUTE_NAMES = LEFT(@ATTRIBUTE_NAMES, LEN(@ATTRIBUTE_NAMES) - 1)  
print @ATTRIBUTE_NAMES  
END  
  print '1'
print @ATTRIBUTE_NAMES  
  Declare @sortOrderPage int 
  set @sortOrderPage=@FROMPAGE-1
IF(@ATTRIBUTE_NAMES!=NULL OR @ATTRIBUTE_NAMES !='')  
 BEGIN  
     
	 print 'oops'
   
  EXEC('  
  IF (SELECT object_id(''TempDB..#TEMP_ITEMS'')) IS NOT NULL  
BEGIN  
    DROP TABLE #TEMP_ITEMS  
END  
  
 DECLARE @PCNT INT=0  
 DECLARE @TEMPROW INT=0  
 DECLARE @TMPFRM INT=0  
 DEClare @TMPTO INT=0  
     
  SELECT ROW_NUMBER() over (Partition by ID1 ORDER BY SORT) AS ID, * INTO #TEMPPRODSPECS FROM (  
  SELECT ROW_NUMBER() over (Partition by TPF.PRODUCT_ID ORDER BY TPF.PRODUCT_ID) AS ID1,TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END   
  as PUBLISH2PRINT,  
  TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,TPS.STRING_VALUE STRING_VALUE,  ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+ CAST(ATTRIBUTE_TYPE as nvarchar)   
   +''__true__true''     AS ATTRIBUTE_NAMETEMP,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL
    FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
   --JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
   JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +'''
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS  
   JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +'  AND TA.ATTRIBUTE_ID =1  
    ) as AA where id1=''1'' 
   

   
 IF( (LEN(ISNULL('+@MAINPROD +', '''')) <> 0 ) AND ('+@MAINPROD +'<>0) )  
 BEGIN  
   
   
 SELECT @TEMPROW= ID FROM #TEMPPRODSPECS WHERE PRODUCT_ID='+@MAINPROD+'  
    
SET @PCNT  =(((@TEMPROW)/'+@PERPAGECOUNT+')* '+@PERPAGECOUNT+')  
   
   
  SET @TMPFRM =   @PCNT + 1  
  
  set @TMPTO=@PCNT +'+@PERPAGECOUNT+'  

      
    
  END  
 ELSE  
 BEGIN  
   
 SET @TMPFRM = '+ @FROMPAGE +'  
 SET @TMPTO = '+ @TOPAGE +'  
 END  
    
 
 SELECT * INTO #TEMP_ITEMS FROM #TEMPPRODSPECS 
    
    
  SELECT  * into #TempData FROM   
  (  
     
  SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%TEXT%'' THEN TPS.STRING_VALUE   
  WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%NUM%'' THEN --CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE)   
  REPLACE(STR(ROUND(CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE),cast(substring(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''),  
  CHARINDEX('','',REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))+1, len(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))) as int)),25,cast(substring(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''),  
  CHARINDEX('','',REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))+1, len(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))) as int)),'' '','''')   
  WHEN  TA.ATTRIBUTE_DATATYPE LIKE ''%HYPER%''  THEN TPS.STRING_VALUE  
  ELSE TPS.STRING_VALUE  
  END   
  STRING_VALUE,CASE WHEN ATTRIBUTE_NAME=''Supplier'' THEN ATTRIBUTE_NAME +''__OBJ__0''+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  ELSE ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  END +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END   AS ATTRIBUTE_NAME,
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL     
    FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
   --JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
   JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +'''
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS  
   LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON  TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   JOIN #TEMP_ITEMS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
   WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +' AND ATTRIBUTE_TYPE <> 6  
  UNION ALL  
   SELECT DISTINCT TPS.CATALOG_ID,TPS.FAMILY_ID,TPS.PRODUCT_ID,TPF.SORT_ORDER SORT,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT, TWS.STATUS_NAME as [WORKFLOW STATUS] ,  
   ATTRIBUTE_VALUE,  
   ATTRIBUTE_NAME + ''__OBJ''+''__''+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
   +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL
   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
   JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID and TPS.CATEGORY_ID = '''+@CATEGORY_ID+'''  
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +''' 
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS   
   LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   JOIN #TEMP_ITEMS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
   WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +'    
  UNION ALL  
  SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,ISNULL(TPS.OBJECT_NAME , '''')  AS STRING_VALUE  
  ,ATTRIBUTE_NAME + ''__1__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE   
  WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME
  ,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL  
   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
  JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
  JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
  JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
  JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
  JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'    
  join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +''' 
  JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS   
  LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
  LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON  TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
  LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
  JOIN #TEMP_ITEMS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
  WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +'  AND TA.ATTRIBUTE_TYPE = 3 AND ATTRIBUTE_TYPE <> 6    
  )AS A   
  PIVOT  
  (  
   MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('+ @ATTRIBUTE_NAMES +')  
  )AS P  
   
  update #TempData set SORT=tt1.SORT_ORDER from #TempData join (select *,ROW_NUMBER() over(order by SORT,PRODUCT_ID)+'+@sortOrderPage+' SORT_ORDER from #TempData) as tt1 on #TempData.PRODUCT_ID=tt1.PRODUCT_ID

   select * into ##TEMPSEARCHDATA from #TempData order by SORT 

  select  * into ##finaldata from #TempData order by SORT    

  '  

   )  
   --------------------To implement the search functionality  - Start



 IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE #Temp1')  
end  


if(@type = 'product')

begin
 
 print 'kids'

 Select column_id as Row_Number,name into #Temp1 From  Tempdb.Sys.Columns Where Object_ID = Object_ID('tempdb..##TEMPSEARCHDATA')
  
   declare @a int = 1;
   declare @name nvarchar(max);
   declare @sysAttribute nvarchar(max) ;
   declare @coloum nvarchar(max);
   declare @ColumName nvarchar(max)


   while ((select count(*) + 1 from  #Temp1)  > @a)
   begin
        set @name = (select name from  #Temp1 where Row_Number =  @a)
		if CHARINDEX('__OBJ',@name) > 0
		  begin
			if(@AttributeSeach =  (select SUBSTRING(@name,1,(SELECT CHARINDEX ('__OBJ',@name) -1))))
			begin
			set @sysAttribute = @name
			print @sysAttribute
			end
		end
		   set @a = @a + 1
   end

   ----------------------------------------------------------------------------------------------------------------------------
set @coloum = @sysAttribute
set @ColumName = '##TEMPSEARCHDATA.[' +@coloum +']'
print 'ooooops1'
print @ColumName
print 'ooooops1'
print @data


if(@ColumName is not null)
begin
print 'IAM not Null'
if(@SearchType = 'Contains')
begin
print 'Contains'
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   like   ''%'+@data+'%''')
end
else if(@SearchType = 'Exact')
begin
print 'EXACT'
print @ColumName
print @data
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   =   '''+@data+'''   ')
end
else if(@SearchType = 'Starts With')
begin
print 'Starts With'
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   like   '''+@data+'%''')
end
else if(@SearchType = 'Ends With')
begin
print 'Ends With'
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   like   ''%'+@data+'''')
end

end
else
begin
print 'IAm a Null'
--select * from ##finaldata

--delete from ##finaldata
select * from ##finaldata
exec('select Count(*) from ##TEMPSEARCHDATA') 
end





End
else
begin
 print '2'  
select * from ##finaldata order by SORT 
exec('select Count(*) from ##TEMPSEARCHDATA') 
 print '22'  
end



 -----------------To implement the search functionality  - End
 
 
 END  
   
ELSE  
 BEGIN  

 	 print 'oops1'

  EXEC('  
  SELECT ROW_NUMBER() over (ORDER BY PRODUCT_ID) AS ID, * FROM   
  (  
  SELECT distinct TCF.CATALOG_ID,TF.CATEGORY_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,TPF.PUBLISH2PRINT ,TPF.PUBLISH2WEB ,TPF.PUBLISH2PORTAL,TPF.PUBLISH2EXPORT,TPF.PUBLISH2PDF
  FROM TB_FAMILY TF   
  JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID AND TF.FAMILY_ID in ('+ @FAMILY_ID +') AND TCF.CATALOG_ID = '+ @CATALOG_ID +'  
  JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
  JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
  JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID  
  JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE =''A''  
  JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID
    
  )AS A    
  --SELECT * FROM #TEMPPRODSPECS WHERE ID BETWEEN  '+ @FROMPAGE +' AND '+ @TOPAGE +'  
   ')  
 END  
/****** Object:  StoredProcedure [dbo].[STP_LS_ProductPivotTable_For_PublishWithoutAttributes]    Script Date: 03/02/2017 17:27:31 ******/  
SET ANSI_NULLS ON



GO




Create PROCEDURE [dbo].[STP_LS_ProductPivotTable_For_PublishEdit]
@FAMILY_ID VARCHAR(MAX)='22525,22526,22527,22528,22529,22530,22531,22532,22533,22534,22535',  
@CATALOG_ID INT=29,  
@USER_ID VARCHAR(50)  ='demo@questudio.com',  
@CATEGORY_ID nvarchar(100)='CAT11628',  
@PAGENO INT =1,    
@PERPAGECOUNT INT =10,  
@CATALOG_ITEM_ID int =0,
@GROUP_ID INT = 0  ,
@type nvarchar(50) = '',
@AttributeSeach nvarchar(max) = '',
@SearchType nvarchar(20) = '',
@data nvarchar(max) = '',
@ProductSpec bit = 1,
@ProductImg  bit =  1,
@ProductPrice bit = 1,
@ProductKey  bit = 1

as


 EXEC('DROP TABLE PRODUCT_ID')  

IF OBJECT_ID('tempdb..##TEMP') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##TEMP')  
end

declare @KeyValues nvarchar(20) = 0

if(@ProductSpec <> 1)
begin
set @KeyValues =  cast(@KeyValues as varchar(50)) +''+1
end

if(@ProductImg <> 1)
begin
print 'img'
set @KeyValues = cast(@KeyValues as varchar(50)) + ',' +cast(3 as varchar(50))
end



if(@ProductKey <> 1)
begin
set @KeyValues = cast(@KeyValues as varchar(50)) + ',' +cast(6 as varchar(50))
end

if(@ProductPrice <> 1)
begin
set @KeyValues = cast(@KeyValues as varchar(50)) + ',' +cast(4 as varchar(50))
end


print @KeyValues

--execute ('select ATTRIBUTE_ID from tb_attribute where attribute_Type not  in ('+ @KeyValues+ ')')

--------------------------------------------------------------------------
DECLARE @ATTRIBUTE_NAMES VARCHAR(MAX);  
DECLARE @ROLE_ID INT =0  

DECLARE @PRODUCT_ID INT =0  
DECLARE @FROMPAGE INT =0  
DECLARE @TOPAGE INT =0  
DECLARE @MAINPROD int =0  
 select  Top 1  @MAINPROD =PRODUCT_ID from TB_SUBPRODUCT where SUBPRODUCT_ID=@CATALOG_ITEM_ID and CATALOG_ID=@CATALOG_ID  
   
   
SET @ATTRIBUTE_NAMES = ''   
Select @ROLE_ID = Role_id from aspnet_Users  au join aspnet_UsersInRoles aur on au.UserId = aur.UserId JOIN aspnet_Roles ar on  ar.RoleId = aur.RoleId where UserName =@USER_ID  


  
  
--Select TOP 1 @PRODUCT_ID = PRODUCT_ID FROM TB_PROD_FAMILY tpf where FAMILY_ID = @FAMILY_ID  
  
print @ROLE_ID  
IF OBJECT_ID('tempdb..#t') IS NOT NULL  
Begin  
 EXEC('DROP TABLE #t')  
end  
IF OBJECT_ID('tempdb..##TEMPSEARCHDATA') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##TEMPSEARCHDATA')  
end  

IF OBJECT_ID('tempdb..##finaldata') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##finaldata')  
end  

IF OBJECT_ID('tempdb..##FilterValu') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterValu')  
end  


IF OBJECT_ID('tempdb..##FilterResult') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterResult')  
end  


IF OBJECT_ID('tempdb..##FilterValu1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterValu1')  
end  


IF OBJECT_ID('tempdb..##FilterResult1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterResult1')  
end  

Declare @FAMILY_ID1 Varchar(max);
SET @FAMILY_ID1 = REPLACE(@FAMILY_ID, '''', '')

  
EXEC('select * into #t from (  
select (ROW_NUMBER() over (partition by family_id,product_id order by family_id))as Row,* from TB_PROD_FAMILY_ATTR_LIST where FAMILY_ID IN ('+@FAMILY_ID+') ---and PRODUCT_ID=36  
) as temp ')  
  
  
--Select TOP 1 @PRODUCT_ID = TPF.PRODUCT_ID FROM TB_PROD_FAMILY tpf   
--join TB_PROD_FAMILY_ATTR_LIST TPFL on TPFL.FAMILY_ID = tpf.FAMILY_ID       
--where TPF.FAMILY_ID = @FAMILY_ID AND TPFL.FAMILY_ID = @FAMILY_ID group by tpf.PRODUCT_ID,tpf.FAMILY_ID   
--order by count(ATTRIBUTE_ID)   
  


EXEC('Select TOP 1  PRODUCT_ID INTO PRODUCT_ID FROM TB_PROD_FAMILY where FAMILY_ID IN ('+@FAMILY_ID+')  and FLAG_RECYCLE=''A''')
  
SET @PRODUCT_ID = (Select PRODUCT_ID FROM PRODUCT_ID)
PRINT @PRODUCT_ID

SET @FROMPAGE =  ((@PAGENO-1) * @PERPAGECOUNT) + 1   
SET @TOPAGE =  ((@PAGENO-1) * @PERPAGECOUNT) + @PERPAGECOUNT   
  



  
 if(@GROUP_ID=0)
 begin 

 print 'king1123'

EXEC(';WITH TTT  
AS  
(  
SELECT DISTINCT * FROM (  
SELECT DISTINCT TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME=''Supplier'' THEN ATTRIBUTE_NAME +''__OBJ__0'' +''__''+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) END +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END AS ATTRIBUTE_NAME FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID    
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE =''A'' 
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE =''A'' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+@CATALOG_ID+' 
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID ='+@ROLE_ID+'  
WHERE TF.FAMILY_ID IN ('+@FAMILY_ID1+') AND TCF.CATALOG_ID= '+@CATALOG_ID+'  AND TA.ATTRIBUTE_TYPE NOT IN (9,7,11,12,13)
UNION ALL  
SELECT DISTINCT TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME+''__'' + ''OBJ''+''__''+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END +''__''+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME
FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID  
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE =''A'' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3) 
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+@CATALOG_ID+'   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID ='+@ROLE_ID+'  AND TA.ATTRIBUTE_TYPE NOT IN (9,7,11,12,13)
WHERE TF.FAMILY_ID IN ('+@FAMILY_ID1+') AND TCF.CATALOG_ID='+@CATALOG_ID+'
UNION ALL  
SELECT DISTINCT TPS.ATTRIBUTE_ID,ATTRIBUTE_NAME+ ''__1__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END   AS ATTRIBUTE_NAME  
 FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  AND (TA.FLAG_RECYCLE =''A'' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+@CATALOG_ID+'   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID ='+@ROLE_ID+'  
WHERE TF.FAMILY_ID IN ('+@FAMILY_ID1+') AND TCF.CATALOG_ID='+@CATALOG_ID+'  AND TA.ATTRIBUTE_TYPE = 3 AND TA.ATTRIBUTE_TYPE NOT IN (9,7,11,12,13)
  
 ) DD    
)   
select * into ##FilterValu from TTT



')
if(@KeyValues = 0)
BEGIN
print'a1'
exec('select * into ##FilterResult from ##FilterValu  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE not in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID) ') 
END
ELSE
BEGIN
print'b1'
exec('select * into ##FilterResult from ##FilterValu  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID) ') 
END
SELECT DISTINCT * INTO ##TEMP  FROM ##FilterResult ORDER BY ATTRIBUTE_ID
SELECT  @ATTRIBUTE_NAMES = @ATTRIBUTE_NAMES +'['+ ATTRIBUTE_NAME + '],' FROM ##TEMP ORDER BY ATTRIBUTE_ID  

Print @ATTRIBUTE_NAMES
END

else 

Begin 

print 'king'

;WITH TTT  
AS  
(  
SELECT DISTINCT * FROM (  
SELECT DISTINCT TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME='Supplier' THEN ATTRIBUTE_NAME +'__OBJ__0' +'__'+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +'__OBJ__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) END +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END AS ATTRIBUTE_NAME,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
--JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TPFAL.PRODUCT_ID = TPF.PRODUCT_ID AND TF.FAMILY_ID = TPFAL.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID  
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID  --AND TP.PRODUCT_ID = @PRODUCT_ID  
UNION ALL  
SELECT DISTINCT TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME+'__' + 'OBJ'+'__'+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END +'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END  AS ATTRIBUTE_NAME,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   
FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID  
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID --AND TP.PRODUCT_ID = @PRODUCT_ID  
UNION ALL  
SELECT DISTINCT TPS.ATTRIBUTE_ID,ATTRIBUTE_NAME+ '__1__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END   AS ATTRIBUTE_NAME  
,case when TPFAL.SORT_ORDER IS null then 1000 else TPFAL.SORT_ORDER end as sort_order   
  FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@USER_ID 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID  AND TA.ATTRIBUTE_TYPE = 3   --AND TP.PRODUCT_ID = @PRODUCT_ID  
  
 ) DD    
)   
    select * into ##FilterValu1 from TTT

	 exec('select * into ##FilterResult1 from ##FilterValu1  where ATTRIBUTE_ID  in (select ATTRIBUTE_ID from  TB_ATTRIBUTE where ATTRIBUTE_TYPE not in ('+ @KeyValues+ ')union select 1 as ATTRIBUTE_ID)') 

SELECT @ATTRIBUTE_NAMES = @ATTRIBUTE_NAMES +'['+ ATTRIBUTE_NAME + '],' FROM ##FilterResult1 where ATTRIBUTE_ID in 
(Select ATTRIBUTE_ID from TB_PACKAGE_DETAILS where GROUP_ID=@GROUP_ID) or ATTRIBUTE_ID=1 ORDER BY SORT_ORDER,ATTRIBUTE_ID

End  

  
IF(@ATTRIBUTE_NAMES!=NULL OR @ATTRIBUTE_NAMES !='')  
BEGIN  
SET @ATTRIBUTE_NAMES = LEFT(@ATTRIBUTE_NAMES, LEN(@ATTRIBUTE_NAMES) - 1)  
print @ATTRIBUTE_NAMES  
END  
  print '1'
print @ATTRIBUTE_NAMES  
  Declare @sortOrderPage int 
  set @sortOrderPage=@FROMPAGE-1
IF(@ATTRIBUTE_NAMES!=NULL OR @ATTRIBUTE_NAMES !='')  
 BEGIN  
     
	 print 'oops'
   
  EXEC('  
  IF (SELECT object_id(''TempDB..#TEMP_ITEMS'')) IS NOT NULL  
BEGIN  
    DROP TABLE #TEMP_ITEMS  
END  
  
 DECLARE @PCNT INT=0  
 DECLARE @TEMPROW INT=0  
 DECLARE @TMPFRM INT=0  
 DEClare @TMPTO INT=0  
     
  SELECT ROW_NUMBER() over (Partition by ID1 ORDER BY SORT) AS ID, * INTO #TEMPPRODSPECS FROM (  
  SELECT ROW_NUMBER() over (Partition by TPF.PRODUCT_ID ORDER BY TPF.PRODUCT_ID) AS ID1,TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END   
  as PUBLISH2PRINT,  
  TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,TPS.STRING_VALUE STRING_VALUE,  ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+ CAST(ATTRIBUTE_TYPE as nvarchar)   
   +''__true__true''     AS ATTRIBUTE_NAMETEMP,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL
    FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
   --JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
   JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +'''
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS  
   JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +'  AND TA.ATTRIBUTE_ID =1  
    ) as AA where id1=''1'' 
   

   
 IF( (LEN(ISNULL('+@MAINPROD +', '''')) <> 0 ) AND ('+@MAINPROD +'<>0) )  
 BEGIN  
   
   
 SELECT @TEMPROW= ID FROM #TEMPPRODSPECS WHERE PRODUCT_ID='+@MAINPROD+'  
    
SET @PCNT  =(((@TEMPROW)/'+@PERPAGECOUNT+')* '+@PERPAGECOUNT+')  
   
   
  SET @TMPFRM =   @PCNT + 1  
  
  set @TMPTO=@PCNT +'+@PERPAGECOUNT+'  

      
    
  END  
 ELSE  
 BEGIN  
   
 SET @TMPFRM = '+ @FROMPAGE +'  
 SET @TMPTO = '+ @TOPAGE +'  
 END  
    
 
 SELECT * INTO #TEMP_ITEMS FROM #TEMPPRODSPECS 
    
    
  SELECT  * into #TempData FROM   
  (  
     
  SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,  
  CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%TEXT%'' THEN TPS.STRING_VALUE   
  WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%NUM%'' THEN --CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE)   
  REPLACE(STR(ROUND(CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE),cast(substring(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''),  
  CHARINDEX('','',REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))+1, len(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))) as int)),25,cast(substring(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''),  
  CHARINDEX('','',REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))+1, len(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))) as int)),'' '','''')   
  WHEN  TA.ATTRIBUTE_DATATYPE LIKE ''%HYPER%''  THEN TPS.STRING_VALUE  
  ELSE TPS.STRING_VALUE  
  END   
  STRING_VALUE,CASE WHEN ATTRIBUTE_NAME=''Supplier'' THEN ATTRIBUTE_NAME +''__OBJ__0''+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  ELSE ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  END +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END   AS ATTRIBUTE_NAME,
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL     
    FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
   --JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
   JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +'''
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS  
   LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON  TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   JOIN #TEMP_ITEMS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
   WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +' AND ATTRIBUTE_TYPE <> 6  
  UNION ALL  
   SELECT DISTINCT TPS.CATALOG_ID,TPS.FAMILY_ID,TPS.PRODUCT_ID,TPF.SORT_ORDER SORT,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT, TWS.STATUS_NAME as [WORKFLOW STATUS] ,  
   ATTRIBUTE_VALUE,  
   ATTRIBUTE_NAME + ''__OBJ''+''__''+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
   +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL
   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
   JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
   JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID and TPS.CATEGORY_ID = '''+@CATEGORY_ID+'''  
   JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
   JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
   JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'   
   join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +''' 
   JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS   
   LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
   LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
   LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
   JOIN #TEMP_ITEMS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
   WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +'    
  UNION ALL  
  SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE  CASE WHEN TPF.PUBLISH2WEB=1 then ''True'' else ''False'' end END as PUBLISH2WEB,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0''   
  THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PRINT=1 then ''True'' else ''False'' end END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]  
  ,ISNULL(TPS.OBJECT_NAME , '''')  AS STRING_VALUE  
  ,ATTRIBUTE_NAME + ''__1__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar)   
  +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE   
  WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME
  ,     
    CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PDF=1 then ''True'' else ''False'' end END   
  as PUBLISH2PDF,  
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2EXPORT=1 then ''True'' else ''False'' end END   
  as PUBLISH2EXPORT,
   CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE CASE WHEN TPF.PUBLISH2PORTAL=1 then ''True'' else ''False'' end END   
  as PUBLISH2PORTAL  
   FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
  JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
  JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
  JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
  JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  
  JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'    
  join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name ='''+@USER_ID +''' 
  JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS   
  LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID   
  LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON  TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
  LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'  
  JOIN #TEMP_ITEMS TI ON TI.PRODUCT_ID = TPS.PRODUCT_ID  
  WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +'  AND TA.ATTRIBUTE_TYPE = 3 AND ATTRIBUTE_TYPE <> 6    
  )AS A   
  PIVOT  
  (  
   MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('+ @ATTRIBUTE_NAMES +')  
  )AS P  
   
  update #TempData set SORT=tt1.SORT_ORDER from #TempData join (select *,ROW_NUMBER() over(order by SORT,PRODUCT_ID)+'+@sortOrderPage+' SORT_ORDER from #TempData) as tt1 on #TempData.PRODUCT_ID=tt1.PRODUCT_ID

   select * into ##TEMPSEARCHDATA from #TempData order by SORT 

  select  * into ##finaldata from #TempData order by SORT    

  '  

   )  
   --------------------To implement the search functionality  - Start



 IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE #Temp1')  
end  


if(@type = 'product')

begin
 
 print 'kids'

 Select column_id as Row_Number,name into #Temp1 From  Tempdb.Sys.Columns Where Object_ID = Object_ID('tempdb..##TEMPSEARCHDATA')
  
   declare @a int = 1;
   declare @name nvarchar(max);
   declare @sysAttribute nvarchar(max) ;
   declare @coloum nvarchar(max);
   declare @ColumName nvarchar(max)


   while ((select count(*) + 1 from  #Temp1)  > @a)
   begin
        set @name = (select name from  #Temp1 where Row_Number =  @a)
		if CHARINDEX('__OBJ',@name) > 0
		  begin
			if(@AttributeSeach =  (select SUBSTRING(@name,1,(SELECT CHARINDEX ('__OBJ',@name) -1))))
			begin
			set @sysAttribute = @name
			print @sysAttribute
			end
		end
		   set @a = @a + 1
   end

   ----------------------------------------------------------------------------------------------------------------------------
set @coloum = @sysAttribute
set @ColumName = '##TEMPSEARCHDATA.[' +@coloum +']'
print 'ooooops1'
print @ColumName
print 'ooooops1'
print @data


if(@ColumName is not null)
begin
print 'IAM not Null'
if(@SearchType = 'Contains')
begin
print 'Contains'
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   like   ''%'+@data+'%''')
end
else if(@SearchType = 'Exact')
begin
print 'EXACT'
print @ColumName
print @data
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   =   '''+@data+'''   ')
end
else if(@SearchType = 'Starts With')
begin
print 'Starts With'
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   like   '''+@data+'%''')
end
else if(@SearchType = 'Ends With')
begin
print 'Ends With'
exec ('select * from  ##TEMPSEARCHDATA  where '+@ColumName+'   like   ''%'+@data+'''')
end

end
else
begin
print 'IAm a Null'
--select * from ##finaldata

--delete from ##finaldata
select * from ##finaldata
end





End
else
begin
 print '2'  
select * from ##finaldata order by SORT 
 print '22'  
end



 -----------------To implement the search functionality  - End
 
 
 END  
   
ELSE  
 BEGIN  

 	 print 'oops1'

  EXEC('  
  SELECT ROW_NUMBER() over (ORDER BY PRODUCT_ID) AS ID, * FROM   
  (  
  SELECT distinct TCF.CATALOG_ID,TF.CATEGORY_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,TPF.PUBLISH2PRINT ,TPF.PUBLISH2WEB ,TPF.PUBLISH2PORTAL,TPF.PUBLISH2EXPORT,TPF.PUBLISH2PDF
  FROM TB_FAMILY TF   
  JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID AND TF.FAMILY_ID in ('+ @FAMILY_ID +') AND TCF.CATALOG_ID = '+ @CATALOG_ID +'  
  JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID AND TPF.FLAG_RECYCLE =''A''  
  JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID  
  JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID  
  JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE =''A''  
  JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID
    
  )AS A    
  --SELECT * FROM #TEMPPRODSPECS WHERE ID BETWEEN  '+ @FROMPAGE +' AND '+ @TOPAGE +'  
   ')  
 END  
/****** Object:  StoredProcedure [dbo].[STP_LS_ProductPivotTable_For_PublishWithoutAttributes]    Script Date: 03/02/2017 17:27:31 ******/  
SET ANSI_NULLS ON



GO



ALTER  PROCEDURE [dbo].[STP_LS_ProductPivotTable_For_PublishWithoutAttributes] 
@FAMILY_ID VARCHAR(MAX)='26143,26144,26145,26146,26147,26148,26149',  
@CATALOG_ID INT=123,
@USER_ID VARCHAR(50)  ='demo@questudio.com',
@CATEGORY_ID nvarchar(100)='CAT12589'
AS
DECLARE @ATTRIBUTE_NAMES VARCHAR(MAX);
DECLARE @ROLE_ID INT =0
DECLARE @PRODUCT_ID INT =0
Declare @FAMILY_ID1 Varchar(max);
SET @FAMILY_ID1 = REPLACE(@FAMILY_ID, '''', '')
SET @ATTRIBUTE_NAMES = '' 
Select @ROLE_ID = Role_id from aspnet_Users  au join aspnet_UsersInRoles aur on au.UserId = aur.UserId JOIN aspnet_Roles ar on  ar.RoleId = aur.RoleId where UserName =@USER_ID
IF OBJECT_ID('tempdb..##FilterValu') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##FilterValu')  
end  
 EXEC('DROP TABLE PRODUCT_ID')  
EXEC('Select TOP 1  PRODUCT_ID INTO PRODUCT_ID FROM TB_PROD_FAMILY where FAMILY_ID IN ('+@FAMILY_ID+')  and FLAG_RECYCLE=''A''')
  
SET @PRODUCT_ID = (Select PRODUCT_ID FROM PRODUCT_ID)

EXEC(';WITH TTT  
AS  
(  
SELECT DISTINCT * FROM (  
SELECT DISTINCT TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME=''Supplier'' THEN ATTRIBUTE_NAME +''__OBJ__0'' +''__''+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) END +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END AS ATTRIBUTE_NAME FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID    
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID   AND TP.FLAG_RECYCLE =''A'' 
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE =''A'' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3) AND TA.CREATE_BY_DEFAULT = 1  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+@CATALOG_ID+' 
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID ='+@ROLE_ID+'  
WHERE TF.FAMILY_ID IN ('+@FAMILY_ID1+') AND TCF.CATALOG_ID= '+@CATALOG_ID+'  AND TA.ATTRIBUTE_TYPE NOT IN (9,7,11,12,13)
UNION ALL  
SELECT DISTINCT TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME+''__'' + ''OBJ''+''__''+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END +''__''+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME
FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID  
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE =''A'' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)   AND TA.CREATE_BY_DEFAULT = 1  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+@CATALOG_ID+'   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID ='+@ROLE_ID+'  
WHERE TF.FAMILY_ID IN ('+@FAMILY_ID1+') AND TCF.CATALOG_ID='+@CATALOG_ID+' AND TA.ATTRIBUTE_TYPE NOT IN (9,7,11,12,13)
UNION ALL  
SELECT DISTINCT TPS.ATTRIBUTE_ID,ATTRIBUTE_NAME+ ''__1__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END   AS ATTRIBUTE_NAME  
 FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID   
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE =''A''  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID  AND (TA.FLAG_RECYCLE =''A'' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)   AND TA.CREATE_BY_DEFAULT = 1  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+@CATALOG_ID+'   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID  
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID ='+@ROLE_ID+'  
WHERE TF.FAMILY_ID IN ('+@FAMILY_ID1+') AND TCF.CATALOG_ID='+@CATALOG_ID+'  AND TA.ATTRIBUTE_TYPE = 3 AND TA.ATTRIBUTE_TYPE NOT IN (9,7,11,12,13)
  
 ) DD    
)   
select * into ##FilterValu from TTT



')

 SELECT @ATTRIBUTE_NAMES = @ATTRIBUTE_NAMES +'['+ ATTRIBUTE_NAME + '],' FROM ##FilterValu ORDER BY ATTRIBUTE_ID

IF(@ATTRIBUTE_NAMES!=NULL OR @ATTRIBUTE_NAMES !='')
BEGIN
SET @ATTRIBUTE_NAMES = LEFT(@ATTRIBUTE_NAMES, LEN(@ATTRIBUTE_NAMES) - 1)
print @ATTRIBUTE_NAMES
END

IF(@ATTRIBUTE_NAMES!=NULL OR @ATTRIBUTE_NAMES !='')
	BEGIN
		EXEC('
		SELECT *  FROM 
		(
		SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE ''True'' END as PUBLISH,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE ''True'' END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]
		,CASE WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%TEXT%'' THEN TPS.STRING_VALUE 
		WHEN TA.ATTRIBUTE_DATATYPE LIKE ''%NUM%'' THEN --CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE) 
		cast(ROUND(CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE),cast(substring(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''), CHARINDEX('','',REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))+1, len(REPLACE(TA.ATTRIBUTE_DATATYPE,'')'',''''))) as int)) as varchar)	
		WHEN  TA.ATTRIBUTE_DATATYPE LIKE ''%HYPER%''  THEN TPS.STRING_VALUE
		ELSE TPS.STRING_VALUE
		END 
		STRING_VALUE,CASE WHEN ATTRIBUTE_NAME=''Supplier'' THEN ATTRIBUTE_NAME +''__OBJ__0''+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) 
		ELSE ATTRIBUTE_NAME +''__OBJ__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) 
		END +''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END   AS ATTRIBUTE_NAME 		
		  FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID	
			JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID
			--JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON TF.FAMILY_ID = TPFAL.FAMILY_ID and TPF.FLAG_RECYCLE=''A''
			JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID 
			JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID and TP.FLAG_RECYCLE=''A''
			JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID
			JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +' 
			JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS
			LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID 
			LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID 
			LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'
			WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +'   AND TA.ATTRIBUTE_ID =1
		UNION ALL
			SELECT DISTINCT TPS.CATALOG_ID,TPS.FAMILY_ID,TPS.PRODUCT_ID,TPF.SORT_ORDER SORT,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE ''True'' END as PUBLISH,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE ''True'' END as PUBLISH2PRINT, TWS.STATUS_NAME as [WORKFLOW STATUS] ,
			ATTRIBUTE_VALUE,
			ATTRIBUTE_NAME + ''__OBJ''+''__''+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) 
			+''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME 
			FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID	
			JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID and TPF.FLAG_RECYCLE=''A''
			JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID and TPS.CATEGORY_ID = '''+@CATEGORY_ID+'''
			JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID and TP.FLAG_RECYCLE=''A''
			JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID
			JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +' 
			JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS 
			LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID 
			LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID 
			LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'
			WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +'	 AND TA.ATTRIBUTE_ID =1	
		UNION ALL
		SELECT TCF.CATALOG_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE ''True'' END as PUBLISH,CASE WHEN CAST(ISNULL(TCP.CATALOG_ID,0) as nvarchar) = ''0'' THEN ''False'' ELSE ''True'' END as PUBLISH2PRINT,TWS.STATUS_NAME as [WORKFLOW STATUS]
		,ISNULL(TPS.OBJECT_NAME , '''')  AS STRING_VALUE
		,ATTRIBUTE_NAME + ''__1__''+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+''__''+CAST(ATTRIBUTE_TYPE as nvarchar) 
		+''__''+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END+''__''+ CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = ''0'' THEN ''false'' ELSE ''true'' END  AS ATTRIBUTE_NAME
		 FROM TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID	
		JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID and TPF.FLAG_RECYCLE=''A''
		JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID --AND TPS.PRODUCT_ID =TPFAL.PRODUCT_ID 
		JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID and TP.FLAG_RECYCLE=''A''
		JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID
		JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID='+ @CATALOG_ID +'		
		JOIN TB_WORKFLOW_STATUS TWS ON TWS.STATUS_CODE = TPF.WORKFLOW_STATUS 
		LEFT OUTER JOIN TB_CATALOG_PRODUCT TCP ON  TCP.PRODUCT_ID=TPF.PRODUCT_ID and TCP.CATALOG_ID=TCF.CATALOG_ID 
		LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID
		LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =' +@ROLE_ID +'
		WHERE TF.FAMILY_ID in ('+ @FAMILY_ID1 +') AND TCF.CATALOG_ID='+ @CATALOG_ID +'  AND TA.ATTRIBUTE_TYPE = 3  AND TA.ATTRIBUTE_ID =1
		)AS A
		PIVOT
		(
			MAX(STRING_VALUE) FOR ATTRIBUTE_NAME IN ('+ @ATTRIBUTE_NAMES +')
		)AS P order by SORT
		')
	END
ELSE
	BEGIN
		EXEC('
		SELECT * FROM 
		(
		SELECT TCF.CATALOG_ID,TF.CATEGORY_ID,TF.FAMILY_ID,TPF.PRODUCT_ID,TPF.SORT_ORDER SORT,TPF.PUBLISH2PRINT ,TPF.PUBLISH2WEB ,TPF.PUBLISH2PORTAL,TPF.PUBLISH2EXPORT,TPF.PUBLISH2PDF
		FROM TB_FAMILY TF 
		JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID AND TF.FAMILY_ID = '+ @FAMILY_ID +' AND TCF.CATALOG_ID = '+ @CATALOG_ID +'
		JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID and TPF.FLAG_RECYCLE=''A''
		JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID
		JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID
		JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID and TP.FLAG_RECYCLE=''A''
		JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID
		)AS A 
		')
	END
















GO

Select * From TB_APPLICATION_SETTINGS

UPDATE TB_APPLICATION_SETTINGS SET ImportSheetProductCount = 100000

















