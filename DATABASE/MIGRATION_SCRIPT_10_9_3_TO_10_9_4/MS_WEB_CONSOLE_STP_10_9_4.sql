GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_SYNC_SNAPSHOT]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_SYNC_SNAPSHOT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_QSWS_SYNC_SNAPSHOT]
GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_IMAGE_LOOPING]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_IMAGE_LOOPING]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_QSWS_IMAGE_LOOPING]
GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GetFilterFamilyDetails]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GetFilterFamilyDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_QSWS_GetFilterFamilyDetails]
GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GET_TEXTSEARCH_RESULT_REFINEFILTER_LEVEL]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GET_TEXTSEARCH_RESULT_REFINEFILTER_LEVEL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_QSWS_GET_TEXTSEARCH_RESULT_REFINEFILTER_LEVEL]
GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GET_FILTER_DETAILS]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GET_FILTER_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_QSWS_GET_FILTER_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GET_AUTOCOMPLETE_TEXTSEARCH_RESULT_REFINEFILTER_SESSION_ID]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GET_AUTOCOMPLETE_TEXTSEARCH_RESULT_REFINEFILTER_SESSION_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_QSWS_GET_AUTOCOMPLETE_TEXTSEARCH_RESULT_REFINEFILTER_SESSION_ID]
GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GET_ATTRIBUTE_FILTER_DETAILS]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GET_ATTRIBUTE_FILTER_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_QSWS_GET_ATTRIBUTE_FILTER_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_CONFIGURATION_SETTINGS]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_CONFIGURATION_SETTINGS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_QSWS_CONFIGURATION_SETTINGS]
GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_CATALOG_SNAPSHOT]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_CATALOG_SNAPSHOT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_QSWS_CATALOG_SNAPSHOT]
GO
/****** Object:  StoredProcedure [dbo].[STP_LS_GET_WEB_ITEMS_DETAILS]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_LS_GET_WEB_ITEMS_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_LS_GET_WEB_ITEMS_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS_FAMILYATTRIBUTES]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS_FAMILYATTRIBUTES]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS_FAMILYATTRIBUTES]
GO
/****** Object:  StoredProcedure [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS]
GO
/****** Object:  StoredProcedure [dbo].[STP_LS_GET_WEB_FAMILY_DETAILS]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_LS_GET_WEB_FAMILY_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_LS_GET_WEB_FAMILY_DETAILS]
GO
/****** Object:  StoredProcedure [dbo].[STP_GET_TEMPLATE_NAME_EMSER]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_GET_TEMPLATE_NAME_EMSER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_GET_TEMPLATE_NAME_EMSER]
GO
/****** Object:  StoredProcedure [dbo].[STP_GET_DATATYPE_AND_DATARULE]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_GET_DATATYPE_AND_DATARULE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_GET_DATATYPE_AND_DATARULE]
GO
/****** Object:  StoredProcedure [dbo].[STP_CATALOGSTUDIO_GETFAMILYID_FROM_ROOTCATEGORY]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_CATALOGSTUDIO_GETFAMILYID_FROM_ROOTCATEGORY]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[STP_CATALOGSTUDIO_GETFAMILYID_FROM_ROOTCATEGORY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetItemCountForProductId]    Script Date: 2023-01-27 17:37:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetItemCountForProductId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetItemCountForProductId]
GO
/****** Object:  UserDefinedFunction [dbo].[GetItemCountForProductId]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetItemCountForProductId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create FUNCTION [dbo].[GetItemCountForProductId](@ProductId AS INT)  
RETURNS TABLE 
AS 

RETURN 
   ( 
     
   select COUNT(PRODUCT_ID) as Value from TB_PROD_FAMILY where FAMILY_ID =@ProductId  and FLAG_RECYCLE = ''A''

   ) 
' 
END

GO
/****** Object:  StoredProcedure [dbo].[STP_CATALOGSTUDIO_GETFAMILYID_FROM_ROOTCATEGORY]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_CATALOGSTUDIO_GETFAMILYID_FROM_ROOTCATEGORY]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_CATALOGSTUDIO_GETFAMILYID_FROM_ROOTCATEGORY] AS' 
END
GO


ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO_GETFAMILYID_FROM_ROOTCATEGORY]

@CATEGORY_ID VARCHAR(MAX)='CAT5552,CAT5842',
@CATALOG_ID INT=6

AS
BEGIN 

IF OBJECT_ID('TEMPDB..#TEMPCAT') IS NOT NULL                   
DROP TABLE #TEMPCAT  

;WITH T AS                  
(     
             
SELECT A.CATEGORY_ID,A.CATEGORY_NAME,A.PARENT_CATEGORY,A.CREATED_DATE,1 AS 'LEVEL'
 FROM TB_CATEGORY A JOIN TB_CATALOG_SECTIONS B ON A.CATEGORY_ID=B.CATEGORY_ID 
WHERE A.FLAG_RECYCLE =  'A' AND A.PUBLISH2WEB = 1 AND A.CATEGORY_ID IN --(@CATEGORY_ID)
(SELECT ITEM FROM SplitString(@CATEGORY_ID,',')) 

AND B.CATALOG_ID=@CATALOG_ID                 
UNION ALL                  
SELECT A.CATEGORY_ID,A.CATEGORY_NAME,A.PARENT_CATEGORY, A.CREATED_DATE, T.LEVEL + 1 
 FROM TB_CATEGORY A JOIN T ON A.PARENT_CATEGORY = T.CATEGORY_ID  AND A.FLAG_RECYCLE = 'A'  AND A.PUBLISH2WEB = 1                
)
SELECT * INTO #TEMPCAT 
FROM T                  

SELECT DISTINCT c.FAMILY_ID,c.CATEGORY_ID FROM TB_FAMILY C 
JOIN #TEMPCAT D ON D.CATEGORY_ID=C.CATEGORY_ID AND C.FLAG_RECYCLE = 'A' AND C.PUBLISH2WEB = 1

END









GO
/****** Object:  StoredProcedure [dbo].[STP_GET_DATATYPE_AND_DATARULE]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_GET_DATATYPE_AND_DATARULE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_GET_DATATYPE_AND_DATARULE] AS' 
END
GO


ALTER procedure [dbo].[STP_GET_DATATYPE_AND_DATARULE]
(
@FAMILY_ID varchar(max)='23208',   
@PRODUCT_ID varchar(max)= '26112',
@CATALOG_ID varchar(max)='51'
--,@ATTRIBUTE_ID varchar(max)=''    
)
as
DROP TABLE IF EXISTS  #temp
--- Get the Attribute data type and symbols
select ATTRIBUTE_NAME, ATTRIBUTE_DATATYPE, ATTRIBUTE_DATARULE into #temp from TB_PROD_SPECS TPS 
join TB_ATTRIBUTE TB on TPS.ATTRIBUTE_ID=TB.ATTRIBUTE_ID where PRODUCT_ID=@PRODUCT_ID and ATTRIBUTE_DATATYPE like 'Num%' AND TB.FLAG_RECYCLE = 'A' AND TB.PUBLISH2WEB = 1
union select ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE, ATTRIBUTE_DATARULE  from TB_FAMILY_SPECS TFS join TB_ATTRIBUTE TBF 
on TFS.ATTRIBUTE_ID=TBF.ATTRIBUTE_ID where FAMILY_ID=@FAMILY_ID and ATTRIBUTE_DATATYPE like 'Num%' AND TBF.FLAG_RECYCLE = 'A' AND TBF.PUBLISH2WEB = 1

select * from #temp
---------   Take Prefix value from table
DROP TABLE IF EXISTS  #temp_Prefix
;WITH CTE_XML AS
	(SELECT  ATTRIBUTE_NAME, CAST( REPLACE (ATTRIBUTE_DATARULE,'<?xml version="1.0" encoding="utf-8"?>','')  as XML) as Prefix_Value	FROM #temp) 
	SELECT CONVERT(varchar(max),T2.Loc.query('.')) as nodes  into #temp_Prefix
	FROM   CTE_XML
	CROSS APPLY Prefix_Value.nodes('/NewDataSet//DataRule//Prefix') as T2(Loc)
--select * from #temp_Prefix

--------   Take Suffix value from table
DROP TABLE IF EXISTS  #temp_Suffix
;WITH CTE_XML AS
	(SELECT ATTRIBUTE_NAME, CAST( REPLACE (ATTRIBUTE_DATARULE,'<?xml version="1.0" encoding="utf-8"?>','')  as XML) as Suffix_Value	FROM #temp) 
	SELECT CONVERT(varchar(max),T2.Loc.query('.')) as nodes  into #temp_Suffix
	FROM   CTE_XML
	CROSS APPLY Suffix_Value.nodes('/NewDataSet//DataRule//Suffix') as T2(Loc)	
--select * from #temp_Suffix

--- Merege into one talbe
DROP TABLE IF EXISTS  #temp_final
select  distinct c.ATTRIBUTE_NAME,c.ATTRIBUTE_DATATYPE, TRIM('<Prefix></Prefix>' from a.nodes) AS Prefix, TRIM('<Suffix></Suffix>' from b.nodes) as Suffix into #temp_final
from #temp_Prefix a, #temp_Suffix b,#temp c
 --where c.ATTRIBUTE_NAME=a.ATTRIBUTE_NAME 

select * from #temp_final


GO
/****** Object:  StoredProcedure [dbo].[STP_GET_TEMPLATE_NAME_EMSER]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_GET_TEMPLATE_NAME_EMSER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_GET_TEMPLATE_NAME_EMSER] AS' 
END
GO





ALTER Procedure [dbo].[STP_GET_TEMPLATE_NAME_EMSER](
@CATALOG_ID varchar(50) = '122',
@CATEGORY_ID varchar(max)='CAT12581',   
@FAMILY_ID varchar(max)='',  
@PRODUCT_ID varchar(max)='' 
)
as 
----  Category----
if(@CATALOG_ID<>'' and @CATEGORY_ID<>'' and @FAMILY_ID='' and @PRODUCT_ID='')
begin
declare @TYPE_CAT varchar(max)='Category';
declare @result_C varchar(max);
set @result_C=(Select TEMPLATE_PATH From TB_PDFXPRESS_HIERARCHY 
Where ASSIGN_TO =  @CATEGORY_ID and TYPE=@TYPE_CAT AND CATALOG_ID = @CATALOG_ID)

IF LEN(ISNULL(@result_C, '')) = 0
begin
Select TEMPLATE_PATH From TB_PDFXPRESS_HIERARCHY Where ASSIGN_TO = @CATALOG_ID
end
else
select @result_C as TEMPLATE_PATH
end

--- Family ---
else if(@CATALOG_ID<>'' and @CATEGORY_ID='' and @FAMILY_ID<>'' and @PRODUCT_ID ='')
begin

declare @result_F varchar(max);
declare @TYPE_FA varchar(max)='Family';
set @result_F=(Select TEMPLATE_PATH From TB_PDFXPRESS_HIERARCHY Where ASSIGN_TO =  @FAMILY_ID
 and TYPE=@TYPE_FA AND CATALOG_ID = @CATALOG_ID)

IF LEN(ISNULL(@result_F, '')) = 0
Select TEMPLATE_PATH From TB_PDFXPRESS_HIERARCHY Where ASSIGN_TO = @CATALOG_ID
else
select @result_F as TEMPLATE_PATH

end

--- Product--- 
else if(@CATALOG_ID<>'' and @CATEGORY_ID<>'' and @FAMILY_ID<>'' and @PRODUCT_ID<>'')
begin
declare @result_P varchar(max);
declare @TYPE_PROD varchar(max)='Product';
set @result_P =(Select TEMPLATE_PATH From TB_PDFXPRESS_HIERARCHY 
Where ASSIGN_TO =  @FAMILY_ID and TYPE=@TYPE_PROD AND CATALOG_ID = @CATALOG_ID)
IF LEN(ISNULL(@result_P, '')) = 0
Select TEMPLATE_PATH From TB_PDFXPRESS_HIERARCHY Where ASSIGN_TO = @CATALOG_ID
else
select @result_P as TEMPLATE_PATH
end

else
begin

Select TEMPLATE_PATH From TB_PDFXPRESS_HIERARCHY Where ASSIGN_TO = @CATALOG_ID
end





GO
/****** Object:  StoredProcedure [dbo].[STP_LS_GET_WEB_FAMILY_DETAILS]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_LS_GET_WEB_FAMILY_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_LS_GET_WEB_FAMILY_DETAILS] AS' 
END
GO

ALTER PROCEDURE [dbo].[STP_LS_GET_WEB_FAMILY_DETAILS]
(@FAMILY_ID int =23208,
@CATALOG_ID int =51,
@CATEGORY_ID nvarchar(max)='CAT12001',
@lastLevel int=1)


AS
BEGIN

DECLARE @LevelOfCategory nvarchar(20)
DECLARE @GETFAMILYATTRIBUTECOLUMN nvarchar(max)='',@GETTABLENAME nvarchar(max)=''

SET @GETFAMILYATTRIBUTECOLUMN=(SELECT Search_Family_Attributes FROM QSWS_CONFIGRATION_SETTINGS WHERE Catalog_Id=@CATALOG_ID)
SET @GETTABLENAME='QSWS_CATALOG_SNAPSHOT' +'_'+ CAST (@CATALOG_ID as nvarchar(10))

IF (@lastLevel=1)
BEGIN
SET @LevelOfCategory='CATEGORY_ID'
END
ELSE IF (@lastLevel=2)
BEGIN
SET @LevelOfCategory='SUBCATL1_ID'
END
ELSE IF (@lastLevel=3)
BEGIN
SET @LevelOfCategory='SUBCATL2_ID'
END
ELSE IF (@lastLevel=4)
BEGIN
SET @LevelOfCategory='SUBCATL3_ID'
END
ELSE IF (@lastLevel=5)
BEGIN
SET @LevelOfCategory='SUBCATL4_ID'
END
ELSE IF (@lastLevel=6)
BEGIN
SET @LevelOfCategory='SUBCATL5_ID'
END
ELSE IF (@lastLevel=7)
BEGIN
SET @LevelOfCategory='SUBCATL6_ID'
END
ELSE IF (@lastLevel=8)
BEGIN
SET @LevelOfCategory='SUBCATL7_ID'
END
IF @GETFAMILYATTRIBUTECOLUMN IS NOT NULL OR @GETFAMILYATTRIBUTECOLUMN <> ''
BEGIN

EXEC('SELECT FAMILY_ID,FAMILY_NAME as Product_Name,FAMILY_IMAGE,'+@GETFAMILYATTRIBUTECOLUMN+' from '+@GETTABLENAME+' where FAMILY_ID='+@FAMILY_ID+' and '+@LevelOfCategory+'='''+ @CATEGORY_ID +''' '  )

END
ELSE
BEGIN

EXEC('SELECT FAMILY_ID,FAMILY_NAME as Product_Name,FAMILY_IMAGE from '+@GETTABLENAME+'  where FAMILY_ID='+@FAMILY_ID+' and '+@LevelOfCategory+'='''+ @CATEGORY_ID +'''')

END

END



 







GO
/****** Object:  StoredProcedure [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS] AS' 
END
GO


ALTER PROCEDURE [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS]
( @LEVEL int=1,
@TYPE nvarchar(5)='C',
@CATGORY_ID nvarchar(10)='CAT12589',
@CATLOG_ID int=123)
AS
BEGIN
DECLARE @GETTABLENAME nvarchar(max)

SET @GETTABLENAME='QSWS_CATALOG_SNAPSHOT' +'_'+ CAST (@CATLOG_ID as nvarchar(10))
if @LEVEL = 0 and  @TYPE ='C'
BEGIN
EXEC('select distinct CATEGORY_ID,CATEGORY_NAME,Category_Image as IMAGE_FILE,''C'' as TYPE,1 as Level from '+@GETTABLENAME+'')
END

ELSE IF  @LEVEL = 1 and  @TYPE ='C'
BEGIN

EXEC('select distinct SUBCATL1_ID as CATEGORY_ID,SUBCATL1_NAME as CATEGORY_NAME,Category_Image as IMAGE_FILE,''C'' as TYPE,2 as Level,1 as rowOrder from '+@GETTABLENAME+' where CATEGORY_ID='''+@CATGORY_ID+''' and SUBCATL1_ID is not null and SUBCATL1_NAME is not null

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''') Order by rowOrder')

END



ELSE IF  @LEVEL = 2 and  @TYPE ='C'
BEGIN
EXEC('select distinct SUBCATL2_ID as CATEGORY_ID,SUBCATL2_NAME as CATEGORY_NAME,Category_Image as IMAGE_FILE,''C'' as TYPE,3 as Level,1 as rowOrder from '+@GETTABLENAME+' where SUBCATL1_ID='''+@CATGORY_ID+''' and SUBCATL2_NAME is not null and SUBCATL2_NAME is not null

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''') Order by rowOrder')
END
ELSE IF  @LEVEL = 3 and  @TYPE ='C'
BEGIN
EXEC('select distinct SUBCATL3_ID as CATEGORY_ID,SUBCATL3_NAME as CATEGORY_NAME,Category_Image as IMAGE_FILE,''C'' as TYPE,4 as Level,1 as rowOrder from '+@GETTABLENAME+' where SUBCATL2_ID='''+@CATGORY_ID+''' and SUBCATL3_ID is not null and SUBCATL3_NAME is not null

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''') Order by rowOrder')

END
ELSE IF  @LEVEL = 4 and  @TYPE ='C'
BEGIN
EXEC('select distinct SUBCATL4_ID as CATEGORY_ID,SUBCATL4_NAME as CATEGORY_NAME,Category_Image as IMAGE_FILE,''C'' as TYPE,5 as Level,1 as rowOrder from '+@GETTABLENAME+' where SUBCATL3_ID='''+@CATGORY_ID+''' and SUBCATL4_ID is not null and SUBCATL4_NAME is not null

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''') Order by rowOrder')

END
ELSE IF  @LEVEL = 5 and  @TYPE ='C'
BEGIN
EXEC('select distinct SUBCATL5_ID as CATEGORY_ID,SUBCATL5_NAME as CATEGORY_NAME,Category_Image as IMAGE_FILE,''C'' as TYPE,6 as Level,1 as rowOrder from '+@GETTABLENAME+' where SUBCATL4_ID='''+@CATGORY_ID+''' and SUBCATL5_ID is not null and SUBCATL5_NAME is not null

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''') Order by rowOrder')

END
ELSE IF  @LEVEL = 6 and  @TYPE ='C'
BEGIN
EXEC('select distinct SUBCATL6_ID as CATEGORY_ID,SUBCATL6_NAME as CATEGORY_NAME,Category_Image as IMAGE_FILE,''C'' as TYPE,7 as Level,1 as rowOrder from '+@GETTABLENAME+' where SUBCATL5_ID='''+@CATGORY_ID+''' and SUBCATL6_ID is not null and SUBCATL6_NAME is not null

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''') Order by rowOrder')

END
ELSE IF  @LEVEL = 7 and  @TYPE ='C'
BEGIN
EXEC('select distinct SUBCATL7_ID as CATEGORY_ID,SUBCATL7_NAME as CATEGORY_NAME,Category_Image as IMAGE_FILE,''C'' as TYPE,8 as Level,1 as rowOrder from '+@GETTABLENAME+' where SUBCATL6_ID='''+@CATGORY_ID+''' and SUBCATL7_ID is not null and SUBCATL7_NAME is not null

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''') Order by rowOrder')

END
ELSE IF  @LEVEL = 8 and  @TYPE ='C'
BEGIN
EXEC('

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')

UNION

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder  from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''') Order by rowOrder')

END
END





GO
/****** Object:  StoredProcedure [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS_FAMILYATTRIBUTES]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS_FAMILYATTRIBUTES]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS_FAMILYATTRIBUTES] AS' 
END
GO


ALTER PROCEDURE [dbo].[STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS_FAMILYATTRIBUTES]
( @LEVEL int=3,
@TYPE nvarchar(5)='C',
@CATGORY_ID nvarchar(10)='CAT34',
@CATLOG_ID int=8,
@IS_CLONE Varchar(5)='0'

)
AS
BEGIN
DECLARE @GETTABLENAME nvarchar(max)

DECLARE @Family_ATTRIBUTE_VARIABLE NVARCHAR(MAX) = ''


DECLARE @Family_ATTRIBUTES_VARIABLE NVARCHAR(MAX) = ''

select @Family_ATTRIBUTE_VARIABLE = REPLACE(REPLACE(Search_Family_Attributes,'[', ''''),']','''')  from QSWS_CONFIGRATION_SETTINGS where catalog_id = @CATLOG_ID

--select @Family_ATTRIBUTE_VARIABLE = REPLACE(Search_Family_Attributes  from QSWS_CONFIGRATION_SETTINGS where catalog_id = @CATALOG_ID

select @Family_ATTRIBUTES_VARIABLE = Search_Family_Attributes  from QSWS_CONFIGRATION_SETTINGS where catalog_id = @CATLOG_ID

print @Family_ATTRIBUTES_VARIABLE

SET @GETTABLENAME='QSWS_CATALOG_SNAPSHOT' +'_'+ CAST (@CATLOG_ID as nvarchar(10))
if @LEVEL = 0 and  @TYPE ='C'
BEGIN
EXEC('select distinct GT.CATEGORY_ID,GT.CATEGORY_NAME,GT.Category_Image as IMAGE_FILE,''C'' as TYPE,1 as Level,''RC'' AS LEVEL_FLAG,TCS.SORT_ORDER,GT.IS_CLONE from '+@GETTABLENAME+' GT
join TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID=GT.CATEGORY_ID
 join TB_CATALOG TC on TC.CATALOG_ID = TCS.CATALOG_ID
Where TC.CATALOG_ID = '+@CATLOG_ID+' AND TCS.FLAG_RECYCLE = ''A''
 GROUP BY GT.CATEGORY_ID,GT.CATEGORY_NAME,GT.Category_Image,TCS.SORT_ORDER,GT.IS_CLONE
ORDER BY TCS.SORT_ORDER ')
END

ELSE IF  @LEVEL = 1 and  @TYPE ='C'
BEGIN

EXEC('SELECT * FROM(
select distinct GT.SUBCATL1_ID as CATEGORY_ID,GT.SUBCATL1_NAME as CATEGORY_NAME,GT.SUBCATL1_IMAGE as IMAGE_FILE,''C'' as TYPE,2 as Level,1 as rowOrder,''SC'' AS LEVEL_FLAG, 
'''' as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCS.SORT_ORDER,row_number() over
    (
        partition by GT.SUBCATL1_ID
        order by GT.SUBCATL1_ID
    )  as rn,GT.SUBCATL1_ID_IS_CLONE AS IS_CLONE,GT.ROOT_CATEGORY,GT.CATEGORY_ID AS CATEGORY_SEARCH_ID from '+@GETTABLENAME+' GT
join TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID=GT.CATEGORY_ID
where GT.CATEGORY_ID='''+@CATGORY_ID+''' and GT.SUBCATL1_ID is not null and GT.SUBCATL1_NAME is not null
GROUP BY GT.SUBCATL1_ID,GT.SUBCATL1_NAME,GT.SUBCATL1_IMAGE,TCS.SORT_ORDER,'+@Family_ATTRIBUTES_VARIABLE+',GT.SUBCATL1_ID_IS_CLONE,GT.ROOT_CATEGORY,GT.CATEGORY_ID
--ORDER BY TCS.SORT_ORDER
--UNION

--select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,'''' as Product_count,'+@Family_ATTRIBUTES_VARIABLE+'   from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')


UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE AS IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.CATEGORY_ID AS CATEGORY_SEARCH_ID
	 from TB_CATALOG_FAMILY 
 TPF LEFT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+'  
WHERE  qcsp.FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''') AND  TCF.ROOT_CATEGORY = ''0'' and QCSP.ROOT_CATEGORY = ''0'' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID, qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.CATEGORY_ID
,QCSP.ROOT_CATEGORY 

 
UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE AS IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.CATEGORY_ID AS CATEGORY_SEARCH_ID  from TB_CATALOG_FAMILY TPF 
 RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+'  AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
WHERE QCSP.CATEGORY_ID='''+@CATGORY_ID+'''  AND  TCF.ROOT_CATEGORY <> ''0'' and QCSP.ROOT_CATEGORY <> ''0''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.CATEGORY_ID


  ) AS T Where rn=1
 Order by SORT_ORDER

--UNION

--select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder,'+@Family_ATTRIBUTES_VARIABLE+'   from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''')

-- Order by rowOrder
') 



END



ELSE IF  @LEVEL = 2 and  @TYPE ='C'
BEGIN
EXEC('SELECT * FROM(
select distinct  GT.SUBCATL2_ID as CATEGORY_ID,GT.SUBCATL2_NAME as CATEGORY_NAME,GT.SUBCATL2_IMAGE as IMAGE_FILE,''C'' as TYPE,3 as Level,1 as rowOrder,''SC'' AS LEVEL_FLAG
,'''' as Product_count,'+@Family_ATTRIBUTES_VARIABLE+' ,TCS.SORT_ORDER,row_number() over
    (
        partition by GT.SUBCATL2_ID
        order by GT.SUBCATL2_ID
    )  as rn,GT.SUBCATL2_ID_IS_CLONE AS IS_CLONE,GT.ROOT_CATEGORY,GT.SUBCATL1_ID AS CATEGORY_SEARCH_ID from '+@GETTABLENAME+' GT
join TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID=GT.CATEGORY_ID
 where SUBCATL1_ID='''+@CATGORY_ID+''' and SUBCATL2_NAME is not null and SUBCATL2_NAME is not null
 GROUP BY GT.SUBCATL2_NAME,GT.SUBCATL2_ID,GT.CATEGORY_ID,GT.CATEGORY_NAME,GT.SUBCATL2_IMAGE,TCS.SORT_ORDER,GT.SUBCATL2_ID_IS_CLONE,'+@Family_ATTRIBUTES_VARIABLE+',GT.ROOT_CATEGORY,GT.SUBCATL1_ID 
--UNION

--select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,'''' as Product_count,'+@Family_ATTRIBUTES_VARIABLE+'  from '+@GETTABLENAME+' where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')


UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE AS IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.SUBCATL1_ID AS CATEGORY_SEARCH_ID  from TB_CATALOG_FAMILY TPF 
 RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' 
WHERE  qcsp.FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')  AND  TCF.ROOT_CATEGORY = ''0'' and QCSP.ROOT_CATEGORY = ''0'' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.SUBCATL1_ID

--UNION

---select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,3 as rowOrder,'''' as Product_count,'+@Family_ATTRIBUTES_VARIABLE+'  from '+@GETTABLENAME+' where FAMILY_ID in (select SUBFAMILY_ID from TB_SUBFAMILY TS join TB_FAMILY TF on TS.FAMILY_ID=TF.FAMILY_ID where CATEGORY_ID='''+@CATGORY_ID+''')

  UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE AS IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.SUBCATL1_ID AS CATEGORY_SEARCH_ID  from TB_CATALOG_FAMILY TPF 
 RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
WHERE QCSP.SUBCATL1_ID='''+@CATGORY_ID+'''  AND  TCF.ROOT_CATEGORY <> ''0'' and QCSP.ROOT_CATEGORY <> ''0''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.SUBCATL1_ID
  

  ) AS T Where rn=1 
 Order by SORT_ORDER
 ')
 

END
ELSE IF  @LEVEL = 3 and  @TYPE ='C'
BEGIN
EXEC('SELECT * FROM(
select distinct  GT.SUBCATL3_ID as CATEGORY_ID,GT.SUBCATL3_NAME as CATEGORY_NAME,GT.SUBCATL3_IMAGE as IMAGE_FILE,''C'' as TYPE,4 as Level,1 as rowOrder,
''SC'' AS LEVEL_FLAG,'''' as Product_count,'+@Family_ATTRIBUTES_VARIABLE+' ,TCS.SORT_ORDER,row_number() over
    (
        partition by GT.SUBCATL3_ID
        order by GT.SUBCATL3_ID
    )  as rn,GT.SUBCATL3_ID_IS_CLONE AS IS_CLONE,GT.ROOT_CATEGORY,GT.SUBCATL2_ID AS CATEGORY_SEARCH_ID 
from '+@GETTABLENAME+' GT
join TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID=GT.CATEGORY_ID
 where GT.SUBCATL2_ID='''+@CATGORY_ID+''' and GT.SUBCATL3_ID is not null and GT.SUBCATL3_NAME is not null
 GROUP BY GT.SUBCATL3_NAME,GT.SUBCATL3_ID,GT.CATEGORY_ID,GT.SUBCATL3_IMAGE,GT.Category_Image,TCS.SORT_ORDER,GT.SUBCATL3_ID_IS_CLONE,'+@Family_ATTRIBUTES_VARIABLE+',GT.ROOT_CATEGORY,GT.SUBCATL2_ID  

UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE AS IS_CLONE ,QCSP.ROOT_CATEGORY,QCSP.SUBCATL2_ID AS CATEGORY_SEARCH_ID   from TB_CATALOG_FAMILY TPF RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A''
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+'
WHERE  qcsp.FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''') AND  TCF.ROOT_CATEGORY = ''0'' and QCSP.ROOT_CATEGORY = ''0'' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.SUBCATL2_ID

UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE AS IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.SUBCATL2_ID AS CATEGORY_SEARCH_ID  from TB_CATALOG_FAMILY TPF 
 RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+'  AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
WHERE QCSP.SUBCATL2_ID='''+@CATGORY_ID+''' AND  TCF.ROOT_CATEGORY <> ''0'' and QCSP.ROOT_CATEGORY <> ''0''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.ROOT_CATEGORY,QCSP.SUBCATL2_ID
  
  ) AS T Where rn=1
 Order by SORT_ORDER
 ')

END
ELSE IF  @LEVEL = 4 and  @TYPE ='C'
BEGIN
EXEC('SELECT * FROM(
select distinct  GT.SUBCATL4_ID as CATEGORY_ID,GT.SUBCATL4_NAME as CATEGORY_NAME,GT.SUBCATL4_IMAGE as IMAGE_FILE,''C'' as TYPE,5 as Level,1 as rowOrder,''SC'' AS LEVEL_FLAG,
'''' as Product_count,TCS.SORT_ORDER,'+@Family_ATTRIBUTES_VARIABLE+',row_number() over
    (
        partition by GT.SUBCATL4_ID
        order by GT.SUBCATL4_ID
    )  as rn,GT.IS_CLONE,GT.SUBCATL3_ID AS CATEGORY_SEARCH_ID   from '+@GETTABLENAME+' GT
join TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID=GT.CATEGORY_ID
 where GT.SUBCATL3_ID='''+@CATGORY_ID+''' and GT.SUBCATL4_ID is not null and GT.SUBCATL4_NAME is not null
 GROUP BY GT.SUBCATL4_NAME,GT.SUBCATL4_ID,GT.CATEGORY_ID,GT.SUBCATL4_IMAGE,TCS.SORT_ORDER,GT.IS_CLONE,'+@Family_ATTRIBUTES_VARIABLE+',GT.SUBCATL3_ID 

UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
Count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL3_ID AS CATEGORY_SEARCH_ID   from TB_CATALOG_FAMILY TPF RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND QCSP.SUBCATL3_ID_IS_CLONE='+@IS_CLONE+' 
WHERE  qcsp.FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''') AND  TCF.ROOT_CATEGORY = ''0'' and QCSP.ROOT_CATEGORY = ''0'' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID,TSF.SUBFAMILY_ID,TF.FAMILY_ID,QCSP.FAMILY_NAME,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL3_ID

UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL3_ID AS CATEGORY_SEARCH_ID    from TB_CATALOG_FAMILY TPF 
 RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+'
 AND QCSP.SUBCATL3_ID_IS_CLONE='+@IS_CLONE+'  AND tcf.CATEGORY_ID='''+@CATGORY_ID+''' AND  TCF.ROOT_CATEGORY <> ''0'' and QCSP.ROOT_CATEGORY <> ''0''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL3_ID
  
  ) AS T Where rn=1
 Order by SORT_ORDER
 ')

END
ELSE IF  @LEVEL = 5 and  @TYPE ='C'
BEGIN
EXEC('SELECT * FROM(
select distinct  GT.SUBCATL5_ID as CATEGORY_ID,GT.SUBCATL5_NAME as CATEGORY_NAME,GT.SUBCATL5_IMAGE as IMAGE_FILE,''C'' as TYPE,6 as Level,1 as rowOrder,''SC'' AS LEVEL_FLAG,
'''' as Product_count,TCS.SORT_ORDER,'+@Family_ATTRIBUTES_VARIABLE+',row_number() over
    (
        partition by GT.SUBCATL5_ID
        order by GT.SUBCATL5_ID
    )  as rn,GT.IS_CLONE,GT.SUBCATL4_ID AS CATEGORY_SEARCH_ID  from '+@GETTABLENAME+' GT
join TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID=GT.CATEGORY_ID where GT.SUBCATL4_ID='''+@CATGORY_ID+''' and GT.SUBCATL5_ID is not null and GT.SUBCATL5_NAME is not null
 GROUP BY GT.SUBCATL5_NAME,GT.SUBCATL5_ID,GT.CATEGORY_ID,GT.SUBCATL5_IMAGE,TCS.SORT_ORDER,GT.IS_CLONE,'+@Family_ATTRIBUTES_VARIABLE+',GT.SUBCATL4_ID


UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder, 
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL4_ID AS CATEGORY_SEARCH_ID    from TB_CATALOG_FAMILY TPF RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID  

JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' 
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND QCSP.SUBCATL4_ID_IS_CLONE='+@IS_CLONE+' 
WHERE  qcsp.FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''') AND  TCF.ROOT_CATEGORY = ''0'' and QCSP.ROOT_CATEGORY = ''0'' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL4_ID


UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL4_ID AS CATEGORY_SEARCH_ID   from TB_CATALOG_FAMILY TPF 
 RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND QCSP.SUBCATL4_ID_IS_CLONE='+@IS_CLONE+' 
AND tcf.CATEGORY_ID='''+@CATGORY_ID+''' AND  TCF.ROOT_CATEGORY <> ''0'' and QCSP.ROOT_CATEGORY <> ''0''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL4_ID
  
  ) AS T Where rn=1
 Order by SORT_ORDER
 ')


END
ELSE IF  @LEVEL = 6 and  @TYPE ='C'
BEGIN
EXEC('SELECT * FROM(

select distinct  GT.SUBCATL6_ID as CATEGORY_ID,GT.SUBCATL6_NAME as CATEGORY_NAME,GT.SUBCATL6_IMAGE as IMAGE_FILE,''C'' as TYPE,7 as Level,1 as rowOrder,''SC'' AS LEVEL_FLAG,
'''' as Product_count,TCS.SORT_ORDER,'+@Family_ATTRIBUTES_VARIABLE+',row_number() over
    (
        partition by GT.SUBCATL6_ID
        order by GT.SUBCATL6_ID
    )  as rn,GT.IS_CLONE,GT.SUBCATL5_ID AS CATEGORY_SEARCH_ID  from '+@GETTABLENAME+' GT
join TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID=GT.CATEGORY_ID where GT.SUBCATL5_ID='''+@CATGORY_ID+''' and GT.SUBCATL6_ID is not null and GT.SUBCATL6_NAME is not null
 GROUP BY GT.SUBCATL6_NAME,GT.SUBCATL6_ID,GT.CATEGORY_ID,GT.SUBCATL6_IMAGE,TCS.SORT_ORDER,GT.IS_CLONE,'+@Family_ATTRIBUTES_VARIABLE+',GT.SUBCATL5_ID
UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL5_ID AS CATEGORY_SEARCH_ID   from TB_CATALOG_FAMILY TPF RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND QCSP.SUBCATL5_ID_IS_CLONE='+@IS_CLONE+'
WHERE  qcsp.FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''') AND  TCF.ROOT_CATEGORY = ''0'' and QCSP.ROOT_CATEGORY = ''0'' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL5_ID


UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL5_ID AS CATEGORY_SEARCH_ID  from TB_CATALOG_FAMILY TPF 
 RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND QCSP.SUBCATL5_ID_IS_CLONE='+@IS_CLONE+' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL5_ID
  
  ) AS T Where rn=1
 Order by SORT_ORDER
 ')


END
ELSE IF  @LEVEL = 7 and  @TYPE ='C'
BEGIN
EXEC('SELECT * FROM(
select distinct GT.SUBCATL7_ID as CATEGORY_ID,GT.SUBCATL7_NAME as CATEGORY_NAME,GT.SUBCATL7_IMAGE as IMAGE_FILE,''C'' as TYPE,8 as Level,1 as rowOrder,''SC'' AS LEVEL_FLAG,
'''' as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCS.SORT_ORDER,row_number() over
    (
        partition by GT.SUBCATL7_ID
        order by GT.SUBCATL7_ID
    )  as rn,GT.IS_CLONE,GT.SUBCATL6_ID AS CATEGORY_SEARCH_ID from '+@GETTABLENAME+' GT
join TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID=GT.CATEGORY_ID where GT.SUBCATL6_ID='''+@CATGORY_ID+''' and GT.SUBCATL7_ID is not null and GT.SUBCATL7_NAME is not null
 GROUP BY GT.SUBCATL7_NAME,GT.SUBCATL7_ID,GT.CATEGORY_ID,GT.SUBCATL7_IMAGE,TCS.SORT_ORDER,GT.IS_CLONE,'+@Family_ATTRIBUTES_VARIABLE+',GT.SUBCATL7_ID
UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL6_ID AS CATEGORY_SEARCH_ID  from TB_CATALOG_FAMILY TPF RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND QCSP.SUBCATL6_ID_IS_CLONE='+@IS_CLONE+'
WHERE  qcsp.FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''') AND  TCF.ROOT_CATEGORY = ''0'' and QCSP.ROOT_CATEGORY = ''0'' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,TSF.SUBFAMILY_ID,TF.FAMILY_ID,QCSP.FAMILY_NAME,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL6_ID

UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL6_ID AS CATEGORY_SEARCH_ID   from TB_CATALOG_FAMILY TPF 
 RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND QCSP.SUBCATL6_ID_IS_CLONE='+@IS_CLONE+' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL6_ID
  
  ) AS T Where rn=1
 Order by SORT_ORDER
 ')


END
ELSE IF  @LEVEL = 8 and  @TYPE ='C'
BEGIN
EXEC('SELECT * FROM(

select distinct CAST(Family_ID as varchar(10)) as CATEGORY_ID ,FAMILY_NAME as CATEGORY_NAME,Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
''SC'' AS LEVEL_FLAG,'''' as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCS.SORT_ORDER ,row_number() over
    (
        partition by Family_ID
        order by Family_ID
    )  as rn,GT.IS_CLONE, GT.SUBCATL7_ID
from '+@GETTABLENAME+' GT
join TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID=GT.CATEGORY_ID where FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''')
 GROUP BY GT.SUBCATL8_NAME,GT.SUBCATL8_ID,GT.CATEGORY_ID,GT.Category_Image,TCS.SORT_ORDER,GT.IS_CLONE,'+@Family_ATTRIBUTES_VARIABLE+'
UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder, 
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+' ,TCF.SORT_ORDER,row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn ,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL7_ID AS CATEGORY_SEARCH_ID from TB_CATALOG_FAMILY TPF RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND QCSP.SUBCATL7_ID_IS_CLONE='+@IS_CLONE+'
WHERE  qcsp.FAMILY_ID in (select Family_ID from TB_FAMILY where CATEGORY_ID='''+@CATGORY_ID+''') AND  TCF.ROOT_CATEGORY = ''0'' and QCSP.ROOT_CATEGORY = ''0'' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,TSF.SUBFAMILY_ID,TF.FAMILY_ID,QCSP.FAMILY_NAME,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL7_ID


UNION

select distinct CAST(QCSP.Family_ID as varchar(10)) as CATEGORY_ID ,QCSP.FAMILY_NAME as CATEGORY_NAME,qcsp.Family_Image as  IMAGE_FILE,''F'' as TYPE,1 as Level,2 as rowOrder,
CASE WHEN TSF.SUBFAMILY_ID=QCSP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 count(DISTINCT QCSP.PRODUCT_ID) as Product_count,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,
 row_number() over
    (
        partition by QCSP.Family_ID
        order by QCSP.Family_ID
    )  as rn,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL7_ID AS CATEGORY_SEARCH_ID  from TB_CATALOG_FAMILY TPF 
 RIGHT JOIN TB_FAMILY TF ON TF.FAMILY_ID=TPF.FAMILY_ID 
JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TF.FLAG_RECYCLE=''A'' AND TF.PUBLISH2WEB = 1 AND TC.FLAG_RECYCLE = ''A'' AND TC.PUBLISH2WEB = 1
join '+@GETTABLENAME+' QCSP ON QCSP.FAMILY_ID=TF.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID = QCSP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = QCSP.FAMILY_ID and tcf.CATALOG_ID='+@CATLOG_ID+' AND QCSP.SUBCATL7_ID_IS_CLONE='+@IS_CLONE+' AND tcf.CATEGORY_ID='''+@CATGORY_ID+'''
group by QCSP.Family_ID ,QCSP.FAMILY_NAME,TSF.SUBFAMILY_ID,TF.FAMILY_ID,qcsp.Family_Image,'+@Family_ATTRIBUTES_VARIABLE+',TCF.SORT_ORDER,QCSP.FAMILY_IS_CLONE,QCSP.SUBCATL7_ID
  
  ) AS T Where rn=1
 Order by SORT_ORDER
 ')


END

END


GO
/****** Object:  StoredProcedure [dbo].[STP_LS_GET_WEB_ITEMS_DETAILS]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_LS_GET_WEB_ITEMS_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_LS_GET_WEB_ITEMS_DETAILS] AS' 
END
GO





ALTER PROCEDURE [dbo].[STP_LS_GET_WEB_ITEMS_DETAILS]
(@CATALOG_ID int=123,
@CATEGORY_ID nvarchar(max) ='CAT12595',
@Family_ID int =26162,
@level nvarchar(max)=2,
@PRODUCT_ID int=26083)


AS
BEGIN

DECLARE @LevelOfCategory nvarchar(20)
DECLARE @GETPRODUCTATTRIBUTECOLUMN nvarchar(max)='',@GETTABLENAME nvarchar(max)=''

SET @GETPRODUCTATTRIBUTECOLUMN=(SELECT Search_Attribute FROM QSWS_CONFIGRATION_SETTINGS WHERE Catalog_Id=@CATALOG_ID)
SET @GETTABLENAME='QSWS_CATALOG_SNAPSHOT' +'_'+ CAST (@CATALOG_ID as nvarchar(10))

IF (@level=1)
BEGIN
SET @LevelOfCategory='CATEGORY_ID'
END
ELSE IF (@level=2)
BEGIN
SET @LevelOfCategory='SUBCATL1_ID'
END
ELSE IF (@level=3)
BEGIN
SET @LevelOfCategory='SUBCATL2_ID'
END
ELSE IF (@level=4)
BEGIN
SET @LevelOfCategory='SUBCATL3_ID'
END
ELSE IF (@level=5)
BEGIN
SET @LevelOfCategory='SUBCATL4_ID'
END
ELSE IF (@level=6)
BEGIN
SET @LevelOfCategory='SUBCATL5_ID'
END
ELSE IF (@level=7)
BEGIN
SET @LevelOfCategory='SUBCATL6_ID'
END
ELSE IF (@level=8)
BEGIN
SET @LevelOfCategory='SUBCATL7_ID'
END
IF @GETPRODUCTATTRIBUTECOLUMN IS NOT NULL OR @GETPRODUCTATTRIBUTECOLUMN <> ''
BEGIN

EXEC('SELECT PRODUCT_ID,ITEMS as ITEM_NAME,PRODUCT_IMAGE,'+@GETPRODUCTATTRIBUTECOLUMN+',''P'' as Type from '+@GETTABLENAME+' where PRODUCT_ID='+@PRODUCT_ID+' and FAMILY_ID='+@FAMILY_ID+' and '+@LevelOfCategory+'='''+ @CATEGORY_ID +''' '  )

END
ELSE
BEGIN

EXEC('SELECT PRODUCT_ID,ITEMS as ITEM_NAME,PRODUCT_IMAGE,''P'' as Type from '+@GETTABLENAME+'  where PRODUCT_ID='+@PRODUCT_ID+'  and  FAMILY_ID='+@FAMILY_ID+' and '+@LevelOfCategory+'='''+ @CATEGORY_ID +'''')

END

END



GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_CATALOG_SNAPSHOT]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_CATALOG_SNAPSHOT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_QSWS_CATALOG_SNAPSHOT] AS' 
END
GO



GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_CONFIGURATION_SETTINGS]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_CONFIGURATION_SETTINGS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_QSWS_CONFIGURATION_SETTINGS] AS' 
END
GO


ALTER  PROCEDURE [dbo].[STP_QSWS_CATALOG_SNAPSHOT]
@CUSTOMER_ID INT=8,
 @columns NVARCHAR(MAX) = '',
 @productcolumns NVARCHAR(MAX) = '',
 @sql NVARCHAR(MAX) = '',
 @Catalog_Id varchar(max) = '123',
 @DynamicSQL varchar(max) = '',
 @Counter INT = 0,
 @GetFamilyAttributeCount int = 0,
 @OPTION VARCHAR(50)='save',
 @RETURN_VALUE VARCHAR(MAX)='' OUTPUT
as 
BEGIN TRY
 BEGIN TRANSACTION

 /*****************************   Declaration of variables ****************************************/

DECLARE @familyAttribute varchar(max)
DECLARE @NextString NVARCHAR(40)
DECLARE @Pos INT
DECLARE @NextPos INT
DECLARE @Delimiter NVARCHAR(40)
declare @AttributeType varchar(20)=''
declare @Getattributetype varchar(20)=''
--declare @CATALOG_NAME varchar(100)=''
DECLARE @TableName VARCHAR(100) =''


IF(@OPTION='SAVE')
BEGIN

SET @Counter=1

--Select @CATALOG_NAME =  CATALOG_NAME FROM TB_CATALOG WHERE CATALOG_ID=@Catalog_Id

--print @CATALOG_NAME


 /*****************************  Drop temporary and permanent tables  ****************************************/


	IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##Family_Attributes')  EXEC('DROP TABLE [##Family_Attributes]')  
	 
	   	IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##CatHierarchy')  EXEC('DROP TABLE [##CatHierarchy]')  


	 
	  -- IF EXISTS (SELECT  TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'QSWS_CATALOG_SNAPSHOT') DROP TABLE QSWS_CATALOG_SNAPSHOT

	   set @TableName  = 'QSWS_CATALOG_SNAPSHOT_'+@Catalog_Id

			IF OBJECT_ID(@TableName) IS NOT NULL
				EXEC ('DROP Table ' + @TableName)

	   --IF EXISTS (SELECT  TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MSWEB_CATALOG_SNAPSHOT') DROP TABLE MSWEB_CATALOG_SNAPSHOT
	
 /*****************************  Category, family , sub family hiearachies  ****************************************/	

 
SELECT DISTINCT @CUSTOMER_ID AS CUSTOMER_ID,TC.CATALOG_ID,TC.CATALOG_NAME,RC.CATEGORY_NAME,RC.CATEGORY_ID ,SL1.CATEGORY_ID AS SUBCATL1_ID,SL1.CATEGORY_NAME AS SUBCATL1_NAME
,SL2.CATEGORY_ID AS SUBCATL2_ID ,SL2.CATEGORY_NAME AS SUBCATL2_NAME,SL3.CATEGORY_ID AS SUBCATL3_ID,SL3.CATEGORY_NAME AS SUBCATL3_NAME,SL4.CATEGORY_ID AS SUBCATL4_ID,
SL4.CATEGORY_NAME AS SUBCATL4_NAME, SL5.CATEGORY_ID AS SUBCATL5_ID,SL5.CATEGORY_NAME AS SUBCATL5_NAME,SL6.CATEGORY_ID AS SUBCATL6_ID,
SL6.CATEGORY_NAME AS SUBCATL6_NAME,SL7.CATEGORY_ID AS SUBCATL7_ID,SL7.CATEGORY_NAME AS SUBCATL7_NAME,NULL AS ROOT_PARENT_FAMILY_ID
, NULL AS FAMILY_ID,NULL AS FAMILY_NAME,NULL AS PRODUCT_ID,RC.IS_CLONE,TCF.ROOT_CATEGORY  into ##CatHierarchy
FROM TB_CATEGORY RC 
JOIN TB_CATALOG_SECTIONS TCS_RC ON TCS_RC.CATEGORY_ID=RC.CATEGORY_ID AND TCS_RC.FLAG_RECYCLE='A' AND RC.PUBLISH2WEB = 1
LEFT JOIN TB_CATEGORY SL1 ON SL1.PARENT_CATEGORY=RC.CATEGORY_ID  AND RC.FLAG_RECYCLE='A' AND   SL1.FLAG_RECYCLE='A' AND  SL1.PUBLISH2WEB = 1

 JOIN TB_CATALOG_SECTIONS TCS_SL1 ON TCS_SL1.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL1.FLAG_RECYCLE='A' 
 LEFT JOIN TB_CATEGORY SL2 ON SL2.PARENT_CATEGORY=SL1.CATEGORY_ID  AND  SL2.FLAG_RECYCLE='A' AND SL2.PUBLISH2WEB = 1

   JOIN TB_CATALOG_SECTIONS TCS_SL2 ON TCS_SL2.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL2.FLAG_RECYCLE='A' 
LEFT JOIN TB_CATEGORY SL3 ON SL3.PARENT_CATEGORY=SL2.CATEGORY_ID AND SL3.FLAG_RECYCLE='A' AND SL3.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL3 ON TCS_SL3.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL3.FLAG_RECYCLE='A' 
  LEFT JOIN TB_CATEGORY SL4 ON SL4.PARENT_CATEGORY=SL3.CATEGORY_ID AND SL4.FLAG_RECYCLE='A' AND SL4.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL4 ON TCS_SL4.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL4.FLAG_RECYCLE='A' 
  LEFT JOIN TB_CATEGORY SL5 ON SL5.PARENT_CATEGORY=SL4.CATEGORY_ID AND SL5.FLAG_RECYCLE='A' AND SL5.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL5 ON TCS_SL5.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL5.FLAG_RECYCLE='A' 
    LEFT JOIN TB_CATEGORY SL6 ON SL6.PARENT_CATEGORY=SL5.CATEGORY_ID AND SL6.FLAG_RECYCLE='A' AND SL6.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL6 ON TCS_SL6.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL6.FLAG_RECYCLE='A' 
      LEFT JOIN TB_CATEGORY SL7 ON SL7.PARENT_CATEGORY=SL6.CATEGORY_ID AND SL7.FLAG_RECYCLE='A' AND SL7.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL7 ON TCS_SL7.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL7.FLAG_RECYCLE='A' 

 JOIN TB_CATALOG_FAMILY TCF on TCF.CATEGORY_ID=RC.CATEGORY_ID --or 
 --    TCF.CATEGORY_ID =ISNULL(SL7.CATEGORY_ID,'')  
 -- OR TCF.CATEGORY_ID =ISNULL(SL6.CATEGORY_ID,'')  
 --OR TCF.CATEGORY_ID=ISNULL(SL5.CATEGORY_ID,'')   
 --OR TCF.CATEGORY_ID=ISNULL(SL4.CATEGORY_ID,'')
 --OR TCF.CATEGORY_ID =ISNULL(SL3.CATEGORY_ID,'')  
 --OR TCF.CATEGORY_ID=ISNULL(SL2.CATEGORY_ID,'')   
 --OR TCF.CATEGORY_ID=ISNULL(SL1.CATEGORY_ID,''
  
 --) AND TCF.FLAG_RECYCLE='A'

  JOIN TB_CATALOG TC ON TC.CATALOG_ID=TCS_RC.CATALOG_ID AND TC.FLAG_RECYCLE='A'
 
WHERE
 RC.PARENT_CATEGORY='0'   
AND TCS_RC.CATALOG_ID <> 1 
AND TCS_RC.CATALOG_ID=@Catalog_Id
AND TCS_RC.CATALOG_ID = @Catalog_Id
AND TCS_SL1.CATALOG_ID = @Catalog_Id
AND TCS_SL2.CATALOG_ID = @Catalog_Id
AND TCS_SL3.CATALOG_ID = @Catalog_Id
AND TCS_SL4.CATALOG_ID = @Catalog_Id
AND TCS_SL5.CATALOG_ID = @Catalog_Id
AND TCS_SL6.CATALOG_ID = @Catalog_Id
AND TCS_SL7.CATALOG_ID = @Catalog_Id
AND RC.CATEGORY_NAME <> 'Default Category' 
AND RC.FLAG_RECYCLE = 'A' AND RC.PUBLISH2WEB = 1

 group by RC.CATEGORY_NAME,RC.CATEGORY_ID,SL1.CATEGORY_NAME,SL1.CATEGORY_ID 
,SL2.CATEGORY_NAME ,SL2.CATEGORY_ID  ,SL3.CATEGORY_NAME,SL3.CATEGORY_ID 
,SL4.CATEGORY_NAME,SL4.CATEGORY_ID 
,SL5.CATEGORY_NAME,SL5.CATEGORY_ID 
,SL6.CATEGORY_NAME,SL6.CATEGORY_ID 
,SL7.CATEGORY_NAME,SL7.CATEGORY_ID 
,TC.CATALOG_ID,tc.CATALOG_NAME,RC.IS_CLONE,TCF.ROOT_CATEGORY

UNION ALL

SELECT @CUSTOMER_ID AS CUSTOMER_ID,TC.CATALOG_ID,TC.CATALOG_NAME,RC.CATEGORY_NAME,RC.CATEGORY_ID,NULL AS SUBCATL1_ID,NULL AS SUBCATL1_NAME
,NULL AS SUBCATL2_ID ,NULL AS SUBCATL2_NAME,NULL AS SUBCATL3_ID,NULL AS SUBCATL3_NAME,NULL AS SUBCATL4_ID,
NULL AS SUBCATL4_NAME, NULL AS SUBCATL5_ID,NULL AS SUBCATL5_NAME,NULL AS SUBCATL6_ID,
NULL AS SUBCATL6_NAME,NULL AS SUBCATL7_ID,NULL AS SUBCATL7_NAME
,CASE WHEN TF.PARENT_FAMILY_ID=0 THEN TF.PARENT_FAMILY_ID 
ELSE TF.PARENT_FAMILY_ID END AS ROOT_PARENT_FAMILY_ID
, TCF.FAMILY_ID, TF.FAMILY_NAME,TPF.PRODUCT_ID,RC.IS_CLONE, TCF.ROOT_CATEGORY
--COUNT(TPF.PRODUCT_ID) as Prod_cnt,TCF.CATALOG_ID
FROM TB_CATEGORY RC 
JOIN TB_CATALOG_SECTIONS TCS_RC ON TCS_RC.CATEGORY_ID=RC.CATEGORY_ID AND TCS_RC.FLAG_RECYCLE='A' AND RC.FLAG_RECYCLE ='A' AND RC.PUBLISH2WEB = 1
--LEFT JOIN TB_CATEGORY SL1 ON SL1.PARENT_CATEGORY=RC.CATEGORY_ID  AND RC.FLAG_RECYCLE='A' AND   SL1.FLAG_RECYCLE='A'
-- JOIN TB_CATALOG_SECTIONS TCS_SL1 ON TCS_SL1.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL1.FLAG_RECYCLE='A' 

  JOIN TB_CATALOG_FAMILY TCF on TCF.CATEGORY_ID=RC.CATEGORY_ID 
  
  AND TCF.FLAG_RECYCLE='A'
  JOIN TB_FAMILY TF ON TF.FAMILY_ID=TCF.FAMILY_ID AND TF.FLAG_RECYCLE='A' AND TF.FLAG_RECYCLE = 'A' AND TF.PUBLISH2WEB = 1 
 LEFT JOIN TB_SUBFAMILY TSF ON TF.FAMILY_ID=TSF.FAMILY_ID 
 LEFT JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID= TF.FAMILY_ID AND TPF.FLAG_RECYCLE='A' AND TPF.PUBLISH2WEB = 1
  JOIN TB_CATALOG TC ON TC.CATALOG_ID=TCF.CATALOG_ID AND TC.FLAG_RECYCLE='A'
 
WHERE
 RC.PARENT_CATEGORY='0'   
AND TCF.CATALOG_ID <> 1 
AND TCF.CATALOG_ID=@Catalog_Id
AND TCS_RC.CATALOG_ID = @Catalog_Id
AND RC.FLAG_RECYCLE = 'A' AND RC.PUBLISH2WEB = 1
 group by RC.CATEGORY_NAME,RC.CATEGORY_ID
, TCF.FAMILY_ID,TPF.FAMILY_ID,TPF.PRODUCT_ID ,TF.FAMILY_ID,TF.PARENT_FAMILY_ID,TSF.FAMILY_ID, TF.FAMILY_NAME,TCF.CATALOG_ID,TC.CATALOG_ID,tc.CATALOG_NAME,RC.IS_CLONE, TCF.ROOT_CATEGORY



UNION

SELECT @CUSTOMER_ID AS CUSTOMER_ID,TC.CATALOG_ID,TC.CATALOG_NAME,RC.CATEGORY_NAME,RC.CATEGORY_ID ,SL1.CATEGORY_ID AS SUBCATL1_ID,SL1.CATEGORY_NAME AS SUBCATL1_NAME
,SL2.CATEGORY_ID AS SUBCATL2_ID ,SL2.CATEGORY_NAME AS SUBCATL2_NAME,SL3.CATEGORY_ID AS SUBCATL3_ID,SL3.CATEGORY_NAME AS SUBCATL3_NAME,SL4.CATEGORY_ID AS SUBCATL4_ID,
SL4.CATEGORY_NAME AS SUBCATL4_NAME, SL5.CATEGORY_ID AS SUBCATL5_ID,SL5.CATEGORY_NAME AS SUBCATL5_NAME,SL6.CATEGORY_ID AS SUBCATL6_ID,
SL6.CATEGORY_NAME AS SUBCATL6_NAME,SL7.CATEGORY_ID AS SUBCATL7_ID,SL7.CATEGORY_NAME AS SUBCATL7_NAME,CASE WHEN TF.PARENT_FAMILY_ID=0 THEN TF.PARENT_FAMILY_ID
ELSE TF.PARENT_FAMILY_ID END AS ROOT_PARENT_FAMILY_ID
, TCF.FAMILY_ID, TF.FAMILY_NAME,TPF.PRODUCT_ID,RC.IS_CLONE, TCF.ROOT_CATEGORY --into ##CatHierarchy
--COUNT(TPF.PRODUCT_ID) as Prod_cnt,TCF.CATALOG_ID
FROM TB_CATEGORY RC 
JOIN TB_CATALOG_SECTIONS TCS_RC ON TCS_RC.CATEGORY_ID=RC.CATEGORY_ID AND TCS_RC.FLAG_RECYCLE='A' AND RC.PUBLISH2WEB = 1
LEFT JOIN TB_CATEGORY SL1 ON SL1.PARENT_CATEGORY=RC.CATEGORY_ID  AND RC.FLAG_RECYCLE='A' AND   SL1.FLAG_RECYCLE='A' AND  SL1.PUBLISH2WEB = 1

 JOIN TB_CATALOG_SECTIONS TCS_SL1 ON TCS_SL1.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL1.FLAG_RECYCLE='A' 
 LEFT JOIN TB_CATEGORY SL2 ON SL2.PARENT_CATEGORY=SL1.CATEGORY_ID  AND  SL2.FLAG_RECYCLE='A' AND SL2.PUBLISH2WEB = 1

   JOIN TB_CATALOG_SECTIONS TCS_SL2 ON TCS_SL2.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL2.FLAG_RECYCLE='A' 
LEFT JOIN TB_CATEGORY SL3 ON SL3.PARENT_CATEGORY=SL2.CATEGORY_ID AND SL3.FLAG_RECYCLE='A' AND SL3.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL3 ON TCS_SL3.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL3.FLAG_RECYCLE='A' 
  LEFT JOIN TB_CATEGORY SL4 ON SL4.PARENT_CATEGORY=SL3.CATEGORY_ID AND SL4.FLAG_RECYCLE='A' AND SL4.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL4 ON TCS_SL4.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL4.FLAG_RECYCLE='A' 
  LEFT JOIN TB_CATEGORY SL5 ON SL5.PARENT_CATEGORY=SL4.CATEGORY_ID AND SL5.FLAG_RECYCLE='A' AND SL5.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL5 ON TCS_SL5.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL5.FLAG_RECYCLE='A' 
    LEFT JOIN TB_CATEGORY SL6 ON SL6.PARENT_CATEGORY=SL5.CATEGORY_ID AND SL6.FLAG_RECYCLE='A' AND SL6.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL6 ON TCS_SL6.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL6.FLAG_RECYCLE='A' 
      LEFT JOIN TB_CATEGORY SL7 ON SL7.PARENT_CATEGORY=SL6.CATEGORY_ID AND SL7.FLAG_RECYCLE='A' AND SL7.PUBLISH2WEB = 1

  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL7 ON TCS_SL7.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL7.FLAG_RECYCLE='A' 

  JOIN TB_CATALOG_FAMILY TCF on TCF.CATEGORY_ID=RC.CATEGORY_ID or 
     TCF.CATEGORY_ID =ISNULL(SL7.CATEGORY_ID,'')  
  OR TCF.CATEGORY_ID =ISNULL(SL6.CATEGORY_ID,'')  
 OR TCF.CATEGORY_ID=ISNULL(SL5.CATEGORY_ID,'')   
 OR TCF.CATEGORY_ID=ISNULL(SL4.CATEGORY_ID,'')
 OR TCF.CATEGORY_ID =ISNULL(SL3.CATEGORY_ID,'')  
 OR TCF.CATEGORY_ID=ISNULL(SL2.CATEGORY_ID,'')   
 OR TCF.CATEGORY_ID=ISNULL(SL1.CATEGORY_ID,''
  
 ) AND TCF.FLAG_RECYCLE='A' 
  JOIN TB_FAMILY TF ON TF.FAMILY_ID=TCF.FAMILY_ID AND TF.FLAG_RECYCLE='A'  AND TF.PUBLISH2WEB = 1
 LEFT JOIN TB_SUBFAMILY TSF ON TF.FAMILY_ID=TSF.FAMILY_ID 
 LEFT JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID= TF.FAMILY_ID AND TPF.FLAG_RECYCLE='A' AND TPF.PUBLISH2WEB = 1
  JOIN TB_CATALOG TC ON TC.CATALOG_ID=TCF.CATALOG_ID AND TC.FLAG_RECYCLE='A'
 
WHERE
 RC.PARENT_CATEGORY='0'   
AND TCF.CATALOG_ID <> 1 
AND TCF.CATALOG_ID=@Catalog_Id
AND TCS_RC.CATALOG_ID = @Catalog_Id
AND TCS_SL1.CATALOG_ID = @Catalog_Id
AND TCS_SL2.CATALOG_ID = @Catalog_Id
AND TCS_SL3.CATALOG_ID = @Catalog_Id
AND TCS_SL4.CATALOG_ID = @Catalog_Id
AND TCS_SL5.CATALOG_ID = @Catalog_Id
AND TCS_SL6.CATALOG_ID = @Catalog_Id
AND TCS_SL7.CATALOG_ID = @Catalog_Id
AND RC.CATEGORY_NAME <> 'Default Category' 
AND RC.FLAG_RECYCLE = 'A' AND RC.PUBLISH2WEB = 1

 group by RC.CATEGORY_NAME,RC.CATEGORY_ID,SL1.CATEGORY_NAME,SL1.CATEGORY_ID 
,SL2.CATEGORY_NAME ,SL2.CATEGORY_ID  ,SL3.CATEGORY_NAME,SL3.CATEGORY_ID 
,SL4.CATEGORY_NAME,SL4.CATEGORY_ID 
,SL5.CATEGORY_NAME,SL5.CATEGORY_ID 
,SL6.CATEGORY_NAME,SL6.CATEGORY_ID 
,SL7.CATEGORY_NAME,SL7.CATEGORY_ID 
, TCF.FAMILY_ID,TPF.FAMILY_ID,TPF.PRODUCT_ID ,TF.FAMILY_ID,TF.PARENT_FAMILY_ID,TSF.FAMILY_ID, TF.FAMILY_NAME,TCF.CATALOG_ID,TC.CATALOG_ID,tc.CATALOG_NAME,RC.IS_CLONE, TCF.ROOT_CATEGORY

--SELECT @CUSTOMER_ID AS CUSTOMER_ID,TC.CATALOG_ID,TC.CATALOG_NAME,RC.CATEGORY_NAME,RC.CATEGORY_ID ,SL1.CATEGORY_ID AS SUBCATL1_ID,SL1.CATEGORY_NAME AS SUBCATL1_NAME
--,SL2.CATEGORY_ID AS SUBCATL2_ID ,SL2.CATEGORY_NAME AS SUBCATL2_NAME,SL3.CATEGORY_ID AS SUBCATL3_ID,SL3.CATEGORY_NAME AS SUBCATL3_NAME,SL4.CATEGORY_ID AS SUBCATL4_ID,
--SL4.CATEGORY_NAME AS SUBCATL4_NAME, SL5.CATEGORY_ID AS SUBCATL5_ID,SL5.CATEGORY_NAME AS SUBCATL5_NAME,SL6.CATEGORY_ID AS SUBCATL6_ID,
--SL6.CATEGORY_NAME AS SUBCATL6_NAME,SL7.CATEGORY_ID AS SUBCATL7_ID,SL7.CATEGORY_NAME AS SUBCATL7_NAME,CASE WHEN TF.PARENT_FAMILY_ID=0 THEN TF.PARENT_FAMILY_ID 
--ELSE TF.PARENT_FAMILY_ID END AS ROOT_PARENT_FAMILY_ID
--, TCF.FAMILY_ID, TF.FAMILY_NAME,TPF.PRODUCT_ID  into ##CatHierarchy
----COUNT(TPF.PRODUCT_ID) as Prod_cnt,TCF.CATALOG_ID
--FROM TB_CATEGORY RC 
--JOIN TB_CATALOG_SECTIONS TCS_RC ON TCS_RC.CATEGORY_ID=RC.CATEGORY_ID AND TCS_RC.FLAG_RECYCLE='A' 
--LEFT JOIN TB_CATEGORY SL1 ON SL1.PARENT_CATEGORY=RC.CATEGORY_ID  AND RC.FLAG_RECYCLE='A' AND   SL1.FLAG_RECYCLE='A'

-- JOIN TB_CATALOG_SECTIONS TCS_SL1 ON TCS_SL1.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL1.FLAG_RECYCLE='A' 
-- LEFT JOIN TB_CATEGORY SL2 ON SL2.PARENT_CATEGORY=SL1.CATEGORY_ID  AND  SL2.FLAG_RECYCLE='A'

--   JOIN TB_CATALOG_SECTIONS TCS_SL2 ON TCS_SL2.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL2.FLAG_RECYCLE='A' 
--LEFT JOIN TB_CATEGORY SL3 ON SL3.PARENT_CATEGORY=SL2.CATEGORY_ID AND SL3.FLAG_RECYCLE='A'

--  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL3 ON TCS_SL3.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL3.FLAG_RECYCLE='A' 
--  LEFT JOIN TB_CATEGORY SL4 ON SL4.PARENT_CATEGORY=SL3.CATEGORY_ID AND SL4.FLAG_RECYCLE='A'

--  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL4 ON TCS_SL4.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL4.FLAG_RECYCLE='A' 
--  LEFT JOIN TB_CATEGORY SL5 ON SL5.PARENT_CATEGORY=SL4.CATEGORY_ID AND SL5.FLAG_RECYCLE='A'

--  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL5 ON TCS_SL5.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL5.FLAG_RECYCLE='A' 
--    LEFT JOIN TB_CATEGORY SL6 ON SL6.PARENT_CATEGORY=SL5.CATEGORY_ID AND SL6.FLAG_RECYCLE='A'

--  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL6 ON TCS_SL6.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL6.FLAG_RECYCLE='A' 
--      LEFT JOIN TB_CATEGORY SL7 ON SL7.PARENT_CATEGORY=SL6.CATEGORY_ID AND SL7.FLAG_RECYCLE='A'

--  LEFT JOIN TB_CATALOG_SECTIONS TCS_SL7 ON TCS_SL7.CATEGORY_ID=RC.CATEGORY_ID AND TCS_SL7.FLAG_RECYCLE='A' 

--  JOIN TB_CATALOG_FAMILY TCF on TCF.CATEGORY_ID=RC.CATEGORY_ID or 
--     TCF.CATEGORY_ID =ISNULL(SL7.CATEGORY_ID,'')  
--  OR TCF.CATEGORY_ID =ISNULL(SL6.CATEGORY_ID,'')  
-- OR TCF.CATEGORY_ID=ISNULL(SL5.CATEGORY_ID,'')   
-- OR TCF.CATEGORY_ID=ISNULL(SL4.CATEGORY_ID,'')
-- OR TCF.CATEGORY_ID =ISNULL(SL3.CATEGORY_ID,'')  
-- OR TCF.CATEGORY_ID=ISNULL(SL2.CATEGORY_ID,'')   
-- OR TCF.CATEGORY_ID=ISNULL(SL1.CATEGORY_ID,''
  
-- ) AND TCF.FLAG_RECYCLE='A'
-- JOIN TB_FAMILY TF ON TF.FAMILY_ID=TCF.FAMILY_ID AND TF.FLAG_RECYCLE='A'  
-- LEFT JOIN TB_SUBFAMILY TSF ON TF.FAMILY_ID=TSF.FAMILY_ID 
-- LEFT JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID= TF.FAMILY_ID AND TPF.FLAG_RECYCLE='A' 
--  JOIN TB_CATALOG TC ON TC.CATALOG_ID=TCF.CATALOG_ID AND TC.FLAG_RECYCLE='A'
 
--WHERE
-- RC.PARENT_CATEGORY='0'   
--AND TCF.CATALOG_ID <> 1 
--AND TCF.CATALOG_ID=@Catalog_Id

-- group by RC.CATEGORY_NAME,RC.CATEGORY_ID,SL1.CATEGORY_NAME,SL1.CATEGORY_ID 
--,SL2.CATEGORY_NAME ,SL2.CATEGORY_ID  ,SL3.CATEGORY_NAME,SL3.CATEGORY_ID 
--,SL4.CATEGORY_NAME,SL4.CATEGORY_ID 
--,SL5.CATEGORY_NAME,SL5.CATEGORY_ID 
--,SL6.CATEGORY_NAME,SL6.CATEGORY_ID 
--,SL7.CATEGORY_NAME,SL7.CATEGORY_ID 
--, TCF.FAMILY_ID,TPF.FAMILY_ID,TPF.PRODUCT_ID ,TF.FAMILY_ID,TF.PARENT_FAMILY_ID,TSF.FAMILY_ID, TF.FAMILY_NAME,TCF.CATALOG_ID,TC.CATALOG_ID,tc.CATALOG_NAME


/** Clone start ***/

 ALTER TABLE ##CatHierarchy
ADD FAMILY_IS_CLONE INT NOT NULL DEFAULT(0)


		  EXEC('UPDATE CAT SET CAT.IS_CLONE = TC.IS_CLONE FROM TB_CATEGORY TC JOIN ##CatHierarchy CAT ON TC.CATEGORY_ID = CAT.CATEGORY_ID AND TC.FLAG_RECYCLE = ''A'' AND TC.IS_CLONE = 1 AND TC.PUBLISH2WEB = 1')

		  EXEC('UPDATE CAT SET CAT.FAMILY_IS_CLONE =1 from ##CatHierarchy CAT JOIN TB_CATALOG_FAMILY TCF ON TCF.CATEGORY_ID = CAT.CATEGORY_ID JOIN TB_CATEGORY TC ON TCF.CATEGORY_ID = TC.CATEGORY_ID 
JOIN TB_FAMILY TF ON TF.FAMILY_ID  = TCF.FAMILY_ID AND CAT.FAMILY_ID=TF.FAMILY_ID  AND TCF.CATALOG_ID=CAT.CATALOG_ID WHERE TCF.ROOT_CATEGORY <> ''0''  AND TF.FLAG_RECYCLE=''A'' AND TCF.FLAG_RECYCLE=''A''
AND TC.CATEGORY_ID = CAT.CATEGORY_ID AND TCF.CATALOG_ID = '+@Catalog_Id+' ')



DECLARE @Loopcount INT;
		DECLARE @SubColumnId nvarchar(100);
		DECLARE @SubCategoryName nvarchar(100);
		DECLARE @sqlQueryDpClmn nvarchar(max);
	 	set @SubColumnId ='SUBCATL1_ID'
        SET @Loopcount = 1;  
		WHILE @Loopcount <= 7
		BEGIN
		  if EXISTS(SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = @SubColumnId AND TABLE_NAME = '##CatHierarchy')	  
		  BEGIN    
		    DECLARE @SUBCAT VARCHAR(500) = 'SUBCATL' + CONVERT(varchar(10), @Loopcount)+ '_ID'
		    set @sqlQueryDpClmn = 'ALTER TABLE ##CatHierarchy ADD '+@SUBCAT+'_IS_CLONE INT NOT NULL DEFAULT(0)'
		  exec sp_executesql @sqlQueryDpClmn

		  print @sqlQueryDpClmn

		  DECLARE @SUBCAT_Image VARCHAR(500) = 'SUBCATL' + CONVERT(varchar(10), @Loopcount)+ '_IMAGE'
		    set @sqlQueryDpClmn = 'ALTER TABLE ##CatHierarchy ADD '+@SUBCAT_Image+' VARCHAR(500) NULL DEFAULT(NULL)'
		  exec sp_executesql @sqlQueryDpClmn

		  print @sqlQueryDpClmn

		   DECLARE @caTEGORY_ID VARCHAR(500) =''

		   IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##CAT_FAMILY_CLONE')  EXEC('DROP TABLE [##CAT_FAMILY_CLONE]')  
		 
		  EXEC('UPDATE CAT SET CAT.'+@SUBCAT+'_IS_CLONE = TC.IS_CLONE FROM TB_CATEGORY TC JOIN ##CatHierarchy CAT ON TC.CATEGORY_ID = CAT.'+@SUBCAT+' AND TC.FLAG_RECYCLE = ''A'' AND TC.IS_CLONE = 1 AND TC.PUBLISH2WEB = 1')

		  EXEC('select DISTINCT TCF.FAMILY_ID,TCF.CATEGORY_ID INTO ##CAT_FAMILY_CLONE from ##CatHierarchy CAT JOIN TB_CATALOG_FAMILY TCF ON TCF.CATEGORY_ID = CAT.'+@SUBCAT+' JOIN TB_CATEGORY TC ON TCF.CATEGORY_ID = TC.CATEGORY_ID 
			JOIN TB_FAMILY TF ON TF.FAMILY_ID  = TCF.FAMILY_ID AND CAT.FAMILY_ID=TF.FAMILY_ID AND TCF.CATALOG_ID=CAT.CATALOG_ID WHERE TCF.ROOT_CATEGORY <> ''0''  AND TF.FLAG_RECYCLE=''A'' AND TCF.FLAG_RECYCLE=''A''
			AND  CAT.'+@SUBCAT+' = TC.CATEGORY_ID AND TCF.CATALOG_ID = '+@Catalog_Id+' AND CAT.ROOT_CATEGORY <> ''0'' ')

			EXEC('UPDATE CAT SET CAT.FAMILY_IS_CLONE =1 from ##CatHierarchy CAT JOIN  ##CAT_FAMILY_CLONE CFC ON CFC.FAMILY_ID = CAT.FAMILY_ID
			AND CFC.CATEGORY_ID = CAT.'+@SUBCAT+' JOIN TB_CATALOG_FAMILY TCF ON CAT.ROOT_CATEGORY <> ''0'' ')

			EXEC('
				Update CAT set CAT.'+@SUBCAT_Image+' = TCS.STRING_VALUE from TB_ATTRIBUTE  TA
				 JOIN TB_CATEGORY_SPECS TCS ON TCS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID 
				 JOIN ##CatHierarchy Cat ON CAT.'+@SUBCAT+' = TCS.CATEGORY_ID AND CAT.CATALOG_ID = TCS.CATALOG_ID
				 JOIN TB_CATEGORY TC ON TC.CATEGORY_ID=CAT.'+@SUBCAT+' AND TC.FLAG_RECYCLE=''A'' AND TC.PUBLISH2WEB = 1
				JOIN QSWS_CONFIGRATION_SETTINGS QCS ON QCS.CATALOG_ID =CAT.CATALOG_ID
				  AND TA.CAPTION  = QCS.Category_Image  WHERE  TA.FLAG_RECYCLE=''A'' AND TA.PUBLISH2WEB = 1
		
		')

		 SET @Loopcount = @Loopcount + 1  
		   END
		END;


	
 /*****************************  Get family and product attributes from  QSWS_CONFIGRATION_SETTINGS ****************************************/	

SET @columns = N'';
SELECT @columns+=N''+Name 
FROM
(
    SELECT Search_Family_Attributes AS [Name],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id
  
) AS x;


IF @columns<>''
begin
print '11'
SELECT @columns+=N''+ ',' + Products
FROM
(
    SELECT Search_Attribute as [Products],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id
  
) AS x;
END
ELSE
BEGIN
print '111'
SELECT @columns+=N''+ Products
FROM
(
    SELECT Search_Attribute as [Products],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id
  
) AS x;
END


--print @columns

 /*****************************  Pivot family and product attributes columns from  QSWS_CONFIGRATION_SETTINGS ****************************************/	

SET @sql = N'
SELECT '+@Catalog_Id+' AS CATALOG_ID,'+@columns+ ' into ##Family_Attributes FROM (
SELECT  Search_Family_Attributes as [Name],Search_Attribute AS [Products] ,Catalog_Id 
    FROM [dbo].[QSWS_CONFIGRATION_SETTINGS]) AS j 
	PIVOT (MAX(Catalog_id) FOR [Name] in 
	   ('+@columns+')) as pivot12

--PIVOT (MAX(Catalog_id) FOR [Products] IN ('+@productcolumns+')) 
-- AS pivot123
 
  ';
	 --print @sql

	
	  --SELECT @sql
	   --EXEC  @sql
EXEC sp_executesql @sql


 /*****************************  Get each attribute names separated from comma columns from  QSWS_CONFIGRATION_SETTINGS ****************************************/	

SET @Delimiter = ','


 DECLARE @String varchar(MAX) = @columns 
 SET @String = @String + @Delimiter
SET @Pos = charindex(@Delimiter,@String)


 SELECT @GetFamilyAttributeCount  = LEN(@String) - LEN(REPLACE(@String, ',', '')) + 1

while (@Counter <= @GetFamilyAttributeCount AND @pos <> 0)
BEGIN
declare @getItem nvarchar(max)
  --select @getItem= ITEM FROM SPLITSTRING(@String,',')
  SET @getItem = substring(@String,1,@Pos - 1)
--print @getItem


/*****************************  If item# add Product Id columns and update the product id values ****************************************/	
DECLARE @ATTRIBUTE_NAME VARCHAR(MAX)=''
select @ATTRIBUTE_NAME =  CAPTION from TB_ATTRIBUTE  where CAPTION=Replace(Replace(@getItem, '[',''), ']', '')  AND ATTRIBUTE_TYPE=1 AND ATTRIBUTE_ID=1
AND FLAG_RECYCLE = 'A' AND PUBLISH2WEB = 1

--IF(@getItem='['+@ATTRIBUTE_NAME+']')
--BEGIN

--ALTER TABLE ##CatHierarchy ADD PRODUCT_ID INT

--EXEC('

--UPDATE CH SET  CH.PRODUCT_ID =TPF.PRODUCT_ID from ##CatHierarchy CH 
--LEFT JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID= CH.FAMILY_ID AND TPF.FLAG_RECYCLE=''A''
		
--')


--END

/*****************************  Get attribute datatype and attribute type from attribute table ****************************************/	

select @AttributeType =  ATTRIBUTE_DATATYPE from TB_ATTRIBUTE  where CAPTION=Replace(Replace(@getItem, '[',''), ']', '') AND FLAG_RECYCLE = 'A' AND PUBLISH2WEB = 1

select @Getattributetype =  ATTRIBUTE_TYPE from TB_ATTRIBUTE  where CAPTION=Replace(Replace(@getItem, '[',''), ']', '') AND FLAG_RECYCLE = 'A' AND PUBLISH2WEB = 1
PRINT @AttributeType
/*****************************  Set attribute type from attribute table for each columns ****************************************/	
IF (@Getattributetype=4)
begin

set @AttributeType='Number(18,2)'
end
IF(@AttributeType='TEXT' OR @AttributeType='TEXT(6000)')

BEGIN

SET @DynamicSQL = 'ALTER TABLE ##CatHierarchy ADD '+ CAST(@getItem AS NVARCHAR(max)) +' NVARCHAR(MAX) NULL '
EXEC(@DynamicSQL)

END

ELSE IF(@Getattributetype='12' OR @Getattributetype='4')

BEGIN
print '11G'
DECLARE @ReplaceType varchar(20)=''
set @ReplaceType = REPLACE(@AttributeType,'Number', 'Decimal')



SET @DynamicSQL = 'ALTER TABLE ##CatHierarchy ADD '+ CAST(@getItem AS NVARCHAR(max)) +' '+@ReplaceType+' '

exec (@DynamicSQL)

END

ELSE IF(@AttributeType='Date and Time' )

BEGIN

print '11GV'
SET @DynamicSQL = 'ALTER TABLE ##CatHierarchy ADD '+ CAST(@getItem AS NVARCHAR(max)) +' DateTime '
EXEC(@DynamicSQL)

END

ELSE

BEGIN
print '11GV!'

SET @DynamicSQL = 'ALTER TABLE ##CatHierarchy ADD '+ CAST(@getItem AS NVARCHAR(max)) +' '+@AttributeType+' '
EXEC(@DynamicSQL)

END


/***************************** Replace empty to Get family and product values ****************************************/	

DECLARE @FAMILY_ATTRIBUTE_VARIABLE NVARCHAR(MAX) = ''

select @FAMILY_ATTRIBUTE_VARIABLE = Replace(Replace(@getItem, '[',''), ']', '') from QSWS_CONFIGRATION_SETTINGS where catalog_id = @Catalog_Id

EXEC('
Update CAT set CAT.['+@FAMILY_ATTRIBUTE_VARIABLE+'] = ISNULL(TFS.STRING_VALUE,Convert(Numeric(38, 2),TFS.NUMERIC_VALUE)) from TB_ATTRIBUTE  TA JOIN TB_FAMILY_SPECS TFS ON TFS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID JOIN ##CatHierarchy Cat ON Cat.FAMILY_ID = TFS.FAMILY_ID
 WHERE  CAPTION  = '''+@FAMILY_ATTRIBUTE_VARIABLE+'''  AND TA.FLAG_RECYCLE=''A'' AND TA.PUBLISH2WEB = 1
		
')

EXEC('
Update CAT set CAT.['+@FAMILY_ATTRIBUTE_VARIABLE+'] = TFK.ATTRIBUTE_VALUE from TB_ATTRIBUTE  TA JOIN TB_FAMILY_KEY TFK ON TFK.ATTRIBUTE_ID=TA.ATTRIBUTE_ID JOIN ##CatHierarchy Cat ON Cat.FAMILY_ID = TFK.FAMILY_ID
 WHERE  CAPTION = '''+@FAMILY_ATTRIBUTE_VARIABLE+''' AND TA.FLAG_RECYCLE=''A''  AND TA.PUBLISH2WEB = 1
		
')

IF EXISTS ( SELECT * FROM  TempDB.INFORMATION_SCHEMA.COLUMNS  WHERE table_name = '##CatHierarchy' AND column_name = 'PRODUCT_ID')

BEGIN


if(@AttributeType<>'Date and Time')

BEGIN

EXEC('
Update CAT set CAT.['+@FAMILY_ATTRIBUTE_VARIABLE+'] = ISNULL(TPS.STRING_VALUE,Convert(Numeric(38, 2),TPS.NUMERIC_VALUE)) from TB_ATTRIBUTE  TA JOIN TB_PROD_SPECS TPS ON TPS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID JOIN ##CatHierarchy Cat ON Cat.PRODUCT_ID = TPS.PRODUCT_ID
 WHERE  CAPTION = '''+@FAMILY_ATTRIBUTE_VARIABLE+''' AND TA.FLAG_RECYCLE=''A'' AND TA.PUBLISH2WEB = 1 
		
')

EXEC('
Update CAT set CAT.['+@FAMILY_ATTRIBUTE_VARIABLE+'] = TPK.ATTRIBUTE_VALUE from TB_ATTRIBUTE  TA JOIN TB_PARTS_KEY TPK ON TPK.ATTRIBUTE_ID=TA.ATTRIBUTE_ID JOIN ##CatHierarchy Cat ON Cat.PRODUCT_ID = TPK.PRODUCT_ID
 WHERE  CAPTION = '''+@FAMILY_ATTRIBUTE_VARIABLE+''' AND TA.FLAG_RECYCLE=''A'' AND TA.PUBLISH2WEB = 1 
		
')



END

ELSE IF(@AttributeType='Date and Time')

BEGIN

EXEC('
Update CAT set CAT.['+@FAMILY_ATTRIBUTE_VARIABLE+'] = CONVERT(datetime,TPS.STRING_VALUE,103) from TB_ATTRIBUTE  TA JOIN TB_PROD_SPECS TPS ON TPS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID JOIN ##CatHierarchy Cat ON Cat.PRODUCT_ID = TPS.PRODUCT_ID
 WHERE  CAPTION = '''+@FAMILY_ATTRIBUTE_VARIABLE+''' AND TA.FLAG_RECYCLE=''A'' AND TA.PUBLISH2WEB = 1
		
')


END

--EXEC('
--Update CAT set CAT.['+@FAMILY_ATTRIBUTE_VARIABLE+'] = ISNULL(TPS.STRING_VALUE,Convert(Numeric(38, 2),TPS.NUMERIC_VALUE)) from TB_ATTRIBUTE  TA JOIN TB_PROD_SPECS TPS ON TPS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID JOIN ##CatHierarchy Cat ON Cat.PRODUCT_ID = TPS.PRODUCT_ID
-- WHERE  CAPTION = '''+@FAMILY_ATTRIBUTE_VARIABLE+''' AND TA.FLAG_RECYCLE=''A'' 
		
--')



END

ELSE

BEGIN


EXEC('
Update CAT set CAT.['+@FAMILY_ATTRIBUTE_VARIABLE+'] = ISNULL(TFS.STRING_VALUE,Convert(Numeric(38, 2),TFS.NUMERIC_VALUE)) from TB_ATTRIBUTE  TA JOIN TB_FAMILY_SPECS TFS ON TFS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID JOIN ##CatHierarchy Cat ON Cat.FAMILY_ID = TFS.FAMILY_ID
 WHERE  CAPTION  = '''+@FAMILY_ATTRIBUTE_VARIABLE+'''  AND TA.FLAG_RECYCLE=''A'' AND TA.PUBLISH2WEB = 1
		
')




END


SET @String = substring(@String,@pos+1,len(@String))
	SET @pos = charindex(@Delimiter,@String)

SET @Counter = @Counter + 1;

END


ALTER TABLE ##CatHierarchy
ADD Category_Image VARCHAR(MAX) NULL
ALTER TABLE ##CatHierarchy
ADD Family_Image VARCHAR(MAX) NULL
ALTER TABLE ##CatHierarchy
ADD Product_Image VARCHAR(MAX) NULL


--EXEC('SELECT CAT.Category_Image , ISNULL(TCS.STRING_VALUE,TCS.NUMERIC_VALUE) from TB_ATTRIBUTE  TA 
--JOIN TB_CATEGORY_SPECS TCS ON TCS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID 
--JOIN ##CatHierarchy Cat ON Cat.CATEGORY_ID = TCS.CATEGORY_ID AND CAT.CATALOG_ID = TCS.CATALOG_ID
--JOIN TB_CATEGORY TC ON TC.CATEGORY_ID=CAT.CATEGORY_ID AND TC.FLAG_RECYCLE=''A''
--JOIN QSWS_CONFIGRATION_SETTINGS QCS ON QCS.CATALOG_ID =CAT.CATALOG_ID
--  AND TA.ATTRIBUTE_NAME  = QCS.Category_Image  WHERE  TA.FLAG_RECYCLE=''A'' ') 

  

EXEC('
Update CAT set CAT.Category_Image = ISNULL(TCS.STRING_VALUE,Convert(Numeric(38, 2),TCS.NUMERIC_VALUE)) from TB_ATTRIBUTE  TA
 JOIN TB_CATEGORY_SPECS TCS ON TCS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID 
 JOIN ##CatHierarchy Cat ON Cat.CATEGORY_ID = TCS.CATEGORY_ID AND CAT.CATALOG_ID = TCS.CATALOG_ID
 JOIN TB_CATEGORY TC ON TC.CATEGORY_ID=CAT.CATEGORY_ID AND TC.FLAG_RECYCLE=''A'' AND TC.PUBLISH2WEB = 1
JOIN QSWS_CONFIGRATION_SETTINGS QCS ON QCS.CATALOG_ID =CAT.CATALOG_ID
  AND TA.CAPTION  = QCS.Category_Image  WHERE  TA.FLAG_RECYCLE=''A'' AND TA.PUBLISH2WEB = 1
		
')



EXEC('
Update CAT set CAT.Family_Image = ISNULL(TFS.STRING_VALUE,Convert(Numeric(38, 2),TFS.NUMERIC_VALUE)) from TB_ATTRIBUTE  TA
 JOIN TB_FAMILY_SPECS TFS ON TFS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID
  JOIN ##CatHierarchy Cat ON Cat.FAMILY_ID = TFS.FAMILY_ID
JOIN QSWS_CONFIGRATION_SETTINGS QCS ON QCS.CATALOG_ID =CAT.CATALOG_ID
  AND TA.CAPTION  = QCS.Family_Image  WHERE  TA.FLAG_RECYCLE=''A'' AND TA.PUBLISH2WEB = 1
		
')


EXEC('
Update CAT set CAT.Product_Image = ISNULL(TPS.STRING_VALUE,Convert(Numeric(38, 2),TPS.NUMERIC_VALUE)) from TB_ATTRIBUTE  TA
 JOIN TB_PROD_SPECS TPS ON TPS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID 
 JOIN ##CatHierarchy Cat ON Cat.PRODUCT_ID = TPS.PRODUCT_ID
JOIN QSWS_CONFIGRATION_SETTINGS QCS ON QCS.CATALOG_ID =CAT.CATALOG_ID
  AND TA.CAPTION  = QCS.Product_Image  WHERE  TA.FLAG_RECYCLE=''A'' AND TA.PUBLISH2WEB = 1
		
')


/***************************** Add Flag recycle and last update date in the temporary table ****************************************/	


ALTER TABLE ##CatHierarchy ADD FLAG_RECYCLE VARCHAR(1) DEFAULT 'A' NOT NULL

ALTER TABLE ##CatHierarchy ADD LAST_UPDATED_DATE DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ;

--Select * INTO MSWEB_CATALOG_SNAPSHOT from ##CatHierarchy  



Exec
('Select * INTO QSWS_CATALOG_SNAPSHOT_'+@Catalog_Id+' from ##CatHierarchy  ')
print @Catalog_Id
--Select * INTO QSWS_CATALOG_SNAPSHOT from ##CatHierarchy 


--select  TPF.PRODUCT_ID,TPF.FAMILY_ID from ##CatHierarchy CH 
--LEFT JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID= CH.FAMILY_ID AND TPF.FLAG_RECYCLE='A'
--WHERE TPF.FAMILY_ID=22800
/***************************** Drop temporary tables  ****************************************/	



IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##Family_Attributes')  
BEGIN  
EXEC('DROP TABLE [##Family_Attributes]')  
END
IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##CatHierarchy')  
BEGIN  
EXEC('DROP TABLE [##CatHierarchy]')  
END

SELECT @RETURN_VALUE=(SELECT 'Success' AS ErrorMessage);

	 SELECT @RETURN_VALUE AS 'Success'


	COMMIT TRANSACTION

	END

	ELSE IF(@OPTION='DELETE')

	BEGIN

	delete from QSWS_CONFIGRATION_SETTINGS where Catalog_id = @Catalog_Id
    delete from QSWS_SEARCH_FILTER where CATALOG_ID = @Catalog_Id
	 set @TableName  = 'QSWS_CATALOG_SNAPSHOT_'+@Catalog_Id
	
	 print @TableName
			IF OBJECT_ID(@TableName) IS NOT NULL
				EXEC ('drop Table ' + @TableName)
SELECT @RETURN_VALUE=(SELECT 'Success' AS ErrorMessage);

	 SELECT @RETURN_VALUE AS 'Success'


	COMMIT TRANSACTION
	END

END TRY

BEGIN CATCH
    IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
			print @@TRANCOUNT
			SELECT @RETURN_VALUE=(SELECT ERROR_MESSAGE() AS ErrorMessage)
			SELECT @RETURN_VALUE AS ErrorMessage

SELECT
    @@ERROR AS Error,
    ERROR_NUMBER() AS ErrorNumber,
    ERROR_STATE() AS ErrorState,
    ERROR_SEVERITY() AS ErrorSeverity,
    ERROR_PROCEDURE() AS ErrorProcedure,
    ERROR_LINE() AS ErrorLine,
    ERROR_MESSAGE() AS ErrorMessage;

END CATCH

GO

ALTER PROCEDURE [dbo].[STP_QSWS_CONFIGURATION_SETTINGS](@CATALOG_ID INT = 123,@ATTRIBUTES VARCHAR(MAX)= 'List Price,Format,Member Price,New price,New key'
,@OPTION VARCHAR(MAX)='SEARCH_SELECTED_LIST')
AS
BEGIN
IF (@OPTION='SELECTED_LIST')
BEGIN
 SELECT DISTINCT @CATALOG_ID AS CATALOG_ID,ATTRIBUTE_ID,CAPTION AS ATTRIBUTE_NAME,ATTRIBUTE_TYPE FROM TB_ATTRIBUTE 
WHERE CAPTION  IN(SELECT Item FROM DBO.[SPLITSTRING](@ATTRIBUTES,',') as ATTRIBUTE_NAME) AND FLAG_RECYCLE = 'A'  and PUBLISH2WEB=1 and ATTRIBUTE_TYPE not in (7,11,13,9,21,23,25) 
UNION
SELECT DISTINCT @CATALOG_ID AS CATALOG_ID,ATTRIBUTE_ID,CAPTION AS ATTRIBUTE_NAME,ATTRIBUTE_TYPE  
FROM TB_ATTRIBUTE A WHERE  A.ATTRIBUTE_ID=1 AND FLAG_RECYCLE = 'A' AND A.ATTRIBUTE_TYPE not in (3)  and PUBLISH2WEB=1 
END
ELSE IF(@OPTION='ALL_LIST')
BEGIN
select DISTINCT D.CATALOG_ID,D.ATTRIBUTE_ID,C.CAPTION AS ATTRIBUTE_NAME  FROM TB_CATALOG_PRODUCT  A
 JOIN TB_CATALOG_ATTRIBUTES D ON D.CATALOG_ID=A.CATALOG_ID 
 JOIN TB_ATTRIBUTE C ON C.ATTRIBUTE_ID=D.ATTRIBUTE_ID
 WHERE A.CATALOG_ID=@CATALOG_ID AND ATTRIBUTE_TYPE not in (7,11,13,9,21,23,25,3)  
 AND C.ATTRIBUTE_ID<>1 AND C.FLAG_RECYCLE = 'A' AND C.PUBLISH2WEB=1
 AND C.CAPTION NOT IN (SELECT Item FROM DBO.[SPLITSTRING](@ATTRIBUTES,',') as ATTRIBUTE_NAME) AND c.PUBLISH2WEB=1 
END
ELSE IF(@OPTION='SEARCH_SELECTED_LIST')
BEGIN
SELECT DISTINCT @CATALOG_ID AS CATALOG_ID,ATTRIBUTE_ID,CAPTION AS ATTRIBUTE_NAME,ATTRIBUTE_TYPE FROM TB_ATTRIBUTE 
WHERE CAPTION IN( SELECT Item FROM DBO.[SPLITSTRING](@ATTRIBUTES,',')) AND FLAG_RECYCLE = 'A'  and PUBLISH2WEB=1 
 AND ATTRIBUTE_TYPE in (1,4,10,6) 
END
ELSE IF (@OPTION='SELECTED_FAMILY_LIST')
BEGIN
 SELECT DISTINCT ATTRIBUTE_ID,CAPTION AS ATTRIBUTE_NAME,ATTRIBUTE_TYPE FROM TB_ATTRIBUTE 
WHERE CAPTION IN(SELECT Item FROM DBO.[SPLITSTRING](@ATTRIBUTES,',') as ATTRIBUTE_NAME) AND FLAG_RECYCLE = 'A'  and PUBLISH2WEB=1 
--UNION
--SELECT DISTINCT @CATALOG_ID AS CATALOG_ID,ATTRIBUTE_ID,ATTRIBUTE_NAME,ATTRIBUTE_TYPE  
--FROM TB_ATTRIBUTE A WHERE  A.ATTRIBUTE_ID=1
END
ELSE IF(@OPTION='ALL_AVAILABLE_FAMILY_LIST')
BEGIN
SELECT DISTINCT D.CATALOG_ID,B.ATTRIBUTE_ID,CAPTION AS ATTRIBUTE_NAME,ATTRIBUTE_TYPE  FROM TB_CATALOG_FAMILY  A
 JOIN 
 TB_FAMILY_SPECS B ON A.FAMILY_ID=B.FAMILY_ID JOIN TB_ATTRIBUTE C ON C.ATTRIBUTE_ID=B.ATTRIBUTE_ID
 JOIN TB_CATALOG_ATTRIBUTES D ON D.CATALOG_ID=A.CATALOG_ID
 WHERE A.CATALOG_ID=@CATALOG_ID 
 AND C.ATTRIBUTE_TYPE!=9 
 AND C.CAPTION NOT IN (SELECT Item FROM DBO.[SPLITSTRING](@ATTRIBUTES,',') as ATTRIBUTE_NAME) AND C.FLAG_RECYCLE = 'A'  and C.PUBLISH2WEB=1 
END
ELSE IF(@OPTION='SEARCH_SELECTED_FAMILY_LIST')
BEGIN
SELECT DISTINCT @CATALOG_ID AS CATALOG_ID,ATTRIBUTE_ID,CAPTION AS ATTRIBUTE_NAME,ATTRIBUTE_TYPE FROM TB_ATTRIBUTE 
WHERE CAPTION IN( SELECT Item FROM DBO.[SPLITSTRING](@ATTRIBUTES,','))  AND FLAG_RECYCLE = 'A'  and PUBLISH2WEB=1 
AND ATTRIBUTE_TYPE <>9
END
ELSE IF(@OPTION='GET_LITERATURE_CATALOG')
BEGIN
SELECT TC.CATALOG_ID ,TC.CATALOG_NAME FROM TB_ATTRIBUTE TA
	JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID = TCA.ATTRIBUTE_ID
	JOIN TB_CATALOG TC ON TC.CATALOG_ID = TCA.CATALOG_ID
WHERE CAPTION IN('LITERATURE_PDF_NAME') 
AND TC.CATALOG_ID <> 1  and TA.PUBLISH2WEB=1 
END
ELSE IF(@OPTION='GET_CATEGORY_IMAGE')
BEGIN
SELECT DISTINCT TA.ATTRIBUTE_NAME FROM TB_ATTRIBUTE TA JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID
WHERE TCA.CATALOG_ID = @CATALOG_ID AND TA.ATTRIBUTE_TYPE = 23   and TA.PUBLISH2WEB=1 
END
ELSE IF(@OPTION='GET_CATALOG')
BEGIN
SELECT CATALOG_ID, CATALOG_NAME,[DESCRIPTION] FROM TB_CATALOG 
	WHERE CATALOG_ID NOT IN(
SELECT TC.CATALOG_ID FROM TB_ATTRIBUTE TA
	JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID = TCA.ATTRIBUTE_ID
	JOIN TB_CATALOG TC ON TC.CATALOG_ID = TCA.CATALOG_ID
WHERE CAPTION IN('LITERATURE_PDF_NAME') and TA.PUBLISH2WEB=1 )
END
END

GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GET_ATTRIBUTE_FILTER_DETAILS]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GET_ATTRIBUTE_FILTER_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_QSWS_GET_ATTRIBUTE_FILTER_DETAILS] AS' 
END
GO




ALTER PROCEDURE [dbo].[STP_QSWS_GET_ATTRIBUTE_FILTER_DETAILS]
(@OPTION VARCHAR(100)='GET_FILTER_RESULT', @CATALOG_ID INT=123,@FAMILY_ID varchar(max)='26152',@CATEGORY_ID varchar(MAX) ='CAT12632,CAT12633,CAT12634,CAT12635',@TEXTSEARCH varchar(max)='Enhancing RTI: How to Ensure Success with Effective Classroom Instruction and Intervention~'
,@SESSION_ID varchar(max)='593e9a95-099e-5356-6e7d-717bad8fbe06')
AS
BEGIN
   DECLARE @AttributeFilterMasterMaster TABLE( 

    Catalog_Id INT NOT NULL, 

    Attribute_Name NVARCHAR(max) ,

	String_Value NVARCHAR(max)

); 

DECLARE  @Filtevalue   NVARCHAR(Max)
DECLARE  @Filter_Name  NVARCHAR(Max)

IF OBJECT_ID('TEMPDB..[##TEMP_ATTRIBUTE_VALUES'+@SESSION_ID+']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_ATTRIBUTE_VALUES'+ @SESSION_ID +']')

IF OBJECT_ID('TEMPDB..[##FilterCount'+ @SESSION_ID +']') IS NOT NULL                            
EXEC('DROP TABLE [##FilterCount'+ @SESSION_ID +'] ')


IF OBJECT_ID('TEMPDB..[##FilterAttributevalue'+ @SESSION_ID +']') IS NOT NULL                            
EXEC('DROP TABLE [##FilterAttributevalue'+ @SESSION_ID +'] ')

--SET @SESSION_ID = NEWID()
--PRINT @SESSION_ID


DECLARE @CATALOG_DETAILS NVARCHAR(MAX)

SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'


DECLARE @COUNT NVARCHAR(MAX)
DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Filter_Name FROM QSWS_SEARCH_FILTER WHERE CATALOG_ID=@CATALOG_ID order by CHECK_FLAG asc
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name 
WHILE @@FETCH_STATUS = 0  
BEGIN  

    
DECLARE @VARFAM1 NVARCHAR(MAX); 

--EXEC('SELECT COUNT ('''+@Filter_Name+''') AS COUNTDETAILS into [##FilterCount'+ @SESSION_ID +'] FROM '''+@CATALOG_DETAILS+''' WHERE '''+@Filter_Name+''' IS NOT NULL AND CATALOG_ID=CONVERT(varchar(10),'''+@CATALOG_ID+''')')
EXEC('SELECT COUNT ('''+@Filter_Name+''') AS COUNTDETAILS into [##FilterCount'+ @SESSION_ID +'] FROM '+@CATALOG_DETAILS+' WHERE '''+@Filter_Name+''' IS NOT NULL AND CATALOG_ID=convert(varchar(10),'+@CATALOG_ID+') ')

--PRINT('SELECT COUNT ('''+@Filter_Name+''') AS COUNTDETAILS into [##FilterCount'+ @SESSION_ID +'] FROM '+@CATALOG_DETAILS+' WHERE '''+@Filter_Name+''' IS NOT NULL AND CATALOG_ID=convert(varchar(10),+@CATALOG_ID+) ')

declare @VARFAM1_Count int;

set @VARFAM1 = 'SELECT @VARFAM1_Count = COUNTDETAILS FROM [##FilterCount'+ @SESSION_ID +'] '
EXECUTE sp_executeSQL @VARFAM1, N'@VARFAM1_Count INT OUTPUT', @VARFAM1_Count OUTPUT

PRINT @VARFAM1_Count

--EXEC(@COUNT)


IF(@VARFAM1_Count>0)
BEGIN


SET @Filtevalue=@Filter_Name

If((@FAMILY_ID='' or @FAMILY_ID is null) and (@CATEGORY_ID='' or @CATEGORY_ID is null) and (@TEXTSEARCH=''or @TEXTSEARCH is null))
BEGIN
print'4'
exec ('select Catalog_Id,'+@Filtevalue+' as String_Value into [##FilterAttributevalue'+@SESSION_ID +'] from '+@CATALOG_DETAILS+' where CATALOG_ID='''+@CATALOG_ID+'''  ')

END
ELSE if((@FAMILY_ID='' or @FAMILY_ID is null) and (@CATEGORY_ID<>'' or @CATEGORY_ID is not null) and (@TEXTSEARCH=''or @TEXTSEARCH is null))
BEGIN
print'85'
EXEC('(SELECT
 Catalog_Id,'+@Filtevalue+' as String_Value
  into [##FilterAttributevalue'+@SESSION_ID +'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID=convert(varchar(10),'+@CATALOG_ID+')) ')


END
ELSE if ((@FAMILY_ID<>'' or @FAMILY_ID is not null) and (@CATEGORY_ID<>'' or @CATEGORY_ID is not null) and (@TEXTSEARCH=''or @TEXTSEARCH is null))
BEGIN
print'2'
EXEC('(SELECT
 Catalog_Id,'+@Filtevalue+' as String_Value
  into [##FilterAttributevalue'+@SESSION_ID +'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID=convert(varchar(10),'+@CATALOG_ID+')  and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')

END
ELSE if ((@TEXTSEARCH<>''or @TEXTSEARCH is not null) )
BEGIN
print'128'
--PRINT ('select PR.Catalog_Id,PR.'+@Filtevalue+' as String_Value into [##FilterAttributevalue'+@SESSION_ID +'] from '+@CATALOG_DETAILS+' CAT join
--[PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] PR on CAT.FAMILY_ID=PR.FAMILY_ID where PR.CATALOG_ID=convert(varchar(10),'+@CATALOG_ID+' ')

EXEC ('select PR.Catalog_Id,PR.'+@Filtevalue+' as String_Value into [##FilterAttributevalue'+@SESSION_ID +'] from '+@CATALOG_DETAILS+' CAT join
[PRODUCTDISPALYDETAILS'+@SESSION_ID+'] PR on CAT.FAMILY_ID=PR.FAMILY_ID where PR.Catalog_Id='+@CATALOG_ID+' ')
END

INSERT INTO @AttributeFilterMasterMaster (Catalog_Id,Attribute_Name,String_Value)
EXEC('SELECT DISTINCT Catalog_Id,'''+@Filter_Name+''',String_Value as Attribute_Name FROM [##FilterAttributevalue'+ @SESSION_ID +'] WHERE '''+@Filter_Name+''' IS NOT NULL AND '''+@Filter_Name+'''<>''''  and string_value IS NOT NULL and LEN(string_value)<> 0 ')
--print('SELECT DISTINCT Catalog_Id,'+@Filter_Name+',String_Value as Attribute_Name FROM [##FilterAttributevalue'+ @SESSION_ID +'] WHERE '+@Filter_Name+' IS NOT NULL AND '+@Filter_Name+'<>''''  and string_value IS NOT NULL and LEN(string_value)<> 0 ')

IF OBJECT_ID('TEMPDB..[##FilterAttributevalue'+ @SESSION_ID +']') IS NOT NULL                            
EXEC('DROP TABLE [##FilterAttributevalue'+ @SESSION_ID +']')


--SELECT Replace(Replace(Attribute_name, '[', ''), ']', '') as Attribute_name,String_Value  FROM @AttributeFilterMasterMaster 

END

IF OBJECT_ID('TEMPDB..[##FilterCount'+@SESSION_ID +']') IS NOT NULL                            
EXEC('DROP TABLE [##FilterCount'+@SESSION_ID +']')
FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name 

END  
CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER

IF OBJECT_ID('TEMPDB..##TEMP_AttributeFilterMaster') IS NOT NULL                            
DROP TABLE ##TEMP_AttributeFilterMaster

SELECT Replace(Replace(Attribute_name, '[', ''), ']', '') as Attribute_name,String_Value INTO ##TEMP_AttributeFilterMaster   FROM @AttributeFilterMasterMaster 


EXEC('SELECT Replace(Replace(Attribute_name, ''['', ''''), '']'', '''') as Attribute_name,String_Value,ROW_NUMBER() OVER(ORDER BY (select(1)) ) AS Row# INTO
 [##TEMP_ATTRIBUTE_VALUES'+@SESSION_ID +'] FROM ##TEMP_AttributeFilterMaster')

 IF OBJECT_ID('TEMPDB..[##NewValue'+@SESSION_ID +']') IS NOT NULL                            
EXEC('DROP TABLE [##NewValue'+@SESSION_ID +']')


EXEC('SELECT  distinct TAV.Attribute_name,TAV.String_Value , TA.ATTRIBUTE_ID ,TA.ATTRIBUTE_TYPE,
ROW_NUMBER() OVER ( PARTITION BY TAV.Attribute_name ORDER BY TAV.Row#) ROW_NUM,Row# into [##NewValue'+@SESSION_ID +']
 FROM  [##TEMP_ATTRIBUTE_VALUES'+@SESSION_ID +']  TAV JOIN TB_ATTRIBUTE TA ON  TA.CAPTION = TAV.Attribute_name 
 and TA.FLAG_RECYCLE=''A'' AND PUBLISH2WEB = 1 WHERE Row# >= 1  order by TAV.Row# ')

 EXEC(';WITH dup_val 
     AS (SELECT Attribute_name, 
                String_Value, 
				ATTRIBUTE_ID,
				ATTRIBUTE_TYPE,
				ROW_NUM,
                Row_number() 
                  OVER( 
                    partition BY Attribute_name, String_Value
                    ORDER BY ROW_NUM)AS Row# 
         FROM   [##NewValue'+@SESSION_ID +']) 
SELECT * 
FROM   dup_val 
WHERE  Row# = 1; ')

--EXEC('SELECT  TAV.Attribute_name,TAV.String_Value , TA.ATTRIBUTE_ID ,
--ROW_NUMBER() OVER (PARTITION BY TAV.Attribute_name ORDER BY TAV.Row#) ROW_NUM,Row#
-- FROM [##TEMP_ATTRIBUTE_VALUES'+@SESSION_ID +']  TAV JOIN TB_ATTRIBUTE TA ON  TA.CAPTION = TAV.Attribute_name 
-- and TA.FLAG_RECYCLE=''A'' order by TAV.Row# ')

END


GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GET_AUTOCOMPLETE_TEXTSEARCH_RESULT_REFINEFILTER_SESSION_ID]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GET_AUTOCOMPLETE_TEXTSEARCH_RESULT_REFINEFILTER_SESSION_ID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_QSWS_GET_AUTOCOMPLETE_TEXTSEARCH_RESULT_REFINEFILTER_SESSION_ID] AS' 
END
GO



ALTER  PROCEDURE [dbo].[STP_QSWS_GET_AUTOCOMPLETE_TEXTSEARCH_RESULT_REFINEFILTER_SESSION_ID] (
 @SEARCH_TEXT VARCHAR(MAX)='',
 @USER_ID INT =8,
 @PAGENO INT= 1,
 @PAGEPERCOUNT INT=25,
 @CATALOG_ID INT =123,
 @CATEGORY_ID varchar(MAX) ='',
 @FAMILY_ID varchar(max)='',
 @sessionId nvarchar(max)='',
 @ATTRIBUTETABLE AttributeParameterValuesValues READONLY

 )as


 
BEGIN

--Select * into ##TESTING from @ATTRIBUTETABLE

--return

 --SET @sessionId = NEWID()
	-- PRINT @sessionId
--Select * into ##newtesting from @ATTRIBUTETABLE

--return

 --SET @sessionId = NEWID()
	-- PRINT @sessionId

BEGIN  
declare @SEARCH_TEXT_TEMP varchar(100)
declare @SEARCH_TEXT_REPLACE varchar(100)
declare @SEARCH_TEXT_COUNT int
set @SEARCH_TEXT = REPLACE(@SEARCH_TEXT,'  ',' ')
set @SEARCH_TEXT_TEMP = @SEARCH_TEXT
SET @SEARCH_TEXT_TEMP = REPLACE(@SEARCH_TEXT_TEMP , ' ', '~')
set @SEARCH_TEXT_REPLACE = replace(@SEARCH_TEXT_TEMP,'~', '')
set @SEARCH_TEXT_COUNT = ((len(@SEARCH_TEXT_TEMP) - len(@SEARCH_TEXT_REPLACE)) / len('~') +1)



DECLARE @CATALOG_DETAILS NVARCHAR(MAX)

SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'

IF OBJECT_ID('TEMPDB..[##TEMP_SEARCH'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_SEARCH'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_FULL_SEARCH'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_FULL_SEARCH'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_SPLIT_SEARCH'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_SPLIT_SEARCH'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##SEARCH_FILTER_RESULT'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##SEARCH_FILTER_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##SEARCH_FINAL_RESULT'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##SEARCH_FINAL_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_RESULT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_Family_PRODUCT'+ @sessionId +']')


 IF OBJECT_ID('TEMPDB..[##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +']') IS NOT NULL                            
  EXEC('DROP TABLE [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +']')

  IF OBJECT_ID('TEMPDB..[##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']')

 IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+']')

END


IF @SEARCH_TEXT in  ('Of', 'in', 'at', 'and', 'is', 'or' , 'an', 'the') 
 SET @SEARCH_TEXT = '~^'

IF OBJECT_ID('TEMPDB..[##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']')


DECLARE  @Filtevalue1   NVARCHAR(Max)

DECLARE  @AttributeAppend   NVARCHAR(Max)
DECLARE  @Filtevalue2   NVARCHAR(Max)
DECLARE  @Filter_Name1  NVARCHAR(Max)
DECLARE  @ATTRIBUTE_TYPE  NVARCHAR(Max)
DECLARE  @columns1 NVARCHAR(MAX) = ''
DECLARE  @columns2 NVARCHAR(MAX) = ''
DECLARE  @columns3 NVARCHAR(MAX) = ''
DECLARE @productcolumns1 NVARCHAR(MAX) = ''
DECLARE @ATTR_COUNT1 NVARCHAR(MAX) =''
DECLARE  @AttributeAppend_ForKeyAttribute   NVARCHAR(Max)

--SET @columns1 = N'';
--SELECT @columns1+=N''+Name 
--FROM
--(
--    SELECT Search_Attribute AS [Name],Catalog_Id
--    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @CATALOG_ID
  
--) AS x;


--PRINT @columns1

/* KEY ATTRIBUTES AND SEARCH ATTRIBUTES START */
 IF OBJECT_ID('TEMPDB..##TEMP_ATTRIBUTES') IS NOT NULL    
 EXEC('DROP TABLE ##TEMP_ATTRIBUTES')
  IF OBJECT_ID('TEMPDB..[##TEMP_SEARCH_KEY_ATTRIBUTES'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##TEMP_SEARCH_KEY_ATTRIBUTES'+ @sessionId +']')
 EXEC('
 ;WITH CTE AS (
SELECT  ITEM, ''1'' AS TYPE 
FROM QSWS_CONFIGRATION_SETTINGS
CROSS APPLY SPLITSTRING(Search_Attribute, '','')
  where catalog_id='''+ @CATALOG_ID +'''
)
, CTE1 AS (
SELECT  QCS.ITEM, ''2'' AS TYPE 
FROM QSWS_CONFIGRATION_SETTINGS 
CROSS APPLY SPLITSTRING(Key_Attribute, '','') QCS
  where catalog_id='''+ @CATALOG_ID +'''
  )
--UPDATE C SET C.TYPE = C1.TYPE 
--  FROM [##TEMP_SEARCH_KEY_ATTRIBUTES'+ @sessionId +'] C JOIN CTE1 C1 ON 
--   C.Item=C1.ITEM
SELECT  * INTO ##TEMP_ATTRIBUTES
FROM    cte
UNION ALL
SELECT  *
FROM    cte1
  ')
/* KEY ATTRIBUTES AND SEARCH ATTRIBUTES END */
DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT ITEM,TYPE FROM ##TEMP_ATTRIBUTES
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1,@ATTRIBUTE_TYPE
WHILE @@FETCH_STATUS = 0  
BEGIN  
BEGIN
DECLARE @MULTIPLY_INT INT = 1;
IF(@ATTRIBUTE_TYPE='2')
BEGIN
--SET @MULTIPLY_INT =2
SET @Filtevalue2 =  ' OR '+@Filter_Name1+' LIKE ''XXXXX'' '
set @AttributeAppend_ForKeyAttribute=  CONCAT(@AttributeAppend_ForKeyAttribute,@Filtevalue2)
END
ELSE IF(@ATTRIBUTE_TYPE='1')
BEGIN
--SET @MULTIPLY_INT =1
SET @Filtevalue2 =  ' OR '+@Filter_Name1+' LIKE ''XXXXX'' '
set @AttributeAppend=  CONCAT( @AttributeAppend,@Filtevalue2)
END
END
FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1,@ATTRIBUTE_TYPE


END  
CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER


	DECLARE @AttributeAppendKeyAttribute_Replace VARCHAR(MAX)=''
set @AttributeAppendKeyAttribute_Replace =@AttributeAppend_ForKeyAttribute
set @AttributeAppendKeyAttribute_Replace =  REPLACE(@AttributeAppendKeyAttribute_Replace, 'XXXXX', ''+@SEARCH_TEXT + '');
print '316'
print @AttributeAppendKeyAttribute_Replace

PRINT @AttributeAppend

DECLARE @AttributeAppend_Replace VARCHAR(MAX)=''

set @AttributeAppend_Replace =@AttributeAppend

set @AttributeAppend_Replace =  REPLACE(@AttributeAppend_Replace, 'XXXXX', ''+@SEARCH_TEXT + '');

print @AttributeAppend_Replace




EXEC('SELECT *,CONVERT(DECIMAL(9,2), 0.00) as FamilyAllwords, CONVERT(DECIMAL(9,2), 0.00) as Familyfullword, 
CONVERT(DECIMAL(9,2), 0.00) as FamilyPartialword, CONVERT(DECIMAL(9,2), 0.00) as Allwords, 
CONVERT(DECIMAL(9,2), 0.00) as fullword, CONVERT(DECIMAL(9,2), 0.00) as Partialword, 
CONVERT(DECIMAL(9,2), 0.00) as totalWeight INTO [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] 
FROM '+@CATALOG_DETAILS+'  ')

-- ********************************** Family Name Full Search ***************************************************
EXEC('
Select distinct  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
  (T1.FAMILY_NAME LIKE '''+@SEARCH_TEXT + ''')

)


')

 print @SEARCH_TEXT
 print '1'
 Exec('UPDATE TQCPS SET TQCPS.FamilyAllwords = ('+@SEARCH_TEXT_COUNT+' *4)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 Exec('UPDATE TQCPS SET TQCPS.FamilyAllwords = ('+@SEARCH_TEXT_COUNT+' *4)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 --- ********************************************************* Family Name Full Search for Key attribute   *************************************
EXEC('
Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1
	  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SEARCH_TEXT + ''')
  '+@AttributeAppendKeyAttribute_Replace +'
)
')
print '385'
 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2 * 2)  +1
  FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS 
  JOIN [##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +']  TFS ON 
  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2 * 2)  +1 FROM  
 [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS 
 JOIN [##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +']  TFS ON
  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')


--- ********************************************************* Family Name Full Search for Key attribute *************************************

--- ********************************************************* Family Name Full Search END   *************************************



EXEC('
Select distinct  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##TEMP_FULL_SEARCH'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SEARCH_TEXT + ''')

  '+@AttributeAppend_Replace +'



)


')

 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')




   IF OBJECT_ID('TEMPDB..##SPIT_SEARCH_TEXT') IS NOT NULL    
EXEC('DROP TABLE ##SPIT_SEARCH_TEXT')

SELECT ITEM into ##SPIT_SEARCH_TEXT FROM SPLITSTRING(@SEARCH_TEXT,'')
 DECLARE @count int = 1;
 DECLARE @SPLITTEXT VARCHAR(50) =''
 --select @SEARCH_TEXT_COUNT
 WHILE (@count <=  @SEARCH_TEXT_COUNT )
 BEGIN

IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Allwords'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Allwords'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Partial'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial_InBetween'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Partial_InBetween'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']')
IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +']') IS NOT NULL 
   
EXEC('DROP TABLE [##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +']')
IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +']') IS NOT NULL  
  
EXEC('DROP TABLE [##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Family_Name_Search_Result_Partial'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']')
 
SELECT TOP 1  @SPLITTEXT= LTRIM(RTRIM(ITEM)) FROM ##SPIT_SEARCH_TEXT where ITEM NOT IN ('Of', 'in', 'at', 'and', 'is', 'or' , 'an', 'the')
 SET @COUNT =  @COUNT +1
  
  IF @SPLITTEXT =''
  SET @SPLITTEXT = '~'

  PRINT @SPLITTEXT

  DECLARE @AttributeAppend_ReplaceForSplit_text_KeyAttribute VARCHAR(MAX)=''
set @AttributeAppend_ReplaceForSplit_text_KeyAttribute =@AttributeAppend_ForKeyAttribute
set @AttributeAppend_ReplaceForSplit_text_KeyAttribute =  REPLACE(@AttributeAppend_ReplaceForSplit_text_KeyAttribute, 'XXXXX', ''+@SPLITTEXT + '%');

DECLARE @AttributeAppend_ReplaceForSplit_text VARCHAR(MAX)=''

set @AttributeAppend_ReplaceForSplit_text =@AttributeAppend

set @AttributeAppend_ReplaceForSplit_text =  REPLACE(@AttributeAppend_ReplaceForSplit_text, 'XXXXX', ''+@SPLITTEXT + '%');

print @AttributeAppend_ReplaceForSplit_text

 /*************************** Family Name Partial word search *******************/
 
 EXEC('

Select distinct  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']
From '+@CATALOG_DETAILS+' T1

		  WHERe  (
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + '%'')

  )

')


 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')


 /*************************** Family Name Partial word search END  *******************/

 /*************************** Partial word search for Key Attribute  *******************/
 EXEC('
Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']
From '+@CATALOG_DETAILS+' T1
		  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + '%'')
  '+@AttributeAppend_ReplaceForSplit_text_KeyAttribute +'
  )
')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] 
 TQCPS JOIN [##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']
  TQCPS JOIN [##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')
 /*************************** Partial word search for Key Attribute *******************/


 /*************************** Partial word search *******************/

 EXEC('

Select distinct  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial'+ @sessionId +']
From '+@CATALOG_DETAILS+' T1

		  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + '%'')

  '+@AttributeAppend_ReplaceForSplit_text +'


  )

')



 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')



 /*************************** Partial word search *******************/

  /*************************** Partial word search in between *******************/

  	DECLARE @AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute VARCHAR(MAX)=''
set @AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute =@AttributeAppend_ForKeyAttribute
set @AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute, 'XXXXX', '%'+@SPLITTEXT + '%');
print @AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute

DECLARE @AttributeAppend_ReplaceForSplit_text_Inbetween VARCHAR(MAX)=''

set @AttributeAppend_ReplaceForSplit_text_Inbetween =@AttributeAppend

set @AttributeAppend_ReplaceForSplit_text_Inbetween =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Inbetween, 'XXXXX', '%'+@SPLITTEXT + '%');

print @AttributeAppend_ReplaceForSplit_text_Inbetween

 /*************************** Partial word search In between *******************/

  

  EXEC('

Select distinct  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
	
  (T1.FAMILY_NAME LIKE ''%'+@SPLITTEXT + '%'')

  
)

')



 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 /*************************** Family Name  Partial word search In between for Key attributes  *******************/
  EXEC('
Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1
	  WHERe  (
	  (T1.CATEGORY_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE ''%'+@SPLITTEXT + '%'')
   '+@AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute +'
  
)
')

  EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')
 /*************************** Family Name  Partial word search In between  for Key attributes END  *******************/

 /*************************** Family Name  Partial word search In between END  *******************/

  EXEC('

Select distinct  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial_InBetween'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
	  (T1.CATEGORY_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE ''%'+@SPLITTEXT + '%'')

   '+@AttributeAppend_ReplaceForSplit_text_Inbetween +'

  
)

')

  EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 /*************************** Partial word search *******************/

  --SELECT @SPLITTEXT
 /*************************** WHOLE word search *******************/
  DECLARE @AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute VARCHAR(MAX)=''
set @AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute =@AttributeAppend_ForKeyAttribute
set @AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute, 'XXXXX',''+@SPLITTEXT + '');

 DECLARE @AttributeAppend_ReplaceForSplit_text_Wholeword VARCHAR(MAX)=''

set @AttributeAppend_ReplaceForSplit_text_Wholeword =@AttributeAppend

set @AttributeAppend_ReplaceForSplit_text_Wholeword =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Wholeword, 'XXXXX',''+@SPLITTEXT + '');

print @AttributeAppend_ReplaceForSplit_text_Wholeword

 /*************************** Family Name WHOLE word search  *******************/
 
  
 EXEC(' 

Select distinct  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##temp_Family_Name_Search_Result_Allwords'+@sessionId +'] 
From '+@CATALOG_DETAILS+' T1

  WHERe  (
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + ''')

)


')

  
 EXEC('UPDATE TQCPS SET TQCPS.Familyfullword = Familyfullword +3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 EXEC('UPDATE TQCPS SET TQCPS.Familyfullword = Familyfullword +3  FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 
  /*************************** Family Name WHOLE word search For key attribute  *******************/
 EXEC(' 
Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +'] 
From '+@CATALOG_DETAILS+' T1
  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + ''')
   '+@AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute +'
)
')
 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')
 /*************************** Family Name WHOLE word search For key attribute END  *******************/

 /*************************** Family Name WHOLE word search END  *******************/

 EXEC(' 

Select distinct  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Allwords'+ @sessionId +'] 
From '+@CATALOG_DETAILS+' T1

  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + ''')

   '+@AttributeAppend_ReplaceForSplit_text_Wholeword +'

)


')

 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 /*************************** WHOLE word search *******************/

 
 DELETE FROM ##SPIT_SEARCH_TEXT WHERE ITEM =@SPLITTEXT

 END

 EXEC('UPDATE TQCPS SET TQCPS.totalWeight = (FamilyAllwords + Familyfullword + FamilyPartialword + Allwords+fullword+PArtialWord)  FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS')

--SELECT * FROM ##QSWS_CATALOG_PRODUCTS_SNAPSHOT TQCPS where totalWeight  > 0  ORDER BY totalWeight desc

 EXEC('SELECT * into [##TEMP_SEARCH'+ @sessionId +'] FROM [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']  where totalWeight  > 0  ORDER BY totalWeight desc')
 
DECLARE @ATTRIBUTE_NAME_ITEMNO VARCHAR(MAX)=''
select @ATTRIBUTE_NAME_ITEMNO =  CAPTION from TB_ATTRIBUTE WHERE ATTRIBUTE_TYPE=1 AND ATTRIBUTE_ID=1 AND FLAG_RECYCLE ='A' AND PUBLISH2WEB = 1

Set @ATTRIBUTE_NAME_ITEMNO = '['+@ATTRIBUTE_NAME_ITEMNO+']'




Declare @ATTRIBUTE_NAME_DISPLAY NVarchar(max)=''
Select @ATTRIBUTE_NAME_DISPLAY=Search_Family_Attributes from QSWS_CONFIGRATION_SETTINGS where Catalog_Id=@CATALOG_ID
set @ATTRIBUTE_NAME_DISPLAY=@ATTRIBUTE_NAME_DISPLAY


Declare @ATTRIBUTE_PRODUCTNAME_DISPLAY NVarchar(max)=''
Select @ATTRIBUTE_PRODUCTNAME_DISPLAY=Search_Attribute from QSWS_CONFIGRATION_SETTINGS where Catalog_Id=@CATALOG_ID 
set @ATTRIBUTE_PRODUCTNAME_DISPLAY=@ATTRIBUTE_PRODUCTNAME_DISPLAY


 
 IF(@CATEGORY_ID='' and (@FAMILY_ID IS NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0  )

 BEGIN

 IF OBJECT_ID('TEMPDB..[##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']')

PRINT '524'
PRINT @ATTRIBUTE_NAME_DISPLAY
  
IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

  
 EXEC(' SELECT distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID
  ,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,
  Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' AS ITEM_NUMBER,PRODUCT_IMAGE AS PRODUCT_IMAGE_FILE
  ,totalWeight,'+@ATTRIBUTE_NAME_DISPLAY+','+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' 
  into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] 
  FROM [##TEMP_SEARCH'+ @sessionId +']  where totalWeight  > 0  ORDER BY totalWeight desc')


END

ELSE


BEGIN 

  
 EXEC(' SELECT distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' AS ITEM_NUMBER,PRODUCT_IMAGE AS PRODUCT_IMAGE_FILE,totalWeight,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] FROM [##TEMP_SEARCH'+ @sessionId +']  where totalWeight  > 0  ORDER BY totalWeight desc')



END

IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_AUTOCOMPLETE'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_AUTOCOMPLETE'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_AUTOCOMPLETE_ATTRIBUTE_VALUES'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_AUTOCOMPLETE_ATTRIBUTE_VALUES'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_AUTOCOMPLETE_FILTER_ATTRIBUTE_VALUES'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_AUTOCOMPLETE_FILTER_ATTRIBUTE_VALUES'+ @sessionId +']')


EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY   totalWeight desc,family_NAME
   )  ROW_NUM INTO [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +'] from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']  ORDER BY totalWeight desc,family_NAME')


   
--	EXEC(';with CTE AS 
--(SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE,SORT_ORDER FROM 

--(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE ,''2'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''3'' AS SORT_ORDER  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
--)

--SELECT TOP(5) * INTO [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+ @sessionId +'] FROM CTE 

--')


print '804'

EXEC('
;with CTE AS 
(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE, ''C'' AS LEVEL_TYPE,
''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']
 WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE ,''C'' AS LEVEL_TYPE,
''2'' AS SORT_ORDER  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']
 WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''F'' AS LEVEL_TYPE,
''3'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] 
WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)

SELECT TOP(5) * INTO [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+ @sessionId +'] FROM CTE 


')
DECLARE @VARFAM1 NVARCHAR(MAX); 

declare @VARFAM1_Count int;

set @VARFAM1 = 'SELECT @VARFAM1_Count =COUNT(*)  FROM [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+  @sessionId +'] '
EXECUTE sp_executeSQL @VARFAM1, N'@VARFAM1_Count INT OUTPUT', @VARFAM1_Count OUTPUT

--PRINT @VARFAM1_Count
declare  @columns NVARCHAR(MAX) = ''



IF(@VARFAM1_Count<5)
BEGIN

--EXEC('
--SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE INTO [##TEMP_AUTOCOMPLETE'+ @sessionId +'] FROM 

--(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''3'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
--)T

--')

EXEC('
SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER INTO [##TEMP_AUTOCOMPLETE'+ @sessionId +'] FROM 

(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''C'' AS LEVEL_TYPE,
''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''C'' AS LEVEL_TYPE,
''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] 
WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,
''F'' AS LEVEL_TYPE,''3'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] 
WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)T

')

  DECLARE @AttributeFilterPRODUCTForAutosearch TABLE( 

    CATEGORY_NAME VARCHAR(MAX), 

    RESULT NVARCHAR(max) ,
		
	TYPE NVARCHAR(10),
     
	LEVEL_TYPE NVARCHAR(10),

	SORT_ORDER NVARCHAR(10)

); 

SET @columns = N'';
SELECT @columns+=N''+Name 
FROM
(
    SELECT Search_Family_Attributes AS [Name],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id
  
) AS x;


IF @columns<>''
begin
print '11'
SELECT @columns+=N''+ ',' + Products
FROM
(
    SELECT Search_Attribute as [Products],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id 
  
) AS x;
END
ELSE
BEGIN
print '111'
SELECT @columns+=N''+ Products
FROM
(
    SELECT Search_Attribute as [Products],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id
  
) AS x;
END

print @columns

DECLARE @ResultTableforAutosearch Nvarchar(50)='@AttributeFilterPRODUCTForAutosearch'

DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Item 
FROM  SPLITSTRING(@columns, ',')  WHERE ITEM NOT IN (@ATTRIBUTE_NAME_ITEMNO)
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1 

WHILE @@FETCH_STATUS = 0 
BEGIN  

BEGIN


print '938'

print @Filter_Name1


if (select count(*) from @AttributeFilterPRODUCTForAutosearch) < 5
BEGIN

INSERT INTO @AttributeFilterPRODUCTForAutosearch (CATEGORY_NAME,RESULT,TYPE,LEVEL_TYPE,SORT_ORDER)

EXEC('

SELECT DISTINCT TOP(5) REPLACE(REPLACE('''+@Filter_Name1+''',''['', ''''),'']'','''')  AS CATEGORY_NAME,

'+@Filter_Name1+' AS RESULT,''A'' AS TYPE,''A'' AS LEVEL_TYPE,''4'' AS SORT_ORDER  FROM '+@CATALOG_DETAILS+' WHERE '+@Filter_Name1+'  LIKE  ''%'+@SEARCH_TEXT + '%'' ')


END

ELSE

BEGIN

BREAK;

END

END

PRINT @Filter_Name1

FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1 
END  

CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER

IF OBJECT_ID('TEMPDB..#TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES') IS NOT NULL                  
DROP TABLE #TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES

SELECT *  INTO [#TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES] FROM @AttributeFilterPRODUCTForAutosearch 



EXEC('
INSERT INTO [##TEMP_AUTOCOMPLETE'+ @sessionId +'] (CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER)

SELECT TOP(5)  CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER FROM [#TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES] ')


EXEC('SELECT TOP(5) * FROM [##TEMP_AUTOCOMPLETE'+ @sessionId +'] ')

END

ELSE

BEGIN


EXEC('
SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER FROM 

(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''C'' AS LEVEL_TYPE,''1'' AS SORT_ORDER  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''C'' AS LEVEL_TYPE,''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''F'' AS LEVEL_TYPE,''3'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)T

')


END
  
  -- EXEC('select * FROM [##TEMP_SEARCH'+ @sessionId +'] ')

--EXEC('select top(10) * FROM [##TEMP_SEARCH'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%'' OR FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%'' OR ITEMS LIKE  ''%'+@SEARCH_TEXT + '%'' ')



--EXEC('
--;with CTE AS 
--(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
--)

--SELECT TOP(5) * INTO [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+ @sessionId +'] FROM CTE 


--')


--IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
--WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+'')
--BEGIN
--EXEC('Drop table [PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+']')

--END

--EXEC('select * into [PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+'] from [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']')

--EXEC('select  distinct FAMILY_ID,FAMILY_NAME as AAAAFAMILY_NAME,IMAGE_FILE as AAAIMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+',totalWeight into [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']')


--print '544'

--EXEC('select *,ROW_NUMBER() OVER(ORDER BY totalWeight desc) AS Row# into  [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +'] ')


-- EXEC('select  * from [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +']  where Row# >= ('+@PAGEPERCOUNT+'*('+@PAGENO+' - 1))+1 AND Row# <= ('+@PAGEPERCOUNT+' *('+@PAGENO+')) order by totalWeight desc, AAAAFAMILY_NAME' )

--EXEC('SELECT COUNT(distinct FAMILY_ID) AS TOTAL_RESULTS FROM [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']')

--EXEC('SELECT  CAST(CEILING(COUNT(distinct FAMILY_ID) * 1.0 / '+@PAGEPERCOUNT+') AS INT) AS TOTAL_PAGES FROM [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +']')



END




 IF(@CATEGORY_ID<>'' and (@FAMILY_ID IS NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0  )
BEGIN
print '58'

 IF OBJECT_ID('TEMPDB..[##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']')

IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN



EXEC('(SELECT
 DISTINCT CATEGORY_NAME,CATEGORY_ID,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+','+@ATTRIBUTE_PRODUCTNAME_DISPLAY+',totalWeight
 into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] FROM 
 [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')


END


ELSE


BEGIN


EXEC('(SELECT
 DISTINCT CATEGORY_NAME,CATEGORY_ID,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+',totalWeight
 into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] FROM 
 [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')



END


Exec('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_PRODUCT'+ @sessionId +'] from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] order by PRODUCT_ID')

--   IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
--WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+'')
--BEGIN
--EXEC('Drop table [PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+']')
--END


IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_AUTOCOMPLETE'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_AUTOCOMPLETE'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_AUTOCOMPLETE_ATTRIBUTE_VALUES'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_AUTOCOMPLETE_ATTRIBUTE_VALUES'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_AUTOCOMPLETE_FILTER_ATTRIBUTE_VALUES'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_AUTOCOMPLETE_FILTER_ATTRIBUTE_VALUES'+ @sessionId +']')



EXEC('
;with CTE AS 
(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''C'' AS LEVEL_TYPE,''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE ,''C'' AS LEVEL_TYPE,''2'' AS SORT_ORDER  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''F'' AS LEVEL_TYPE,''3'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)

SELECT TOP(5) * INTO [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+ @sessionId +'] FROM CTE 


')
DECLARE @VARFAM1_Category NVARCHAR(MAX); 

declare @VARFAM1_Count_Category int;

set @VARFAM1_Category = 'SELECT @VARFAM1_Count =COUNT(*)  FROM [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+  @sessionId +'] '
EXECUTE sp_executeSQL @VARFAM1_Category, N'@VARFAM1_Count INT OUTPUT', @VARFAM1_Count_Category OUTPUT

--PRINT @VARFAM1_Count
declare  @columns_Category NVARCHAR(MAX) = ''



IF(@VARFAM1_Count_Category<5)
BEGIN

--EXEC('
--SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE INTO [##TEMP_AUTOCOMPLETE'+ @sessionId +'] FROM 

--(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''3'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
--)T

--')

EXEC('
SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER INTO [##TEMP_AUTOCOMPLETE'+ @sessionId +'] FROM 

(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''C'' AS LEVEL_TYPE,''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''C'' AS LEVEL_TYPE,''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''F'' AS LEVEL_TYPE,''3'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)T

')

  DECLARE @AttributeFilterPRODUCTForAutosearch_CATEGORY TABLE( 

    CATEGORY_NAME VARCHAR(MAX), 

    RESULT NVARCHAR(max) ,
		
	TYPE NVARCHAR(10),

	LEVEL_TYPE NVARCHAR(10),

	SORT_ORDER NVARCHAR(10)

); 

SET @columns_Category = N'';
SELECT @columns_Category+=N''+Name 
FROM
(
    SELECT Search_Family_Attributes AS [Name],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id
  
) AS x;


IF @columns_Category<>''
begin
print '11'
SELECT @columns_Category+=N''+ ',' + Products
FROM
(
    SELECT Search_Attribute as [Products],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id 
  
) AS x;
END
ELSE
BEGIN
print '111'
SELECT @columns_Category+=N''+ Products
FROM
(
    SELECT Search_Attribute as [Products],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id
  
) AS x;
END

print @columns_Category

DECLARE @ResultTableforAutosearch_ForCategory Nvarchar(50)='@AttributeFilterPRODUCTForAutosearch_CATEGORY'

DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Item 
FROM  SPLITSTRING(@columns, ',')  WHERE ITEM NOT IN (@ATTRIBUTE_NAME_ITEMNO)
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1 

WHILE @@FETCH_STATUS = 0 
BEGIN  

BEGIN


print '938'

print @Filter_Name1


if (select count(*) from @AttributeFilterPRODUCTForAutosearch_CATEGORY) < 5
BEGIN

INSERT INTO @AttributeFilterPRODUCTForAutosearch_CATEGORY (CATEGORY_NAME,RESULT,TYPE,LEVEL_TYPE,SORT_ORDER)

EXEC('

SELECT DISTINCT TOP(5) REPLACE(REPLACE('''+@Filter_Name1+''',''['', ''''),'']'','''')  AS CATEGORY_NAME,'+@Filter_Name1+' AS RESULT,''A'' AS TYPE,''A'' AS LEVEL_TYPE,''4'' AS SORT_ORDER  FROM '+@CATALOG_DETAILS+' WHERE '+@Filter_Name1+'  LIKE  ''%'+@SEARCH_TEXT + '%'' ')


END

ELSE

BEGIN

BREAK;

END

END

PRINT @Filter_Name1

FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1 
END  

CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER

IF OBJECT_ID('TEMPDB..#TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES_ForCategory') IS NOT NULL                  
DROP TABLE #TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES_ForCategory

SELECT *  INTO [#TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES_ForCategory] FROM @AttributeFilterPRODUCTForAutosearch_CATEGORY 


EXEC('
INSERT INTO [##TEMP_AUTOCOMPLETE'+ @sessionId +'] (CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER)

SELECT TOP(5)  CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER FROM [#TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES_ForCategory] ')

EXEC('SELECT TOP(5) * FROM [##TEMP_AUTOCOMPLETE'+ @sessionId +'] ')

END

ELSE

BEGIN


EXEC('
SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER FROM 

(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''C'' AS LEVEL_TYPE,''1'' AS SORT_ORDER  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''C'' AS LEVEL_TYPE,''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''F'' AS LEVEL_TYPE,''3'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)T

')


END
  


END 
Else If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') and (select count(*) from @ATTRIBUTETABLE) = 0 )
BEGIN

 IF OBJECT_ID('TEMPDB..[##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']')


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

EXEC('(SELECT
 DISTINCT CATEGORY_NAME,CATEGORY_ID,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+','+@ATTRIBUTE_PRODUCTNAME_DISPLAY+',totalWeight
  into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] FROM 
  [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')

END


ELSE
BEGIN

EXEC('(SELECT
 DISTINCT CATEGORY_NAME,CATEGORY_ID,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+',totalWeight
  into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] FROM 
  [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')




END


EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_Family_PRODUCT'+ @sessionId +'] from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] order by PRODUCT_ID')


--  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
--WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+'')
--BEGIN

--EXEC('Drop table [PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+']')

--END
IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT_RESULT'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_Family_PRODUCT_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +']')

EXEC('
;with CTE AS 
(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''C'' AS LEVEL_TYPE,''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE ,''C'' AS LEVEL_TYPE,''2'' AS SORT_ORDER  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''F'' AS LEVEL_TYPE,''3'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)

SELECT TOP(5) * INTO [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+ @sessionId +'] FROM CTE 


')
DECLARE @VARFAM1_CategoryFamily NVARCHAR(MAX); 

declare @VARFAM1_Count_CategoryFamily int;

set @VARFAM1_CategoryFamily = 'SELECT @VARFAM1_Count =COUNT(*)  FROM [##TEMP_TEXTSEARCH_AUTOCOMPLETE_COUNT'+  @sessionId +'] '
EXECUTE sp_executeSQL @VARFAM1_CategoryFamily, N'@VARFAM1_Count INT OUTPUT', @VARFAM1_Count_CategoryFamily OUTPUT




IF(@VARFAM1_Count_CategoryFamily<5)
BEGIN

--EXEC('
--SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE INTO [##TEMP_AUTOCOMPLETE'+ @sessionId +'] FROM 

--(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''3'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
--)T

--')

EXEC('
SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER INTO [##TEMP_AUTOCOMPLETE'+ @sessionId +'] FROM 

(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''C'' AS LEVEL_TYPE,''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''C'' AS LEVEL_TYPE,''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''F'' AS LEVEL_TYPE,''3'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)T

')

  DECLARE @AttributeFilterPRODUCTForAutosearch_CATEGORYFamily TABLE( 

    CATEGORY_NAME VARCHAR(MAX), 

    RESULT NVARCHAR(max) ,
		
	TYPE NVARCHAR(10),

	LEVEL_TYPE NVARCHAR(10),

	SORT_ORDER NVARCHAR(10)

); 

SET @columns_Category = N'';
SELECT @columns_Category+=N''+Name 
FROM
(
    SELECT Search_Family_Attributes AS [Name],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id
  
) AS x;


IF @columns_Category<>''
begin
print '11'
SELECT @columns_Category+=N''+ ',' + Products
FROM
(
    SELECT Search_Attribute as [Products],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id 
  
) AS x;
END
ELSE
BEGIN
print '111'
SELECT @columns_Category+=N''+ Products
FROM
(
    SELECT Search_Attribute as [Products],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @Catalog_Id
  
) AS x;
END

print @columns_Category

DECLARE @ResultTableforAutosearch_ForCategoryFamily Nvarchar(50)='@@AttributeFilterPRODUCTForAutosearch_CATEGORYFamily'

DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Item 
FROM  SPLITSTRING(@columns, ',')  WHERE ITEM NOT IN (@ATTRIBUTE_NAME_ITEMNO)
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1 

WHILE @@FETCH_STATUS = 0 
BEGIN  

BEGIN


print '938'

print @Filter_Name1


if (select count(*) from @AttributeFilterPRODUCTForAutosearch_CATEGORYFamily) < 5
BEGIN

INSERT INTO @AttributeFilterPRODUCTForAutosearch_CATEGORYFamily (CATEGORY_NAME,RESULT,TYPE,LEVEL_TYPE,SORT_ORDER)

EXEC('

SELECT DISTINCT TOP(5) REPLACE(REPLACE('''+@Filter_Name1+''',''['', ''''),'']'','''')  AS CATEGORY_NAME,'+@Filter_Name1+' AS RESULT,''A'' AS TYPE,''A'' AS LEVEL_TYPE,''4'' AS SORT_ORDER  FROM '+@CATALOG_DETAILS+' WHERE '+@Filter_Name1+'  LIKE  ''%'+@SEARCH_TEXT + '%'' ')


END

ELSE

BEGIN

BREAK;

END

END

PRINT @Filter_Name1

FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1 
END  

CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER

IF OBJECT_ID('TEMPDB..#TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES_ForCategoryFamily') IS NOT NULL                  
DROP TABLE #TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES_ForCategoryFamily

SELECT *  INTO [#TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES_ForCategoryFamily] FROM @AttributeFilterPRODUCTForAutosearch_CATEGORYFamily 


EXEC('
INSERT INTO [##TEMP_AUTOCOMPLETE'+ @sessionId +'] (CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER)

SELECT TOP(5)  CATEGORY_NAME, RESULT,TYPE,SORT_ORDER FROM [#TEMP_ATTRIBUTETABLE_AUTOCOMPLETE_VALUES_ForCategoryFamily] ')

EXEC('SELECT TOP(5) * FROM [##TEMP_AUTOCOMPLETE'+ @sessionId +'] ')

END

ELSE

BEGIN


EXEC('
SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER FROM 

(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''C'' AS LEVEL_TYPE,''1'' AS SORT_ORDER  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''C'' AS LEVEL_TYPE,''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''F'' AS LEVEL_TYPE,''3'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)T

')


END



END




Else if ((select count(*) from @ATTRIBUTETABLE) > 0)
BEGIN


 SET NOCOUNT ON

 

	
  DECLARE @AttributeFilterPRODUCT TABLE( 
  CATEGORY_NAME NVARCHAR(max) ,

 CATEGORY_ID NVARCHAR(max), 

    FAMILY_ID INT NOT NULL, 

    FAMILY_NAME NVARCHAR(max) ,
		
	IMAGE_FILE NVARCHAR(max),

	PRODUCT_ID INT NOT NULL,

	ITEM_NUMBER NVARCHAR(max) ,

	PRODUCT_IMAGE_FILE NVARCHAR(max),

	totalWeight NVARCHAR(max)

); 

DECLARE  @Filtevalue   NVARCHAR(Max)
DECLARE  @Attribute_NAME  NVARCHAR(Max)
DECLARE  @STRING_VALUE  NVARCHAR(Max)


SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'

If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') )
BEGIN
EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] FROM 
  [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')
END
 ELSE IF (@CATEGORY_ID<>'' or @CATEGORY_ID IS NOT NULL)
  BEGIN

   EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] FROM 
 [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

  END
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+ @sessionId+'')

BEGIN
EXEC('DROP TABLE [FilterProductTable'+ @sessionId+']')
END



DECLARE @COUNT_FILTER int=0
DECLARE @ResultTable Nvarchar(50)='@AttributeFilterPRODUCT'



DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Attribute_NAME,STRING_VALUE FROM @ATTRIBUTETABLE
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE
WHILE @@FETCH_STATUS = 0  
BEGIN  



if (@COUNT_FILTER=0)
begin

 IF(@CATEGORY_ID<>'' and @FAMILY_ID<>'')
 BEGIN
 
 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 
 
exec ('select * into [FilterProductTable'+ @sessionId+'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')


 END

 ELSE IF(@CATEGORY_ID<>'')
BEGIN
 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

exec ('select * into [FilterProductTable'+ @sessionId+'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] where ['+@Attribute_NAME+'] in('''+@STRING_VALUE+''') ')

END

 ELSE
 BEGIN
  SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

 exec ('select * into [FilterProductTable'+ @sessionId+'] from [##TEMP_SEARCH'+ @sessionId +'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')
 END

SET @COUNT_FILTER=1

end

else
BEGIN
 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

Exec ('delete from [FilterProductTable'+ @sessionId+'] where ['+@Attribute_NAME+'] IS NULL')
Exec ('delete from [FilterProductTable'+ @sessionId+'] where ['+@Attribute_NAME+'] NOT IN ('''+@STRING_VALUE+''') ')

END



FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE

END  
CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER



IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+ @sessionId+'')
BEGIN


INSERT INTO @AttributeFilterPRODUCT (CATEGORY_NAME,CATEGORY_ID,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,ITEM_NUMBER,PRODUCT_IMAGE_FILE,totalWeight)


exec('SELECT DISTINCT CATEGORY_NAME,CATEGORY_ID,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,totalWeight FROM [FilterProductTable'+ @sessionId+'] ')


EXEC('DROP TABLE [FilterProductTable'+ @sessionId+']')
END

--select DISTINCT * from @AttributeFilterPRODUCT

IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PAGINATION'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION_Result'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PAGINATION_Result'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..#TEMP_ATTRIBUTETABLE_VALUES') IS NOT NULL                  
DROP TABLE #TEMP_ATTRIBUTETABLE_VALUES

   SELECT * INTO [#TEMP_ATTRIBUTETABLE_VALUES] FROM  @AttributeFilterPRODUCT 

    EXEC ('Select * ,ROW_NUMBER() OVER (
	ORDER BY   PRODUCT_ID 
   )  ROW_NUM INTO [##TEMP_PAGINATION'+ @sessionId+'] from [#TEMP_ATTRIBUTETABLE_VALUES] ORDER BY  PRODUCT_ID  ')


--select * ,ROW_NUMBER() OVER (
--	ORDER BY   PRODUCT_ID 
--   )  ROW_NUM INTO [##TEMP_PAGINATION'+ @sessionId +'] from @AttributeFilterPRODUCT  ORDER BY   PRODUCT_ID

--     IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
--WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+'')
--BEGIN
--EXEC('Drop table [PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+']')
--END

PRINT '1028'


EXEC('select * into [PRODUCTDISPALYDETAILS_AUTOCOMPLETE'+ @sessionId+'] from [##TEMP_PAGINATION'+ @sessionId +']')



EXEC('
SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE,LEVEL_TYPE,SORT_ORDER FROM 

(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''C'' AS LEVEL_TYPE,''1'' AS SORT_ORDER  from [##TEMP_PAGINATION'+ @sessionId+'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''C'' AS LEVEL_TYPE,''2'' AS SORT_ORDER   from [##TEMP_PAGINATION'+ @sessionId+'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

UNION ALL 

Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''F'' AS LEVEL_TYPE,''3'' AS SORT_ORDER   from [##TEMP_PAGINATION'+ @sessionId+'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
)T

')


END

--ELSE 

--BEGIN

--print '1484'


--EXEC('
--SELECT distinct top(5) CATEGORY_NAME, RESULT,TYPE,SORT_ORDER FROM 

--(Select distinct top(5) CATEGORY_NAME,CATEGORY_NAME AS RESULT, ''C'' AS TYPE,''1'' AS SORT_ORDER from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE CATEGORY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) CATEGORY_NAME, FAMILY_NAME  AS RESULT,''F'' AS TYPE,''2'' AS SORT_ORDER   from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE FAMILY_NAME  LIKE  ''%'+@SEARCH_TEXT + '%''

--UNION ALL 

--Select distinct top(5) FAMILY_NAME  AS FAMILY_NAME , ITEM_NUMBER AS RESULT,''P'' AS TYPE,''3'' AS SORT_ORDER  from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] WHERE ITEM_NUMBER  LIKE ''%'+@SEARCH_TEXT + '%''
--)T

--')


--END

END 

END














GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GET_FILTER_DETAILS]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GET_FILTER_DETAILS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_QSWS_GET_FILTER_DETAILS] AS' 
END
GO



ALTER PROCEDURE [dbo].[STP_QSWS_GET_FILTER_DETAILS](
@OPTION VARCHAR(100)='GET_FILTER_RESULT',
@SearchOption nvarchar(Max)='',
 @CATALOG_ID INT=123,
 @CATEGORY_ID varchar(MAX) ='CAT12590,CAT12594',
@USER_ID INT =0,
@FAMILY_ID varchar(max)='',
@SESSION_ID NVARCHAR(MAX) ='593e9a95-099e-5356-6e7d-717bad8fbe06',
@IS_CLONE Varchar(5)='0',
@ATTRIBUTETABLE AttributeParameterValuesValues READONLY)
AS
BEGIN

--SELECT * INTO ##TESTING_Attributes FROM @ATTRIBUTETABLE

--RETURN


 --SET @SESSION_ID = NEWID()
	-- PRINT @SESSION_ID

IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT'+@SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT'+@SESSION_ID+']')

IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_RESULT'+@SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_RESULT'+@SESSION_ID+']')

IF OBJECT_ID('TEMPDB..[##TEMP_FILTERPRODUCT_RESULT'+@SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_FILTERPRODUCT_RESULT'+@SESSION_ID+']')

IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT'+@SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_Family_PRODUCT'+@SESSION_ID+']')


 IF OBJECT_ID('TEMPDB..[##TEMP_ATTRIBUTE_VALUESDetails'+@SESSION_ID+']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_ATTRIBUTE_VALUESDetails'+@SESSION_ID+']')

DECLARE @CATALOG_DETAILS NVARCHAR(MAX)

SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'

DECLARE @ATTRIBUTE_NAME_ITEMNO VARCHAR(MAX)=''
select @ATTRIBUTE_NAME_ITEMNO =  CAPTION from TB_ATTRIBUTE WHERE ATTRIBUTE_TYPE=1 AND ATTRIBUTE_ID=1 AND PUBLISH2WEB = 1 AND FLAG_RECYCLE = 'A'

Set @ATTRIBUTE_NAME_ITEMNO = '['+@ATTRIBUTE_NAME_ITEMNO+']'

Declare @ATTRIBUTE_NAME_DISPLAY NVarchar(max)=''
Select @ATTRIBUTE_NAME_DISPLAY=Search_Family_Attributes from QSWS_CONFIGRATION_SETTINGS where Catalog_Id=@CATALOG_ID
set @ATTRIBUTE_NAME_DISPLAY=@ATTRIBUTE_NAME_DISPLAY

print '47'
print @ATTRIBUTE_NAME_DISPLAY


IF(@OPTION='GET_FILTER_RESULT' OR @OPTION='GET_FILTERSearch_RESULT' )
BEGIN



if(@SearchOption != 'null' and @SearchOption != 'undefined' and @SearchOption !='' and @CATEGORY_ID='' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0)
BEGIN
print 'ee'



 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_Distinict'+@SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_Distinict'+@SESSION_ID+']')

 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_Distinict_Values'+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+']')


EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,IMAGE_FILE
 into  [##TEMP_PRODUCT_RESULT'+@SESSION_ID+'] FROM 
[PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''')')


print '88'

--EXEC('
--select * ,ROW_NUMBER() OVER (
--	ORDER BY FAMILY_ID
--   ) ROW_NUM  from [##TEMP_PRODUCT_RESULT'+@SESSION_ID+'] order by FAMILY_NAME'
   
EXEC('select distinct TPR.FAMILY_ID as CATEGORY_ID,TPR.FAMILY_NAME as CATEGORY_NAME, TPR.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=TPR.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,TCF.SORT_ORDER,TPR.FAMILY_IS_CLONE AS IS_CLONE
  into [##TEMP_PRODUCT_Distinict'+@SESSION_ID+'] 
from [##TEMP_PRODUCT_RESULT'+@SESSION_ID+'] TPR
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TPR.FAMILY_ID
  join tb_catalog_family TCF ON TCF.FAMILY_ID = TPR.FAMILY_ID  and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''
')


EXEC('select *,ROW_NUMBER() OVER(ORDER BY CATEGORY_NAME ASC) AS Row# into  [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+'] from [##TEMP_PRODUCT_Distinict'+@SESSION_ID+']')


EXEC('select * from [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+'] ORDER BY SORT_ORDER')


END
ELSE if(@SearchOption != 'null' and @SearchOption != 'undefined' and @SearchOption !='' and @CATEGORY_ID='' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0)

BEGIN

print '121'

EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,IMAGE_FILE
 into  [##TEMP_PRODUCT_RESULT'+@SESSION_ID+'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''')')


EXEC('
select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   ) ROW_NUM INTO [##TEMP_PRODUCT_Distinict'+@SESSION_ID+'] from [##TEMP_PRODUCT_RESULT'+@SESSION_ID+'] order by FAMILY_NAME')

   
EXEC('select distinct TPR.FAMILY_ID as CATEGORY_ID,TPR.FAMILY_NAME as CATEGORY_NAME,ROW_NUMBER() OVER(ORDER BY TPR.FAMILY_NAME ASC) AS Row# , TPR.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=TPR.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,TCF.SORT_ORDER,TPR.FAMILY_IS_CLONE AS IS_CLONE
 into [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+'] from [##TEMP_PRODUCT_Distinct'+@SESSION_ID+'] TPR
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TPR.FAMILY_ID
  join tb_catalog_family TCF ON TCF.FAMILY_ID = TP.FAMILY_ID  and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''
 ')


--EXEC('select *,ROW_NUMBER() OVER(ORDER BY AAAAFAMILY_NAME ASC) AS Row# into  [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+'] from [##TEMP_PRODUCT_Distinict'+@SESSION_ID+']')


EXEC('select * from [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+'] ORDER BY SORT_ORDER')



END



--EXEC('(SELECT
-- DISTINCT FAMILY_ID,FAMILY_NAME
-- into  [##TEMP_PRODUCT_RESULT'+@SESSION_ID+'] FROM 
-- '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
--AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
--	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
--	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
--		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
--		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
--		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
--			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
--			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
--	   )


--AND CATALOG_ID='''+ @CATALOG_ID +''')')


--EXEC('select * ,ROW_NUMBER() OVER (
--	ORDER BY FAMILY_ID
--   ) ROW_NUM from [##TEMP_PRODUCT_RESULT'+ @SESSION_ID+'] order by FAMILY_NAME')





 END



 IF(@CATEGORY_ID<>'' and (@FAMILY_ID IS NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0 and (@SearchOption = 'null' OR  @SearchOption = 'undefined' OR @SearchOption ='') )
BEGIN
print '83'

  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @SESSION_ID+'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @SESSION_ID+']')
END


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN



EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+',FAMILY_IS_CLONE
 into [##TEMP_FILTERPRODUCT_RESULT'+@SESSION_ID+'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

END

ELSE

BEGIN

EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE
 into [##TEMP_FILTERPRODUCT_RESULT'+@SESSION_ID+'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')


END

EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_PRODUCT'+@SESSION_ID+'] from [##TEMP_FILTERPRODUCT_RESULT'+@SESSION_ID+'] order by PRODUCT_ID')


 EXEC('select * into [PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] from [##TEMP_PRODUCT'+@SESSION_ID+']')


 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_Distinict'+@SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_Distinict'+@SESSION_ID+']')

 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_Distinict_Values'+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+']')


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

print '278'

EXEC('SELECT * INTO  [##TEMP_PRODUCT_Distinict'+@SESSION_ID+'] FROM ( select distinct  TP.FAMILY_ID as CATEGORY_ID,TP.FAMILY_NAME as CATEGORY_NAME , TP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=TP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
'+@ATTRIBUTE_NAME_DISPLAY+',TCF.SORT_ORDER,TP.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
        partition by TP.ROW_NUM
        order by TCF.SORT_ORDER
    )  as rn from [##TEMP_PRODUCT'+@SESSION_ID+'] TP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TP.FAMILY_ID
 join tb_catalog_family TCF ON TCF.FAMILY_ID = TP.FAMILY_ID  and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''  

  )AS T
  Where rn=1

')



END


ELSE

BEGIN

EXEC('SELECT * INTO  [##TEMP_PRODUCT_Distinict'+@SESSION_ID+'] FROM (select distinct  TP.FAMILY_ID as CATEGORY_ID,TP.FAMILY_NAME as CATEGORY_NAME, TP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=TP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,TCF.SORT_ORDER,TP.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
         partition by TP.ROW_NUM
        order by TCF.SORT_ORDER
    )  as rn
from [##TEMP_PRODUCT'+@SESSION_ID+'] TP
  LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TP.FAMILY_ID
   join tb_catalog_family TCF ON TCF.FAMILY_ID = TP.FAMILY_ID and TCF.CATALOG_ID ='''+ @CATALOG_ID +''' 

   )AS T
  Where rn=1
')



END



EXEC('select *  from [##TEMP_PRODUCT_Distinict'+@SESSION_ID+'] ORDER BY SORT_ORDER')


--EXEC('select FAMILY_ID,FAMILY_NAME as AAAAFAMILY_NAME, IMAGE_FILE as AAAIMAGE_FILE,ROW_NUMBER() OVER(ORDER BY AAAAFAMILY_NAME ASC) AS Row#  from [##TEMP_PRODUCT_Distinict'+@SESSION_ID+']')


EXEC('select *,ROW_NUMBER() OVER(ORDER BY CATEGORY_NAME ASC) AS Row# into  [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+'] from [##TEMP_PRODUCT_Distinict'+@SESSION_ID+'] ')

--EXEC('select * from [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+']  where Row# >= ('''+@PAGEPERCOUNT+'*('''+@PAGENO+''' - 1))+1 AND Row# <= ('''+@PAGEPERCOUNT+' *('''+@PAGENO+''')) ')

EXEC('select * from [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+'] ORDER BY SORT_ORDER')



END 
Else If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') and (select count(*) from @ATTRIBUTETABLE) = 0 and (@SearchOption = 'null' OR @SearchOption = 'undefined' OR @SearchOption =''))
BEGIN
print'137'

  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @SESSION_ID+'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @SESSION_ID+']')
END

IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE,'+@ATTRIBUTE_NAME_DISPLAY+'
  into [##TEMP_FILTERPRODUCT_RESULT'+ @SESSION_ID+'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')

END


ELSE


BEGIN


EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE
  into [##TEMP_FILTERPRODUCT_RESULT'+ @SESSION_ID+'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')

END


EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_Family_PRODUCT'+ @SESSION_ID+'] from [##TEMP_FILTERPRODUCT_RESULT'+ @SESSION_ID+']  order by PRODUCT_ID')


 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+']')

 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_FAMILY_Distinict_Result '+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_FAMILY_Distinict_Result '+ @SESSION_ID+']')


 EXEC('select * into [PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] from [##TEMP_Family_PRODUCT'+ @SESSION_ID+']')

 
IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN




 EXEC('SELECT *  into [##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+'] FROM (select distinct TFP.FAMILY_ID as CATEGORY_ID,TFP.FAMILY_NAME as CATEGORY_NAME, TFP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID ,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=TFP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,TCF.SORT_ORDER,TFP.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
        partition by TFP.ROW_NUM
        order by TCF.SORT_ORDER
    )  as rn,
 '+@ATTRIBUTE_NAME_DISPLAY+'  from [##TEMP_Family_PRODUCT'+ @SESSION_ID+'] TFP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TFP.FAMILY_ID
 join tb_catalog_family TCF ON TCF.FAMILY_ID = TFP.FAMILY_ID and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''
  )AS T
   Where rn=1
 ')

 END

 ELSE


 BEGIN

  EXEC('SELECT *  into [##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+'] FROM(select distinct  TFP.FAMILY_ID as CATEGORY_ID,TFP.FAMILY_NAME as CATEGORY_NAME, TFP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
  CASE WHEN TSF.SUBFAMILY_ID=TFP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,TCF.SORT_ORDER,TFP.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
        partition by TFP.ROW_NUM
        order by TCF.SORT_ORDER
    )  as rn
    from [##TEMP_Family_PRODUCT'+ @SESSION_ID+'] TFP
	 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TFP.FAMILY_ID
	 join tb_catalog_family TCF ON TCF.FAMILY_ID = TFP.FAMILY_ID and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''  
	  )AS T
   Where rn=1
	')

 END

EXEC('select *,ROW_NUMBER() OVER(ORDER BY CATEGORY_NAME ASC) AS Row# into [##TEMP_PRODUCT_FAMILY_Distinict_Result '+ @SESSION_ID+'] from [##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+']')


EXEC('select * from [##TEMP_PRODUCT_FAMILY_Distinict_Result '+ @SESSION_ID+']  ORDER BY SORT_ORDER ')


END


Else if ((select count(*) from @ATTRIBUTETABLE) > 0 and (@SearchOption = 'null' OR @SearchOption = 'undefined' OR @SearchOption =''))
BEGIN

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @SESSION_ID+'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @SESSION_ID+']')
END

 SET NOCOUNT ON

 
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+ @SESSION_ID+'')
BEGIN
EXEC('Drop table [FilterProductTable'+ @SESSION_ID+']')
END

	
 DECLARE @AttributeFilterPRODUCT TABLE( 

    FAMILY_ID INT NOT NULL, 

    FAMILY_NAME NVARCHAR(max) ,
		
	IMAGE_FILE NVARCHAR(max),

	PRODUCT_ID INT NULL,

	CATEGORY_ID NVARCHAR(max),

	ITEM_NUMBER NVARCHAR(max) ,

	PRODUCT_IMAGE_FILE NVARCHAR(max),

	FAMILY_IS_CLONE VARCHAR(2)

); 

DECLARE  @Filtevalue   NVARCHAR(Max)
DECLARE  @Attribute_NAME  NVARCHAR(Max)
DECLARE  @STRING_VALUE  NVARCHAR(Max)


SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'

If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') )
BEGIN
EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')
END
 ELSE IF (@CATEGORY_ID<>'' or @CATEGORY_ID IS NOT NULL)
  BEGIN

   EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

  END

DECLARE @COUNT int=0

DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Attribute_NAME,STRING_VALUE FROM @ATTRIBUTETABLE
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE
WHILE @@FETCH_STATUS = 0  
BEGIN  


if (@COUNT=0)
begin

 IF(@CATEGORY_ID<>'' and @FAMILY_ID<>'')
 BEGIN

 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 
  --print @STRING_VALUE

-- print ('select * into [FilterProductTable'+ @SESSION_ID+'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

 exec ('select * into [FilterProductTable'+ @SESSION_ID+'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] in  ('''+@STRING_VALUE+''') ')
--exec ('select * into [FilterProductTable'+ @SESSION_ID+'] from [##TEMP_ATTRIBUTE_VALUESDetails '+ @SESSION_ID+'] where ['+@Attribute_NAME+'] in ('+@STRING_VALUE+') ')



 END

 ELSE IF(@CATEGORY_ID<>'')
BEGIN
 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

exec ('select * into [FilterProductTable'+ @SESSION_ID+'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

END

 ELSE
 BEGIN
  SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

 exec ('select * into [FilterProductTable'+ @SESSION_ID+'] from '+@CATALOG_DETAILS+' where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

 END

SET @COUNT=1

end

else
BEGIN
 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

Exec ('delete from [FilterProductTable'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] IS NULL')
Exec ('delete from [FilterProductTable'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] NOT IN  ('''+@STRING_VALUE+''') ')


END



FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE

END  
CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+ @SESSION_ID+'')

BEGIN

INSERT INTO @AttributeFilterPRODUCT(FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,CATEGORY_ID,ITEM_NUMBER,PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE)

EXEC('SELECT DISTINCT FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,CATEGORY_ID,'+@ATTRIBUTE_NAME_ITEMNO+' as ITEM_NUMBER,
Product_Image AS PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE FROM [FilterProductTable'+ @SESSION_ID+'] ')

EXEC('DROP TABLE [FilterProductTable'+ @SESSION_ID+']')
END

--select  * from @AttributeFilterPRODUCT

IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION'+ @SESSION_ID+']') IS NOT NULL                            

EXEC('DROP TABLE [##TEMP_PAGINATION'+ @SESSION_ID+']')

IF OBJECT_ID('TEMPDB..#TEMP_ATTRIBUTETABLE_VALUES') IS NOT NULL                  
DROP TABLE #TEMP_ATTRIBUTETABLE_VALUES

   SELECT * INTO [#TEMP_ATTRIBUTETABLE_VALUES] FROM  @AttributeFilterPRODUCT 

    EXEC ('Select TPV.*,TCF.SORT_ORDER ,ROW_NUMBER() OVER (
	ORDER BY   TPV.PRODUCT_ID 
   )  ROW_NUM INTO [##TEMP_PAGINATION'+ @SESSION_ID+'] from [#TEMP_ATTRIBUTETABLE_VALUES] TPV 
   join TB_CATALOG_FAMILY TCF ON TCF.FAMILY_ID = TPV.FAMILY_ID  and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''
   ORDER BY TCF.SORT_ORDER  ')

   IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict'+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict'+ @SESSION_ID+']')
 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+']')

  EXEC('select * into [PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] from [##TEMP_PAGINATION'+ @SESSION_ID+']  ORDER BY SORT_ORDER')

EXEC('select distinct FAMILY_ID,FAMILY_NAME, IMAGE_FILE,CATEGORY_ID,SORT_ORDER,FAMILY_IS_CLONE  into [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict'+ @SESSION_ID+'] from [##TEMP_PAGINATION'+ @SESSION_ID+']')

EXEC('select *,ROW_NUMBER() OVER(ORDER BY FAMILY_NAME ASC) AS Row# into  [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+'] from [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict'+ @SESSION_ID+']  ORDER BY SORT_ORDER')

IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

 EXEC ('SELECT * FROM(select distinct Temp.FAMILY_ID as CATEGORY_ID,Temp.FAMILY_NAME as CATEGORY_NAME, Temp.IMAGE_FILE, TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
 CASE WHEN TSF.SUBFAMILY_ID=Temp.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG, '+@ATTRIBUTE_NAME_DISPLAY+', TCF.SORT_ORDER,Temp.FAMILY_IS_CLONE as IS_CLONE,
 row_number() over
    (
        partition by Temp.ROW#
        order by TCF.SORT_ORDER
    )  as rn
 from [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+'] Temp join  '+@CATALOG_DETAILS+' QSWS on Temp.FAMILY_ID=QSWS.FAMILY_ID 
  LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =Temp.FAMILY_ID
  join tb_catalog_family TCF ON TCF.FAMILY_ID = Temp.FAMILY_ID  and TCF.CATALOG_ID ='''+ @CATALOG_ID +''' 
   )AS T Where rn=1
 ')

END

ELSE 


BEGIN

 EXEC ('SELECT * FROM(select distinct  Temp.FAMILY_ID as CATEGORY_ID,Temp.FAMILY_NAME as CATEGORY_NAME, Temp.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
  CASE WHEN TSF.SUBFAMILY_ID=Temp.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG, TCF.SORT_ORDER,Temp.FAMILY_IS_CLONE as IS_CLONE,
   row_number() over
    (
        partition by Temp.ROW#
        order by TCF.SORT_ORDER
    )  as rn
  from [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+'] Temp join  '+@CATALOG_DETAILS+' QSWS on Temp.FAMILY_ID=QSWS.FAMILY_ID 
   LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =Temp.FAMILY_ID
   join tb_catalog_family TCF ON TCF.FAMILY_ID = Temp.FAMILY_ID  and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''
   )AS T Where rn=1 
   ')

END

END



 IF(@CATEGORY_ID<>'' and (@FAMILY_ID IS NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0 and (@SearchOption != 'null' and @SearchOption != 'undefined' and @SearchOption !='') )
BEGIN
print '635'

 
IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

print '651'

print @ATTRIBUTE_NAME_ITEMNO
print @ATTRIBUTE_NAME_DISPLAY

print '631'


EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,PRODUCT_ID ,IMAGE_FILE,'''+@ATTRIBUTE_NAME_ITEMNO+''' as ITEM_NUMBER, PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+',FAMILY_IS_CLONE
 into [##TEMP_FILTERPRODUCT_RESULT'+@SESSION_ID+'] FROM 
[PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')


END


ELSE

BEGIN


print '633'

EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,PRODUCT_ID ,IMAGE_FILE,'''+@ATTRIBUTE_NAME_ITEMNO+''' as ITEM_NUMBER, PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE
 into [##TEMP_FILTERPRODUCT_RESULT'+@SESSION_ID+'] FROM 
[PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

END


print '680'

EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_PRODUCT'+@SESSION_ID+'] from [##TEMP_FILTERPRODUCT_RESULT'+@SESSION_ID+'] order by PRODUCT_ID')


   print '502'



 --EXEC('select * into [PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] from [##TEMP_PRODUCT'+@SESSION_ID+']')


 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_Distinict'+@SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_Distinict'+@SESSION_ID+']')

 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_Distinict_Values'+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+']')




IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


EXEC('SELECT * into [##TEMP_PRODUCT_Distinict'+@SESSION_ID+'] FROM(select distinct TP.FAMILY_ID as CATEGORY_ID,TP.FAMILY_NAME as CATEGORY_NAME, TP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=TP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
'+@ATTRIBUTE_NAME_DISPLAY+', TCF.SORT_ORDER,TP.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
        partition by TP.ROW_NUM
        order by TCF.SORT_ORDER
    )  as rn from [##TEMP_PRODUCT'+@SESSION_ID+'] TP
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TP.FAMILY_ID
join tb_catalog_family TCF ON TCF.FAMILY_ID = TP.FAMILY_ID  and TCF.CATALOG_ID ='''+ @CATALOG_ID +''' 
 )AS T Where rn=1 
')

END

ELSE

BEGIN
EXEC('SELECT * into [##TEMP_PRODUCT_Distinict'+@SESSION_ID+'] FROM(select distinct  TP.FAMILY_ID as CATEGORY_ID,TP.FAMILY_NAME as CATEGORY_NAME, TP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=TP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG, TCF.SORT_ORDER,TP.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
        partition by TP.ROW_NUM
        order by TCF.SORT_ORDER
    )  as rn
  from [##TEMP_PRODUCT'+@SESSION_ID+'] TP
  LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TP.FAMILY_ID
  join tb_catalog_family TCF ON TCF.FAMILY_ID = TP.FAMILY_ID  and TCF.CATALOG_ID ='''+ @CATALOG_ID +''' 
   )AS T Where rn=1 
  ')


END



 EXEC('select *  from [##TEMP_PRODUCT_Distinict'+@SESSION_ID+']  ORDER BY SORT_ORDER')


EXEC('select *,ROW_NUMBER() OVER(ORDER BY CATEGORY_NAME ASC) AS Row# into  [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+'] from [##TEMP_PRODUCT_Distinict'+@SESSION_ID+']')

--EXEC('select * from [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+']  where Row# >= ('''+@PAGEPERCOUNT+'*('''+@PAGENO+''' - 1))+1 AND Row# <= ('''+@PAGEPERCOUNT+' *('''+@PAGENO+''')) ')

EXEC('select * from [##TEMP_PRODUCT_Distinict_Values'+@SESSION_ID+'] ORDER BY SORT_ORDER ')



END 
Else If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') and (select count(*) from @ATTRIBUTETABLE) = 0 and (@SearchOption != 'null' and @SearchOption != 'undefined' and @SearchOption !=''))
BEGIN

print '742'

IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN



EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,PRODUCT_ID,IMAGE_FILE,'''+@ATTRIBUTE_NAME_ITEMNO+'''  as ITEM_NUMBER,PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+',FAMILY_IS_CLONE
  into [##TEMP_FILTERPRODUCT_RESULT'+ @SESSION_ID+'] FROM 
[PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')

END


ELSE


BEGIN



EXEC('(SELECT
 DISTINCT FAMILY_ID,FAMILY_NAME,CATEGORY_ID,PRODUCT_ID, IMAGE_FILE,'''+@ATTRIBUTE_NAME_ITEMNO+'''  as ITEM_NUMBER,PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE
  into [##TEMP_FILTERPRODUCT_RESULT'+ @SESSION_ID+'] FROM 
[PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')


END


EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_Family_PRODUCT'+ @SESSION_ID+'] from [##TEMP_FILTERPRODUCT_RESULT'+ @SESSION_ID+']  order by PRODUCT_ID')


 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+']')

 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_FAMILY_Distinict_Result '+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_FAMILY_Distinict_Result '+ @SESSION_ID+']')


 --EXEC('select * into [PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] from [##TEMP_Family_PRODUCT'+ @SESSION_ID+']')
 
 
IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

 EXEC('SELECT * into [##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+'] FROM(select distinct  TFP.FAMILY_ID as CATEGORY_ID,TFP.FAMILY_NAME as CATEGORY_NAME, TFP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID ,''F'' AS TYPE,
 CASE WHEN TSF.SUBFAMILY_ID=TFP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 '+@ATTRIBUTE_NAME_DISPLAY+', TCF.SORT_ORDER,TFP.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
        partition by TFP.ROW_NUM
        order by TCF.SORT_ORDER
    )  as rn
   from [##TEMP_Family_PRODUCT'+ @SESSION_ID+'] TFP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TFP.FAMILY_ID
 join tb_catalog_family TCF ON TCF.FAMILY_ID = TFP.FAMILY_ID and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''
  )AS T Where rn=1
 ')

 END


 ELSE

 BEGIN

  EXEC('SELECT * into [##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+'] FROMselect distinct  TFP.FAMILY_ID as CATEGORY_ID,TFP.FAMILY_NAME as CATEGORY_NAME, TFP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
  CASE WHEN TSF.SUBFAMILY_ID=TFP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG, TCF.SORT_ORDER,TFP.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
        partition by TFP.ROW_NUM
        order by TCF.SORT_ORDER
    )  as rn
   from [##TEMP_Family_PRODUCT'+ @SESSION_ID+'] TFP
   LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TFP.FAMILY_ID 
   join tb_catalog_family TCF ON TCF.FAMILY_ID = TfP.FAMILY_ID and TCF.CATALOG_ID ='''+ @CATALOG_ID +''' 
   )AS T Where rn=1
  ')

 END


EXEC('select *,ROW_NUMBER() OVER(ORDER BY CATEGORY_NAME ASC) AS Row# into [##TEMP_PRODUCT_FAMILY_Distinict_Result '+ @SESSION_ID+'] from [##TEMP_PRODUCT_FAMILY_Distinict '+ @SESSION_ID+']')


EXEC('select * from [##TEMP_PRODUCT_FAMILY_Distinict_Result '+ @SESSION_ID+']  ORDER BY SORT_ORDER ')



END


Else if ((select count(*) from @ATTRIBUTETABLE) > 0 and (@SearchOption != 'null' and @SearchOption != 'undefined' and @SearchOption !=''))
BEGIN



 SET NOCOUNT ON

	
  DECLARE @AttributeFilterSearchPRODUCT TABLE( 

    FAMILY_ID INT NOT NULL, 

    FAMILY_NAME NVARCHAR(max) ,
		
	IMAGE_FILE NVARCHAR(max),

	CATEGORY_ID NVARCHAR(max),

	PRODUCT_ID INT NULL,

	ITEM_NUMBER NVARCHAR(max) ,

	PRODUCT_IMAGE_FILE NVARCHAR(max),

	FAMILY_IS_CLONE VARCHAR(2)


); 

DECLARE  @Filtevalue_Search   NVARCHAR(Max)
DECLARE  @Attribute_NAME_Search  NVARCHAR(Max)
DECLARE  @STRING_VALUE_Search NVARCHAR(Max)


SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'

If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') )
BEGIN
EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')
END
 ELSE IF (@CATEGORY_ID<>'' or @CATEGORY_ID IS NOT NULL)
  BEGIN

   EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] FROM 
 '+@CATALOG_DETAILS+' WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

  END

   IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+@SESSION_ID+'')
BEGIN

EXEC('Drop table [FilterProductTable'+@SESSION_ID+']')
END


DECLARE @COUNT_Search int=0

DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Attribute_NAME,STRING_VALUE FROM @ATTRIBUTETABLE
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE
WHILE @@FETCH_STATUS = 0  
BEGIN  


if (@COUNT_Search=0)
begin

 IF(@CATEGORY_ID<>'' OR  @FAMILY_ID<>'')
 BEGIN

 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 
  --print @STRING_VALUE

-- print ('select * into [FilterProductTable'+ @SESSION_ID+'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

 exec ('select * into [FilterProductTable'+ @SESSION_ID+'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] in  ('''+@STRING_VALUE+''') ')
--exec ('select * into [FilterProductTable'+ @SESSION_ID+'] from [##TEMP_ATTRIBUTE_VALUESDetails '+ @SESSION_ID+'] where ['+@Attribute_NAME+'] in ('+@STRING_VALUE+') ')



 END

 ELSE IF(@CATEGORY_ID<>'')
BEGIN
 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

exec ('select * into [FilterProductTable'+ @SESSION_ID+'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

END

 ELSE
 BEGIN
  SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

 exec ('select * into [FilterProductTable'+ @SESSION_ID+'] from '+@CATALOG_DETAILS+' where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

 END

SET @COUNT_Search=1

end

else
BEGIN
 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

Exec ('delete from [FilterProductTable'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] IS NULL')
Exec ('delete from [FilterProductTable'+ @SESSION_ID+'] where ['+@Attribute_NAME+'] NOT IN  ('''+@STRING_VALUE+''') ')


END



FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE

END  
CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+ @SESSION_ID+'')

BEGIN


INSERT INTO @AttributeFilterSearchPRODUCT(FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,CATEGORY_ID,ITEM_NUMBER,PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE )

EXEC('SELECT DISTINCT FAMILY_ID,FAMILY_NAME,PRODUCT_ID,FAMILY_IMAGE AS IMAGE_FILE,CATEGORY_ID,'+@ATTRIBUTE_NAME_ITEMNO+' as ITEM_NUMBER,FAMILY_IMAGE AS PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE  FROM [FilterProductTable'+ @SESSION_ID+'] ')


EXEC('DROP TABLE [FilterProductTable'+ @SESSION_ID+']')
END

--select  * from @AttributeFilterPRODUCT

IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION'+ @SESSION_ID+']') IS NOT NULL                            

EXEC('DROP TABLE [##TEMP_PAGINATION'+ @SESSION_ID+']')

IF OBJECT_ID('TEMPDB..#TEMP_ATTRIBUTETABLE_Search_VALUES') IS NOT NULL                  
DROP TABLE #TEMP_ATTRIBUTETABLE_Search_VALUES

   SELECT * INTO [#TEMP_ATTRIBUTETABLE_Search_VALUES] FROM  @AttributeFilterSearchPRODUCT 

 --   EXEC ('Select * ,ROW_NUMBER() OVER (
	--ORDER BY   PRODUCT_ID 
 --  )  ROW_NUM INTO [##TEMP_PAGINATION'+ @SESSION_ID+'] from [#TEMP_ATTRIBUTETABLE_Search_VALUES] ORDER BY  PRODUCT_ID  ')


    EXEC ('Select TPV.*,TCF.SORT_ORDER ,ROW_NUMBER() OVER (
	ORDER BY   TPV.PRODUCT_ID 
   )  ROW_NUM INTO [##TEMP_PAGINATION'+ @SESSION_ID+'] from [#TEMP_ATTRIBUTETABLE_Search_VALUES] TPV 
   join TB_CATALOG_FAMILY TCF ON TCF.FAMILY_ID = TPV.FAMILY_ID  and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''
   ORDER BY TCF.SORT_ORDER  ')

   IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict'+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict'+ @SESSION_ID+']')
 IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+']')

  --EXEC('select * into [PRODUCTDISPALYDETAILS'+ @SESSION_ID+'] from [##TEMP_PAGINATION'+ @SESSION_ID+']')


EXEC('select distinct FAMILY_ID as CATEGORY_ID,FAMILY_NAME as CATEGORY_NAME,  IMAGE_FILE,CATEGORY_ID as CATEGORYSEARCH_ID,FAMILY_ID,FAMILY_NAME,FAMILY_IS_CLONE  into [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict'+ @SESSION_ID+'] from [##TEMP_PAGINATION'+ @SESSION_ID+']')

EXEC('select *,ROW_NUMBER() OVER(ORDER BY CATEGORY_NAME ASC) AS Row# into  [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+'] from [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict'+ @SESSION_ID+']')


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


 EXEC ('SELECT * FROM(select distinct Temp.FAMILY_ID as CATEGORY_ID,Temp.FAMILY_NAME as CATEGORY_NAME, Temp.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE ,
 CASE WHEN TSF.SUBFAMILY_ID=Temp.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 '+@ATTRIBUTE_NAME_DISPLAY+', TCF.SORT_ORDER,Temp.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
        partition by Temp.ROW#
        order by TCF.SORT_ORDER
    )  as rn from [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+'] Temp join  '+@CATALOG_DETAILS+' QSWS on Temp.FAMILY_ID=QSWS.FAMILY_ID
  LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =Temp.FAMILY_ID
  join tb_catalog_family TCF ON TCF.FAMILY_ID = Temp.FAMILY_ID and TCF.CATALOG_ID ='''+ @CATALOG_ID +''' 
   )AS T Where rn=1
  ')

END


ELSE 

BEGIN

 EXEC ('SELECT * FROM(select distinct Temp.FAMILY_ID as CATEGORY_ID,Temp.FAMILY_NAME as CATEGORY_NAME,Temp.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE 
  CASE WHEN TSF.SUBFAMILY_ID=Temp.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG, TCF.SORT_ORDER,Temp.FAMILY_IS_CLONE AS IS_CLONE,
 row_number() over
    (
        partition by Temp.ROW#
        order by TCF.SORT_ORDER
    )  as rn 
 from [##TEMP_PRODUCT_FAMILY__ATTRIBUT_Distinict_Result'+ @SESSION_ID+'] Temp join  '+@CATALOG_DETAILS+' QSWS on Temp.FAMILY_ID=QSWS.FAMILY_ID 
   LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =Temp.FAMILY_ID
   join tb_catalog_family TCF ON TCF.FAMILY_ID = Temp.FAMILY_ID and TCF.CATALOG_ID ='''+ @CATALOG_ID +'''  
    )AS T Where rn=1
 ')


END

END



END



GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GET_TEXTSEARCH_RESULT_REFINEFILTER_LEVEL]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GET_TEXTSEARCH_RESULT_REFINEFILTER_LEVEL]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_QSWS_GET_TEXTSEARCH_RESULT_REFINEFILTER_LEVEL] AS' 
END
GO






ALTER  PROCEDURE [dbo].[STP_QSWS_GET_TEXTSEARCH_RESULT_REFINEFILTER_LEVEL] (
 @SEARCH_TEXT VARCHAR(MAX)='men',
 @USER_ID INT =8,
 @CATALOG_ID INT =10,
 @CATEGORY_ID varchar(MAX) ='',
 @FAMILY_ID varchar(max)='',
 @sessionId nvarchar(max)='7d80f3db-2f91-e01f-5cf8-401ae8a7f39b',
 @ATTRIBUTETABLE AttributeParameterValuesValues READONLY

 )as


 
BEGIN

--Select * into ##newtesting from @ATTRIBUTETABLE

--return

 --SET @sessionId = NEWID()
	-- PRINT @sessionId



IF OBJECT_ID('TEMPDB..#SEARCHVALUE') IS NOT NULL    
EXEC('DROP TABLE #SEARCHVALUE')

--  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
--WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId+'')
--BEGIN
--PRINT '41'
--EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId+']')

--END

DECLARE @SEARCHCOUNT int 

SET @SEARCHCOUNT= (SELECT count (Item) FROM  SPLITSTRING(@SEARCH_TEXT, '~'))

print '50'
PRINT @SEARCHCOUNT

SELECT Item, IDENTITY( INT, 1, 1) AS NEW_ID  into #SEARCHVALUE FROM  SPLITSTRING(@SEARCH_TEXT, '~')


DECLARE @Counter INT 
SET @Counter=1
WHILE ( @Counter <= @SEARCHCOUNT)

BEGIN
print 'count1'
print @SEARCHCOUNT
IF ( @Counter=1)
BEGIN

SET @SEARCH_TEXT=(select item from #SEARCHVALUE where NEW_ID=@Counter)

PRINT @SEARCH_TEXT





BEGIN  
declare @SEARCH_TEXT_TEMP varchar(100)
declare @SEARCH_TEXT_REPLACE varchar(100)
declare @SEARCH_TEXT_COUNT int
set @SEARCH_TEXT = REPLACE(@SEARCH_TEXT,'  ',' ')
set @SEARCH_TEXT_TEMP = @SEARCH_TEXT
SET @SEARCH_TEXT_TEMP = REPLACE(@SEARCH_TEXT_TEMP , ' ', '~')
set @SEARCH_TEXT_REPLACE = replace(@SEARCH_TEXT_TEMP,'~', '')
set @SEARCH_TEXT_COUNT = ((len(@SEARCH_TEXT_TEMP) - len(@SEARCH_TEXT_REPLACE)) / len('~') +1)

print '84'

print @SEARCH_TEXT_TEMP

DECLARE @CATALOG_DETAILS NVARCHAR(MAX)

SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'


IF OBJECT_ID('TEMPDB..[##TEMP_SEARCH'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_SEARCH'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_FULL_SEARCH'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_FULL_SEARCH'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_SPLIT_SEARCH'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_SPLIT_SEARCH'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##SEARCH_FILTER_RESULT'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##SEARCH_FILTER_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##SEARCH_FINAL_RESULT'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##SEARCH_FINAL_RESULT'+ @sessionId +']')



IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_RESULT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_Family_PRODUCT'+ @sessionId +']')


 IF OBJECT_ID('TEMPDB..[##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +']') IS NOT NULL                            
  EXEC('DROP TABLE [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +']')

  IF OBJECT_ID('TEMPDB..[##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']')


 	IF OBJECT_ID('TEMPDB..[##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +']') IS NOT NULL    		
EXEC('DROP TABLE [##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +']')


IF @SEARCH_TEXT in  ('Of', 'in', 'at', 'and', 'is', 'or' , 'an', 'the') 
 SET @SEARCH_TEXT = '~^'

IF OBJECT_ID('TEMPDB..[##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']')


DECLARE  @Filtevalue1   NVARCHAR(Max)

DECLARE  @AttributeAppend   NVARCHAR(Max)
DECLARE  @Filtevalue2   NVARCHAR(Max)
DECLARE  @Filter_Name1  NVARCHAR(Max)
DECLARE  @columns1 NVARCHAR(MAX) = ''
DECLARE @productcolumns1 NVARCHAR(MAX) = ''
DECLARE @ATTR_COUNT1 NVARCHAR(MAX) =''
DECLARE  @ATTRIBUTE_TYPE  NVARCHAR(Max)
DECLARE  @columns2 NVARCHAR(MAX) = ''	
DECLARE  @columns3 NVARCHAR(MAX) = ''
DECLARE  @AttributeAppend_ForKeyAttribute   NVARCHAR(Max)

--SET @columns1 = N'';
--SELECT @columns1+=N''+Name 
--FROM
--(
--    SELECT Search_Attribute AS [Name],Catalog_Id
--    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @CATALOG_ID
  
--) AS x;


--PRINT @columns1



/* KEY ATTRIBUTES AND SEARCH ATTRIBUTES START */

 IF OBJECT_ID('TEMPDB..##TEMP_ATTRIBUTES') IS NOT NULL    
 EXEC('DROP TABLE ##TEMP_ATTRIBUTES')

  IF OBJECT_ID('TEMPDB..[##TEMP_SEARCH_KEY_ATTRIBUTES'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##TEMP_SEARCH_KEY_ATTRIBUTES'+ @sessionId +']')

 EXEC('

 ;WITH CTE AS (

SELECT  ITEM, ''1'' AS TYPE 

FROM QSWS_CONFIGRATION_SETTINGS
CROSS APPLY SPLITSTRING(Search_Attribute, '','')

  where catalog_id='''+ @CATALOG_ID +'''
)
, CTE1 AS (
SELECT  QCS.ITEM, ''2'' AS TYPE 

FROM QSWS_CONFIGRATION_SETTINGS 
CROSS APPLY SPLITSTRING(Key_Attribute, '','') QCS
  where catalog_id='''+ @CATALOG_ID +'''

  )

--UPDATE C SET C.TYPE = C1.TYPE 
--  FROM [##TEMP_SEARCH_KEY_ATTRIBUTES'+ @sessionId +'] C JOIN CTE1 C1 ON 
--   C.Item=C1.ITEM

SELECT  * INTO ##TEMP_ATTRIBUTES
FROM    cte
UNION ALL
SELECT  *
FROM    cte1

  ')



/* KEY ATTRIBUTES AND SEARCH ATTRIBUTES END */

DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  

SELECT ITEM,TYPE FROM ##TEMP_ATTRIBUTES

OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1,@ATTRIBUTE_TYPE

WHILE @@FETCH_STATUS = 0  
BEGIN  


BEGIN

DECLARE @MULTIPLY_INT INT = 1;


IF(@ATTRIBUTE_TYPE='2')

BEGIN


--SET @MULTIPLY_INT =2



SET @Filtevalue2 =  ' OR '+@Filter_Name1+' LIKE ''XXXXX'' '

set @AttributeAppend_ForKeyAttribute=  CONCAT(@AttributeAppend_ForKeyAttribute,@Filtevalue2)

END

ELSE IF(@ATTRIBUTE_TYPE='1')

BEGIN

--SET @MULTIPLY_INT =1


SET @Filtevalue2 =  ' OR '+@Filter_Name1+' LIKE ''XXXXX'' '

set @AttributeAppend=  CONCAT( @AttributeAppend,@Filtevalue2)


END



END


FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1,@ATTRIBUTE_TYPE

END  
CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER


DECLARE @AttributeAppendKeyAttribute_Replace VARCHAR(MAX)=''

set @AttributeAppendKeyAttribute_Replace =@AttributeAppend_ForKeyAttribute

set @AttributeAppendKeyAttribute_Replace =  REPLACE(@AttributeAppendKeyAttribute_Replace, 'XXXXX', ''+@SEARCH_TEXT + '');

print '316'

print @AttributeAppendKeyAttribute_Replace

DECLARE @AttributeAppend_Replace VARCHAR(MAX)=''

set @AttributeAppend_Replace =@AttributeAppend

set @AttributeAppend_Replace =  REPLACE(@AttributeAppend_Replace, 'XXXXX', ''+@SEARCH_TEXT + '');

print @AttributeAppend_Replace


EXEC('SELECT *,CONVERT(DECIMAL(9,2), 0.00) as FamilyAllwords, CONVERT(DECIMAL(9,2), 0.00) as Familyfullword, CONVERT(DECIMAL(9,2), 0.00) as FamilyPartialword, CONVERT(DECIMAL(9,2), 0.00) as Allwords, CONVERT(DECIMAL(9,2), 0.00) as fullword, CONVERT(DECIMAL(9,2), 0.00) as Partialword, CONVERT(DECIMAL(9,2), 0.00) as totalWeight INTO [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] FROM '+@CATALOG_DETAILS+'  ')

-- ********************************** Family Name Full Search ***************************************************
EXEC('
Select distinct CATALOG_ID,  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
  (T1.FAMILY_NAME LIKE '''+ @SEARCH_TEXT + ''')

)


')

 print @SEARCH_TEXT
 print '1'
 Exec('UPDATE TQCPS SET TQCPS.FamilyAllwords = ('+@SEARCH_TEXT_COUNT+' *4)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 Exec('UPDATE TQCPS SET TQCPS.FamilyAllwords = ('+@SEARCH_TEXT_COUNT+' *4)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')



 --- ********************************************************* Family Name Full Search for Key attribute   *************************************


EXEC('
Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SEARCH_TEXT + ''')

  '+@AttributeAppendKeyAttribute_Replace +'



)


')


print '385'
 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2 * 2)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2 * 2)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_FULL_SEARCH_KEY_ATTRIBUTE'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

--- ********************************************************* Family Name Full Search END   *************************************



EXEC('
Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##TEMP_FULL_SEARCH'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SEARCH_TEXT + ''')

  '+@AttributeAppend_Replace +'



)


')

 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')




   IF OBJECT_ID('TEMPDB..##SPIT_SEARCH_TEXT') IS NOT NULL    
EXEC('DROP TABLE ##SPIT_SEARCH_TEXT')

SELECT ITEM into ##SPIT_SEARCH_TEXT FROM SPLITSTRING(@SEARCH_TEXT,'')
 DECLARE @count int = 1;
 DECLARE @SPLITTEXT VARCHAR(50) =''
 --select @SEARCH_TEXT_COUNT
 print 'ee'
 print @SEARCH_TEXT_COUNT
 WHILE (@count <=  @SEARCH_TEXT_COUNT )
 BEGIN

IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Allwords'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Allwords'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Partial'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial_InBetween'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Partial_InBetween'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##temp_Family_Name_Search_Result_Partial'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']')


 	IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']') IS NOT NULL    		
EXEC('DROP TABLE [##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']')

	IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +']') IS NOT NULL    	
EXEC('DROP TABLE [##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +']')	
IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +']') IS NOT NULL    	
EXEC('DROP TABLE [##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +']')	

 
SELECT TOP 1  @SPLITTEXT= LTRIM(RTRIM(ITEM)) FROM ##SPIT_SEARCH_TEXT where ITEM NOT IN ('Of', 'in', 'at', 'and', 'is', 'or' , 'an', 'the')
 SET @COUNT =  @COUNT +1
  
  IF @SPLITTEXT =''
  SET @SPLITTEXT = '~'

  PRINT @SPLITTEXT

  	DECLARE @AttributeAppend_ReplaceForSplit_text_KeyAttribute VARCHAR(MAX)=''	
set @AttributeAppend_ReplaceForSplit_text_KeyAttribute =@AttributeAppend_ForKeyAttribute	
set @AttributeAppend_ReplaceForSplit_text_KeyAttribute =  REPLACE(@AttributeAppend_ReplaceForSplit_text_KeyAttribute, 'XXXXX', ''+@SPLITTEXT + '%');

DECLARE @AttributeAppend_ReplaceForSplit_text VARCHAR(MAX)=''

set @AttributeAppend_ReplaceForSplit_text =@AttributeAppend

set @AttributeAppend_ReplaceForSplit_text =  REPLACE(@AttributeAppend_ReplaceForSplit_text, 'XXXXX', ''+@SPLITTEXT + '%');

print @AttributeAppend_ReplaceForSplit_text

 /*************************** Family Name Partial word search *******************/
 
 EXEC('

Select distinct CATALOG_ID,CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']
From '+@CATALOG_DETAILS+' T1

		  WHERe  (
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + '%'')

  )

')


 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')


 /*************************** Family Name Partial word search END  *******************/


 
 /*************************** Partial word search for Key Attribute  *******************/

 EXEC('

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']
From '+@CATALOG_DETAILS+' T1

		  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + '%'')

  '+@AttributeAppend_ReplaceForSplit_text_KeyAttribute +'


  )

')



 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_KeyAttribute'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')



 /*************************** Partial word search for Key Attribute *******************/

 /*************************** Partial word search *******************/

 EXEC('

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial'+ @sessionId +']
From '+@CATALOG_DETAILS+' T1

		  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + '%'')

  '+@AttributeAppend_ReplaceForSplit_text +'


  )

')



 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')



 /*************************** Partial word search *******************/

  /*************************** Partial word search in between *******************/

  DECLARE @AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute VARCHAR(MAX)=''	
set @AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute =@AttributeAppend_ForKeyAttribute	
set @AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute, 'XXXXX', '%'+@SPLITTEXT + '%');	
print @AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute
  
DECLARE @AttributeAppend_ReplaceForSplit_text_Inbetween VARCHAR(MAX)=''

set @AttributeAppend_ReplaceForSplit_text_Inbetween =@AttributeAppend

set @AttributeAppend_ReplaceForSplit_text_Inbetween =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Inbetween, 'XXXXX', '%'+@SPLITTEXT + '%');

print @AttributeAppend_ReplaceForSplit_text_Inbetween

 /*************************** Partial word search In between *******************/

  

  EXEC('

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
	
  (T1.FAMILY_NAME LIKE ''%'+@SPLITTEXT + '%'')

  
)

')



 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')


  /*************************** Family Name  Partial word search In between for Key attributes  *******************/

  EXEC('

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
	  (T1.CATEGORY_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE ''%'+@SPLITTEXT + '%'')

   '+@AttributeAppend_ReplaceForSplit_text_Inbetween_ForKeyAttribute +'

  
)

')

  EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween_KeyAttribute'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')


 /*************************** Family Name  Partial word search In between END  *******************/

  EXEC('

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial_InBetween'+ @sessionId +'] 
From '+@CATALOG_DETAILS+'  T1

	  WHERe  (
	  (T1.CATEGORY_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE ''%'+@SPLITTEXT + '%'')

   '+@AttributeAppend_ReplaceForSplit_text_Inbetween +'

  
)

')

  EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 /*************************** Partial word search *******************/

  --SELECT @SPLITTEXT
 /*************************** WHOLE word search *******************/

 DECLARE @AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute VARCHAR(MAX)=''	
set @AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute =@AttributeAppend_ForKeyAttribute	
set @AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute, 'XXXXX',''+@SPLITTEXT + '');

 DECLARE @AttributeAppend_ReplaceForSplit_text_Wholeword VARCHAR(MAX)=''

set @AttributeAppend_ReplaceForSplit_text_Wholeword =@AttributeAppend

set @AttributeAppend_ReplaceForSplit_text_Wholeword =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Wholeword, 'XXXXX',''+@SPLITTEXT + '');

print @AttributeAppend_ReplaceForSplit_text_Wholeword

 /*************************** Family Name WHOLE word search  *******************/
 
  
 EXEC(' 

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##temp_Family_Name_Search_Result_Allwords'+@sessionId +'] 
From '+@CATALOG_DETAILS+' T1

  WHERe  (
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + ''')

)


')

  
 EXEC('UPDATE TQCPS SET TQCPS.Familyfullword = Familyfullword +3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 EXEC('UPDATE TQCPS SET TQCPS.Familyfullword = Familyfullword +3  FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

   /*************************** Family Name WHOLE word search For key attribute  *******************/

 EXEC(' 

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +'] 
From '+@CATALOG_DETAILS+' T1

  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + ''')

   '+@AttributeAppend_ReplaceForSplit_text_Wholeword_KeyAttribute +'

)


')

 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 * 2 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords_KeyAttribute'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')


 /*************************** Family Name WHOLE word search END  *******************/

 EXEC(' 

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Allwords'+ @sessionId +'] 
From '+@CATALOG_DETAILS+' T1

  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + ''')

   '+@AttributeAppend_ReplaceForSplit_text_Wholeword +'

)


')

 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 /*************************** WHOLE word search *******************/

 
 DELETE FROM ##SPIT_SEARCH_TEXT WHERE ITEM =@SPLITTEXT

 END

 EXEC('UPDATE TQCPS SET TQCPS.totalWeight = (FamilyAllwords + Familyfullword + FamilyPartialword + Allwords+fullword+PArtialWord)  FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS')

--SELECT * FROM ##QSWS_CATALOG_PRODUCTS_SNAPSHOT TQCPS where totalWeight  > 0  ORDER BY totalWeight desc

 EXEC('SELECT * into [##TEMP_SEARCH'+ @sessionId +'] FROM [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']  where totalWeight  > 0  ORDER BY totalWeight desc')

 
-- EXEC('SELECT * from [##TEMP_SEARCH'+ @sessionId +']')

DECLARE @ATTRIBUTE_NAME_ITEMNO VARCHAR(MAX)=''
select @ATTRIBUTE_NAME_ITEMNO =  CAPTION from TB_ATTRIBUTE WHERE ATTRIBUTE_TYPE=1 AND ATTRIBUTE_ID=1 AND FLAG_RECYCLE = 'A' AND PUBLISH2WEB = 1

Set @ATTRIBUTE_NAME_ITEMNO = '['+@ATTRIBUTE_NAME_ITEMNO+']'




Declare @ATTRIBUTE_NAME_DISPLAY NVarchar(max)=''
Select @ATTRIBUTE_NAME_DISPLAY=Search_Family_Attributes from QSWS_CONFIGRATION_SETTINGS where Catalog_Id=@CATALOG_ID
set @ATTRIBUTE_NAME_DISPLAY=@ATTRIBUTE_NAME_DISPLAY


Declare @ATTRIBUTE_PRODUCTNAME_DISPLAY NVarchar(max)=''
Select @ATTRIBUTE_PRODUCTNAME_DISPLAY=Search_Attribute from QSWS_CONFIGRATION_SETTINGS where Catalog_Id=@CATALOG_ID
set @ATTRIBUTE_PRODUCTNAME_DISPLAY=@ATTRIBUTE_PRODUCTNAME_DISPLAY


print '561'
print @ATTRIBUTE_PRODUCTNAME_DISPLAY


 IF(@CATEGORY_ID='' and (@FAMILY_ID IS NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0  )

 BEGIN

 IF OBJECT_ID('TEMPDB..[##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']')


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

  
 EXEC(' SELECT distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' AS ITEM_NUMBER,PRODUCT_IMAGE AS PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_NAME_DISPLAY+','+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] FROM [##TEMP_SEARCH'+ @sessionId +']  where totalWeight  > 0  ORDER BY totalWeight desc')


END

ELSE


BEGIN 

  
 EXEC(' SELECT distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' AS ITEM_NUMBER,PRODUCT_IMAGE AS PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] FROM [##TEMP_SEARCH'+ @sessionId +']  where totalWeight  > 0  ORDER BY totalWeight desc')



END


IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +']')




EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY   totalWeight desc,family_NAME
   )  ROW_NUM INTO [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +'] from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']  ORDER BY totalWeight desc,family_NAME')





IF OBJECT_ID('TEMPDB..[##ResultProduct'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProduct'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProductTotalCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductTotalCount'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##ResultProductCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductCount'+ @sessionId +']')



IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId+'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId+']')

END

print '606'

EXEC('select * into [PRODUCTDISPALYDETAILS'+ @sessionId+'] from [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']')

print '611'

IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

EXEC('select  distinct TTP.FAMILY_ID as CATEGORY_ID,FAMILY_NAME as CATEGORY_NAME,IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TTP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,totalWeight,TTP.FAMILY_IS_CLONE AS IS_CLONE,'+@ATTRIBUTE_NAME_DISPLAY+' into [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +'] TTP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TTP.FAMILY_ID
  join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TTP.FAMILY_ID
')

END


ELSE

 BEGIN

 EXEC('select  distinct TTP.FAMILY_ID as CATEGORY_ID,FAMILY_NAME as CATEGORY_NAME,IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
 CASE WHEN TSF.SUBFAMILY_ID=TTP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,totalWeight,TTP.FAMILY_IS_CLONE AS IS_CLONE into [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +'] ttp
  LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TTP.FAMILY_ID
    join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TTP.FAMILY_ID
   ')

END


print '662'



--EXEC('select * from [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +'] order by totalWeight desc')

EXEC('select * into  [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +'] order by totalWeight desc')

--EXEC('select * from [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +'] order by totalWeight desc')


--Declare @QUERY nvarchar(max)
--Declare @ParmDefinition nvarchar(Max)
--Declare @CHK nvarchar(Max)
--SET @QUERY = 'Select @retvalOUT =  EXEC STP_LS_GET_WEB_GETCATEGORYFAMILYDETAILS ''1'', ''C'' ,''CAT12589'',123'
--SET @ParmDefinition = '@retvalOUT nvarchar(Max) OUTPUT';
--EXEC sp_executesql @QUERY, @ParmDefinition, @retvalOUT=@CHK OUTPUT;
--Select @CHK

 EXEC('select * into [##ResultProduct'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +'] '  )

 print '670'

 --EXEC('SELECT * FROM  [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +'] ')

--EXEC('SELECT COUNT(distinct FAMILY_ID) AS TOTAL_RESULTS into [##ResultProductTotalCount'+ @sessionId +'] FROM [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +'] ')


print '674'


END




 IF(@CATEGORY_ID<>'' and (@FAMILY_ID IS NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0  )
BEGIN
print '683'



IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


EXEC('(SELECT
 DISTINCT CATALOG_ID,CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+',totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+'
 into [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] FROM 
 [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')


END


ELSE 


BEGIN

EXEC('(SELECT
 DISTINCT CATALOG_ID,CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+'
 into [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] FROM 
 [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

END


Exec('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_PRODUCT'+ @sessionId +'] from [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] order by PRODUCT_ID')



   
IF OBJECT_ID('TEMPDB..[##ResultProduct'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProduct'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProductTotalCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductTotalCount'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##ResultProductCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductCount'+ @sessionId +']')



IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +']')


DECLARE @TEMP_PRODUCT NVARCHAR(MAX); 

declare @TEMP_PRODUCT_Count int;

set @TEMP_PRODUCT = 'SELECT @TEMP_PRODUCT_Count =  count(*) from [##TEMP_PRODUCT'+ @sessionId +']'
EXECUTE sp_executeSQL @TEMP_PRODUCT, N'@TEMP_PRODUCT_Count INT OUTPUT', @TEMP_PRODUCT_Count OUTPUT

PRINT '776'
PRINT @TEMP_PRODUCT_Count

IF(@TEMP_PRODUCT_Count > 0)

BEGIN


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId+'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId+']')

END

PRINT '773'

EXEC('select * into [PRODUCTDISPALYDETAILS'+ @sessionId +'] from [##TEMP_PRODUCT'+ @sessionId +']')

END




IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

EXEC('select  distinct TP.FAMILY_ID as CATEGORY_ID,TP.FAMILY_NAME as CATEGORY_NAME,TP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG
,'+@ATTRIBUTE_NAME_DISPLAY+',totalWeight,TP.FAMILY_IS_CLONE AS IS_CLONE into [##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +'] from [##TEMP_PRODUCT'+ @sessionId +'] TP
  LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TP.FAMILY_ID
    join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TP.FAMILY_ID
 ')

END

ELSE


BEGIN
EXEC('select  distinct TP.FAMILY_ID as CATEGORY_ID,TP.FAMILY_NAME as CATEGORY_NAME,TP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
totalWeight,TP.FAMILY_IS_CLONE AS IS_CLONE  into [##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +'] from [##TEMP_PRODUCT'+ @sessionId +'] TP
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TP.FAMILY_ID
 join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TP.FAMILY_ID
')


END


EXEC('select *,ROW_NUMBER() OVER(ORDER BY totalWeight desc) AS Row# into [##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +'] from [##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +'] ')

print '796'

 EXEC('select *  into [##ResultProduct'+ @sessionId +'] from [##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +'] ')


END 
Else If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') and (select count(*) from @ATTRIBUTETABLE) = 0 )
BEGIN
print'1'

IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


EXEC('(SELECT
  DISTINCT CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME, FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+',totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+'
  into [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] FROM 
  [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')

END

ELSE

BEGIN
EXEC('(SELECT
  DISTINCT CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME, FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+'
  into [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] FROM 
  [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')



END
EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_Family_PRODUCT'+ @sessionId +'] from [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] order by PRODUCT_ID')

  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'ProductFilterTable'+ @sessionId +'')
BEGIN
EXEC('Drop table [ProductFilterTable'+ @sessionId +']')
END
IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT_RESULT'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_Family_PRODUCT_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProduct'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProduct'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProductTotalCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductTotalCount'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##ResultProductCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductCount'+ @sessionId +']')

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId+'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId+']')

END

EXEC('select * into [PRODUCTDISPALYDETAILS'+ @sessionId +'] from [##TEMP_Family_PRODUCT'+ @sessionId +']')


print '913'

IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN
print '918'

EXEC('select  distinct TFP.FAMILY_ID as CATEGORY_ID,TFP.FAMILY_NAME as CATEGORY_NAME,TFP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TFP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
'+@ATTRIBUTE_NAME_DISPLAY+',totalWeight,TFP.FAMILY_IS_CLONE AS IS_CLONE into [##TEMP_Family_PRODUCT_RESULT'+ @sessionId +'] from [##TEMP_Family_PRODUCT'+ @sessionId +'] TFP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TFP.FAMILY_ID
  join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TFP.FAMILY_ID
')


END


ELSE 

BEGIN
print '928'

EXEC('select  distinct TFP.FAMILY_ID as CATEGORY_ID,TFP.FAMILY_NAME as CATEGORY_NAME,TFP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TFP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
totalWeight,TFP.FAMILY_IS_CLONE AS IS_CLONE  into [##TEMP_Family_PRODUCT_RESULT'+ @sessionId +'] from [##TEMP_Family_PRODUCT'+ @sessionId +'] TFP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TFP.FAMILY_ID
   join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TFP.FAMILY_ID
')

END

EXEC('select *,ROW_NUMBER() OVER(ORDER BY totalWeight desc) AS Row# into [##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +'] from [##TEMP_Family_PRODUCT_RESULT'+ @sessionId +'] ')


 EXEC(' select  * into [##ResultProduct'+ @sessionId +'] from [##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +'] ')

--exec('select * from [##TEMP_Family_PRODUCT'+ @sessionId +']')

END


Else if ((select count(*) from @ATTRIBUTETABLE) > 0)
BEGIN


 SET NOCOUNT ON

 

	
  DECLARE @AttributeFilterPRODUCT TABLE( 

    FAMILY_ID INT NOT NULL, 

    FAMILY_NAME NVARCHAR(max) ,
		
	IMAGE_FILE NVARCHAR(max),

	PRODUCT_ID INT NOT NULL,

	ITEM_NUMBER NVARCHAR(max) ,

	PRODUCT_IMAGE_FILE NVARCHAR(max),

	totalWeight NVARCHAR(max)

); 

DECLARE  @Filtevalue   NVARCHAR(Max)
DECLARE  @Attribute_NAME  NVARCHAR(Max)
DECLARE  @STRING_VALUE  NVARCHAR(Max)


SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'

If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') )
BEGIN
EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] FROM 
  [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')
END
 ELSE IF (@CATEGORY_ID<>'' or @CATEGORY_ID IS NOT NULL)
  BEGIN

   EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] FROM 
 [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

  END

  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+ @sessionId +'')

BEGIN
EXEC('DROP TABLE [FilterProductTable'+ @sessionId +']')
END

DECLARE @COUNT_FILTER int=0
DECLARE @ResultTable Nvarchar(50)='@AttributeFilterPRODUCT'

DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Attribute_NAME,STRING_VALUE FROM @ATTRIBUTETABLE
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE
WHILE @@FETCH_STATUS = 0  
BEGIN  


if (@COUNT_FILTER=0)
begin


 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

 IF(@CATEGORY_ID<>'' and @FAMILY_ID<>'')
 BEGIN


 
exec ('select * into [FilterProductTable'+ @sessionId +'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')



 END

 ELSE IF(@CATEGORY_ID<>'')
BEGIN

exec ('select * into [FilterProductTable'+ @sessionId +'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

END

 ELSE
 BEGIN

 exec ('select * into [FilterProductTable'+ @sessionId +'] from [##TEMP_SEARCH'+ @sessionId +'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

 END

SET @COUNT_FILTER=1

end

else
BEGIN

 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

Exec ('delete from [FilterProductTable'+ @sessionId +'] where ['+@Attribute_NAME+'] IS NULL')
Exec ('delete from [FilterProductTable'+ @sessionId +'] where ['+@Attribute_NAME+'] NOT IN ('''+@STRING_VALUE+''') ')

END



FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE

END  
CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER




IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+ @sessionId +'')

BEGIN

--INSERT INTO @AttributeFilterPRODUCT (FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,ITEM_NUMBER,PRODUCT_IMAGE_FILE,totalWeight)


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


exec('SELECT DISTINCT CATALOG_ID,CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+',totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' into [ProductFilterTable'+ @sessionId +'] FROM [FilterProductTable'+ @sessionId +'] ')


  END


  ELSE


  BEGIN
  
exec('SELECT DISTINCT CATALOG_ID,CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,Family_Image AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' as ITEM_NUMBER,Product_Image AS PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' into [ProductFilterTable'+ @sessionId +'] FROM [FilterProductTable'+ @sessionId +'] ')

  END

EXEC('DROP TABLE [FilterProductTable'+ @sessionId +']')
END

--select DISTINCT * from @AttributeFilterPRODUCT

IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PAGINATION'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION_Result'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PAGINATION_Result'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +']')
  

EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY   PRODUCT_ID 
   )  ROW_NUM INTO [##TEMP_PAGINATION'+ @sessionId +'] from [ProductFilterTable'+ @sessionId +']  ORDER BY   PRODUCT_ID ')

 IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId+'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId+']')

END

   IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'ProductFilterTable'+ @sessionId +'')
BEGIN
EXEC('Drop table [ProductFilterTable'+ @sessionId +']')
END

IF OBJECT_ID('TEMPDB..[##ResultProduct'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProduct'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProductTotalCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductTotalCount'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##ResultProductCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductCount'+ @sessionId +']')


EXEC('select * into [PRODUCTDISPALYDETAILS'+ @sessionId+'] from [##TEMP_PAGINATION'+ @sessionId +']')

EXEC('select  distinct FAMILY_ID,FAMILY_NAME,IMAGE_FILE,CATEGORY_ID,FAMILY_IS_CLONE AS IS_CLONE into [##TEMP_PAGINATION_Result'+ @sessionId +'] from [##TEMP_PAGINATION'+ @sessionId +']')

EXEC('select *,ROW_NUMBER() OVER(ORDER BY FAMILY_NAME ASC) AS Row# into [##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +'] from [##TEMP_PAGINATION_Result'+ @sessionId +'] ')


print '1182'


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

EXEC('select  distinct temp.FAMILY_ID as CATEGORY_ID,temp.FAMILY_NAME as CATEGORY_NAME,temp.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=temp.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
1 as ''Level'',totalWeight,temp.FAMILY_IS_CLONE AS IS_CLONE,'+@ATTRIBUTE_NAME_DISPLAY+' into [##ResultProduct'+ @sessionId +'] from [##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +'] temp 
join [##TEMP_SEARCH'+ @sessionId +'] QSWSTable on temp.FAMILY_ID=QSWSTable.FAMILY_ID
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =temp.FAMILY_ID
  join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=temp.FAMILY_ID
')


END


ELSE


BEGIN

EXEC('select  distinct temp.FAMILY_ID as CATEGORY_ID,temp.FAMILY_NAME as CATEGORY_NAME,temp.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=temp.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
1 as ''Level'',totalWeight,temp.FAMILY_IS_CLONE AS IS_CLONE into [##ResultProduct'+ @sessionId +'] from [##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +'] temp 
join [##TEMP_SEARCH'+ @sessionId +'] QSWSTable on temp.CATEGORY_ID=QSWSTable.FAMILY_ID 
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =temp.FAMILY_ID
  join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=temp.FAMILY_ID
')

END


END



 END 
 print '100011'
 SET @Counter  = @Counter  + 1
 print @Counter
 END

 ELSE

 BEGIN
  print '12'

   
DECLARE @VARFAM1 NVARCHAR(MAX); 

declare @VARFAM1_Count int;

set @VARFAM1 = 'SELECT @VARFAM1_Count =  count(*) from [PRODUCTDISPALYDETAILS'+ @sessionId+']'
EXECUTE sp_executeSQL @VARFAM1, N'@VARFAM1_Count INT OUTPUT', @VARFAM1_Count OUTPUT

PRINT @VARFAM1_Count

IF (@VARFAM1_Count > 0)

 begin
 print '1'


 
SET @SEARCH_TEXT=(select item from #SEARCHVALUE where NEW_ID=@Counter)

set @SEARCH_TEXT = REPLACE(@SEARCH_TEXT,'  ',' ')
set @SEARCH_TEXT_TEMP = @SEARCH_TEXT
SET @SEARCH_TEXT_TEMP = REPLACE(@SEARCH_TEXT_TEMP , ' ', '~')
set @SEARCH_TEXT_REPLACE = replace(@SEARCH_TEXT_TEMP,'~', '')
set @SEARCH_TEXT_COUNT = ((len(@SEARCH_TEXT_TEMP) - len(@SEARCH_TEXT_REPLACE)) / len('~') +1)





SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'


IF OBJECT_ID('TEMPDB..[##TEMP_SEARCH'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_SEARCH'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_FULL_SEARCH'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_FULL_SEARCH'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_SPLIT_SEARCH'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_SPLIT_SEARCH'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##SEARCH_FILTER_RESULT'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##SEARCH_FILTER_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##SEARCH_FINAL_RESULT'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##SEARCH_FINAL_RESULT'+ @sessionId +']')



IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_RESULT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_PRODUCT_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##TEMP_Family_PRODUCT'+ @sessionId +']')


 IF OBJECT_ID('TEMPDB..[##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +']') IS NOT NULL                            
  EXEC('DROP TABLE [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +']')

  IF OBJECT_ID('TEMPDB..[##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']')




IF @SEARCH_TEXT in  ('Of', 'in', 'at', 'and', 'is', 'or' , 'an', 'the') 
 SET @SEARCH_TEXT = '~^'

IF OBJECT_ID('TEMPDB..[##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']')



SET @columns1 = N'';
SELECT @columns1+=N''+Name 
FROM
(
    SELECT Search_Attribute AS [Name],Catalog_Id
    FROM QSWS_CONFIGRATION_SETTINGS where Catalog_Id = @CATALOG_ID
  
) AS x;


PRINT @columns1

DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Item 
FROM  SPLITSTRING(@columns1, ',') 
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1 
WHILE @@FETCH_STATUS = 0  
BEGIN  



BEGIN


SET @Filtevalue2 =  ' OR '+@Filter_Name1+' LIKE ''XXXXX'' '

set @AttributeAppend=  CONCAT( @AttributeAppend,@Filtevalue2)

END


FETCH NEXT FROM SEARCHFILTER INTO  @Filter_Name1 

END  
CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER

PRINT @AttributeAppend



set @AttributeAppend_Replace =@AttributeAppend

set @AttributeAppend_Replace =  REPLACE(@AttributeAppend_Replace, 'XXXXX', ''+@SEARCH_TEXT + '');

print @AttributeAppend_Replace




EXEC('SELECT *,CONVERT(DECIMAL(9,2), 0.00) as FamilyAllwords, CONVERT(DECIMAL(9,2), 0.00) as Familyfullword, CONVERT(DECIMAL(9,2), 0.00) as FamilyPartialword, CONVERT(DECIMAL(9,2), 0.00) as Allwords, CONVERT(DECIMAL(9,2), 0.00) as fullword, CONVERT(DECIMAL(9,2), 0.00) as Partialword INTO [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] FROM [PRODUCTDISPALYDETAILS'+ @sessionId +']  ')


print '1142'
-- ********************************** Family Name Full Search ***************************************************
EXEC('
Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +'] 
From [PRODUCTDISPALYDETAILS'+ @sessionId +']  T1

	  WHERe  (
  (T1.FAMILY_NAME LIKE '''+ @SEARCH_TEXT + ''')

)


')


print '1159'

 print @SEARCH_TEXT
 print '1'
 Exec('UPDATE TQCPS SET TQCPS.FamilyAllwords = ('+@SEARCH_TEXT_COUNT+' *4)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 Exec('UPDATE TQCPS SET TQCPS.FamilyAllwords = ('+@SEARCH_TEXT_COUNT+' *4)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_Family_NAME_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')


--- ********************************************************* Family Name Full Search END   *************************************



EXEC('
Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##TEMP_FULL_SEARCH'+ @sessionId +'] 
From [PRODUCTDISPALYDETAILS'+ @sessionId +']  T1

	  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SEARCH_TEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SEARCH_TEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SEARCH_TEXT + ''')

  '+@AttributeAppend_Replace +'



)


')

 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 Exec('UPDATE TQCPS SET TQCPS.Allwords = ('+@SEARCH_TEXT_COUNT+' *2)  +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##TEMP_FULL_SEARCH'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')




 IF OBJECT_ID('TEMPDB..##SPIT_SEARCH_TEXT1') IS NOT NULL    
EXEC('DROP TABLE ##SPIT_SEARCH_TEXT1')

SELECT ITEM into ##SPIT_SEARCH_TEXT1 FROM SPLITSTRING(@SEARCH_TEXT,'')




 print 'new'
 print @count
 print @SEARCH_TEXT_COUNT 

 set @count=1
 WHILE (@count <=  @SEARCH_TEXT_COUNT )
 BEGIN

IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Allwords'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Allwords'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Partial'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Search_Result_Partial_InBetween'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Search_Result_Partial_InBetween'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##temp_Family_Name_Search_Result_Partial'+ @sessionId +']') IS NOT NULL    
EXEC('DROP TABLE [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']') IS NOT NULL    
 EXEC('DROP TABLE [##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']')

 
-- IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
--WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId+']')
--BEGIN
--EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId+']')

--END

 
SELECT TOP 1  @SPLITTEXT= LTRIM(RTRIM(ITEM)) FROM ##SPIT_SEARCH_TEXT1 where ITEM NOT IN ('Of', 'in', 'at', 'and', 'is', 'or' , 'an', 'the')
 SET @COUNT =  @COUNT +1
  
  IF @SPLITTEXT =''
  SET @SPLITTEXT = '~'

  PRINT @SPLITTEXT




set @AttributeAppend_ReplaceForSplit_text =@AttributeAppend

set @AttributeAppend_ReplaceForSplit_text =  REPLACE(@AttributeAppend_ReplaceForSplit_text, 'XXXXX', ''+@SPLITTEXT + '%');

print @AttributeAppend_ReplaceForSplit_text

 /*************************** Family Name Partial word search *******************/
 
 EXEC('

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']
From [PRODUCTDISPALYDETAILS'+ @sessionId +'] T1

		  WHERe  (
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + '%'')

  )

')


 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')


 /*************************** Family Name Partial word search END  *******************/

 /*************************** Partial word search *******************/

 EXEC('

Select distinct CATALOG_ID,  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial'+ @sessionId +']
From [PRODUCTDISPALYDETAILS'+ @sessionId +'] T1

		  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + '%'')

  '+@AttributeAppend_ReplaceForSplit_text +'


  )

')



 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')



 /*************************** Partial word search *******************/

  /*************************** Partial word search in between *******************/

  print '1342'


set @AttributeAppend_ReplaceForSplit_text_Inbetween =@AttributeAppend

set @AttributeAppend_ReplaceForSplit_text_Inbetween =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Inbetween, 'XXXXX', '%'+@SPLITTEXT + '%');

print @AttributeAppend_ReplaceForSplit_text_Inbetween

 /*************************** Partial word search In between *******************/

  

  EXEC('

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +'] 
From [PRODUCTDISPALYDETAILS'+ @sessionId +']  T1

	  WHERe  (
	
  (T1.FAMILY_NAME LIKE ''%'+@SPLITTEXT + '%'')

  
)

')



 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.FamilyPartialword = FamilyPartialword + 3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 /*************************** Family Name  Partial word search In between END  *******************/

  EXEC('

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Partial_InBetween'+ @sessionId +'] 
From [PRODUCTDISPALYDETAILS'+ @sessionId +']  T1

	 WHERE  (
	  (T1.CATEGORY_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL1_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL2_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL3_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL4_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL5_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.SUBCATL6_NAME LIKE ''%'+@SPLITTEXT + '%'' )
	OR
( T1.SUBCATL7_NAME LIKE ''%'+@SPLITTEXT + '%'')
OR
  (T1.FAMILY_NAME LIKE ''%'+@SPLITTEXT + '%'')

   '+@AttributeAppend_ReplaceForSplit_text_Inbetween +'

  
)

')

  EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID')
 EXEC('UPDATE TQCPS SET TQCPS.PartialWord = PArtialWord + 0.5 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Partial_InBetween'+ @sessionId +']  TFS ON TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 /*************************** Partial word search *******************/

  --SELECT @SPLITTEXT
 /*************************** WHOLE word search *******************/




set @AttributeAppend_ReplaceForSplit_text_Wholeword =@AttributeAppend

set @AttributeAppend_ReplaceForSplit_text_Wholeword =  REPLACE(@AttributeAppend_ReplaceForSplit_text_Wholeword, 'XXXXX',''+@SPLITTEXT + '');

print @AttributeAppend_ReplaceForSplit_text_Wholeword

 /*************************** Family Name WHOLE word search  *******************/
 
  
 EXEC(' 

Select distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID  into [##temp_Family_Name_Search_Result_Allwords'+@sessionId +'] 
From [PRODUCTDISPALYDETAILS'+ @sessionId +'] T1

  WHERe  (
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + ''')

)


')

  
 EXEC('UPDATE TQCPS SET TQCPS.Familyfullword = Familyfullword +3 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 EXEC('UPDATE TQCPS SET TQCPS.Familyfullword = Familyfullword +3  FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Family_Name_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')


 /*************************** Family Name WHOLE word search END  *******************/

 EXEC(' 

Select distinct CATALOG_ID,  CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,
   T1.FAMILY_ID, T1.FAMILY_NAME,T1.PRODUCT_ID into [##temp_Search_Result_Allwords'+ @sessionId +'] 
From [PRODUCTDISPALYDETAILS'+ @sessionId +'] T1

  WHERe  (
	  (T1.CATEGORY_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL1_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL2_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL3_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL4_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL5_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.SUBCATL6_NAME LIKE '''+@SPLITTEXT + ''' )
	OR
( T1.SUBCATL7_NAME LIKE '''+@SPLITTEXT + ''')
OR
  (T1.FAMILY_NAME LIKE '''+@SPLITTEXT + ''')

   '+@AttributeAppend_ReplaceForSplit_text_Wholeword +'

)


')

 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TFS.PRODUCT_ID=TQCPS.PRODUCT_ID ')
 EXEC('UPDATE TQCPS SET TQCPS.fullword = fullword +1 FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS JOIN [##temp_Search_Result_Allwords'+ @sessionId +']  TFS ON  TFS.FAMILY_ID=TQCPS.FAMILY_ID and TQCPS.PRODUCT_ID is null ')

 /*************************** WHOLE word search *******************/

 
 DELETE FROM ##SPIT_SEARCH_TEXT1 WHERE ITEM =@SPLITTEXT

 END



 EXEC('UPDATE TQCPS SET TQCPS.totalWeight = (FamilyAllwords + Familyfullword + FamilyPartialword + Allwords+fullword+PArtialWord)  FROM  [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +'] TQCPS')

--SELECT * FROM ##QSWS_CATALOG_PRODUCTS_SNAPSHOT TQCPS where totalWeight  > 0  ORDER BY totalWeight desc

 EXEC('SELECT * into [##TEMP_SEARCH'+ @sessionId +'] FROM [##QSWS_CATALOG_SNAPSHOT'+ @sessionId +']  where totalWeight  > 0  ORDER BY totalWeight desc')

 

select @ATTRIBUTE_NAME_ITEMNO =  CAPTION from TB_ATTRIBUTE WHERE ATTRIBUTE_TYPE=1 AND ATTRIBUTE_ID=1 AND FLAG_RECYCLE = 'A' AND PUBLISH2WEB = 1

Set @ATTRIBUTE_NAME_ITEMNO = '['+@ATTRIBUTE_NAME_ITEMNO+']'





Select @ATTRIBUTE_NAME_DISPLAY=Search_Family_Attributes from QSWS_CONFIGRATION_SETTINGS where Catalog_Id=@CATALOG_ID
set @ATTRIBUTE_NAME_DISPLAY=@ATTRIBUTE_NAME_DISPLAY




Select @ATTRIBUTE_PRODUCTNAME_DISPLAY=Search_Attribute from QSWS_CONFIGRATION_SETTINGS where Catalog_Id=@CATALOG_ID
set @ATTRIBUTE_PRODUCTNAME_DISPLAY=@ATTRIBUTE_PRODUCTNAME_DISPLAY


print '1520'
print @ATTRIBUTE_PRODUCTNAME_DISPLAY
 
 IF(@CATEGORY_ID='' and (@FAMILY_ID IS NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0  )

 BEGIN

 IF OBJECT_ID('TEMPDB..[##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']') IS NOT NULL   
EXEC('DROP TABLE [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']')



IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN
  
 EXEC(' SELECT distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' AS ITEM_NUMBER, PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_NAME_DISPLAY+','+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] FROM [##TEMP_SEARCH'+ @sessionId +']  where totalWeight  > 0  ORDER BY totalWeight desc')


 END


 ELSE

 BEGIN

   
 EXEC(' SELECT distinct CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' AS ITEM_NUMBER, PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' into [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +'] FROM [##TEMP_SEARCH'+ @sessionId +']  where totalWeight  > 0  ORDER BY totalWeight desc')


 END

IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +']')




EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY   totalWeight desc,family_NAME
   )  ROW_NUM INTO [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +'] from [##QSWS_CATALOG_SNAPSHOT_1'+ @sessionId +']  ORDER BY totalWeight desc,family_NAME')



IF OBJECT_ID('TEMPDB..[##ResultProduct'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProduct'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProductTotalCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductTotalCount'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##ResultProductCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductCount'+ @sessionId +']')

print '1562'

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId +'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId +']')

END

EXEC('select * into [PRODUCTDISPALYDETAILS'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +']')


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


EXEC('select  distinct TTP.FAMILY_ID as CATEGORY_ID, TTP.FAMILY_NAME as CATEGORY_NAME, TTP.IMAGE_FILE, TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TTP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG
,'+@ATTRIBUTE_NAME_DISPLAY+', TTP.totalWeight,TTP.FAMILY_IS_CLONE AS IS_CLONE into [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +'] TTP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TTP.FAMILY_ID
 join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TTP.FAMILY_ID
')

END

ELSE

BEGIN

EXEC('select  distinct  TTP.FAMILY_ID as CATEGORY_ID, TTP.FAMILY_NAME as CATEGORY_NAME, TTP.IMAGE_FILE, TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TTP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
 TTP.totalWeight,TTP.FAMILY_IS_CLONE AS IS_CLONE  into [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION'+ @sessionId +'] TTP
LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TTP.FAMILY_ID
 join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TTP.FAMILY_ID
')

END


print '10001'

EXEC('select *,ROW_NUMBER() OVER(ORDER BY totalWeight desc) AS Row# into  [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION_Result'+ @sessionId +'] ')


 EXEC('select * into [##ResultProduct'+ @sessionId +'] from [##TEMP_TEXTSEARCH_PAGINATION_ResultDetails'+ @sessionId +']' )

END


 end
 
 IF(@CATEGORY_ID<>'' and (@FAMILY_ID IS NULL or @FAMILY_ID='') and (select count(*) from @ATTRIBUTETABLE) = 0  )
BEGIN
print '1813'


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN
EXEC('(SELECT
 DISTINCT CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+',totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+'
 into [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] FROM 
 [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

END


ELSE 


BEGIN

EXEC('(SELECT
 DISTINCT CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+'
 into [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] FROM 
 [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

END



Exec('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_PRODUCT'+ @sessionId +'] from [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] order by PRODUCT_ID')



   
IF OBJECT_ID('TEMPDB..[##ResultProduct'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProduct'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProductCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductCount'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##ResultProductTotalCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductTotalCount'+ @sessionId +']')

   IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId +'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId +']')
END


IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +']')

EXEC('select * into [PRODUCTDISPALYDETAILS'+ @sessionId +'] from [##TEMP_PRODUCT'+ @sessionId +']')


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


EXEC('select  distinct TP.FAMILY_ID as CATEGORY_ID,TP.FAMILY_NAME as CATEGORY_NAME,TP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,
CASE WHEN TSF.SUBFAMILY_ID=TP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
1 as ''Level'','+@ATTRIBUTE_NAME_DISPLAY+',TP.totalWeight,TP.FAMILY_IS_CLONE AS IS_CLONE into [##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +'] from [##TEMP_PRODUCT'+ @sessionId +'] TP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TP.FAMILY_ID
   join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TP.FAMILY_ID
')


END


ELSE 

BEGIN

EXEC('select  distinct TP.FAMILY_ID as CATEGORY_ID,TP.FAMILY_NAME as CATEGORY_NAME,TP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
TP.totalWeight,TP.FAMILY_IS_CLONE AS IS_CLONE  into [##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +'] from [##TEMP_PRODUCT'+ @sessionId +'] TP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TP.FAMILY_ID
   join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TP.FAMILY_ID
')

END

EXEC('select *,ROW_NUMBER() OVER(ORDER BY totalWeight desc) AS Row# into [##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +'] from [##TEMP_PRODUCT_PAGEINATION_RESULT_Result'+ @sessionId +'] ')



 EXEC('select * into [##ResultProduct'+ @sessionId +'] from [##TEMP_PRODUCT_PAGINATION_ResultDetails'+ @sessionId +'] ')




END 

Else If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') and (select count(*) from @ATTRIBUTETABLE) = 0 )
BEGIN
print'1'

IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


EXEC('(SELECT
  DISTINCT CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME, FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,PRODUCT_IMAGE_FILE,'+@ATTRIBUTE_NAME_DISPLAY+',totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+'
  into [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] FROM 
  [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')

END

ELSE 

BEGIN


EXEC('(SELECT
  DISTINCT CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME, FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+'  as ITEM_NUMBER,PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+'
  into [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] FROM 
  [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')


END

EXEC('select * ,ROW_NUMBER() OVER (
	ORDER BY FAMILY_ID
   )  ROW_NUM INTO [##TEMP_Family_PRODUCT'+ @sessionId +'] from [##TEMP_FILTERPRODUCT_RESULT'+ @sessionId +'] order by PRODUCT_ID')

  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId +'')
BEGIN
EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId +']')
END

IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT_RESULT'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_Family_PRODUCT_RESULT'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProduct'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProduct'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProductTotalCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductTotalCount'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##ResultProductCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductCount'+ @sessionId +']')

EXEC('select * into [PRODUCTDISPALYDETAILS'+ @sessionId +'] from [##TEMP_Family_PRODUCT'+ @sessionId +']')

IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

EXEC('select  distinct TFP.FAMILY_ID as CATEGORY_ID,TFP.FAMILY_NAME as CATEGORY_NAME,TFP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TFP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
'+@ATTRIBUTE_NAME_DISPLAY+',TFP.totalWeight,TFP.FAMILY_IS_CLONE AS IS_CLONE into [##TEMP_Family_PRODUCT_RESULT'+ @sessionId +'] from [##TEMP_Family_PRODUCT'+ @sessionId +'] TFP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TFP.FAMILY_ID
   join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TFP.FAMILY_ID
')


END

ELSE


BEGIN
EXEC('select  distinct TFP.FAMILY_ID as CATEGORY_ID,TFP.FAMILY_NAME as CATEGORY_NAME,TFP.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=TFP.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,
TFP.totalWeight,TFP.FAMILY_IS_CLONE AS IS_CLONE  into [##TEMP_Family_PRODUCT_RESULT'+ @sessionId +'] from [##TEMP_Family_PRODUCT'+ @sessionId +'] TFP
 LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =TFP.FAMILY_ID
   join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TFP.FAMILY_ID
')


END
EXEC('select *,ROW_NUMBER() OVER(ORDER BY totalWeight desc) AS Row# into [##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +'] from [##TEMP_Family_PRODUCT_RESULT'+ @sessionId +'] ')


 EXEC(' select * into [##ResultProduct'+ @sessionId +'] from [##TEMP_Family_PRODUCT_RESULT_Result_Details'+ @sessionId +'] ')


END


Else if ((select count(*) from @ATTRIBUTETABLE) > 0)
BEGIN


 SET NOCOUNT ON

 

		
 IF OBJECT_ID('TEMPDB..[##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +']') IS NOT NULL                            
  EXEC('DROP TABLE [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +']')



SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'

If(@CATEGORY_ID<>'' and (@FAMILY_ID IS NOT NULL or @FAMILY_ID<>'') )
BEGIN
EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] FROM 
  [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' and (FAMILY_ID in (SELECT ITEM FROM SPLITSTRING('''+@FAMILY_ID+''','','')) OR ISNULL('''+@FAMILY_ID+''', '''') = ''''))')
END
 ELSE IF (@CATEGORY_ID<>'' or @CATEGORY_ID IS NOT NULL)
  BEGIN

   EXEC('(SELECT *
  into [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] FROM 
 [##TEMP_SEARCH'+ @sessionId +'] WHERE  FAMILY_ID is not null
AND ((CATEGORY_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','','')) OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
	   OR (SUBCATL1_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	     OR (SUBCATL2_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		  OR (SUBCATL3_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		   OR (SUBCATL4_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
		    OR (SUBCATL5_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			 OR (SUBCATL6_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''')
			  OR (SUBCATL7_ID in (SELECT ITEM FROM SPLITSTRING('''+@CATEGORY_ID+''','',''))OR ISNULL('''+@CATEGORY_ID+''', '''') = '''' )
	   )


AND CATALOG_ID='''+ @CATALOG_ID +''' )')

  END

  IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+ @sessionId +'')

BEGIN
EXEC('DROP TABLE [FilterProductTable'+ @sessionId +']')
END

set @COUNT_FILTER=0
--DECLARE @ResultTable Nvarchar(50)='@AttributeFilterPRODUCT'

DECLARE  SEARCHFILTER  CURSOR  
LOCAL  FORWARD_ONLY  FOR  
SELECT Attribute_NAME,STRING_VALUE FROM @ATTRIBUTETABLE
OPEN SEARCHFILTER  
FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE
WHILE @@FETCH_STATUS = 0  
BEGIN  


if (@COUNT_FILTER=0)
begin

 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

 IF(@CATEGORY_ID<>'' and @FAMILY_ID<>'')
 BEGIN

 
exec ('select * into [FilterProductTable'+ @sessionId +'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')



 END

 ELSE IF(@CATEGORY_ID<>'')
BEGIN

exec ('select * into [FilterProductTable'+ @sessionId +'] from [##TEMP_ATTRIBUTE_VALUESDetails'+ @sessionId +'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

END

 ELSE
 BEGIN

 exec ('select * into [FilterProductTable'+ @sessionId +'] from [##TEMP_SEARCH'+ @sessionId +'] where ['+@Attribute_NAME+'] in ('''+@STRING_VALUE+''') ')

 END

SET @COUNT_FILTER=1

end

else
BEGIN

 SET @STRING_VALUE = substring(@STRING_VALUE,2,(LEN(@STRING_VALUE)-2)) 
 set @STRING_VALUE =  REPLACE(@STRING_VALUE, '''','''''')
 set @STRING_VALUE = replace(@STRING_VALUE, ''',''', ',') 

Exec ('delete from [FilterProductTable'+ @sessionId +'] where ['+@Attribute_NAME+'] IS NULL')
Exec ('delete from [FilterProductTable'+ @sessionId +'] where ['+@Attribute_NAME+'] NOT IN ('''+@STRING_VALUE+''')')

END



FETCH NEXT FROM SEARCHFILTER INTO  @Attribute_NAME,@STRING_VALUE

END  
CLOSE SEARCHFILTER  
DEALLOCATE SEARCHFILTER




IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'FilterProductTable'+ @sessionId +'')

BEGIN


--INSERT INTO @AttributeFilterPRODUCT (FAMILY_ID,FAMILY_NAME,PRODUCT_ID,IMAGE_FILE,ITEM_NUMBER,PRODUCT_IMAGE_FILE,totalWeight)


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN

exec('SELECT DISTINCT CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME, FAMILY_ID,FAMILY_NAME,PRODUCT_ID,FAMILY_IMAGE AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' as ITEM_NUMBER,PRODUCT_IMAGE as PRODUCT_IMAGE_FILE,FAMILY_IS_CLONE,'+@ATTRIBUTE_NAME_DISPLAY+',totalWeight,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' into [ProductFilterTable'+ @sessionId +'] FROM [FilterProductTable'+ @sessionId +'] ')
  END

  ELSE

  BEGIN

  
exec('SELECT DISTINCT CATALOG_ID, CATEGORY_ID, CATEGORY_NAME, SUBCATL1_ID, 
  SUBCATL1_NAME,SUBCATL2_ID,SUBCATL2_NAME,SUBCATL3_ID,SUBCATL3_NAME,SUBCATL4_ID,SUBCATL4_NAME,SUBCATL5_ID,SUBCATL5_NAME,SUBCATL6_ID,SUBCATL6_NAME,SUBCATL7_ID,SUBCATL7_NAME, FAMILY_ID,FAMILY_NAME,PRODUCT_ID,FAMILY_IMAGE AS IMAGE_FILE,'+@ATTRIBUTE_NAME_ITEMNO+' as ITEM_NUMBER,PRODUCT_IMAGE as PRODUCT_IMAGE_FILE,totalWeight,FAMILY_IS_CLONE,'+@ATTRIBUTE_PRODUCTNAME_DISPLAY+' into [ProductFilterTable'+ @sessionId +'] FROM [FilterProductTable'+ @sessionId +'] ')
  END

EXEC('DROP TABLE [FilterProductTable'+ @sessionId +']')
END

--select DISTINCT * from @AttributeFilterPRODUCT

IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PAGINATION'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION_Result'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PAGINATION_Result'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +']')

  

exec('select * ,ROW_NUMBER() OVER (
	ORDER BY   PRODUCT_ID 
   )  ROW_NUM INTO [##TEMP_PAGINATION'+ @sessionId +'] from [ProductFilterTable'+ @sessionId +']  ORDER BY   PRODUCT_ID')


   --EXEC('select * from [##TEMP_PAGINATION'+ @sessionId +']')


     IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'PRODUCTDISPALYDETAILS'+ @sessionId +'')
BEGIN

EXEC('Drop table [PRODUCTDISPALYDETAILS'+ @sessionId +']')
END

   IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME = N'ProductFilterTable'+ @sessionId +'')
BEGIN
EXEC('Drop table [ProductFilterTable'+ @sessionId +']')
END

IF OBJECT_ID('TEMPDB..[##ResultProduct'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProduct'+ @sessionId +']')

IF OBJECT_ID('TEMPDB..[##ResultProductTotalCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductTotalCount'+ @sessionId +']')


IF OBJECT_ID('TEMPDB..[##ResultProductCount'+ @sessionId +']') IS NOT NULL                            
EXEC('DROP TABLE [##ResultProductCount'+ @sessionId +']')


print '1909'


EXEC('select * into [PRODUCTDISPALYDETAILS'+ @sessionId +'] from [##TEMP_PAGINATION'+ @sessionId +']')


print '1916'


EXEC('select  distinct FAMILY_ID,FAMILY_NAME,IMAGE_FILE,CATEGORY_ID,FAMILY_IS_CLONE AS IS_CLONE into [##TEMP_PAGINATION_Result'+ @sessionId +'] from [##TEMP_PAGINATION'+ @sessionId +']')

EXEC('select *,ROW_NUMBER() OVER(ORDER BY FAMILY_NAME ASC) AS Row# into [##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +'] from [##TEMP_PAGINATION_Result'+ @sessionId +'] ')


IF(@ATTRIBUTE_NAME_DISPLAY<>'')

BEGIN


EXEC('select  distinct temp.FAMILY_ID as CATEGORY_ID,temp.FAMILY_NAME as CATEGORY_NAME,TCF.IMAGE_FILE,temp.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=temp.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,totalWeight,temp.FAMILY_IS_CLONE AS IS_CLONE,
'+@ATTRIBUTE_NAME_DISPLAY+' into [##ResultProduct'+ @sessionId +'] from [##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +'] temp join [##TEMP_SEARCH'+ @sessionId +']
 QSWSTable on temp.FAMILY_ID=QSWSTable.FAMILY_ID
  LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =temp.FAMILY_ID
    join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=temp.FAMILY_ID
  ')

END

ELSE


BEGIN

EXEC('select  distinct temp.FAMILY_ID as CATEGORY_ID,temp.FAMILY_NAME as CATEGORY_NAME,temp.IMAGE_FILE,TCF.CATEGORY_ID AS CATEGORYSEARCH_ID,''F'' AS TYPE,1 as ''Level'',
CASE WHEN TSF.SUBFAMILY_ID=temp.FAMILY_ID THEN ''SF'' ELSE ''F'' END AS LEVEL_FLAG,totalWeight,temp.FAMILY_IS_CLONE AS IS_CLONE
 into [##ResultProduct'+ @sessionId +'] from [##TEMP_PAGINATION_RESULT_DETAILS'+ @sessionId +'] temp join [##TEMP_SEARCH'+ @sessionId +'] 
 QSWSTable on temp.FAMILY_ID=QSWSTable.FAMILY_ID 
  LEFT JOIN TB_SUBFAMILY TSF ON TSF.SUBFAMILY_ID =temp.FAMILY_ID
   join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=temp.FAMILY_ID
  ')

END


END

  SET @Counter  = @Counter  + 1
 END



 END


EXEC(' select * from [##ResultProduct'+ @sessionId +']  order by totalWeight desc')
--EXEC('select * from [##ResultProductTotalCount'+ @sessionId +']')
--exec('select * from [##ResultProductCount'+ @sessionId +']')

END


GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_GetFilterFamilyDetails]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_GetFilterFamilyDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_QSWS_GetFilterFamilyDetails] AS' 
END
GO


ALTER PROCEDURE [dbo].[STP_QSWS_GetFilterFamilyDetails]
(
@CategoryId NVARCHAR(MAX) ='',
@catalogID int =123
)
as

BEGIN
set @CategoryId= CONCAT( '''', REPLACE(@CategoryId, ',', ''','''), '''');

EXEC ('SELECT FAMILY_ID,FAMILY_NAME,CATEGORY_ID FROM (select DISTINCT tcf.FAMILY_ID,TF.FAMILY_NAME,tcf.CATEGORY_ID,TCF.SORT_ORDER,
row_number() over
    (
        partition by tcf.FAMILY_ID
        order by TCF.SORT_ORDER
    )  as rn
 from TB_FAmily TF
join TB_CATALOG_FAMILY  tcf on tcf.FAMILY_ID=tf.FAMILY_ID 
where tcf.CATALOG_ID='+@catalogID+' and tcf.CATEGORY_ID in ('+@CategoryId+') and tcf.FLAG_RECYCLE=''A''
AND TF.FLAG_RECYCLE = ''A'' AND TF.PUBLISH2WEB = 1 

)AS T WHERE rn=1 ORDER BY T.SORT_ORDER
')


END



GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_IMAGE_LOOPING]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_IMAGE_LOOPING]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_QSWS_IMAGE_LOOPING] AS' 
END
GO



ALTER PROCEDURE [dbo].[STP_QSWS_IMAGE_LOOPING]
(
  @OPTION VARCHAR(25)='GET_PRODUCT_IMAGES',
  @CATALOG_ID INT=123,
  @PRODUCT_ID INT=26057,
  @PRODUCT_NAME VARCHAR(100)='',
  @FAMILY_ID INT=26147

)
	
AS
BEGIN
	
	IF(@OPTION='GET_PRODUCT_IMAGES')
	BEGIN
	

select distinct TPS.STRING_VALUE AS PRODUCT_IMAGE from TB_PROD_SPECS TPS JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID=TPS.ATTRIBUTE_ID AND TA.FLAG_RECYCLE='A'
AND TA.ATTRIBUTE_TYPE=3 WHERE TPS.PRODUCT_ID=@PRODUCT_ID AND TPS.STRING_VALUE IS NOT NULL AND TA.CAPTION NOT IN('SpecSheet','CAD File','CAD File 2','CAD File 3','3D CAD File','Webinar_Link','Event Spec Card','File name','CAD File 4')

	END


	IF(@OPTION='GET_FAMILY_IMAGES')
	BEGIN


	


 
select TPS.STRING_VALUE AS FAMILY_IMAGE from TB_FAMILY_SPECS TPS JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID=TPS.ATTRIBUTE_ID AND TA.FLAG_RECYCLE='A'
AND TA.ATTRIBUTE_TYPE=9 WHERE TPS.FAMILY_ID=@FAMILY_ID AND TA.CAPTION NOT IN('SpecSheet','CAD File','CAD File 2','CAD File 3','3D CAD File','Webinar_Link','Event Spec Card','File name','CAD File 4')
AND TPS.STRING_VALUE <> ''
	END









	
END







GO
/****** Object:  StoredProcedure [dbo].[STP_QSWS_SYNC_SNAPSHOT]    Script Date: 2023-01-27 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STP_QSWS_SYNC_SNAPSHOT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[STP_QSWS_SYNC_SNAPSHOT] AS' 
END
GO

ALTER PROCEDURE [dbo].[STP_QSWS_SYNC_SNAPSHOT]	

	@CATALOG_ID INT=123



AS

BEGIN
	 


DECLARE  @CATALOG_DETAILS NVARCHAR(MAX)
DECLARE @LAST_SYNC_DATE NVARCHAR(MAX)
DECLARE @QUERY  NVARCHAR(MAX)
Declare @ParmDefinition nvarchar(Max)

 SET @CATALOG_DETAILS=  '[QSWS_CATALOG_SNAPSHOT_'+CAST(@CATALOG_ID as varchar(10))+']'

 --EXEC('SELECT  TOP(1) LAST_UPDATED_DATE FROM '+@CATALOG_DETAILS+' ')

-- SET @LAST_SYNC_DATE =('SELECT TOP(1) LAST_UPDATED_DATE FROM  '+@CATALOG_DETAILS+' ')
  
  --EXEC (@LAST_SYNC_DATE)
  SET @QUERY = 'Select @retvalOUT =  LAST_UPDATED_DATE FROM  '+@CATALOG_DETAILS+' '
SET @ParmDefinition = '@retvalOUT nvarchar(Max) OUTPUT';
EXEC sp_executesql @QUERY, @ParmDefinition, @retvalOUT=@LAST_SYNC_DATE OUTPUT;
--Select @LAST_SYNC_DATE

  PRINT @LAST_SYNC_DATE


 -- select CONVERT(datetime, @LAST_SYNC_DATE, 103)

 --select CONVERT(datetime, @LAST_SYNC_DATE, 121)


 Select sum(Count) as Count from(


  select top(1) COUNT(*) As Count from TB_CATEGORY WHERE  ( CONVERT(datetime, @LAST_SYNC_DATE, 121) < CREATED_DATE) OR  (CONVERT(datetime, @LAST_SYNC_DATE, 121) < MODIFIED_DATE) 


UNION


  select top(1) COUNT(*) As Count from TB_FAMILY WHERE  ( CONVERT(datetime, @LAST_SYNC_DATE, 121) < CREATED_DATE) OR  (CONVERT(datetime, @LAST_SYNC_DATE, 121) < MODIFIED_DATE) 

  
UNION


  select top(1) COUNT(*) As Count from TB_SUBFAMILY WHERE  ( CONVERT(datetime, @LAST_SYNC_DATE, 121) < CREATED_DATE) OR  (CONVERT(datetime, @LAST_SYNC_DATE, 121) < MODIFIED_DATE) 

    
UNION


  select top(1) COUNT(*) As Count from TB_PROD_SPECS WHERE  ( CONVERT(datetime, @LAST_SYNC_DATE, 121) < CREATED_DATE) OR  (CONVERT(datetime, @LAST_SYNC_DATE, 121) < MODIFIED_DATE) 

      
UNION


  select top(1) COUNT(*) As Count from TB_FAMILY_SPECS WHERE  ( CONVERT(datetime, @LAST_SYNC_DATE, 121) < CREATED_DATE) OR  (CONVERT(datetime, @LAST_SYNC_DATE, 121) < MODIFIED_DATE) 

        
UNION

  select top(1) COUNT(*) As Count from TB_PARTS_KEY WHERE  ( CONVERT(datetime, @LAST_SYNC_DATE, 121) < CREATED_DATE) OR  (CONVERT(datetime, @LAST_SYNC_DATE, 121) < MODIFIED_DATE) 

  UNION

  select top(1) COUNT(*) As Count from TB_FAMILY_KEY WHERE  ( CONVERT(datetime, @LAST_SYNC_DATE, 121) < CREATED_DATE) OR  (CONVERT(datetime, @LAST_SYNC_DATE, 121) < MODIFIED_DATE) 

    UNION

  select top(1) COUNT(*) As Count from TB_CATEGORY_SPECS WHERE  ( CONVERT(datetime, @LAST_SYNC_DATE, 121) < CREATED_DATE) OR  (CONVERT(datetime, @LAST_SYNC_DATE, 121) < MODIFIED_DATE) 


group by CREATED_DATE,MODIFIED_DATE 

) as t

 END





GO

delete from TB_FUNCTION where FUNCTION_ID in (5000088,5000091,5000092)

GO

update TB_FUNCTION set FUNCTION_GROUP_ID=1 where FUNCTION_GROUP_ID=6
go
update TB_ROLE_FUNCTIONS set FUNCTION_GROUP_ID=1 where FUNCTION_GROUP_ID=6
go
delete from TB_FUNCTIONGROUP where FUNCTION_GROUP_ID=6

--Notification Start
GO
/****** Object:  Trigger [tr_Update_from_TB_Notification]    Script Date: 04/06/2023 6:30:55 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_Update_from_TB_Notification]'))
DROP TRIGGER [dbo].[tr_Update_from_TB_Notification]
GO
/****** Object:  Trigger [tr_Insert_to_TB_Notification]    Script Date: 04/06/2023 6:30:55 PM ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_Insert_to_TB_Notification]'))
DROP TRIGGER [dbo].[tr_Insert_to_TB_Notification]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TB_NOTIFI__MESSA__16A149D2]') AND parent_object_id = OBJECT_ID(N'[dbo].[TB_NOTIFICATION_DETAILS]'))
ALTER TABLE [dbo].[TB_NOTIFICATION_DETAILS] DROP CONSTRAINT [FK__TB_NOTIFI__MESSA__16A149D2]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Get_allDetails_by_RoleId]    Script Date: 04/06/2023 6:30:55 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Get_allDetails_by_RoleId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_Get_allDetails_by_RoleId]
GO
/****** Object:  Table [dbo].[TB_NOTIFICATION_DETAILS]    Script Date: 04/06/2023 6:30:55 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_NOTIFICATION_DETAILS]') AND type in (N'U'))
DROP TABLE [dbo].[TB_NOTIFICATION_DETAILS]
GO
/****** Object:  Table [dbo].[TB_NOTIFICATION]    Script Date: 04/06/2023 6:30:55 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_NOTIFICATION]') AND type in (N'U'))
DROP TABLE [dbo].[TB_NOTIFICATION]
GO
/****** Object:  Table [dbo].[TB_NOTIFICATION]    Script Date: 04/06/2023 6:30:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_NOTIFICATION]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_NOTIFICATION](
	[MESSAGE_ID] [int] IDENTITY(1,1) NOT NULL,
	[USERID] [nvarchar](50) NOT NULL,
	[MESSAGE_TITLE] [nvarchar](100) NOT NULL,
	[MESSAGE_CONTENT] [nvarchar](max) NOT NULL,
	[VALID_FROM] [datetime] NOT NULL,
	[VALID_TO] [datetime] NOT NULL,
	[CREATED_USER] [nvarchar](50) NOT NULL,
	[MODIFIED_USER] [nvarchar](50) NOT NULL,
	[CREATED_DATE] [datetime] NOT NULL,
	[MODIFIED_DATE] [datetime] NOT NULL,
	[ROLE_ID] [nvarchar](50) NULL,
	[NOTIFICATION_FLAG] [bit] NOT NULL,
	[EMAIL_NOTIFICATION] [bit] NULL,
	[ATTACHED_FILE_NAME] [nvarchar](100) NULL,
	[FLAG_RECYCLE] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[MESSAGE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_NOTIFICATION_DETAILS]    Script Date: 04/06/2023 6:30:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_NOTIFICATION_DETAILS]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_NOTIFICATION_DETAILS](
	[USERID] [nvarchar](50) NOT NULL,
	[VIEWED_USER] [nvarchar](50) NOT NULL,
	[ROLE_ID] [int] NOT NULL,
	[NOTIFICATION_FLAG] [bit] NOT NULL,
	[MESSAGE_ID] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Get_allDetails_by_RoleId]    Script Date: 04/06/2023 6:30:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Get_allDetails_by_RoleId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE function [dbo].[fn_Get_allDetails_by_RoleId](@RoleId VARCHAR(10))
returns table
as 
return
(
select am.UserId,am.Email as TB_USER_ID,ar.Role_id as ROLE_ID from aspnet_Membership am
join aspnet_UsersInRoles au on au.UserId=am.UserId
join aspnet_Roles ar on au.RoleId=ar.RoleId
where ar.Role_id=@RoleId
)' 
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__TB_NOTIFI__MESSA__16A149D2]') AND parent_object_id = OBJECT_ID(N'[dbo].[TB_NOTIFICATION_DETAILS]'))
ALTER TABLE [dbo].[TB_NOTIFICATION_DETAILS]  WITH CHECK ADD FOREIGN KEY([MESSAGE_ID])
REFERENCES [dbo].[TB_NOTIFICATION] ([MESSAGE_ID])
GO
/****** Object:  Trigger [dbo].[tr_Insert_to_TB_Notification]    Script Date: 04/06/2023 6:30:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_Insert_to_TB_Notification]'))
EXEC dbo.sp_executesql @statement = N'CREATE trigger [dbo].[tr_Insert_to_TB_Notification] on [dbo].[TB_NOTIFICATION]
for Insert 
as 
begin

   Declare @MessageId int  
   Declare @RoleID nvarchar(50)  
   SELECT @MessageId = MESSAGE_ID,@RoleID=ROLE_ID from inserted

INSERT INTO TB_NOTIFICATION_DETAILS (USERID, VIEWED_USER, ROLE_ID,NOTIFICATION_FLAG,MESSAGE_ID)
select tf.UserId,tf.TB_USER_ID as VIEWED_USER,tf.ROLE_ID,1 as NOTIFICATION_FLAG,@MessageId as MESSAGE_ID from SplitString(@RoleID,'','') ta
Cross apply fn_Get_allDetails_by_RoleId(ta.Item) as tf;

end' 
GO
/****** Object:  Trigger [dbo].[tr_Update_from_TB_Notification]    Script Date: 04/06/2023 6:30:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_Update_from_TB_Notification]'))
EXEC dbo.sp_executesql @statement = N'CREATE trigger [dbo].[tr_Update_from_TB_Notification] on [dbo].[TB_NOTIFICATION]
for  update 
as 
begin

 Declare @MessageId int  
 Declare @RoleID nvarchar(50)
 Declare @FlagRecycle nvarchar(50)
 SELECT @RoleID = ins.ROLE_ID,@MessageId=ins.MESSAGE_ID,@FlagRecycle=FLAG_RECYCLE FROM INSERTED ins
 
 IF UPDATE(ROLE_ID)
BEGIN

delete from TB_NOTIFICATION_DETAILS where MESSAGE_ID=@MessageId

INSERT INTO TB_NOTIFICATION_DETAILS (USERID, VIEWED_USER, ROLE_ID,NOTIFICATION_FLAG,MESSAGE_ID)
select tf.UserId,tf.TB_USER_ID as VIEWED_USER,tf.ROLE_ID,1 as NOTIFICATION_FLAG,@MessageId as MESSAGE_ID from SplitString(@RoleID,'','') ta
Cross apply fn_Get_allDetails_by_RoleId(ta.Item) as tf;

END
IF UPDATE(FLAG_RECYCLE)
BEGIN

if(@FlagRecycle=''R'')

begin
delete from TB_NOTIFICATION_DETAILS where MESSAGE_ID=@MessageId
end

END

end' 
GO

--Notification End