
GO
/****** Object:  Table [dbo].[TB_ASSET_SIZE_UNIT]    Script Date: 2023-01-19 18:01:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_SIZE_UNIT]') AND type in (N'U'))
DROP TABLE [dbo].[TB_ASSET_SIZE_UNIT]
GO
/****** Object:  Table [dbo].[TB_ASSET_SIZE_RANGE]    Script Date: 2023-01-19 18:01:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_SIZE_RANGE]') AND type in (N'U'))
DROP TABLE [dbo].[TB_ASSET_SIZE_RANGE]
GO
/****** Object:  Table [dbo].[TB_ASSET_SIZE_RANGE]    Script Date: 2023-01-19 18:01:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_SIZE_RANGE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_ASSET_SIZE_RANGE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SIZE_RANG] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_ASSET_SIZE_UNIT]    Script Date: 2023-01-19 18:01:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ASSET_SIZE_UNIT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_ASSET_SIZE_UNIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SIZE_UNIT] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TB_ASSET_SIZE_RANGE] ON 

INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (1, N'Greater than')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (2, N'Less than')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (3, N'Tiny (0KB - 16KB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (4, N'Small (16KB - 1MB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (5, N'Medium (1MB - 128MB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (6, N'Large (128MB - 1GB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (7, N'Extra Large (1GB - 4GB)')
INSERT [dbo].[TB_ASSET_SIZE_RANGE] ([ID], [SIZE_RANG]) VALUES (8, N'Huge (Greater than 4GB)')
SET IDENTITY_INSERT [dbo].[TB_ASSET_SIZE_RANGE] OFF
SET IDENTITY_INSERT [dbo].[TB_ASSET_SIZE_UNIT] ON 

INSERT [dbo].[TB_ASSET_SIZE_UNIT] ([ID], [SIZE_UNIT]) VALUES (1, N'KB')
INSERT [dbo].[TB_ASSET_SIZE_UNIT] ([ID], [SIZE_UNIT]) VALUES (2, N'MB')
INSERT [dbo].[TB_ASSET_SIZE_UNIT] ([ID], [SIZE_UNIT]) VALUES (3, N'GB')
SET IDENTITY_INSERT [dbo].[TB_ASSET_SIZE_UNIT] OFF
