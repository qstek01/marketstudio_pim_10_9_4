ALTER TABLE QSWS_CONFIGRATION_SETTINGS ADD Font_Family nvarchar(max);


go 

ALTER TABLE QSWS_CONFIGRATION_SETTINGS ADD Banner_BackColor nvarchar(max),Banner_FontColor nvarchar(max),
Button_BackColor nvarchar(max),Button_FontColor nvarchar(max),Customer_Address nvarchar(max);

go


ALTER TABLE QSWS_CONFIGRATION_SETTINGS
DROP COLUMN Footer_Color;

go

ALTER TABLE QSWS_CONFIGRATION_SETTINGS ADD Footer_LogoImage nvarchar(max),
Footer_FontColor nvarchar(max),Sidemenu_BackColor nvarchar(max),Sidemenu_FontColor nvarchar(max);

go

ALTER TABLE QSWS_CONFIGRATION_SETTINGS ADD FooterLogo_Height nvarchar(max),FooterLogo_Width nvarchar(max);

go

/****** Object:  Table [dbo].[QSWS_MULTIPLE_WISHLIST]    Script Date: 09-01-2023 11:00:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TB_WISHLIST](
	[WISHLIST_ID] [int] IDENTITY(1,1) NOT NULL,
	[WISHLIST_NAME] [varchar](max) NULL,
	[USER_ID] [int] NULL,
	[CREATED_DATE] [datetime] NULL,
	[MODIFIED_DATE] [datetime] NULL,
	[CATALOG_ID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[QSWS_MULTIPLE_WISHLIST_DETAILS]    Script Date: 09-01-2023 11:00:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_WISHLIST_DETAILS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WISHLIST_ID] [int] NOT NULL,
	[FAMILY_ID] [int] NULL,
	[PRODUCT_ID] [int] NULL,
	[ITEM#] [nvarchar](500) NULL,
	[CREATED_DATE] [datetime] NULL,
	[MODIFIED_DATE] [datetime] NULL,
	[CATALOG_ID] [int] NULL
) ON [PRIMARY]

GO



/****** Object:  Table [dbo].[TB_ACTIVE_DIRECTORY]    Script Date: 2023-02-22 16:28:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ACTIVE_DIRECTORY]') AND type in (N'U'))
DROP TABLE [dbo].[TB_ACTIVE_DIRECTORY]
GO
/****** Object:  Table [dbo].[TB_ACTIVE_DIRECTORY]    Script Date: 2023-02-22 16:28:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_ACTIVE_DIRECTORY]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_ACTIVE_DIRECTORY](
	[UserID] [nvarchar](max) NULL,
	[WorkGroup] [nvarchar](max) NULL,
	[WorkGroupUser] [nvarchar](max) NULL,
	[User_Email] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
INSERT [dbo].[TB_ACTIVE_DIRECTORY] ([UserID], [WorkGroup], [WorkGroupUser], [User_Email]) VALUES (N'16A38FEA-C137-45BC-8E6C-D58D8114F210', N'demoGrp', N'demoGrpUser', N'demo@questudio.com')


go

update Customer_Settings set EnableWorkFlow=0,DisplayIDColumns=0,AllowDuplicateItem_PartNum=0,WebcatSyncURL=0 
go
update Customer_Settings set ProductLevelMultitablePreview=1,ShowCustomAPPS=1,SubCatalog_Items=1
go

--Dashboard Dynamic banner Image Start
ALTER TABLE [QSWS_CONFIGRATION_SETTINGS]
ADD BannerImage_1 nvarchar(50),
	BannerImage_2  nvarchar(50),
	BannerImage_3  nvarchar(50),
	BannerImage_4  nvarchar(50),
	BannerImage_5  nvarchar(50),
	BannerImage_6  nvarchar(50),
	Banner_Seconds int;
	go

--Dashboard Dynamic banner Image End

--npm i guid-typescript --save

--npm install ngx-image-zoom --save

--npm install @angular-material-components/color-picker@5.0.1

--npm i ngp-image-picker@2.2.2

--npm i ng-block-ui@3.0.2