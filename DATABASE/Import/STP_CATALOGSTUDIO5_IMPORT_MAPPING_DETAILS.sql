
GO
/****** Object:  StoredProcedure [dbo].[STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS]    Script Date: 2020-08-14 03:34:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS]
	@TEMPLATE_ID int=693,
	@IMPORT_TYPE varchar(20)='Family',
	@CAPTION varchar(100)='SERIES1',
	@ATTRIBUTE_ID  varchar(10)='-1',	
	@SHEET_NAME varchar(100) ='Series',
	@USER_NAME varchar(100) ='demo@questudio.com',
	@OPTION varchar(20)='INSERTCAPTION' ,
	@Session_Id varchar(100)='73bba23b-e61d-4cc3-bd96-aa9a4f0291f4'
AS
BEGIN

	/*==============To get the import sheet id for the specific template=============*/
		
		DECLARE @Import_Sheet_id INT;
				
		SET  @Import_Sheet_id =0;

		/*=============To get specific import sheet id ==============*/

        DECLARE @Import_Sheet_idforCaption INT;

		SET @Import_Sheet_idforCaption=0;

	
	IF(@OPTION='INSERT' )
	BEGIN TRY
		
		
		/*==============To check and insert sheet details for the specific template =============*/
			IF NOT EXISTS(SELECT TEMPLATE_ID FROM TB_TEMPLATE_SHEET_DETAILS_IMPORT 
			              WHERE  TEMPLATE_ID=@TEMPLATE_ID and IMPORT_TYPE=@IMPORT_TYPE)
			BEGIN
				INSERT INTO TB_TEMPLATE_SHEET_DETAILS_IMPORT 
				(TEMPLATE_ID,IMPORT_TYPE,SHEET_NAME,FLAG_MAPPING,FLAG_VALIDATION,FLAG_IMPORT,MODIFIED_DATE,MODIFIED_USER)
				VALUES(@TEMPLATE_ID,@IMPORT_TYPE,@SHEET_NAME,1,0,0,GETDATE(),@USER_NAME)
			END
			ELSE

			BEGIN

			UPDATE TB_TEMPLATE_SHEET_DETAILS_IMPORT 
			SET FLAG_MAPPING=1,FLAG_VALIDATION=0,FLAG_IMPORT=0
			WHERE  TEMPLATE_ID=@TEMPLATE_ID AND IMPORT_TYPE=@IMPORT_TYPE AND SHEET_NAME=@SHEET_NAME

			END
			
			 SET @Import_Sheet_id = (SELECT TOP(1) IMPORT_SHEET_ID 
			 FROM TB_TEMPLATE_SHEET_DETAILS_IMPORT 
			 WHERE TEMPLATE_ID=@TEMPLATE_ID AND SHEET_NAME=@SHEET_NAME AND IMPORT_TYPE=@IMPORT_TYPE
			 ORDER BY IMPORT_SHEET_ID DESC)

			 
		    /*==============To delete the specific template mapping details without deleting caption names in the table =============*/
 
			 DELETE 
			 FROM TB_TEMPLATE_MAPPING_DETAILS_IMPORT 
			 WHERE TEMPLATE_ID=@TEMPLATE_ID AND IMPORT_SHEET_ID=@Import_Sheet_id AND ATTRIBUTE_ID!=-1
		
	
	        /*==============To insert Caption and Attribute Id for the mapping table=============*/
			 IF OBJECT_ID('TEMPDB..#TEMP_INSERT_MAPPING_ATTRIBUTES') IS NOT NULL	DROP TABLE #TEMP_INSERT_MAPPING_ATTRIBUTES

			 CREATE TABLE #TEMP_INSERT_MAPPING_ATTRIBUTES(
				RowNum INT,
				captionName VARCHAR(100),
				Attribute_ID INT,
				Sort_Order INT,
				Attribute_Type INT,
				selectedToImport BIT
				)

				/*==============To insert Caption and Attribute Id for the mapping table checking with attribute name=============*/
					EXEC('
					insert into #TEMP_INSERT_MAPPING_ATTRIBUTES (RowNum,captionName,Attribute_ID,Sort_Order,Attribute_Type,selectedToImport)
					SELECT cap.RowNum, captionName,ISNULL(TA.ATTRIBUTE_ID,0) AS Attribute_id,ROW_NUMBER() 
							OVER (ORDER BY (SELECT CAST(CAP.RowNum AS int))) AS SORT_ORDER,ISNULL(TA.ATTRIBUTE_TYPE,cap.attributeType) as Attribute_Type,cap.selectedToImport as selectedToImport
					FROM [##TEMP_CAPTION'+@Session_Id+'] as Cap 
					  left JOIN TB_ATTRIBUTE TA ON
							 TA.FLAG_RECYCLE=''A''  AND  TA.ATTRIBUTE_NAME=cap.attributeName   WHERE   cap.attributeName <> '''' and cap.captionName 
							 NOT in(''CATALOG_NAME'', ''CATEGORY_NAME'' , ''FAMILY_NAME'' , ''SUBFAMILY_NAME'' ,
							  ''Category_PdfTemplate'',''CATEGORY_PUBLISH2WEB'', ''CATEGORY_PUBLISH2PRINT'',''CATEGORY_PUBLISH2PDF'',
							 ''CATEGORY_PUBLISH2EXPORT'', ''CATEGORY_PUBLISH2PORTAL'', ''FAMILY_ID'',''SUBFAMILY_ID'',
							 ''Family_PdfTemplate'',''Product_PdfTemplate'',''FAMILY_PUBLISH2PORTAL'',''FAMILY_PUBLISH2EXPORT'',
							''FAMILY_PUBLISH2PDF'', ''FAMILY_PUBLISH2PRINT'',''FAMILY_PUBLISH2WEB'',''PRODUCT_PUBLISH2WEB'',
							 ''PRODUCT_PUBLISH2PRINT'',''PRODUCT_PUBLISH2PDF'', ''PRODUCT_PUBLISH2EXPORT'', ''PRODUCT_PUBLISH2PORTAL'',
							 ''Catalog_PdfTemplate'') And  cap.captionName not like ''SUBCATNAME_L%'' and cap.captionName not like ''SUBCATID_L%''
					 ORDER BY CAST(CAP.RowNum AS int) ASC

                    ')


			 /*==============Update caption name Attribute Id for the mapping table checking with caption name=============*/

					 UPDATE TIMA  SET TIMA.ATTRIBUTE_ID=TA.ATTRIBUTE_ID,TIMA.Attribute_Type=TA.ATTRIBUTE_TYPE
					FROM #TEMP_INSERT_MAPPING_ATTRIBUTES TIMA  
					JOIN TB_ATTRIBUTE TA ON TA.FLAG_RECYCLE='A' AND
					TIMA.captionName=TA.Caption  WHERE  TIMA.ATTRIBUTE_ID=0 	
					
             /*==============Update  Attribute Id for the mapping table checking with attribute name=============*/

					 UPDATE TIMA  SET TIMA.ATTRIBUTE_ID=TA.ATTRIBUTE_ID,TIMA.Attribute_Type=TA.ATTRIBUTE_TYPE
					FROM #TEMP_INSERT_MAPPING_ATTRIBUTES TIMA  
					JOIN TB_ATTRIBUTE TA ON TA.FLAG_RECYCLE='A' AND
					TIMA.captionName=TA.ATTRIBUTE_NAME WHERE TIMA.ATTRIBUTE_ID=0 

			 /*==============Update Attribute Id if they give caption name in customers settings table=============*/

     --               UPDATE TIMA  SET TIMA.ATTRIBUTE_ID=1
					--FROM #TEMP_INSERT_MAPPING_ATTRIBUTES TIMA  
					--JOIN Customers CU on CU.CustomizeItemNo=TIMA.captionName 
					--WHERE TIMA.ATTRIBUTE_ID=0 and cu.Email =@USER_NAME

			 /*==============Insert user selected attributes values in Mapping table for the particular sheet and import type =============*/

					 INSERT
					 INTO TB_TEMPLATE_MAPPING_DETAILS_IMPORT 
					 (TEMPLATE_ID,IMPORT_SHEET_ID,CAPTION,ATTRIBUTE_ID,SORT_ORDER,ATTRIBUTE_TYPE,SelectedToImport)
					(
						SELECT @TEMPLATE_ID AS TEMPLATE_ID,@Import_Sheet_id AS IMPORT_SHEET_ID,captionName,ATTRIBUTE_ID,SORT_ORDER,Attribute_Type,selectedToImport
						FROM #TEMP_INSERT_MAPPING_ATTRIBUTES
				    )

              
			/*==============To drop Caption details table=============*/
					EXEC('DROP TABLE [##TEMP_CAPTION'+@Session_Id+'] ')


	END TRY

	BEGIN CATCH

				  /*==============To drop Caption details table=============*/
							  EXEC('DROP TABLE [##TEMP_CAPTION'+@Session_Id+'] ')

	END CATCH


	--IF(@OPTION='UPDATE' )
	--BEGIN

	-- DECLARE @update_Import_Sheet_id int;

 --    SET  @Import_Sheet_id = (SELECT TOP(1) IMPORT_SHEET_ID FROM TB_TEMPLATE_SHEET_DETAILS_IMPORT WHERE TEMPLATE_ID=@TEMPLATE_ID and IMPORT_TYPE=@IMPORT_TYPE and @SHEET_NAME=@SHEET_NAME)

	-- update TB_TEMPLATE_MAPPING_DETAILS_IMPORT SET Attribute_id=@ATTRIBUTE_ID WHERE TEMPLATE_ID=@TEMPLATE_ID AND CAPTION=@CAPTION AND IMPORT_SHEET_ID=@Import_Sheet_id
	--END


	IF(@OPTION='INSERTCAPTION' )
	BEGIN
		
			SET  @Import_Sheet_idforCaption = (SELECT TOP(1) IMPORT_SHEET_ID 
		FROM TB_TEMPLATE_SHEET_DETAILS_IMPORT 
		WHERE IMPORT_TYPE=@IMPORT_TYPE AND TEMPLATE_ID=@TEMPLATE_ID ORDER BY IMPORT_SHEET_ID DESC)

		/*==============To check and insert sheet details for the specific template =============*/
			IF NOT EXISTS(SELECT TEMPLATE_ID 
			 FROM TB_TEMPLATE_SHEET_DETAILS_IMPORT 
			 WHERE  TEMPLATE_ID=@TEMPLATE_ID and IMPORT_TYPE=@IMPORT_TYPE)


	BEGIN
	/*==============To check and insert sheet and import type details for the specific template in sheet details table=============*/
	Print '170'


		 INSERT INTO 
		 TB_TEMPLATE_SHEET_DETAILS_IMPORT
		  VALUES(@TEMPLATE_ID,@IMPORT_TYPE,@SHEET_NAME,1,0,0,GETDATE(),@USER_NAME)
		
		  
	/*==============To check and insert caption name details for the specific template in mapping table=============*/

		 INSERT 
		INTO TB_TEMPLATE_MAPPING_DETAILS_IMPORT 
		VALUES(@TEMPLATE_ID,@Import_Sheet_idforCaption,@CAPTION,@ATTRIBUTE_ID,0,0,0)
    END

	ELSE

	BEGIN

	/*==============To update the caption value for the specific template and the sheet name in mapping table =============*/
	 	Print '189'

		
		print @Import_Sheet_idforCaption

	  UPDATE TB_TEMPLATE_MAPPING_DETAILS_IMPORT
	  SET CAPTION=@CAPTION
	  WHERE TEMPLATE_ID=@TEMPLATE_ID AND IMPORT_SHEET_ID=@Import_Sheet_idforCaption AND ATTRIBUTE_ID=-1

	END

	

	

	END

END


