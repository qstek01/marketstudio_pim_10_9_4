Create table TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP(

GROUP_REFERENCE_ID  int not null Primary key identity(1,1),
GROUP_ID int not null,
TEMPLATE_ID int not null,
IMPORT_SHEET_ID int not null,
SORT_ORDER int not null default (0)

)

Create table TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP_DETAILS(

ATTRIBUTE_GROUP_DETAILS_ID int not null primary key identity(1,1),
GROUP_REFERENCE_ID int not null foreign key (GROUP_REFERENCE_ID) references  TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP (GROUP_REFERENCE_ID),
ATTRIBUTE_ID int not null,
SORT_ORDER int not null default (0)

)



--drop table TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP

--drop table TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP_DETAILS