



ALTER PROCEDURE [dbo].[STP_LS_IMPORT_PICKLIST](
@PICKLIST_ITEM_VALUE VARCHAR(MAX)= 'Lemon',
@PICKLIST_NAME varchar(max)='Flavours',

@CREATED_USER VARCHAR(MAX) = 'demo@questudio.com',

@MODIFIED_USER VARCHAR(MAX) = 'demo@questudio.com',

@SESSION_ID NVARCHAR(500) = '660bb969-ccc2-4630-88ac-a59ca2274585',

@OPTION char(50)='IMPORT'
)
AS
BEGIN
declare @idcount int

											/*-------------------VALIDATION-------------------*/

IF(@OPTION='Validation For DataType')
BEGIN

EXEC('select * from [##IMPORTTEMP'+@SESSION_ID+'] WHERE PICKLIST_DATA_TYPE = ''Numeric''
 AND ISNUMERIC(PICKLIST_ITEM_VALUE)=0
 union 
select * from [##IMPORTTEMP'+@SESSION_ID+'] WHERE PICKLIST_DATA_TYPE = ''Date and Time''
 AND ISDATE(PICKLIST_ITEM_VALUE)=0
 union
 select * from [##IMPORTTEMP'+@SESSION_ID+'] WHERE PICKLIST_DATA_TYPE = ''Hyperlink''
 AND dbo.ValidateURL(PICKLIST_ITEM_VALUE)=0')

END

if(@OPTION='Validation For PickList Id')
begin     
 EXEC('SELECT TEMP.ACTION,TEMP.PICKLIST_ID,TEMP.PICKLIST_NAME,TEMP.PICKLIST_DATA_TYPE,TEMP.PICKLIST_ITEM_ID,TEMP.PICKLIST_ITEM_VALUE
   FROM [##IMPORTTEMP'+@SESSION_ID+'] TEMP
   LEFT JOIN TB_PICKLIST TP
   ON TEMP.PICKLIST_ID= TP.ID WHERE TEMP.PICKLIST_ID IS NOT NULL
EXCEPT
   SELECT TEMP.ACTION,TEMP.PICKLIST_ID,TEMP.PICKLIST_NAME,TEMP.PICKLIST_DATA_TYPE,TEMP.PICKLIST_ITEM_ID,TEMP.PICKLIST_ITEM_VALUE
   FROM [##IMPORTTEMP'+@SESSION_ID+'] TEMP
   RIGHT JOIN TB_PICKLIST TP
   ON TEMP.PICKLIST_ID = TP.ID');
       PRINT 'A'  
end

if(@OPTION='Validation For Delete')
begin
EXEC('SELECT DISTINCT TEMP.ACTION,TEMP.PICKLIST_ID,TEMP.PICKLIST_NAME,TEMP.PICKLIST_DATA_TYPE,TEMP.PICKLIST_ITEM_ID,TEMP.PICKLIST_ITEM_VALUE
FROM [##IMPORTTEMP'+@SESSION_ID+'] TEMP 
JOIN TB_ATTRIBUTE TB 
on TEMP.PICKLIST_NAME = TB.PICKLIST_NAME 
where TEMP.ACTION = ''DELETE''')
end


if(@OPTION ='Validation For Item ID')
begin
--EXEC('SELECT PICKLIST_ITEM_ID
--       FROM [##IMPORTPICKLISTTEMP'+@SESSION_ID+'] where PICKLIST_ITEM_ID is not null
--EXCEPT
--SELECT PICKLIST_ID
--       FROM TB_PICKLIST_DATA');
       
EXEC('SELECT TEMP.ACTION,TEMP.PICKLIST_ID,TEMP.PICKLIST_NAME,TEMP.PICKLIST_DATA_TYPE,TEMP.PICKLIST_ITEM_ID,TEMP.PICKLIST_ITEM_VALUE
   FROM [##IMPORTTEMP'+@SESSION_ID+'] TEMP
   LEFT JOIN TB_PICKLIST_DATA TPD
   ON TEMP.PICKLIST_ITEM_ID= TPD.PICKLIST_ID WHERE TEMP.PICKLIST_ITEM_ID is not null
EXCEPT
   SELECT TEMP.ACTION,TEMP.PICKLIST_ID,TEMP.PICKLIST_NAME,TEMP.PICKLIST_DATA_TYPE,TEMP.PICKLIST_ITEM_ID,TEMP.PICKLIST_ITEM_VALUE
   FROM [##IMPORTTEMP'+@SESSION_ID+'] TEMP
   RIGHT JOIN TB_PICKLIST_DATA TPD
   ON TEMP.PICKLIST_ITEM_ID = TPD.PICKLIST_ID');
   
   
       
       PRINT 'BB'
end


												/*-----------Delete-------------*/

if(@OPTION = 'DELETE')
begin
delete from TB_PICKLIST_DATA where PICKLIST_DATA = @PICKLIST_ITEM_VALUE

if not exists (select * from TB_PICKLIST_DATA where PICKLIST_NAME = @PICKLIST_NAME )
begin
delete from TB_PICKLIST where PICKLIST_NAME = @PICKLIST_NAME
end
end

											/* ----------PICKLIST IMPORT------------*/


if(@OPTION = 'IMPORT')
begin

IF OBJECT_ID('tempdb..##picklistwithid') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##picklistwithid')
	END
	
	IF OBJECT_ID('tempdb..##picklistwithoutid') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##picklistwithoutid')
	END
	
	IF OBJECT_ID('tempdb..##picklistwithoutids') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##picklistwithoutids')
	END
--EXEC('select count(*) as updated from [##IMPORTPICKLISTTEMP'+@SESSION_ID+'] temp join TB_PICKLIST TP on temp.PICKLIST_NAME = TP.PICKLIST_NAME')
--EXEC('select count(*) as inserted from [##IMPORTPICKLISTTEMP'+@SESSION_ID+'] temp join')

create table ##picklistwithid (PICKLIST_ID varchar(max),PICKLIST_NAME varchar(max),PICKLIST_DATA_TYPE varchar(max),PICKLIST_ITEM_ID varchar(max),PICKLIST_ITEM_VALUE varchar(max))
create table ##picklistwithoutid (PICKLIST_ID varchar(max),PICKLIST_NAME varchar(max),PICKLIST_DATA_TYPE varchar(max),PICKLIST_ITEM_ID varchar(max),PICKLIST_ITEM_VALUE varchar(max))
--create table ##picklistwithoutids (PICKLIST_ID varchar(max),PICKLIST_NAME varchar(max),PICKLIST_DATA_TYPE varchar(max),PICKLIST_ITEM_ID varchar(max),PICKLIST_ITEM_VALUE varchar(max))


EXEC('insert into ##picklistwithid select PICKLIST_ID,PICKLIST_NAME,PICKLIST_DATA_TYPE,PICKLIST_ITEM_ID,PICKLIST_ITEM_VALUE from [##IMPORTTEMP'+@SESSION_ID+'] where PICKLIST_ID IS NOT NULL AND PICKLIST_ITEM_ID IS NOT NULL')
EXEC('insert into ##picklistwithoutid select PICKLIST_ID,PICKLIST_NAME,PICKLIST_DATA_TYPE,PICKLIST_ITEM_ID,PICKLIST_ITEM_VALUE from [##IMPORTTEMP'+@SESSION_ID+'] where PICKLIST_ID IS NULL OR PICKLIST_ITEM_ID IS NULL')
 select  PICKLIST_ID,PICKLIST_NAME,PICKLIST_DATA_TYPE,PICKLIST_ITEM_ID,PICKLIST_ITEM_VALUE,dense_RANK() OVER (ORDER BY PICKLIST_NAME)PICKLIST_NAME_dense_rank INTO ##picklistwithoutids FROM ##picklistwithoutid;
if exists(SELECT * FROM ##picklistwithid)
BEGIN
ALTER TABLE ##picklistwithid add Flag varchar(20);

											/* ----------Updation for coloumn-------------*/

update temp set Flag = 'update' from ##picklistwithid temp join TB_PICKLIST TP on temp.PICKLIST_NAME = TP.PICKLIST_NAME

select  COUNT(*) as updated from ##picklistwithid where Flag='update'
select  COUNT(*) as inserted from ##picklistwithid where Flag IS NULL


										/* ----------Import Updation in tables-------------*/

update TP set TP.PICKLIST_DATA_TYPE = temp.PICKLIST_DATA_TYPE from TB_PICKLIST TP join ##picklistwithid temp on TP.PICKLIST_NAME = temp.PICKLIST_NAME 

print 'a';
update TPD set TPD.PICKLIST_DATA = temp.PICKLIST_ITEM_VALUE from TB_PICKLIST_DATA TPD join ##picklistwithid temp on TPD.PICKLIST_NAME = temp.PICKLIST_NAME where TPD.PICKLIST_ID = temp.PICKLIST_ITEM_ID
print 'b';
										/* ----------Import Insertion in tables-------------*/
--if not exists (select * from TB_PICKLIST where PICKLIST_NAME = @PICKLIST_NAME)

--begin
--Exec('insert into TB_PICKLIST select '''',PICKLIST_NAME,PICKLIST_DATA_TYPE,'''+ @CREATED_USER +''',GETDATE(),'''+ @MODIFIED_USER +''',GETDATE() from ##picklistwithid where ')
--end

insert into TB_PICKLIST_DATA select PICKLIST_ITEM_VALUE as PICKLIST_DATA,PICKLIST_NAME,PICKLIST_ID as ID from ##picklistwithid where PICKLIST_ITEM_ID is null
print 'd';

	END
	
	                         /*------------------PickListImport without Id---------------------------------*/
	
if exists(SELECT * FROM ##picklistwithoutids)
begin
print 'a'
ALTER TABLE ##picklistwithoutids add Flag varchar(20);

update temp set Flag = 'update' from ##picklistwithoutids temp join TB_PICKLIST TP on temp.PICKLIST_NAME = TP.PICKLIST_NAME Where temp.PICKLIST_ITEM_ID is Not null

 select  COUNT(*) as updated from ##picklistwithoutids where Flag = 'update' --and PICKLIST_ITEM_ID is not null
 select  COUNT(*) as inserted from ##picklistwithoutids where Flag IS NULL--PICKLIST_ITEM_ID is null

update temp set temp.PICKLIST_ID = TP.ID from ##picklistwithoutids temp join TB_PICKLIST TP on temp.PICKLIST_NAME = TP.PICKLIST_NAME where temp.Flag ='update'
update temp set temp.PICKLIST_ITEM_ID=TPD.PICKLIST_ID FROM ##picklistwithoutids temp JOIN TB_PICKLIST_DATA TPD on temp.PICKLIST_NAME = TPD.PICKLIST_NAME AND temp.PICKLIST_ITEM_VALUE=TPD.PICKLIST_DATA where temp.Flag ='update'

return
if exists(select * from ##picklistwithoutids where Flag is null)
 begin

   set @idcount = (select MAX(ID) from TB_PICKLIST)
   print @idcount

   update ##picklistwithoutids set ##picklistwithoutids.PICKLIST_ID=(@idcount+PICKLIST_NAME_dense_rank)
 end
--from ##picklistwithoutids temp  join TB_PICKLIST_DATA TPD on temp.PICKLIST_NAME=tpd.PICKLIST_NAME where temp.PICKLIST_ITEM_ID is null

									/*--------------------Id Generation Completed----------*/

update TP set TP.PICKLIST_DATA_TYPE = temp.PICKLIST_DATA_TYPE from TB_PICKLIST TP join ##picklistwithoutids temp on TP.PICKLIST_NAME = temp.PICKLIST_NAME where temp.Flag = 'update'
update TPD set TPD.PICKLIST_DATA = temp.PICKLIST_ITEM_VALUE from TB_PICKLIST_DATA TPD join ##picklistwithoutids temp on TPD.PICKLIST_NAME = temp.PICKLIST_NAME where temp.Flag = 'update' and TPD.PICKLIST_ID = temp.PICKLIST_ITEM_ID


Exec('insert into TB_PICKLIST select distinct '''',PICKLIST_NAME,PICKLIST_DATA_TYPE,'''+ @CREATED_USER +''',GETDATE(),'''+ @MODIFIED_USER +''',GETDATE() from ##picklistwithoutids where Flag is null')
print 'g'
insert into TB_PICKLIST_DATA select temp.PICKLIST_ITEM_VALUE as PICKLIST_DATA,temp.PICKLIST_NAME,tp.ID from ##picklistwithoutids temp
join tb_picklist tp on temp.PICKLIST_NAME=tp.PICKLIST_NAME 
 where temp.Flag IS NULL and temp.PICKLIST_ITEM_ID is null
 print 'A'
insert into TB_PICKLIST_DATA select PICKLIST_ITEM_VALUE as PICKLIST_DATA,PICKLIST_NAME,PICKLIST_ID as ID from ##picklistwithoutids where Flag ='update' and PICKLIST_ITEM_ID is null

end




IF OBJECT_ID('tempdb..##picklistwithid') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##picklistwithid')
	END
	
	IF OBJECT_ID('tempdb..##picklistwithoutid') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##picklistwithoutid')
	END
	
	IF OBJECT_ID('tempdb..##picklistwithoutids') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##picklistwithoutids')
	END
	
--	IF OBJECT_ID('tempdb..##IMPORTTEMP'+@SESSION_ID) IS NOT NULL 	
--BEGIN		
--EXEC('DROP TABLE [##IMPORTTEMP'+@SESSION_ID+']')
--END

end



	
	
	
END
	
	
--IF OBJECT_ID('tempdb..##IMPORTPICKLISTTEMP0bf91dfd-18c8-44c8-bdb8-dcf78de92070') IS NOT NULL 
--	BEGIN
--		EXEC('DROP TABLE [##IMPORTPICKLISTTEMP0bf91dfd-18c8-44c8-bdb8-dcf78de92070]')
--	END


