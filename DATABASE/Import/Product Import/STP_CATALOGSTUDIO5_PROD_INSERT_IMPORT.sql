

ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO5_PROD_INSERT_IMPORT]  
@SID VARCHAR(100)='ef9745cf-4c10-4412-b977-d3be40b436b9',  
@ALLOWDUP INT = 0,  
@CUSTOMERID INT=8,  
@CUSTOMERNAME NVARCHAR(500)='demo@questudio.com'  
 AS
BEGIN  
 --begin tran test  
 --rollback tran test  
 SET ANSI_WARNINGS OFF  
 if(@CUSTOMERID=0)
Begin
select @CUSTOMERID=CustomerId from Customer_User where User_Name=@CUSTOMERNAME
print @CUSTOMERID
End

--SELECT PRODUCT_ID FROM [##IMPORTTEMP4c333ffe-cfb3-4d5f-a702-bb62643a50b6]

IF((SELECT COUNT(NAME) FROM TEMPDB.SYS.all_columns WHERE OBJECT_ID = (SELECT OBJECT_ID('TEMPDB..[##IMPORTTEMP'+ @SID +']'))  
AND name LIKE 'CATALOG_ITEM_NO')>0)  
BEGIN  
--DECLARE @SID VARCHAR(100)='c215fb13-fead-4de4-bd55-000cfc05eb93',ARRTok11  
--@ALLOWDUP INT=0  
 DECLARE @TableName sysname  
 DECLARE @IdFieldName sysname  
   DECLARE @SQL varchar(max)   
   DECLARE @SCRIPT_USER varchar(100)  
   DECLARE @PRODUCT_ID_S INT  
   SET @TableName = '##IMPORTTEMP'+ @SID +''  
   SET @IdFieldName ='PRODUCT_ID'  
      
   --PRINT N'TRANSPOSE PROCESS START';  
--TRANSPOSE PROCESS START  
 PRINT 'Setp1'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
   IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##IMPORTTEMPTRANSPOSE'+ @SID +'')  
   BEGIN  
    EXEC('DROP TABLE [##IMPORTTEMPTRANSPOSE'+ @SID +']')  
   END   
   --IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##PRODCOUNTDETAILS'+ @SID +'')  
   --BEGIN  
   -- EXEC('DROP TABLE [##PRODCOUNTDETAILS'+ @SID +']')  
   --END   
   --ALTER TABLE STEVENIMPORT ADD Product_id int,StatusUI varchar(50),Sort_Order int  
    EXEC('DELETE FROM [##IMPORTTEMP'+ @SID +'] WHERE [##IMPORTTEMP'+ @SID +'].CATALOG_NAME IS NULL')  
--PROCESS TO POPULATE PRODUCT ID AND ITS STATUS  
  PRINT 'Setp2'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
 EXEC('UPDATE TT SET TT.product_id=NULL FROM   [##IMPORTTEMP'+ @SID +'] TT JOIN TB_ProdUCT TP ON TP.product_id = TT.product_id  AND TP.FLAG_RECYCLE =''R'' WHERE ISNULL(TT.product_id,'''') <> ''''  ')  

 

--EXEC('SELECT * FROM [##IMPORTTEMP'+ @SID +']')  
  EXEC('
  ;with CTE 
  AS (
   select DISTINCT TPS.STRING_VALUE , TPS.PRODUCT_ID  FROM  TB_PROD_SPECS tps LEFT OUTER JOIN TB_CATALOG_PRODUCT tcp on tps.PRODUCT_ID=tcp.PRODUCT_ID 
   join Customer_Role_Catalog crc on crc.CATALOG_ID=tcp.CATALOG_ID LEFT OUTER join Customer_User cu on crc.CustomerId=cu.CustomerId 
    WHERE TPS.ATTRIBUTE_ID =1 AND CU.user_name = '''+@CUSTOMERNAME+'''   AND TCP.CATALOG_ID = (SELECT TOP 1 CATALOG_ID FROM [##IMPORTTEMP'+ @SID +'])  
    )
   
   UPDATE TT SET TT.product_id=CTE.product_id,TT.StatusUI=''Update'' FROM CTE join [##IMPORTTEMP'+ @SID +'] TT ON TT.PRODUCT_ID = CTE.PRODUCT_ID 
   WHERE cast(TT.CATALOG_ITEM_NO as nvarchar)=cast(CTE.STRING_VALUE as nvarchar) AND TT.CATALOG_ITEM_NO IS NOT NULL   
   AND TT.PRODUCT_ID IS NOT NULL AND TT.StatusUI NOT LIKE ''%Associate%''')  
   print 'update 1'  
    PRINT 'Setp3'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
   
   
   

   EXEC('
   
    ;with CTE 
  AS (
   select DISTINCT TPS.STRING_VALUE , TPS.PRODUCT_ID  FROM  TB_PROD_SPECS tps LEFT OUTER JOIN TB_CATALOG_PRODUCT tcp on tps.PRODUCT_ID=tcp.PRODUCT_ID 
   join Customer_Role_Catalog crc on crc.CATALOG_ID=tcp.CATALOG_ID LEFT OUTER join Customer_User cu on crc.CustomerId=cu.CustomerId 
    WHERE ATTRIBUTE_ID =1 AND CU.user_name = '''+@CUSTOMERNAME+'''  
    )
   UPDATE TT SET TT.StatusUI=''Update'' FROM CTE join [##IMPORTTEMP'+ @SID +'] TT ON TT.PRODUCT_ID = CTE.PRODUCT_ID 
   WHERE cast(TT.CATALOG_ITEM_NO as nvarchar)=cast(CTE.STRING_VALUE as nvarchar)   AND TT.CATALOG_ITEM_NO IS NOT NULL   
   AND TT.PRODUCT_ID IS NOT NULL')  
   print 'update 2'
        
   --------EXEC('UPDATE [##IMPORTTEMP'+ @SID +'] SET [##IMPORTTEMP'+ @SID +'].StatusUI=''Associate'',[##IMPORTTEMP'+ @SID +'].product_id=tps.product_id FROM TB_PROD_SPECS tps  
   --------WHERE cast([##IMPORTTEMP'+ @SID +'].CATALOG_ITEM_NO as nvarchar)=cast(tps.STRING_VALUE as nvarchar) and tps.ATTRIBUTE_ID=1 AND [##IMPORTTEMP'+ @SID +'].CATALOG_ITEM_NO IS NOT NULL   
   --------AND [##IMPORTTEMP'+ @SID +'].PRODUCT_ID IS NULL ') 
   
   
 
  IF(@ALLOWDUP=0)  
   BEGIN

    IF OBJECT_ID('TEMPDB..##DUPLICATEPRODIDS') IS NOT NULL   DROP TABLE  ##DUPLICATEPRODIDS
 EXEC('
     ;with CTE 
  AS (
   select DISTINCT TPS.STRING_VALUE , TPS.PRODUCT_ID  FROM  TB_PROD_SPECS tps LEFT OUTER JOIN TB_CATALOG_PRODUCT tcp on tps.PRODUCT_ID=tcp.PRODUCT_ID 
   join Customer_Role_Catalog crc on crc.CATALOG_ID=tcp.CATALOG_ID LEFT OUTER join Customer_User cu on crc.CustomerId=cu.CustomerId 
    WHERE ATTRIBUTE_ID =1 AND CU.user_name = '''+@CUSTOMERNAME+'''  
    )
    UPDATE TT   SET TT.StatusUI=''Associate'',TT.product_id=tps.product_id FROM [##IMPORTTEMP'+ @SID +'] TT JOIN  CTE tps ON cast(TT.CATALOG_ITEM_NO as nvarchar)=cast(tps.STRING_VALUE as nvarchar)  LEFT   OUTER  JOIN TB_PRODUCT TP
 ON TP.product_id = tps.product_id AND TP.FLAG_RECYCLE =''A''  
   WHERE    TT.CATALOG_ITEM_NO IS NOT NULL   
   AND TT.PRODUCT_ID IS NULL ')  



 --RETURN

   end
   --RETURN
   PRINT 'Setp4'
      
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
  print 'Associate'
    EXEC('
    UPDATE [##IMPORTTEMP'+ @SID +'] SET [##IMPORTTEMP'+ @SID +'].StatusUI=''Insert'' WHERE [##IMPORTTEMP'+ @SID +'].CATALOG_ITEM_NO IS NOT NULL   
   AND [##IMPORTTEMP'+ @SID +'].PRODUCT_ID IS NULL AND isnull(StatusUI,'''') not like ''Update''')  
    


   print 'Update FlagRecycle'
      --EXEC(' UPDATE tt set tt.PRODUCT_ID=null,tt.StatusUI=''Insert'' from [##IMPORTTEMP'+ @SID +']  tt join TB_PRODUCT tp on tt.PRODUCT_ID=tp.PRODUCT_ID and tp.FLAG_RECYCLE=''R''')
     print 'Insert'
	   
--	----------------------------------------------CODE ADDED BY MOHAN----------------------------------------

--EXEC('SELECT * INTO [##PRODCOUNTDETAILS'+ @SID +'] FROM [##IMPORTTEMP'+ @SID +']')

EXEC('UPDATE A SET A.StatusUI = B.StatusUI FROM [##IMPORTALLDATA'+ @SID +'] A JOIN [##IMPORTTEMP'+ @SID +'] B ON A.CATALOG_NAME=B.CATALOG_NAME AND A.CATEGORY_NAME=B.CATEGORY_NAME AND
A.CATALOG_ITEM_NO = B.CATALOG_ITEM_NO')
--	EXEC('UPDATE A SET A.StatusUI=''Update'' from [##IMPORTTEMP'+ @SID +'] A 
--JOIN TB_PROD_SPECS B ON A.CATALOG_ITEM_NO = B.STRING_VALUE 
--JOIN TB_PROD_FAMILY D ON A.FAMILY_ID = D.FAMILY_ID
--JOIN TB_PRODUCT C ON B.PRODUCT_ID = C.PRODUCT_ID WHERE B.ATTRIBUTE_ID=1 AND C.FLAG_RECYCLE=''A'' and A.SUBFAMILY_ID is null')

--EXEC('UPDATE A SET A.StatusUI=''Update'' from [##IMPORTTEMP'+ @SID +'] A 
--JOIN TB_PROD_SPECS B ON A.CATALOG_ITEM_NO = B.STRING_VALUE 
--JOIN TB_PROD_FAMILY D ON A.SUBFAMILY_ID = D.FAMILY_ID
--JOIN TB_PRODUCT C ON B.PRODUCT_ID = C.PRODUCT_ID WHERE B.ATTRIBUTE_ID=1 AND C.FLAG_RECYCLE=''A'' and A.SUBFAMILY_ID is not null')
------------------------------------------------CODE ADDED BY MOHAN----------------------------------------     
     EXEC('
     UPDATE PS SET PS.STRING_VALUE = IT.CATALOG_ITEM_NO FROM TB_PROD_SPECS PS  
    JOIN [##IMPORTTEMP'+ @SID +'] IT ON IT.PRODUCT_ID = PS.PRODUCT_ID AND PS.ATTRIBUTE_ID = 1')   
   --UPDATE [##IMPORTTEMP'+ @SID +'] SET [##IMPORTTEMP'+ @SID +'].StatusUI='UPDATE' WHERE [##IMPORTTEMP'+ @SID +'].CATALOG_ITEM_NO IS NOT NULL AND [##IMPORTTEMP'+ @SID +'].PRODUCT_ID IS NOT NULL AND isnull(StatusUI,'') not like 'INSERT'  
 EXEC('SELECT * FROM [##IMPORTTEMP'+ @SID +']')  
 Print 'TEST Prod2'     
 exec('IF not EXISTS(SELECT * FROM tempdb.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME like ''ID'' and TABLE_NAME LIKE ''##IMPORTTEMP'+ @SID +'%'')  
BEGIN  
ALTER TABLE [##IMPORTTEMP'+ @SID +'] ADD ID INT IDENTITY(1,1)
END')  

   PRINT 'Setp5'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))    
      if((select COUNT(*) from TB_PRODUCT)=0)  
   begin  
   set @PRODUCT_ID_S = 1  
   end  
     
   else  
   begin  
   SELECT @PRODUCT_ID_S=MAX(product_id)+1 from TB_PRODUCT  
      end  
    Print 'Update/Insert/Association - Completed'    
   
   IF(@ALLOWDUP=0)  
   BEGIN  
   EXEC('CREATE TABLE [##TempProductId'+ @SID +'](CATALOG_ITEM_NO VARCHAR(100))')  
     
   EXEC('INSERT INTO [##TempProductId'+ @SID +'](CATALOG_ITEM_NO)SELECT DISTINCT CATALOG_ITEM_NO FROM [##IMPORTTEMP'+ @SID +'] WHERE StatusUI=''Insert''')  
   DECLARE @Stat VARCHAR(500)    
   SET @Stat = 'ALTER TABLE [##TempProductId'+ @SID +']  ADD PRODUCT_ID INT IDENTITY(' + CAST(ISNULL(@PRODUCT_ID_S,0) AS NVARCHAR) + ', 1);'   
   EXEC (@Stat)  
     
   exec('UPDATE [##IMPORTTEMP'+ @SID +'] SET [##IMPORTTEMP'+ @SID +'].Product_id = TPI.PRODUCT_ID,[##IMPORTTEMP'+ @SID +'].StatusUI=''Insert'' from [##TempProductId'+ @SID +'] TPI    
   where [##IMPORTTEMP'+ @SID +'].CATALOG_ITEM_NO = TPI.CATALOG_ITEM_NO ')  
 
   EXEC('DROP TABLE [##TempProductId'+ @SID +']')  
   END  
   ELSE  
   BEGIN  
   exec('CREATE TABLE [##TempProductId'+ @SID +'](ID INT,CATALOG_ITEM_NO VARCHAR(100))')  
   --SELECT ID,CATALOG_ITEM_NO into [##TempProductId'+ @SID +'] from [##IMPORTTEMP'+ @SID +'] WHERE 1=2-- StatusUI='Insert'  
   DECLARE @Statement VARCHAR(500)    
   SET @Statement = 'ALTER TABLE [##TempProductId'+ @SID +']  ADD PRODUCT_ID INT IDENTITY(' + CAST(ISNULL(@PRODUCT_ID_S,0) AS NVARCHAR) + ', 1);'   
   exec (@Statement)  
     
   exec('INSERT INTO [##TempProductId'+ @SID +'](CATALOG_ITEM_NO,ID) SELECT CATALOG_ITEM_NO,ID FROM [##IMPORTTEMP'+ @SID +']')  
  
     exec('UPDATE [##IMPORTTEMP'+ @SID +'] SET [##IMPORTTEMP'+ @SID +'].StatusUI=''Update'' 
   where [##IMPORTTEMP'+ @SID +'].Product_id is not null and [##IMPORTTEMP'+ @SID +'].Product_id<>''''') 

   exec('UPDATE [##IMPORTTEMP'+ @SID +'] SET [##IMPORTTEMP'+ @SID +'].Product_id = TPI.PRODUCT_ID,[##IMPORTTEMP'+ @SID +'].StatusUI=''Insert'' from [##TempProductId'+ @SID +'] TPI   
   where [##IMPORTTEMP'+ @SID +'].CATALOG_ITEM_NO = TPI.CATALOG_ITEM_NO AND [##IMPORTTEMP'+ @SID +'].ID = TPI.ID and ([##IMPORTTEMP'+ @SID +'].Product_id is null or [##IMPORTTEMP'+ @SID +'].Product_id='''')')  

    
     exec('DROP TABLE [##TempProductId'+ @SID +']')  

   END
 PRINT 'Setp6'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
Print 'TEST Prod3'        
   -- EXEC('UPDATE [##IMPORTTEMP'+ @SID +'] SET [##IMPORTTEMP'+ @SID +'].StatusUI=''INSERT'' FROM TB_PROD_SPECS TPS  
   --JOIN [##IMPORTTEMP'+ @SID +'] SI on TPS.STRING_VALUE=SI.CATALOG_ITEM_NO AND TPS.ATTRIBUTE_ID=1  
   --AND SI.CATALOG_ITEM_NO IS NOT NULL AND SI.STATUSUI NOT LIKE ''%INSERT%''')  
---PROCESS TO POPULATE PRODUCT SORT ORDER  
  
   EXEC('SELECT Product_ID,Family_id,ROW_NUMBER() OVER(PARTITION BY Family_id ORDER BY rowimportID ) AS Sortorder into [##TempFamilySortOrder'+ @SID +']  
   FROM [##IMPORTTEMP'+ @SID +'] WHERE [##IMPORTTEMP'+ @SID +'].CATALOG_ITEM_NO IS NOT NULL AND [##IMPORTTEMP'+ @SID +'].PRODUCT_ID IS NOT NULL ORDER BY rowimportID   
   ')  
   ---UPDATE SORTORDER BASED ON EXISTING PRODUCT SORTORDER--START  
     
   --EXEC('SELECT MAX(SORT_ORDER) AS SORT_ORDER,TPF.FAMILY_ID INTO [##TEMPSORTMAX'+ @SID +'] FROM TB_PROD_FAMILY TPF,[##TempFamilySortOrder'+ @SID +'] TSO WHERE   
   --TSO.PRODUCT_ID=TPF.PRODUCT_ID AND TSO.FAMILY_ID=TPF.FAMILY_ID GROUP BY TPF.FAMILY_ID')  
  
   --EXEC('UPDATE [##TempFamilySortOrder'+ @SID +'] SET SORTORDER=SORTORDER+SORT_ORDER  FROM [##TEMPSORTMAX'+ @SID +'] TSM WHERE  
   --[##TempFamilySortOrder'+ @SID +'].FAMILY_ID=TSM.FAMILY_ID ')
   
     EXEC('SELECT TPF.SORT_ORDER,TPF.FAMILY_ID,TSO.PRODUCT_ID INTO [##TEMPSORTMAX'+ @SID +'] FROM TB_PROD_FAMILY TPF,[##TempFamilySortOrder'+ @SID +'] TSO WHERE   
   TSO.PRODUCT_ID=TPF.PRODUCT_ID AND TSO.FAMILY_ID=TPF.FAMILY_ID ORDER BY TSO.Sortorder')  

 
 
   EXEC('UPDATE [##TempFamilySortOrder'+ @SID +'] SET SORTORDER=TSM.SORT_ORDER  FROM [##TEMPSORTMAX'+ @SID +'] TSM WHERE  
   [##TempFamilySortOrder'+ @SID +'].FAMILY_ID=TSM.FAMILY_ID and [##TempFamilySortOrder'+ @SID +'].PRODUCT_ID=TSM.PRODUCT_ID ') 
  
     
   ---UPDATE SORTORDER BASED ON EXISTING PRODUCT SORTORDER--END  
     
   EXEC('UPDATE [##IMPORTTEMP'+ @SID +'] SET [##IMPORTTEMP'+ @SID +'].Sort_Order=TFS.SORTORDER FROM [##TempFamilySortOrder'+ @SID +'] TFS  
   WHERE [##IMPORTTEMP'+ @SID +'].Product_id=TFS.Product_id and [##IMPORTTEMP'+ @SID +'].Family_id=TFS.Family_id')  
     
   EXEC('DROP TABLE [##TEMPSORTMAX'+ @SID +']   
            DROP TABLE [##TempFamilySortOrder'+ @SID +']')  
  PRINT 'Setp7'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))              
---NEED TO POPULATE NEW PRODUCT ID AND EXISTING PRODUCTID, SORT ORDER FOR FAMILY PRODUCT   
 EXEC('CREATE TABLE [##IMPORTTEMPTRANSPOSE'+ @SID +']([Catalog_id] int,[Product_id] int,[ColumnName]  nvarchar(4000),[Value] sql_variant,FAMILY_ID int,SUBFAMILY_ID int,ATTRIBUTE_ID int,ATTRIBUTE_DATATYPE nvarchar(50),ATTRIBUTE_TYPE tinyint,ATTR_SORT_ORDER int,family_name varchar(500))')  
Print 'TEST Prod4'   
---TO DELETE THE UNWANTED COLUMNS FROM EXCEL---START  
--IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE NAME='[##TEMPCOLREJECTIONLIST'+ @SID +']')  
--   BEGIN  
--    DROP TABLE [##TEMPCOLREJECTIONLIST'+ @SID +']  
--   END   
   IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TEMPCOLREJECTIONLIST'+ @SID +'')  
   BEGIN  
    EXEC('DROP TABLE [##TEMPCOLREJECTIONLIST'+ @SID +']')  
   END   
     
   EXEC('CREATE TABLE [##TEMPCOLREJECTIONLIST'+ @SID +'](COLUMNNAME VARCHAR(100))')  
  
  PRINT 'Setp8'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))   
 DECLARE @SQLC VARCHAR(MAX)  
  SET @SQLC='SELECT NULL AS CNT,''NULL'' AS COLUMNNAME '  
  SELECT  @SQLC=@SQLC + 'UNION SELECT COUNT([' + NAME + ']) AS CNT,''' + REPLACE(NAME,'''','''''') + ''' AS COLUMNNAME  FROM [##IMPORTTEMP'+ @SID +'] ' + CHAR(10)  
  FROM tempdb.SYS.COLUMNS   
  WHERE OBJECT_ID IN(SELECT OBJECT_ID FROM tempdb.SYS.OBJECTS WHERE NAME='##IMPORTTEMP'+ @SID +'')  
 
  EXEC('INSERT INTO [##TEMPCOLREJECTIONLIST'+ @SID +'] SELECT COLUMNNAME FROM(' + @SQLC +') A WHERE CNT>=0')  
  --PRINT('INSERT INTO [##TEMPCOLREJECTIONLIST'+ @SID +'] SELECT COLUMNNAME FROM(' + @SQLC +') A WHERE CNT>2')  
   --PRINT @SQLC;  

Print 'TEST Prod5'     
---TO DELETE THE UNWANTED COLUMNS FROM EXCEL---START  
  PRINT 'Setp9'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
  SET @SQL='SELECT NULL AS CATALOG_ID,NULL AS PRODUCT_ID,NULL AS COLUMNAME,NULL AS VALUE,NULL AS FAMILY_ID ,null as family_name'  
  DECLARE @QRY_OUT NVARCHAR(MAX) = 'SELECT @retvalOUT = @retvalOUT + '' UNION ALL SELECT CATALOG_ID,PRODUCT_ID, ''''''+   REPLACE(COLUMN_NAME, '''''''', '''''''''''')   +'''''',CONVERT(sql_variant, cast(''
  + ''['' + COLUMN_NAME + ''] as nvarchar(4000))),FAMILY_ID,family_name FROM [##IMPORTTEMP'+ @SID +'] ''  
  + CHAR(10)  
  FROM  
  tempdb.INFORMATION_SCHEMA.COLUMNS  
  WHERE TABLE_NAME = ''##IMPORTTEMP'+ @SID +'''  
  AND COLUMN_NAME <> ''PRODUCT_ID''  
  AND COLUMN_NAME <> ''CATALOG_ID''  
  AND COLUMN_NAME  IN(SELECT COLUMNNAME FROM [##TEMPCOLREJECTIONLIST'+ @SID +'])  
  ORDER BY COLUMN_NAME'  
   DECLARE @ParmDefinition NVARCHAR(1000) = '@retvalOUT NVARCHAR(MAX) OUTPUT';  
   select @QRY_OUT
   EXEC sp_executesql @QRY_OUT, @ParmDefinition, @retvalOUT=@SQL OUTPUT;  
   Print 'TEST Prod5A'   
   exec( 'Insert into [##IMPORTTEMPTRANSPOSE'+ @SID +'](CATALOG_ID,PRODUCT_ID,COLUMNNAME,VALUE,FAMILY_ID,family_name) ' + @sql)  
   
   EXEC('DELETE FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE PRODUCT_ID IS NULL')  
   EXEC('DROP TABLE [##TEMPCOLREJECTIONLIST'+ @SID +']')    
   Print 'TEST Prod5B'   
     DECLARE @MAXATTRID VARCHAR(500)    
   SET @MAXATTRID = (SELECT MAX(ATTRIBUTE_ID) from TB_ATTRIBUTE)  
    EXEC('SELECT DISTINCT COLUMNNAME AS NEW_ATTRIBUTE,NULL AS ATTRIBUTE_ID,[##ATTRIBUTETEMP'+ @SID +'].ATTRIBUTE_TYPE INTO [##TEMPATTRIBUTE'+ @SID +']   
    FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] join [##ATTRIBUTETEMP'+ @SID +']   
    on [##IMPORTTEMPTRANSPOSE'+ @SID +'].ColumnName=[##ATTRIBUTETEMP'+ @SID +'].ATTRIBUTE_NAME   
    WHERE COLUMNNAME NOT IN(SELECT   
    ATTRIBUTE_NAME FROM TB_ATTRIBUTE TA join CUSTOMERATTRIBUTE CA on TA.ATTRIBUTE_ID=CA.ATTRIBUTE_ID where CUSTOMER_ID='+@CUSTOMERID+') AND COLUMNNAME NOT IN(''CATALOG_ITEM_NO'',''CATALOG_NAME'',''CATALOG_VERSION'',  
    ''CATEGORY_ID'',''CATEGORY_NAME'',''FAMILY_ID'',''FAMILY_NAME'',''SLNO'',''SORT_ORDER'',''STATUSUI'',''CATALOG_ID'',''SUBFAMILY_ID'',''SUBFAMILY_NAME'',  
    ''CATALOG_DESCRIPTION'',''CATEGORY_SHORT_DESC'',''CATEGORY_IMAGE_FILE'',''CATEGORY_IMAGE_NAME'',''CATEGORY_IMAGE_TYPE'',''CATEGORY_IMAGE_FILE2'',  
    ''CATEGORY_IMAGE_NAME2'',''CATEGORY_IMAGE_TYPE2'',''CATEGORY_CUSTOM_NUM_FIELD1'',''CATEGORY_CUSTOM_NUM_FIELD2'',''CATEGORY_CUSTOM_NUM_FIELD3'',  
    ''CATEGORY_CUSTOM_TEXT_FIELD1'',''CATEGORY_CUSTOM_TEXT_FIELD2'',''CATEGORY_CUSTOM_TEXT_FIELD3'',''FOOT_NOTES''  
    ,''STATUS'',''FAMILY_SORT'',''PRODUCT_SORT'',''REMOVE_PRODUCT'',''FAMILY_STATUS'',''SFSORT_ORDER'',''STATUSUI'',''SORT_ORDER''  
    ) AND  
    COLUMNNAME NOT LIKE ''SUBCATID_%'' AND COLUMNNAME NOT LIKE ''SUBCATNAME_%'' AND COLUMNNAME NOT LIKE ''F_'' AND   COLUMNNAME NOT LIKE ''PUBLISH2%'' and
    COLUMNNAME NOT LIKE ''F__'' AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
    ')  
    PRINT 'Setp10'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))    
   Print 'TEST Prod5C'        
    PRINT('  
    INSERT INTO TB_ATTRIBUTE(ATTRIBUTE_NAME,ATTRIBUTE_TYPE,PUBLISH2PDF,PUBLISH2PORTAL,PUBLISH2EXPORT,PUBLISH2PRINT,PUBLISH2WEB,PUBLISH2CDROM,PUBLISH2ODP,USE_PICKLIST  
    ,ATTRIBUTE_DATATYPE  
    ,ATTRIBUTE_DATAFORMAT,CREATE_BY_DEFAULT,FLAG_RECYCLE,CAPTION)  
    SELECT NEW_ATTRIBUTE,[##TEMPATTRIBUTE'+ @SID +'].ATTRIBUTE_TYPE,1,1,1,1,1,1,1,0,CASE WHEN ATTRIBUTE_TYPE = 10 THEN ''Date and Time''    
    WHEN ATTRIBUTE_TYPE = 4 THEN ''Number(13,6)'' ELSE ''Text'' END  
    ,CASE WHEN ATTRIBUTE_TYPE = 10 THEN ''(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?
!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)''    
    WHEN ATTRIBUTE_TYPE = 4 THEN ''^-{0,1}?\d*\.{0,1}\d{0,6}$'' ELSE ''^[ 0-9a-zA-Z\r\n\x20-\x7E\u0000-\uFFFF]*$'' END ,0 FROM [##TEMPATTRIBUTE'+ @SID +'] WHERE NEW_ATTRIBUTE NOT LIKE ''%STATUS%''   
    AND NEW_ATTRIBUTE NOT LIKE ''%SORT_ORDER%'' , ''A'', NEW_ATTRIBUTE  
    ')  
    EXEC('  
    INSERT INTO TB_ATTRIBUTE(ATTRIBUTE_NAME,ATTRIBUTE_TYPE,PUBLISH2PDF,PUBLISH2PORTAL,PUBLISH2EXPORT,PUBLISH2PRINT,PUBLISH2WEB,PUBLISH2CDROM,PUBLISH2ODP,USE_PICKLIST  
    ,ATTRIBUTE_DATATYPE  
    ,ATTRIBUTE_DATAFORMAT,CREATE_BY_DEFAULT,FLAG_RECYCLE,CAPTION)  
    SELECT NEW_ATTRIBUTE,[##TEMPATTRIBUTE'+ @SID +'].ATTRIBUTE_TYPE,1,1,1,1,1,1,1,0,CASE WHEN ATTRIBUTE_TYPE = 10 THEN ''Date and Time''    
    WHEN ATTRIBUTE_TYPE = 4 THEN ''Number(13,6)'' ELSE ''Text'' END  
    ,CASE WHEN ATTRIBUTE_TYPE = 10 THEN ''(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?
!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)''    
    WHEN ATTRIBUTE_TYPE = 4 THEN ''^-{0,1}?\d*\.{0,1}\d{0,6}$'' ELSE ''^[ 0-9a-zA-Z\r\n\x20-\x7E\u0000-\uFFFF]*$'' END ,0,''A'', NEW_ATTRIBUTE FROM [##TEMPATTRIBUTE'+ @SID +'] WHERE NEW_ATTRIBUTE NOT LIKE ''%STATUS%''   
    AND NEW_ATTRIBUTE NOT LIKE ''%SORT_ORDER%''   and [##TEMPATTRIBUTE'+ @SID +'].ATTRIBUTE_TYPE<>0  
    ')  
    Print 'TEST Prod5D'  
     PRINT 'Setp11'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))   
    --exec ('select * into ##attr from [##TEMPATTRIBUTE'+ @SID +']')  
   -- exec ('INSERT INTO CUSTOMERATTRIBUTE(ATTRIBUTE_ID,CUSTOMER_ID,CREATED_DATE,MODIFIED_DATE,CREATED_USER,MODIFIED_USER)  
   --SELECT ta.ATTRIBUTE_ID,'+@CUSTOMERID+',getdate(),getdate(),'''+@CUSTOMERNAME+''','''+@CUSTOMERNAME+''' from TB_ATTRIBUTE ta   
   --join [##TEMPATTRIBUTE'+ @SID +'] tt on tt.NEW_ATTRIBUTE=ta.Attribute_Name where ta.ATTRIBUTE_ID>'+@MAXATTRID+'')  

	  exec ('INSERT INTO CUSTOMERATTRIBUTE(ATTRIBUTE_ID,CUSTOMER_ID,CREATED_DATE,MODIFIED_DATE,CREATED_USER,MODIFIED_USER)  
   SELECT ATTRIBUTE_ID,Customer_ID,getdate(),getdate(),'''+@CUSTOMERNAME+''','''+@CUSTOMERNAME+''' from (
   select ta.ATTRIBUTE_ID,'+@CUSTOMERID+' as Customer_ID from TB_ATTRIBUTE ta   
   join [##TEMPATTRIBUTE'+ @SID +'] tt on tt.NEW_ATTRIBUTE=ta.Attribute_Name  and ta.ATTRIBUTE_ID not in (select ATTRIBUTE_ID from CUSTOMERATTRIBUTE  where CUSTOMER_ID<>'+@CUSTOMERID+')
   except 
   select ATTRIBUTE_ID,CUSTOMER_ID from CUSTOMERATTRIBUTE where CUSTOMER_ID='+@CUSTOMERID+'
   ) as Temp 

   ')  
select 234  
print'ARRTok'  
print @MAXATTRID  
 PRINT 'Setp12'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
exec (' SELECT ta.ATTRIBUTE_ID,'+@CUSTOMERID+',getdate(),getdate(),'''+@CUSTOMERNAME+''','''+@CUSTOMERNAME+''' from TB_ATTRIBUTE ta   
join [##TEMPATTRIBUTE'+ @SID +'] tt on tt.NEW_ATTRIBUTE=ta.Attribute_Name where ta.ATTRIBUTE_ID>'+@MAXATTRID+'')  
print '4r'  
     EXEC('  
     UPDATE [##TEMPATTRIBUTE'+ @SID +'] SET [##TEMPATTRIBUTE'+ @SID +'].ATTRIBUTE_ID=TA.ATTRIBUTE_ID FROM TB_ATTRIBUTE TA join CUSTOMERATTRIBUTE CA on TA.ATTRIBUTE_ID=CA.ATTRIBUTE_ID WHERE  
     [##TEMPATTRIBUTE'+ @SID +'].NEW_ATTRIBUTE=TA.ATTRIBUTE_NAME and CUSTOMER_ID='+@CUSTOMERID+'  
     ')  
print'ARRTok11'  
print @MAXATTRID    
 PRINT 'Setp13'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 

   --=============================**************Sort _ORDER***********************==========================[To get the order based attributeid and its name and set rownumber.]

   IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TEMPORDER')  
   BEGIN  
    EXEC('DROP TABLE ##TEMPORDER')  
   END  
	exec('select *,row_number() over ( order by (select 100) ) as SORT_ORDER, 0 as ATTRIBUTE_ID  into ##TEMPORDER from [##ATTRIBUTETEMP'+ @SID +']')
	exec('update ##TEMPORDER set ##TEMPORDER.ATTRIBUTE_ID = tt.ATTRIBUTE_ID  from TB_ATTRIBUTE tt inner join ##TEMPORDER p on p.attribute_name = tt.ATTRIBUTE_NAME')

	--=============================**************Sort _ORDER***********************==========================
--return     
    --UPDATING ATTRIBUTE_ID,ATTRIBUTE_DATATYPE,FAMILY_ID  
    --DECLARE @SID VARCHAR(100)='551dac12-af3e-4352-bd04-aea8a2367e8f'  
   EXEC('  
   UPDATE [##IMPORTTEMPTRANSPOSE'+ @SID +'] SET [##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTRIBUTE_ID = TA.ATTRIBUTE_ID,[##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTRIBUTE_DATATYPE = TA.ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE TA join CUSTOMERATTRIBUTE CA on TA.ATTRIBUTE_ID
=CA.ATTRIBUTE_ID WHERE TA.ATTRIBUTE_NAME=[##IMPORTTEMPTRANSPOSE'+ @SID +'].COLUMNNAME   
   AND TA.ATTRIBUTE_TYPE NOT IN(11,12,13) and CUSTOMER_ID='+@CUSTOMERID+'  
   UPDATE [##IMPORTTEMPTRANSPOSE'+ @SID +'] SET [##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTRIBUTE_ID = 1,[##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTRIBUTE_DATATYPE = ''Text(6000)'' WHERE [##IMPORTTEMPTRANSPOSE'+ @SID +'].COLUMNNAME=''CATALOG_ITEM_NO''  
   UPDATE [##IMPORTTEMPTRANSPOSE'+ @SID +'] SET [##IMPORTTEMPTRANSPOSE'+ @SID +'].FAMILY_ID=SI.FAMILY_ID FROM [##IMPORTTEMP'+ @SID +'] SI WHERE [##IMPORTTEMPTRANSPOSE'+ @SID +'].PRODUCT_ID=SI.PRODUCT_ID and [##IMPORTTEMPTRANSPOSE'+ @SID +'].family_name
   =SI.family_name  
   UPDATE [##IMPORTTEMPTRANSPOSE'+ @SID +'] SET [##IMPORTTEMPTRANSPOSE'+ @SID +'].SUBFAMILY_ID=SI.SUBFAMILY_ID FROM [##IMPORTTEMP'+ @SID +'] SI WHERE [##IMPORTTEMPTRANSPOSE'+ @SID +'].PRODUCT_ID=SI.PRODUCT_ID 
   and  [##IMPORTTEMPTRANSPOSE'+ @SID +'].FAMILY_ID=SI.FAMILY_ID   
   UPDATE [##IMPORTTEMPTRANSPOSE'+ @SID +'] SET [##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTRIBUTE_TYPE =ATT.ATTRIBUTE_TYPE FROM [##ATTRIBUTETEMP'+ @SID +'] ATT WHERE [##IMPORTTEMPTRANSPOSE'+ @SID +'].ColumnName =ATT.ATTRIBUTE_NAME   
   ')  
    
    
   EXEC('  
       INSERT INTO TB_CATALOG_ATTRIBUTES(CATALOG_ID,ATTRIBUTE_ID)  
    SELECT DISTINCT [##IMPORTTEMP'+ @SID +'].CATALOG_ID,ATTRIBUTE_ID  FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'],[##IMPORTTEMP'+ @SID +'] WHERE ATTRIBUTE_ID IS NOT NULL AND ATTRIBUTE_TYPE NOT IN(11,12,13)  
    EXCEPT SELECT CATALOG_ID,ATTRIBUTE_ID FROM TB_CATALOG_ATTRIBUTES WHERE CATALOG_ID IN(SELECT DISTINCT CATALOG_ID FROM [##IMPORTTEMP'+ @SID +']) ')  
    EXEC('DROP TABLE [##TEMPATTRIBUTE'+ @SID +']')  
    ----------------------------------------  
  ---- GET LIST OF ATTRIBUTES -----------------  
  exec('IF OBJECT_ID(''TEMPDB..##ATTRIBUTELIST'') IS NOT NULL   DROP TABLE  ##ATTRIBUTELIST')    
  exec('IF OBJECT_ID(''TEMPDB..##MISSINGATTRIBUTESLIST'') IS NOT NULL   DROP TABLE  ##MISSINGATTRIBUTESLIST')        
  EXEC('SELECT DISTINCT ATTRIBUTE_ID INTO ##ATTRIBUTELIST FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'],[##IMPORTTEMP'+ @SID +'] WHERE ATTRIBUTE_ID IS NOT NULL AND ATTRIBUTE_TYPE NOT IN(11,12,13)')   
  Select TA.ATTRIBUTE_ID INTO ##MISSINGATTRIBUTESLIST FROM TB_ATTRIBUTE TA JOIN CUSTOMERATTRIBUTE CA ON CA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID WHERE TA.ATTRIBUTE_ID NOT IN
  (select ATTRIBUTE_ID from ##ATTRIBUTELIST ) AND TA.CREATE_BY_DEFAULT = 1  
 -------DATE FORMAT VALIDATION----------  
    DECLARE @CHKDAT int  
    DECLARE @ERRORFLG int=0  
    --select @CHKDAT  = COUNT(*) FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] A where a.Value is null and a.ATTRIBUTE_DATATYPE='Date and Time'  
    SET @QRY_OUT = 'SET @retvalOUT = (SELECT COUNT(*) FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] A where a.Value = '''' and a.ATTRIBUTE_DATATYPE=''Date and Time'')'  
    SET @ParmDefinition = '@retvalOUT INT OUTPUT';  
    EXEC sp_executesql @QRY_OUT, @ParmDefinition, @retvalOUT=@CHKDAT OUTPUT;  
      
    --print @CHKDAT  
    --IF(@CHKDAT >0)  
    -- BEGIN  
    --  SET @ERRORFLG=1  
    --  RAISERROR('Date Format Should be in MM/DD/YYYY',16,1)  
    --  RETURN -1  
    -- END  
 -----------------  
--CREATING PRODUCTS  
--DECLARE @STATEMENT VARCHAR(500)    
 PRINT 'Setp14'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
--------------------------------------------------  
IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='[##TEMP'+ @SID +']')  
    BEGIN  
    exec('DROP TABLE [##TEMP'+ @SID +']')  
    END   
      
    IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='[##TB_PRODUCT'+ @SID +']')  
    BEGIN  
    exec('drop table [##TB_PRODUCT'+ @SID +']')  
    END   
exec('CREATE TABLE [##TB_PRODUCT'+ @SID +'](PRODUCT_ID INT,CATEGORY_ID NVARCHAR(50))')  
DECLARE @CNT varchar(50)   
DECLARE @STATEMENT1 varchar(max)='';  
select @CNT=COUNT(*) from TEMPDB.sys.tables t inner join TEMPDB.SYS.columns c on c.object_id=t.object_id  where t.name='##IMPORTTEMP'+ @SID +'' and c.name like 'SUBCATID_L%'  
SET @STATEMENT1 = '';  
  
    
 DECLARE @CHKCNT3 INT = @CNT;  
 SET @STATEMENT1 = @STATEMENT1 + 'INSERT INTO [##TB_PRODUCT'+ @SID +']([PRODUCT_ID],[CATEGORY_ID])'  
 WHILE(@CHKCNT3 <> 0)  
  BEGIN  
   SET @STATEMENT1 = @STATEMENT1 + ' SELECT DISTINCT [product_id],[SUBCATID_L'+ CONVERT(VARCHAR(MAX),@CHKCNT3) +'] FROM [##IMPORTTEMP'+ @SID +'] WHERE   '  
    DECLARE @TEMP_COUNT3 INT = 0;  
   WHILE(@TEMP_COUNT3 <> @CHKCNT3)  
    BEGIN  
     SET @STATEMENT1 = @STATEMENT1 + '[SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT3 + 1)) +'] IS NOT NULL AND'  
     SET @TEMP_COUNT3 = @TEMP_COUNT3 + 1;  
       
    END  
   --SET @TEMP_COUNT = @TEMP_COUNT + 1;  
   WHILE(@TEMP_COUNT3 <> @CNT)  
    BEGIN  
     SET @STATEMENT1 = @STATEMENT1 + ' [SUBCATID_L' + CONVERT(VARCHAR(MAX),(@TEMP_COUNT3 + 1)) +'] IS NULL AND'  
     SET @TEMP_COUNT3 = @TEMP_COUNT3 + 1;  
    END  
   SET @STATEMENT1 = @STATEMENT1 + ' StatusUI='+''''+'Insert'+''' UNION ';  
   SET @CHKCNT3 = @CHKCNT3 - 1;  
  END  
 SET @STATEMENT1 = @STATEMENT1 + 'SELECT DISTINCT [product_id],[CATEGORY_ID] FROM [##IMPORTTEMP'+ @SID +'] WHERE StatusUI=''Insert'''  
 SET @CHKCNT3 =@CNT;  
 WHILE(@CHKCNT3 <> 0)  
  BEGIN  
  SET @STATEMENT1 = @STATEMENT1 + ' AND [SUBCATID_L' + CONVERT(VARCHAR(MAX),@CHKCNT3 )+'] IS NULL '  
  SET @CHKCNT3 = @CHKCNT3 - 1;  
  END  
   EXEC (@STATEMENT1);  
  exec('SELECT *,ROW_NUMBER() OVER(PARTITION BY PRODUCT_ID ORDER BY PRODUCT_ID) AS DUPLICATE INTO [##TEMP'+ @SID +'] FROM [##TB_PRODUCT'+ @SID +']')  
  Print 'TEST Prod6A'  
   
    PRINT 'Setp15'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
     
     
   
     
  exec('SET IDENTITY_INSERT DBO.TB_PRODUCT ON set ANSI_WARNINGS ON  
  INSERT INTO TB_PRODUCT(PRODUCT_ID,SINGLE_OR_KIT,STATUS,CATEGORY_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE,FLAG_RECYCLE)  
  SELECT PRODUCT_ID,1,''Active'',CATEGORY_ID,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE(),''A'' as FLAG_RECYCLE FROM [##TEMP'+ @SID +'] WHERE DUPLICATE=1 and product_id is not null')  
  Print 'TEST Prod6B'  
  exec('drop table [##TB_PRODUCT'+ @SID +']  
        drop table [##TEMP'+ @SID +']')  
  Print 'TEST Prod6C'  
   PRINT 'Setp16'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
 -----------------------------------  
 EXEC('DELETE FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE ATTRIBUTE_TYPE IN (7,9)')  
 IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##FINDDUPLICATE'+ @SID +'')  
   BEGIN  
    EXEC('DROP TABLE [##FINDDUPLICATE'+ @SID +']')  
   END   
 EXEC('SELECT *,ROW_NUMBER() OVER(PARTITION BY CATALOG_ID,PRODUCT_ID,FAMILY_ID,PRODUCT_ID,ATTRIBUTE_ID ORDER BY CATALOG_ID,PRODUCT_ID,FAMILY_ID,PRODUCT_ID,ATTRIBUTE_ID) AS R   
 INTO [##FINDDUPLICATE'+ @SID +']  FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE ATTRIBUTE_TYPE=6')  
      
    IF OBJECT_ID('TEMPDB..##PK1') IS NOT NULL   DROP TABLE  ##PK1    
      
    IF OBJECT_ID('TEMPDB..##PKFINAL') IS NOT NULL   DROP TABLE  ##PKFINAL    
     PRINT 'Setp16A'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
  EXEC('SELECT DISTINCT [##FINDDUPLICATE'+ @SID +'].ATTRIBUTE_ID,[##FINDDUPLICATE'+ @SID +'].PRODUCT_ID,CASE  WHEN [##FINDDUPLICATE'+ @SID +'].SUBFAMILY_ID IS NULL  THEN [##FINDDUPLICATE'+ @SID +'].[family_id] 
  ELSE [##FINDDUPLICATE'+ @SID +'].SUBFAMILY_ID END as FAMILY_ID,[##FINDDUPLICATE'+ @SID +'].CATALOG_ID,  
convert(VARCHAR(MAX),[##FINDDUPLICATE'+ @SID +'].Value) as ATTRVALUE,tb_catalog_family.category_id into ##PK1 FROM [##FINDDUPLICATE'+ @SID +'],  
[##IMPORTTEMP'+ @SID +'],tb_catalog_family,  
TB_ATTRIBUTE WHERE [##FINDDUPLICATE'+ @SID +'].ColumnName =TB_ATTRIBUTE.ATTRIBUTE_NAME 
 AND [##FINDDUPLICATE'+ @SID +'].ATTRIBUTE_ID =TB_ATTRIBUTE.ATTRIBUTE_ID
AND [##FINDDUPLICATE'+ @SID +'].ATTRIBUTE_TYPE=6 AND TB_ATTRIBUTE.ATTRIBUTE_TYPE = 6 and [##FINDDUPLICATE'+ @SID +'].r=1 and tb_catalog_family.family_id = case 
when [##FINDDUPLICATE'+ @SID +'].subfamily_id is null then [##FINDDUPLICATE'+ @SID +'].family_id else [##FINDDUPLICATE'+ @SID +'].subfamily_id end and tb_catalog_family.CATALOG_ID = [##IMPORTTEMP'+ @SID +'].catalog_id
and tb_catalog_family.family_id = case when [##IMPORTTEMP'+ @SID +'].subfamily_id is null then [##IMPORTTEMP'+ @SID +'].family_id else [##IMPORTTEMP'+ @SID +'].subfamily_id end
AND case when [##FINDDUPLICATE'+ @SID +'].subfamily_id is null then [##FINDDUPLICATE'+ @SID +'].family_id else [##FINDDUPLICATE'+ @SID +'].subfamily_id end = 
case when [##IMPORTTEMP'+ @SID +'].subfamily_id is null then [##IMPORTTEMP'+ @SID +'].family_id else [##IMPORTTEMP'+ @SID +'].subfamily_id end
')  

    PRINT 'Setp16B'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
EXEC('select ATTRIBUTE_ID,PRODUCT_ID,FAMILY_ID,CATALOG_ID,CATEGORY_ID into ##PKFINAL from ##PK1  
except  
select ATTRIBUTE_ID,PRODUCT_ID,FAMILY_ID,CATALOG_ID,CATEGORY_ID from TB_PARTS_KEY')  
  PRINT 'Setp17'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
Print 'TEST Prod6d'  
  EXEC('INSERT INTO TB_PARTS_KEY (ATTRIBUTE_ID,PRODUCT_ID,FAMILY_ID,CATALOG_ID,ATTRIBUTE_VALUE,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE,category_id)  
select DISTINCT p.ATTRIBUTE_ID,p.PRODUCT_ID,p.FAMILY_ID,p.CATALOG_ID,p.ATTRVALUE,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE(),p.CATEGORY_ID from ##PK1 p join ##PKFINAL tpk on p.attribute_id=tpk.ATTRIBUTE_ID and p.CATALOG_ID=tpk.catalog_id and p.CATEGORY_ID
=tpk.CATEGORY_ID and p.FAMILY_ID=tpk.FAMILY_ID and  
p.PRODUCT_ID=tpk.Product_id  
')  
  
    IF OBJECT_ID('TEMPDB..##PK1') IS NOT NULL   DROP TABLE  ##PK1    
      
    IF OBJECT_ID('TEMPDB..##PKFINAL') IS NOT NULL   DROP TABLE  ##PKFINAL    
 Print 'TEST Prod6d123'  
 EXEC('UPDATE TB_PARTS_KEY SET TB_PARTS_KEY.ATTRIBUTE_VALUE=convert(VARCHAR(MAX),[##IMPORTTEMPTRANSPOSE'+ @SID +'].Value) FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'],[##IMPORTTEMP'+ @SID +'],TB_PARTS_KEY  
 WHERE TB_PARTS_KEY.CATALOG_ID=[##IMPORTTEMPTRANSPOSE'+ @SID +'].CATALOG_ID AND TB_PARTS_KEY.FAMILY_ID=CASE  WHEN [##IMPORTTEMPTRANSPOSE'+ @SID +'].SUBFAMILY_ID IS NULL  THEN [##IMPORTTEMPTRANSPOSE'+ @SID +'].[family_id] 
 ELSE [##IMPORTTEMPTRANSPOSE'+ @SID +'].SUBFAMILY_ID END AND TB_PARTS_KEY.PRODUCT_ID=[##IMPORTTEMPTRANSPOSE'+ @SID +'].PRODUCT_ID AND  
 TB_PARTS_KEY.ATTRIBUTE_ID=[##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTRIBUTE_ID AND [##IMPORTTEMP'+ @SID +'].StatusUI=''Update''')  
 
  EXEC('UPDATE TB_PARTS_KEY SET TB_PARTS_KEY.ATTRIBUTE_VALUE=convert(VARCHAR(MAX),[##IMPORTTEMPTRANSPOSE'+ @SID +'].Value) FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'],[##IMPORTTEMP'+ @SID +'],TB_PARTS_KEY  
 WHERE TB_PARTS_KEY.CATALOG_ID=[##IMPORTTEMPTRANSPOSE'+ @SID +'].CATALOG_ID AND TB_PARTS_KEY.FAMILY_ID=CASE  WHEN [##IMPORTTEMPTRANSPOSE'+ @SID +'].SUBFAMILY_ID IS NULL  THEN [##IMPORTTEMPTRANSPOSE'+ @SID +'].[family_id] 
 ELSE [##IMPORTTEMPTRANSPOSE'+ @SID +'].SUBFAMILY_ID END AND TB_PARTS_KEY.PRODUCT_ID=[##IMPORTTEMPTRANSPOSE'+ @SID +'].PRODUCT_ID AND  
 TB_PARTS_KEY.ATTRIBUTE_ID=[##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTRIBUTE_ID AND [##IMPORTTEMP'+ @SID +'].StatusUI=''Associate''') 
   PRINT 'Setp18'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
 Print 'TEST Prod6E'  
 -------------------------------------  
--------------------------------------------------  
--INSERTING CATALOG PRODUCT  
   EXEC('UPDATE A SET A.STATUSUI=''INSERT'' FROM [##IMPORTTEMP'+ @SID +']  A LEFT JOIN TB_CATALOG_PRODUCT B ON A.CATALOG_ID = B.CATALOG_ID AND A.PRODUCT_ID = B.PRODUCT_ID  
   WHERE B.PRODUCT_ID IS NULL')  
  
   EXEC('  
   INSERT INTO TB_CATALOG_PRODUCT([PRODUCT_ID],[CATALOG_ID],[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT DISTINCT [product_id],[##IMPORTTEMP'+ @SID +'].CATALOG_ID,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMP'+ @SID +'] WHERE   
   [##IMPORTTEMP'+ @SID +'].CATALOG_ITEM_NO IS NOT NULL AND [##IMPORTTEMP'+ @SID +'].PRODUCT_ID IS NOT NULL AND StatusUI=''Insert''  
   ')  
     ---------------------------------  ************** To check the cloned products and attribute check [START] *********************---------------------------


   exec('select row_number() over (partition by product_id,attribute_id  order by product_id,attribute_id) as SORT_ORDER, * into ##TEMPCLONEDVALUES from  [##IMPORTTEMPTRANSPOSE'+ @SID +']')

   exec('delete from ##TEMPCLONEDVALUES where attribute_id is not null and SORT_ORDER <> 1 ')

   IF OBJECT_ID('TEMPDB..[##IMPORTTEMPTRANSPOSE'+ @SID +']') IS NOT NULL  exec (' DROP TABLE  [##IMPORTTEMPTRANSPOSE'+ @SID +'] ') 


   exec('select * into [##IMPORTTEMPTRANSPOSE'+ @SID +'] from ##TEMPCLONEDVALUES ')

 exec('drop table ##TEMPCLONEDVALUES')

  ---------------------------------  ************** To check the cloned products and attribute check [END] *********************--------------------------- 
 Print 'TEST Prod6f'   
     
   SET IDENTITY_INSERT dbo.TB_PRODUCT OFF     
   print 'error'  
    PRINT 'Setp19'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))   
   exec('IF OBJECT_ID(''TEMPDB..##CHECKPRODSPEC'') IS NOT NULL   DROP TABLE  ##CHECKPRODSPEC')        
   EXEC('  
   SELECT distinct  [product_id],attribute_datatype,ATTRIBUTE_TYPE INTO ##CHECKPRODSPEC  
   FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE attribute_datatype LIKE ''Text%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Insert'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   GROUP BY [product_id],[attribute_id],attribute_datatype,ATTRIBUTE_TYPE  
     
   ')  
     
     
     
   exec('INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT distinct MAX(case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),VALUE))),Replace(substring(ATTRIBUTE_DATATYPE,charindex(''('',ATTRIBUTE_DATATYPE)+1,len(ATTRIBUTE_DATATYPE)),'')'',''''))
else    convert(nvarchar(max),VALUE) End),[product_id],[attribute_id],null,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE()   
   FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE attribute_datatype LIKE ''Text%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Insert'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   GROUP BY [product_id],[attribute_id]')  

      EXEC('INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT distinct NULL,[product_id],[attribute_id],null,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE()   
   FROM  ##CHECKPRODSPEC , ##MISSINGATTRIBUTESLIST WHERE attribute_datatype LIKE ''Text%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13)')  
     
     
    PRINT 'Setp20'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))   
   print '11213'  
   print('update [##IMPORTTEMPTRANSPOSE'+ @SID +'] set value = 0 where ATTRIBUTE_DATATYPE LIKE ''Number%'' and (value is null or value = '''')')  
   exec('update [##IMPORTTEMPTRANSPOSE'+ @SID +'] set value = 0 where ATTRIBUTE_DATATYPE LIKE ''Number%'' and (value is null or value = '''')')  
  
      
   DECLARE @COUNT_NUMERIC_VALUES INT = 0;  
   SET @QRY_OUT = 'SET @retvalOUT = (SELECT COUNT(*) FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] A WHERE ISNUMERIC(CONVERT(VARCHAR(MAX),A.VALUE))=0 AND A.ATTRIBUTE_DATATYPE LIKE ''Number%'')'  
      
   SET @ParmDefinition = '@retvalOUT INT OUTPUT';  
   EXEC sp_executesql @QRY_OUT, @ParmDefinition, @retvalOUT=@COUNT_NUMERIC_VALUES OUTPUT;  
   print @COUNT_NUMERIC_VALUES  
   --IF(@COUNT_NUMERIC_VALUES >0)  
   -- BEGIN  
   --  SET @ERRORFLG=1  
   --  --RAISERROR('Invalid price values for price attributes',16,1)  
   -- -- RETURN -1  
   -- END  
    print '1121'  
     PRINT 'Setp21'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
   EXEC('  
   INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT distinct null,[product_id],[attribute_id],case when ISNUMERIC(convert(nvarchar(max),[value]))=1 then cast([value] as float)  end as value,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] 
   WHERE attribute_datatype LIKE ''Number%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Insert'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   ')  
     
   print '1111'  
     
     
   --DATE AND TIME FORMAT--  
   EXEC('INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT DISTINCT convert(nvarchar(10),[value],101),[product_id],[attribute_id],null,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE attribute_datatype LIKE ''Date and Time%'' AND ATTRIBUTE_TYPE NOT IN(6
,11,12,13) AND PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Insert'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   ')  
Print 'TEST Prod8'     
    PRINT 'Setp22'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
--SELECT * FROM [##IMPORTTEMPTRANSPOSE'+ @SID +']  
   --PRINT N'UPDATING PRODUCT SPECIFICATIONS FOR EXISTING PRODUCT';  
--UPDATING PRODUCT SPECIFICATIONS FOR EXISTING PRODUCT  
 
   EXEC('UPDATE TB_PROD_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
else    convert(nvarchar(max),SIT.VALUE) End FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT WHERE TB_PROD_SPECS.PRODUCT_ID=SIT.Product_id AND  
   TB_PROD_SPECS.ATTRIBUTE_ID=SIT.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%'' AND SIT.ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND SIT.PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Update'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
  
   UPDATE TB_PROD_SPECS SET NUMERIC_VALUE=case when ISNUMERIC(convert(nvarchar(max),SIT.VALUE))=1 then cast(SIT.VALUE as float)  end FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT WHERE TB_PROD_SPECS.PRODUCT_ID=SIT.Product_id AND  
   TB_PROD_SPECS.ATTRIBUTE_ID=SIT.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''number%'' AND SIT.ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND SIT.PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Update'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   ')  
     
 
   EXEC('   
   SELECT  convert(nvarchar(max),[value]),[product_id],[attribute_id],null,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT  
   WHERE attribute_datatype LIKE ''Hyperlink%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' ) AND ATTRIBUTE_ID NOT IN  
   (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID=SIT.Product_id) AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
     
   ')  
     PRINT 'Setp23'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
   print'Hyper error'  
     
   --Hyperlink Insertion --  
     
   EXEC('INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT  convert(nvarchar(max),[value]),[product_id],[attribute_id],null,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT  
   WHERE attribute_datatype LIKE ''Hyperlink%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' ) AND ATTRIBUTE_ID NOT IN  
   (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID=SIT.Product_id) AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
     
   ')  
     
     
   --Date And Time--  
   EXEC('UPDATE TB_PROD_SPECS SET STRING_VALUE=convert(nvarchar(max),SIT.VALUE,101) FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT WHERE TB_PROD_SPECS.PRODUCT_ID=SIT.Product_id AND  
   TB_PROD_SPECS.ATTRIBUTE_ID=SIT.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Date and Time%'' AND SIT.ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND SIT.PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Update'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
     
   INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT convert(nvarchar(max),[value]),[product_id],[attribute_id],null,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT  
   WHERE attribute_datatype LIKE ''Text%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Update'') AND ATTRIBUTE_ID NOT IN  
   (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID=SIT.Product_id) AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
     
   INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT null,[product_id],[attribute_id],convert(nvarchar(max),[value]),null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT   
   WHERE attribute_datatype LIKE ''Number%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Update'') AND ATTRIBUTE_ID NOT IN  
   (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID=SIT.Product_id) AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   ')  
   --Date And Time-- 
    PRINT 'Setp24'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
   EXEC('INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT convert(nvarchar(max),[value],101),[product_id],[attribute_id],null,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT  
   WHERE attribute_datatype LIKE ''Date and Time%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Update'') AND ATTRIBUTE_ID NOT IN  
   (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID=SIT.Product_id) AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   ')  
------------------PROD_SPEC ENTRY FOR ASSOCIATED PRODUCT-------  
   EXEC('UPDATE TB_PROD_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
else    convert(nvarchar(max),SIT.VALUE) End FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT WHERE TB_PROD_SPECS.PRODUCT_ID=SIT.Product_id AND  
   TB_PROD_SPECS.ATTRIBUTE_ID=SIT.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%'' AND SIT.ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND SIT.PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Associate'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
  
   UPDATE TB_PROD_SPECS SET NUMERIC_VALUE=convert(nvarchar(max),SIT.VALUE) FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT WHERE TB_PROD_SPECS.PRODUCT_ID=SIT.Product_id AND  
   TB_PROD_SPECS.ATTRIBUTE_ID=SIT.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''number%'' AND SIT.ATTRIBUTE_TYPE NOT IN(6,11,12,13)AND SIT.PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Associate'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   ')  
   --Date And Time--  
   EXEC('UPDATE TB_PROD_SPECS SET STRING_VALUE=convert(nvarchar(max),SIT.VALUE,101) FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT WHERE TB_PROD_SPECS.PRODUCT_ID=SIT.Product_id AND  
   TB_PROD_SPECS.ATTRIBUTE_ID=SIT.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Date and Time%'' AND SIT.ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND SIT.PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Associate'')AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
     
   INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT convert(nvarchar(max),[value]),[product_id],[attribute_id],null,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT  
   WHERE attribute_datatype LIKE ''Text%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13)AND PRODUCT_ID IN  
   (SELECT DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Associate'') AND ATTRIBUTE_ID NOT IN  
   (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID=SIT.Product_id) AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
     
   INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT null,[product_id],[attribute_id],convert(nvarchar(max),[value]),null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT   
   WHERE attribute_datatype LIKE ''Number%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT  DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Associate'') AND ATTRIBUTE_ID NOT IN  
   (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID=SIT.Product_id) AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   ')  
   --Date And Time--  
   EXEC('INSERT INTO TB_PROD_SPECS([string_value],[product_id],[attribute_id],[Numeric_value],[object_type],[object_name],[created_User],[created_date],[MODIFIED_USER],[MODIFIED_DATE])   
   SELECT convert(nvarchar(max),[value],101),[product_id],[attribute_id],null,null,null,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] SIT  
   WHERE attribute_datatype LIKE ''Date and Time%'' AND ATTRIBUTE_TYPE NOT IN(6,11,12,13) AND PRODUCT_ID IN  
   (SELECT DISTINCT PRODUCT_ID FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] WHERE COLUMNNAME=''StatusUI'' AND VALUE=''Associate'') AND ATTRIBUTE_ID NOT IN  
   (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID=SIT.Product_id) AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%''  
   ')  
---------------------------------------------------------------  
   PRINT N'INSERTING INTO PROD_FAMILY';  
--INSERTING INTO PROD_FAMILY  
   PRINT 'Setp25'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
    exec('IF OBJECT_ID(''TEMPDB..##PF1'') IS NOT NULL   DROP TABLE  ##PF1')    
      
    exec('IF OBJECT_ID(''TEMPDB..##PFFINAL'') IS NOT NULL   DROP TABLE  ##PFFINAL')   
  
    EXEC('SELECT distinct [sort_order],CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id] ELSE SUBFAMILY_ID END as FAMILY_ID,PRODUCT_ID into ##PF1 FROM [##IMPORTTEMP'+ @SID +']   
   WHERE CATALOG_ITEM_NO IS NOT NULL AND PRODUCT_ID IS NOT NULL AND StatusUI=''Insert'' AND SORT_ORDER IS NOT NULL  
   ')  
  
   EXEC('select PRODUCT_ID,FAMILY_ID into ##PFFINAL from ##PF1  
    except  
      select PRODUCT_ID,FAMILY_ID from TB_PROD_FAMILY')  
               
      PRINT 'Setp26'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
           print 'dddd5'  
   EXEC('set ANSI_WARNINGS ON  
   INSERT INTO TB_PROD_FAMILY([SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[PUBLISH] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] , FLAG_RECYCLE,PUBLISH2WEB,PUBLISH2PDF,PUBLISH2EXPORT,PUBLISH2PORTAL)  
   SELECT sort_order,F.FAMILY_ID,F.PRODUCT_ID,1,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE(), ''A'' as FLAG_RECYCLE,1,1,1,1 FROM ##PF1 P JOIN ##PFFINAL F ON P.PRODUCT_ID=F.PRODUCT_ID and P.FAMILY_ID=F.FAMILY_ID')  

   -- Attribute Pack
   IF NOT EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TEMP_ATTRIBUTE_PACK_PRODUCT'+ @SID +'')
BEGIN
	EXECUTE ('CREATE TABLE [##TEMP_ATTRIBUTE_PACK_PRODUCT'+ @SID +'] (
	FAMILY_ID int,
	PRODUCT_ID int)') 
	END
	--EXEC('INSERT INTO [##TEMP_ATTRIBUTE_PACK_PRODUCT'+ @SID +'] (FAMILY_ID ,PRODUCT_ID) SELECT F.FAMILY_ID,F.PRODUCT_ID FROM ##PF1 P JOIN ##PFFINAL F ON P.PRODUCT_ID=F.PRODUCT_ID and P.FAMILY_ID=F.FAMILY_ID')  
 

   -- Attribute Pack
  
   exec('IF OBJECT_ID(''TEMPDB..##PF1'') IS NOT NULL   DROP TABLE  ##PF1')    
      
   exec('IF OBJECT_ID(''TEMPDB..##PFFINAL'') IS NOT NULL   DROP TABLE  ##PFFINAL')   
  
   EXEC('SELECT DISTINCT [sort_order],CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id] ELSE SUBFAMILY_ID END as FAMILY_ID,PRODUCT_ID into ##PF1 FROM [##IMPORTTEMP'+ @SID +']   
   WHERE CATALOG_ITEM_NO IS NOT NULL AND PRODUCT_ID IS NOT NULL AND StatusUI=''Associate'' AND SORT_ORDER IS NOT NULL  
   ')  
  
   EXEC('select PRODUCT_ID,FAMILY_ID into ##PFFINAL from ##PF1  
    except  
      select PRODUCT_ID,FAMILY_ID from TB_PROD_FAMILY')  
    print '331'  
   EXEC('INSERT INTO TB_PROD_FAMILY([SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[PUBLISH] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] , FLAG_RECYCLE,PUBLISH2WEB,PUBLISH2PDF,PUBLISH2EXPORT,PUBLISH2PORTAL )  
   SELECT sort_order,F.FAMILY_ID,F.PRODUCT_ID,1,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE(), ''A'' as FLAG_RECYCLE,1,1,1,1  FROM ##PF1 P JOIN ##PFFINAL F ON P.PRODUCT_ID=F.PRODUCT_ID and P.FAMILY_ID=F.FAMILY_ID')  
  
   exec('IF OBJECT_ID(''TEMPDB..##PF1'') IS NOT NULL   DROP TABLE  ##PF1')        
   exec('IF OBJECT_ID(''TEMPDB..##PFFINAL'') IS NOT NULL   DROP TABLE  ##PFFINAL')   
   PRINT 'Setp27'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
   PRINT N'INSERTED INTO PROD_FAMILY';  
---NEWLY ADDED FOR EXISTING PRODUCT--START   
   EXEC('UPDATE PF SET PF.FAMILY_ID=SI.FAMILY_ID,PF.PRODUCT_ID=SI.PRODUCT_ID,PF.PUBLISH=1,PF.SORT_ORDER=SI.SORT_ORDER   
 FROM TB_PROD_FAMILY PF JOIN  [##IMPORTTEMP'+ @SID +'] SI on PF.FAMILY_ID=si.family_id  
and PF.PRODUCT_ID=si.product_id WHERE PF.FAMILY_ID= (CASE  WHEN SI.SUBFAMILY_ID IS NULL  THEN SI.FAMILY_ID ELSE SI.SUBFAMILY_ID END) AND  
    SI.StatusUI=''Update'' AND SI.SORT_ORDER IS NOT NULL' )  
     
   --INSERT INTO TB_PROD_FAMILY([SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[PUBLISH] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] )  
   --SELECT[sort_order],CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id] ELSE SUBFAMILY_ID END,[PRODUCT_ID],1,SYSTEM_USER,GETDATE(),SYSTEM_USER,GETDATE() FROM [##IMPORTTEMP'+ @SID +'] SI  
   --WHERE CATALOG_ITEM_NO IS NOT NULL AND PRODUCT_ID IS NOT NULL AND StatusUI='UPDATE' AND PRODUCT_ID NOT IN(SELECT PRODUCT_ID FROM TB_PROD_FAMILY WHERE FAMILY_ID=SI.FAMILY_ID) AND SI.SORT_ORDER IS NOT NULL  
     
   --UPDATE TB_PROD_FAMILY SET  
-----NEWLY ADDED FOR EXISTING PRODUCT--END        
   --PRINT N'CREATING SORT_ORDER FOR ATTRIBUTE';  
--CREATING SORT_ORDER FOR ATTRIBUTE   
Print 'TEST Prod9'   
 PRINT 'Setp28'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
  IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TempTest'+ @SID +'')  
    BEGIN  
     EXEC('DROP TABLE [##TempTest'+ @SID +']')  
    END   
    EXEC('SELECT DISTINCT attribute_id,columnName,IDENTITY(int,1,1) as sort_order into [##TempTest'+ @SID +']   
   from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where attribute_id is not null and Product_id in(select distinct Product_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''INSERT'') AND COLUMNNAME NOT LIKE ''%STATUS%'' 
   AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' ORDER BY ATTRIBUTE_ID  
   ')  
   Print 'TEST Prod9A'   
     IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TempTest'+ @SID +'')  
    BEGIN  
    DECLARE @STATUS_ATT NVARCHAR(25)  
    DECLARE @CNT_ST INT  
    --DECLARE @QRY_OUT NVARCHAR(MAX)  
    --DECLARE @ParmDefinition NVARCHAR(MAX)  
    --select @CHKDAT  = COUNT(*) FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] A where a.Value is null and a.ATTRIBUTE_DATATYPE='Date and Time'  
    SET @QRY_OUT = 'SET @retvalOUT = (SELECT COUNT(attribute_id) FROM [##TempTest'+ @SID +'] )'  
    SET @ParmDefinition = '@retvalOUT INT OUTPUT';  
    EXEC sp_executesql @QRY_OUT, @ParmDefinition, @retvalOUT=@CNT_ST OUTPUT;  
    Print 'TEST Prod9B'   
    PRINT @CNT_ST  
    IF(@CNT_ST=0)  
    BEGIN  
    PRINT @CNT_ST  
    print 'error found'  
    EXEC('DROP TABLE [##TempTest'+ @SID +']')  
    Print 'TEST Prod9C'   
    EXEC('SELECT DISTINCT attribute_id,columnName,IDENTITY(int,1,1) as sort_order into [##TempTest'+ @SID +']   
   from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where attribute_id is not null and Product_id in(select distinct Product_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''Associate'') AND COLUMNNAME NOT LIKE ''%STATUS%'' 
   AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' ORDER BY ATTRIBUTE_ID  
   ')  
   Print 'TEST Prod9D'   
    END  
    END   
   --EXEC('SELECT DISTINCT attribute_id,columnName,IDENTITY(int,1,1) as sort_order into [##TempTest'+ @SID +']   
   --from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where attribute_id is not null and Product_id in(select distinct Product_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''Associate'') AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' ORDER BY ATTRIBUTE_ID  
   --')  
  --PRINT N'UPDATING ATTRIBUTE SORT_ORDER ';  
--UPDATING ATTRIBUTE SORT_ORDER    
print 'next' 
 PRINT 'Setp29'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))  
   EXEC('UPDATE [##IMPORTTEMPTRANSPOSE'+ @SID +'] SET [##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTR_SORT_ORDER=[##TempTest'+ @SID +'].SORT_ORDER FROM [##TempTest'+ @SID +'] WHERE [##TempTest'+ @SID +'].ATTRIBUTE_ID =[##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTRIBUTE_ID
 AND [##IMPORTTEMPTRANSPOSE'+ @SID +'].ATTRIBUTE_ID IS NOT NULL')  
   --PRINT N'INSERT INTO PROD_FAMILY_ATTR_LIST';  
--INSERT INTO PROD_FAMILY_ATTR_LIST  
  
--exec('IF OBJECT_ID(''TEMPDB..##PAF1'') IS NOT NULL   DROP TABLE  ##PAF1')        
--   exec('IF OBJECT_ID(''TEMPDB..##PAFFINAL'') IS NOT NULL   DROP TABLE  ##PAFFINAL')   
     
--   EXEC('select DISTINCT [attribute_id],[Attr_sort_order],CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id]  ELSE SUBFAMILY_ID END as FAMILY_ID,Product_id into ##PAF1 from [##IMPORTTEMPTRANSPOSE'+ @SID +']  where attribute_id is not null  
--   and Product_id in(select distinct Product_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''INSERT'') AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' AND ATTRIBUTE_TYPE NOT IN(11,12,13) 
 
--   ')  
     
--   print 'first'  
--   EXEC('select PRODUCT_ID,FAMILY_ID,attribute_id,Attr_sort_order into ##PAFFINAL from ##PAF1  
--    except  
--      select PRODUCT_ID,FAMILY_ID,attribute_id,sort_order from TB_PROD_FAMILY_ATTR_LIST')  
        
--      EXEC('INSERT into TB_PROD_FAMILY_ATTR_LIST ([ATTRIBUTE_ID] ,[SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] )  
--   select DISTINCT F.[attribute_id],F.[Attr_sort_order],F.[family_id],F.[Product_id],SYSTEM_USER,GETDATE(),SYSTEM_USER,GETDATE() from ##PAF1 p join ##PAFFINAL F  ON P.PRODUCT_ID=F.PRODUCT_ID and P.FAMILY_ID=F.FAMILY_ID and p.attribute_id = f.attribute_id     
--   ')  
 
   Print 'TEST Prod9E'   
 PRINT 'Setp30'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
    EXEC('IF OBJECT_ID(''TEMPDB..#prodfamilyattrListTemp'') IS NOT NULL   DROP TABLE  #prodfamilyattrListTemp  
     select DISTINCT [attribute_id],CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id] ELSE SUBFAMILY_ID END Family_id,0 as PRODUCT_ID into #prodfamilyattrListTemp from [##IMPORTTEMPTRANSPOSE'+ @SID +']  
   where attribute_id is not null  and family_id in(select distinct family_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''INSERT'') AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' 
   AND ATTRIBUTE_TYPE NOT IN(11,12,13)  
   except   
   select tpfal.attribute_id,tpfal.Family_id,tpfal.PRODUCT_ID from tb_prod_family_attr_list tpfal   
  
   INSERT into TB_PROD_FAMILY_ATTR_LIST ([ATTRIBUTE_ID] ,[SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] )  
select attribute_id,Attr_sort_order,Family_id,PRODUCT_ID,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() from (  
   select DISTINCT tt.[attribute_id],tt.[Attr_sort_order],CASE  WHEN tt.SUBFAMILY_ID IS NULL  THEN tt.[family_id] ELSE tt.SUBFAMILY_ID END Family_id,0 as PRODUCT_ID  from [##IMPORTTEMPTRANSPOSE'+ @SID +'] tt join #prodfamilyattrListTemp 
   on #prodfamilyattrListTemp.family_id=tt.family_id  and  #prodfamilyattrListTemp.attribute_id=tt.attribute_id  
   where tt.attribute_id is not null and tt.family_id in(select distinct family_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''INSERT'') AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' AND
 ATTRIBUTE_TYPE NOT IN(11,12,13)  
     
  
  
   ) as temp')  
  
  
  
   PRINT 'Setp31'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
     
  
--   EXEC('INSERT into TB_PROD_FAMILY_ATTR_LIST ([ATTRIBUTE_ID] ,[SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] )  
--select attribute_id,Attr_sort_order,Family_id,PRODUCT_ID,SYSTEM_USER,GETDATE(),SYSTEM_USER,GETDATE() from (  
--   select DISTINCT [attribute_id],[Attr_sort_order],CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id] ELSE SUBFAMILY_ID END Family_id,0 as PRODUCT_ID  from [##IMPORTTEMPTRANSPOSE'+ @SID +']  
--   where attribute_id is not null and family_id in(select distinct family_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''INSERT'') AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' AND ATTRIBUTE_TYPE NOT IN(11,12,13)  
--   except   
--   select tpfal.attribute_id,sort_order,tpfal.Family_id,tpfal.PRODUCT_ID from tb_prod_family_attr_list tpfal join  [##IMPORTTEMPTRANSPOSE'+ @SID +']   on tpfal.family_id=[##IMPORTTEMPTRANSPOSE'+ @SID +'].FAMILY_ID  
--   ) as temp')  
  
   --EXEC('INSERT into TB_PROD_FAMILY_ATTR_LIST ([ATTRIBUTE_ID] ,[SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] )  
   --select DISTINCT [attribute_id],[Attr_sort_order],CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id] ELSE SUBFAMILY_ID END,0,SYSTEM_USER,GETDATE(),SYSTEM_USER,GETDATE() from [##IMPORTTEMPTRANSPOSE'+ @SID +']  where attribute_id is not null  
   --and family_id in(select distinct family_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''INSERT'')  
   -- AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' AND ATTRIBUTE_TYPE NOT IN(11,12,13)  
   --')  

   exec('IF OBJECT_ID(''TEMPDB..##DEFAULT_ATTR'') IS NOT NULL   DROP TABLE  ##DEFAULT_ATTR')   
   EXEC('select DISTINCT CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id] ELSE SUBFAMILY_ID END as FAMILY_ID INTO ##DEFAULT_ATTR from [##IMPORTTEMPTRANSPOSE'+ @SID +']  where attribute_id is not null  
   and Product_id in(select distinct Product_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''INSERT'')  
    AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' AND ATTRIBUTE_TYPE NOT IN(11,12,13)  
   ')  
     
   DECLARE @DEFAULT_ATTR_FAMILY_ID int = 0  
   DECLARE @DEFAULT_ATTR_MAXSORT int = 0  
   DECLARE @DEFAULTATTR_CURSOR TABLE  
   (  
   FAMILY_ID INT  
   )  
   INSERT INTO @DEFAULTATTR_CURSOR    
   SELECT FAMILY_ID FROM ##DEFAULT_ATTR   
     
   PRINT 'Setp32'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))    
   WHILE ((SELECT COUNT(*) FROM @DEFAULTATTR_CURSOR)>0)  
   BEGIN   
   PRINT 'DF FAM ID'  
   PRINT  @DEFAULT_ATTR_FAMILY_ID      
     SELECT TOP 1 @DEFAULT_ATTR_FAMILY_ID=FAMILY_ID FROM @DEFAULTATTR_CURSOR  
       
     select @DEFAULT_ATTR_MAXSORT = case when MAX(SORT_ORDER) is null then 0 else case when MAX(SORT_ORDER)=0 then 0 else  MAX(SORT_ORDER) end end FROm TB_PROD_FAMILY_ATTR_LIST where FAMILY_ID = @DEFAULT_ATTR_FAMILY_ID  
  
  
     EXEC('INSERT into TB_PROD_FAMILY_ATTR_LIST ([ATTRIBUTE_ID] ,[SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] )  
     SELECT ATTRIBUTE_ID, row_number() over( order by ATTRIBUTE_ID) + '+@DEFAULT_ATTR_MAXSORT+'  as SORT_ORDER, FAMILY_ID ,0,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE() FROM (  
     select DISTINCT  TA.[attribute_id],   '+ @DEFAULT_ATTR_FAMILY_ID +'  as FAMILY_ID  
      from [##IMPORTTEMPTRANSPOSE'+ @SID +']  DFA  
     RIGHT JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = DFA.ATTRIBUTE_ID and DFA.ColumnName=''StatusUI'' and DFA.Value=''INSERT''  
     JOIN CUSTOMERATTRIBUTE CA ON CA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID where CA.CUSTOMER_ID = '+ @CUSTOMERID +'  
     except   
     select DISTINCT  [attribute_id],  '+ @DEFAULT_ATTR_FAMILY_ID +'  as FAMILY_ID  
      from TB_PROD_FAMILY_ATTR_LIST WHERE FAMILY_ID = '+ @DEFAULT_ATTR_FAMILY_ID +'  
      ) ff  
      ')  
     DELETE TOP (1) FROM @DEFAULTATTR_CURSOR  
   END     
        
   Print 'TEST Prod9F'   
   exec('IF OBJECT_ID(''TEMPDB..##PAF2'') IS NOT NULL   DROP TABLE  ##PAF2')        
   exec('IF OBJECT_ID(''TEMPDB..##PAFFINAL2'') IS NOT NULL   DROP TABLE  ##PAFFINAL2')   
   exec('IF OBJECT_ID(''TEMPDB..##te4'') IS NOT NULL   DROP TABLE  ##te4')   
    exec('IF OBJECT_ID(''TEMPDB..##tt'') IS NOT NULL   DROP TABLE  ##tt')   
   Print 'TEST Prod9G'   
    EXEC('select DISTINCT [attribute_id],[Attr_sort_order],CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id] ELSE SUBFAMILY_ID END as FAMILY_ID,0 as Product_id into ##PAF2 from [##IMPORTTEMPTRANSPOSE'+ @SID +']  where attribute_id is not null  
   and Product_id in(select distinct Product_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where ColumnName=''StatusUI'' and Value=''Associate'') AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' AND ATTRIBUTE_TYPE NOT IN(11,12,13)
  
   ')  
   EXEC('select 0 as PRODUCT_ID,FAMILY_ID,attribute_id,Attr_sort_order into ##PAFFINAL2 from ##PAF2  
    except  
      select 0 as PRODUCT_ID,FAMILY_ID,attribute_id,sort_order from TB_PROD_FAMILY_ATTR_LIST')  
   Print 'TEST Prod9E'    
    exec('select DISTINCT [attribute_id],[Attr_sort_order],[family_id],0 as [Product_id] into ##tt from ##PAF2 ')  
   print'prodfamattr associate'   
    exec('IF OBJECT_ID(''TEMPDB..##te4'') IS NOT NULL   DROP TABLE  ##te4')   
    exec('select DISTINCT [attribute_id],[family_id],0 as [Product_id] into ##te4 from ##tt   
    except    
    select [attribute_id],[family_id],0 as [Product_id] from TB_PROD_FAMILY_ATTR_LIST ')   
    print 'new error'  
     PRINT 'Setp33'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff')) 
    /************** FOR CB SORT ORDER **************/  
   
--     EXEC('INSERT into TB_PROD_FAMILY_ATTR_LIST ([ATTRIBUTE_ID] ,[SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] )  
--select DISTINCT  ATTRIBUTE_ID,SORT_ORDER,FAMILY_ID, product_id, SYSTEM_USER,GETDATE(),SYSTEM_USER,GETDATE() from (  
--    select tpf.ATTRIBUTE_ID,tpf.SORT_ORDER,##te4.FAMILY_ID, ##te4.product_id,ROW_NUMBER() over(partition by tpf.ATTRIBUTE_ID,##te4.FAMILY_ID, ##te4.product_id order by tpf.ATTRIBUTE_ID,tpf.SORT_ORDER,##te4.FAMILY_ID, ##te4.product_id) as cnt  
--     from TB_PROD_FAMILY_ATTR_LIST tpf JOIN TB_CATALOG_FAMILY tcf on tcf.FAMILY_ID = tpf.FAMILY_ID join ##te4 on ##te4.product_id=tpf.product_id   where  tcf.CATALOG_ID= 2  
--    )temp where  cnt=1   
  
--')   
print 'Sort 3'


IF OBJECT_ID('TEMPDB..##ascoiateupdate') IS NOT NULL   DROP TABLE  ##ascoiateupdate   
			exec('select tt.*,row_number() over (partition by [family_id]  order by [attribute_id]  ) as SORT_ORDER,SYSTEM_USER as CREATED_USER ,GETDATE() as CREATED_DATE,
			SYSTEM_USER as MODIFIED_USER,GETDATE()as MODIFIED_DATE into ##ascoiateupdate  from (
			select DISTINCT [ATTRIBUTE_ID],[Attr_sort_order],CASE  WHEN SUBFAMILY_ID IS NULL  THEN [family_id]  ELSE SUBFAMILY_ID  END as FAMILY_ID,0 [PRODUCT_ID] 	
			 from [##IMPORTTEMPTRANSPOSE'+ @SID +'] where attribute_id is not null
			and Product_id in(select distinct Product_id from [##IMPORTTEMPTRANSPOSE'+ @SID +']
		
		
			 where ColumnName=''StatusUI'' and (Value=''Update'' or Value=''Associate'')) AND COLUMNNAME NOT LIKE ''%STATUS%'' AND COLUMNNAME NOT LIKE ''%SORT_ORDER%'' AND
			  ATTRIBUTE_TYPE NOT IN(11,12,13)) as tt
			')
			--=============================**************Sort _ORDER***********************==========================[To check the order based sort]
  IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TEMPSortOrder_FINAL')  
   BEGIN  
    EXEC('DROP TABLE ##TEMPSortOrder_FINAL')  
   END  


  exec('select dense_rank() over (partition by ##ascoiateupdate.FAMILY_ID order by ##TEMPORDER.sort_order) ORDERS,##ascoiateupdate.[ATTRIBUTE_ID] ,
  ##ascoiateupdate.[FAMILY_ID] ,##ascoiateupdate.[PRODUCT_ID] ,##ascoiateupdate.[CREATED_USER] ,##ascoiateupdate.[CREATED_DATE] ,##ascoiateupdate.[MODIFIED_USER],##ascoiateupdate.[MODIFIED_DATE]
  into ##TEMPSortOrder_FINAL from ##ascoiateupdate 
 left join ##TEMPORDER on ##TEMPORDER.attribute_id = ##ascoiateupdate.attribute_id order by ##TEMPORDER.sort_order')

				
IF EXISTS(SELECT * FROM tempdb.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME like 'SUBFAMILY_ID' and TABLE_NAME LIKE '##IMPORTTEMPTRANSPOSE'+ @SID +'%')  
BEGIN  
print 'oops'
  	exec (' INSERT INTO TB_PROD_FAMILY_ATTR_LIST ([ATTRIBUTE_ID] ,[SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER],   [MODIFIED_DATE])
			select [ATTRIBUTE_ID],ORDERS as SORT_ORDER,[FAMILY_ID] ,0 as [PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] 
			from ##TEMPSortOrder_FINAL where attribute_id not in (select distinct ATTRIBUTE_ID 
			from TB_PROD_FAMILY_ATTR_LIST where family_id in (select distinct family_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] ) or family_id in (select distinct SUBFAMILY_ID 
			from [##IMPORTTEMPTRANSPOSE'+ @SID +'] ))')
			END
			else 
			Begin
			print 'popopo'
			exec('INSERT INTO TB_PROD_FAMILY_ATTR_LIST ([ATTRIBUTE_ID] ,[SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER],   [MODIFIED_DATE])
			select ORDERS as SORT_ORDER,[FAMILY_ID] ,0 as [PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] 
			from ##TEMPSortOrder_FINAL where attribute_id not in (select distinct ATTRIBUTE_ID 
			from TB_PROD_FAMILY_ATTR_LIST where family_id in (select distinct family_id from [##IMPORTTEMPTRANSPOSE'+ @SID +'] ))')
			End			
  --=============================**************Sort _ORDER***********************==========================
SELECT 'part key'  
--exec('select tpk.ATTRIBUTE_ID,tpk.PRODUCT_ID,ff.family_id ,ff.catalog_id,case when SUBCATID_L1 is not null then ff.SUBCATID_L1 else ff.category_id end as Category_id  from TB_PARTS_KEY tpk  
--join [##importtemp'+ @SID +'] ff on tpk.PRODUCT_ID = ff.product_id and tpk.CATALOG_ID = 2  
-- ')  
 PRINT 'Setp34'
   PRINT (format(getdate(), 'yyyy-MM-dd HH:mm:ss.fff'))   
Print 'TEST PartsKey'   
  
  
exec('IF not EXISTS(SELECT * FROM tempdb.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME like ''SUBCATID_L1'' and TABLE_NAME LIKE ''##importtemp'+ @SID +'%'')  
BEGIN  
    alter table  [##importtemp'+ @SID +'] add SUBCATID_L1 nvarchar(250)  
  
   
END')  
exec('  
select DISTINCT tpk.ATTRIBUTE_ID,tpk.PRODUCT_ID,ff.family_id,ff.catalog_id,case when SUBCATID_L1 is not null then ff.SUBCATID_L1 else ff.category_id end as Category_id into #tempPartsKey from TB_PARTS_KEY tpk  
join [##importtemp'+ @SID +'] ff on tpk.PRODUCT_ID = ff.product_id and tpk.CATALOG_ID = 2   
except  
select ATTRIBUTE_ID,PRODUCT_ID,family_id,catalog_id,category_id from TB_PARTS_KEY tpk  
--------------------------  
INSERT INTO TB_PARTS_KEY (ATTRIBUTE_ID,PRODUCT_ID,FAMILY_ID,ATTRIBUTE_VALUE,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE,CATALOG_ID,category_id)  
select ATTRIBUTE_ID,PRODUCT_ID,family_id,ATTRIBUTE_VALUE,'''+@CUSTOMERNAME+''',GETDATE(),'''+@CUSTOMERNAME+''',GETDATE(),catalog_id, Category_id from  
 (  
select DISTINCT tpk.ATTRIBUTE_ID,tpk.PRODUCT_ID,ff.family_id,tpk.ATTRIBUTE_VALUE,ff.catalog_id, ff.Category_id, ROW_NUMBER() over(partition by tpk.ATTRIBUTE_ID,tpk.PRODUCT_ID,ff.family_id,ff.catalog_id, ff.Category_id order by tpk.ATTRIBUTE_ID
,tpk.PRODUCT_ID,ff.family_id,ff.catalog_id, ff.Category_id) as cnt  from    
#tempPartsKey ff   
join TB_PARTS_KEY tpk on tpk.PRODUCT_ID = ff.product_id and tpk.ATTRIBUTE_ID=ff.ATTRIBUTE_ID and tpk.CATALOG_ID = 2 --group by tpk.ATTRIBUTE_ID,tpk.PRODUCT_ID,ff.family_id,ff.catalog_id, ff.Category_id   
) tempdata where cnt=1  
')  
 

Print 'TEST CatalogAttr'   
  
--exec('  
--insert into TB_CATALOG_ATTRIBUTES (CATALOG_ID,attribute_id)  
--select (select distinct top 1 CATALOG_ID  from [##IMPORTTEMP'+ @SID +']) as CATALOG_ID,attribute_id from TB_CATALOG_ATTRIBUTES where CATALOG_ID = 2  
--except   
--select CATALOG_ID,attribute_id from TB_CATALOG_ATTRIBUTES  
--')  
/************** FOR CB SORT ORDER **************/  
Print 'TEST Prod10'   
      
   -- EXEC('INSERT into TB_PROD_FAMILY_ATTR_LIST ([ATTRIBUTE_ID] ,[SORT_ORDER] ,[FAMILY_ID] ,[PRODUCT_ID] ,[CREATED_USER] ,[CREATED_DATE] ,[MODIFIED_USER] ,[MODIFIED_DATE] )  
   --select DISTINCT F.[attribute_id],F.[Attr_sort_order],F.[family_id],F.[Product_id],SYSTEM_USER,GETDATE(),SYSTEM_USER,GETDATE() from ##PAF2 p join ##PAFFINAL2 F ON P.PRODUCT_ID=F.PRODUCT_ID and P.FAMILY_ID=F.FAMILY_ID and p.attribute_id = f.attribute_id  
   --')  
   ---------LOG PROCESS-----------------  
   --EXEC('SELECT DISTINCT IM.CATALOG_NAME, (IM.CATEGORY_NAME+''(ID:''+ CONVERT(nvarchar,IM.CATEGORY_ID) +'')'') as CATEGORY_DETAILS,(IM.FAMILY_NAME +''(ID:''+CONVERT(nvarchar,A.FAMILY_ID)+'')'') AS FAMILY_DETAILS,(IM.SUBFAMILY_NAME +''(ID:''+CONVERT(nvarchar,A.SUBFAMILY_ID)+'')'') AS SUBFAMILY_DETAILS,  
   --(IM.CATALOG_ITEM_NO +''(ID:''+CONVERT(nvarchar,A.PRODUCT_ID)+'')'') AS PRODUCT_DETAILS,''UPDATED'' STATUS INTO [##LOGTEMPTABLE'+ @SID +'] FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] A JOIN [##IMPORTTEMP'+ @SID +'] IM ON IM.FAMILY_ID=A.FAMILY_ID')  
   --EXEC('SELECT DISTINCT IM.CATALOG_NAME, (cast(IM.CATEGORY_NAME as nvarchar)+''(ID:''+ cast(IM.CATEGORY_ID as nvarchar) +'')'') as CATEGORY_DETAILS,(cast(IM.FAMILY_NAME as nvarchar) +''(ID:''+cast(A.FAMILY_ID as nvarchar)+'')'') AS FAMILY_DETAILS,(cast(IM.SUBFAMILY_NAME as nvarchar) +''(ID:''+cast(A.SUBFAMILY_ID as nvarchar)+'')'') AS SUBFAMILY_DETAILS,  
   --(cast(IM.CATALOG_ITEM_NO as nvarchar) +''(ID:''+cast(A.PRODUCT_ID as nvarchar)+'')'') AS PRODUCT_DETAILS,''UPDATED'' STATUS INTO [##LOGTEMPTABLE'+ @SID +'] FROM [##IMPORTTEMPTRANSPOSE'+ @SID +'] A JOIN [##IMPORTTEMP'+ @SID +'] IM ON IM.FAMILY_ID=A.FAMILY_ID')  
--   print 'Update SortOrder'
--   EXEC('SELECT ROW_NUMBER() OVER (PARTITION BY FAMILY_ID ORDER BY MODIFIED_DATE)AS RO,* INTO #TEMP FROM TB_PROD_FAMILY 
--WHERE FAMILY_ID IN(SELECT distinct TF.FAMILY_ID FROM TB_FAMILY TF JOIN [##IMPORTTEMP'+ @SID +'] TT ON TT.FAMILY_ID = TF.FAMILY_ID and tt.StatusUI<>''Update'')  
--UPDATE TPF SET TPF.SORT_ORDER = T.RO FROM TB_PROD_FAMILY TPF JOIN #TEMP T ON TPF.FAMILY_ID = T.FAMILY_ID AND TPF.PRODUCT_ID = T.PRODUCT_ID AND TPF.PUBLISH = T.PUBLISH')  

 print 'Update SortOrder'

  IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##GetMaxUpdate')
	BEGIN
				EXEC('DROP TABLE [##GetMaxUpdate]')
	END	 
	 IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TestingNew')
	BEGIN
				EXEC('DROP TABLE [##TestingNew]')
	END	 

   EXEC('SELECT ROW_NUMBER() OVER (PARTITION BY FAMILY_ID ORDER BY SORT_ORDER)AS RO,*,0 as ColumnRow INTO #TEMP FROM TB_PROD_FAMILY 
WHERE FAMILY_ID IN(SELECT distinct TF.FAMILY_ID FROM TB_FAMILY TF JOIN [##IMPORTTEMP'+ @SID +'] TT ON TT.FAMILY_ID = TF.FAMILY_ID and tt.StatusUI<>''Update'')
SELECT *,ROW_NUMBER() OVER (ORDER BY SORT_ORDER ASC) AS RN into ##TestingNew FROM #TEMP  
update SS SET SS.ColumnRow=TT.rowimportid from ##TestingNew SS join [##IMPORTTEMP'+ @SID +'] TT on ss.FAMILY_ID=TT.FAMILY_ID and SS.PRODUCT_ID=TT.PRODUCT_ID 
update SS SET SS.ColumnRow=TT.SORT_ORDER from ##TestingNew SS join ##TestingNew TT on ss.FAMILY_ID=TT.FAMILY_ID and SS.PRODUCT_ID=TT.PRODUCT_ID where SS.ColumnRow=0
select ROW_NUMBER() OVER (PARTITION by FAMILY_ID ORDER BY ColumnRow)AS ROW_ID,* into ##GetMaxUpdate from ##TestingNew
UPDATE TPF SET TPF.SORT_ORDER = T.ROW_ID FROM TB_PROD_FAMILY TPF JOIN ##GetMaxUpdate T ON TPF.FAMILY_ID = T.FAMILY_ID AND TPF.PRODUCT_ID = T.PRODUCT_ID AND TPF.PUBLISH = T.PUBLISH')  

--    IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##GetMaxUpdate')
--	BEGIN
--				EXEC('DROP TABLE [##GetMaxUpdate]')
--	END	  
--	IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##NewSort')
--	BEGIN
--				EXEC('DROP TABLE [##NewSort]')
--	END	

--  select (' select TPF.family_id,Max(TPF.SORT_ORDER) as UpdateOrder into ##GetMaxUpdate from TB_PROD_FAMILY TPF join [##IMPORTTEMP'+ @SID +'] IMP on TPF.FAMILY_ID=IMP.FAMILY_ID where  IMP.StatusUI<>''Update'' GROUP BY TPF.FAMILY_ID ')

--select('select ROW_NUMBER() OVER (ORDER BY (select(1)))AS RO,Tff.FAMILY_ID,Tff.PRODUCT_ID,UpdateOrder into ##NewSort from ##GetMaxUpdate  TT join [##IMPORTTEMP'+ @SID +'] Tff ON Tff.FAMILY_ID=TT.FAMILY_ID and tff.StatusUI=''INSERT''')

--select ('UPDATE TPF SET TPF.SORT_ORDER = T.RO + T.UpdateOrder FROM TB_PROD_FAMILY TPF JOIN ##NewSort T ON TPF.FAMILY_ID = T.FAMILY_ID AND TPF.PRODUCT_ID = T.PRODUCT_ID')

   PRINT 'Import into LOGTEMPTABLE';
   IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##LOGTEMPTABLE'+ @SID +'')
			BEGIN
				EXEC('DROP TABLE [##LOGTEMPTABLE'+ @SID +']')
			END	  
   EXEC('SELECT DISTINCT IM.CATALOG_NAME, 
   (IM.CATEGORY_NAME+''(ID:''+ CONVERT(nvarchar,IM.CATEGORY_ID) +'')'') as CATEGORY_DETAILS,
   (IM.FAMILY_NAME +''(ID:''+CONVERT(nvarchar,IM.FAMILY_ID)+'')'') AS FAMILY_DETAILS,
   (IM.SUBFAMILY_NAME +''(ID:''+CONVERT(nvarchar,IM.SUBFAMILY_ID)+'')'') AS SUBFAMILY_DETAILS,  
   (IM.CATALOG_ITEM_NO +''(ID:''+CONVERT(nvarchar,IM.PRODUCT_ID)+'')'') AS PRODUCT_DETAILS,
   ''UPDATED'' AS STATUS INTO [##LOGTEMPTABLE'+ @SID +'] FROM [##IMPORTTEMP'+ @SID +'] IM ')  
   
     IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TempTest'+ @SID +'')  
   BEGIN  
    EXEC('DROP TABLE [##TempTest'+ @SID +']')  
   END  
   
     --EXEC('DROP TABLE [##TempTest'+ @SID +']')  
     PRINT N'Import Completed';  
     -------------------------------------Table Layout Generation:Main Family-------------------  
     DECLARE @ProdtblStructure NVARCHAR(MAX)='<?xml version="1.0" encoding="utf-8"?><TradingBell TableType="SuperTable"><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText>
     <DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader>
     <MergeSummaryFields></MergeSummaryFields></TradingBell>';  
     EXEC('INSERT INTO TB_FAMILY_TABLE_STRUCTURE(CATALOG_ID,FAMILY_ID,FAMILY_TABLE_STRUCTURE,STRUCTURE_NAME,IS_DEFAULT)   
     SELECT DISTINCT [##IMPORTTEMP'+ @SID +'].CATALOG_ID,[##IMPORTTEMP'+ @SID +'].FAMILY_ID,'''+ @ProdtblStructure +''',''Default Layout'',1 FROM [##IMPORTTEMP'+ @SID +'] where [##IMPORTTEMP'+ @SID +'].FAMILY_ID not in (select FAMILY_ID from TB_FAMILY_TABLE_STRUCTURE where IS_DEFAULT=1)')   
     EXEC('INSERT INTO TB_FAMILY_TABLE_STRUCTURE(CATALOG_ID,FAMILY_ID,FAMILY_TABLE_STRUCTURE,STRUCTURE_NAME,IS_DEFAULT)  
     SELECT DISTINCT 1,[##IMPORTTEMP'+ @SID +'].FAMILY_ID,'''+ @ProdtblStructure +''',''Default Layout'',1 FROM [##IMPORTTEMP'+ @SID +'] where [##IMPORTTEMP'+ @SID +'].FAMILY_ID not in (select FAMILY_ID from TB_FAMILY_TABLE_STRUCTURE where IS_DEFAULT=1)')   
      -----------------------------------------------------------------------------  
      -----------------------------------Table Layout Generation:Sub Family-------------------  
     DECLARE @ProdtblStructure1 NVARCHAR(MAX)='<?xml version="1.0" encoding="utf-8"?><TradingBell TableType="SuperTable"><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText>
     <DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader>
     <MergeSummaryFields></MergeSummaryFields></TradingBell>';  
     EXEC('INSERT INTO TB_FAMILY_TABLE_STRUCTURE(CATALOG_ID,FAMILY_ID,FAMILY_TABLE_STRUCTURE,STRUCTURE_NAME,IS_DEFAULT)   
     SELECT DISTINCT [##IMPORTTEMP'+ @SID +'].CATALOG_ID,[##IMPORTTEMP'+ @SID +'].SUBFAMILY_ID,'''+ @ProdtblStructure1 +''',''Default Layout'',1 FROM [##IMPORTTEMP'+ @SID +'] WHERE [##IMPORTTEMP'+ @SID +'].SUBFAMILY_ID IS NOT NULL and [##IMPORTTEMP'+ @SID +'].SUBFAMILY_ID not in (select FAMILY_ID from TB_FAMILY_TABLE_STRUCTURE where IS_DEFAULT=1)')   
     EXEC('INSERT INTO TB_FAMILY_TABLE_STRUCTURE(CATALOG_ID,FAMILY_ID,FAMILY_TABLE_STRUCTURE,STRUCTURE_NAME,IS_DEFAULT)  
     SELECT DISTINCT 1,[##IMPORTTEMP'+ @SID +'].SUBFAMILY_ID,'''+ @ProdtblStructure1 +''',''Default Layout'',1 FROM [##IMPORTTEMP'+ @SID +'] WHERE [##IMPORTTEMP'+ @SID +'].SUBFAMILY_ID IS NOT NULL and [##IMPORTTEMP'+ @SID +'].SUBFAMILY_ID not in (select FAMILY_ID from TB_FAMILY_TABLE_STRUCTURE where IS_DEFAULT=1)')   
      -----------------------------------------------------------------------------  

EXEC('UPDATE T SET  PUBLISH2WEB = B.PRODUCT_PUBLISH2WEB FROm TB_PROD_FAMILY T JOIN  [##IMPORTTEMP'+ @SID +'] B ON T.FAMILY_ID = (CASE WHEN B.SUBFAMILY_ID IS NOT NULL THEN  B.SUBFAMILY_ID ELSE B.FAMILY_ID END) AND 
T.PRODUCT_ID=B.PRODUCT_ID and  B.PRODUCT_PUBLISH2WEB is not null AND T.FLAG_RECYCLE =''A''')
EXEC('UPDATE T SET  PUBLISH2EXPORT = B.PRODUCT_PUBLISH2EXPORT FROm TB_PROD_FAMILY T JOIN  [##IMPORTTEMP'+ @SID +'] B ON T.FAMILY_ID = (CASE WHEN B.SUBFAMILY_ID IS NOT NULL THEN B.SUBFAMILY_ID ELSE B.FAMILY_ID END)  AND T.PRODUCT_ID=B.PRODUCT_ID and  B.PRODUCT_PUBLISH2EXPORT is not null AND T.FLAG_RECYCLE =''A''')
EXEC('UPDATE T SET  PUBLISH2PDF = B.PRODUCT_PUBLISH2PDF FROm TB_PROD_FAMILY T JOIN  [##IMPORTTEMP'+ @SID +'] B ON T.FAMILY_ID = (CASE WHEN B.SUBFAMILY_ID IS NOT NULL THEN B.SUBFAMILY_ID ELSE B.FAMILY_ID END) AND      T.PRODUCT_ID=B.PRODUCT_ID and  B.PRODUCT_PUBLISH2PDF is not null AND T.FLAG_RECYCLE =''A''')
EXEC('UPDATE T SET  PUBLISH2PRINT = B.PRODUCT_PUBLISH2PRINT FROm TB_PROD_FAMILY T JOIN  [##IMPORTTEMP'+ @SID +'] B ON T.FAMILY_ID = (CASE WHEN B.SUBFAMILY_ID IS NOT NULL THEN B.SUBFAMILY_ID ELSE B.FAMILY_ID END) AND  T.PRODUCT_ID=B.PRODUCT_ID and  B.PRODUCT_PUBLISH2PRINT is not null AND T.FLAG_RECYCLE =''A''')
EXEC('UPDATE T SET  PUBLISH2PORTAL =  B.PRODUCT_PUBLISH2PORTAL   FROm TB_PROD_FAMILY T JOIN  [##IMPORTTEMP'+ @SID +'] B ON T.FAMILY_ID = (CASE WHEN B.SUBFAMILY_ID IS NOT NULL THEN B.SUBFAMILY_ID ELSE B.FAMILY_ID END) AND T.PRODUCT_ID=B.PRODUCT_ID and  B.PRODUCT_PUBLISH2PORTAL is not null AND T.FLAG_RECYCLE =''A''') 

PRINT 'ERRORFLAG'  
PRINT @ERRORFLG  
 IF(@ERRORFLG=1)  
  BEGIN  
   EXEC('DROP TABLE [##LOGTEMPTABLE'+ @SID +']')  
   --EXEC('SELECT ERROR_MESSAGE() AS ErrorMessage,  
   --  ERROR_PROCEDURE() AS ErrorProcedure,  
   --  ERROR_SEVERITY() AS ErrorSeverity,  
   --  ERROR_STATE() AS ErrorState,  
   --  ERROR_NUMBER() AS ErrorNumber,  
   --  ERROR_LINE() AS ErrorLine INTO [##LOGTEMPTABLE'+ @SID +']')  
   
    EXEC('SELECT ''Import failed due to data error...'' AS ErrorMessage,
     ERROR_PROCEDURE() AS ErrorProcedure,  
     ERROR_SEVERITY() AS ErrorSeverity,  
     ERROR_STATE() AS ErrorState,  
     ERROR_NUMBER() AS ErrorNumber,  
     ERROR_LINE() AS ErrorLine INTO [##LOGTEMPTABLE'+ @SID +']')  
  END  
END  
ELSE  
BEGIN  
 EXEC('SELECT ''FAMILY DETAILS UPDATED''as STATUS INTO [##LOGTEMPTABLE'+ @SID +']')  
END  
  

Print 'TEST Prod11'   
  


exec('
if((select count(subfamily_id) from [##IMPORTTEMP'+ @SID +'] where subfamily_id is not null)>0)
Begin
SELECT   ROW_NUMBER() OVER (PARTITION BY FAMILY_ID ORDER BY MODIFIED_DATE)AS RO,*  INTO #TEMP1 from (
 select distinct tpf.MODIFIED_DATE,tpf.FAMILY_ID,PARENT_FAMILY_ID,tpf.product_id,tpf.PUBLISH FROM  TB_PROD_FAMILY tpf join  [##IMPORTTEMP'+ @SID +']  tt on tpf.family_id=tt.subfamily_id  and tt.StatusUI<>''Update''
 join TB_FAMILY tf on tt.family_id=tf.PARENT_FAMILY_ID
 WHERE tpf.FLAG_RECYCLE =''A''   )as tt ORDER BY MODIFIED_DATE

UPDATE TPF SET TPF.SORT_ORDER = T.RO FROM TB_PROD_FAMILY TPF 
JOIN #TEMP1 T ON TPF.FAMILY_ID = T.FAMILY_ID AND TPF.PRODUCT_ID = T.PRODUCT_ID AND TPF.PUBLISH = T.PUBLISH 
End')
 EXEC('SELECT * from [##LOGTEMPTABLE'+ @SID +']')  
 select 'Final'
IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##ATTRSORT')  
   BEGIN  
    EXEC('DROP TABLE ##ATTRSORT')  
   END    
  
END






