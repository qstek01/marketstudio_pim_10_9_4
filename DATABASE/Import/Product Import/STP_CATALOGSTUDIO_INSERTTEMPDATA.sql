



--select * from [##IMPORTTEMP7d14b811-79db-4319-b238-90205614f645]

ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO_INSERTTEMPDATA](@TABLENAME NVARCHAR(500))
As 
BEGIN

IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TEMPTABLE')  
   BEGIN  
    EXEC('drop table ##TEMPTABLE ')  
   END  

DECLARE @COLUMNS nVARCHAR(MAX)=''
SELECT @COLUMNS =@COLUMNS +' case when ISNUMERIC(['+ col.name + '])=1 then ['+ col.name + '] else CONVERT(NVARCHAR(MAX), CONVERT(VARBINARY(MAX), CONVERT(IMAGE, CAST(['+ col.name + '] AS VARBINARY(MAX))),2)) end as ['+col.name+'],'  from tempdb.sys.columns col join tempdb.sys.tables tab on col.object_id=tab.object_id and tab.name like ''+replace(replace(@TABLENAME,'[',''),']','')+'%' --and col.name not in ('PRODUCT_ID','CATALOG_ITEM_NO')
 order by col.column_id
SET @COLUMNS = LEFT(@COLUMNS, LEN(@COLUMNS) - 1)
print @COLUMNS

 EXEC('select '+@COLUMNS+' into ##TEMPTABLE from'+@TABLENAME+'')
 IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME=''+replace(replace(@TABLENAME,'[',''),']','')+'')  
   BEGIN  
    EXEC('drop table  '+@TABLENAME+'')  
   END 

   EXEC('select '+@COLUMNS+',0 as rowImportid into '+@TABLENAME+' from
   (select '+@COLUMNS+' from [##TEMPTABLE])as tep1')
   IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TEMPTABLE')  
   BEGIN  
    EXEC('drop table ##TEMPTABLE ')  
   END  
 --select * from [##IMPORTTEMP9483331c-359c-46ad-982a-5900c80d8cb8] order by rowImportid 
 END
 
    