

ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO5_PROD_BASED_ASSOCIATE_REMOVAL]
@SID VARCHAR(MAX)='51fcdba9-b317-4463-bdf6-a248a746c576',
@CUSTOMERNAME VARCHAR(MAX)='demo@questudio.com',
@USERID VARCHAR(MAX)=8


AS
BEGIN
DECLARE @CNT INT=0
Declare @Prodids varchar(max)
Declare @DEFAULT_FAMILY varchar(max)

  ---------------------------------------------------OBJECT CHECKING------------------------------------------------------------------ 
    IF OBJECT_ID('tempdb..##REMOVEFAM') IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##REMOVEFAM')
	END
	IF OBJECT_ID('tempdb..##PRODIDS') IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##PRODIDS')
	END
	IF OBJECT_ID('tempdb..##TEMP1') IS NOT NULL
	BEGIN
		EXEC('DROP TABLE ##TEMP1')
	END
  ---------------------------------------------------OBJECT CHECKING------------------------------------------------------------------ 
	
  ---------------------------------------------------FAMILY_ID ADDITION------------------------------------------------------------------
  IF NOT EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'FAMILY_ID' AND TABLE_NAME = '##IMPORTTEMP'+ @SID +'')
  BEGIN
  PRINT 'FAMILY_ID ADDITION'
  EXEC('ALTER TABLE [##IMPORTTEMP'+ @SID +'] add  FAMILY_ID INT')
  --EXEC('UPDATE TMP set TMP.FAMILY_ID=TPF.FAMILY_ID from [##IMPORTTEMP'+ @SID +'] TMP join TB_PROD_FAMILY TPF on TMP.PRODUCT_ID=TPF.PRODUCT_ID AND TPF.FLAG_RECYCLE=''A''')
  END
  ---------------------------------------------------FAMILY_ID ADDITION------------------------------------------------------------------

  ---------------------------------------------------PRODCOUNT ADDITION------------------------------------------------------------------

  IF NOT EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PRODCOUNT' AND TABLE_NAME = '##IMPORTTEMP'+ @SID +'')
  BEGIN
  PRINT 'PRODCOUNT ADDITION'
  EXEC('ALTER TABLE [##IMPORTTEMP'+ @SID +'] add  PRODCOUNT INT')
    EXEC('update [##IMPORTTEMP'+ @SID +'] set PRODCOUNT = (select count (*) from TB_PROD_FAMILY where TB_PROD_FAMILY.PRODUCT_ID =
	[##IMPORTTEMP'+ @SID +'].PRODUCT_ID 	AND TB_PROD_FAMILY.FLAG_RECYCLE=''A'')')
  END
  ---------------------------------------------------PRODCOUNT ADDITION------------------------------------------------------------------

  ---------------------------------------------------TEMPTABLE ADDITION------------------------------------------------------------------
  PRINT 'PRODUCT_ID BASED PRODUCT_REMOVAL'
 SET @CNT=(SELECT COUNT(C.NAME) FROM TEMPDB.SYS.TABLES T INNER JOIN TEMPDB.SYS.COLUMNS C ON C.OBJECT_ID=T.OBJECT_ID 
 WHERE T.NAME='##IMPORTTEMP' + @SID + '' AND C.NAME IN ('PRODUCT_ID','REMOVE_ASSOCIATE'))
 print @CNT
 EXEC('SELECT *into ##REMOVEFAM from  [##IMPORTTEMP'+ @SID +']')
  ---------------------------------------------------TEMPTABLE ADDITION------------------------------------------------------------------
 
  ------------------------------------------------------DEFAULT_FAMILY-------------------------------------------------------
  SELECT	@DEFAULT_FAMILY=DEFAULT_FAMILY FROM TB_CATALOG WHERE CATALOG_ID in (SELECT CATALOG_ID from ##REMOVEFAM WHERE REMOVE_ASSOCIATE='YES')
  ------------------------------------------------------DEFAULT_FAMILY-------------------------------------------------------

IF EXISTS(SELECT FAMILY_ID FROM ##REMOVEFAM WHERE FAMILY_ID IS NOT NULL)
BEGIN 
	PRINT 'Detach Process'
	SELECT PRODUCT_ID,FAMILY_ID INTO ##PRODIDS FROM  ##REMOVEFAM WHERE REMOVE_ASSOCIATE='YES' AND PRODCOUNT > 1

	DELETE TPF FROM TB_PROD_FAMILY TPF JOIN ##PRODIDS PDS ON TPF.PRODUCT_ID= PDS.PRODUCT_ID 
	AND TPF.FAMILY_ID= PDS.FAMILY_ID 

END
ELSE
BEGIN

	SELECT PRODUCT_ID INTO ##PRODIDS1 FROM  ##REMOVEFAM WHERE REMOVE_ASSOCIATE='YES'

	DELETE FROM TB_PROD_FAMILY WHERE PRODUCT_ID  in (SELECT PRODUCT_ID from ##REMOVEFAM WHERE REMOVE_ASSOCIATE='YES' AND PRODCOUNT > 1
	 AND FAMILY_ID=@DEFAULT_FAMILY)
END

IF EXISTS(SELECT PRODCOUNT FROM ##REMOVEFAM WHERE PRODCOUNT =1)
BEGIN 
  PRINT 'R-Flag Process'

  EXEC('UPDATE TMP set tmp.FLAG_RECYCLE=''R'' from TB_PROD_FAMILY TMP join [##IMPORTTEMP'+ @SID +'] TPF on TMP.PRODUCT_ID=TPF.PRODUCT_ID and TMP.FAMILY_ID=TPF.FAMILY_ID AND  tpf.PRODCOUNT=1')
  PRINT 'Recycle Process'

  EXEC('INSERT INTO TB_RECYCLE_TABLE select ( SELECT CAST(RAND() * 10000 AS INT) AS [RandomNumber]) as  XML_ID,[CATALOG_ID], '''' as [CATEGORY_ID],'''' as [CATEGORY_NAME],
		FAMILY_Id ,'''' as [FAMILY_NAME] , '''' as [SUB_FAMILY_ID],'''' as [SUB_FAMILY_NAME],[PRODUCT_ID],CATALOG_ITEM_NO as [PRODUCT_NAME],'''' as[XML_DOC],'''+ @CUSTOMERNAME +''' 
		AS [DELETED_USER],GETDATE() AS DELETED_DATE,'''' AS [CATEGORY_SHORT],''A'' AS FLAG_RECYCLE ,NULL AS SUBPRODUCT_NAME,''Product'' as [RECYCLE_TYPE],NULL AS SUBPRODUCT_ID from
		 [##IMPORTTEMP'+ @SID +'] where PRODCOUNT=1')
END



	

	PRINT 'UPDATE SORT ORDER STEP-ONE'
	exec('SELECT ROW_NUMBER() OVER (PARTITION BY FAMILY_ID ORDER BY FAMILY_ID )AS RO,* into ##TEMP1 FROM  TB_PROD_FAMILY WHERE FAMILY_ID in 
	(select family_id from TB_PROD_FAMILY where PRODUCT_ID in
	 (SELECT PRODUCT_ID from [##IMPORTTEMP'+@SID+'] WHERE REMOVE_ASSOCIATE=''YES'')) ANd FLAG_RECYCLE =''A''  ORDER BY MODIFIED_DATE')

	 PRINT 'UPDATE SORT ORDER STEP-TWO'
	UPDATE TPF SET TPF.SORT_ORDER = T.RO FROM TB_PROD_FAMILY TPF 
	JOIN ##TEMP1 T ON TPF.FAMILY_ID = T.FAMILY_ID AND TPF.PRODUCT_ID = T.PRODUCT_ID 
	AND TPF.PUBLISH = T.PUBLISH and TPF.FLAG_RECYCLE='A' --and TPF.FAMILY_ID =@FAMILY_ID
	
	DROP TABLE ##TEMP1
	
	---------LOG PROCESS-----------------
		 EXEC('SELECT DISTINCT [##IMPORTTEMP'+ @SID +'].CATALOG_ITEM_NO, ''DETACHED'' STATUS INTO [##LOGTEMPTABLE'+ @SID +'] FROM 
		 [##IMPORTTEMP'+ @SID +'] WHERE [##IMPORTTEMP'+ @SID +'].REMOVE_ASSOCIATE=''YES'' ')
	---------LOG PROCESS-----------------

 END

