

ALTER procedure [dbo].[STP_CATALOGSTUDIO5_CATEGORYATTRIBUTE_SPECSIMPORT] 

@SID VARCHAR(100)='20a17c7d-e03e-43df-b060-df1c2500ef53',
@CUSTOMER_ID VARCHAR(100)=2,
@CUSTOMERNAME NVARCHAR(500)='Kelly'
AS
BEGIN
----------------*****Create Temp Table Using For Split Column Values*************-----------------------
EXEC('Drop Index IX_TestTableone on [##IMPORTTEMP'+ @SID +']')
EXEC('ALTER TABLE [##IMPORTTEMP'+ @SID +']
DROP COLUMN rowImportid;')
	DECLARE @Catalog_ID INT=0

	IF(OBJECT_ID('TEMPDB..##CATALOGDETAILS') IS NOT NULL) drop table ##CATALOGDETAILS

	EXEC('select CATALOG_ID as CATALOG_ID into ##CATALOGDETAILS from [##IMPORTTEMP'+ @SID +']')

	SET @Catalog_ID=(select DISTINCT (CATALOG_ID) FROM ##CATALOGDETAILS)

    print @Catalog_ID

	IF (OBJECT_ID('tempdb..#TEMP_KNOWN_COLUMNS') is not null) drop table #TEMP_KNOWN_COLUMNS

	CREATE TABLE #TEMP_KNOWN_COLUMNS (Name_Flag VARCHAR(200))

INSERT INTO #TEMP_KNOWN_COLUMNS VALUES('CATEGORY_ID'),('SUBCATID_L1'),('SUBCATID_L2'),('SUBCATID_L3'),('SUBCATID_L4'),('SUBCATID_L5'),('SUBCATID_L6'),('SUBCATID_L7')

----------------*****Create Temp Table Using For Split Column Values END*************-----------------------


--------------------***** Store Import Column in the TempTable**************--------------------------------

	IF(OBJECT_ID('tempdb..#temp_ColumnNames') IS NOT NULL) drop table #temp_ColumnNames

	SELECT * INTO #temp_ColumnNames FROM TEMPDB.sys.all_columns A LEFT JOIN  #TEMP_KNOWN_COLUMNS B ON (A.name=B.Name_Flag)
WHERE object_id= OBJECT_ID('tempdb..[##IMPORTTEMP'+ @SID +']') AND  name NOT IN ('Action','Family_Status','FStatusUI','StatusUI','SFSort_Order','Sort_Order')


--------------------***** Store Import Column in the TempTableEnd**************--------------------------------

--------------------****** GET ORIGNAL COLUMN IN EXCEL SHEET COLUMN***********------------------------------

	IF(OBJECT_ID('tempdb..##temp_AttributeColumnNames') IS NOT NULL) drop table ##temp_AttributeColumnNames

	EXEC('SELECT row_number() over(ORDER BY(select 1)) as Row#,* into ##temp_AttributeColumnNames from [##AttributeTemp'+@SID+'] A 
 LEFT JOIN  #TEMP_KNOWN_COLUMNS B ON (A.ATTRIBUTE_NAME=B.Name_Flag)')



--------------------****** GET ORIGNAL COLUMN IN EXCEL SHEET COLUMN END***********------------------------------

-------------------******* DECLAREATION PART*******------------------------------------------------------


	DECLARE @Get_Filtered_ParentColumn_Names VARCHAR(2000)=''  
	DECLARE @Get_Filtered_AttributeParentColumn_Names VARCHAR(2000)='' 
	DECLARE @Get_Filtered_SubcategoryColumn_Names1 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_AttributeSubcategoryColumn_Names1 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_SubcategoryColumn_Names2 VARCHAR(2000)=''  
	DECLARE @Get_Filtered_AttributeSubcategoryColumn_Names2 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_SubcategoryColumn_Names3 VARCHAR(2000)=''  
	DECLARE @Get_Filtered_AttributeSubcategoryColumn_Names3 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_SubcategoryColumn_Names4 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_AttributeSubcategoryColumn_Names4 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_SubcategoryColumn_Names5 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_AttributeSubcategoryColumn_Names5 VARCHAR(2000)=''  
	DECLARE @Get_Filtered_SubcategoryColumn_Names6 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_AttributeSubcategoryColumn_Names6 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_SubcategoryColumn_Names7 VARCHAR(2000)='' 
	DECLARE @Get_Filtered_AttributeSubcategoryColumn_Names7 VARCHAR(2000)=''   
	DECLARE @Previous_Column_Name VARCHAR(1000)=''
	DECLARE @Flag_Already_InProcess VARCHAR(10)=''
	DECLARE @Count int =0
	DECLARE @Get_Category_AttributeName VARCHAR(2000)=''
	DECLARE @Count_Columns int = (SELECT COUNT(*) FROM #temp_ColumnNameS)
	DECLARE @InsertCount_Details int=0
	DECLARE @UpdateCount_Details int=0


 -------------------******* DECLAREATION PART*******------------------------------------------------------
 ------------------*********SPLIT THE ROOT LEVEl AND SUB LEVEL**************------------------------------
WHILE (@Count_Columns>0)
BEGIN


     DECLARE @Get_Column_Name VARCHAR(500) = (SELECT TOP 1 name FROM #temp_ColumnNames )
	 DECLARE @Get_Column_Flag VARCHAR(500) = (SELECT TOP 1 NAME_FLAG FROM #temp_ColumnNames )
	 DECLARE @Get_Column_Id INT = (SELECT TOP 1 column_id FROM #temp_ColumnNames )
	 DECLARE @Get_AttributeColumn_Id INT= (select TOP 1 Row# from ##temp_AttributeColumnNames)
	 DECLARE @Get_Attribute_Name Varchar(500)=(select TOP 1 ATTRIBUTE_NAME from ##temp_AttributeColumnNames)
	 
	 IF(@Get_Column_Flag <> 'NULL')
	 BEGIN
		set @Count = @Count +1
	 END
	 IF(@Count>0)
	 BEGIN

		IF(@Count=1)
		BEGIN
			SET @Get_Filtered_ParentColumn_Names=@Get_Filtered_ParentColumn_Names+','+@Get_Column_Name
			SET @Get_Filtered_AttributeParentColumn_Names=@Get_Filtered_AttributeParentColumn_Names+','+@Get_Attribute_Name
		END

		IF(@Count=2)
		BEGIN
			SET @Get_Filtered_SubcategoryColumn_Names1=@Get_Filtered_SubcategoryColumn_Names1+','+@Get_Column_Name
			SET @Get_Filtered_AttributeSubcategoryColumn_Names1=@Get_Filtered_AttributeSubcategoryColumn_Names1+','+@Get_Attribute_Name
		END

		IF(@Count=3)
		BEGIN
			SET @Get_Filtered_SubcategoryColumn_Names2=@Get_Filtered_SubcategoryColumn_Names2+','+@Get_Column_Name
			SET @Get_Filtered_AttributeSubcategoryColumn_Names2=@Get_Filtered_AttributeSubcategoryColumn_Names2+','+@Get_Attribute_Name
		END

		IF(@Count=4)
		BEGIN
			SET @Get_Filtered_SubcategoryColumn_Names3=@Get_Filtered_SubcategoryColumn_Names3+','+@Get_Column_Name
			SET @Get_Filtered_AttributeSubcategoryColumn_Names3=@Get_Filtered_AttributeSubcategoryColumn_Names3+','+@Get_Attribute_Name
		END

		IF(@Count=5)
		BEGIN
			SET @Get_Filtered_SubcategoryColumn_Names4=@Get_Filtered_SubcategoryColumn_Names4+','+@Get_Column_Name
			SET @Get_Filtered_AttributeSubcategoryColumn_Names4=@Get_Filtered_AttributeSubcategoryColumn_Names4+','+@Get_Attribute_Name
		END

		IF(@Count=6)
		BEGIN
			SET @Get_Filtered_SubcategoryColumn_Names5=@Get_Filtered_SubcategoryColumn_Names5+','+@Get_Column_Name
			SET @Get_Filtered_AttributeSubcategoryColumn_Names5=@Get_Filtered_AttributeSubcategoryColumn_Names5+','+@Get_Attribute_Name
		END

		IF(@Count=7)
		BEGIN
			SET @Get_Filtered_SubcategoryColumn_Names6=@Get_Filtered_SubcategoryColumn_Names6+','+@Get_Column_Name
			SET @Get_Filtered_AttributeSubcategoryColumn_Names6=@Get_Filtered_AttributeSubcategoryColumn_Names6+','+@Get_Attribute_Name
		END

		IF(@Count=8)
		BEGIN
			SET @Get_Filtered_SubcategoryColumn_Names7=@Get_Filtered_SubcategoryColumn_Names7+','+@Get_Column_Name
			SET @Get_Filtered_AttributeSubcategoryColumn_Names7=@Get_Filtered_AttributeSubcategoryColumn_Names7+','+@Get_Attribute_Name
		END

	 END
     
	DELETE FROM #temp_ColumnNames WHERE column_id IN (@Get_Column_Id )
	DELETE FROM ##temp_AttributeColumnNames WHERE ROW# IN (@Get_AttributeColumn_Id )

	SET @Count_Columns = (SELECT COUNT(*) FROM #temp_ColumnNameS)

  END
   ------------------*********SPLIT THE ROOT LEVEl AND SUB LEVEL END **************------------------------------

   ------------------*********ROOT LEVEL CATEGORY ****************-----------------------------------------------
   select @Get_Filtered_ParentColumn_Names as Testing
   select @Get_Filtered_AttributeParentColumn_Names
 IF (@Get_Filtered_ParentColumn_Names <>'' OR @Get_Filtered_ParentColumn_Names<>null and @Get_Filtered_AttributeParentColumn_Names <>'' OR @Get_Filtered_AttributeParentColumn_Names<>null )
  BEGIN

      IF(OBJECT_ID('tempdb..##Parent_Category') is not null) drop table ##Parent_Category

      SET @Get_Filtered_ParentColumn_Names=RIGHT(@Get_Filtered_ParentColumn_Names,LEN(@Get_Filtered_ParentColumn_Names)-1)

	  SET @Get_Filtered_ParentColumn_Names = REPLACE(@Get_Filtered_ParentColumn_Names, ',', '],[') 

      SET @Get_Filtered_ParentColumn_Names = REPLACE(@Get_Filtered_ParentColumn_Names, ')', '])')

      SET @Get_Filtered_ParentColumn_Names = REPLACE(@Get_Filtered_ParentColumn_Names, '(', '([')

	  SET @Get_Filtered_ParentColumn_Names='[' + @Get_Filtered_ParentColumn_Names +']'

      EXEC('SELECT '+ @Get_Filtered_ParentColumn_Names+ ' into ##Parent_Category  FROM [##IMPORTTEMP'+ @SID +']') 
	
      select @Get_Category_AttributeName=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE(name,']',']]') + ']' FROM TEMPDB.sys.all_columns where object_id= OBJECT_ID('tempdb..##Parent_Category') and name not like 'CATEGORY_ID' and  name not like 'CATEGORY_NAME' and  name not like 'CATEGORY_PUBLISH2WEB' FOR XML PATH('')),2,200000))
	  select @Get_Category_AttributeName
      SELECT @Get_Category_AttributeName = (SELECT REPLACE(@Get_Category_AttributeName, ',[],', ','))
	  select @Get_Category_AttributeName
   IF(@Get_Category_AttributeName IS NOT NULL)
    BEGIN

       IF(OBJECT_ID('tempdb..##ParentColumn_Names') is not null) drop table ##ParentColumn_Names
	  
       EXEC ('SELECT row_number() over(ORDER BY (Select 1)) as Row#,CATEGORY_ID,CATEGORY_NAME,PA.ATTRIBUTE_NAME,PA.STRING_VALUE,TA.ATTRIBUTE_ID,TA.ATTRIBUTE_TYPE,TA.ATTRIBUTE_DATATYPE into ##ParentColumn_Names FROM(SELECT  CATEGORY_ID,CATEGORY_NAME,ATTRIBUTE_NAME,STRING_VALUE FROM [##Parent_Category] 
       UNPIVOT (        
       STRING_VALUE      
       FOR ATTRIBUTE_NAME IN ('+@Get_Category_AttributeName+') ) P)PA join TB_ATTRIBUTE TA ON PA.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME')

	    ALTER TABLE ##ParentColumn_Names ADD FLAG Varchar(50)

       IF(OBJECT_ID('tempdb..##AttributeParent_Category') is not null) drop table ##AttributeParent_Category

		SET @Get_Filtered_AttributeParentColumn_Names=RIGHT(@Get_Filtered_AttributeParentColumn_Names,LEN(@Get_Filtered_AttributeParentColumn_Names)-1)

	    if(OBJECT_ID('tempdb..#AttributeParentColumn_Names') is not null) drop table #AttributeParentColumn_Names

	    CREATE table #AttributeParentColumn_Names
       (
        ATTRIBUTE_NAME varchar(500)
       ) 
	        
       Insert Into #AttributeParentColumn_Names select @Get_Filtered_AttributeParentColumn_Names 

	   if(OBJECT_ID('tempdb..##OriginalParentAttributeColumn') is not null) drop table ##OriginalParentAttributeColumn

       select item into ##OriginalParentAttributeColumn from #AttributeParentColumn_Names CROSS APPLY SplitString(ATTRIBUTE_NAME, ',')

       EXEC('SELECT row_number() OVER(ORDER BY (SELECT 1))AS Row#,* into ##AttributeParent_Category FROM [##OriginalParentAttributeColumn] where item NOT IN (''CATEGORY_ID'',''CATEGORY_NAME'') ')
	   

       UPDATE ##ParentColumn_Names SET ##ParentColumn_Names.ATTRIBUTE_NAME = ##AttributeParent_Category.item,##ParentColumn_Names.FLAG='Insert'  FROM ##ParentColumn_Names, ##AttributeParent_Category WHERE ##ParentColumn_Names.Row# = ##AttributeParent_Category.Row# 

       DECLARE @Get_CATEGORY_SPECS_COUNT INT=0
       Declare @Get_AllCategory_Spec_Count INT=0

       SET @Get_AllCategory_Spec_Count=(select count(*) from TB_CATEGORY_SPECS TCS Join ##ParentColumn_Names SCN ON TCS.CATEGORY_ID=SCN.CATEGORY_ID and TCS.CATALOG_ID=@Catalog_ID)

       SET @Get_CATEGORY_SPECS_COUNT=(select count(*) from TB_CATEGORY_SPECS TCS Join ##ParentColumn_Names SCN ON TCS.CATEGORY_ID=SCN.CATEGORY_ID and TCS.CATALOG_ID=@Catalog_ID and TCS.ATTRIBUTE_ID=SCN.ATTRIBUTE_ID)

	  select @Get_CATEGORY_SPECS_COUNT

     IF(@Get_CATEGORY_SPECS_COUNT<>0)

        BEGIN
		  UPDATE ##ParentColumn_Names SET ##ParentColumn_Names.Row#=TB_CATEGORY_SPECS.SORT_ORDER,##ParentColumn_Names.FLAG='Update' from TB_CATEGORY_SPECS ,##ParentColumn_Names  where  TB_CATEGORY_SPECS.CATEGORY_ID=##ParentColumn_Names.CATEGORY_ID and TB_CATEGORY_SPECS.CATALOG_ID=@Catalog_ID and TB_CATEGORY_SPECS.ATTRIBUTE_ID=##ParentColumn_Names.ATTRIBUTE_ID
        END

      IF(@Get_AllCategory_Spec_Count=0)

       BEGIN

       EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
       SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##ParentColumn_Names].STRING_VALUE))),Replace(substring([##ParentColumn_Names].ATTRIBUTE_DATATYPE,charindex(''('',[##ParentColumn_Names].ATTRIBUTE_DATATYPE)+1,len([##ParentColumn_Names].ATTRIBUTE_DATATYPE)),'')'',''''))
       else  convert(nvarchar(max),[##ParentColumn_Names].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##ParentColumn_Names].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##ParentColumn_Names].Row#,'''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##ParentColumn_Names] join TB_CATEGORY  on [##ParentColumn_Names].CATEGORY_ID= TB_CATEGORY.CATEGORY_ID
       WHERE [##ParentColumn_Names].CATEGORY_ID IS NOT NULL AND [##ParentColumn_Names].ATTRIBUTE_DATATYPE LIKE ''Text%'' ')
       END

     IF(@Get_AllCategory_Spec_Count<>0)
       BEGIN

       DECLARE @MAXATTRID VARCHAR(500)  
	   SET @MAXATTRID = (select MAX(SORT_ORDER) from TB_CATEGORY_SPECS TCS Join ##ParentColumn_Names SCN ON TCS.CATEGORY_ID=SCN.CATEGORY_ID and TCS.CATALOG_ID=@Catalog_ID)
       EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
       SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##ParentColumn_Names].STRING_VALUE))),Replace(substring([##ParentColumn_Names].ATTRIBUTE_DATATYPE,charindex(''('',[##ParentColumn_Names].ATTRIBUTE_DATATYPE)+1,len([##ParentColumn_Names].ATTRIBUTE_DATATYPE)),'')'',''''))
       else  convert(nvarchar(max),[##ParentColumn_Names].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##ParentColumn_Names].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##ParentColumn_Names].Row# +'+@MAXATTRID+','''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##ParentColumn_Names] join TB_CATEGORY  on [##ParentColumn_Names].CATEGORY_ID= TB_CATEGORY.CATEGORY_ID
       WHERE [##ParentColumn_Names].CATEGORY_ID IS NOT NULL AND [##ParentColumn_Names].ATTRIBUTE_DATATYPE LIKE ''Text%'' and [##ParentColumn_Names].Flag=''Insert'' ')
       EXEC('UPDATE TB_CATEGORY_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.STRING_VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
       else convert(nvarchar(max),SIT.STRING_VALUE) End FROM [##ParentColumn_Names] SIT WHERE SIT.ATTRIBUTE_ID=TB_CATEGORY_SPECS.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%''  AND  SIT.Flag=''Update''')
   END
  Set @InsertCount_Details=@InsertCount_Details + (Select count(*) from ##ParentColumn_Names where FLAG='Insert')
 SET @UpdateCount_Details=@UpdateCount_Details + (Select count(*) from ##ParentColumn_Names where FLAG='Update')
 END
END

 ------------------*********ROOT LEVEL CATEGORY END ****************-----------------------------------------------
 ------------------*********SUBCATEGORY LEVEL1 CATEGORY ****************------------------------------------------
   IF (@Get_Filtered_SubcategoryColumn_Names1 <>'' OR @Get_Filtered_SubcategoryColumn_Names1<>null and @Get_Filtered_AttributeSubcategoryColumn_Names1 <>'' OR @Get_Filtered_AttributeSubcategoryColumn_Names1<>null )
  BEGIN
   Print'SUB1'
  IF(OBJECT_ID('tempdb..##SUB_Category') is not null) drop table ##SUB_Category
  SET @Get_Filtered_SubcategoryColumn_Names1=RIGHT(@Get_Filtered_SubcategoryColumn_Names1,LEN(@Get_Filtered_SubcategoryColumn_Names1)-1)
  select @Get_Filtered_SubcategoryColumn_Names1
  SET @Get_Filtered_SubcategoryColumn_Names1 = REPLACE(@Get_Filtered_SubcategoryColumn_Names1, ',', '],[') 
  SET @Get_Filtered_SubcategoryColumn_Names1 = REPLACE(@Get_Filtered_SubcategoryColumn_Names1, ')', '])')
  SET @Get_Filtered_SubcategoryColumn_Names1 = REPLACE(@Get_Filtered_SubcategoryColumn_Names1, '(', '([')
  SET @Get_Filtered_SubcategoryColumn_Names1='[' + @Get_Filtered_SubcategoryColumn_Names1 +']'
  EXEC('SELECT '+ @Get_Filtered_SubcategoryColumn_Names1+ ' into ##SUB_Category  FROM [##IMPORTTEMP'+ @SID +']') 
  SET @Get_Category_AttributeName=''
  select @Get_Category_AttributeName=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE(name,']',']]') + ']' FROM TEMPDB.sys.all_columns where object_id= OBJECT_ID('tempdb..##SUB_Category') and name not like 'SUBCATID_L1' and  name not like 'SUBCATNAME_L1' and  name not like 'CATEGORY_PUBLISH2WEB' and  name not like 'CATEGORY_PUBLISH2PRINT' and  name not like 'CATEGORY_PUBLISH2PDF' and  name not like 'CATEGORY_PUBLISH2EXPORT' and  name not like 'CATEGORY_PUBLISH2PORTAL' FOR XML PATH('')),2,200000))
  SELECT @Get_Category_AttributeName = (SELECT REPLACE(@Get_Category_AttributeName, ',[],', ','))

  select @Get_Category_AttributeName
  IF(@Get_Category_AttributeName IS NOT NULL)
   BEGIN
   Print'SUB2'
    IF(OBJECT_ID('tempdb..##SubColumn_Names') is not null) drop table ##SubColumn_Names
    EXEC ('SELECT row_number() over(ORDER BY (Select 1)) as Row#,SUBCATID_L1,SUBCATNAME_L1,PA.ATTRIBUTE_NAME,PA.STRING_VALUE into ##SubColumn_Names FROM(SELECT SUBCATID_L1,SUBCATNAME_L1,ATTRIBUTE_NAME,STRING_VALUE FROM [##SUB_Category] 
    UNPIVOT (        
    STRING_VALUE      
    FOR ATTRIBUTE_NAME IN ('+@Get_Category_AttributeName+') ) P)PA')
   if(OBJECT_ID('tempdb..##AttributeSub_Category') is not null) drop table ##AttributeSub_Category
   SET @Get_Filtered_AttributeSubcategoryColumn_Names1=RIGHT(@Get_Filtered_AttributeSubcategoryColumn_Names1,LEN(@Get_Filtered_AttributeSubcategoryColumn_Names1)-1)
   if(OBJECT_ID('tempdb..##AttributeSubColumn_Names') is not null) drop table ##AttributeSubColumn_Names
   CREATE table ##AttributeSubColumn_Names
   (
   ATTRIBUTE_NAME varchar(500)
   )      
    Insert Into ##AttributeSubColumn_Names select @Get_Filtered_AttributeSubcategoryColumn_Names1 
    if(OBJECT_ID('tempdb..##OriginalSubAttributeColumn') is not null) drop table ##OriginalSubAttributeColumn
    select item into ##OriginalSubAttributeColumn from ##AttributeSubColumn_Names CROSS APPLY SplitString(ATTRIBUTE_NAME, ',')
    EXEC('SELECT row_number() OVER(ORDER BY (SELECT 1))AS Row#,* into ##AttributeSub_Category FROM [##OriginalSubAttributeColumn] where item NOT IN (''SUBCATID_L1'',''SUBCATNAME_L1'',''CATEGORY_PUBLISH2WEB'',''CATEGORY_PUBLISH2PRINT'',''CATEGORY_PUBLISH2PDF'',''CATEGORY_PUBLISH2EXPORT'',''CATEGORY_PUBLISH2PORTAL'') ')
    ALTER TABLE ##SubColumn_Names ADD FLAG Varchar(50)
    UPDATE ##SubColumn_Names SET ##SubColumn_Names.ATTRIBUTE_NAME = ##AttributeSub_Category.item, ##SubColumn_Names.FLAG='Insert' FROM ##SubColumn_Names, ##AttributeSub_Category 
    WHERE ##SubColumn_Names.Row# = ##AttributeSub_Category.Row# 
    if(OBJECT_ID('tempdb..##SUBCATEGORYAttribute1_Column') is not null) drop table ##SUBCATEGORYAttribute1_Column
    select PA.ROW#,PA.SUBCATID_L1,PA.SUBCATNAME_L1,PA.ATTRIBUTE_NAME,PA.STRING_VALUE,TA.ATTRIBUTE_ID,TA.ATTRIBUTE_DATATYPE,TA.ATTRIBUTE_TYPE,PA.FLAG INTo ##SUBCATEGORYAttribute1_Column from ##SubColumn_Names PA join TB_ATTRIBUTE TA ON PA.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME 
    DECLARE @Get_SUBCATEGORY_SPECS_COUNT INT=0
    Declare @Get_AllSUBCategory_Spec_Count INT=0
    SET @Get_AllSUBCategory_Spec_Count=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute1_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L1 and TCS.CATALOG_ID=@Catalog_ID)
    SET @Get_SUBCATEGORY_SPECS_COUNT=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute1_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L1 and TCS.CATALOG_ID=@Catalog_ID and TCS.ATTRIBUTE_ID=SCN.ATTRIBUTE_ID)
    IF(@Get_SUBCATEGORY_SPECS_COUNT<>0)
    BEGIN
     UPDATE ##SUBCATEGORYAttribute1_Column SET ##SUBCATEGORYAttribute1_Column.Flag='Update',##SUBCATEGORYAttribute1_Column.Row#=TB_CATEGORY_SPECS.SORT_ORDER from TB_CATEGORY_SPECS ,##SUBCATEGORYAttribute1_Column  where  TB_CATEGORY_SPECS.CATEGORY_ID=##SUBCATEGORYAttribute1_Column.SUBCATID_L1 and TB_CATEGORY_SPECS.CATALOG_ID=@Catalog_ID and TB_CATEGORY_SPECS.ATTRIBUTE_ID=##SUBCATEGORYAttribute1_Column.ATTRIBUTE_ID
    END
    IF(@Get_AllSUBCategory_Spec_Count=0)
     BEGIN
	    Print'SUB3'
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute1_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute1_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute1_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute1_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute1_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute1_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute1_Column].Row#,'''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute1_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute1_Column].SUBCATID_L1= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute1_Column].SUBCATID_L1 IS NOT NULL AND [##SUBCATEGORYAttribute1_Column].ATTRIBUTE_DATATYPE LIKE ''Text%''  
    ')
     END
    IF(@Get_AllSUBCategory_Spec_Count<>0)
    BEGIN
	   Print'SUB4'
    DECLARE @MAXSUBATTRID VARCHAR(500)  
	SET @MAXATTRID = (select MAX(SORT_ORDER) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute1_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L1 and TCS.CATALOG_ID=@Catalog_ID)
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute1_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute1_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute1_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute1_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute1_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute1_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute1_Column].Row# '+''+@MAXSUBATTRID+','''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute1_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute1_Column].SUBCATID_L1= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute1_Column].SUBCATID_L1 IS NOT NULL AND [##SUBCATEGORYAttribute1_Column].ATTRIBUTE_DATATYPE LIKE ''Text%'' and [##SUBCATEGORYAttribute1_Column].Flag=''Insert''  ')
     EXEC('UPDATE TB_CATEGORY_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.STRING_VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
     else    convert(nvarchar(max),SIT.STRING_VALUE) End FROM [##SUBCATEGORYAttribute1_Column] SIT WHERE 
		SIT.ATTRIBUTE_ID=TB_CATEGORY_SPECS.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%''  AND  SIT.Flag=''Update''')
    END

	 Set @InsertCount_Details=@InsertCount_Details + (Select count(*) from ##SUBCATEGORYAttribute1_Column where FLAG='Insert')
     SET @UpdateCount_Details=@UpdateCount_Details + (Select count(*) from ##SUBCATEGORYAttribute1_Column where FLAG='Update')
    END
	
    END


------------------*********SUBCATEGORY LEVEL1 CATEGORY END****************------------------------------------------ 
  
  
------------------*********SUBCATEGORY LEVEL2 CATEGORY ****************------------------------------------------
   IF (@Get_Filtered_SubcategoryColumn_Names2 <>'' OR @Get_Filtered_SubcategoryColumn_Names2<>null and @Get_Filtered_AttributeSubcategoryColumn_Names2 <>'' OR @Get_Filtered_AttributeSubcategoryColumn_Names2<>null )
  BEGIN
 
  
  
  
  IF(OBJECT_ID('tempdb..##SUB_Category2') is not null) drop table ##SUB_Category2
  
  SET @Get_Filtered_SubcategoryColumn_Names2=RIGHT(@Get_Filtered_SubcategoryColumn_Names2,LEN(@Get_Filtered_SubcategoryColumn_Names2)-1)
  select @Get_Filtered_SubcategoryColumn_Names2
  SET @Get_Filtered_SubcategoryColumn_Names2 = REPLACE(@Get_Filtered_SubcategoryColumn_Names2, ',', '],[') 
  SET @Get_Filtered_SubcategoryColumn_Names2 = REPLACE(@Get_Filtered_SubcategoryColumn_Names2, ')', '])')
  SET @Get_Filtered_SubcategoryColumn_Names2 = REPLACE(@Get_Filtered_SubcategoryColumn_Names2, '(', '([')
  SET @Get_Filtered_SubcategoryColumn_Names2='[' + @Get_Filtered_SubcategoryColumn_Names2 +']'
  
  
  EXEC('SELECT '+ @Get_Filtered_SubcategoryColumn_Names2+ ' into ##SUB_Category2  FROM [##IMPORTTEMP'+ @SID +']') 
  
  
  SET @Get_Category_AttributeName=''
  select @Get_Category_AttributeName=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE(name,']',']]') + ']' FROM TEMPDB.sys.all_columns where object_id= OBJECT_ID('tempdb..##SUB_Category2') and name not like 'SUBCATID_L2' and  name not like 'SUBCATNAME_L2' and  name not like 'CATEGORY_PUBLISH2WEB' and  name not like 'CATEGORY_PUBLISH2PRINT' and  name not like 'CATEGORY_PUBLISH2PDF' and  name not like 'CATEGORY_PUBLISH2EXPORT' and  name not like 'CATEGORY_PUBLISH2PORTAL' FOR XML PATH('')),2,200000))
  SELECT @Get_Category_AttributeName = (SELECT REPLACE(@Get_Category_AttributeName, ',[],', ','))
  select @Get_Category_AttributeName
  
  
  IF(@Get_Category_AttributeName IS NOT NULL)
   BEGIN
   Print'SUB2'
    IF(OBJECT_ID('tempdb..##SubColumn_Names2') is not null) drop table ##SubColumn_Names2
    EXEC ('SELECT row_number() over(ORDER BY (Select 1)) as Row#,SUBCATID_L2,SUBCATNAME_L2,PA.ATTRIBUTE_NAME,PA.STRING_VALUE into ##SubColumn_Names2 FROM(SELECT SUBCATID_L2,SUBCATNAME_L2,ATTRIBUTE_NAME,STRING_VALUE FROM [##SUB_Category2] 
    UNPIVOT (        
    STRING_VALUE      
    FOR ATTRIBUTE_NAME IN ('+@Get_Category_AttributeName+') ) P)PA')
   
   
   if(OBJECT_ID('tempdb..##AttributeSub_Category2') is not null) drop table ##AttributeSub_Category2
  
   SET @Get_Filtered_AttributeSubcategoryColumn_Names2=RIGHT(@Get_Filtered_AttributeSubcategoryColumn_Names2,LEN(@Get_Filtered_AttributeSubcategoryColumn_Names2)-1)
   
   if(OBJECT_ID('tempdb..##AttributeSubColumn_Names2') is not null) drop table ##AttributeSubColumn_Names2
   CREATE table ##AttributeSubColumn_Names2
   (
   ATTRIBUTE_NAME varchar(500)
   )      
    
	Insert Into ##AttributeSubColumn_Names2 select @Get_Filtered_AttributeSubcategoryColumn_Names2
    
	if(OBJECT_ID('tempdb..##OriginalSubAttributeColumn2') is not null) drop table ##OriginalSubAttributeColumn2
    select item into ##OriginalSubAttributeColumn2 from ##AttributeSubColumn_Names2 CROSS APPLY SplitString(ATTRIBUTE_NAME, ',')
    
	EXEC('SELECT row_number() OVER(ORDER BY (SELECT 1))AS Row#,* into ##AttributeSub_Category2 FROM [##OriginalSubAttributeColumn2] where item NOT IN (''SUBCATID_L2'',''SUBCATNAME_L2'',''CATEGORY_PUBLISH2WEB'',''CATEGORY_PUBLISH2PRINT'',''CATEGORY_PUBLISH2PDF'',''CATEGORY_PUBLISH2EXPORT'',''CATEGORY_PUBLISH2PORTAL'') ')
    
	ALTER TABLE ##SubColumn_Names2 ADD FLAG Varchar(50)
    
	UPDATE ##SubColumn_Names2 SET ##SubColumn_Names2.ATTRIBUTE_NAME = ##AttributeSub_Category2.item, ##SubColumn_Names2.FLAG='Insert' FROM ##SubColumn_Names2, ##AttributeSub_Category2 
    WHERE ##SubColumn_Names2.Row# = ##AttributeSub_Category2.Row# 
   
    if(OBJECT_ID('tempdb..##SUBCATEGORYAttribute1_Column2') is not null) drop table ##SUBCATEGORYAttribute1_Column2
    
	select PA.ROW#,PA.SUBCATID_L2,PA.SUBCATNAME_L2,PA.ATTRIBUTE_NAME,PA.STRING_VALUE,TA.ATTRIBUTE_ID,TA.ATTRIBUTE_DATATYPE,TA.ATTRIBUTE_TYPE,PA.FLAG INTo ##SUBCATEGORYAttribute1_Column2 from ##SubColumn_Names2 PA join TB_ATTRIBUTE TA ON PA.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME 
    DECLARE @Get_SUBCATEGORY_SPECS_COUNT2 INT=0
    Declare @Get_AllSUBCategory_Spec_Count2 INT=0
   
    SET @Get_AllSUBCategory_Spec_Count2=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute1_Column2 SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L2 and TCS.CATALOG_ID=@Catalog_ID)
    SET @Get_SUBCATEGORY_SPECS_COUNT2=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute1_Column2 SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L2 and TCS.CATALOG_ID=@Catalog_ID and TCS.ATTRIBUTE_ID=SCN.ATTRIBUTE_ID)
   
    IF(@Get_SUBCATEGORY_SPECS_COUNT2<>0)
    BEGIN
     UPDATE ##SUBCATEGORYAttribute1_Column2 SET ##SUBCATEGORYAttribute1_Column2.Flag='Update',##SUBCATEGORYAttribute1_Column2.Row#=TB_CATEGORY_SPECS.SORT_ORDER from TB_CATEGORY_SPECS ,##SUBCATEGORYAttribute1_Column2  where  TB_CATEGORY_SPECS.CATEGORY_ID=##SUBCATEGORYAttribute1_Column2.SUBCATID_L2 and TB_CATEGORY_SPECS.CATALOG_ID=@Catalog_ID and TB_CATEGORY_SPECS.ATTRIBUTE_ID=##SUBCATEGORYAttribute1_Column2.ATTRIBUTE_ID
    END
    IF(@Get_AllSUBCategory_Spec_Count2=0)
     BEGIN
	    Print'SUB3'
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute1_Column2].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute1_Column2].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute1_Column2].Row#,'''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute1_Column2] join TB_CATEGORY  on [##SUBCATEGORYAttribute1_Column2].SUBCATID_L2= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute1_Column2].SUBCATID_L2 IS NOT NULL AND [##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_DATATYPE LIKE ''Text%''  
    ')
     END
    IF(@Get_AllSUBCategory_Spec_Count2<>0)
    BEGIN
	   Print'SUB4'
	   print '2222'
    DECLARE @MAXSUBATTRID2 VARCHAR(500)  
	SET @MAXSUBATTRID2 = (select MAX(SORT_ORDER) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute1_Column2 SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L2 and TCS.CATALOG_ID=@Catalog_ID)
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute1_Column2].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute1_Column2].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute1_Column2].Row# '+''''+@MAXSUBATTRID2+''','''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute1_Column2] join TB_CATEGORY  on [##SUBCATEGORYAttribute1_Column2].SUBCATID_L2= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute1_Column2].SUBCATID_L2 IS NOT NULL AND [##SUBCATEGORYAttribute1_Column2].ATTRIBUTE_DATATYPE LIKE ''Text%'' and [##SUBCATEGORYAttribute1_Column2].Flag=''Insert''  ')
     EXEC('UPDATE TB_CATEGORY_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.STRING_VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
     else    convert(nvarchar(max),SIT.STRING_VALUE) End FROM [##SUBCATEGORYAttribute1_Column2] SIT WHERE 
		SIT.ATTRIBUTE_ID=TB_CATEGORY_SPECS.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%''  AND  SIT.Flag=''Update''')
    END
	Set @InsertCount_Details=@InsertCount_Details + (Select count(*) from ##SUBCATEGORYAttribute1_Column2 where FLAG='Insert')
    SET @UpdateCount_Details=@UpdateCount_Details + (Select count(*) from ##SUBCATEGORYAttribute1_Column2 where FLAG='Update')
    END
	
    END


------------------*********SUBCATEGORY LEVEL2 CATEGORY END****************------------------------------------------ 
  
  
  ------------------*********SUBCATEGORY LEVEL3 CATEGORY ****************------------------------------------------
   IF (@Get_Filtered_SubcategoryColumn_Names3 <>'' OR @Get_Filtered_SubcategoryColumn_Names3<>null and @Get_Filtered_AttributeSubcategoryColumn_Names3 <>'' OR @Get_Filtered_AttributeSubcategoryColumn_Names3<>null )
  BEGIN
   Print'SUB1'
  IF(OBJECT_ID('tempdb..##SUB_Category3') is not null) drop table ##SUB_Category3
  SET @Get_Filtered_SubcategoryColumn_Names3=RIGHT(@Get_Filtered_SubcategoryColumn_Names3,LEN(@Get_Filtered_SubcategoryColumn_Names3)-1)
  select @Get_Filtered_SubcategoryColumn_Names3
    SET @Get_Filtered_SubcategoryColumn_Names3 = REPLACE(@Get_Filtered_SubcategoryColumn_Names3, ',', '],[') 
  SET @Get_Filtered_SubcategoryColumn_Names3 = REPLACE(@Get_Filtered_SubcategoryColumn_Names3, ')', '])')
  SET @Get_Filtered_SubcategoryColumn_Names3 = REPLACE(@Get_Filtered_SubcategoryColumn_Names3, '(', '([')
  SET @Get_Filtered_SubcategoryColumn_Names3='[' + @Get_Filtered_SubcategoryColumn_Names3 +']'
  EXEC('SELECT '+ @Get_Filtered_SubcategoryColumn_Names3+ ' into ##SUB_Category3  FROM [##IMPORTTEMP'+ @SID +']') 
  SET @Get_Category_AttributeName=''
  select @Get_Category_AttributeName=(SELECT SUBSTRING((SELECT ',['+ REPLACE(name,']',']]') + ']' FROM TEMPDB.sys.all_columns where object_id= OBJECT_ID('tempdb..##SUB_Category3') and name not like 'SUBCATID_L3' and  name not like 'SUBCATNAME_L3' and  name not like 'CATEGORY_PUBLISH2WEB' and  name not like 'CATEGORY_PUBLISH2PRINT' and  name not like 'CATEGORY_PUBLISH2PDF' and  name not like 'CATEGORY_PUBLISH2EXPORT' and  name not like 'CATEGORY_PUBLISH2PORTAL' FOR XML PATH('')),2,200000))
  print '555'
   print @Get_Category_AttributeName
  SELECT @Get_Category_AttributeName = (SELECT REPLACE(@Get_Category_AttributeName, ',[],', ','))
  select @Get_Category_AttributeName
  print @Get_Category_AttributeName
  IF(@Get_Category_AttributeName IS NOT NULL)
   BEGIN
   Print'SUB3'
   print'111111'
    IF(OBJECT_ID('tempdb..##SubLevel3Column_Names') is not null) drop table ##SubLevel3Column_Names
    EXEC ('SELECT row_number() over(ORDER BY (Select 1)) as Row#,SUBCATID_L3,SUBCATNAME_L3,PA.ATTRIBUTE_NAME,PA.STRING_VALUE into ##SubLevel3Column_Names FROM(SELECT SUBCATID_L3,SUBCATNAME_L3,ATTRIBUTE_NAME,STRING_VALUE FROM [##SUB_Category3] 
    UNPIVOT (        
    STRING_VALUE      
    FOR ATTRIBUTE_NAME IN ('+@Get_Category_AttributeName+') ) P)PA')
   if(OBJECT_ID('tempdb..##AttributeSubLevel3_Category') is not null) drop table ##AttributeSubLevel3_Category
   SET @Get_Filtered_AttributeSubcategoryColumn_Names3=RIGHT(@Get_Filtered_AttributeSubcategoryColumn_Names3,LEN(@Get_Filtered_AttributeSubcategoryColumn_Names3)-1)
   if(OBJECT_ID('tempdb..##AttributeSubLevel3Column_Names') is not null) drop table ##AttributeSubLevel3Column_Names
   CREATE table ##AttributeSubLevel3Column_Names
   (
   ATTRIBUTE_NAME varchar(500)
   )      
    Insert Into ##AttributeSubLevel3Column_Names select @Get_Filtered_AttributeSubcategoryColumn_Names3 
    if(OBJECT_ID('tempdb..##OriginalSubLevel3AttributeColumn') is not null) drop table ##OriginalSubLevel3AttributeColumn
    select item into ##OriginalSubLevel3AttributeColumn from ##AttributeSubLevel3Column_Names CROSS APPLY SplitString(ATTRIBUTE_NAME, ',')
    EXEC('SELECT row_number() OVER(ORDER BY (SELECT 1))AS Row#,* into ##AttributeSubLevel3_Category FROM [##OriginalSubLevel3AttributeColumn] where item NOT IN (''SUBCATID_L3'',''SUBCATNAME_L3'',''CATEGORY_PUBLISH2WEB'',''CATEGORY_PUBLISH2PRINT'',''CATEGORY_PUBLISH2PDF'',''CATEGORY_PUBLISH2EXPORT'',''CATEGORY_PUBLISH2PORTAL'') ')
    ALTER TABLE ##SubLevel3Column_Names ADD FLAG Varchar(50)
    UPDATE ##SubLevel3Column_Names SET ##SubLevel3Column_Names.ATTRIBUTE_NAME = ##AttributeSubLevel3_Category.item, ##SubLevel3Column_Names.FLAG='Insert' FROM ##SubLevel3Column_Names, ##AttributeSubLevel3_Category
    WHERE ##SubLevel3Column_Names.Row# = ##AttributeSubLevel3_Category.Row# 
    if(OBJECT_ID('tempdb..##SUBCATEGORYAttribute3_Column') is not null) drop table ##SUBCATEGORYAttribute3_Column
    select PA.ROW#,PA.SUBCATID_L3,PA.SUBCATNAME_L3,PA.ATTRIBUTE_NAME,PA.STRING_VALUE,TA.ATTRIBUTE_ID,TA.ATTRIBUTE_DATATYPE,TA.ATTRIBUTE_TYPE,PA.FLAG INTo ##SUBCATEGORYAttribute3_Column from ##SubLevel3Column_Names PA join TB_ATTRIBUTE TA ON PA.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME 
    DECLARE @Get_SUBCATEGORY3_SPECS_COUNT INT=0
    Declare @Get_AllSUBCategory3_Spec_Count INT=0
    SET @Get_AllSUBCategory3_Spec_Count=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute3_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L3 and TCS.CATALOG_ID=@Catalog_ID)
    SET @Get_SUBCATEGORY3_SPECS_COUNT=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute3_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L3 and TCS.CATALOG_ID=@Catalog_ID and TCS.ATTRIBUTE_ID=SCN.ATTRIBUTE_ID)
    IF(@Get_SUBCATEGORY3_SPECS_COUNT<>0)
    BEGIN
     UPDATE ##SUBCATEGORYAttribute3_Column SET ##SUBCATEGORYAttribute3_Column.Flag='Update',##SUBCATEGORYAttribute3_Column.Row#=TB_CATEGORY_SPECS.SORT_ORDER from TB_CATEGORY_SPECS ,##SUBCATEGORYAttribute3_Column  where  TB_CATEGORY_SPECS.CATEGORY_ID=##SUBCATEGORYAttribute3_Column.SUBCATID_L3 and TB_CATEGORY_SPECS.CATALOG_ID=@Catalog_ID and TB_CATEGORY_SPECS.ATTRIBUTE_ID=##SUBCATEGORYAttribute3_Column.ATTRIBUTE_ID
    END
    IF(@Get_AllSUBCategory3_Spec_Count=0)
     BEGIN
	    Print'SUB3'
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute3_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute3_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute3_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute3_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute3_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute3_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute3_Column].Row#,'''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute3_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute3_Column].SUBCATID_L3= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute3_Column].SUBCATID_L3 IS NOT NULL AND [##SUBCATEGORYAttribute3_Column].ATTRIBUTE_DATATYPE LIKE ''Text%''  
    ')
     END
    IF(@Get_AllSUBCategory3_Spec_Count<>0)
    BEGIN
	   Print'SUB4'
    DECLARE @MAXSUB3ATTRID VARCHAR(500)  
	SET @MAXSUB3ATTRID = (select MAX(SORT_ORDER) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute3_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L3 and TCS.CATALOG_ID=@Catalog_ID)
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute3_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute3_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute3_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute3_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute3_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute3_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute3_Column].Row# '+''''+@MAXSUB3ATTRID+''','''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute3_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute3_Column].SUBCATID_L3= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute3_Column].SUBCATID_L3 IS NOT NULL AND [##SUBCATEGORYAttribute3_Column].ATTRIBUTE_DATATYPE LIKE ''Text%'' and [##SUBCATEGORYAttribute3_Column].Flag=''Insert''  ')
     EXEC('UPDATE TB_CATEGORY_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.STRING_VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
     else    convert(nvarchar(max),SIT.STRING_VALUE) End FROM [##SUBCATEGORYAttribute3_Column] SIT WHERE 
		SIT.ATTRIBUTE_ID=TB_CATEGORY_SPECS.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%''  AND  SIT.Flag=''Update''')
    END
	Set @InsertCount_Details=@InsertCount_Details + (Select count(*) from ##SUBCATEGORYAttribute3_Column where FLAG='Insert')
    SET @UpdateCount_Details=@UpdateCount_Details + (Select count(*) from ##SUBCATEGORYAttribute3_Column where FLAG='Update')
    END
		
    END



------------------*********SUBCATEGORY LEVEL3 CATEGORY END****************------------------------------------------ 

--------------------*********SUBCATEGORY LEVEL4 CATEGORY ****************------------------------------------------
 IF (@Get_Filtered_SubcategoryColumn_Names4 <>'' OR @Get_Filtered_SubcategoryColumn_Names4<>null and @Get_Filtered_AttributeSubcategoryColumn_Names4 <>'' OR @Get_Filtered_AttributeSubcategoryColumn_Names4<>null )
  BEGIN
   Print'SUB1'
  IF(OBJECT_ID('tempdb..##SUB_Category4') is not null) drop table ##SUB_Category4
  SET @Get_Filtered_SubcategoryColumn_Names4=RIGHT(@Get_Filtered_SubcategoryColumn_Names4,LEN(@Get_Filtered_SubcategoryColumn_Names4)-1)
  select @Get_Filtered_SubcategoryColumn_Names4
    SET @Get_Filtered_SubcategoryColumn_Names4 = REPLACE(@Get_Filtered_SubcategoryColumn_Names4, ',', '],[') 
  SET @Get_Filtered_SubcategoryColumn_Names4 = REPLACE(@Get_Filtered_SubcategoryColumn_Names4, ')', '])')
  SET @Get_Filtered_SubcategoryColumn_Names4 = REPLACE(@Get_Filtered_SubcategoryColumn_Names4, '(', '([')
  SET @Get_Filtered_SubcategoryColumn_Names4='[' + @Get_Filtered_SubcategoryColumn_Names4 +']'
  EXEC('SELECT '+ @Get_Filtered_SubcategoryColumn_Names4+ ' into ##SUB_Category4  FROM [##IMPORTTEMP'+ @SID +']') 
  SET @Get_Category_AttributeName=''
  select @Get_Category_AttributeName=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE(name,']',']]') + ']' FROM TEMPDB.sys.all_columns where object_id= OBJECT_ID('tempdb..##SUB_Category4') and name not like 'SUBCATID_L4' and  name not like 'SUBCATNAME_L4' and  name not like 'CATEGORY_PUBLISH2WEB' and  name not like 'CATEGORY_PUBLISH2PRINT' and  name not like 'CATEGORY_PUBLISH2PDF' and  name not like 'CATEGORY_PUBLISH2EXPORT' and  name not like 'CATEGORY_PUBLISH2PORTAL' FOR XML PATH('')),2,200000))
  SELECT @Get_Category_AttributeName = (SELECT REPLACE(@Get_Category_AttributeName, ',[],', ','))
  select @Get_Category_AttributeName
  IF(@Get_Category_AttributeName IS NOT NULL)
   BEGIN
   Print'SUB4'
    IF(OBJECT_ID('tempdb..##SubLevel4Column_Names') is not null) drop table ##SubLevel4Column_Names
    EXEC ('SELECT row_number() over(ORDER BY (Select 1)) as Row#,SUBCATID_L4,SUBCATNAME_L4,PA.ATTRIBUTE_NAME,PA.STRING_VALUE into ##SubLevel4Column_Names FROM(SELECT SUBCATID_L4,SUBCATNAME_L4,ATTRIBUTE_NAME,STRING_VALUE FROM [##SUB_Category4] 
    UNPIVOT (        
    STRING_VALUE      
    FOR ATTRIBUTE_NAME IN ('+@Get_Category_AttributeName+') ) P)PA')
   if(OBJECT_ID('tempdb..##AttributeSubLevel4_Category') is not null) drop table ##AttributeSubLevel4_Category
   SET @Get_Filtered_AttributeSubcategoryColumn_Names4=RIGHT(@Get_Filtered_AttributeSubcategoryColumn_Names4,LEN(@Get_Filtered_AttributeSubcategoryColumn_Names4)-1)
   if(OBJECT_ID('tempdb..##AttributeSubLevel4Column_Names') is not null) drop table ##AttributeSubLevel4Column_Names
   CREATE table ##AttributeSubLevel4Column_Names
   (
   ATTRIBUTE_NAME varchar(500)
   )      
    Insert Into ##AttributeSubLevel4Column_Names select @Get_Filtered_AttributeSubcategoryColumn_Names4 
    if(OBJECT_ID('tempdb..##OriginalSubLevel4AttributeColumn') is not null) drop table ##OriginalSubLevel4AttributeColumn
    select item into ##OriginalSubLevel4AttributeColumn from ##AttributeSubLevel4Column_Names CROSS APPLY SplitString(ATTRIBUTE_NAME, ',')
    EXEC('SELECT row_number() OVER(ORDER BY (SELECT 1))AS Row#,* into ##AttributeSubLevel4_Category FROM [##OriginalSubLevel4AttributeColumn] where item NOT IN (''SUBCATID_L4'',''SUBCATNAME_L4'',''CATEGORY_PUBLISH2WEB'',''CATEGORY_PUBLISH2PRINT'',''CATEGORY_PUBLISH2PDF'',''CATEGORY_PUBLISH2EXPORT'',''CATEGORY_PUBLISH2PORTAL'') ')
    ALTER TABLE ##SubLevel4Column_Names ADD FLAG Varchar(50)
    UPDATE ##SubLevel4Column_Names SET ##SubLevel4Column_Names.ATTRIBUTE_NAME = ##AttributeSubLevel4_Category.item, ##SubLevel4Column_Names.FLAG='Insert' FROM ##SubLevel4Column_Names, ##AttributeSubLevel4_Category
    WHERE ##SubLevel4Column_Names.Row# = ##AttributeSubLevel4_Category.Row# 
    if(OBJECT_ID('tempdb..##SUBCATEGORYAttribute4_Column') is not null) drop table ##SUBCATEGORYAttribute4_Column
    select PA.ROW#,PA.SUBCATID_L4,PA.SUBCATNAME_L4,PA.ATTRIBUTE_NAME,PA.STRING_VALUE,TA.ATTRIBUTE_ID,TA.ATTRIBUTE_DATATYPE,TA.ATTRIBUTE_TYPE,PA.FLAG INTo ##SUBCATEGORYAttribute4_Column from ##SubLevel4Column_Names PA join TB_ATTRIBUTE TA ON PA.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME 
    DECLARE @Get_SUBCATEGORY4_SPECS_COUNT INT=0
    Declare @Get_AllSUBCategory4_Spec_Count INT=0
    SET @Get_AllSUBCategory4_Spec_Count=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute4_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L4 and TCS.CATALOG_ID=@Catalog_ID)
    SET @Get_SUBCATEGORY4_SPECS_COUNT=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute4_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L4 and TCS.CATALOG_ID=@Catalog_ID and TCS.ATTRIBUTE_ID=SCN.ATTRIBUTE_ID)
    IF(@Get_SUBCATEGORY4_SPECS_COUNT<>0)
    BEGIN
     UPDATE ##SUBCATEGORYAttribute4_Column SET ##SUBCATEGORYAttribute4_Column.Flag='Update',##SUBCATEGORYAttribute4_Column.Row#=TB_CATEGORY_SPECS.SORT_ORDER from TB_CATEGORY_SPECS ,##SUBCATEGORYAttribute4_Column  where  TB_CATEGORY_SPECS.CATEGORY_ID=##SUBCATEGORYAttribute4_Column.SUBCATID_L4 and TB_CATEGORY_SPECS.CATALOG_ID=@Catalog_ID and TB_CATEGORY_SPECS.ATTRIBUTE_ID=##SUBCATEGORYAttribute4_Column.ATTRIBUTE_ID
    END
    IF(@Get_AllSUBCategory4_Spec_Count=0)
     BEGIN
	    Print'SUB4'
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute4_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute4_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute4_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute4_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute4_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute4_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute4_Column].Row#,'''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute4_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute4_Column].SUBCATID_L4= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute4_Column].SUBCATID_L4 IS NOT NULL AND [##SUBCATEGORYAttribute4_Column].ATTRIBUTE_DATATYPE LIKE ''Text%''  
    ')
     END
    IF(@Get_AllSUBCategory4_Spec_Count<>0)
    BEGIN
	   Print'SUB4'
    DECLARE @MAXSUB4ATTRID VARCHAR(500)  
	SET @MAXSUB4ATTRID = (select MAX(SORT_ORDER) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute4_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L4 and TCS.CATALOG_ID=@Catalog_ID)
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute4_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute4_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute4_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute4_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute4_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute4_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute4_Column].Row# '+''''+@MAXSUB4ATTRID+''','''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute4_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute4_Column].SUBCATID_L4= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute4_Column].SUBCATID_L4 IS NOT NULL AND [##SUBCATEGORYAttribute4_Column].ATTRIBUTE_DATATYPE LIKE ''Text%'' and [##SUBCATEGORYAttribute4_Column].Flag=''Insert''  ')
     EXEC('UPDATE TB_CATEGORY_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.STRING_VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
     else    convert(nvarchar(max),SIT.STRING_VALUE) End FROM [##SUBCATEGORYAttribute4_Column] SIT WHERE 
		SIT.ATTRIBUTE_ID=TB_CATEGORY_SPECS.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%''  AND  SIT.Flag=''Update''')
    END
	Set @InsertCount_Details=@InsertCount_Details + (Select count(*) from ##SUBCATEGORYAttribute4_Column where FLAG='Insert')
    SET @UpdateCount_Details=@UpdateCount_Details + (Select count(*) from ##SUBCATEGORYAttribute4_Column where FLAG='Update')
    END
		
    END



--------------------*********SUBCATEGORY LEVEL4 CATEGORY END****************------------------------------------------ 

--------------------*********SUBCATEGORY LEVEL5 CATEGORY ****************------------------------------------------
 IF (@Get_Filtered_SubcategoryColumn_Names5 <>'' OR @Get_Filtered_SubcategoryColumn_Names5<>null and @Get_Filtered_AttributeSubcategoryColumn_Names5 <>'' OR @Get_Filtered_AttributeSubcategoryColumn_Names5<>null )
  BEGIN
   Print'SUB1'
  IF(OBJECT_ID('tempdb..##SUB_Category5') is not null) drop table ##SUB_Category5
  SET @Get_Filtered_SubcategoryColumn_Names5=RIGHT(@Get_Filtered_SubcategoryColumn_Names5,LEN(@Get_Filtered_SubcategoryColumn_Names5)-1)
  select @Get_Filtered_SubcategoryColumn_Names5
    SET @Get_Filtered_SubcategoryColumn_Names5 = REPLACE(@Get_Filtered_SubcategoryColumn_Names5, ',', '],[') 
  SET @Get_Filtered_SubcategoryColumn_Names5 = REPLACE(@Get_Filtered_SubcategoryColumn_Names5, ')', '])')
  SET @Get_Filtered_SubcategoryColumn_Names5 = REPLACE(@Get_Filtered_SubcategoryColumn_Names5, '(', '([')
  SET @Get_Filtered_SubcategoryColumn_Names5='[' + @Get_Filtered_SubcategoryColumn_Names5 +']'
  EXEC('SELECT '+ @Get_Filtered_SubcategoryColumn_Names5+ ' into ##SUB_Category5  FROM [##IMPORTTEMP'+ @SID +']') 
  SET @Get_Category_AttributeName=''
  select @Get_Category_AttributeName=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE(name,']',']]') + ']' FROM TEMPDB.sys.all_columns where object_id= OBJECT_ID('tempdb..##SUB_Category5') and name not like 'SUBCATID_L5' and  name not like 'SUBCATNAME_L5' and  name not like 'CATEGORY_PUBLISH2WEB' and  name not like 'CATEGORY_PUBLISH2PRINT' and  name not like 'CATEGORY_PUBLISH2PDF' and  name not like 'CATEGORY_PUBLISH2EXPORT' and  name not like 'CATEGORY_PUBLISH2PORTAL' FOR XML PATH('')),2,200000))
  SELECT @Get_Category_AttributeName = (SELECT REPLACE(@Get_Category_AttributeName, ',[],', ','))
  select @Get_Category_AttributeName
  IF(@Get_Category_AttributeName IS NOT NULL)
   BEGIN
   Print'SUB5'
    IF(OBJECT_ID('tempdb..##SubLevel5Column_Names') is not null) drop table ##SubLevel5Column_Names
    EXEC ('SELECT row_number() over(ORDER BY (Select 1)) as Row#,SUBCATID_L5,SUBCATNAME_L5,PA.ATTRIBUTE_NAME,PA.STRING_VALUE into ##SubLevel5Column_Names FROM(SELECT SUBCATID_L5,SUBCATNAME_L5,ATTRIBUTE_NAME,STRING_VALUE FROM [##SUB_Category5] 
    UNPIVOT (        
    STRING_VALUE      
    FOR ATTRIBUTE_NAME IN ('+@Get_Category_AttributeName+') ) P)PA')
   if(OBJECT_ID('tempdb..##AttributeSubLevel5_Category') is not null) drop table ##AttributeSubLevel5_Category
   SET @Get_Filtered_AttributeSubcategoryColumn_Names5=RIGHT(@Get_Filtered_AttributeSubcategoryColumn_Names5,LEN(@Get_Filtered_AttributeSubcategoryColumn_Names5)-1)
   if(OBJECT_ID('tempdb..##AttributeSubLevel5Column_Names') is not null) drop table ##AttributeSubLevel5Column_Names
   CREATE table ##AttributeSubLevel5Column_Names
   (
   ATTRIBUTE_NAME varchar(500)
   )      
    Insert Into ##AttributeSubLevel5Column_Names select @Get_Filtered_AttributeSubcategoryColumn_Names5 
    if(OBJECT_ID('tempdb..##OriginalSubLevel5AttributeColumn') is not null) drop table ##OriginalSubLevel5AttributeColumn
    select item into ##OriginalSubLevel5AttributeColumn from ##AttributeSubLevel5Column_Names CROSS APPLY SplitString(ATTRIBUTE_NAME, ',')
    EXEC('SELECT row_number() OVER(ORDER BY (SELECT 1))AS Row#,* into ##AttributeSubLevel5_Category FROM [##OriginalSubLevel5AttributeColumn] where item NOT IN (''SUBCATID_L5'',''SUBCATNAME_L5'',''CATEGORY_PUBLISH2WEB'',''CATEGORY_PUBLISH2PRINT'',''CATEGORY_PUBLISH2PDF'',''CATEGORY_PUBLISH2EXPORT'',''CATEGORY_PUBLISH2PORTAL'') ')
    ALTER TABLE ##SubLevel5Column_Names ADD FLAG Varchar(50)
    UPDATE ##SubLevel5Column_Names SET ##SubLevel5Column_Names.ATTRIBUTE_NAME = ##AttributeSubLevel5_Category.item, ##SubLevel5Column_Names.FLAG='Insert' FROM ##SubLevel5Column_Names, ##AttributeSubLevel5_Category
    WHERE ##SubLevel5Column_Names.Row# = ##AttributeSubLevel5_Category.Row# 
    if(OBJECT_ID('tempdb..##SUBCATEGORYAttribute5_Column') is not null) drop table ##SUBCATEGORYAttribute5_Column
    select PA.ROW#,PA.SUBCATID_L5,PA.SUBCATNAME_L5,PA.ATTRIBUTE_NAME,PA.STRING_VALUE,TA.ATTRIBUTE_ID,TA.ATTRIBUTE_DATATYPE,TA.ATTRIBUTE_TYPE,PA.FLAG INTo ##SUBCATEGORYAttribute5_Column from ##SubLevel5Column_Names PA join TB_ATTRIBUTE TA ON PA.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME 
    DECLARE @Get_SUBCATEGORY5_SPECS_COUNT INT=0
    Declare @Get_AllSUBCategory5_Spec_Count INT=0
    SET @Get_AllSUBCategory5_Spec_Count=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute5_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L5 and TCS.CATALOG_ID=@Catalog_ID)
    SET @Get_SUBCATEGORY5_SPECS_COUNT=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute5_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L5 and TCS.CATALOG_ID=@Catalog_ID and TCS.ATTRIBUTE_ID=SCN.ATTRIBUTE_ID)
    IF(@Get_SUBCATEGORY5_SPECS_COUNT<>0)
    BEGIN
     UPDATE ##SUBCATEGORYAttribute5_Column SET ##SUBCATEGORYAttribute5_Column.Flag='Update',##SUBCATEGORYAttribute5_Column.Row#=TB_CATEGORY_SPECS.SORT_ORDER from TB_CATEGORY_SPECS ,##SUBCATEGORYAttribute5_Column  where  TB_CATEGORY_SPECS.CATEGORY_ID=##SUBCATEGORYAttribute5_Column.SUBCATID_L5 and TB_CATEGORY_SPECS.CATALOG_ID=@Catalog_ID and TB_CATEGORY_SPECS.ATTRIBUTE_ID=##SUBCATEGORYAttribute5_Column.ATTRIBUTE_ID
    END
    IF(@Get_AllSUBCategory5_Spec_Count=0)
     BEGIN
	    Print'SUB5'
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute5_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute5_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute5_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute5_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute5_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute5_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute5_Column].Row#,'''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute5_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute5_Column].SUBCATID_L5= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute5_Column].SUBCATID_L5 IS NOT NULL AND [##SUBCATEGORYAttribute5_Column].ATTRIBUTE_DATATYPE LIKE ''Text%''  
    ')
     END
    IF(@Get_AllSUBCategory5_Spec_Count<>0)
    BEGIN
	   Print'SUB5'
    DECLARE @MAXSUB5ATTRID VARCHAR(500)  
	SET @MAXSUB5ATTRID = (select MAX(SORT_ORDER) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute5_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L5 and TCS.CATALOG_ID=@Catalog_ID)
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute5_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute5_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute5_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute5_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute5_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute5_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute5_Column].Row# '+''''+@MAXSUB5ATTRID+''','''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute5_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute5_Column].SUBCATID_L5= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute5_Column].SUBCATID_L5 IS NOT NULL AND [##SUBCATEGORYAttribute5_Column].ATTRIBUTE_DATATYPE LIKE ''Text%'' and [##SUBCATEGORYAttribute5_Column].Flag=''Insert''  ')
     EXEC('UPDATE TB_CATEGORY_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.STRING_VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
     else    convert(nvarchar(max),SIT.STRING_VALUE) End FROM [##SUBCATEGORYAttribute5_Column] SIT WHERE 
		SIT.ATTRIBUTE_ID=TB_CATEGORY_SPECS.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%''  AND  SIT.Flag=''Update''')
    END
	Set @InsertCount_Details=@InsertCount_Details + (Select count(*) from ##SUBCATEGORYAttribute5_Column where FLAG='Insert')
    SET @UpdateCount_Details=@UpdateCount_Details + (Select count(*) from ##SUBCATEGORYAttribute5_Column where FLAG='Update')
    END
	
    END



--------------------*********SUBCATEGORY LEVEL5 CATEGORY END****************------------------------------------------ 

------------------*********SUBCATEGORY LEVEL6 CATEGORY ****************------------------------------------------
    IF (@Get_Filtered_SubcategoryColumn_Names6 <>'' OR @Get_Filtered_SubcategoryColumn_Names6<>null and @Get_Filtered_AttributeSubcategoryColumn_Names6 <>'' OR @Get_Filtered_AttributeSubcategoryColumn_Names6<>null )
  BEGIN
   Print'SUB1'
  IF(OBJECT_ID('tempdb..##SUB_Category6') is not null) drop table ##SUB_Category6
  SET @Get_Filtered_SubcategoryColumn_Names6=RIGHT(@Get_Filtered_SubcategoryColumn_Names6,LEN(@Get_Filtered_SubcategoryColumn_Names6)-1)
  select @Get_Filtered_SubcategoryColumn_Names6
    SET @Get_Filtered_SubcategoryColumn_Names6 = REPLACE(@Get_Filtered_SubcategoryColumn_Names6, ',', '],[') 
  SET @Get_Filtered_SubcategoryColumn_Names6 = REPLACE(@Get_Filtered_SubcategoryColumn_Names6, ')', '])')
  SET @Get_Filtered_SubcategoryColumn_Names6 = REPLACE(@Get_Filtered_SubcategoryColumn_Names6, '(', '([')
  SET @Get_Filtered_SubcategoryColumn_Names6='[' + @Get_Filtered_SubcategoryColumn_Names6 +']'
  EXEC('SELECT '+ @Get_Filtered_SubcategoryColumn_Names6+ ' into ##SUB_Category6  FROM [##IMPORTTEMP'+ @SID +']') 
  SET @Get_Category_AttributeName=''
  select @Get_Category_AttributeName=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE(name,']',']]') + ']' FROM TEMPDB.sys.all_columns where object_id= OBJECT_ID('tempdb..##SUB_Category6') and name not like 'SUBCATID_L6' and  name not like 'SUBCATNAME_L6' and  name not like 'CATEGORY_PUBLISH2WEB' and  name not like 'CATEGORY_PUBLISH2PRINT' and  name not like 'CATEGORY_PUBLISH2PDF' and  name not like 'CATEGORY_PUBLISH2EXPORT' and  name not like 'CATEGORY_PUBLISH2PORTAL' FOR XML PATH('')),2,200000))
  SELECT @Get_Category_AttributeName = (SELECT REPLACE(@Get_Category_AttributeName, ',[],', ','))
  select @Get_Category_AttributeName
  IF(@Get_Category_AttributeName IS NOT NULL)
   BEGIN
   Print'SUB6'
    IF(OBJECT_ID('tempdb..##SubLevel6Column_Names') is not null) drop table ##SubLevel6Column_Names
    EXEC ('SELECT row_number() over(ORDER BY (Select 1)) as Row#,SUBCATID_L6,SUBCATNAME_L6,PA.ATTRIBUTE_NAME,PA.STRING_VALUE into ##SubLevel6Column_Names FROM(SELECT SUBCATID_L6,SUBCATNAME_L6,ATTRIBUTE_NAME,STRING_VALUE FROM [##SUB_Category6] 
    UNPIVOT (        
    STRING_VALUE      
    FOR ATTRIBUTE_NAME IN ('+@Get_Category_AttributeName+') ) P)PA')
   if(OBJECT_ID('tempdb..##AttributeSubLevel6_Category') is not null) drop table ##AttributeSubLevel6_Category
   SET @Get_Filtered_AttributeSubcategoryColumn_Names6=RIGHT(@Get_Filtered_AttributeSubcategoryColumn_Names6,LEN(@Get_Filtered_AttributeSubcategoryColumn_Names6)-1)
   if(OBJECT_ID('tempdb..##AttributeSubLevel6Column_Names') is not null) drop table ##AttributeSubLevel6Column_Names
   CREATE table ##AttributeSubLevel6Column_Names
   (
   ATTRIBUTE_NAME varchar(600)
   )      
    Insert Into ##AttributeSubLevel6Column_Names select @Get_Filtered_AttributeSubcategoryColumn_Names6 
    if(OBJECT_ID('tempdb..##OriginalSubLevel6AttributeColumn') is not null) drop table ##OriginalSubLevel6AttributeColumn
    select item into ##OriginalSubLevel6AttributeColumn from ##AttributeSubLevel6Column_Names CROSS APPLY SplitString(ATTRIBUTE_NAME, ',')
    EXEC('SELECT row_number() OVER(ORDER BY (SELECT 1))AS Row#,* into ##AttributeSubLevel6_Category FROM [##OriginalSubLevel6AttributeColumn] where item NOT IN (''SUBCATID_L6'',''SUBCATNAME_L6'',''CATEGORY_PUBLISH2WEB'',''CATEGORY_PUBLISH2PRINT'',''CATEGORY_PUBLISH2PDF'',''CATEGORY_PUBLISH2EXPORT'',''CATEGORY_PUBLISH2PORTAL'') ')
    ALTER TABLE ##SubLevel6Column_Names ADD FLAG Varchar(60)
    UPDATE ##SubLevel6Column_Names SET ##SubLevel6Column_Names.ATTRIBUTE_NAME = ##AttributeSubLevel6_Category.item, ##SubLevel6Column_Names.FLAG='Insert' FROM ##SubLevel6Column_Names, ##AttributeSubLevel6_Category
    WHERE ##SubLevel6Column_Names.Row# = ##AttributeSubLevel6_Category.Row# 
    if(OBJECT_ID('tempdb..##SUBCATEGORYAttribute6_Column') is not null) drop table ##SUBCATEGORYAttribute6_Column
    select PA.ROW#,PA.SUBCATID_L6,PA.SUBCATNAME_L6,PA.ATTRIBUTE_NAME,PA.STRING_VALUE,TA.ATTRIBUTE_ID,TA.ATTRIBUTE_DATATYPE,TA.ATTRIBUTE_TYPE,PA.FLAG INTo ##SUBCATEGORYAttribute6_Column from ##SubLevel6Column_Names PA join TB_ATTRIBUTE TA ON PA.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME 
    DECLARE @Get_SUBCATEGORY6_SPECS_COUNT INT=0
    Declare @Get_AllSUBCategory6_Spec_Count INT=0
    SET @Get_AllSUBCategory6_Spec_Count=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute6_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L6 and TCS.CATALOG_ID=@Catalog_ID)
    SET @Get_SUBCATEGORY6_SPECS_COUNT=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute6_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L6 and TCS.CATALOG_ID=@Catalog_ID and TCS.ATTRIBUTE_ID=SCN.ATTRIBUTE_ID)
    IF(@Get_SUBCATEGORY6_SPECS_COUNT<>0)
    BEGIN
     UPDATE ##SUBCATEGORYAttribute6_Column SET ##SUBCATEGORYAttribute6_Column.Flag='Update',##SUBCATEGORYAttribute6_Column.Row#=TB_CATEGORY_SPECS.SORT_ORDER from TB_CATEGORY_SPECS ,##SUBCATEGORYAttribute6_Column  where  TB_CATEGORY_SPECS.CATEGORY_ID=##SUBCATEGORYAttribute6_Column.SUBCATID_L6 and TB_CATEGORY_SPECS.CATALOG_ID=@Catalog_ID and TB_CATEGORY_SPECS.ATTRIBUTE_ID=##SUBCATEGORYAttribute6_Column.ATTRIBUTE_ID
    END
    IF(@Get_AllSUBCategory6_Spec_Count=0)
     BEGIN
	    Print'SUB6'
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute6_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute6_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute6_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute6_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute6_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute6_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute6_Column].Row#,'''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute6_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute6_Column].SUBCATID_L6= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute6_Column].SUBCATID_L6 IS NOT NULL AND [##SUBCATEGORYAttribute6_Column].ATTRIBUTE_DATATYPE LIKE ''Text%''  
    ')
     END
    IF(@Get_AllSUBCategory6_Spec_Count<>0)
    BEGIN
	   Print'SUB6'
    DECLARE @MAXSUB6ATTRID VARCHAR(600)  
	SET @MAXSUB6ATTRID = (select MAX(SORT_ORDER) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute6_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L6 and TCS.CATALOG_ID=@Catalog_ID)
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute6_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute6_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute6_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute6_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute6_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute6_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute6_Column].Row# '+''''+@MAXSUB6ATTRID+''','''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute6_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute6_Column].SUBCATID_L6= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute6_Column].SUBCATID_L6 IS NOT NULL AND [##SUBCATEGORYAttribute6_Column].ATTRIBUTE_DATATYPE LIKE ''Text%'' and [##SUBCATEGORYAttribute6_Column].Flag=''Insert''  ')
     EXEC('UPDATE TB_CATEGORY_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.STRING_VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
     else    convert(nvarchar(max),SIT.STRING_VALUE) End FROM [##SUBCATEGORYAttribute6_Column] SIT WHERE 
		SIT.ATTRIBUTE_ID=TB_CATEGORY_SPECS.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%''  AND  SIT.Flag=''Update''')
    END
	Set @InsertCount_Details=@InsertCount_Details + (Select count(*) from ##SUBCATEGORYAttribute6_Column where FLAG='Insert')
    SET @UpdateCount_Details=@UpdateCount_Details + (Select count(*) from ##SUBCATEGORYAttribute6_Column where FLAG='Update')
    END
	
    END


------------------*********SUBCATEGORY LEVEL6 CATEGORY END****************------------------------------------------ 

------------------*********SUBCATEGORY LEVEL7 CATEGORY ****************------------------------------------------
    IF (@Get_Filtered_SubcategoryColumn_Names7 <>'' OR @Get_Filtered_SubcategoryColumn_Names7<>null and @Get_Filtered_AttributeSubcategoryColumn_Names7 <>'' OR @Get_Filtered_AttributeSubcategoryColumn_Names7<>null )
  BEGIN
   Print'SUB1'
  IF(OBJECT_ID('tempdb..##SUB_Category7') is not null) drop table ##SUB_Category7
  SET @Get_Filtered_SubcategoryColumn_Names7=RIGHT(@Get_Filtered_SubcategoryColumn_Names7,LEN(@Get_Filtered_SubcategoryColumn_Names7)-1)
  select @Get_Filtered_SubcategoryColumn_Names7
    SET @Get_Filtered_SubcategoryColumn_Names7 = REPLACE(@Get_Filtered_SubcategoryColumn_Names7, ',', '],[') 
  SET @Get_Filtered_SubcategoryColumn_Names7 = REPLACE(@Get_Filtered_SubcategoryColumn_Names7, ')', '])')
  SET @Get_Filtered_SubcategoryColumn_Names7 = REPLACE(@Get_Filtered_SubcategoryColumn_Names7, '(', '([')
  SET @Get_Filtered_SubcategoryColumn_Names7='[' + @Get_Filtered_SubcategoryColumn_Names7 +']'
  EXEC('SELECT '+ @Get_Filtered_SubcategoryColumn_Names7+ ' into ##SUB_Category7  FROM [##IMPORTTEMP'+ @SID +']') 
  SET @Get_Category_AttributeName=''
  select @Get_Category_AttributeName=(SELECT  SUBSTRING((SELECT ',[' +  REPLACE(name,']',']]') + ']' FROM TEMPDB.sys.all_columns where object_id= OBJECT_ID('tempdb..##SUB_Category7') and name not like 'SUBCATID_L7' and  name not like 'SUBCATNAME_L7' and  name not like 'CATEGORY_PUBLISH2WEB' and  name not like 'CATEGORY_PUBLISH2PRINT' and  name not like 'CATEGORY_PUBLISH2PDF' and  name not like 'CATEGORY_PUBLISH2EXPORT' and  name not like 'CATEGORY_PUBLISH2PORTAL' FOR XML PATH('')),2,200000))
  SELECT @Get_Category_AttributeName = (SELECT REPLACE(@Get_Category_AttributeName, ',[],', ','))
  select @Get_Category_AttributeName
  IF(@Get_Category_AttributeName IS NOT NULL)
   BEGIN
   Print'SUB7'
    IF(OBJECT_ID('tempdb..##SubLevel7Column_Names') is not null) drop table ##SubLevel7Column_Names
    EXEC ('SELECT row_number() over(ORDER BY (Select 1)) as Row#,SUBCATID_L7,SUBCATNAME_L7,PA.ATTRIBUTE_NAME,PA.STRING_VALUE into ##SubLevel7Column_Names FROM(SELECT SUBCATID_L7,SUBCATNAME_L7,ATTRIBUTE_NAME,STRING_VALUE FROM [##SUB_Category7] 
    UNPIVOT (        
    STRING_VALUE      
    FOR ATTRIBUTE_NAME IN ('+@Get_Category_AttributeName+') ) P)PA')
   if(OBJECT_ID('tempdb..##AttributeSubLevel7_Category') is not null) drop table ##AttributeSubLevel7_Category
   SET @Get_Filtered_AttributeSubcategoryColumn_Names7=RIGHT(@Get_Filtered_AttributeSubcategoryColumn_Names7,LEN(@Get_Filtered_AttributeSubcategoryColumn_Names7)-1)
   if(OBJECT_ID('tempdb..##AttributeSubLevel7Column_Names') is not null) drop table ##AttributeSubLevel7Column_Names
   CREATE table ##AttributeSubLevel7Column_Names
   (
   ATTRIBUTE_NAME varchar(700)
   )      
    Insert Into ##AttributeSubLevel7Column_Names select @Get_Filtered_AttributeSubcategoryColumn_Names7 
    if(OBJECT_ID('tempdb..##OriginalSubLevel7AttributeColumn') is not null) drop table ##OriginalSubLevel7AttributeColumn
    select item into ##OriginalSubLevel7AttributeColumn from ##AttributeSubLevel7Column_Names CROSS APPLY SplitString(ATTRIBUTE_NAME, ',')
    EXEC('SELECT row_number() OVER(ORDER BY (SELECT 1))AS Row#,* into ##AttributeSubLevel7_Category FROM [##OriginalSubLevel7AttributeColumn] where item NOT IN (''SUBCATID_L7'',''SUBCATNAME_L7'',''CATEGORY_PUBLISH2WEB'',''CATEGORY_PUBLISH2PRINT'',''CATEGORY_PUBLISH2PDF'',''CATEGORY_PUBLISH2EXPORT'',''CATEGORY_PUBLISH2PORTAL'') ')
    ALTER TABLE ##SubLevel7Column_Names ADD FLAG Varchar(70)
    UPDATE ##SubLevel7Column_Names SET ##SubLevel7Column_Names.ATTRIBUTE_NAME = ##AttributeSubLevel7_Category.item, ##SubLevel7Column_Names.FLAG='Insert' FROM ##SubLevel7Column_Names, ##AttributeSubLevel7_Category
    WHERE ##SubLevel7Column_Names.Row# = ##AttributeSubLevel7_Category.Row# 
    if(OBJECT_ID('tempdb..##SUBCATEGORYAttribute7_Column') is not null) drop table ##SUBCATEGORYAttribute7_Column
    select PA.ROW#,PA.SUBCATID_L7,PA.SUBCATNAME_L7,PA.ATTRIBUTE_NAME,PA.STRING_VALUE,TA.ATTRIBUTE_ID,TA.ATTRIBUTE_DATATYPE,TA.ATTRIBUTE_TYPE,PA.FLAG INTo ##SUBCATEGORYAttribute7_Column from ##SubLevel7Column_Names PA join TB_ATTRIBUTE TA ON PA.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME 
    DECLARE @Get_SUBCATEGORY7_SPECS_COUNT INT=0
    Declare @Get_AllSUBCategory7_Spec_Count INT=0
    SET @Get_AllSUBCategory7_Spec_Count=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute7_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L7 and TCS.CATALOG_ID=@Catalog_ID)
    SET @Get_SUBCATEGORY7_SPECS_COUNT=(select count(*) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute7_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L7 and TCS.CATALOG_ID=@Catalog_ID and TCS.ATTRIBUTE_ID=SCN.ATTRIBUTE_ID)
    IF(@Get_SUBCATEGORY7_SPECS_COUNT<>0)
    BEGIN
     UPDATE ##SUBCATEGORYAttribute7_Column SET ##SUBCATEGORYAttribute7_Column.Flag='Update',##SUBCATEGORYAttribute7_Column.Row#=TB_CATEGORY_SPECS.SORT_ORDER from TB_CATEGORY_SPECS ,##SUBCATEGORYAttribute7_Column  where  TB_CATEGORY_SPECS.CATEGORY_ID=##SUBCATEGORYAttribute7_Column.SUBCATID_L7 and TB_CATEGORY_SPECS.CATALOG_ID=@Catalog_ID and TB_CATEGORY_SPECS.ATTRIBUTE_ID=##SUBCATEGORYAttribute7_Column.ATTRIBUTE_ID
    END
    IF(@Get_AllSUBCategory7_Spec_Count=0)
     BEGIN
	    Print'SUB7'
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute7_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute7_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute7_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute7_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute7_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute7_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute7_Column].Row#,'''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute7_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute7_Column].SUBCATID_L7= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute7_Column].SUBCATID_L7 IS NOT NULL AND [##SUBCATEGORYAttribute7_Column].ATTRIBUTE_DATATYPE LIKE ''Text%''  
    ')
     END
    IF(@Get_AllSUBCategory7_Spec_Count<>0)
    BEGIN
	   Print'SUB7'
    DECLARE @MAXSUB7ATTRID VARCHAR(700)  
	SET @MAXSUB7ATTRID = (select MAX(SORT_ORDER) from TB_CATEGORY_SPECS TCS Join ##SUBCATEGORYAttribute7_Column SCN ON TCS.CATEGORY_ID=SCN.SUBCATID_L7 and TCS.CATALOG_ID=@Catalog_ID)
     EXEC('INSERT INTO TB_CATEGORY_SPECS(STRING_VALUE,CATEGORY_ID,ATTRIBUTE_ID,CATALOG_ID,SORT_ORDER,CREATED_USER,CREATED_DATE,MODIFIED_DATE,MODIFIED_USER,FLAG_RECYCLE,PUBLICHCATEGORY_FLAG)
     SELECT case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),[##SUBCATEGORYAttribute7_Column].STRING_VALUE))),Replace(substring([##SUBCATEGORYAttribute7_Column].ATTRIBUTE_DATATYPE,charindex(''('',[##SUBCATEGORYAttribute7_Column].ATTRIBUTE_DATATYPE)+1,len([##SUBCATEGORYAttribute7_Column].ATTRIBUTE_DATATYPE)),'')'',''''))
     else  convert(nvarchar(max),[##SUBCATEGORYAttribute7_Column].STRING_VALUE) End as String_value,TB_CATEGORY.CATEGORY_ID,[##SUBCATEGORYAttribute7_Column].ATTRIBUTE_ID,'''+@Catalog_ID+''',[##SUBCATEGORYAttribute7_Column].Row# '+''''+@MAXSUB7ATTRID+''','''+@CUSTOMERNAME+''',GETDATE(),GETDATE(),'''+@CUSTOMERNAME+''',''A'',1 FROM [##SUBCATEGORYAttribute7_Column] join TB_CATEGORY  on [##SUBCATEGORYAttribute7_Column].SUBCATID_L7= TB_CATEGORY.CATEGORY_ID
     WHERE [##SUBCATEGORYAttribute7_Column].SUBCATID_L7 IS NOT NULL AND [##SUBCATEGORYAttribute7_Column].ATTRIBUTE_DATATYPE LIKE ''Text%'' and [##SUBCATEGORYAttribute7_Column].Flag=''Insert''  ')
     EXEC('UPDATE TB_CATEGORY_SPECS SET STRING_VALUE=case when ATTRIBUTE_DATATYPE like ''Text(%'' then   LEFT(LTRIM(RTRIM( convert(nvarchar(max),SIT.STRING_VALUE))),Replace(substring(SIT.ATTRIBUTE_DATATYPE,charindex(''('',SIT.ATTRIBUTE_DATATYPE)+1,len(SIT.ATTRIBUTE_DATATYPE)),'')'',''''))
     else    convert(nvarchar(max),SIT.STRING_VALUE) End FROM [##SUBCATEGORYAttribute7_Column] SIT WHERE 
		SIT.ATTRIBUTE_ID=TB_CATEGORY_SPECS.ATTRIBUTE_ID AND SIT.ATTRIBUTE_DATATYPE LIKE ''Text%''  AND  SIT.Flag=''Update''')
    END
	Set @InsertCount_Details=@InsertCount_Details + (Select count(*) from ##SUBCATEGORYAttribute7_Column where FLAG='Insert')
    SET @UpdateCount_Details=@UpdateCount_Details + (Select count(*) from ##SUBCATEGORYAttribute7_Column where FLAG='Update')
    END
	
  END
  if(OBJECT_ID('tempdb..##AttributeCount') is not null) drop table ##AttributeCount
   CREATE table ##AttributeCount
   (
    InsertCount int,
	UpdateCount int
   )      
    Insert Into ##AttributeCount select @InsertCount_Details,@UpdateCount_Details 
  Select @InsertCount_Details
   Select @UpdateCount_Details

END

------------------*********SUBCATEGORY LEVEL7 CATEGORY END****************------------------------------------------ 
  
  
  
  
  
  
  
 














