

ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO_INSERTCATEGORYTEMPDATA](@TABLENAME NVARCHAR(500))
As
BEGIN

 IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TEMPTABLE')  
   BEGIN  
    EXEC('drop table ##TEMPTABLE ')  
   END  
--select CATEGORY_ID,CATEGORY_NAME,[ATTRIBUTENAME],[ATTRIBUTEVALUE],[ImportId] into ##TEMPTABLE from @TEMPTABLE

--set @TABLENAME='[##IMPORTTEMP1a703192-9bc4-4749-b3c4-ebc2669177c8]'
DECLARE @COLUMNSPIV nVARCHAR(MAX)=''
SELECT @COLUMNSPIV =@COLUMNSPIV +'['+ col.name + '],'  from tempdb.sys.columns col join tempdb.sys.tables tab on col.object_id=tab.object_id and tab.name like ''+replace(replace(@TABLENAME,'[',''),']','')+'%' --and col.name not in ('FAMILY_ID','FAMILY_NAME','SUBFAMILY_ID','SUBFAMILY_NAME')
 order by col.column_id
SET @COLUMNSPIV = LEFT(@COLUMNSPIV, LEN(@COLUMNSPIV) - 1)
print @COLUMNSPIV

DECLARE @COLUMNS nVARCHAR(MAX)=''
SELECT @COLUMNS =@COLUMNS +' CONVERT(NVARCHAR(MAX), CONVERT(VARBINARY(MAX), CONVERT(IMAGE, CAST(['+ col.name + '] AS VARBINARY(MAX))),2)) as ['+col.name+'],' from tempdb.sys.columns col join tempdb.sys.tables tab on col.object_id=tab.object_id and tab.name like ''+replace(replace(@TABLENAME,'[',''),']','')+'%' and col.name not in ('CATEGORY_ID') --and col.name not in ('PRODUCT_ID','CATALOG_ITEM_NO')
 order by col.column_id
select @COLUMNS = REPLACE(@COLUMNS,'CONVERT(NVARCHAR(MAX), CONVERT(VARBINARY(MAX), CONVERT(IMAGE, CAST([CATEGORY_NAME] AS VARBINARY(MAX))),2)) as [CATEGORY_NAME]','[CATEGORY_ID],CONVERT(NVARCHAR(MAX), CONVERT(VARBINARY(MAX), CONVERT(IMAGE, CAST([CATEGORY_NAME] AS VARBINARY(MAX))),2)) as [CATEGORY_NAME]')

SET @COLUMNS = LEFT(@COLUMNS, LEN(@COLUMNS) - 1)
print @COLUMNS



DECLARE @INSERTCOLUMNS VARCHAR(MAX)=''
SELECT @INSERTCOLUMNS =@INSERTCOLUMNS +'['+ col.name + '],'  from tempdb.sys.columns col join tempdb.sys.tables tab on col.object_id=tab.object_id and tab.name like ''+replace(replace(@TABLENAME,'[',''),']','')+'%' and col.name not in ('CATEGORY_ID','CATEGORY_NAME') --and col.name not in ('PRODUCT_ID','CATALOG_ITEM_NO')
 order by col.column_id
SET @INSERTCOLUMNS = LEFT(@INSERTCOLUMNS, LEN(@INSERTCOLUMNS) - 1)
print @INSERTCOLUMNS
 EXEC('select '+@COLUMNS+' into ##TEMPTABLE from'+@TABLENAME+'')
 IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME=''+replace(replace(@TABLENAME,'[',''),']','')+'')  
   BEGIN  
    EXEC('drop table  '+@TABLENAME)  
   END 
   EXEC('select '+@COLUMNS+',0 as rowImportid into '+@TABLENAME+' from
   (select '+@COLUMNS+' from [##TEMPTABLE])as tep1')

  IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='##TEMPTABLE')  
   BEGIN  
    EXEC('drop table ##TEMPTABLE ')  
   END  


 --exec('select * from '+@TABLENAME)
END













