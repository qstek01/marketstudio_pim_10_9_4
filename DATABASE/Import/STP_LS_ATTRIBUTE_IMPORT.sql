

ALTER procedure [dbo].[STP_LS_ATTRIBUTE_IMPORT](
@ATTRNAME NVARCHAR(MAX)='',
@SESSION_ID NVARCHAR(MAX)='96e37b02-d2ed-4b58-a3d5-e68a0d97dd5d',
@OPTION Nvarchar(50)='IMPORT',
@CUSTOMER_ID INT=8,
@CREATED_USER VARCHAR(MAX)='demo@questudio.com' ,
@MODIFIED_USER VARCHAR(MAX)='demo@questudio.com',
@CATALOG_ID INT='29'
) 
as
begin
declare @idcount int
                              /*-------------------VALIDATION-------------------*/
if(@OPTION='VALIDATION ATTR_NAMES and caption')
begin
exec('
;with cte
As(
select  Attribute_name,Caption from [##IMPORTTEMP'+@SESSION_ID+'] where Attribute_name not in (select Attribute_name from tb_attribute) and Attribute_id is null)
select Attribute_name from cte where Attribute_name <> caption and Attribute_name in (select caption from tb_attribute) 
')

exec('
;with cte
As(
select Attribute_name,Caption from [##IMPORTTEMP'+@SESSION_ID+'] where Attribute_name not in (select Attribute_name from tb_attribute) and Attribute_id is null)
select Attribute_name from cte where Attribute_name <> caption and Attribute_name in (select caption from [##IMPORTTEMP'+@SESSION_ID+'])
')

end

if(@OPTION='DATATYPE VALIDATION')
begin

EXEC('
SELECT ATTRIBUTE_NAME,ATTRIBUTE_TYPE, ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM [##IMPORTTEMP'+@SESSION_ID+'] WHERE ATTRIBUTE_TYPE IN (''Product Specifications'',''Category Specifications'',''Family Specifications'',''Family Key'',''Product Key'',''Category Description'') and ((ATTRIBUTE_DATATYPE like ''Tex%'' and ATTRIBUTE_DATAFORMAT not in (''All Characters'',''Alpha Numeric'',''System default'',''Alphabets Only''))or 
(ATTRIBUTE_DATATYPE like ''Num%'' and ATTRIBUTE_DATAFORMAT not in (''Integer'',''Real Numbers'') )or (ATTRIBUTE_DATATYPE like ''Date%'' and ATTRIBUTE_DATAFORMAT not in(''Short Format (dd/mm/yyyy)'',''Short Format (mm/dd/yyyy)'',''Long Format'',''DateTime'',''System default''))or
(ATTRIBUTE_DATATYPE like ''Hyper%'' and ATTRIBUTE_DATAFORMAT not like ''Hyper%''))

union all 

SELECT ATTRIBUTE_NAME,ATTRIBUTE_TYPE, ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM [##IMPORTTEMP'+@SESSION_ID+'] WHERE ATTRIBUTE_TYPE IN (''Product Image/Attachment'',''Family Image/Attachment'',''Category Image/Attachment'',''Family Description'') and (ATTRIBUTE_DATATYPE <> ''Text'' 
or ATTRIBUTE_DATAFORMAT <> ''All Characters'' )

union all 

SELECT ATTRIBUTE_NAME,ATTRIBUTE_TYPE, ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM [##IMPORTTEMP'+@SESSION_ID+'] WHERE ATTRIBUTE_TYPE in (''Product Price'',''Family Price'') and (ATTRIBUTE_DATATYPE not like ''Number%''
and ATTRIBUTE_DATAFORMAT <> ''Integer'')')


end




if(@OPTION='VALIDATION DUP_ATTR_NAME')
begin
print 0
-- EXEC('
--;WITH VALCTE
--AS (
--  SELECT ATTRIBUTE_NAME,CAPTION FROM TB_ATTRIBUTE TA 
--  JOIN  CUSTOMERATTRIBUTE CA ON TA.ATTRIBUTE_ID = CA.ATTRIBUTE_ID WHERE CA.CUSTOMER_ID = '''+@CUSTOMER_ID+'''
   
--   EXCEPT

--    SELECT ATTRIBUTE_NAME,CAPTION FROM [##IMPORTTEMP'+@SESSION_ID+']
--	)

--   select * from VALCTE WHERE ATTRIBUTE_NAME IN (SELECT CAPTION FROM [##IMPORTTEMP'+@SESSION_ID+']) OR ATTRIBUTE_NAME IN (SELECT ATTRIBUTE_NAME FROM [##IMPORTTEMP'+@SESSION_ID+']) OR CAPTION IN (SELECT ATTRIBUTE_NAME FROM [##IMPORTTEMP'+@SESSION_ID+'])
--   OR CAPTION IN (SELECT CAPTION FROM [##IMPORTTEMP'+@SESSION_ID+'])
--   ')
   
   
exec('
;WITH VALCTE
AS (

SELECT Attribute_id,ATTRIBUTE_NAME,CAPTION FROM [##IMPORTTEMP'+@SESSION_ID+'] where Attribute_id is not null
  
   
   EXCEPT

   SELECT TA.Attribute_id,ATTRIBUTE_NAME,CAPTION FROM TB_ATTRIBUTE TA 
  JOIN  CUSTOMERATTRIBUTE CA ON TA.ATTRIBUTE_ID = CA.ATTRIBUTE_ID WHERE CA.CUSTOMER_ID = '''+@CUSTOMER_ID+''' 
	)

   select VALCTE.* from VALCTE join TB_ATTRIBUTE ta on VALCTE.ATTRIBUTE_ID=ta.ATTRIBUTE_ID where (VALCTE.ATTRIBUTE_NAME<>ta.ATTRIBUTE_NAME or VALCTE.Caption<>ta.Caption) 
   and (VALCTE.ATTRIBUTE_NAME IN (SELECT CAPTION FROM TB_ATTRIBUTE) 
   OR VALCTE.ATTRIBUTE_NAME IN (SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE)) 
   AND (VALCTE.CAPTION IN (SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE)
  OR VALCTE.CAPTION IN (SELECT CAPTION FROM TB_ATTRIBUTE))
  ')
end




                                    /*----------Delete------------*/

 if(@OPTION = 'DELETE')
begin

--delete from CUSTOMERATTRIBUTE CA join tb_attribute ta on CA.Attribute_id=ta.Attribute_id where ta.attribute_name=@ATTRNAME
delete from TB_ATTRIBUTE where ATTRIBUTE_NAME = @ATTRNAME
END


                              /*--------------------------IMPORT----------------------------*/

IF(@OPTION = 'IMPORT')
BEGIN

IF OBJECT_ID('tempdb..##attrwithid') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##attrwithid')
	END

IF OBJECT_ID('tempdb..##attrwithoutid') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##attrwithoutid')
	END

IF OBJECT_ID('tempdb..##attrwithoutids') IS NOT NULL 
	BEGIN
		EXEC('DROP TABLE ##attrwithoutids')
	END
exec('alter table [##IMPORTTEMP'+@SESSION_ID+'] drop column Action')

	print 1;
	--select * from [##importtemp93bd09a5-3a39-47e7-8320-71ddb07930b6]

--CREATE TABLE ##attrwithid(ATTRIBUTE_NAME nvarchar(50),ATTRIBUTE_TYPE int,ATTRIBUTE_ID int,CREATE_BY_DEFAULT bit,VALUE_REQUIRED bit,
--STYLE_NAME decimal(5,2),STYLE_FORMAT nvarchar(max),DEFAULT_VALUE INT,PUBLISH2PRINT BIT,PUBLISH2WEB BIT,USE_PICKLIST BIT,ATTRIBUTE_DATATYPE NVARCHAR(50),
--ATTRIBUTE_DATAFORMAT NVARCHAR(MAX),IS_CALCULATED BIT,ATTRIBUTE_CALC_FORMULA NVARCHAR(MAX),PICKLIST_NAME NVARCHAR(50),ATTRIBUTE_DATARULE NVARCHAR(MAX),
--PUBLISH2PDF BIT,PUBLISH2EXPORT BIT,PUBLISH2PORTAL BIT
--);

--print 2
--CREATE TABLE ##attrwithoutid(ATTRIBUTE_NAME nvarchar(50),ATTRIBUTE_TYPE int,ATTRIBUTE_ID int,CREATE_BY_DEFAULT bit,VALUE_REQUIRED bit,
--STYLE_NAME decimal(5,2),STYLE_FORMAT nvarchar(max),DEFAULT_VALUE INT,PUBLISH2PRINT BIT,PUBLISH2WEB BIT,USE_PICKLIST BIT,ATTRIBUTE_DATATYPE NVARCHAR(50),
--ATTRIBUTE_DATAFORMAT NVARCHAR(MAX),IS_CALCULATED BIT,ATTRIBUTE_CALC_FORMULA NVARCHAR(MAX),PICKLIST_NAME NVARCHAR(50),ATTRIBUTE_DATARULE NVARCHAR(MAX),
--PUBLISH2PDF BIT,PUBLISH2EXPORT BIT,PUBLISH2PORTAL BIT
--);

print 3

exec('select * into ##attrwithid from [##IMPORTTEMP'+@SESSION_ID+'] where ATTRIBUTE_ID IS NOT NULL')
exec('select * into ##attrwithoutid from [##IMPORTTEMP'+@SESSION_ID+'] where ATTRIBUTE_ID IS NULL')
--EXEC('insert into ##attrwithid select * from [##IMPORTTEMP'+@SESSION_ID+'] where ATTRIBUTE_ID IS NOT NULL')
--EXEC('insert into ##attrwithoutid select * from [##IMPORTTEMP'+@SESSION_ID+'] where ATTRIBUTE_ID IS NULL')
 --select *,dense_RANK() OVER (ORDER BY ATTRIBUTE_NAME)ATTRIBUTE_NAME_dense_rank INTO ##attrwithoutids FROM ##attrwithoutid;

 print 4

             /*----------------------ATTR WITH_ID----------------*/


if (SELECT COUNT(*) FROM ##attrwithid)>0 
BEGIN

ALTER TABLE ##attrwithid add FLAG varchar(20);

update temp set FLAG = 'update' from ##attrwithid temp join TB_ATTRIBUTE TA on temp.ATTRIBUTE_ID = TA.ATTRIBUTE_ID

select COUNT(*) as updated from ##attrwithid where FLAG='update'
--select COUNT(*) as inserted from ##attrwithid where Flag IS NULL

UPDATE TA SET TA.ATTRIBUTE_NAME=TEMP.ATTRIBUTE_NAME,TA.STYLE_NAME=TEMP.STYLE_NAME,TA.PUBLISH2PRINT=TEMP.PUBLISH2PRINT,TA.PUBLISH2WEB=TEMP.PUBLISH2WEB,TA.PUBLISH2PDF=TEMP.PUBLISH2PDF,
TA.PUBLISH2EXPORT='1',TA.PUBLISH2PORTAL='1',TA.CREATE_BY_DEFAULT=TEMP.CREATE_BY_DEFAULT,TA.VALUE_REQUIRED=TEMP.VALUE_REQUIRED,TA.USE_PICKLIST=TEMP.USE_PICKLIST,
TA.CAPTION=TEMP.CAPTION
FROM TB_ATTRIBUTE TA JOIN ##attrwithid TEMP ON TA.ATTRIBUTE_ID=TEMP.ATTRIBUTE_ID WHERE TA.FLAG_RECYCLE='A'

--SELECT * FROM TB_ATTRIBUTE where attribute_name like 'parts cos%'

END

print 5

if (SELECT COUNT(*) FROM ##attrwithoutid)>0
BEGIN

ALTER TABLE ##attrwithoutid ADD FLAG VARCHAR(20);

UPDATE ##attrwithoutid SET FLAG='update' FROM ##attrwithoutid TEMP JOIN TB_ATTRIBUTE TA ON TEMP.ATTRIBUTE_NAME=TA.ATTRIBUTE_NAME

SELECT COUNT(*) as updated FROM  ##attrwithoutid WHERE FLAG='update'
select count(*) as inserted from ##attrwithoutid where flag is null

--update temp set temp.ATTRIBUTE_ID = TA.ATTRIBUTE_ID from ##attrwithoutids temp join TB_ATTRIBUTE TA on temp.ATTRIBUTE_NAME = TA.ATTRIBUTE_NAME where temp.Flag ='update'

--if (select COUNT(*) from ##attrwithoutids where Flag is null)>0
-- begin

--   set @idcount = (select MAX(ATTRIBUTE_ID) from TB_ATTRIBUTE)
--   print @idcount

--   update ##attrwithoutids set ATTRIBUTE_ID=(@idcount+ATTRIBUTE_NAME_dense_rank)
-- end

UPDATE TA SET TA.STYLE_NAME=TEMP.STYLE_NAME,TA.PUBLISH2PRINT=TEMP.PUBLISH2PRINT,TA.PUBLISH2WEB=TEMP.PUBLISH2WEB,TA.PUBLISH2PDF=TEMP.PUBLISH2PDF,
TA.PUBLISH2EXPORT='1',TA.PUBLISH2PORTAL='1',TA.CREATE_BY_DEFAULT=TEMP.CREATE_BY_DEFAULT,TA.VALUE_REQUIRED=TEMP.VALUE_REQUIRED,TA.USE_PICKLIST=TEMP.USE_PICKLIST,
TA.CAPTION=TEMP.CAPTION
FROM TB_ATTRIBUTE TA JOIN ##attrwithoutid TEMP ON TA.ATTRIBUTE_ID=TEMP.ATTRIBUTE_ID WHERE temp.flag='update' and TA.FLAG_RECYCLE='A'

print 555

Set IDENTITY_INSERT TB_Attribute OFF



if(SELECT COUNT(CAPTION) FROM ##attrwithoutid WHERE FLAG IS NULL)>0
BEGIN

Exec('insert into TB_ATTRIBUTE (ATTRIBUTE_NAME,ATTRIBUTE_TYPE,CREATE_BY_DEFAULT,VALUE_REQUIRED,STYLE_NAME,STYLE_FORMAT,DEFAULT_VALUE,PUBLISH2PRINT,PUBLISH2WEB,PUBLISH2CDROM,PUBLISH2ODP,USE_PICKLIST,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT,ATTRIBUTE_DATARULE,IS_CALCULATED,ATTRIBUTE_CALC_FORMULA,PICKLIST_NAME,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE,PUBLISH2PDF,PUBLISH2EXPORT,PUBLISH2PORTAL,CAPTION)
 select ATTRIBUTE_NAME,ATTRIBUTE_TYPE,CREATE_BY_DEFAULT,VALUE_REQUIRED,STYLE_NAME,STYLE_FORMAT,DEFAULT_VALUE,PUBLISH2PRINT,PUBLISH2WEB,1,1,USE_PICKLIST,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT,ATTRIBUTE_DATARULE,IS_CALCULATED,ATTRIBUTE_CALC_FORMULA,PICKLIST_NAME,'''+ @CREATED_USER +''',GETDATE(),'''+ @MODIFIED_USER +''',GETDATE(),PUBLISH2PDF,1,1,CAPTION from ##attrwithoutid where Flag is null')
 END
ELSE
BEGIN
EXEC('insert into TB_ATTRIBUTE (ATTRIBUTE_NAME,ATTRIBUTE_TYPE,CREATE_BY_DEFAULT,VALUE_REQUIRED,STYLE_NAME,STYLE_FORMAT,DEFAULT_VALUE,PUBLISH2PRINT,PUBLISH2WEB,PUBLISH2CDROM,PUBLISH2ODP,USE_PICKLIST,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT,ATTRIBUTE_DATARULE,IS_CALCULATED,ATTRIBUTE_CALC_FORMULA,PICKLIST_NAME,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE,PUBLISH2PDF,PUBLISH2EXPORT,PUBLISH2PORTAL,CAPTION)
select ATTRIBUTE_NAME,ATTRIBUTE_TYPE,CREATE_BY_DEFAULT,VALUE_REQUIRED,STYLE_NAME,STYLE_FORMAT,DEFAULT_VALUE,PUBLISH2PRINT,PUBLISH2WEB,1,1,USE_PICKLIST,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT,ATTRIBUTE_DATARULE,IS_CALCULATED,ATTRIBUTE_CALC_FORMULA,PICKLIST_NAME,'''+ @CREATED_USER +''',GETDATE(),'''+ @MODIFIED_USER +''',GETDATE(),PUBLISH2PDF,1,1,ATTRIBUTE_NAME from ##attrwithoutid where Flag is null')
END



update temp set temp.attribute_id= ta.attribute_id from ##attrwithoutid temp join tb_attribute ta on temp.attribute_name = ta.attribute_name where ta.flag_recycle='A' 

INSERT INTO TB_CATALOG_ATTRIBUTES (CATALOG_ID,ATTRIBUTE_ID,CREATED_USER,MODIFIED_USER)
SELECT @CATALOG_ID,ATTRIBUTE_ID,@CREATED_USER,@MODIFIED_USER from ##attrwithoutid where Flag is null AND @CATALOG_ID <>1

--Exec('insert into  CUSTOMERATTRIBUTE (CUSTOMER_ID,ATTRIBUTE_ID,MODIFIED_USER,MODIFIED_DATE,CREATED_USER,CREATED_DATE) select ('''+@CUSTOMER_ID+''',ATTRIBUTE_ID,'''+ @MODIFIED_USER +''',GETDATE(),'''+ @CREATED_USER +''',GETDATE()) from ##attrwithoutid where Flag is null')

insert into  CUSTOMERATTRIBUTE (CUSTOMER_ID,MODIFIED_USER,CREATED_USER,ATTRIBUTE_ID)
select @CUSTOMER_ID,@MODIFIED_USER, @CREATED_USER,ATTRIBUTE_ID from ##attrwithoutid where Flag is null


--Exec('insert into TB_catalog_attributes select 7083,ATTRIBUTE_ID,'''+ @CREATED_USER +''',GETDATE(),'''+ @MODIFIED_USER +''',GETDATE() from ##attrwithoutids where Flag is null')

Set IDENTITY_INSERT TB_Attribute ON

--select * from tb_catalog_attributes

END	


		  

END


END


--select * from TB_ATTRIBUTE




