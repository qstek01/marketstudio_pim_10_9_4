
ALTER TABLE TB_CATEGORY_SPECS
ADD PRIMARY KEY(ID)

ALTER TABLE Image_Format_Types
ADD PRIMARY KEY(ID)


ALTER TABLE QS_IMPORTMAPPING
ADD PRIMARY KEY(ID)

ALTER TABLE TB_ASSETDETAILS
 ADD ID INT NOT NULL IDENTITY(1,1)
CONSTRAINT PK_TB_ASSETDETAILS PRIMARY KEY CLUSTERED


ALTER TABLE TB_FUNCTIONGROUP
   ADD ID INT NOT NULL IDENTITY(1,1)
CONSTRAINT PK_TB_FUNCTIONGROUP PRIMARY KEY CLUSTERED



ALTER TABLE TB_INVERTEDPRODUCTS_TEMPLATE
   ADD ID INT NOT NULL IDENTITY(1,1)
CONSTRAINT PK_TB_INVERTEDPRODUCTS_TEMPLATE PRIMARY KEY CLUSTERED


ALTER TABLE TB_MULTIPLE_TABLE_STRUCTURE
   ADD ID INT NOT NULL IDENTITY(1,1)
CONSTRAINT PK_TB_MULTIPLE_TABLE_STRUCTURE PRIMARY KEY CLUSTERED

ALTER TABLE TB_PICKLIST_DATA
ADD PRIMARY KEY(PICKLIST_ID)



ALTER TABLE TB_RECYCLE_FOR_ATTRIBUTES
 ADD ID INT NOT NULL IDENTITY(1,1)
CONSTRAINT PK_TB_RECYCLE_FOR_ATTRIBUTES PRIMARY KEY CLUSTERED


ALTER TABLE TB_RECYCLE_TABLE
 ADD ID INT NOT NULL IDENTITY(1,1)
CONSTRAINT PK_TB_RECYCLE_TABLE PRIMARY KEY CLUSTERED

ALTER TABLE TB_TEMPLATE_MAPPING_DETAILS_IMPORT
ADD PRIMARY KEY(ID)


ALTER TABLE TB_TEMPLATE_SHEET_DETAILS_IMPORT
ADD PRIMARY KEY(IMPORT_SHEET_ID)

ALTER TABLE TB_TRANSACTION_HISTORY
 ADD ID INT NOT NULL IDENTITY(1,1)
CONSTRAINT PK_TB_TRANSACTION_HISTORY PRIMARY KEY CLUSTERED


ALTER TABLE TB_WORKFLOW_SETTINGS
ADD PRIMARY KEY(ID)

ALTER TABLE TB2WS_SYNCDATA
ADD PRIMARY KEY(PACKET_ID)
