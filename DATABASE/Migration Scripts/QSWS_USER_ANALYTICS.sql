
/****** Object:  Table [dbo].[QSWS_USER_ANALYTICS]    Script Date: 2022-11-03 13:28:28 ******/
DROP TABLE [dbo].[QSWS_USER_ANALYTICS]
GO

/****** Object:  Table [dbo].[QSWS_USER_ANALYTICS]    Script Date: 2022-11-03 13:28:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[QSWS_USER_ANALYTICS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[USER_ID] [varchar](max) NULL,
	[Session_Id] [varchar](max) NULL,
	[EMAIL] [varchar](max) NULL,
	[LOGGED_IN_DATETIME] [datetime] NULL,
	[LOGGED_OUT_DATETIME] [datetime] NULL,
	[MODIFIED_DATE] [datetime] NULL,
	[ORGANIZATION_ROLE] [varchar](max) NULL,
	[FLAG] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


