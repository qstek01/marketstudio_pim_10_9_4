

ALTER procedure [dbo].[STP_FOR_EDIT_INVERTED_PRODUCTS]
 @family_id varchar(max)=22527,
@product_Id varchar (max)=13447,
 @catalog_id varchar(max)=29,
 @created_user nvarchar(max)='demo@questudio.com',
 @Role_id int=30
 --@PAGENO int=1,
 --@PERPAGECOUNT int=10,
 --@NULLVALUEATTRIBUTE varchar(max)=''

 as 
 

 IF OBJECT_ID('tempdb..##Temp') IS NOT NULL  
Begin  
 EXEC('DROP TABLE ##Temp')  
end 

IF OBJECT_ID('tempdb..##Temp1') IS NOT NULL  
Begin  
 EXEC('DROP TABLE  ##Temp1')  
end 

BEGIN

 WITH TTT  
AS  
(  
 
SELECT DISTINCT TCF.CATALOG_ID, TPF.FAMILY_ID,
TPS.PRODUCT_ID, TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME='Supplier' THEN ATTRIBUTE_NAME +'__OBJ__0' +'__'+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +'__OBJ__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) END +'__'+ CASE WHEN CAST(ISNULL(TA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END AS ATTRIBUTE_NAME,TPS.STRING_VALUE   
 from TB_PROD_SPECS TPS
join TB_ATTRIBUTE TA on TPS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID
join TB_PROD_FAMILY TPF on TPF.PRODUCT_ID=TPS.PRODUCT_ID
join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TPF.FAMILY_ID
join TB_CATALOG TC on TC.CATALOG_ID=tcf.CATALOG_ID
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@catalog_id 
where TC.CATALOG_ID=@catalog_id and TCF.FAMILY_ID=@family_id and TPS.PRODUCT_ID=@product_Id and TA.ATTRIBUTE_ID<>3  and TA.ATTRIBUTE_TYPE<>4 and TA.ATTRIBUTE_TYPE not in (7,9,11,12,13)
UNION 

SELECT DISTINCT TCF.CATALOG_ID, TPF.FAMILY_ID,
TPS.PRODUCT_ID, TPS.ATTRIBUTE_ID,  
CASE WHEN ATTRIBUTE_NAME='Supplier' THEN ATTRIBUTE_NAME +'__OBJ__0' +'__'+ cast(ATTRIBUTE_TYPE as nvarchar)  
  ELSE ATTRIBUTE_NAME +'__OBJ__'+CONVERT(VARCHAR(1000),TPS.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) END +'__'+ CASE WHEN CAST(ISNULL(TA.ATTRIBUTE_ID,0) 
  as nvarchar) = '0' THEN 'false' ELSE 'true' END+'__'+CASE WHEN CAST(ISNULL(TA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END AS
   ATTRIBUTE_NAME,CONVERT(VARCHAR(1000),TPS.NUMERIC_VALUE )  
 from TB_PROD_SPECS TPS
join TB_ATTRIBUTE TA on TPS.ATTRIBUTE_ID=TA.ATTRIBUTE_ID
join TB_PROD_FAMILY TPF on TPF.PRODUCT_ID=TPS.PRODUCT_ID
join TB_CATALOG_FAMILY TCF on TCF.FAMILY_ID=TPF.FAMILY_ID
join TB_CATALOG TC on TC.CATALOG_ID=tcf.CATALOG_ID
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@catalog_id
where TC.CATALOG_ID=@catalog_id and TCF.FAMILY_ID=@family_id and TPS.PRODUCT_ID=@product_Id and TA.ATTRIBUTE_ID<>3 and TA.ATTRIBUTE_TYPE=4 and TA.ATTRIBUTE_TYPE not in (7,9,11,12,13)
  
)
  select * into ##Temp from TTT
  




;WITH PROD
as
(
SELECT DISTINCT TCF.CATALOG_ID, TPS.FAMILY_ID , TPS.PRODUCT_ID, TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME+'__' + 'OBJ'+'__'+CONVERT(VARCHAR(1000),TA.ATTRIBUTE_ID)+'__'+CAST(ATTRIBUTE_TYPE as nvarchar) +'__'+ CASE WHEN CAST(ISNULL(TRA.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END +'__'+CASE WHEN CAST(ISNULL(TPFAL.ATTRIBUTE_ID,0) as nvarchar) = '0' THEN 'false' ELSE 'true' END  AS ATTRIBUTE_NAME
,TPS.ATTRIBUTE_VALUE as STRING_VALUE FROM  TB_FAMILY TF JOIN TB_CATALOG_FAMILY TCF ON TF.FAMILY_ID = TCF.FAMILY_ID   
JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID = TF.FAMILY_ID  
JOIN TB_PARTS_KEY TPS ON TPS.PRODUCT_ID = TPF.PRODUCT_ID AND TPS.FAMILY_ID =TF.FAMILY_ID and TPS.CATALOG_ID=TCF.CATALOG_ID  
JOIN TB_PRODUCT TP ON TP.PRODUCT_ID = TPS.PRODUCT_ID  AND TP.FLAG_RECYCLE ='A'  
JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TPS.ATTRIBUTE_ID AND (TA.FLAG_RECYCLE ='A' OR TA.ATTRIBUTE_ID =1 OR TA.ATTRIBUTE_ID =3)  
JOIN TB_CATALOG_ATTRIBUTES TCA ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID AND TCA.CATALOG_ID=@CATALOG_ID   
join CUSTOMERATTRIBUTE ca on  ca.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID join Customer_User cu on ca.CUSTOMER_ID=cu.CustomerId and  cu.User_Name =@created_user 
LEFT OUTER JOIN TB_PROD_FAMILY_ATTR_LIST TPFAL ON   TF.FAMILY_ID = TPFAL.FAMILY_ID AND TCA.ATTRIBUTE_ID =TPFAL.ATTRIBUTE_ID   
LEFT OUTER JOIN TB_READONLY_ATTRIBUTES TRA on  TRA.ATTRIBUTE_ID = ta.ATTRIBUTE_ID AND TRA.ROLE_ID =@ROLE_ID  
WHERE TF.FAMILY_ID=@FAMILY_ID AND TCF.CATALOG_ID=@CATALOG_ID and TA.ATTRIBUTE_TYPE not in (7,9,11,12,13)
)

select * into ##Temp1
---TM.PRODUCT_ID,TM.ATTRIBUTE_ID + PD.ATTRIBUTE_ID as ATTRIBUTE_ID,TM.ATTRIBUTE_NAME+PD.ATTRIBUTE_NAME as ATTRIBUTE_NAME, TM.STRING_VALUE+PD.STRING_VALUE as STRING_VALUE 
from PROD PD
--left join ##Temp TM on TM.PRODUCT_ID=PD.PRODUCT_ID
--order by TM.ATTRIBUTE_ID

;with FinalResult
as
(
select * from ##Temp
union
select * from ##Temp1
)

select * from FinalResult where PRODUCT_ID=@product_Id 


--drop table ##Temp
--drop table ##Temp1

 end

