

ALTER procedure [dbo].[STP_LS_GET_RECENTLYMODIFIEDCATEGORY] 
 (
 @CATALOG_ID int = 2,
 @startDate datetime = '2020-06-01 12:00:00' ,
 @endDate datetime = '2020-06-27 12:00:00',
 @MODIFIED_USER nvarchar(100) = 'nathan@wes.net.au',
 @RowsPerPage INT = 5, 
 @PageNumber INT =1,
 @filterOption int =0

 --Need To add Paramerter for date range
 )
 as
 begin

 DECLARE @GETCATALOG_ID INT=@CATALOG_ID
 DECLARE @GETSTARTDATE DATETIME= @startDate
 DECLARE @GETENDDATE DATETIME=@endDate
 DECLARE @GETMODIFIED_USER NVARCHAR(100)=@MODIFIED_USER
 DECLARE @GETROWSPERPAGE INT=@RowsPerPage
 DECLARE @GETPAGENUMBER INT=@PageNumber
 DECLARE @GETFILTEROPTION  INT=@filterOption



 Declare  @categoryCount int=0
 set @categoryCount=(select Count(TCS.CATEGORY_ID) from TB_CATEGORY TC
              inner join TB_CATALOG_SECTIONS TCS on  TC.CATEGORY_ID = TCS.CATEGORY_ID and TCS.CATALOG_ID =  @GETCATALOG_ID and TC.FLAG_RECYCLE = 'A'
			  inner join TB_CATALOG C on C.CATALOG_ID = TCS.CATALOG_ID 
			  inner join Customer_User CU on CU.CustomerId = TC.CUSTOMER_ID and CU.User_Name =  @GETMODIFIED_USER  
			  where  TC.MODIFIED_DATE between @GETSTARTDATE and @GETENDDATE )

IF (@GETFILTEROPTION!=0)
 BEGIN
 select TCS.CATEGORY_ID,tcs.CATALOG_ID,

case when   
      tc.CATEGORY_PARENT_SHORT = '0'
	  then
	  '-'
	  else
	  tc.CATEGORY_PARENT_SHORT
	  end
as PARENT_CATEGORY

,
CU.FirstName + CU.LastName as MODIFIED_USER ,TC.MODIFIED_DATE,

tc.CATEGORY_NAME,@categoryCount as categoryCount from TB_CATEGORY TC
              inner join TB_CATALOG_SECTIONS TCS on  TC.CATEGORY_ID = TCS.CATEGORY_ID and TCS.CATALOG_ID =  @GETCATALOG_ID and TC.FLAG_RECYCLE = 'A'
			  inner join TB_CATALOG C on C.CATALOG_ID = TCS.CATALOG_ID 
			  inner join Customer_User CU on CU.CustomerId = TC.CUSTOMER_ID and CU.User_Name =  @GETMODIFIED_USER  
			  where  TC.MODIFIED_DATE between @GETSTARTDATE and @GETENDDATE 

				   order by CATEGORY_ID  
				 END
				  
ELSE
BEGIN
select TCS.CATEGORY_ID,tcs.CATALOG_ID,

case when   
      tc.CATEGORY_PARENT_SHORT = '0'
	  then
	  '-'
	  else
	  tc.CATEGORY_PARENT_SHORT
	  end
as PARENT_CATEGORY

,
CU.FirstName + CU.LastName as MODIFIED_USER ,TC.MODIFIED_DATE,

tc.CATEGORY_NAME,@categoryCount as categoryCount from TB_CATEGORY TC
              inner join TB_CATALOG_SECTIONS TCS on  TC.CATEGORY_ID = TCS.CATEGORY_ID and TCS.CATALOG_ID =  @GETCATALOG_ID and TC.FLAG_RECYCLE = 'A'
			  inner join TB_CATALOG C on C.CATALOG_ID = TCS.CATALOG_ID 
			  inner join Customer_User CU on CU.CustomerId = TC.CUSTOMER_ID and CU.User_Name =  @GETMODIFIED_USER  
			  where  TC.MODIFIED_DATE between @GETSTARTDATE and @GETENDDATE 

				   order by CATEGORY_ID  
				   OFFSET @GETPAGENUMBER*@GETROWSPERPAGE ROWS
					FETCH NEXT 5 ROWS ONLY



				
END


 end








