
        
ALTER  procedure  [dbo].[STP_LS_GET_RECENTLYMODIFIEDPRODUCTS]         
(@CatalogID int =12,        
 @Customer_id int = 8,        
 @startDate datetime = '2021-04-01 12:48:41.463' ,        
@endDate datetime = '2021-04-20 12:48:41.463',
@modifiedUser nvarchar(max)='demo@questudio.com',
@loadOption nvarchar(100)='8', 
@RowsPerPage INT = 5, 
 @PageNumber INT =1,
 @filterOption int =0                
 )        
AS          
BEGIN     


Declare @ProductCount int=0

  set @ProductCount=(Select count (TP.PRODUCT_ID) from        
              [TB_CATALOG_PRODUCT] TBCP         
             JOIN [Customer_Catalog] cc ON cc.CATALOG_ID = TBCP.CATALOG_ID  and TBCP.CATALOG_ID = @CatalogID and  cc.CustomerId  = @Customer_id         
    join Customer_User cu on cu.CustomerId = cc.CustomerId         
    join TB_CATALOG_FAMILY cf on cc.CATALOG_ID = cf.CATALOG_ID        
                JOIN [TB_PRODUCT] TP ON TP.PRODUCT_ID = TBCP.PRODUCT_ID  AND    TP.FLAG_RECYCLE = 'A' and  tp.MODIFIED_DATE between @startDate and @endDate        
                JOIN [TB_PROD_FAMILY] TPF ON TPF.PRODUCT_ID = TP.PRODUCT_ID AND    TPF.FLAG_RECYCLE = 'A' and cf.FAMILY_ID = TPF.FAMILY_ID        
              JOIN [TB_PROD_SPECS] TPS ON TPS.PRODUCT_ID = TBCP.PRODUCT_ID and cu.User_Name = @modifiedUser   AND TPS.ATTRIBUTE_ID =1  and ISNULL(TPS.STRING_VALUE,'') <> '' )       


IF (@filterOption!=0) 
BEGIN
    Select 1 AS [C1],         
           NULL  AS [C2],         
            NULL AS [C3],        
    TP.PRODUCT_ID ,         
   TPS.[STRING_VALUE] ,         
    TPS.[MODIFIED_DATE] ,         
    CU.FirstName + CU.LastName as MODIFIED_USER,         
  cc.CustomerId,@ProductCount as ProductCount  from        
         
              [TB_CATALOG_PRODUCT] TBCP         
             JOIN [Customer_Catalog] cc ON cc.CATALOG_ID = TBCP.CATALOG_ID  and TBCP.CATALOG_ID = @CatalogID and  cc.CustomerId  = @Customer_id         
    join Customer_User cu on cu.CustomerId = cc.CustomerId         
    join TB_CATALOG_FAMILY cf on cc.CATALOG_ID = cf.CATALOG_ID        
                JOIN [TB_PRODUCT] TP ON TP.PRODUCT_ID = TBCP.PRODUCT_ID  AND    TP.FLAG_RECYCLE = 'A' and  tp.MODIFIED_DATE between @startDate and @endDate        
                JOIN [TB_PROD_FAMILY] TPF ON TPF.PRODUCT_ID = TP.PRODUCT_ID AND    TPF.FLAG_RECYCLE = 'A' and cf.FAMILY_ID = TPF.FAMILY_ID        
              JOIN [TB_PROD_SPECS] TPS ON TPS.PRODUCT_ID = TBCP.PRODUCT_ID and cu.User_Name = @modifiedUser   AND TPS.ATTRIBUTE_ID =1  and ISNULL(TPS.STRING_VALUE,'') <> ''  
  
END
ELSE 
BEGIN
Select   1 AS [C1],         
           NULL  AS [C2],         
            NULL AS [C3],        
    TP.PRODUCT_ID ,         
   TPS.[STRING_VALUE] ,         
    TPS.[MODIFIED_DATE] ,         
    CU.FirstName + CU.LastName as MODIFIED_USER,         
  cc.CustomerId,@ProductCount as ProductCount  from        
         
              [TB_CATALOG_PRODUCT] TBCP         
             JOIN [Customer_Catalog] cc ON cc.CATALOG_ID = TBCP.CATALOG_ID  and TBCP.CATALOG_ID = @CatalogID and  cc.CustomerId  = @Customer_id         
    join Customer_User cu on cu.CustomerId = cc.CustomerId         
    join TB_CATALOG_FAMILY cf on cc.CATALOG_ID = cf.CATALOG_ID        
                JOIN [TB_PRODUCT] TP ON TP.PRODUCT_ID = TBCP.PRODUCT_ID  AND    TP.FLAG_RECYCLE = 'A' and  tp.MODIFIED_DATE between @startDate and @endDate        
                JOIN [TB_PROD_FAMILY] TPF ON TPF.PRODUCT_ID = TP.PRODUCT_ID AND    TPF.FLAG_RECYCLE = 'A' and cf.FAMILY_ID = TPF.FAMILY_ID        
              JOIN [TB_PROD_SPECS] TPS ON TPS.PRODUCT_ID = TBCP.PRODUCT_ID and cu.User_Name = @modifiedUser   AND TPS.ATTRIBUTE_ID =1  and ISNULL(TPS.STRING_VALUE,'') <> '' 
			  
			   order by TP.PRODUCT_ID
			    OFFSET @PageNumber*@RowsPerPage ROWS
					FETCH NEXT 5 ROWS ONLY     
END
             
END        
        
        
        
        
        
        
        
        
        
        
        
        
        


