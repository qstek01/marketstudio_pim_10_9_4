

ALTER procedure [dbo].[STP_LS_GET_DASHBOARD_DATA_COUNT]
(
 @CATALOG_ID int=2,
 @MODIFIED_USER_ID int='6',
 @MODIFIED_USER varchar(50) ='nathan@wes.net.au'
)
as
begin


Declare @GETCATALOG_ID int=@CATALOG_ID

Declare @GETMODIFIED_USER_ID int=@MODIFIED_USER_ID

Declare @GETtMODIFIED_USER varchar(50)=@MODIFIED_USER
--set statistics time on

print('category count start')

--declare @CustomerId int

--set @CustomerId = 'select top 1 Customerid from Customer_User where  User_Name='''+@MODIFIED_USER+''''

--CATEGORY COUNT - START
select count (TCS.CATEGORY_ID) as CATEGORYCOUNT from TB_CATEGORY TC
              inner join TB_CATALOG_SECTIONS TCS on  TC.CATEGORY_ID = TCS.CATEGORY_ID and TCS.CATALOG_ID =  @GETCATALOG_ID and TC.FLAG_RECYCLE = 'A'
			  inner join TB_CATALOG C on C.CATALOG_ID = TCS.CATALOG_ID 
			  inner join Customers CU on CU.CustomerId = @GETMODIFIED_USER_ID

--CATEGORY COUNT - END

print('category count end')

--FAMILY COUNT - START
print('family count start')

 select Count(Distinct (tcf.FAMILY_ID)) AS FAMILYCOUNT
 
  from TB_CATALOG_FAMILY tcf 
    inner join TB_FAMILY tpf on tcf.FAMILY_ID = tpf.FAMILY_ID AND  tcf.FLAG_RECYCLE = 'A' AND tpf.FLAG_RECYCLE = 'A'
	inner join TB_CATALOG tcp on tcp.CATALOG_ID = tcf.CATALOG_ID AND tcp.CATALOG_ID = @GETCATALOG_ID 
	inner join TB_CATEGORY tc on tc.CATEGORY_ID = tcf.CATEGORY_ID
	inner join Customers CU on CU.CustomerId = @GETMODIFIED_USER_ID

	print('family count end')
--FAMILY COUNT - END

-- PRODUCT COUNT - START

print('Product count start')
 
     select  count(Distinct(TBCP.PRODUCT_ID)) as PRODUCTCOUNT from   [TB_CATALOG_PRODUCT] TBCP 
	            JOIN [Customer_Catalog] cc ON cc.CATALOG_ID = TBCP.CATALOG_ID  and TBCP.CATALOG_ID = @GETCATALOG_ID and  cc.CustomerId  = @GETMODIFIED_USER_ID  
				join Customers cu on cu.CustomerId = cc.CustomerId and cu.CustomerId=@GETMODIFIED_USER_ID
				join TB_CATALOG_FAMILY cf on cc.CATALOG_ID = cf.CATALOG_ID
                JOIN [TB_PRODUCT] TP ON TP.PRODUCT_ID = TBCP.PRODUCT_ID  AND    TP.FLAG_RECYCLE = 'A' 
                JOIN [TB_PROD_FAMILY] TPF ON TPF.PRODUCT_ID = TP.PRODUCT_ID AND    TPF.FLAG_RECYCLE = 'A' and cf.FAMILY_ID = TPF.FAMILY_ID
              JOIN [TB_PROD_SPECS] TPS ON TPS.PRODUCT_ID = TBCP.PRODUCT_ID   AND TPS.ATTRIBUTE_ID =1 
			   and ISNULL(TPS.STRING_VALUE,'') <> '' 

             
               -- LEFT OUTER JOIN [TB_CATEGORY] tc ON tc.CUSTOMER_ID = cc.CustomerId  AND tc.FLAG_RECYCLE = 'A' 
                
           print('Product count end') 
-- PRODUCT COUNT - END

-- SUBPRODUCT COUNT - START

print('sub product count start')

SELECT count(*) as SUBPRODUCTCOUNT
FROM TB_SUBPRODUCT TS 
       INNER JOIN TB_PRODUCT TP ON TS.PRODUCT_ID = TP.PRODUCT_ID          AND TP.[FLAG_RECYCLE] = 'A' AND  TS.[FLAG_RECYCLE] = 'A' 
	   INNER JOIN TB_PROD_FAMILY TPF ON TPF.PRODUCT_ID = TP.PRODUCT_ID
	   INNER JOIN TB_PROD_SPECS TPS ON  TS.SUBPRODUCT_ID = TPS.PRODUCT_ID AND TPS.ATTRIBUTE_ID = 1
	   INNER JOIN TB_CATALOG_PRODUCT TCP ON TCP.PRODUCT_ID = TP.PRODUCT_ID AND  TCP.CATALOG_ID =@GETCATALOG_ID
	   INNER JOIN Customer_Catalog CC ON CC.[CATALOG_ID] = TCP.[CATALOG_ID] AND CC.CATALOG_ID =@GETCATALOG_ID AND  CC.CustomerId =  @GETMODIFIED_USER_ID  
	   INNER JOIN Customer_User CU ON CU.customerid =  CC.CustomerId AND  CU.User_Name = TPS.MODIFIED_USER

	   print('sub product count end')

-- SUBPROCUCT COUNT - END
end









