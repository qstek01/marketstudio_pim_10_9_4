
ALTER procedure [dbo].[STP_LS_GET_RECENTLYMODIFIEDFAMILY]

 (
 @CATALOG_ID int = 2,
 @startDate datetime = '2020-06-01 12:48:41.463' ,
@endDate datetime = '2020-06-27 12:48:41.463',
 @MODIFIED_USER nvarchar(100) = 'nathan@wes.net.au'  ,
 @RowsPerPage INT = 5, 
 @PageNumber INT =1,
 @filterOption int =0
 )
AS
BEGIN

 DECLARE @FamilyCount int=0
 DECLARE @GETCATALOG_ID INT=@CATALOG_ID
 DECLARE @GETSTARTDATE DATETIME= @startDate
 DECLARE @GETENDDATE DATETIME=@endDate
 DECLARE @GETMODIFIED_USER NVARCHAR(100)=@MODIFIED_USER
 DECLARE @GETROWSPERPAGE INT=@RowsPerPage
 DECLARE @GETPAGENUMBER INT=@PageNumber
 DECLARE @GETFILTEROPTION  INT=@filterOption

IF OBJECT_ID('tempdb..#RecentlyModifyFamily', 'U') IS NOT NULL
BEGIN
DROP TABLE #RecentlyModifyFamily
END

IF OBJECT_ID('tempdb..#RecentlyModifyFamilyFilter', 'U') IS NOT NULL
BEGIN
DROP TABLE ##RecentlyModifyFamilyFilter
END


Set @FamilyCount=(select 
 
count( tpf.FAMILY_ID) 


  from TB_CATALOG_FAMILY tcf 
    inner join TB_FAMILY tpf on tcf.FAMILY_ID = tpf.FAMILY_ID and tpf.MODIFIED_DATE between @GETSTARTDATE and @GETENDDATE 
	inner join TB_CATALOG tcp on tcp.CATALOG_ID = tcf.CATALOG_ID
	inner join TB_CATEGORY tc on tc.CATEGORY_ID = tcf.CATEGORY_ID
	inner join Customer_User CU on CU.CustomerId = tc.CUSTOMER_ID
	
where  
    tcp.CATALOG_ID = @GETCATALOG_ID 
and tcf.FLAG_RECYCLE = 'A'
and tpf.FLAG_RECYCLE = 'A'
and cu.User_Name = @GETMODIFIED_USER)

 IF (@filterOption!=0)
 BEGIN
 

begin

 with familyModified as(
 
 select null as EDIT,
 null as PREVIEW,
 tpf.FAMILY_ID,
 tpf.FAMILY_NAME,
 CU.FirstName + CU.LastName as MODIFIED_USER, 
 tpf.MODIFIED_DATE,
 tcf.CATEGORY_ID,
 tcf.CATALOG_ID,
 tcp.CATALOG_NAME,
 tcp.DESCRIPTION,
 tcp.VERSION,
 tcf.SORT_ORDER

  from TB_CATALOG_FAMILY tcf 
    inner join TB_FAMILY tpf on tcf.FAMILY_ID = tpf.FAMILY_ID and tpf.MODIFIED_DATE between @GETSTARTDATE and @GETENDDATE 
	inner join TB_CATALOG tcp on tcp.CATALOG_ID = tcf.CATALOG_ID
	inner join TB_CATEGORY tc on tc.CATEGORY_ID = tcf.CATEGORY_ID
	inner join Customer_User CU on CU.CustomerId = tc.CUSTOMER_ID
	
where  
    tcp.CATALOG_ID = @GETCATALOG_ID 
and tcf.FLAG_RECYCLE = 'A'
and tpf.FLAG_RECYCLE = 'A'
and cu.User_Name = @GETMODIFIED_USER

 )
 
 select * into  #RecentlyModifyFamily  from familyModified  order by MODIFIED_DATE  
end


select *,@FamilyCount as FamilyCount from #RecentlyModifyFamily

END
ELSE
BEGIN
begin

 with familyModified as(
 
 select null as EDIT,
 null as PREVIEW,
 tpf.FAMILY_ID,
 tpf.FAMILY_NAME,
 CU.FirstName + CU.LastName as MODIFIED_USER, 
 tpf.MODIFIED_DATE,
 tcf.CATEGORY_ID,
 tcf.CATALOG_ID,
 tcp.CATALOG_NAME,
 tcp.DESCRIPTION,
 tcp.VERSION,
 tcf.SORT_ORDER

  from TB_CATALOG_FAMILY tcf 
    inner join TB_FAMILY tpf on tcf.FAMILY_ID = tpf.FAMILY_ID and tpf.MODIFIED_DATE between @GETSTARTDATE and @GETENDDATE 
	inner join TB_CATALOG tcp on tcp.CATALOG_ID = tcf.CATALOG_ID
	inner join TB_CATEGORY tc on tc.CATEGORY_ID = tcf.CATEGORY_ID
	inner join Customer_User CU on CU.CustomerId = tc.CUSTOMER_ID
	
where  
    tcp.CATALOG_ID = @GETCATALOG_ID 
and tcf.FLAG_RECYCLE = 'A'
and tpf.FLAG_RECYCLE = 'A'
and cu.User_Name = @GETMODIFIED_USER

 )
 
 select * into  #RecentlyModifyFamilyFilter  from familyModified  order by MODIFIED_DATE  

 END
 

 select *,@FamilyCount as FamilyCount from #RecentlyModifyFamilyFilter  order by MODIFIED_DATE  OFFSET @PageNumber*@RowsPerPage ROWS
					FETCH NEXT 5 ROWS ONLY
					END

 end 












