

ALTER PROCEDURE [dbo].[STP_CATALOGSTUDIO_IMPORTVALIDATION](@SESSIONID VARCHAR(100)='5038079f-7682-44f0-8e8f-869685c63ad0')
AS BEGIN
 if not exists(select 1 from tempdb.sys.tables t  join tempdb.sys.columns c on t.object_id=c.object_id where t.name like '%validationResult'+@SESSIONID+'%' and c.name='ROWID')
 Begin
  
  exec('  alter table [##validationResult'+@SESSIONID+'] add ROWID int identity')
 End

 IF EXISTS(select 1 from tempdb.sys.tables t  join tempdb.sys.columns c on t.object_id=c.object_id where t.name like '%validationResult'+@SESSIONID+'%' AND C.name like 'PRODUCT_PUBLISH2%')
	BEGIN
	print '1'
	EXEC('update [##validationResult'+@SESSIONID+'] SET PRODUCT_PUBLISH2EXPORT=CASE WHEN PRODUCT_PUBLISH2EXPORT=''Y'' THEN ''1'' Else ''0'' end , 
PRODUCT_PUBLISH2PDF=CASE WHEN PRODUCT_PUBLISH2PDF=''Y'' THEN ''1'' Else ''0'' end,
PRODUCT_PUBLISH2PORTAL=CASE WHEN PRODUCT_PUBLISH2PORTAL=''Y'' THEN ''1'' Else ''0'' end,
PRODUCT_PUBLISH2PRINT=CASE WHEN PRODUCT_PUBLISH2PRINT=''Y'' THEN ''1'' Else ''0'' end,
PRODUCT_PUBLISH2WEB=CASE WHEN PRODUCT_PUBLISH2WEB=''Y'' THEN ''1'' Else ''0'' end
 from [##validationResult'+@SESSIONID+']')
 END
 
--exec('select * into ##attrTemp from [##AttributeTemp'+@SESSIONID+']')
 	 declare @val varchar(max)
 -- select @val=COALESCE(@val + ',[' + Replace(attribute_name,'.','#')+']',+'['+Replace(attribute_name,'.','#')+']')  from ##attrTemp where attribute_type<>0 and attribute_name<>'' and attribute_name not in ('CATALOG_ID',
 --'CATALOG_NAME','CATEGORY_ID','CATEGORY_NAME','FAMILY_ID','FAMILY_NAME','SUBFAMILY_ID','SUBFAMILY_NAME','PRODUCT_ID','CATALOG_ITEM_NO')
 --print @val
 
 declare @CATEGORYLEVEL varchar(max)
 select @CATEGORYLEVEL=COALESCE(@CATEGORYLEVEL+',['+c.name+']',+'['+c.name+']') from tempdb.sys.tables t  join tempdb.sys.columns c on t.object_id=c.object_id where t.name like '%validationResult'+@SESSIONID+'%' and (c.name like 'Subcatname%' or c.name='CATEGORY_NAME')
 select @val=COALESCE(@val + ',[' + name+']',+'['+name+']') 
 from (
 select distinct name  from tempdb.sys.columns col join TB_ATTRIBUTE ta on col.name=ta.ATTRIBUTE_NAME
 where object_id=OBJECT_ID(N'tempdb..##validationResult'+@SESSIONID+'') and name not in ('CATALOG_ID', 'CATALOG_NAME','CATEGORY_ID','CATEGORY_NAME','FAMILY_ID',
 'FAMILY_NAME','SUBFAMILY_ID','SUBFAMILY_NAME','PRODUCT_ID','CATALOG_ITEM_NO','ITEM#')) as temp
 --drop table ##attrTemp
 select @val=COALESCE(@val + ',[' + name+']',+'['+name+']') 
 from (
 select distinct name  from tempdb.sys.columns col where object_id=OBJECT_ID(N'tempdb..##validationResult'+@SESSIONID+'') and name like 'PRODUCT_PUBLISH2%'
 ) as temp
 print @val
 if exists(select 1  from sys.tables where name like 'FinalvalidationResult'+@SESSIONID+'%')
 begin
 exec('Drop table [FinalvalidationResult'+@SESSIONID+']')
 End

 if((select  Count(name)  from sys.tables where name in('Missingcolumns'+@SESSIONID+'','validatedresult'+@SESSIONID+'','Picklistlog'+@SESSIONID+''))=3)
 begin
	print '1'
	if(@val is null )
	Begin
	exec(' select * into #validateResult from (select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end 
	as DUPLICATE_ITEM_NO, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case 
	when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  
	case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No order by 
   CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
       ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null 


	   select distinct *  into [FinalvalidationResult' + @SESSIONID + '] from (

  select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee

	union
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME, CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME, CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [validatedresult'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	else
	Begin
	exec(' select * into #validateResult from (select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end 
	as DUPLICATE_ITEM_NO, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case 
	when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  
	case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No order by 
   CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
       ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null 


	   select distinct *  into [FinalvalidationResult' + @SESSIONID + '] from (

  select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee

	union
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME, CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME, CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [validatedresult'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else  if((select  Count(name)  from sys.tables where name in('Missingcolumns'+@SESSIONID+'','validatedresult'+@SESSIONID+''))=2)
 begin
	print '2'
	if(@val is null )
	Begin
	exec(' select * into #validateResult from (select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO, 
	
	case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%''  then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No order by  
  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  
  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
      ) as validate where  DATATYPE_VALIDATION is not null or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null or NEW_CATALOG_ITEM_NO is not null

  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (

  select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [validatedresult'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	else 
	Begin
	exec(' select * into #validateResult from (select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO, 
	
	case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%''  then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No order by  
  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  
  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
      ) as validate where  DATATYPE_VALIDATION is not null or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null or NEW_CATALOG_ITEM_NO is not null

  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (

  select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [validatedresult'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else  if ((select Count(name)  from sys.tables where name in('Missingcolumns'+@SESSIONID+'','Picklistlog'+@SESSIONID+''))=2)
 begin
	print '3'
	if(@val is null )
	Begin
	exec('select * into #validateResult from( select distinct temp.*,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO, case when PICKLIST_VALUE is not null
	 then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%''  then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null  or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null 


	select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt
	print ''1111''
	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	Else
	Begin
	exec('select * into #validateResult from( select distinct temp.*,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO, case when PICKLIST_VALUE is not null
	 then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null  or DUPLICATE_ITEM_NO is not null  or MISSING_COLUMNS is not null 


	select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-''ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt
	print ''1111''
	drop table #validateResult
	drop table [Missingcolumns'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else if((select  Count(name)  from sys.tables where name in('validatedresult'+@SESSIONID+'','Picklistlog'+@SESSIONID+''))=2)
 begin
	print '4'
	if(@val is null )
	Begin
exec('select * into #validateResult from ( select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO
, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%''  then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO

 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null  or DUPLICATE_ITEM_NO is not null

   select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [validatedresult'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	else
	Begin
	exec('select * into #validateResult from ( select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO
, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO

 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null  or DUPLICATE_ITEM_NO is not null

   select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [validatedresult'+@SESSIONID+']
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else if((select  Count(name)  from sys.tables where name in('validatedresult'+@SESSIONID+''))=1)
 begin
	print '5'
	if(@val is null )
	Begin
exec('select * into #validateResult from ( select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,case when rowno=2 then ''Duplicate Records'' end as 
DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null  or DUPLICATE_ITEM_NO is not null
    
   select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME ,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME ,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 

	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [validatedresult'+@SESSIONID+']

	drop table [##validationResult'+@SESSIONID+']
	')
	end
	else 
	Begin
	exec('select * into #validateResult from ( select distinct temp.*,valid.CATALOG_ITEM_NO as NEW_CATALOG_ITEM_NO,case when rowno=2 then ''Duplicate Records'' end as 
DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [validatedresult'+@SESSIONID+'] valid on valid.catalog_item_no=temp.catalog_item_no
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  DATATYPE_VALIDATION is not null or NEW_CATALOG_ITEM_NO is not null  or DUPLICATE_ITEM_NO is not null
    
   select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME ,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New Catalog Item no'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (NEW_CATALOG_ITEM_NO)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME ,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 

	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	drop table [validatedresult'+@SESSIONID+']

	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END

	else if((select  Count(name)  from sys.tables where name in('Picklistlog'+@SESSIONID+''))=1)
 begin
	print '7'
	if(@val is null )
	Begin
exec('select * into #validateResult from ( select distinct temp.*,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO
, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO

  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null   or DUPLICATE_ITEM_NO is not null

    select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	Else
	Begin
	exec('select * into #validateResult from ( select distinct temp.*,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO
, case when PICKLIST_VALUE is not null then PICKLIST_VALUE end PICKLIST_VALUE,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 
then ''Invalid numeric format'' end else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end 
end end as DATATYPE_VALIDATION from (  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Picklistlog'+@SESSIONID+'] pick on pick.attribute_name=attr.attribute_name  and temp.Catalog_item_no=pick.ITEM_NO

  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No 
  order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
   ) as validate where  PICKLIST_VALUE is not null or DATATYPE_VALIDATION is not null   or DUPLICATE_ITEM_NO is not null

    select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	
	select  CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - New picklist value found.'' as MESSAGE,ROWID+2 as ROWID  from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (PICKLIST_VALUE)
	)ee
	union
	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	
	drop table [Picklistlog'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END
	else  if ((select Count(name)  from sys.tables where name in('Missingcolumns'+@SESSIONID+''))=1)
 begin
	print '8'
	print @val
	if(@val is null )
	Begin
	exec('select * into #validateResult from( select distinct temp.*,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where   DATATYPE_VALIDATION is not null or MISSING_COLUMNS is not null or DUPLICATE_ITEM_NO is not null


	  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-'' ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	

	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	
drop table [Missingcolumns'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	end 

	else 
	Begin
	exec('select * into #validateResult from( select distinct temp.*,MISSING_COLUMNS,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
 left join [Missingcolumns'+@SESSIONID+'] miss  on temp.Catalog_item_no=miss.Catalog_item_no
 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where   DATATYPE_VALIDATION is not null or MISSING_COLUMNS is not null or DUPLICATE_ITEM_NO is not null


	  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select top 1 CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''-''ATTRIBUTE_NAME,''-'' ATTRIBUTE_VALUES,VALIDATION_TYPE,''"''+validation+''" - Columns / values not found'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (MISSING_COLUMNS)
	)ee
	union
	
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	

	
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	
drop table [Missingcolumns'+@SESSIONID+']
	drop table [##validationResult'+@SESSIONID+']
	')
	End
	END

	else
	Begin
	print '11'
	if(@val is null )
	Begin
	exec('select * into #validateResult from( select distinct temp.*,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' then case 
	when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,attribute_values as Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ([CATALOG_ITEM_NO])
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where  DATATYPE_VALIDATION is not null or DUPLICATE_ITEM_NO is not null 


	  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	

	drop table [##validationResult'+@SESSIONID+']
	')
	End
	Else
	Begin
	exec('select * into #validateResult from( select distinct temp.*,case when rowno=2 then ''Duplicate Records'' end as DUPLICATE_ITEM_NO,case when attribute_type =4 or ATTRIBUTE_DATATYPE like ''Number%'' or temp.attribute_name like ''PRODUCT_PUBLISH2%'' then case 
	when ISNUMERIC(attribute_values)=0 then ''Invalid numeric format'' end 
	 else case when ATTRIBUTE_DATATYPE=''Date and Time'' then  case when isDate(attribute_values)=0 then ''Invalid date format'' end end end as DATATYPE_VALIDATION from (
  select catalog_name,category_name,family_name,Catalog_Item_No,rowId,attribute_name,attribute_values from [##validationResult'+@SESSIONID+']
 unpivot
 (
 attribute_values for attribute_name in ('+@val+')
 )tt where attribute_values<>''@null'') as temp left join tb_attribute attr on temp.attribute_name=attr.attribute_name 
  left join (select * from (select ROW_NUMBER() over(partition by CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,
  Catalog_Item_No order by  CATALOG_NAME,'+@CATEGORYLEVEL+',FAMILY_NAME,Catalog_Item_No) as rowno,* from [##validationResult'+@SESSIONID+'] 
  where ACTION is null or ACTION ='''') as temmp  where rowno=2)  tt1 on temp.Catalog_Item_No=tt1.Catalog_Item_No
    ) as validate where  DATATYPE_VALIDATION is not null or DUPLICATE_ITEM_NO is not null 


	  select distinct * into [FinalvalidationResult' + @SESSIONID + '] from (
	select CATALOG_NAME,CATEGORY_NAME,FAMILY_NAME,CATALOG_ITEM_NO,''Catalog item no'' as ATTRIBUTE_NAME,CATALOG_ITEM_NO as ATTRIBUTE_VALUES,VALIDATION_TYPE,''Duplicate "ITEM NO" exists.'' as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DUPLICATE_ITEM_NO)
	)ee
	union 
	select catalog_name,category_name,FAMILY_NAME,CATALOG_ITEM_NO,ATTRIBUTE_NAME,ATTRIBUTE_VALUES,VALIDATION_TYPE,validation as MESSAGE,ROWID+2 as ROWID from #validateResult
	unpivot 
	(
	validation for VALIDATION_TYPE in (DATATYPE_VALIDATION)
	)ee)tttt

	drop table #validateResult
	

	drop table [##validationResult'+@SESSIONID+']
	')
	end
	End

	 if exists(select 1  from sys.tables where name like 'FinalvalidationResult'+@SESSIONID+'%')
 begin
 	print '9'
 exec('select * from [FinalvalidationResult'+@SESSIONID+']')
 End
 declare @tempName varchar(100)
declare temptable cursor 
static for
select name from sys.tables where replace(datepart(hh, modify_date)-datepart(hh, GETDATE()),'-','')>2 and (name like 'validatedresult%'  or name like 'errorlog%' or name like 'Missingcolumns%' or name like 'Picklistlog%' or name like 'tempresult%' or name like 'tempresultsub%' or name like 'FinalvalidationResult%')
open temptable
if(@@cursor_rows>0)
Begin 
fetch next from temptable into @tempName
while(@@fetch_status=0)
begin

exec('drop table ['+ @tempName+']')

fetch next from temptable into @tempName
End

End
close temptable
deallocate temptable

END















